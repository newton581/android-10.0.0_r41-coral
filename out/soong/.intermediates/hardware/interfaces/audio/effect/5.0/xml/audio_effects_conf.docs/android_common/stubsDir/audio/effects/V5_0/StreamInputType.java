
package audio.effects.V5_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public enum StreamInputType {
mic,
voice_uplink,
voice_downlink,
voice_call,
camcorder,
voice_recognition,
voice_communication,
unprocessed,
voice_performance;

public java.lang.String getRawName() { throw new RuntimeException("Stub!"); }
}

