
package audio.effects.V5_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class EffectProxyType extends audio.effects.V5_0.EffectType {

public EffectProxyType() { throw new RuntimeException("Stub!"); }

public audio.effects.V5_0.EffectImplType getLibsw() { throw new RuntimeException("Stub!"); }

public void setLibsw(audio.effects.V5_0.EffectImplType libsw) { throw new RuntimeException("Stub!"); }

public audio.effects.V5_0.EffectImplType getLibhw() { throw new RuntimeException("Stub!"); }

public void setLibhw(audio.effects.V5_0.EffectImplType libhw) { throw new RuntimeException("Stub!"); }
}

