
package audio.effects.V5_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AudioEffectsConf {

public AudioEffectsConf() { throw new RuntimeException("Stub!"); }

public audio.effects.V5_0.LibrariesType getLibraries() { throw new RuntimeException("Stub!"); }

public void setLibraries(audio.effects.V5_0.LibrariesType libraries) { throw new RuntimeException("Stub!"); }

public audio.effects.V5_0.EffectsType getEffects() { throw new RuntimeException("Stub!"); }

public void setEffects(audio.effects.V5_0.EffectsType effects) { throw new RuntimeException("Stub!"); }

public audio.effects.V5_0.AudioEffectsConf.Postprocess getPostprocess() { throw new RuntimeException("Stub!"); }

public void setPostprocess(audio.effects.V5_0.AudioEffectsConf.Postprocess postprocess) { throw new RuntimeException("Stub!"); }

public audio.effects.V5_0.AudioEffectsConf.Preprocess getPreprocess() { throw new RuntimeException("Stub!"); }

public void setPreprocess(audio.effects.V5_0.AudioEffectsConf.Preprocess preprocess) { throw new RuntimeException("Stub!"); }

public audio.effects.V5_0.VersionType getVersion() { throw new RuntimeException("Stub!"); }

public void setVersion(audio.effects.V5_0.VersionType version) { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Postprocess {

public Postprocess() { throw new RuntimeException("Stub!"); }

public java.util.List<audio.effects.V5_0.StreamPostprocessType> getStream() { throw new RuntimeException("Stub!"); }
}

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Preprocess {

public Preprocess() { throw new RuntimeException("Stub!"); }

public java.util.List<audio.effects.V5_0.StreamPreprocessType> getStream() { throw new RuntimeException("Stub!"); }
}

}

