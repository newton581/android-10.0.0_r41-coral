
package audio.effects.V5_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public enum StreamOutputType {
voice_call,
system,
ring,
music,
alarm,
notification,
bluetooth_sco,
enforced_audible,
dtmf,
tts;

public java.lang.String getRawName() { throw new RuntimeException("Stub!"); }
}

