
package audio.policy.configuration.V5_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public enum DeviceCategory {
DEVICE_CATEGORY_HEADSET,
DEVICE_CATEGORY_SPEAKER,
DEVICE_CATEGORY_EARPIECE,
DEVICE_CATEGORY_EXT_MEDIA,
DEVICE_CATEGORY_HEARING_AID;

public java.lang.String getRawName() { throw new RuntimeException("Stub!"); }
}

