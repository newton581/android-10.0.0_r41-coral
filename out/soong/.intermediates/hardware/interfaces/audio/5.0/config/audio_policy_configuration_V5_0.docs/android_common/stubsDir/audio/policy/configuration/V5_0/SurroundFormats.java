
package audio.policy.configuration.V5_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class SurroundFormats {

public SurroundFormats() { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V5_0.SurroundFormats.Format> getFormat() { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Format {

public Format() { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V5_0.AudioFormat getName() { throw new RuntimeException("Stub!"); }

public void setName(audio.policy.configuration.V5_0.AudioFormat name) { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V5_0.AudioFormat> getSubformats() { throw new RuntimeException("Stub!"); }

public void setSubformats(java.util.List<audio.policy.configuration.V5_0.AudioFormat> subformats) { throw new RuntimeException("Stub!"); }
}

}

