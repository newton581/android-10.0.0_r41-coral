
package audio.policy.configuration.V5_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AudioPolicyConfiguration {

public AudioPolicyConfiguration() { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V5_0.GlobalConfiguration getGlobalConfiguration() { throw new RuntimeException("Stub!"); }

public void setGlobalConfiguration(audio.policy.configuration.V5_0.GlobalConfiguration globalConfiguration) { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V5_0.Modules> getModules() { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V5_0.Volumes> getVolumes() { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V5_0.SurroundSound getSurroundSound() { throw new RuntimeException("Stub!"); }

public void setSurroundSound(audio.policy.configuration.V5_0.SurroundSound surroundSound) { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V5_0.Version getVersion() { throw new RuntimeException("Stub!"); }

public void setVersion(audio.policy.configuration.V5_0.Version version) { throw new RuntimeException("Stub!"); }
}

