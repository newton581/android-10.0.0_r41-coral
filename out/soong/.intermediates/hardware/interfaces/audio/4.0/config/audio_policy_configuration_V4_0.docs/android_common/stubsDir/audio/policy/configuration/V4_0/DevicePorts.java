
package audio.policy.configuration.V4_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DevicePorts {

public DevicePorts() { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V4_0.DevicePorts.DevicePort> getDevicePort() { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class DevicePort {

public DevicePort() { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V4_0.Profile> getProfile() { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V4_0.Gains getGains() { throw new RuntimeException("Stub!"); }

public void setGains(audio.policy.configuration.V4_0.Gains gains) { throw new RuntimeException("Stub!"); }

public java.lang.String getTagName() { throw new RuntimeException("Stub!"); }

public void setTagName(java.lang.String tagName) { throw new RuntimeException("Stub!"); }

public java.lang.String getType() { throw new RuntimeException("Stub!"); }

public void setType(java.lang.String type) { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V4_0.Role getRole() { throw new RuntimeException("Stub!"); }

public void setRole(audio.policy.configuration.V4_0.Role role) { throw new RuntimeException("Stub!"); }

public java.lang.String getAddress() { throw new RuntimeException("Stub!"); }

public void setAddress(java.lang.String address) { throw new RuntimeException("Stub!"); }

public boolean get_default() { throw new RuntimeException("Stub!"); }

public void set_default(boolean _default) { throw new RuntimeException("Stub!"); }
}

}

