
package audio.policy.configuration.V4_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Gains {

public Gains() { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V4_0.Gains.Gain> getGain() { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Gain {

public Gain() { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V4_0.GainMode getMode() { throw new RuntimeException("Stub!"); }

public void setMode(audio.policy.configuration.V4_0.GainMode mode) { throw new RuntimeException("Stub!"); }

public java.lang.String getChannel_mask() { throw new RuntimeException("Stub!"); }

public void setChannel_mask(java.lang.String channel_mask) { throw new RuntimeException("Stub!"); }

public int getMinValueMB() { throw new RuntimeException("Stub!"); }

public void setMinValueMB(int minValueMB) { throw new RuntimeException("Stub!"); }

public int getMaxValueMB() { throw new RuntimeException("Stub!"); }

public void setMaxValueMB(int maxValueMB) { throw new RuntimeException("Stub!"); }

public int getDefaultValueMB() { throw new RuntimeException("Stub!"); }

public void setDefaultValueMB(int defaultValueMB) { throw new RuntimeException("Stub!"); }

public int getStepValueMB() { throw new RuntimeException("Stub!"); }

public void setStepValueMB(int stepValueMB) { throw new RuntimeException("Stub!"); }

public int getMinRampMs() { throw new RuntimeException("Stub!"); }

public void setMinRampMs(int minRampMs) { throw new RuntimeException("Stub!"); }

public int getMaxRampMs() { throw new RuntimeException("Stub!"); }

public void setMaxRampMs(int maxRampMs) { throw new RuntimeException("Stub!"); }
}

}

