
package audio.policy.configuration.V4_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Modules {

public Modules() { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V4_0.Modules.Module> getModule() { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Module {

public Module() { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V4_0.AttachedDevices getAttachedDevices() { throw new RuntimeException("Stub!"); }

public void setAttachedDevices(audio.policy.configuration.V4_0.AttachedDevices attachedDevices) { throw new RuntimeException("Stub!"); }

public java.lang.String getDefaultOutputDevice() { throw new RuntimeException("Stub!"); }

public void setDefaultOutputDevice(java.lang.String defaultOutputDevice) { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V4_0.MixPorts getMixPorts() { throw new RuntimeException("Stub!"); }

public void setMixPorts(audio.policy.configuration.V4_0.MixPorts mixPorts) { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V4_0.DevicePorts getDevicePorts() { throw new RuntimeException("Stub!"); }

public void setDevicePorts(audio.policy.configuration.V4_0.DevicePorts devicePorts) { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V4_0.Routes getRoutes() { throw new RuntimeException("Stub!"); }

public void setRoutes(audio.policy.configuration.V4_0.Routes routes) { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V4_0.HalVersion getHalVersion() { throw new RuntimeException("Stub!"); }

public void setHalVersion(audio.policy.configuration.V4_0.HalVersion halVersion) { throw new RuntimeException("Stub!"); }
}

}

