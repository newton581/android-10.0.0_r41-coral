
package audio.policy.configuration.V4_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public enum GainMode {
AUDIO_GAIN_MODE_JOINT,
AUDIO_GAIN_MODE_CHANNELS,
AUDIO_GAIN_MODE_RAMP;

public java.lang.String getRawName() { throw new RuntimeException("Stub!"); }
}

