
package audio.policy.configuration.V4_0;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MixPorts {

public MixPorts() { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V4_0.MixPorts.MixPort> getMixPort() { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class MixPort {

public MixPort() { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V4_0.Profile> getProfile() { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V4_0.Gains getGains() { throw new RuntimeException("Stub!"); }

public void setGains(audio.policy.configuration.V4_0.Gains gains) { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public audio.policy.configuration.V4_0.Role getRole() { throw new RuntimeException("Stub!"); }

public void setRole(audio.policy.configuration.V4_0.Role role) { throw new RuntimeException("Stub!"); }

public java.lang.String getFlags() { throw new RuntimeException("Stub!"); }

public void setFlags(java.lang.String flags) { throw new RuntimeException("Stub!"); }

public long getMaxOpenCount() { throw new RuntimeException("Stub!"); }

public void setMaxOpenCount(long maxOpenCount) { throw new RuntimeException("Stub!"); }

public long getMaxActiveCount() { throw new RuntimeException("Stub!"); }

public void setMaxActiveCount(long maxActiveCount) { throw new RuntimeException("Stub!"); }

public java.util.List<audio.policy.configuration.V4_0.AudioUsage> getPreferredUsage() { throw new RuntimeException("Stub!"); }

public void setPreferredUsage(java.util.List<audio.policy.configuration.V4_0.AudioUsage> preferredUsage) { throw new RuntimeException("Stub!"); }
}

}

