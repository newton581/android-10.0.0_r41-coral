#define LOG_TAG "android.hardware.broadcastradio@2.0::types"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hardware/broadcastradio/2.0/types.h>
#include <android/hardware/broadcastradio/2.0/hwtypes.h>

namespace android {
namespace hardware {
namespace broadcastradio {
namespace V2_0 {

::android::status_t readEmbeddedFromParcel(
        const VendorKeyValue &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.key),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::VendorKeyValue, key));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.value),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::VendorKeyValue, value));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const VendorKeyValue &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.key,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::VendorKeyValue, key));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.value,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::VendorKeyValue, value));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const AmFmRegionConfig &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_ranges_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::broadcastradio::V2_0::AmFmBandRange> &>(obj.ranges),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::AmFmRegionConfig, ranges), &_hidl_ranges_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const AmFmRegionConfig &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_ranges_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.ranges,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::AmFmRegionConfig, ranges), &_hidl_ranges_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const DabTableEntry &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.label),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::DabTableEntry, label));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const DabTableEntry &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.label,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::DabTableEntry, label));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const Properties &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.maker),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, maker));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.product),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, product));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.version),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, version));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.serial),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, serial));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_supportedIdentifierTypes_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint32_t> &>(obj.supportedIdentifierTypes),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, supportedIdentifierTypes), &_hidl_supportedIdentifierTypes_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_vendorInfo_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::broadcastradio::V2_0::VendorKeyValue> &>(obj.vendorInfo),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, vendorInfo), &_hidl_vendorInfo_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.vendorInfo.size(); ++_hidl_index_0) {
        _hidl_err = readEmbeddedFromParcel(
                const_cast<::android::hardware::broadcastradio::V2_0::VendorKeyValue &>(obj.vendorInfo[_hidl_index_0]),
                parcel,
                _hidl_vendorInfo_child,
                _hidl_index_0 * sizeof(::android::hardware::broadcastradio::V2_0::VendorKeyValue));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const Properties &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.maker,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, maker));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.product,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, product));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.version,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, version));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.serial,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, serial));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_supportedIdentifierTypes_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.supportedIdentifierTypes,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, supportedIdentifierTypes), &_hidl_supportedIdentifierTypes_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_vendorInfo_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.vendorInfo,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Properties, vendorInfo), &_hidl_vendorInfo_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.vendorInfo.size(); ++_hidl_index_0) {
        _hidl_err = writeEmbeddedToParcel(
                obj.vendorInfo[_hidl_index_0],
                parcel,
                _hidl_vendorInfo_child,
                _hidl_index_0 * sizeof(::android::hardware::broadcastradio::V2_0::VendorKeyValue));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const ProgramSelector &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_secondaryIds_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::broadcastradio::V2_0::ProgramIdentifier> &>(obj.secondaryIds),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramSelector, secondaryIds), &_hidl_secondaryIds_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const ProgramSelector &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_secondaryIds_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.secondaryIds,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramSelector, secondaryIds), &_hidl_secondaryIds_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const ProgramInfo &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::broadcastradio::V2_0::ProgramSelector &>(obj.selector),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramInfo, selector));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_relatedContent_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::broadcastradio::V2_0::ProgramIdentifier> &>(obj.relatedContent),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramInfo, relatedContent), &_hidl_relatedContent_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_metadata_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::broadcastradio::V2_0::Metadata> &>(obj.metadata),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramInfo, metadata), &_hidl_metadata_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.metadata.size(); ++_hidl_index_0) {
        _hidl_err = readEmbeddedFromParcel(
                const_cast<::android::hardware::broadcastradio::V2_0::Metadata &>(obj.metadata[_hidl_index_0]),
                parcel,
                _hidl_metadata_child,
                _hidl_index_0 * sizeof(::android::hardware::broadcastradio::V2_0::Metadata));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    size_t _hidl_vendorInfo_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::broadcastradio::V2_0::VendorKeyValue> &>(obj.vendorInfo),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramInfo, vendorInfo), &_hidl_vendorInfo_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.vendorInfo.size(); ++_hidl_index_0) {
        _hidl_err = readEmbeddedFromParcel(
                const_cast<::android::hardware::broadcastradio::V2_0::VendorKeyValue &>(obj.vendorInfo[_hidl_index_0]),
                parcel,
                _hidl_vendorInfo_child,
                _hidl_index_0 * sizeof(::android::hardware::broadcastradio::V2_0::VendorKeyValue));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const ProgramInfo &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = writeEmbeddedToParcel(
            obj.selector,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramInfo, selector));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_relatedContent_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.relatedContent,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramInfo, relatedContent), &_hidl_relatedContent_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_metadata_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.metadata,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramInfo, metadata), &_hidl_metadata_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.metadata.size(); ++_hidl_index_0) {
        _hidl_err = writeEmbeddedToParcel(
                obj.metadata[_hidl_index_0],
                parcel,
                _hidl_metadata_child,
                _hidl_index_0 * sizeof(::android::hardware::broadcastradio::V2_0::Metadata));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    size_t _hidl_vendorInfo_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.vendorInfo,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramInfo, vendorInfo), &_hidl_vendorInfo_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.vendorInfo.size(); ++_hidl_index_0) {
        _hidl_err = writeEmbeddedToParcel(
                obj.vendorInfo[_hidl_index_0],
                parcel,
                _hidl_vendorInfo_child,
                _hidl_index_0 * sizeof(::android::hardware::broadcastradio::V2_0::VendorKeyValue));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const Metadata &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.stringValue),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Metadata, stringValue));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const Metadata &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.stringValue,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Metadata, stringValue));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const ProgramListChunk &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_modified_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::broadcastradio::V2_0::ProgramInfo> &>(obj.modified),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramListChunk, modified), &_hidl_modified_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.modified.size(); ++_hidl_index_0) {
        _hidl_err = readEmbeddedFromParcel(
                const_cast<::android::hardware::broadcastradio::V2_0::ProgramInfo &>(obj.modified[_hidl_index_0]),
                parcel,
                _hidl_modified_child,
                _hidl_index_0 * sizeof(::android::hardware::broadcastradio::V2_0::ProgramInfo));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    size_t _hidl_removed_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::broadcastradio::V2_0::ProgramIdentifier> &>(obj.removed),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramListChunk, removed), &_hidl_removed_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const ProgramListChunk &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_modified_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.modified,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramListChunk, modified), &_hidl_modified_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.modified.size(); ++_hidl_index_0) {
        _hidl_err = writeEmbeddedToParcel(
                obj.modified[_hidl_index_0],
                parcel,
                _hidl_modified_child,
                _hidl_index_0 * sizeof(::android::hardware::broadcastradio::V2_0::ProgramInfo));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    size_t _hidl_removed_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.removed,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramListChunk, removed), &_hidl_removed_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const ProgramFilter &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_identifierTypes_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint32_t> &>(obj.identifierTypes),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramFilter, identifierTypes), &_hidl_identifierTypes_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_identifiers_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::broadcastradio::V2_0::ProgramIdentifier> &>(obj.identifiers),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramFilter, identifiers), &_hidl_identifiers_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const ProgramFilter &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_identifierTypes_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.identifierTypes,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramFilter, identifierTypes), &_hidl_identifierTypes_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_identifiers_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.identifiers,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::ProgramFilter, identifiers), &_hidl_identifiers_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const Announcement &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::broadcastradio::V2_0::ProgramSelector &>(obj.selector),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Announcement, selector));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_vendorInfo_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::broadcastradio::V2_0::VendorKeyValue> &>(obj.vendorInfo),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Announcement, vendorInfo), &_hidl_vendorInfo_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.vendorInfo.size(); ++_hidl_index_0) {
        _hidl_err = readEmbeddedFromParcel(
                const_cast<::android::hardware::broadcastradio::V2_0::VendorKeyValue &>(obj.vendorInfo[_hidl_index_0]),
                parcel,
                _hidl_vendorInfo_child,
                _hidl_index_0 * sizeof(::android::hardware::broadcastradio::V2_0::VendorKeyValue));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const Announcement &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = writeEmbeddedToParcel(
            obj.selector,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Announcement, selector));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_vendorInfo_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.vendorInfo,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::broadcastradio::V2_0::Announcement, vendorInfo), &_hidl_vendorInfo_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.vendorInfo.size(); ++_hidl_index_0) {
        _hidl_err = writeEmbeddedToParcel(
                obj.vendorInfo[_hidl_index_0],
                parcel,
                _hidl_vendorInfo_child,
                _hidl_index_0 * sizeof(::android::hardware::broadcastradio::V2_0::VendorKeyValue));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    return _hidl_err;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V2_0
}  // namespace broadcastradio
}  // namespace hardware
}  // namespace android
