#define LOG_TAG "android.hardware.automotive.vehicle@2.0::types"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hardware/automotive/vehicle/2.0/types.h>
#include <android/hardware/automotive/vehicle/2.0/hwtypes.h>

namespace android {
namespace hardware {
namespace automotive {
namespace vehicle {
namespace V2_0 {

::android::status_t readEmbeddedFromParcel(
        const VehiclePropConfig &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_areaConfigs_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::automotive::vehicle::V2_0::VehicleAreaConfig> &>(obj.areaConfigs),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropConfig, areaConfigs), &_hidl_areaConfigs_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_configArray_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<int32_t> &>(obj.configArray),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropConfig, configArray), &_hidl_configArray_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.configString),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropConfig, configString));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const VehiclePropConfig &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_areaConfigs_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.areaConfigs,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropConfig, areaConfigs), &_hidl_areaConfigs_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_configArray_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.configArray,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropConfig, configArray), &_hidl_configArray_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.configString,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropConfig, configString));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const VehiclePropValue::RawValue &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_int32Values_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<int32_t> &>(obj.int32Values),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue, int32Values), &_hidl_int32Values_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_floatValues_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<float> &>(obj.floatValues),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue, floatValues), &_hidl_floatValues_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_int64Values_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<int64_t> &>(obj.int64Values),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue, int64Values), &_hidl_int64Values_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_bytes_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint8_t> &>(obj.bytes),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue, bytes), &_hidl_bytes_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.stringValue),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue, stringValue));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const VehiclePropValue::RawValue &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_int32Values_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.int32Values,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue, int32Values), &_hidl_int32Values_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_floatValues_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.floatValues,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue, floatValues), &_hidl_floatValues_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_int64Values_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.int64Values,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue, int64Values), &_hidl_int64Values_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_bytes_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.bytes,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue, bytes), &_hidl_bytes_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.stringValue,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue, stringValue));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const VehiclePropValue &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::automotive::vehicle::V2_0::VehiclePropValue::RawValue &>(obj.value),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue, value));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const VehiclePropValue &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = writeEmbeddedToParcel(
            obj.value,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::automotive::vehicle::V2_0::VehiclePropValue, value));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V2_0
}  // namespace vehicle
}  // namespace automotive
}  // namespace hardware
}  // namespace android
