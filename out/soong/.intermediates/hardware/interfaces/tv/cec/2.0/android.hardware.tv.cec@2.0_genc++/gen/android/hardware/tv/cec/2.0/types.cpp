#define LOG_TAG "android.hardware.tv.cec@2.0::types"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hardware/tv/cec/2.0/types.h>
#include <android/hardware/tv/cec/2.0/hwtypes.h>

namespace android {
namespace hardware {
namespace tv {
namespace cec {
namespace V2_0 {

::android::status_t readEmbeddedFromParcel(
        const CecMessage &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_body_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint8_t> &>(obj.body),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::tv::cec::V2_0::CecMessage, body), &_hidl_body_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const CecMessage &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_body_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.body,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::tv::cec::V2_0::CecMessage, body), &_hidl_body_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::hardware::tv::cec::V2_0::CecRcProfile1::CecRcProfile1() {
    static_assert(offsetof(::android::hardware::tv::cec::V2_0::CecRcProfile1, hidl_d) == 0, "wrong offset");
    static_assert(offsetof(::android::hardware::tv::cec::V2_0::CecRcProfile1, hidl_u) == 1, "wrong offset");

    ::std::memset(&hidl_u, 0, sizeof(hidl_u));
    // no padding to zero starting at offset 1
    // no padding to zero starting at offset 2

    hidl_d = hidl_discriminator::profileId;
    new (&hidl_u.profileId) ::android::hardware::tv::cec::V2_0::CecRcProfileId();
}

::android::hardware::tv::cec::V2_0::CecRcProfile1::~CecRcProfile1() {
    hidl_destructUnion();
}

::android::hardware::tv::cec::V2_0::CecRcProfile1::CecRcProfile1(CecRcProfile1&& other) : ::android::hardware::tv::cec::V2_0::CecRcProfile1() {
    switch (other.hidl_d) {
        case hidl_discriminator::profileId: {
            new (&hidl_u.profileId) ::android::hardware::tv::cec::V2_0::CecRcProfileId(std::move(other.hidl_u.profileId));
            break;
        }
        case hidl_discriminator::profileSource: {
            new (&hidl_u.profileSource) ::android::hardware::hidl_bitfield<::android::hardware::tv::cec::V2_0::CecRcProfileSource>(std::move(other.hidl_u.profileSource));
            break;
        }
        default: {
            ::android::hardware::details::logAlwaysFatal((
                    "Unknown union discriminator (value: " +
                    std::to_string((uint8_t) other.hidl_d) + ").").c_str());
        }
    }

    hidl_d = other.hidl_d;
}

::android::hardware::tv::cec::V2_0::CecRcProfile1::CecRcProfile1(const CecRcProfile1& other) : ::android::hardware::tv::cec::V2_0::CecRcProfile1() {
    switch (other.hidl_d) {
        case hidl_discriminator::profileId: {
            new (&hidl_u.profileId) ::android::hardware::tv::cec::V2_0::CecRcProfileId(other.hidl_u.profileId);
            break;
        }
        case hidl_discriminator::profileSource: {
            new (&hidl_u.profileSource) ::android::hardware::hidl_bitfield<::android::hardware::tv::cec::V2_0::CecRcProfileSource>(other.hidl_u.profileSource);
            break;
        }
        default: {
            ::android::hardware::details::logAlwaysFatal((
                    "Unknown union discriminator (value: " +
                    std::to_string((uint8_t) other.hidl_d) + ").").c_str());
        }
    }

    hidl_d = other.hidl_d;
}

::android::hardware::tv::cec::V2_0::CecRcProfile1& (::android::hardware::tv::cec::V2_0::CecRcProfile1::operator=)(CecRcProfile1&& other) {
    if (this == &other) { return *this; }

    switch (other.hidl_d) {
        case hidl_discriminator::profileId: {
            profileId(std::move(other.hidl_u.profileId));
            break;
        }
        case hidl_discriminator::profileSource: {
            profileSource(std::move(other.hidl_u.profileSource));
            break;
        }
        default: {
            ::android::hardware::details::logAlwaysFatal((
                    "Unknown union discriminator (value: " +
                    std::to_string((uint8_t) other.hidl_d) + ").").c_str());
        }
    }
    return *this;
}

::android::hardware::tv::cec::V2_0::CecRcProfile1& (::android::hardware::tv::cec::V2_0::CecRcProfile1::operator=)(const CecRcProfile1& other) {
    if (this == &other) { return *this; }

    switch (other.hidl_d) {
        case hidl_discriminator::profileId: {
            profileId(other.hidl_u.profileId);
            break;
        }
        case hidl_discriminator::profileSource: {
            profileSource(other.hidl_u.profileSource);
            break;
        }
        default: {
            ::android::hardware::details::logAlwaysFatal((
                    "Unknown union discriminator (value: " +
                    std::to_string((uint8_t) other.hidl_d) + ").").c_str());
        }
    }
    return *this;
}

void ::android::hardware::tv::cec::V2_0::CecRcProfile1::hidl_destructUnion() {
    switch (hidl_d) {
        case hidl_discriminator::profileId: {
            ::android::hardware::details::destructElement(&(hidl_u.profileId));
            break;
        }
        case hidl_discriminator::profileSource: {
            ::android::hardware::details::destructElement(&(hidl_u.profileSource));
            break;
        }
        default: {
            ::android::hardware::details::logAlwaysFatal((
                    "Unknown union discriminator (value: " +
                    std::to_string((uint8_t) hidl_d) + ").").c_str());
        }
    }

}

void ::android::hardware::tv::cec::V2_0::CecRcProfile1::profileId(::android::hardware::tv::cec::V2_0::CecRcProfileId o) {
    if (hidl_d != hidl_discriminator::profileId) {
        hidl_destructUnion();
        ::std::memset(&hidl_u, 0, sizeof(hidl_u));

        new (&hidl_u.profileId) ::android::hardware::tv::cec::V2_0::CecRcProfileId(o);
        hidl_d = hidl_discriminator::profileId;
    }
    else if (&(hidl_u.profileId) != &o) {
        hidl_u.profileId = o;
    }
}

::android::hardware::tv::cec::V2_0::CecRcProfileId& (::android::hardware::tv::cec::V2_0::CecRcProfile1::profileId)() {
    if (CC_UNLIKELY(hidl_d != hidl_discriminator::profileId)) {
        LOG_ALWAYS_FATAL("Bad safe_union access: safe_union has discriminator %" PRIu64 " but discriminator %" PRIu64 " was accessed.",
                static_cast<uint64_t>(hidl_d), static_cast<uint64_t>(hidl_discriminator::profileId));
    }

    return hidl_u.profileId;
}

::android::hardware::tv::cec::V2_0::CecRcProfileId (::android::hardware::tv::cec::V2_0::CecRcProfile1::profileId)() const {
    if (CC_UNLIKELY(hidl_d != hidl_discriminator::profileId)) {
        LOG_ALWAYS_FATAL("Bad safe_union access: safe_union has discriminator %" PRIu64 " but discriminator %" PRIu64 " was accessed.",
                static_cast<uint64_t>(hidl_d), static_cast<uint64_t>(hidl_discriminator::profileId));
    }

    return hidl_u.profileId;
}

void ::android::hardware::tv::cec::V2_0::CecRcProfile1::profileSource(::android::hardware::hidl_bitfield<::android::hardware::tv::cec::V2_0::CecRcProfileSource> o) {
    if (hidl_d != hidl_discriminator::profileSource) {
        hidl_destructUnion();
        ::std::memset(&hidl_u, 0, sizeof(hidl_u));

        new (&hidl_u.profileSource) ::android::hardware::hidl_bitfield<::android::hardware::tv::cec::V2_0::CecRcProfileSource>(o);
        hidl_d = hidl_discriminator::profileSource;
    }
    else if (&(hidl_u.profileSource) != &o) {
        hidl_u.profileSource = o;
    }
}

::android::hardware::hidl_bitfield<::android::hardware::tv::cec::V2_0::CecRcProfileSource>& (::android::hardware::tv::cec::V2_0::CecRcProfile1::profileSource)() {
    if (CC_UNLIKELY(hidl_d != hidl_discriminator::profileSource)) {
        LOG_ALWAYS_FATAL("Bad safe_union access: safe_union has discriminator %" PRIu64 " but discriminator %" PRIu64 " was accessed.",
                static_cast<uint64_t>(hidl_d), static_cast<uint64_t>(hidl_discriminator::profileSource));
    }

    return hidl_u.profileSource;
}

::android::hardware::hidl_bitfield<::android::hardware::tv::cec::V2_0::CecRcProfileSource> (::android::hardware::tv::cec::V2_0::CecRcProfile1::profileSource)() const {
    if (CC_UNLIKELY(hidl_d != hidl_discriminator::profileSource)) {
        LOG_ALWAYS_FATAL("Bad safe_union access: safe_union has discriminator %" PRIu64 " but discriminator %" PRIu64 " was accessed.",
                static_cast<uint64_t>(hidl_d), static_cast<uint64_t>(hidl_discriminator::profileSource));
    }

    return hidl_u.profileSource;
}

::android::hardware::tv::cec::V2_0::CecRcProfile1::hidl_union::hidl_union() {}

::android::hardware::tv::cec::V2_0::CecRcProfile1::hidl_union::~hidl_union() {}

::android::hardware::tv::cec::V2_0::CecRcProfile1::hidl_discriminator (::android::hardware::tv::cec::V2_0::CecRcProfile1::getDiscriminator)() const {
    return hidl_d;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V2_0
}  // namespace cec
}  // namespace tv
}  // namespace hardware
}  // namespace android
