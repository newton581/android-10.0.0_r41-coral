package android.hardware.contexthub.V1_0;


public final class MemRange {
    public int totalBytes;
    public int freeBytes;
    public int type;
    public int flags;

    @Override
    public final boolean equals(Object otherObject) {
        if (this == otherObject) {
            return true;
        }
        if (otherObject == null) {
            return false;
        }
        if (otherObject.getClass() != android.hardware.contexthub.V1_0.MemRange.class) {
            return false;
        }
        android.hardware.contexthub.V1_0.MemRange other = (android.hardware.contexthub.V1_0.MemRange)otherObject;
        if (this.totalBytes != other.totalBytes) {
            return false;
        }
        if (this.freeBytes != other.freeBytes) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        if (!android.os.HidlSupport.deepEquals(this.flags, other.flags)) {
            return false;
        }
        return true;
    }

    @Override
    public final int hashCode() {
        return java.util.Objects.hash(
                android.os.HidlSupport.deepHashCode(this.totalBytes), 
                android.os.HidlSupport.deepHashCode(this.freeBytes), 
                android.os.HidlSupport.deepHashCode(this.type), 
                android.os.HidlSupport.deepHashCode(this.flags));
    }

    @Override
    public final String toString() {
        java.lang.StringBuilder builder = new java.lang.StringBuilder();
        builder.append("{");
        builder.append(".totalBytes = ");
        builder.append(this.totalBytes);
        builder.append(", .freeBytes = ");
        builder.append(this.freeBytes);
        builder.append(", .type = ");
        builder.append(android.hardware.contexthub.V1_0.HubMemoryType.toString(this.type));
        builder.append(", .flags = ");
        builder.append(android.hardware.contexthub.V1_0.HubMemoryFlag.dumpBitfield(this.flags));
        builder.append("}");
        return builder.toString();
    }

    public final void readFromParcel(android.os.HwParcel parcel) {
        android.os.HwBlob blob = parcel.readBuffer(16 /* size */);
        readEmbeddedFromParcel(parcel, blob, 0 /* parentOffset */);
    }

    public static final java.util.ArrayList<MemRange> readVectorFromParcel(android.os.HwParcel parcel) {
        java.util.ArrayList<MemRange> _hidl_vec = new java.util.ArrayList();
        android.os.HwBlob _hidl_blob = parcel.readBuffer(16 /* sizeof hidl_vec<T> */);

        {
            int _hidl_vec_size = _hidl_blob.getInt32(0 + 8 /* offsetof(hidl_vec<T>, mSize) */);
            android.os.HwBlob childBlob = parcel.readEmbeddedBuffer(
                    _hidl_vec_size * 16,_hidl_blob.handle(),
                    0 + 0 /* offsetof(hidl_vec<T>, mBuffer) */,true /* nullable */);

            _hidl_vec.clear();
            for (int _hidl_index_0 = 0; _hidl_index_0 < _hidl_vec_size; ++_hidl_index_0) {
                android.hardware.contexthub.V1_0.MemRange _hidl_vec_element = new android.hardware.contexthub.V1_0.MemRange();
                ((android.hardware.contexthub.V1_0.MemRange) _hidl_vec_element).readEmbeddedFromParcel(parcel, childBlob, _hidl_index_0 * 16);
                _hidl_vec.add(_hidl_vec_element);
            }
        }

        return _hidl_vec;
    }

    public final void readEmbeddedFromParcel(
            android.os.HwParcel parcel, android.os.HwBlob _hidl_blob, long _hidl_offset) {
        totalBytes = _hidl_blob.getInt32(_hidl_offset + 0);
        freeBytes = _hidl_blob.getInt32(_hidl_offset + 4);
        type = _hidl_blob.getInt32(_hidl_offset + 8);
        flags = _hidl_blob.getInt32(_hidl_offset + 12);
    }

    public final void writeToParcel(android.os.HwParcel parcel) {
        android.os.HwBlob _hidl_blob = new android.os.HwBlob(16 /* size */);
        writeEmbeddedToBlob(_hidl_blob, 0 /* parentOffset */);
        parcel.writeBuffer(_hidl_blob);
    }

    public static final void writeVectorToParcel(
            android.os.HwParcel parcel, java.util.ArrayList<MemRange> _hidl_vec) {
        android.os.HwBlob _hidl_blob = new android.os.HwBlob(16 /* sizeof(hidl_vec<T>) */);
        {
            int _hidl_vec_size = _hidl_vec.size();
            _hidl_blob.putInt32(0 + 8 /* offsetof(hidl_vec<T>, mSize) */, _hidl_vec_size);
            _hidl_blob.putBool(0 + 12 /* offsetof(hidl_vec<T>, mOwnsBuffer) */, false);
            android.os.HwBlob childBlob = new android.os.HwBlob((int)(_hidl_vec_size * 16));
            for (int _hidl_index_0 = 0; _hidl_index_0 < _hidl_vec_size; ++_hidl_index_0) {
                _hidl_vec.get(_hidl_index_0).writeEmbeddedToBlob(childBlob, _hidl_index_0 * 16);
            }
            _hidl_blob.putBlob(0 + 0 /* offsetof(hidl_vec<T>, mBuffer) */, childBlob);
        }

        parcel.writeBuffer(_hidl_blob);
    }

    public final void writeEmbeddedToBlob(
            android.os.HwBlob _hidl_blob, long _hidl_offset) {
        _hidl_blob.putInt32(_hidl_offset + 0, totalBytes);
        _hidl_blob.putInt32(_hidl_offset + 4, freeBytes);
        _hidl_blob.putInt32(_hidl_offset + 8, type);
        _hidl_blob.putInt32(_hidl_offset + 12, flags);
    }
};

