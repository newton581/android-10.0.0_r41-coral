#define LOG_TAG "android.hardware.wifi.offload@1.0::types"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hardware/wifi/offload/1.0/types.h>
#include <android/hardware/wifi/offload/1.0/hwtypes.h>

namespace android {
namespace hardware {
namespace wifi {
namespace offload {
namespace V1_0 {

::android::status_t readEmbeddedFromParcel(
        const NetworkInfo &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_ssid_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint8_t> &>(obj.ssid),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::NetworkInfo, ssid), &_hidl_ssid_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const NetworkInfo &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_ssid_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.ssid,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::NetworkInfo, ssid), &_hidl_ssid_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const ScanResult &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::offload::V1_0::NetworkInfo &>(obj.networkInfo),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanResult, networkInfo));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const ScanResult &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = writeEmbeddedToParcel(
            obj.networkInfo,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanResult, networkInfo));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const ScanParam &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_ssidList_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::hidl_vec<uint8_t>> &>(obj.ssidList),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanParam, ssidList), &_hidl_ssidList_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.ssidList.size(); ++_hidl_index_0) {
        size_t _hidl_ssidList_indexed_child;

        _hidl_err = ::android::hardware::readEmbeddedFromParcel(
                const_cast<::android::hardware::hidl_vec<uint8_t> &>(obj.ssidList[_hidl_index_0]),
                parcel,
                _hidl_ssidList_child,
                _hidl_index_0 * sizeof(::android::hardware::hidl_vec<uint8_t>), &_hidl_ssidList_indexed_child);

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    size_t _hidl_frequencyList_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint32_t> &>(obj.frequencyList),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanParam, frequencyList), &_hidl_frequencyList_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const ScanParam &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_ssidList_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.ssidList,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanParam, ssidList), &_hidl_ssidList_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.ssidList.size(); ++_hidl_index_0) {
        size_t _hidl_ssidList_indexed_child;

        _hidl_err = ::android::hardware::writeEmbeddedToParcel(
                obj.ssidList[_hidl_index_0],
                parcel,
                _hidl_ssidList_child,
                _hidl_index_0 * sizeof(::android::hardware::hidl_vec<uint8_t>), &_hidl_ssidList_indexed_child);

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    size_t _hidl_frequencyList_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.frequencyList,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanParam, frequencyList), &_hidl_frequencyList_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const ScanFilter &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_preferredNetworkInfoList_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::wifi::offload::V1_0::NetworkInfo> &>(obj.preferredNetworkInfoList),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanFilter, preferredNetworkInfoList), &_hidl_preferredNetworkInfoList_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.preferredNetworkInfoList.size(); ++_hidl_index_0) {
        _hidl_err = readEmbeddedFromParcel(
                const_cast<::android::hardware::wifi::offload::V1_0::NetworkInfo &>(obj.preferredNetworkInfoList[_hidl_index_0]),
                parcel,
                _hidl_preferredNetworkInfoList_child,
                _hidl_index_0 * sizeof(::android::hardware::wifi::offload::V1_0::NetworkInfo));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const ScanFilter &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_preferredNetworkInfoList_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.preferredNetworkInfoList,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanFilter, preferredNetworkInfoList), &_hidl_preferredNetworkInfoList_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    for (size_t _hidl_index_0 = 0; _hidl_index_0 < obj.preferredNetworkInfoList.size(); ++_hidl_index_0) {
        _hidl_err = writeEmbeddedToParcel(
                obj.preferredNetworkInfoList[_hidl_index_0],
                parcel,
                _hidl_preferredNetworkInfoList_child,
                _hidl_index_0 * sizeof(::android::hardware::wifi::offload::V1_0::NetworkInfo));

        if (_hidl_err != ::android::OK) { return _hidl_err; }

    }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const ScanStats &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_scanRecord_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::wifi::offload::V1_0::ScanRecord> &>(obj.scanRecord),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanStats, scanRecord), &_hidl_scanRecord_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_logRecord_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<::android::hardware::wifi::offload::V1_0::LogRecord> &>(obj.logRecord),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanStats, logRecord), &_hidl_logRecord_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const ScanStats &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    size_t _hidl_scanRecord_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.scanRecord,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanStats, scanRecord), &_hidl_scanRecord_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_logRecord_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.logRecord,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::ScanStats, logRecord), &_hidl_logRecord_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const OffloadStatus &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.description),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::OffloadStatus, description));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const OffloadStatus &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.description,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::wifi::offload::V1_0::OffloadStatus, description));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_0
}  // namespace offload
}  // namespace wifi
}  // namespace hardware
}  // namespace android
