#define LOG_TAG "android.hardware.wifi@1.3::WifiStaIface"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/wifi/1.3/BpHwWifiStaIface.h>
#include <android/hardware/wifi/1.3/BnHwWifiStaIface.h>
#include <android/hardware/wifi/1.3/BsWifiStaIface.h>
#include <android/hardware/wifi/1.2/BpHwWifiStaIface.h>
#include <android/hardware/wifi/1.0/BpHwWifiStaIface.h>
#include <android/hardware/wifi/1.0/BpHwWifiIface.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace wifi {
namespace V1_3 {

const char* IWifiStaIface::descriptor("android.hardware.wifi@1.3::IWifiStaIface");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(IWifiStaIface::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwWifiStaIface(static_cast<IWifiStaIface *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(IWifiStaIface::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsWifiStaIface(static_cast<IWifiStaIface *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(IWifiStaIface::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(IWifiStaIface::descriptor);
};

// Methods from ::android::hardware::wifi::V1_0::IWifiIface follow.
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getType(getType_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getName(getName_cb _hidl_cb)

// Methods from ::android::hardware::wifi::V1_0::IWifiStaIface follow.
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::registerEventCallback(const ::android::sp<::android::hardware::wifi::V1_0::IWifiStaIfaceEventCallback>& callback, registerEventCallback_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getCapabilities(getCapabilities_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getApfPacketFilterCapabilities(getApfPacketFilterCapabilities_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::installApfPacketFilter(uint32_t cmdId, const ::android::hardware::hidl_vec<uint8_t>& program, installApfPacketFilter_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getBackgroundScanCapabilities(getBackgroundScanCapabilities_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getValidFrequenciesForBand(::android::hardware::wifi::V1_0::WifiBand band, getValidFrequenciesForBand_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::startBackgroundScan(uint32_t cmdId, const ::android::hardware::wifi::V1_0::StaBackgroundScanParameters& params, startBackgroundScan_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::stopBackgroundScan(uint32_t cmdId, stopBackgroundScan_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::enableLinkLayerStatsCollection(bool debug, enableLinkLayerStatsCollection_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::disableLinkLayerStatsCollection(disableLinkLayerStatsCollection_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getLinkLayerStats(getLinkLayerStats_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::startRssiMonitoring(uint32_t cmdId, int32_t maxRssi, int32_t minRssi, startRssiMonitoring_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::stopRssiMonitoring(uint32_t cmdId, stopRssiMonitoring_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getRoamingCapabilities(getRoamingCapabilities_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::configureRoaming(const ::android::hardware::wifi::V1_0::StaRoamingConfig& config, configureRoaming_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::setRoamingState(::android::hardware::wifi::V1_0::StaRoamingState state, setRoamingState_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::enableNdOffload(bool enable, enableNdOffload_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::startSendingKeepAlivePackets(uint32_t cmdId, const ::android::hardware::hidl_vec<uint8_t>& ipPacketData, uint16_t etherType, const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& dstAddress, uint32_t periodInMs, startSendingKeepAlivePackets_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::stopSendingKeepAlivePackets(uint32_t cmdId, stopSendingKeepAlivePackets_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::setScanningMacOui(const ::android::hardware::hidl_array<uint8_t, 3>& oui, setScanningMacOui_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::startDebugPacketFateMonitoring(startDebugPacketFateMonitoring_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getDebugTxPacketFates(getDebugTxPacketFates_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getDebugRxPacketFates(getDebugRxPacketFates_cb _hidl_cb)

// Methods from ::android::hardware::wifi::V1_2::IWifiStaIface follow.
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::readApfPacketFilterData(readApfPacketFilterData_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::setMacAddress(const ::android::hardware::hidl_array<uint8_t, 6>& mac, setMacAddress_cb _hidl_cb)

// Methods from ::android::hardware::wifi::V1_3::IWifiStaIface follow.
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getLinkLayerStats_1_3(getLinkLayerStats_1_3_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> IWifiStaIface::getFactoryMacAddress(getFactoryMacAddress_cb _hidl_cb)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> IWifiStaIface::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::wifi::V1_3::IWifiStaIface::descriptor,
        ::android::hardware::wifi::V1_2::IWifiStaIface::descriptor,
        ::android::hardware::wifi::V1_0::IWifiStaIface::descriptor,
        ::android::hardware::wifi::V1_0::IWifiIface::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> IWifiStaIface::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifiStaIface::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::wifi::V1_3::IWifiStaIface::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifiStaIface::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){59,239,48,232,182,26,176,80,192,246,253,38,87,39,18,190,94,187,119,7,214,36,201,170,108,116,187,185,214,165,180,169} /* 3bef30e8b61ab050c0f6fd26572712be5ebb7707d624c9aa6c74bbb9d6a5b4a9 */,
        (uint8_t[32]){245,104,45,191,25,247,18,190,249,204,63,170,95,227,220,103,11,111,251,203,98,161,71,161,216,107,157,67,87,76,216,63} /* f5682dbf19f712bef9cc3faa5fe3dc670b6ffbcb62a147a1d86b9d43574cd83f */,
        (uint8_t[32]){59,128,147,211,158,241,225,14,67,197,83,138,251,245,255,110,57,184,216,22,142,187,225,153,141,153,62,137,226,95,20,165} /* 3b8093d39ef1e10e43c5538afbf5ff6e39b8d8168ebbe1998d993e89e25f14a5 */,
        (uint8_t[32]){107,154,212,58,94,251,230,202,33,79,117,30,34,206,67,207,92,212,213,213,242,203,168,15,36,204,211,117,90,114,64,28} /* 6b9ad43a5efbe6ca214f751e22ce43cf5cd4d5d5f2cba80f24ccd3755a72401c */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifiStaIface::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IWifiStaIface::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> IWifiStaIface::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifiStaIface::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> IWifiStaIface::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> IWifiStaIface::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_3::IWifiStaIface>> IWifiStaIface::castFrom(const ::android::sp<::android::hardware::wifi::V1_3::IWifiStaIface>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_3::IWifiStaIface>> IWifiStaIface::castFrom(const ::android::sp<::android::hardware::wifi::V1_2::IWifiStaIface>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifiStaIface, ::android::hardware::wifi::V1_2::IWifiStaIface, BpHwWifiStaIface>(
            parent, "android.hardware.wifi@1.3::IWifiStaIface", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_3::IWifiStaIface>> IWifiStaIface::castFrom(const ::android::sp<::android::hardware::wifi::V1_0::IWifiStaIface>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifiStaIface, ::android::hardware::wifi::V1_0::IWifiStaIface, BpHwWifiStaIface>(
            parent, "android.hardware.wifi@1.3::IWifiStaIface", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_3::IWifiStaIface>> IWifiStaIface::castFrom(const ::android::sp<::android::hardware::wifi::V1_0::IWifiIface>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifiStaIface, ::android::hardware::wifi::V1_0::IWifiIface, BpHwWifiStaIface>(
            parent, "android.hardware.wifi@1.3::IWifiStaIface", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::V1_3::IWifiStaIface>> IWifiStaIface::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<IWifiStaIface, ::android::hidl::base::V1_0::IBase, BpHwWifiStaIface>(
            parent, "android.hardware.wifi@1.3::IWifiStaIface", emitError);
}

BpHwWifiStaIface::BpHwWifiStaIface(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<IWifiStaIface>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.wifi@1.3", "IWifiStaIface") {
}

// Methods from ::android::hardware::wifi::V1_3::IWifiStaIface follow.
::android::hardware::Return<void> BpHwWifiStaIface::_hidl_getLinkLayerStats_1_3(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, getLinkLayerStats_1_3_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IWifiStaIface::getLinkLayerStats_1_3::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi", "1.3", "IWifiStaIface", "getLinkLayerStats_1_3", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    ::android::hardware::wifi::V1_0::WifiStatus* _hidl_out_status;
    ::android::hardware::wifi::V1_3::StaLinkLayerStats* _hidl_out_stats;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwWifiStaIface::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(28 /* getLinkLayerStats_1_3 */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    size_t _hidl__hidl_out_status_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_status), &_hidl__hidl_out_status_parent,  const_cast<const void**>(reinterpret_cast<void **>(&_hidl_out_status)));
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::V1_0::WifiStatus &>(*_hidl_out_status),
            _hidl_reply,
            _hidl__hidl_out_status_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl__hidl_out_stats_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_stats), &_hidl__hidl_out_stats_parent,  const_cast<const void**>(reinterpret_cast<void **>(&_hidl_out_stats)));
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::V1_3::StaLinkLayerStats &>(*_hidl_out_stats),
            _hidl_reply,
            _hidl__hidl_out_stats_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(*_hidl_out_status, *_hidl_out_stats);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)_hidl_out_status);
        _hidl_args.push_back((void *)_hidl_out_stats);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi", "1.3", "IWifiStaIface", "getLinkLayerStats_1_3", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwWifiStaIface::_hidl_getFactoryMacAddress(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, getFactoryMacAddress_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::IWifiStaIface::getFactoryMacAddress::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi", "1.3", "IWifiStaIface", "getFactoryMacAddress", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    ::android::hardware::wifi::V1_0::WifiStatus* _hidl_out_status;
    const ::android::hardware::hidl_array<uint8_t, 6>* _hidl_out_mac;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwWifiStaIface::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(29 /* getFactoryMacAddress */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    size_t _hidl__hidl_out_status_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_status), &_hidl__hidl_out_status_parent,  const_cast<const void**>(reinterpret_cast<void **>(&_hidl_out_status)));
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::V1_0::WifiStatus &>(*_hidl_out_status),
            _hidl_reply,
            _hidl__hidl_out_status_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl__hidl_out_mac_parent;

    _hidl_err = _hidl_reply.readBuffer(6 * sizeof(uint8_t), &_hidl__hidl_out_mac_parent,  reinterpret_cast<const void **>(&_hidl_out_mac));

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(*_hidl_out_status, *_hidl_out_mac);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)_hidl_out_status);
        _hidl_args.push_back((void *)_hidl_out_mac);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi", "1.3", "IWifiStaIface", "getFactoryMacAddress", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}


// Methods from ::android::hardware::wifi::V1_0::IWifiIface follow.
::android::hardware::Return<void> BpHwWifiStaIface::getType(getType_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiIface::_hidl_getType(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getName(getName_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiIface::_hidl_getName(this, this, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::V1_0::IWifiStaIface follow.
::android::hardware::Return<void> BpHwWifiStaIface::registerEventCallback(const ::android::sp<::android::hardware::wifi::V1_0::IWifiStaIfaceEventCallback>& callback, registerEventCallback_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_registerEventCallback(this, this, callback, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getCapabilities(getCapabilities_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_getCapabilities(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getApfPacketFilterCapabilities(getApfPacketFilterCapabilities_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_getApfPacketFilterCapabilities(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::installApfPacketFilter(uint32_t cmdId, const ::android::hardware::hidl_vec<uint8_t>& program, installApfPacketFilter_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_installApfPacketFilter(this, this, cmdId, program, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getBackgroundScanCapabilities(getBackgroundScanCapabilities_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_getBackgroundScanCapabilities(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getValidFrequenciesForBand(::android::hardware::wifi::V1_0::WifiBand band, getValidFrequenciesForBand_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_getValidFrequenciesForBand(this, this, band, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::startBackgroundScan(uint32_t cmdId, const ::android::hardware::wifi::V1_0::StaBackgroundScanParameters& params, startBackgroundScan_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_startBackgroundScan(this, this, cmdId, params, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::stopBackgroundScan(uint32_t cmdId, stopBackgroundScan_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_stopBackgroundScan(this, this, cmdId, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::enableLinkLayerStatsCollection(bool debug, enableLinkLayerStatsCollection_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_enableLinkLayerStatsCollection(this, this, debug, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::disableLinkLayerStatsCollection(disableLinkLayerStatsCollection_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_disableLinkLayerStatsCollection(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getLinkLayerStats(getLinkLayerStats_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_getLinkLayerStats(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::startRssiMonitoring(uint32_t cmdId, int32_t maxRssi, int32_t minRssi, startRssiMonitoring_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_startRssiMonitoring(this, this, cmdId, maxRssi, minRssi, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::stopRssiMonitoring(uint32_t cmdId, stopRssiMonitoring_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_stopRssiMonitoring(this, this, cmdId, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getRoamingCapabilities(getRoamingCapabilities_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_getRoamingCapabilities(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::configureRoaming(const ::android::hardware::wifi::V1_0::StaRoamingConfig& config, configureRoaming_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_configureRoaming(this, this, config, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::setRoamingState(::android::hardware::wifi::V1_0::StaRoamingState state, setRoamingState_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_setRoamingState(this, this, state, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::enableNdOffload(bool enable, enableNdOffload_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_enableNdOffload(this, this, enable, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::startSendingKeepAlivePackets(uint32_t cmdId, const ::android::hardware::hidl_vec<uint8_t>& ipPacketData, uint16_t etherType, const ::android::hardware::hidl_array<uint8_t, 6>& srcAddress, const ::android::hardware::hidl_array<uint8_t, 6>& dstAddress, uint32_t periodInMs, startSendingKeepAlivePackets_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_startSendingKeepAlivePackets(this, this, cmdId, ipPacketData, etherType, srcAddress, dstAddress, periodInMs, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::stopSendingKeepAlivePackets(uint32_t cmdId, stopSendingKeepAlivePackets_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_stopSendingKeepAlivePackets(this, this, cmdId, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::setScanningMacOui(const ::android::hardware::hidl_array<uint8_t, 3>& oui, setScanningMacOui_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_setScanningMacOui(this, this, oui, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::startDebugPacketFateMonitoring(startDebugPacketFateMonitoring_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_startDebugPacketFateMonitoring(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getDebugTxPacketFates(getDebugTxPacketFates_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_getDebugTxPacketFates(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getDebugRxPacketFates(getDebugRxPacketFates_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_0::BpHwWifiStaIface::_hidl_getDebugRxPacketFates(this, this, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::V1_2::IWifiStaIface follow.
::android::hardware::Return<void> BpHwWifiStaIface::readApfPacketFilterData(readApfPacketFilterData_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_2::BpHwWifiStaIface::_hidl_readApfPacketFilterData(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::setMacAddress(const ::android::hardware::hidl_array<uint8_t, 6>& mac, setMacAddress_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_2::BpHwWifiStaIface::_hidl_setMacAddress(this, this, mac, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::V1_3::IWifiStaIface follow.
::android::hardware::Return<void> BpHwWifiStaIface::getLinkLayerStats_1_3(getLinkLayerStats_1_3_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_3::BpHwWifiStaIface::_hidl_getLinkLayerStats_1_3(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getFactoryMacAddress(getFactoryMacAddress_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::V1_3::BpHwWifiStaIface::_hidl_getFactoryMacAddress(this, this, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwWifiStaIface::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwWifiStaIface::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwWifiStaIface::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwWifiStaIface::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwWifiStaIface::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwWifiStaIface::BnHwWifiStaIface(const ::android::sp<IWifiStaIface> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.wifi@1.3", "IWifiStaIface") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwWifiStaIface::~BnHwWifiStaIface() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::wifi::V1_3::IWifiStaIface follow.
::android::status_t BnHwWifiStaIface::_hidl_getLinkLayerStats_1_3(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwWifiStaIface::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IWifiStaIface::getLinkLayerStats_1_3::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi", "1.3", "IWifiStaIface", "getLinkLayerStats_1_3", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IWifiStaIface*>(_hidl_this->getImpl().get())->getLinkLayerStats_1_3([&](const auto &_hidl_out_status, const auto &_hidl_out_stats) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("getLinkLayerStats_1_3: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        size_t _hidl__hidl_out_status_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_status, sizeof(_hidl_out_status), &_hidl__hidl_out_status_parent);
        /* _hidl_err ignored! */

        _hidl_err = writeEmbeddedToParcel(
                _hidl_out_status,
                _hidl_reply,
                _hidl__hidl_out_status_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        size_t _hidl__hidl_out_stats_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_stats, sizeof(_hidl_out_stats), &_hidl__hidl_out_stats_parent);
        /* _hidl_err ignored! */

        _hidl_err = writeEmbeddedToParcel(
                _hidl_out_stats,
                _hidl_reply,
                _hidl__hidl_out_stats_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_status);
            _hidl_args.push_back((void *)&_hidl_out_stats);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi", "1.3", "IWifiStaIface", "getLinkLayerStats_1_3", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("getLinkLayerStats_1_3: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwWifiStaIface::_hidl_getFactoryMacAddress(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwWifiStaIface::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::IWifiStaIface::getFactoryMacAddress::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi", "1.3", "IWifiStaIface", "getFactoryMacAddress", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<IWifiStaIface*>(_hidl_this->getImpl().get())->getFactoryMacAddress([&](const auto &_hidl_out_status, const auto &_hidl_out_mac) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("getFactoryMacAddress: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        size_t _hidl__hidl_out_status_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_status, sizeof(_hidl_out_status), &_hidl__hidl_out_status_parent);
        /* _hidl_err ignored! */

        _hidl_err = writeEmbeddedToParcel(
                _hidl_out_status,
                _hidl_reply,
                _hidl__hidl_out_status_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        size_t _hidl__hidl_out_mac_parent;

        _hidl_err = _hidl_reply->writeBuffer(_hidl_out_mac.data(), 6 * sizeof(uint8_t), &_hidl__hidl_out_mac_parent);
        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_status);
            _hidl_args.push_back((void *)&_hidl_out_mac);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi", "1.3", "IWifiStaIface", "getFactoryMacAddress", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("getFactoryMacAddress: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}


// Methods from ::android::hardware::wifi::V1_0::IWifiIface follow.

// Methods from ::android::hardware::wifi::V1_0::IWifiStaIface follow.

// Methods from ::android::hardware::wifi::V1_2::IWifiStaIface follow.

// Methods from ::android::hardware::wifi::V1_3::IWifiStaIface follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwWifiStaIface::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwWifiStaIface::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwWifiStaIface::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* getType */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiIface::_hidl_getType(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* getName */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiIface::_hidl_getName(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* registerEventCallback */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_registerEventCallback(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* getCapabilities */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_getCapabilities(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* getApfPacketFilterCapabilities */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_getApfPacketFilterCapabilities(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* installApfPacketFilter */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_installApfPacketFilter(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 7 /* getBackgroundScanCapabilities */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_getBackgroundScanCapabilities(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 8 /* getValidFrequenciesForBand */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_getValidFrequenciesForBand(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 9 /* startBackgroundScan */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_startBackgroundScan(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 10 /* stopBackgroundScan */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_stopBackgroundScan(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 11 /* enableLinkLayerStatsCollection */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_enableLinkLayerStatsCollection(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 12 /* disableLinkLayerStatsCollection */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_disableLinkLayerStatsCollection(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 13 /* getLinkLayerStats */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_getLinkLayerStats(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 14 /* startRssiMonitoring */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_startRssiMonitoring(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 15 /* stopRssiMonitoring */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_stopRssiMonitoring(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 16 /* getRoamingCapabilities */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_getRoamingCapabilities(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 17 /* configureRoaming */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_configureRoaming(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 18 /* setRoamingState */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_setRoamingState(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 19 /* enableNdOffload */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_enableNdOffload(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 20 /* startSendingKeepAlivePackets */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_startSendingKeepAlivePackets(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 21 /* stopSendingKeepAlivePackets */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_stopSendingKeepAlivePackets(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 22 /* setScanningMacOui */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_setScanningMacOui(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 23 /* startDebugPacketFateMonitoring */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_startDebugPacketFateMonitoring(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 24 /* getDebugTxPacketFates */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_getDebugTxPacketFates(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 25 /* getDebugRxPacketFates */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_0::BnHwWifiStaIface::_hidl_getDebugRxPacketFates(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 26 /* readApfPacketFilterData */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_2::BnHwWifiStaIface::_hidl_readApfPacketFilterData(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 27 /* setMacAddress */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_2::BnHwWifiStaIface::_hidl_setMacAddress(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 28 /* getLinkLayerStats_1_3 */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_3::BnHwWifiStaIface::_hidl_getLinkLayerStats_1_3(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 29 /* getFactoryMacAddress */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::V1_3::BnHwWifiStaIface::_hidl_getFactoryMacAddress(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsWifiStaIface::BsWifiStaIface(const ::android::sp<::android::hardware::wifi::V1_3::IWifiStaIface> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.wifi@1.3", "IWifiStaIface"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsWifiStaIface::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<IWifiStaIface> IWifiStaIface::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwWifiStaIface>(serviceName, false, getStub);
}

::android::sp<IWifiStaIface> IWifiStaIface::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwWifiStaIface>(serviceName, true, getStub);
}

::android::status_t IWifiStaIface::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool IWifiStaIface::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.wifi@1.3::IWifiStaIface",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_3
}  // namespace wifi
}  // namespace hardware
}  // namespace android
