#define LOG_TAG "android.hardware.wifi.supplicant@1.2::SupplicantP2pIface"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hidl/manager/1.0/IServiceManager.h>
#include <android/hardware/wifi/supplicant/1.2/BpHwSupplicantP2pIface.h>
#include <android/hardware/wifi/supplicant/1.2/BnHwSupplicantP2pIface.h>
#include <android/hardware/wifi/supplicant/1.2/BsSupplicantP2pIface.h>
#include <android/hardware/wifi/supplicant/1.0/BpHwSupplicantP2pIface.h>
#include <android/hardware/wifi/supplicant/1.0/BpHwSupplicantIface.h>
#include <android/hidl/base/1.0/BpHwBase.h>
#include <hidl/ServiceManagement.h>

namespace android {
namespace hardware {
namespace wifi {
namespace supplicant {
namespace V1_2 {

const char* ISupplicantP2pIface::descriptor("android.hardware.wifi.supplicant@1.2::ISupplicantP2pIface");

__attribute__((constructor)) static void static_constructor() {
    ::android::hardware::details::getBnConstructorMap().set(ISupplicantP2pIface::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hardware::IBinder> {
                return new BnHwSupplicantP2pIface(static_cast<ISupplicantP2pIface *>(iIntf));
            });
    ::android::hardware::details::getBsConstructorMap().set(ISupplicantP2pIface::descriptor,
            [](void *iIntf) -> ::android::sp<::android::hidl::base::V1_0::IBase> {
                return new BsSupplicantP2pIface(static_cast<ISupplicantP2pIface *>(iIntf));
            });
};

__attribute__((destructor))static void static_destructor() {
    ::android::hardware::details::getBnConstructorMap().erase(ISupplicantP2pIface::descriptor);
    ::android::hardware::details::getBsConstructorMap().erase(ISupplicantP2pIface::descriptor);
};

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantIface follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::getName(getName_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::getType(getType_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::addNetwork(addNetwork_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::removeNetwork(uint32_t id, removeNetwork_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::getNetwork(uint32_t id, getNetwork_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::listNetworks(listNetworks_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setWpsDeviceName(const ::android::hardware::hidl_string& name, setWpsDeviceName_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setWpsDeviceType(const ::android::hardware::hidl_array<uint8_t, 8>& type, setWpsDeviceType_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setWpsManufacturer(const ::android::hardware::hidl_string& manufacturer, setWpsManufacturer_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setWpsModelName(const ::android::hardware::hidl_string& modelName, setWpsModelName_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setWpsModelNumber(const ::android::hardware::hidl_string& modelNumber, setWpsModelNumber_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setWpsSerialNumber(const ::android::hardware::hidl_string& serialNumber, setWpsSerialNumber_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setWpsConfigMethods(::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods, setWpsConfigMethods_cb _hidl_cb)

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::registerCallback(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback>& callback, registerCallback_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::getDeviceAddress(getDeviceAddress_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setSsidPostfix(const ::android::hardware::hidl_vec<uint8_t>& postfix, setSsidPostfix_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setGroupIdle(const ::android::hardware::hidl_string& groupIfName, uint32_t timeoutInSec, setGroupIdle_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setPowerSave(const ::android::hardware::hidl_string& groupIfName, bool enable, setPowerSave_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::find(uint32_t timeoutInSec, find_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::stopFind(stopFind_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::flush(flush_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::connect(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface::WpsProvisionMethod provisionMethod, const ::android::hardware::hidl_string& preSelectedPin, bool joinExistingGroup, bool persistent, uint32_t goIntent, connect_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::cancelConnect(cancelConnect_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::provisionDiscovery(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface::WpsProvisionMethod provisionMethod, provisionDiscovery_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::addGroup(bool persistent, uint32_t persistentNetworkId, addGroup_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::removeGroup(const ::android::hardware::hidl_string& groupIfName, removeGroup_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::reject(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, reject_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::invite(const ::android::hardware::hidl_string& groupIfName, const ::android::hardware::hidl_array<uint8_t, 6>& goDeviceAddress, const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, invite_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::reinvoke(uint32_t persistentNetworkId, const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, reinvoke_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::configureExtListen(uint32_t periodInMillis, uint32_t intervalInMillis, configureExtListen_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setListenChannel(uint32_t channel, uint32_t operatingClass, setListenChannel_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setDisallowedFrequencies(const ::android::hardware::hidl_vec<::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface::FreqRange>& ranges, setDisallowedFrequencies_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::getSsid(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, getSsid_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::getGroupCapability(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, getGroupCapability_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::addBonjourService(const ::android::hardware::hidl_vec<uint8_t>& query, const ::android::hardware::hidl_vec<uint8_t>& response, addBonjourService_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::removeBonjourService(const ::android::hardware::hidl_vec<uint8_t>& query, removeBonjourService_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::addUpnpService(uint32_t version, const ::android::hardware::hidl_string& serviceName, addUpnpService_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::removeUpnpService(uint32_t version, const ::android::hardware::hidl_string& serviceName, removeUpnpService_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::flushServices(flushServices_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::requestServiceDiscovery(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, const ::android::hardware::hidl_vec<uint8_t>& query, requestServiceDiscovery_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::cancelServiceDiscovery(uint64_t identifier, cancelServiceDiscovery_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setMiracastMode(::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface::MiracastMode mode, setMiracastMode_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::startWpsPbc(const ::android::hardware::hidl_string& groupIfName, const ::android::hardware::hidl_array<uint8_t, 6>& bssid, startWpsPbc_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::startWpsPinKeypad(const ::android::hardware::hidl_string& groupIfName, const ::android::hardware::hidl_string& pin, startWpsPinKeypad_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::startWpsPinDisplay(const ::android::hardware::hidl_string& groupIfName, const ::android::hardware::hidl_array<uint8_t, 6>& bssid, startWpsPinDisplay_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::cancelWps(const ::android::hardware::hidl_string& groupIfName, cancelWps_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::enableWfd(bool enable, enableWfd_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setWfdDeviceInfo(const ::android::hardware::hidl_array<uint8_t, 6>& info, setWfdDeviceInfo_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::createNfcHandoverRequestMessage(createNfcHandoverRequestMessage_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::createNfcHandoverSelectMessage(createNfcHandoverSelectMessage_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::reportNfcHandoverResponse(const ::android::hardware::hidl_vec<uint8_t>& request, reportNfcHandoverResponse_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::reportNfcHandoverInitiation(const ::android::hardware::hidl_vec<uint8_t>& select, reportNfcHandoverInitiation_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::saveConfig(saveConfig_cb _hidl_cb)

// Methods from ::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface follow.
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::addGroup_1_2(const ::android::hardware::hidl_vec<uint8_t>& ssid, const ::android::hardware::hidl_string& pskPassphrase, bool persistent, uint32_t freq, const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, bool joinExistingGroup, addGroup_1_2_cb _hidl_cb)
// no default implementation for: ::android::hardware::Return<void> ISupplicantP2pIface::setMacRandomization(bool enable, setMacRandomization_cb _hidl_cb)

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> ISupplicantP2pIface::interfaceChain(interfaceChain_cb _hidl_cb){
    _hidl_cb({
        ::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface::descriptor,
        ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface::descriptor,
        ::android::hardware::wifi::supplicant::V1_0::ISupplicantIface::descriptor,
        ::android::hidl::base::V1_0::IBase::descriptor,
    });
    return ::android::hardware::Void();}

::android::hardware::Return<void> ISupplicantP2pIface::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    (void)fd;
    (void)options;
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantP2pIface::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    _hidl_cb(::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface::descriptor);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantP2pIface::getHashChain(getHashChain_cb _hidl_cb){
    _hidl_cb({
        (uint8_t[32]){18,2,17,55,31,221,41,251,19,72,55,7,29,67,42,48,45,123,96,233,185,90,246,17,221,141,222,134,189,31,119,238} /* 120211371fdd29fb134837071d432a302d7b60e9b95af611dd8dde86bd1f77ee */,
        (uint8_t[32]){73,7,65,3,56,197,232,219,238,196,181,237,194,96,142,163,35,245,86,25,69,248,129,10,248,24,16,196,123,1,145,132} /* 4907410338c5e8dbeec4b5edc2608ea323f5561945f8810af81810c47b019184 */,
        (uint8_t[32]){53,186,123,205,241,143,36,168,102,167,229,66,149,72,240,103,104,187,32,162,87,247,91,16,163,151,196,216,37,239,132,56} /* 35ba7bcdf18f24a866a7e5429548f06768bb20a257f75b10a397c4d825ef8438 */,
        (uint8_t[32]){236,127,215,158,208,45,250,133,188,73,148,38,173,174,62,190,35,239,5,36,243,205,105,87,19,147,36,184,59,24,202,76} /* ec7fd79ed02dfa85bc499426adae3ebe23ef0524f3cd6957139324b83b18ca4c */});
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantP2pIface::setHALInstrumentation(){
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISupplicantP2pIface::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    (void)cookie;
    return (recipient != nullptr);
}

::android::hardware::Return<void> ISupplicantP2pIface::ping(){
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantP2pIface::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = -1;
    info.ptr = 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::hardware::Return<void> ISupplicantP2pIface::notifySyspropsChanged(){
    ::android::report_sysprop_change();
    return ::android::hardware::Void();
}

::android::hardware::Return<bool> ISupplicantP2pIface::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    return (recipient != nullptr);
}


::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface>> ISupplicantP2pIface::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface>& parent, bool /* emitError */) {
    return parent;
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface>> ISupplicantP2pIface::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicantP2pIface, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface, BpHwSupplicantP2pIface>(
            parent, "android.hardware.wifi.supplicant@1.2::ISupplicantP2pIface", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface>> ISupplicantP2pIface::castFrom(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantIface>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicantP2pIface, ::android::hardware::wifi::supplicant::V1_0::ISupplicantIface, BpHwSupplicantP2pIface>(
            parent, "android.hardware.wifi.supplicant@1.2::ISupplicantP2pIface", emitError);
}

::android::hardware::Return<::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface>> ISupplicantP2pIface::castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError) {
    return ::android::hardware::details::castInterface<ISupplicantP2pIface, ::android::hidl::base::V1_0::IBase, BpHwSupplicantP2pIface>(
            parent, "android.hardware.wifi.supplicant@1.2::ISupplicantP2pIface", emitError);
}

BpHwSupplicantP2pIface::BpHwSupplicantP2pIface(const ::android::sp<::android::hardware::IBinder> &_hidl_impl)
        : BpInterface<ISupplicantP2pIface>(_hidl_impl),
          ::android::hardware::details::HidlInstrumentor("android.hardware.wifi.supplicant@1.2", "ISupplicantP2pIface") {
}

// Methods from ::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface follow.
::android::hardware::Return<void> BpHwSupplicantP2pIface::_hidl_addGroup_1_2(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, const ::android::hardware::hidl_vec<uint8_t>& ssid, const ::android::hardware::hidl_string& pskPassphrase, bool persistent, uint32_t freq, const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, bool joinExistingGroup, addGroup_1_2_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIface::addGroup_1_2::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&ssid);
        _hidl_args.push_back((void *)&pskPassphrase);
        _hidl_args.push_back((void *)&persistent);
        _hidl_args.push_back((void *)&freq);
        _hidl_args.push_back((void *)&peerAddress);
        _hidl_args.push_back((void *)&joinExistingGroup);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.2", "ISupplicantP2pIface", "addGroup_1_2", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    ::android::hardware::wifi::supplicant::V1_0::SupplicantStatus* _hidl_out_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIface::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_ssid_parent;

    _hidl_err = _hidl_data.writeBuffer(&ssid, sizeof(ssid), &_hidl_ssid_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_ssid_child;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            ssid,
            &_hidl_data,
            _hidl_ssid_parent,
            0 /* parentOffset */, &_hidl_ssid_child);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_pskPassphrase_parent;

    _hidl_err = _hidl_data.writeBuffer(&pskPassphrase, sizeof(pskPassphrase), &_hidl_pskPassphrase_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            pskPassphrase,
            &_hidl_data,
            _hidl_pskPassphrase_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeBool(persistent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeUint32(freq);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    size_t _hidl_peerAddress_parent;

    _hidl_err = _hidl_data.writeBuffer(peerAddress.data(), 6 * sizeof(uint8_t), &_hidl_peerAddress_parent);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeBool(joinExistingGroup);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(54 /* addGroup_1_2 */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    size_t _hidl__hidl_out_status_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_status), &_hidl__hidl_out_status_parent,  const_cast<const void**>(reinterpret_cast<void **>(&_hidl_out_status)));
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::supplicant::V1_0::SupplicantStatus &>(*_hidl_out_status),
            _hidl_reply,
            _hidl__hidl_out_status_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(*_hidl_out_status);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)_hidl_out_status);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.2", "ISupplicantP2pIface", "addGroup_1_2", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::_hidl_setMacRandomization(::android::hardware::IInterface *_hidl_this, ::android::hardware::details::HidlInstrumentor *_hidl_this_instrumentor, bool enable, setMacRandomization_cb _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this_instrumentor->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this_instrumentor->getInstrumentationCallbacks();
    #else
    (void) _hidl_this_instrumentor;
    #endif // __ANDROID_DEBUGGABLE__
    if (_hidl_cb == nullptr) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_ILLEGAL_ARGUMENT,
                "Null synchronous callback passed.");
    }

    ::android::ScopedTrace PASTE(___tracer, __LINE__) (ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIface::setMacRandomization::client");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&enable);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_ENTRY, "android.hardware.wifi.supplicant", "1.2", "ISupplicantP2pIface", "setMacRandomization", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    ::android::hardware::Parcel _hidl_data;
    ::android::hardware::Parcel _hidl_reply;
    ::android::status_t _hidl_err;
    ::android::hardware::Status _hidl_status;

    ::android::hardware::wifi::supplicant::V1_0::SupplicantStatus* _hidl_out_status;

    _hidl_err = _hidl_data.writeInterfaceToken(BpHwSupplicantP2pIface::descriptor);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = _hidl_data.writeBool(enable);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::IInterface::asBinder(_hidl_this)->transact(55 /* setMacRandomization */, _hidl_data, &_hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = ::android::hardware::readFromParcel(&_hidl_status, _hidl_reply);
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    if (!_hidl_status.isOk()) { return _hidl_status; }

    size_t _hidl__hidl_out_status_parent;

    _hidl_err = _hidl_reply.readBuffer(sizeof(*_hidl_out_status), &_hidl__hidl_out_status_parent,  const_cast<const void**>(reinterpret_cast<void **>(&_hidl_out_status)));
    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::wifi::supplicant::V1_0::SupplicantStatus &>(*_hidl_out_status),
            _hidl_reply,
            _hidl__hidl_out_status_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { goto _hidl_error; }

    _hidl_cb(*_hidl_out_status);

    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)_hidl_out_status);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::CLIENT_API_EXIT, "android.hardware.wifi.supplicant", "1.2", "ISupplicantP2pIface", "setMacRandomization", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>();

_hidl_error:
    _hidl_status.setFromStatusT(_hidl_err);
    return ::android::hardware::Return<void>(_hidl_status);
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantIface follow.
::android::hardware::Return<void> BpHwSupplicantP2pIface::getName(getName_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_getName(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::getType(getType_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_getType(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::addNetwork(addNetwork_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_addNetwork(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::removeNetwork(uint32_t id, removeNetwork_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_removeNetwork(this, this, id, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::getNetwork(uint32_t id, getNetwork_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_getNetwork(this, this, id, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::listNetworks(listNetworks_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_listNetworks(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setWpsDeviceName(const ::android::hardware::hidl_string& name, setWpsDeviceName_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsDeviceName(this, this, name, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setWpsDeviceType(const ::android::hardware::hidl_array<uint8_t, 8>& type, setWpsDeviceType_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsDeviceType(this, this, type, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setWpsManufacturer(const ::android::hardware::hidl_string& manufacturer, setWpsManufacturer_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsManufacturer(this, this, manufacturer, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setWpsModelName(const ::android::hardware::hidl_string& modelName, setWpsModelName_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsModelName(this, this, modelName, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setWpsModelNumber(const ::android::hardware::hidl_string& modelNumber, setWpsModelNumber_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsModelNumber(this, this, modelNumber, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setWpsSerialNumber(const ::android::hardware::hidl_string& serialNumber, setWpsSerialNumber_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsSerialNumber(this, this, serialNumber, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setWpsConfigMethods(::android::hardware::hidl_bitfield<::android::hardware::wifi::supplicant::V1_0::WpsConfigMethods> configMethods, setWpsConfigMethods_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantIface::_hidl_setWpsConfigMethods(this, this, configMethods, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface follow.
::android::hardware::Return<void> BpHwSupplicantP2pIface::registerCallback(const ::android::sp<::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIfaceCallback>& callback, registerCallback_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_registerCallback(this, this, callback, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::getDeviceAddress(getDeviceAddress_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_getDeviceAddress(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setSsidPostfix(const ::android::hardware::hidl_vec<uint8_t>& postfix, setSsidPostfix_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_setSsidPostfix(this, this, postfix, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setGroupIdle(const ::android::hardware::hidl_string& groupIfName, uint32_t timeoutInSec, setGroupIdle_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_setGroupIdle(this, this, groupIfName, timeoutInSec, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setPowerSave(const ::android::hardware::hidl_string& groupIfName, bool enable, setPowerSave_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_setPowerSave(this, this, groupIfName, enable, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::find(uint32_t timeoutInSec, find_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_find(this, this, timeoutInSec, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::stopFind(stopFind_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_stopFind(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::flush(flush_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_flush(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::connect(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface::WpsProvisionMethod provisionMethod, const ::android::hardware::hidl_string& preSelectedPin, bool joinExistingGroup, bool persistent, uint32_t goIntent, connect_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_connect(this, this, peerAddress, provisionMethod, preSelectedPin, joinExistingGroup, persistent, goIntent, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::cancelConnect(cancelConnect_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_cancelConnect(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::provisionDiscovery(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface::WpsProvisionMethod provisionMethod, provisionDiscovery_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_provisionDiscovery(this, this, peerAddress, provisionMethod, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::addGroup(bool persistent, uint32_t persistentNetworkId, addGroup_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_addGroup(this, this, persistent, persistentNetworkId, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::removeGroup(const ::android::hardware::hidl_string& groupIfName, removeGroup_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_removeGroup(this, this, groupIfName, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::reject(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, reject_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_reject(this, this, peerAddress, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::invite(const ::android::hardware::hidl_string& groupIfName, const ::android::hardware::hidl_array<uint8_t, 6>& goDeviceAddress, const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, invite_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_invite(this, this, groupIfName, goDeviceAddress, peerAddress, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::reinvoke(uint32_t persistentNetworkId, const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, reinvoke_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_reinvoke(this, this, persistentNetworkId, peerAddress, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::configureExtListen(uint32_t periodInMillis, uint32_t intervalInMillis, configureExtListen_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_configureExtListen(this, this, periodInMillis, intervalInMillis, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setListenChannel(uint32_t channel, uint32_t operatingClass, setListenChannel_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_setListenChannel(this, this, channel, operatingClass, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setDisallowedFrequencies(const ::android::hardware::hidl_vec<::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface::FreqRange>& ranges, setDisallowedFrequencies_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_setDisallowedFrequencies(this, this, ranges, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::getSsid(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, getSsid_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_getSsid(this, this, peerAddress, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::getGroupCapability(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, getGroupCapability_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_getGroupCapability(this, this, peerAddress, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::addBonjourService(const ::android::hardware::hidl_vec<uint8_t>& query, const ::android::hardware::hidl_vec<uint8_t>& response, addBonjourService_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_addBonjourService(this, this, query, response, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::removeBonjourService(const ::android::hardware::hidl_vec<uint8_t>& query, removeBonjourService_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_removeBonjourService(this, this, query, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::addUpnpService(uint32_t version, const ::android::hardware::hidl_string& serviceName, addUpnpService_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_addUpnpService(this, this, version, serviceName, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::removeUpnpService(uint32_t version, const ::android::hardware::hidl_string& serviceName, removeUpnpService_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_removeUpnpService(this, this, version, serviceName, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::flushServices(flushServices_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_flushServices(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::requestServiceDiscovery(const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, const ::android::hardware::hidl_vec<uint8_t>& query, requestServiceDiscovery_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_requestServiceDiscovery(this, this, peerAddress, query, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::cancelServiceDiscovery(uint64_t identifier, cancelServiceDiscovery_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_cancelServiceDiscovery(this, this, identifier, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setMiracastMode(::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface::MiracastMode mode, setMiracastMode_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_setMiracastMode(this, this, mode, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::startWpsPbc(const ::android::hardware::hidl_string& groupIfName, const ::android::hardware::hidl_array<uint8_t, 6>& bssid, startWpsPbc_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_startWpsPbc(this, this, groupIfName, bssid, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::startWpsPinKeypad(const ::android::hardware::hidl_string& groupIfName, const ::android::hardware::hidl_string& pin, startWpsPinKeypad_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_startWpsPinKeypad(this, this, groupIfName, pin, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::startWpsPinDisplay(const ::android::hardware::hidl_string& groupIfName, const ::android::hardware::hidl_array<uint8_t, 6>& bssid, startWpsPinDisplay_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_startWpsPinDisplay(this, this, groupIfName, bssid, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::cancelWps(const ::android::hardware::hidl_string& groupIfName, cancelWps_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_cancelWps(this, this, groupIfName, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::enableWfd(bool enable, enableWfd_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_enableWfd(this, this, enable, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setWfdDeviceInfo(const ::android::hardware::hidl_array<uint8_t, 6>& info, setWfdDeviceInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_setWfdDeviceInfo(this, this, info, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::createNfcHandoverRequestMessage(createNfcHandoverRequestMessage_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_createNfcHandoverRequestMessage(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::createNfcHandoverSelectMessage(createNfcHandoverSelectMessage_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_createNfcHandoverSelectMessage(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::reportNfcHandoverResponse(const ::android::hardware::hidl_vec<uint8_t>& request, reportNfcHandoverResponse_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_reportNfcHandoverResponse(this, this, request, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::reportNfcHandoverInitiation(const ::android::hardware::hidl_vec<uint8_t>& select, reportNfcHandoverInitiation_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_reportNfcHandoverInitiation(this, this, select, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::saveConfig(saveConfig_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_0::BpHwSupplicantP2pIface::_hidl_saveConfig(this, this, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface follow.
::android::hardware::Return<void> BpHwSupplicantP2pIface::addGroup_1_2(const ::android::hardware::hidl_vec<uint8_t>& ssid, const ::android::hardware::hidl_string& pskPassphrase, bool persistent, uint32_t freq, const ::android::hardware::hidl_array<uint8_t, 6>& peerAddress, bool joinExistingGroup, addGroup_1_2_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_2::BpHwSupplicantP2pIface::_hidl_addGroup_1_2(this, this, ssid, pskPassphrase, persistent, freq, peerAddress, joinExistingGroup, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setMacRandomization(bool enable, setMacRandomization_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hardware::wifi::supplicant::V1_2::BpHwSupplicantP2pIface::_hidl_setMacRandomization(this, this, enable, _hidl_cb);

    return _hidl_out;
}


// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BpHwSupplicantP2pIface::interfaceChain(interfaceChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_debug(this, this, fd, options);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::interfaceDescriptor(interfaceDescriptor_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_interfaceDescriptor(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::getHashChain(getHashChain_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getHashChain(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::setHALInstrumentation(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_setHALInstrumentation(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicantP2pIface::linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie){
    ::android::hardware::ProcessState::self()->startThreadPool();
    ::android::hardware::hidl_binder_death_recipient *binder_recipient = new ::android::hardware::hidl_binder_death_recipient(recipient, cookie, this);
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    _hidl_mDeathRecipients.push_back(binder_recipient);
    return (remote()->linkToDeath(binder_recipient) == ::android::OK);
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::ping(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_ping(this, this);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::getDebugInfo(getDebugInfo_cb _hidl_cb){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_getDebugInfo(this, this, _hidl_cb);

    return _hidl_out;
}

::android::hardware::Return<void> BpHwSupplicantP2pIface::notifySyspropsChanged(){
    ::android::hardware::Return<void>  _hidl_out = ::android::hidl::base::V1_0::BpHwBase::_hidl_notifySyspropsChanged(this, this);

    return _hidl_out;
}

::android::hardware::Return<bool> BpHwSupplicantP2pIface::unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient){
    std::unique_lock<std::mutex> lock(_hidl_mMutex);
    for (auto it = _hidl_mDeathRecipients.rbegin();it != _hidl_mDeathRecipients.rend();++it) {
        if ((*it)->getRecipient() == recipient) {
            ::android::status_t status = remote()->unlinkToDeath(*it);
            _hidl_mDeathRecipients.erase(it.base()-1);
            return status == ::android::OK;
        }
    }
    return false;
}


BnHwSupplicantP2pIface::BnHwSupplicantP2pIface(const ::android::sp<ISupplicantP2pIface> &_hidl_impl)
        : ::android::hidl::base::V1_0::BnHwBase(_hidl_impl, "android.hardware.wifi.supplicant@1.2", "ISupplicantP2pIface") { 
            _hidl_mImpl = _hidl_impl;
            auto prio = ::android::hardware::details::gServicePrioMap->get(_hidl_impl, {SCHED_NORMAL, 0});
            mSchedPolicy = prio.sched_policy;
            mSchedPriority = prio.prio;
            setRequestingSid(::android::hardware::details::gServiceSidMap->get(_hidl_impl, false));
}

BnHwSupplicantP2pIface::~BnHwSupplicantP2pIface() {
    ::android::hardware::details::gBnMap->eraseIfEqual(_hidl_mImpl.get(), this);
}

// Methods from ::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface follow.
::android::status_t BnHwSupplicantP2pIface::_hidl_addGroup_1_2(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIface::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    const ::android::hardware::hidl_vec<uint8_t>* ssid;
    const ::android::hardware::hidl_string* pskPassphrase;
    bool persistent;
    uint32_t freq;
    const ::android::hardware::hidl_array<uint8_t, 6>* peerAddress;
    bool joinExistingGroup;

    size_t _hidl_ssid_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*ssid), &_hidl_ssid_parent,  reinterpret_cast<const void **>(&ssid));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_ssid_child;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_vec<uint8_t> &>(*ssid),
            _hidl_data,
            _hidl_ssid_parent,
            0 /* parentOffset */, &_hidl_ssid_child);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_pskPassphrase_parent;

    _hidl_err = _hidl_data.readBuffer(sizeof(*pskPassphrase), &_hidl_pskPassphrase_parent,  reinterpret_cast<const void **>(&pskPassphrase));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(*pskPassphrase),
            _hidl_data,
            _hidl_pskPassphrase_parent,
            0 /* parentOffset */);

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readBool(&persistent);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readUint32(&freq);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    size_t _hidl_peerAddress_parent;

    _hidl_err = _hidl_data.readBuffer(6 * sizeof(uint8_t), &_hidl_peerAddress_parent,  reinterpret_cast<const void **>(&peerAddress));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = _hidl_data.readBool(&joinExistingGroup);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIface::addGroup_1_2::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)ssid);
        _hidl_args.push_back((void *)pskPassphrase);
        _hidl_args.push_back((void *)&persistent);
        _hidl_args.push_back((void *)&freq);
        _hidl_args.push_back((void *)peerAddress);
        _hidl_args.push_back((void *)&joinExistingGroup);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.2", "ISupplicantP2pIface", "addGroup_1_2", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIface*>(_hidl_this->getImpl().get())->addGroup_1_2(*ssid, *pskPassphrase, persistent, freq, *peerAddress, joinExistingGroup, [&](const auto &_hidl_out_status) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("addGroup_1_2: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        size_t _hidl__hidl_out_status_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_status, sizeof(_hidl_out_status), &_hidl__hidl_out_status_parent);
        /* _hidl_err ignored! */

        _hidl_err = writeEmbeddedToParcel(
                _hidl_out_status,
                _hidl_reply,
                _hidl__hidl_out_status_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_status);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.2", "ISupplicantP2pIface", "addGroup_1_2", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("addGroup_1_2: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}

::android::status_t BnHwSupplicantP2pIface::_hidl_setMacRandomization(
        ::android::hidl::base::V1_0::BnHwBase* _hidl_this,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        TransactCallback _hidl_cb) {
    #ifdef __ANDROID_DEBUGGABLE__
    bool mEnableInstrumentation = _hidl_this->isInstrumentationEnabled();
    const auto &mInstrumentationCallbacks = _hidl_this->getInstrumentationCallbacks();
    #endif // __ANDROID_DEBUGGABLE__

    ::android::status_t _hidl_err = ::android::OK;
    if (!_hidl_data.enforceInterface(BnHwSupplicantP2pIface::Pure::descriptor)) {
        _hidl_err = ::android::BAD_TYPE;
        return _hidl_err;
    }

    bool enable;

    _hidl_err = _hidl_data.readBool(&enable);
    if (_hidl_err != ::android::OK) { return _hidl_err; }

    atrace_begin(ATRACE_TAG_HAL, "HIDL::ISupplicantP2pIface::setMacRandomization::server");
    #ifdef __ANDROID_DEBUGGABLE__
    if (UNLIKELY(mEnableInstrumentation)) {
        std::vector<void *> _hidl_args;
        _hidl_args.push_back((void *)&enable);
        for (const auto &callback: mInstrumentationCallbacks) {
            callback(InstrumentationEvent::SERVER_API_ENTRY, "android.hardware.wifi.supplicant", "1.2", "ISupplicantP2pIface", "setMacRandomization", &_hidl_args);
        }
    }
    #endif // __ANDROID_DEBUGGABLE__

    bool _hidl_callbackCalled = false;

    ::android::hardware::Return<void> _hidl_ret = static_cast<ISupplicantP2pIface*>(_hidl_this->getImpl().get())->setMacRandomization(enable, [&](const auto &_hidl_out_status) {
        if (_hidl_callbackCalled) {
            LOG_ALWAYS_FATAL("setMacRandomization: _hidl_cb called a second time, but must be called once.");
        }
        _hidl_callbackCalled = true;

        ::android::hardware::writeToParcel(::android::hardware::Status::ok(), _hidl_reply);

        size_t _hidl__hidl_out_status_parent;

        _hidl_err = _hidl_reply->writeBuffer(&_hidl_out_status, sizeof(_hidl_out_status), &_hidl__hidl_out_status_parent);
        /* _hidl_err ignored! */

        _hidl_err = writeEmbeddedToParcel(
                _hidl_out_status,
                _hidl_reply,
                _hidl__hidl_out_status_parent,
                0 /* parentOffset */);

        /* _hidl_err ignored! */

        atrace_end(ATRACE_TAG_HAL);
        #ifdef __ANDROID_DEBUGGABLE__
        if (UNLIKELY(mEnableInstrumentation)) {
            std::vector<void *> _hidl_args;
            _hidl_args.push_back((void *)&_hidl_out_status);
            for (const auto &callback: mInstrumentationCallbacks) {
                callback(InstrumentationEvent::SERVER_API_EXIT, "android.hardware.wifi.supplicant", "1.2", "ISupplicantP2pIface", "setMacRandomization", &_hidl_args);
            }
        }
        #endif // __ANDROID_DEBUGGABLE__

        _hidl_cb(*_hidl_reply);
    });

    _hidl_ret.assertOk();
    if (!_hidl_callbackCalled) {
        LOG_ALWAYS_FATAL("setMacRandomization: _hidl_cb not called, but must be called once.");
    }

    return _hidl_err;
}


// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantIface follow.

// Methods from ::android::hardware::wifi::supplicant::V1_0::ISupplicantP2pIface follow.

// Methods from ::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface follow.

// Methods from ::android::hidl::base::V1_0::IBase follow.
::android::hardware::Return<void> BnHwSupplicantP2pIface::ping() {
    return ::android::hardware::Void();
}
::android::hardware::Return<void> BnHwSupplicantP2pIface::getDebugInfo(getDebugInfo_cb _hidl_cb) {
    ::android::hidl::base::V1_0::DebugInfo info = {};
    info.pid = ::android::hardware::details::getPidIfSharable();
    info.ptr = ::android::hardware::details::debuggable()? reinterpret_cast<uint64_t>(this) : 0;
    info.arch = 
    #if defined(__LP64__)
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_64BIT
    #else
    ::android::hidl::base::V1_0::DebugInfo::Architecture::IS_32BIT
    #endif
    ;
    _hidl_cb(info);
    return ::android::hardware::Void();
}

::android::status_t BnHwSupplicantP2pIface::onTransact(
        uint32_t _hidl_code,
        const ::android::hardware::Parcel &_hidl_data,
        ::android::hardware::Parcel *_hidl_reply,
        uint32_t _hidl_flags,
        TransactCallback _hidl_cb) {
    ::android::status_t _hidl_err = ::android::OK;

    switch (_hidl_code) {
        case 1 /* getName */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_getName(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 2 /* getType */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_getType(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 3 /* addNetwork */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_addNetwork(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 4 /* removeNetwork */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_removeNetwork(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 5 /* getNetwork */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_getNetwork(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 6 /* listNetworks */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_listNetworks(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 7 /* setWpsDeviceName */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsDeviceName(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 8 /* setWpsDeviceType */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsDeviceType(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 9 /* setWpsManufacturer */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsManufacturer(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 10 /* setWpsModelName */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsModelName(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 11 /* setWpsModelNumber */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsModelNumber(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 12 /* setWpsSerialNumber */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsSerialNumber(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 13 /* setWpsConfigMethods */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantIface::_hidl_setWpsConfigMethods(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 14 /* registerCallback */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_registerCallback(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 15 /* getDeviceAddress */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_getDeviceAddress(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 16 /* setSsidPostfix */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_setSsidPostfix(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 17 /* setGroupIdle */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_setGroupIdle(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 18 /* setPowerSave */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_setPowerSave(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 19 /* find */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_find(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 20 /* stopFind */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_stopFind(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 21 /* flush */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_flush(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 22 /* connect */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_connect(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 23 /* cancelConnect */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_cancelConnect(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 24 /* provisionDiscovery */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_provisionDiscovery(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 25 /* addGroup */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_addGroup(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 26 /* removeGroup */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_removeGroup(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 27 /* reject */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_reject(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 28 /* invite */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_invite(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 29 /* reinvoke */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_reinvoke(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 30 /* configureExtListen */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_configureExtListen(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 31 /* setListenChannel */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_setListenChannel(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 32 /* setDisallowedFrequencies */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_setDisallowedFrequencies(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 33 /* getSsid */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_getSsid(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 34 /* getGroupCapability */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_getGroupCapability(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 35 /* addBonjourService */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_addBonjourService(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 36 /* removeBonjourService */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_removeBonjourService(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 37 /* addUpnpService */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_addUpnpService(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 38 /* removeUpnpService */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_removeUpnpService(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 39 /* flushServices */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_flushServices(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 40 /* requestServiceDiscovery */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_requestServiceDiscovery(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 41 /* cancelServiceDiscovery */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_cancelServiceDiscovery(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 42 /* setMiracastMode */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_setMiracastMode(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 43 /* startWpsPbc */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_startWpsPbc(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 44 /* startWpsPinKeypad */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_startWpsPinKeypad(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 45 /* startWpsPinDisplay */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_startWpsPinDisplay(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 46 /* cancelWps */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_cancelWps(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 47 /* enableWfd */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_enableWfd(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 48 /* setWfdDeviceInfo */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_setWfdDeviceInfo(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 49 /* createNfcHandoverRequestMessage */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_createNfcHandoverRequestMessage(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 50 /* createNfcHandoverSelectMessage */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_createNfcHandoverSelectMessage(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 51 /* reportNfcHandoverResponse */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_reportNfcHandoverResponse(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 52 /* reportNfcHandoverInitiation */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_reportNfcHandoverInitiation(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 53 /* saveConfig */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_0::BnHwSupplicantP2pIface::_hidl_saveConfig(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 54 /* addGroup_1_2 */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_2::BnHwSupplicantP2pIface::_hidl_addGroup_1_2(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        case 55 /* setMacRandomization */:
        {
            bool _hidl_is_oneway = _hidl_flags & 1u /* oneway */;
            if (_hidl_is_oneway != false) {
                return ::android::UNKNOWN_ERROR;
            }

            _hidl_err = ::android::hardware::wifi::supplicant::V1_2::BnHwSupplicantP2pIface::_hidl_setMacRandomization(this, _hidl_data, _hidl_reply, _hidl_cb);
            break;
        }

        default:
        {
            return ::android::hidl::base::V1_0::BnHwBase::onTransact(
                    _hidl_code, _hidl_data, _hidl_reply, _hidl_flags, _hidl_cb);
        }
    }

    if (_hidl_err == ::android::UNEXPECTED_NULL) {
        _hidl_err = ::android::hardware::writeToParcel(
                ::android::hardware::Status::fromExceptionCode(::android::hardware::Status::EX_NULL_POINTER),
                _hidl_reply);
    }return _hidl_err;
}

BsSupplicantP2pIface::BsSupplicantP2pIface(const ::android::sp<::android::hardware::wifi::supplicant::V1_2::ISupplicantP2pIface> impl) : ::android::hardware::details::HidlInstrumentor("android.hardware.wifi.supplicant@1.2", "ISupplicantP2pIface"), mImpl(impl) {
    mOnewayQueue.start(3000 /* similar limit to binderized */);
}

::android::hardware::Return<void> BsSupplicantP2pIface::addOnewayTask(std::function<void(void)> fun) {
    if (!mOnewayQueue.push(fun)) {
        return ::android::hardware::Status::fromExceptionCode(
                ::android::hardware::Status::EX_TRANSACTION_FAILED,
                "Passthrough oneway function queue exceeds maximum size.");
    }
    return ::android::hardware::Status();
}

::android::sp<ISupplicantP2pIface> ISupplicantP2pIface::tryGetService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSupplicantP2pIface>(serviceName, false, getStub);
}

::android::sp<ISupplicantP2pIface> ISupplicantP2pIface::getService(const std::string &serviceName, const bool getStub) {
    return ::android::hardware::details::getServiceInternal<BpHwSupplicantP2pIface>(serviceName, true, getStub);
}

::android::status_t ISupplicantP2pIface::registerAsService(const std::string &serviceName) {
    return ::android::hardware::details::registerAsServiceInternal(this, serviceName);
}

bool ISupplicantP2pIface::registerForNotifications(
        const std::string &serviceName,
        const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification) {
    const ::android::sp<::android::hidl::manager::V1_0::IServiceManager> sm
            = ::android::hardware::defaultServiceManager();
    if (sm == nullptr) {
        return false;
    }
    ::android::hardware::Return<bool> success =
            sm->registerForNotifications("android.hardware.wifi.supplicant@1.2::ISupplicantP2pIface",
                    serviceName, notification);
    return success.isOk() && success;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_2
}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace android
