#define LOG_TAG "android.hardware.tetheroffload.control@1.0::types"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hardware/tetheroffload/control/1.0/types.h>
#include <android/hardware/tetheroffload/control/1.0/hwtypes.h>

namespace android {
namespace hardware {
namespace tetheroffload {
namespace control {
namespace V1_0 {

::android::status_t readEmbeddedFromParcel(
        const IPv4AddrPortPair &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::readEmbeddedFromParcel(
            const_cast<::android::hardware::hidl_string &>(obj.addr),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::tetheroffload::control::V1_0::IPv4AddrPortPair, addr));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const IPv4AddrPortPair &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = ::android::hardware::writeEmbeddedToParcel(
            obj.addr,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::tetheroffload::control::V1_0::IPv4AddrPortPair, addr));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t readEmbeddedFromParcel(
        const NatTimeoutUpdate &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::tetheroffload::control::V1_0::IPv4AddrPortPair &>(obj.src),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::tetheroffload::control::V1_0::NatTimeoutUpdate, src));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::tetheroffload::control::V1_0::IPv4AddrPortPair &>(obj.dst),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::tetheroffload::control::V1_0::NatTimeoutUpdate, dst));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const NatTimeoutUpdate &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = writeEmbeddedToParcel(
            obj.src,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::tetheroffload::control::V1_0::NatTimeoutUpdate, src));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    _hidl_err = writeEmbeddedToParcel(
            obj.dst,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::tetheroffload::control::V1_0::NatTimeoutUpdate, dst));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_0
}  // namespace control
}  // namespace tetheroffload
}  // namespace hardware
}  // namespace android
