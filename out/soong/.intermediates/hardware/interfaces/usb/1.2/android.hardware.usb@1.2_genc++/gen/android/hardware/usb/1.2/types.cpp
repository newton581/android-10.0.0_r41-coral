#define LOG_TAG "android.hardware.usb@1.2::types"

#include <log/log.h>
#include <cutils/trace.h>
#include <hidl/HidlTransportSupport.h>

#include <hidl/Static.h>
#include <hwbinder/ProcessState.h>
#include <utils/Trace.h>
#include <android/hardware/usb/1.2/types.h>
#include <android/hardware/usb/1.2/hwtypes.h>

namespace android {
namespace hardware {
namespace usb {
namespace V1_2 {

::android::status_t readEmbeddedFromParcel(
        const PortStatus &obj,
        const ::android::hardware::Parcel &parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = readEmbeddedFromParcel(
            const_cast<::android::hardware::usb::V1_1::PortStatus_1_1 &>(obj.status_1_1),
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::usb::V1_2::PortStatus, status_1_1));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

::android::status_t writeEmbeddedToParcel(
        const PortStatus &obj,
        ::android::hardware::Parcel *parcel,
        size_t parentHandle,
        size_t parentOffset) {
    ::android::status_t _hidl_err = ::android::OK;

    _hidl_err = writeEmbeddedToParcel(
            obj.status_1_1,
            parcel,
            parentHandle,
            parentOffset + offsetof(::android::hardware::usb::V1_2::PortStatus, status_1_1));

    if (_hidl_err != ::android::OK) { return _hidl_err; }

    return _hidl_err;
}

static_assert(sizeof(::android::hardware::MQDescriptor<char, ::android::hardware::kSynchronizedReadWrite>) == 32, "wrong size");
static_assert(sizeof(::android::hardware::hidl_handle) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_memory) == 40, "wrong size");
static_assert(sizeof(::android::hardware::hidl_string) == 16, "wrong size");
static_assert(sizeof(::android::hardware::hidl_vec<char>) == 16, "wrong size");

}  // namespace V1_2
}  // namespace usb
}  // namespace hardware
}  // namespace android
