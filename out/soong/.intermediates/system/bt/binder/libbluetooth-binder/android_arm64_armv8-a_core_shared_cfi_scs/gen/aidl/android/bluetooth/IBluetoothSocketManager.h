#ifndef AIDL_GENERATED_ANDROID_BLUETOOTH_I_BLUETOOTH_SOCKET_MANAGER_H_
#define AIDL_GENERATED_ANDROID_BLUETOOTH_I_BLUETOOTH_SOCKET_MANAGER_H_

#include <android/bluetooth/bluetooth_device.h>
#include <android/os/parcel_uuid.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/ParcelFileDescriptor.h>
#include <binder/Status.h>
#include <cstdint>
#include <memory>
#include <utils/String16.h>
#include <utils/StrongPointer.h>

namespace android {

namespace bluetooth {

class IBluetoothSocketManager : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(BluetoothSocketManager)
  virtual ::android::binder::Status connectSocket(const ::android::bluetooth::BluetoothDevice& device, int32_t type, const ::std::unique_ptr<::android::os::ParcelUuid>& uuid, int32_t port, int32_t flag, ::std::unique_ptr<::android::os::ParcelFileDescriptor>* _aidl_return) = 0;
  virtual ::android::binder::Status createSocketChannel(int32_t type, const ::std::unique_ptr<::android::String16>& serviceName, const ::std::unique_ptr<::android::os::ParcelUuid>& uuid, int32_t port, int32_t flag, ::std::unique_ptr<::android::os::ParcelFileDescriptor>* _aidl_return) = 0;
  virtual ::android::binder::Status requestMaximumTxDataLength(const ::android::bluetooth::BluetoothDevice& device) = 0;
};  // class IBluetoothSocketManager

class IBluetoothSocketManagerDefault : public IBluetoothSocketManager {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status connectSocket(const ::android::bluetooth::BluetoothDevice& device, int32_t type, const ::std::unique_ptr<::android::os::ParcelUuid>& uuid, int32_t port, int32_t flag, ::std::unique_ptr<::android::os::ParcelFileDescriptor>* _aidl_return) override;
  ::android::binder::Status createSocketChannel(int32_t type, const ::std::unique_ptr<::android::String16>& serviceName, const ::std::unique_ptr<::android::os::ParcelUuid>& uuid, int32_t port, int32_t flag, ::std::unique_ptr<::android::os::ParcelFileDescriptor>* _aidl_return) override;
  ::android::binder::Status requestMaximumTxDataLength(const ::android::bluetooth::BluetoothDevice& device) override;

};

}  // namespace bluetooth

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_BLUETOOTH_I_BLUETOOTH_SOCKET_MANAGER_H_
