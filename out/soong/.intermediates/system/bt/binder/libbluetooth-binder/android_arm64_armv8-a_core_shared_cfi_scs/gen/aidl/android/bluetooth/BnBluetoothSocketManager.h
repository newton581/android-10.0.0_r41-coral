#ifndef AIDL_GENERATED_ANDROID_BLUETOOTH_BN_BLUETOOTH_SOCKET_MANAGER_H_
#define AIDL_GENERATED_ANDROID_BLUETOOTH_BN_BLUETOOTH_SOCKET_MANAGER_H_

#include <binder/IInterface.h>
#include <android/bluetooth/IBluetoothSocketManager.h>

namespace android {

namespace bluetooth {

class BnBluetoothSocketManager : public ::android::BnInterface<IBluetoothSocketManager> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnBluetoothSocketManager

}  // namespace bluetooth

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_BLUETOOTH_BN_BLUETOOTH_SOCKET_MANAGER_H_
