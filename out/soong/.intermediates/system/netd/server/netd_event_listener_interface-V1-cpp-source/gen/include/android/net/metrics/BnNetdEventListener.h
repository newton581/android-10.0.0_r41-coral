#ifndef AIDL_GENERATED_ANDROID_NET_METRICS_BN_NETD_EVENT_LISTENER_H_
#define AIDL_GENERATED_ANDROID_NET_METRICS_BN_NETD_EVENT_LISTENER_H_

#include <binder/IInterface.h>
#include <android/net/metrics/INetdEventListener.h>

namespace android {

namespace net {

namespace metrics {

class BnNetdEventListener : public ::android::BnInterface<INetdEventListener> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
  int32_t getInterfaceVersion() final override;
};  // class BnNetdEventListener

}  // namespace metrics

}  // namespace net

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_NET_METRICS_BN_NETD_EVENT_LISTENER_H_
