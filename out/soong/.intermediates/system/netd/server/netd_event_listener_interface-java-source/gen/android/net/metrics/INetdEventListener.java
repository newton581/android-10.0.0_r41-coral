/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.net.metrics;
/**
 * Logs netd events.
 *
 * {@hide}
 */
public interface INetdEventListener extends android.os.IInterface
{
  /**
   * The version of this interface that the caller is built against.
   * This might be different from what {@link #getInterfaceVersion()
   * getInterfaceVersion} returns as that is the version of the interface
   * that the remote object is implementing.
   */
  public static final int VERSION = 10000;
  /** Default implementation for INetdEventListener. */
  public static class Default implements android.net.metrics.INetdEventListener
  {
    /**
         * Logs a DNS lookup function call (getaddrinfo and gethostbyname).
         *
         * @param netId the ID of the network the lookup was performed on.
         * @param eventType one of the EVENT_* constants in this interface.
         * @param returnCode the return value of the function call.
         * @param latencyMs the latency of the function call.
         * @param hostname the name that was looked up.
         * @param ipAddresses (possibly a subset of) the IP addresses returned.
         *        At most {@link #DNS_REPORTED_IP_ADDRESSES_LIMIT} addresses are logged.
         * @param ipAddressesCount the number of IP addresses returned. May be different from the length
         *        of ipAddresses if there were too many addresses to log.
         * @param uid the UID of the application that performed the query.
         */
    @Override public void onDnsEvent(int netId, int eventType, int returnCode, int latencyMs, java.lang.String hostname, java.lang.String[] ipAddresses, int ipAddressesCount, int uid) throws android.os.RemoteException
    {
    }
    /**
         * Represents a private DNS validation success or failure.
         *
         * @param netId the ID of the network the validation was performed on.
         * @param ipAddress the IP address for which validation was performed.
         * @param hostname the hostname for which validation was performed.
         * @param validated whether or not validation was successful.
         */
    @Override public void onPrivateDnsValidationEvent(int netId, java.lang.String ipAddress, java.lang.String hostname, boolean validated) throws android.os.RemoteException
    {
    }
    /**
         * Logs a single connect library call.
         *
         * @param netId the ID of the network the connect was performed on.
         * @param error 0 if the connect call succeeded, otherwise errno if it failed.
         * @param latencyMs the latency of the connect call.
         * @param ipAddr destination IP address.
         * @param port destination port number.
         * @param uid the UID of the application that performed the connection.
         */
    @Override public void onConnectEvent(int netId, int error, int latencyMs, java.lang.String ipAddr, int port, int uid) throws android.os.RemoteException
    {
    }
    /**
         * Logs a single RX packet which caused the main CPU to exit sleep state.
         * @param prefix arbitrary string provided via wakeupAddInterface()
         * @param uid UID of the destination process or -1 if no UID is available.
         * @param ethertype of the RX packet encoded in an int in native order, or -1 if not available.
         * @param ipNextHeader ip protocol of the RX packet as IPPROTO_* number,
                  or -1 if the packet was not IPv4 or IPv6.
         * @param dstHw destination hardware address, or 0 if not available.
         * @param srcIp source IP address, or null if not available.
         * @param dstIp destination IP address, or null if not available.
         * @param srcPort src port of RX packet in native order, or -1 if the packet was not UDP or TCP.
         * @param dstPort dst port of RX packet in native order, or -1 if the packet was not UDP or TCP.
         * @param timestampNs receive timestamp for the offending packet. In units of nanoseconds and
         *        synchronized to CLOCK_MONOTONIC.
         */
    @Override public void onWakeupEvent(java.lang.String prefix, int uid, int ethertype, int ipNextHeader, byte[] dstHw, java.lang.String srcIp, java.lang.String dstIp, int srcPort, int dstPort, long timestampNs) throws android.os.RemoteException
    {
    }
    /**
         * An event sent after every Netlink sock_diag poll performed by Netd. This reported batch
         * groups TCP socket stats aggregated by network id. Per-network data are stored in a
         * structure-of-arrays style where networkIds, sentPackets, lostPackets, rttUs, and
         * sentAckDiffMs have the same length. Stats for the i-th network is spread across all these
         * arrays at index i.
         * @param networkIds an array of network ids for which there was tcp socket stats to collect in
         *        the last sock_diag poll.
         * @param sentPackets an array of packet sent across all TCP sockets still alive and new
                  TCP sockets since the last sock_diag poll, summed per network id.
         * @param lostPackets, an array of packet lost across all TCP sockets still alive and new
                  TCP sockets since the last sock_diag poll, summed per network id.
         * @param rttUs an array of smoothed round trip times in microseconds, averaged across all TCP
                  sockets since the last sock_diag poll for a given network id.
         * @param sentAckDiffMs an array of milliseconds duration between the last packet sent and the
                  last ack received for a socket, averaged across all TCP sockets for a network id.
         */
    @Override public void onTcpSocketStatsEvent(int[] networkIds, int[] sentPackets, int[] lostPackets, int[] rttUs, int[] sentAckDiffMs) throws android.os.RemoteException
    {
    }
    /**
         * Represents adding or removing a NAT64 prefix.
         *
         * @param netId the ID of the network the prefix was discovered on.
         * @param added true if the NAT64 prefix was added, or false if the NAT64 prefix was removed.
         *        There is only one prefix at a time for each netId. If a prefix is added, it replaces
         *        the previous-added prefix.
         * @param prefixString the detected NAT64 prefix as a string literal.
         * @param prefixLength the prefix length associated with this NAT64 prefix.
         */
    @Override public void onNat64PrefixEvent(int netId, boolean added, java.lang.String prefixString, int prefixLength) throws android.os.RemoteException
    {
    }
    @Override
    public int getInterfaceVersion() {
      return -1;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.net.metrics.INetdEventListener
  {
    private static final java.lang.String DESCRIPTOR = "android.net.metrics.INetdEventListener";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.net.metrics.INetdEventListener interface,
     * generating a proxy if needed.
     */
    public static android.net.metrics.INetdEventListener asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.net.metrics.INetdEventListener))) {
        return ((android.net.metrics.INetdEventListener)iin);
      }
      return new android.net.metrics.INetdEventListener.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onDnsEvent:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          java.lang.String _arg4;
          _arg4 = data.readString();
          java.lang.String[] _arg5;
          _arg5 = data.createStringArray();
          int _arg6;
          _arg6 = data.readInt();
          int _arg7;
          _arg7 = data.readInt();
          this.onDnsEvent(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg7);
          return true;
        }
        case TRANSACTION_onPrivateDnsValidationEvent:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          boolean _arg3;
          _arg3 = (0!=data.readInt());
          this.onPrivateDnsValidationEvent(_arg0, _arg1, _arg2, _arg3);
          return true;
        }
        case TRANSACTION_onConnectEvent:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          int _arg5;
          _arg5 = data.readInt();
          this.onConnectEvent(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          return true;
        }
        case TRANSACTION_onWakeupEvent:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          byte[] _arg4;
          _arg4 = data.createByteArray();
          java.lang.String _arg5;
          _arg5 = data.readString();
          java.lang.String _arg6;
          _arg6 = data.readString();
          int _arg7;
          _arg7 = data.readInt();
          int _arg8;
          _arg8 = data.readInt();
          long _arg9;
          _arg9 = data.readLong();
          this.onWakeupEvent(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg7, _arg8, _arg9);
          return true;
        }
        case TRANSACTION_onTcpSocketStatsEvent:
        {
          data.enforceInterface(descriptor);
          int[] _arg0;
          _arg0 = data.createIntArray();
          int[] _arg1;
          _arg1 = data.createIntArray();
          int[] _arg2;
          _arg2 = data.createIntArray();
          int[] _arg3;
          _arg3 = data.createIntArray();
          int[] _arg4;
          _arg4 = data.createIntArray();
          this.onTcpSocketStatsEvent(_arg0, _arg1, _arg2, _arg3, _arg4);
          return true;
        }
        case TRANSACTION_onNat64PrefixEvent:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          java.lang.String _arg2;
          _arg2 = data.readString();
          int _arg3;
          _arg3 = data.readInt();
          this.onNat64PrefixEvent(_arg0, _arg1, _arg2, _arg3);
          return true;
        }
        case TRANSACTION_getInterfaceVersion:
        {
          data.enforceInterface(descriptor);
          reply.writeNoException();
          reply.writeInt(getInterfaceVersion());
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.net.metrics.INetdEventListener
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      private int mCachedVersion = -1;
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Logs a DNS lookup function call (getaddrinfo and gethostbyname).
           *
           * @param netId the ID of the network the lookup was performed on.
           * @param eventType one of the EVENT_* constants in this interface.
           * @param returnCode the return value of the function call.
           * @param latencyMs the latency of the function call.
           * @param hostname the name that was looked up.
           * @param ipAddresses (possibly a subset of) the IP addresses returned.
           *        At most {@link #DNS_REPORTED_IP_ADDRESSES_LIMIT} addresses are logged.
           * @param ipAddressesCount the number of IP addresses returned. May be different from the length
           *        of ipAddresses if there were too many addresses to log.
           * @param uid the UID of the application that performed the query.
           */
      @Override public void onDnsEvent(int netId, int eventType, int returnCode, int latencyMs, java.lang.String hostname, java.lang.String[] ipAddresses, int ipAddressesCount, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(netId);
          _data.writeInt(eventType);
          _data.writeInt(returnCode);
          _data.writeInt(latencyMs);
          _data.writeString(hostname);
          _data.writeStringArray(ipAddresses);
          _data.writeInt(ipAddressesCount);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onDnsEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onDnsEvent(netId, eventType, returnCode, latencyMs, hostname, ipAddresses, ipAddressesCount, uid);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Represents a private DNS validation success or failure.
           *
           * @param netId the ID of the network the validation was performed on.
           * @param ipAddress the IP address for which validation was performed.
           * @param hostname the hostname for which validation was performed.
           * @param validated whether or not validation was successful.
           */
      @Override public void onPrivateDnsValidationEvent(int netId, java.lang.String ipAddress, java.lang.String hostname, boolean validated) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(netId);
          _data.writeString(ipAddress);
          _data.writeString(hostname);
          _data.writeInt(((validated)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_onPrivateDnsValidationEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onPrivateDnsValidationEvent(netId, ipAddress, hostname, validated);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Logs a single connect library call.
           *
           * @param netId the ID of the network the connect was performed on.
           * @param error 0 if the connect call succeeded, otherwise errno if it failed.
           * @param latencyMs the latency of the connect call.
           * @param ipAddr destination IP address.
           * @param port destination port number.
           * @param uid the UID of the application that performed the connection.
           */
      @Override public void onConnectEvent(int netId, int error, int latencyMs, java.lang.String ipAddr, int port, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(netId);
          _data.writeInt(error);
          _data.writeInt(latencyMs);
          _data.writeString(ipAddr);
          _data.writeInt(port);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onConnectEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onConnectEvent(netId, error, latencyMs, ipAddr, port, uid);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Logs a single RX packet which caused the main CPU to exit sleep state.
           * @param prefix arbitrary string provided via wakeupAddInterface()
           * @param uid UID of the destination process or -1 if no UID is available.
           * @param ethertype of the RX packet encoded in an int in native order, or -1 if not available.
           * @param ipNextHeader ip protocol of the RX packet as IPPROTO_* number,
                    or -1 if the packet was not IPv4 or IPv6.
           * @param dstHw destination hardware address, or 0 if not available.
           * @param srcIp source IP address, or null if not available.
           * @param dstIp destination IP address, or null if not available.
           * @param srcPort src port of RX packet in native order, or -1 if the packet was not UDP or TCP.
           * @param dstPort dst port of RX packet in native order, or -1 if the packet was not UDP or TCP.
           * @param timestampNs receive timestamp for the offending packet. In units of nanoseconds and
           *        synchronized to CLOCK_MONOTONIC.
           */
      @Override public void onWakeupEvent(java.lang.String prefix, int uid, int ethertype, int ipNextHeader, byte[] dstHw, java.lang.String srcIp, java.lang.String dstIp, int srcPort, int dstPort, long timestampNs) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(prefix);
          _data.writeInt(uid);
          _data.writeInt(ethertype);
          _data.writeInt(ipNextHeader);
          _data.writeByteArray(dstHw);
          _data.writeString(srcIp);
          _data.writeString(dstIp);
          _data.writeInt(srcPort);
          _data.writeInt(dstPort);
          _data.writeLong(timestampNs);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onWakeupEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onWakeupEvent(prefix, uid, ethertype, ipNextHeader, dstHw, srcIp, dstIp, srcPort, dstPort, timestampNs);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * An event sent after every Netlink sock_diag poll performed by Netd. This reported batch
           * groups TCP socket stats aggregated by network id. Per-network data are stored in a
           * structure-of-arrays style where networkIds, sentPackets, lostPackets, rttUs, and
           * sentAckDiffMs have the same length. Stats for the i-th network is spread across all these
           * arrays at index i.
           * @param networkIds an array of network ids for which there was tcp socket stats to collect in
           *        the last sock_diag poll.
           * @param sentPackets an array of packet sent across all TCP sockets still alive and new
                    TCP sockets since the last sock_diag poll, summed per network id.
           * @param lostPackets, an array of packet lost across all TCP sockets still alive and new
                    TCP sockets since the last sock_diag poll, summed per network id.
           * @param rttUs an array of smoothed round trip times in microseconds, averaged across all TCP
                    sockets since the last sock_diag poll for a given network id.
           * @param sentAckDiffMs an array of milliseconds duration between the last packet sent and the
                    last ack received for a socket, averaged across all TCP sockets for a network id.
           */
      @Override public void onTcpSocketStatsEvent(int[] networkIds, int[] sentPackets, int[] lostPackets, int[] rttUs, int[] sentAckDiffMs) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeIntArray(networkIds);
          _data.writeIntArray(sentPackets);
          _data.writeIntArray(lostPackets);
          _data.writeIntArray(rttUs);
          _data.writeIntArray(sentAckDiffMs);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onTcpSocketStatsEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onTcpSocketStatsEvent(networkIds, sentPackets, lostPackets, rttUs, sentAckDiffMs);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Represents adding or removing a NAT64 prefix.
           *
           * @param netId the ID of the network the prefix was discovered on.
           * @param added true if the NAT64 prefix was added, or false if the NAT64 prefix was removed.
           *        There is only one prefix at a time for each netId. If a prefix is added, it replaces
           *        the previous-added prefix.
           * @param prefixString the detected NAT64 prefix as a string literal.
           * @param prefixLength the prefix length associated with this NAT64 prefix.
           */
      @Override public void onNat64PrefixEvent(int netId, boolean added, java.lang.String prefixString, int prefixLength) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(netId);
          _data.writeInt(((added)?(1):(0)));
          _data.writeString(prefixString);
          _data.writeInt(prefixLength);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onNat64PrefixEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onNat64PrefixEvent(netId, added, prefixString, prefixLength);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override
      public int getInterfaceVersion() throws android.os.RemoteException {
        if (mCachedVersion == -1) {
          android.os.Parcel data = android.os.Parcel.obtain();
          android.os.Parcel reply = android.os.Parcel.obtain();
          try {
            data.writeInterfaceToken(DESCRIPTOR);
            mRemote.transact(Stub.TRANSACTION_getInterfaceVersion, data, reply, 0);
            reply.readException();
            mCachedVersion = reply.readInt();
          } finally {
            reply.recycle();
            data.recycle();
          }
        }
        return mCachedVersion;
      }
      public static android.net.metrics.INetdEventListener sDefaultImpl;
    }
    static final int TRANSACTION_onDnsEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onPrivateDnsValidationEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_onConnectEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_onWakeupEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_onTcpSocketStatsEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_onNat64PrefixEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_getInterfaceVersion = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16777214);
    public static boolean setDefaultImpl(android.net.metrics.INetdEventListener impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.net.metrics.INetdEventListener getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int EVENT_GETADDRINFO = 1;
  public static final int EVENT_GETHOSTBYNAME = 2;
  public static final int EVENT_GETHOSTBYADDR = 3;
  public static final int EVENT_RES_NSEND = 4;
  public static final int REPORTING_LEVEL_NONE = 0;
  public static final int REPORTING_LEVEL_METRICS = 1;
  public static final int REPORTING_LEVEL_FULL = 2;
  public static final int DNS_REPORTED_IP_ADDRESSES_LIMIT = 10;
  /**
       * Logs a DNS lookup function call (getaddrinfo and gethostbyname).
       *
       * @param netId the ID of the network the lookup was performed on.
       * @param eventType one of the EVENT_* constants in this interface.
       * @param returnCode the return value of the function call.
       * @param latencyMs the latency of the function call.
       * @param hostname the name that was looked up.
       * @param ipAddresses (possibly a subset of) the IP addresses returned.
       *        At most {@link #DNS_REPORTED_IP_ADDRESSES_LIMIT} addresses are logged.
       * @param ipAddressesCount the number of IP addresses returned. May be different from the length
       *        of ipAddresses if there were too many addresses to log.
       * @param uid the UID of the application that performed the query.
       */
  public void onDnsEvent(int netId, int eventType, int returnCode, int latencyMs, java.lang.String hostname, java.lang.String[] ipAddresses, int ipAddressesCount, int uid) throws android.os.RemoteException;
  /**
       * Represents a private DNS validation success or failure.
       *
       * @param netId the ID of the network the validation was performed on.
       * @param ipAddress the IP address for which validation was performed.
       * @param hostname the hostname for which validation was performed.
       * @param validated whether or not validation was successful.
       */
  public void onPrivateDnsValidationEvent(int netId, java.lang.String ipAddress, java.lang.String hostname, boolean validated) throws android.os.RemoteException;
  /**
       * Logs a single connect library call.
       *
       * @param netId the ID of the network the connect was performed on.
       * @param error 0 if the connect call succeeded, otherwise errno if it failed.
       * @param latencyMs the latency of the connect call.
       * @param ipAddr destination IP address.
       * @param port destination port number.
       * @param uid the UID of the application that performed the connection.
       */
  public void onConnectEvent(int netId, int error, int latencyMs, java.lang.String ipAddr, int port, int uid) throws android.os.RemoteException;
  /**
       * Logs a single RX packet which caused the main CPU to exit sleep state.
       * @param prefix arbitrary string provided via wakeupAddInterface()
       * @param uid UID of the destination process or -1 if no UID is available.
       * @param ethertype of the RX packet encoded in an int in native order, or -1 if not available.
       * @param ipNextHeader ip protocol of the RX packet as IPPROTO_* number,
                or -1 if the packet was not IPv4 or IPv6.
       * @param dstHw destination hardware address, or 0 if not available.
       * @param srcIp source IP address, or null if not available.
       * @param dstIp destination IP address, or null if not available.
       * @param srcPort src port of RX packet in native order, or -1 if the packet was not UDP or TCP.
       * @param dstPort dst port of RX packet in native order, or -1 if the packet was not UDP or TCP.
       * @param timestampNs receive timestamp for the offending packet. In units of nanoseconds and
       *        synchronized to CLOCK_MONOTONIC.
       */
  public void onWakeupEvent(java.lang.String prefix, int uid, int ethertype, int ipNextHeader, byte[] dstHw, java.lang.String srcIp, java.lang.String dstIp, int srcPort, int dstPort, long timestampNs) throws android.os.RemoteException;
  /**
       * An event sent after every Netlink sock_diag poll performed by Netd. This reported batch
       * groups TCP socket stats aggregated by network id. Per-network data are stored in a
       * structure-of-arrays style where networkIds, sentPackets, lostPackets, rttUs, and
       * sentAckDiffMs have the same length. Stats for the i-th network is spread across all these
       * arrays at index i.
       * @param networkIds an array of network ids for which there was tcp socket stats to collect in
       *        the last sock_diag poll.
       * @param sentPackets an array of packet sent across all TCP sockets still alive and new
                TCP sockets since the last sock_diag poll, summed per network id.
       * @param lostPackets, an array of packet lost across all TCP sockets still alive and new
                TCP sockets since the last sock_diag poll, summed per network id.
       * @param rttUs an array of smoothed round trip times in microseconds, averaged across all TCP
                sockets since the last sock_diag poll for a given network id.
       * @param sentAckDiffMs an array of milliseconds duration between the last packet sent and the
                last ack received for a socket, averaged across all TCP sockets for a network id.
       */
  public void onTcpSocketStatsEvent(int[] networkIds, int[] sentPackets, int[] lostPackets, int[] rttUs, int[] sentAckDiffMs) throws android.os.RemoteException;
  /**
       * Represents adding or removing a NAT64 prefix.
       *
       * @param netId the ID of the network the prefix was discovered on.
       * @param added true if the NAT64 prefix was added, or false if the NAT64 prefix was removed.
       *        There is only one prefix at a time for each netId. If a prefix is added, it replaces
       *        the previous-added prefix.
       * @param prefixString the detected NAT64 prefix as a string literal.
       * @param prefixLength the prefix length associated with this NAT64 prefix.
       */
  public void onNat64PrefixEvent(int netId, boolean added, java.lang.String prefixString, int prefixLength) throws android.os.RemoteException;
  public int getInterfaceVersion() throws android.os.RemoteException;
}
