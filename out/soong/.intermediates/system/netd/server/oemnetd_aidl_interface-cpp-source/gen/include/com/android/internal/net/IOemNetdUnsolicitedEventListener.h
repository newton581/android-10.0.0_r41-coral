#ifndef AIDL_GENERATED_COM_ANDROID_INTERNAL_NET_I_OEM_NETD_UNSOLICITED_EVENT_LISTENER_H_
#define AIDL_GENERATED_COM_ANDROID_INTERNAL_NET_I_OEM_NETD_UNSOLICITED_EVENT_LISTENER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <utils/StrongPointer.h>

namespace com {

namespace android {

namespace internal {

namespace net {

class IOemNetdUnsolicitedEventListener : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(OemNetdUnsolicitedEventListener)
  virtual ::android::binder::Status onRegistered() = 0;
};  // class IOemNetdUnsolicitedEventListener

class IOemNetdUnsolicitedEventListenerDefault : public IOemNetdUnsolicitedEventListener {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onRegistered() override;

};

}  // namespace net

}  // namespace internal

}  // namespace android

}  // namespace com

#endif  // AIDL_GENERATED_COM_ANDROID_INTERNAL_NET_I_OEM_NETD_UNSOLICITED_EVENT_LISTENER_H_
