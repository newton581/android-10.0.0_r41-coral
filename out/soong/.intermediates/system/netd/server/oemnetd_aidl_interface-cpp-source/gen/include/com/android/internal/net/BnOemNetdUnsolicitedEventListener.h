#ifndef AIDL_GENERATED_COM_ANDROID_INTERNAL_NET_BN_OEM_NETD_UNSOLICITED_EVENT_LISTENER_H_
#define AIDL_GENERATED_COM_ANDROID_INTERNAL_NET_BN_OEM_NETD_UNSOLICITED_EVENT_LISTENER_H_

#include <binder/IInterface.h>
#include <com/android/internal/net/IOemNetdUnsolicitedEventListener.h>

namespace com {

namespace android {

namespace internal {

namespace net {

class BnOemNetdUnsolicitedEventListener : public ::android::BnInterface<IOemNetdUnsolicitedEventListener> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnOemNetdUnsolicitedEventListener

}  // namespace net

}  // namespace internal

}  // namespace android

}  // namespace com

#endif  // AIDL_GENERATED_COM_ANDROID_INTERNAL_NET_BN_OEM_NETD_UNSOLICITED_EVENT_LISTENER_H_
