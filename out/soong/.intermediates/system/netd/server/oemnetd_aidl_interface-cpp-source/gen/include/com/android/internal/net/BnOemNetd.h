#ifndef AIDL_GENERATED_COM_ANDROID_INTERNAL_NET_BN_OEM_NETD_H_
#define AIDL_GENERATED_COM_ANDROID_INTERNAL_NET_BN_OEM_NETD_H_

#include <binder/IInterface.h>
#include <com/android/internal/net/IOemNetd.h>

namespace com {

namespace android {

namespace internal {

namespace net {

class BnOemNetd : public ::android::BnInterface<IOemNetd> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnOemNetd

}  // namespace net

}  // namespace internal

}  // namespace android

}  // namespace com

#endif  // AIDL_GENERATED_COM_ANDROID_INTERNAL_NET_BN_OEM_NETD_H_
