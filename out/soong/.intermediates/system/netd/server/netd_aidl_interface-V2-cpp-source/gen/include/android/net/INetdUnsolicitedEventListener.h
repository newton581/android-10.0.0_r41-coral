#ifndef AIDL_GENERATED_ANDROID_NET_I_NETD_UNSOLICITED_EVENT_LISTENER_H_
#define AIDL_GENERATED_ANDROID_NET_I_NETD_UNSOLICITED_EVENT_LISTENER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace net {

class INetdUnsolicitedEventListener : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(NetdUnsolicitedEventListener)
  const int32_t VERSION = 2;
  virtual ::android::binder::Status onInterfaceClassActivityChanged(bool isActive, int32_t timerLabel, int64_t timestampNs, int32_t uid) = 0;
  virtual ::android::binder::Status onQuotaLimitReached(const ::std::string& alertName, const ::std::string& ifName) = 0;
  virtual ::android::binder::Status onInterfaceDnsServerInfo(const ::std::string& ifName, int64_t lifetimeS, const ::std::vector<::std::string>& servers) = 0;
  virtual ::android::binder::Status onInterfaceAddressUpdated(const ::std::string& addr, const ::std::string& ifName, int32_t flags, int32_t scope) = 0;
  virtual ::android::binder::Status onInterfaceAddressRemoved(const ::std::string& addr, const ::std::string& ifName, int32_t flags, int32_t scope) = 0;
  virtual ::android::binder::Status onInterfaceAdded(const ::std::string& ifName) = 0;
  virtual ::android::binder::Status onInterfaceRemoved(const ::std::string& ifName) = 0;
  virtual ::android::binder::Status onInterfaceChanged(const ::std::string& ifName, bool up) = 0;
  virtual ::android::binder::Status onInterfaceLinkStateChanged(const ::std::string& ifName, bool up) = 0;
  virtual ::android::binder::Status onRouteChanged(bool updated, const ::std::string& route, const ::std::string& gateway, const ::std::string& ifName) = 0;
  virtual ::android::binder::Status onStrictCleartextDetected(int32_t uid, const ::std::string& hex) = 0;
  virtual int32_t getInterfaceVersion() = 0;
};  // class INetdUnsolicitedEventListener

class INetdUnsolicitedEventListenerDefault : public INetdUnsolicitedEventListener {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onInterfaceClassActivityChanged(bool isActive, int32_t timerLabel, int64_t timestampNs, int32_t uid) override;
  ::android::binder::Status onQuotaLimitReached(const ::std::string& alertName, const ::std::string& ifName) override;
  ::android::binder::Status onInterfaceDnsServerInfo(const ::std::string& ifName, int64_t lifetimeS, const ::std::vector<::std::string>& servers) override;
  ::android::binder::Status onInterfaceAddressUpdated(const ::std::string& addr, const ::std::string& ifName, int32_t flags, int32_t scope) override;
  ::android::binder::Status onInterfaceAddressRemoved(const ::std::string& addr, const ::std::string& ifName, int32_t flags, int32_t scope) override;
  ::android::binder::Status onInterfaceAdded(const ::std::string& ifName) override;
  ::android::binder::Status onInterfaceRemoved(const ::std::string& ifName) override;
  ::android::binder::Status onInterfaceChanged(const ::std::string& ifName, bool up) override;
  ::android::binder::Status onInterfaceLinkStateChanged(const ::std::string& ifName, bool up) override;
  ::android::binder::Status onRouteChanged(bool updated, const ::std::string& route, const ::std::string& gateway, const ::std::string& ifName) override;
  ::android::binder::Status onStrictCleartextDetected(int32_t uid, const ::std::string& hex) override;
  int32_t getInterfaceVersion() override;

};

}  // namespace net

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_NET_I_NETD_UNSOLICITED_EVENT_LISTENER_H_
