#ifndef AIDL_GENERATED_ANDROID_NET_TETHER_STATS_PARCEL_H_
#define AIDL_GENERATED_ANDROID_NET_TETHER_STATS_PARCEL_H_

#include <binder/Parcel.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>

namespace android {

namespace net {

class TetherStatsParcel : public ::android::Parcelable {
public:
  ::std::string iface;
  int64_t rxBytes;
  int64_t rxPackets;
  int64_t txBytes;
  int64_t txPackets;
  ::android::status_t readFromParcel(const ::android::Parcel* _aidl_parcel) override final;
  ::android::status_t writeToParcel(::android::Parcel* _aidl_parcel) const override final;
};  // class TetherStatsParcel

}  // namespace net

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_NET_TETHER_STATS_PARCEL_H_
