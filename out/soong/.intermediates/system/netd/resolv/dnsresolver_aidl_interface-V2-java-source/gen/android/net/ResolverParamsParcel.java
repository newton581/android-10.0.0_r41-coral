/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.net;
public class ResolverParamsParcel implements android.os.Parcelable
{

  public int netId;

  public int sampleValiditySeconds;

  public int successThreshold;

  public int minSamples;

  public int maxSamples;

  public int baseTimeoutMsec;

  public int retryCount;

  public java.lang.String[] servers;

  public java.lang.String[] domains;

  public java.lang.String tlsName;

  public java.lang.String[] tlsServers;

  public java.lang.String[] tlsFingerprints;
  public static final android.os.Parcelable.Creator<ResolverParamsParcel> CREATOR = new android.os.Parcelable.Creator<ResolverParamsParcel>() {
    @Override
    public ResolverParamsParcel createFromParcel(android.os.Parcel _aidl_source) {
      ResolverParamsParcel _aidl_out = new ResolverParamsParcel();
      _aidl_out.readFromParcel(_aidl_source);
      return _aidl_out;
    }
    @Override
    public ResolverParamsParcel[] newArray(int _aidl_size) {
      return new ResolverParamsParcel[_aidl_size];
    }
  };
  @Override public final void writeToParcel(android.os.Parcel _aidl_parcel, int _aidl_flag)
  {
    int _aidl_start_pos = _aidl_parcel.dataPosition();
    _aidl_parcel.writeInt(0);
    _aidl_parcel.writeInt(netId);
    _aidl_parcel.writeInt(sampleValiditySeconds);
    _aidl_parcel.writeInt(successThreshold);
    _aidl_parcel.writeInt(minSamples);
    _aidl_parcel.writeInt(maxSamples);
    _aidl_parcel.writeInt(baseTimeoutMsec);
    _aidl_parcel.writeInt(retryCount);
    _aidl_parcel.writeStringArray(servers);
    _aidl_parcel.writeStringArray(domains);
    _aidl_parcel.writeString(tlsName);
    _aidl_parcel.writeStringArray(tlsServers);
    _aidl_parcel.writeStringArray(tlsFingerprints);
    int _aidl_end_pos = _aidl_parcel.dataPosition();
    _aidl_parcel.setDataPosition(_aidl_start_pos);
    _aidl_parcel.writeInt(_aidl_end_pos - _aidl_start_pos);
    _aidl_parcel.setDataPosition(_aidl_end_pos);
  }
  public final void readFromParcel(android.os.Parcel _aidl_parcel)
  {
    int _aidl_start_pos = _aidl_parcel.dataPosition();
    int _aidl_parcelable_size = _aidl_parcel.readInt();
    if (_aidl_parcelable_size < 0) return;
    try {
      netId = _aidl_parcel.readInt();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      sampleValiditySeconds = _aidl_parcel.readInt();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      successThreshold = _aidl_parcel.readInt();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      minSamples = _aidl_parcel.readInt();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      maxSamples = _aidl_parcel.readInt();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      baseTimeoutMsec = _aidl_parcel.readInt();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      retryCount = _aidl_parcel.readInt();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      servers = _aidl_parcel.createStringArray();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      domains = _aidl_parcel.createStringArray();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      tlsName = _aidl_parcel.readString();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      tlsServers = _aidl_parcel.createStringArray();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      tlsFingerprints = _aidl_parcel.createStringArray();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
    } finally {
      _aidl_parcel.setDataPosition(_aidl_start_pos + _aidl_parcelable_size);
    }
  }
  @Override public int describeContents()
  {
    return 0;
  }
}
