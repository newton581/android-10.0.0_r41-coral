#ifndef AIDL_GENERATED_ANDROID_NET_BN_DNS_RESOLVER_H_
#define AIDL_GENERATED_ANDROID_NET_BN_DNS_RESOLVER_H_

#include <binder/IInterface.h>
#include <android/net/IDnsResolver.h>

namespace android {

namespace net {

class BnDnsResolver : public ::android::BnInterface<IDnsResolver> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
  int32_t getInterfaceVersion() final override;
};  // class BnDnsResolver

}  // namespace net

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_NET_BN_DNS_RESOLVER_H_
