#ifndef AIDL_GENERATED_ANDROID_NET_RESOLVER_PARAMS_PARCEL_H_
#define AIDL_GENERATED_ANDROID_NET_RESOLVER_PARAMS_PARCEL_H_

#include <binder/Parcel.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>
#include <vector>

namespace android {

namespace net {

class ResolverParamsParcel : public ::android::Parcelable {
public:
  int32_t netId;
  int32_t sampleValiditySeconds;
  int32_t successThreshold;
  int32_t minSamples;
  int32_t maxSamples;
  int32_t baseTimeoutMsec;
  int32_t retryCount;
  ::std::vector<::std::string> servers;
  ::std::vector<::std::string> domains;
  ::std::string tlsName;
  ::std::vector<::std::string> tlsServers;
  ::std::vector<::std::string> tlsFingerprints;
  ::android::status_t readFromParcel(const ::android::Parcel* _aidl_parcel) override final;
  ::android::status_t writeToParcel(::android::Parcel* _aidl_parcel) const override final;
};  // class ResolverParamsParcel

}  // namespace net

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_NET_RESOLVER_PARAMS_PARCEL_H_
