#pragma once

#include "aidl/android/net/IDnsResolver.h"

#include <android/binder_ibinder.h>
#include <json/value.h>
#include <functional>
#include <chrono>
#include <sstream>

namespace aidl {
namespace android {
namespace net {
class BpDnsResolver : public ::ndk::BpCInterface<IDnsResolver> {
public:
  BpDnsResolver(const ::ndk::SpAIBinder& binder);
  virtual ~BpDnsResolver();

  ::ndk::ScopedAStatus isAlive(bool* _aidl_return) override;
  ::ndk::ScopedAStatus registerEventListener(const std::shared_ptr<::aidl::android::net::metrics::INetdEventListener>& in_listener) override;
  ::ndk::ScopedAStatus setResolverConfiguration(const ::aidl::android::net::ResolverParamsParcel& in_resolverParams) override;
  ::ndk::ScopedAStatus getResolverInfo(int32_t in_netId, std::vector<std::string>* out_servers, std::vector<std::string>* out_domains, std::vector<std::string>* out_tlsServers, std::vector<int32_t>* out_params, std::vector<int32_t>* out_stats, std::vector<int32_t>* out_wait_for_pending_req_timeout_count) override;
  ::ndk::ScopedAStatus startPrefix64Discovery(int32_t in_netId) override;
  ::ndk::ScopedAStatus stopPrefix64Discovery(int32_t in_netId) override;
  ::ndk::ScopedAStatus getPrefix64(int32_t in_netId, std::string* _aidl_return) override;
  ::ndk::ScopedAStatus createNetworkCache(int32_t in_netId) override;
  ::ndk::ScopedAStatus destroyNetworkCache(int32_t in_netId) override;
  ::ndk::ScopedAStatus setLogSeverity(int32_t in_logSeverity) override;
  ::ndk::ScopedAStatus getInterfaceVersion(int32_t* _aidl_return) override;
  int32_t _aidl_cached_value = -1;
  static std::function<void(const Json::Value&)> logFunc;
};
}  // namespace net
}  // namespace android
}  // namespace aidl
