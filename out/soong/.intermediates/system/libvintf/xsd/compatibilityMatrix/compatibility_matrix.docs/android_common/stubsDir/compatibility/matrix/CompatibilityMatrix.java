
package compatibility.matrix;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class CompatibilityMatrix {

public CompatibilityMatrix() { throw new RuntimeException("Stub!"); }

public java.util.List<compatibility.matrix.Hal> getHal() { throw new RuntimeException("Stub!"); }

public java.util.List<compatibility.matrix.Kernel> getKernel() { throw new RuntimeException("Stub!"); }

public compatibility.matrix.Sepolicy getSepolicy() { throw new RuntimeException("Stub!"); }

public void setSepolicy(compatibility.matrix.Sepolicy sepolicy) { throw new RuntimeException("Stub!"); }

public compatibility.matrix.Avb getAvb() { throw new RuntimeException("Stub!"); }

public void setAvb(compatibility.matrix.Avb avb) { throw new RuntimeException("Stub!"); }

public compatibility.matrix.Vndk getVndk() { throw new RuntimeException("Stub!"); }

public void setVndk(compatibility.matrix.Vndk vndk) { throw new RuntimeException("Stub!"); }

public compatibility.matrix.VendorNdk getVendorNdk() { throw new RuntimeException("Stub!"); }

public void setVendorNdk(compatibility.matrix.VendorNdk vendorNdk) { throw new RuntimeException("Stub!"); }

public compatibility.matrix.SystemSdk getSystemSdk() { throw new RuntimeException("Stub!"); }

public void setSystemSdk(compatibility.matrix.SystemSdk systemSdk) { throw new RuntimeException("Stub!"); }

public java.util.List<compatibility.matrix.Xmlfile> getXmlfile() { throw new RuntimeException("Stub!"); }

public java.lang.String getVersion() { throw new RuntimeException("Stub!"); }

public void setVersion(java.lang.String version) { throw new RuntimeException("Stub!"); }

public java.lang.String getType() { throw new RuntimeException("Stub!"); }

public void setType(java.lang.String type) { throw new RuntimeException("Stub!"); }

public java.lang.String getLevel() { throw new RuntimeException("Stub!"); }

public void setLevel(java.lang.String level) { throw new RuntimeException("Stub!"); }
}

