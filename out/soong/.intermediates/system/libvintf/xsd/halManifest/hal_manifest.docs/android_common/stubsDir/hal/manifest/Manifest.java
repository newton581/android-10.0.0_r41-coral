
package hal.manifest;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Manifest {

public Manifest() { throw new RuntimeException("Stub!"); }

public java.util.List<hal.manifest.Hal> getHal() { throw new RuntimeException("Stub!"); }

public hal.manifest.Sepolicy getSepolicy() { throw new RuntimeException("Stub!"); }

public void setSepolicy(hal.manifest.Sepolicy sepolicy) { throw new RuntimeException("Stub!"); }

public hal.manifest.Kernel getKernel() { throw new RuntimeException("Stub!"); }

public void setKernel(hal.manifest.Kernel kernel) { throw new RuntimeException("Stub!"); }

public java.util.List<hal.manifest.Vndk> getVndk() { throw new RuntimeException("Stub!"); }

public java.util.List<hal.manifest.VendorNdk> getVendorNdk() { throw new RuntimeException("Stub!"); }

public hal.manifest.SystemSdk getSystemSdk() { throw new RuntimeException("Stub!"); }

public void setSystemSdk(hal.manifest.SystemSdk systemSdk) { throw new RuntimeException("Stub!"); }

public java.lang.String getVersion() { throw new RuntimeException("Stub!"); }

public void setVersion(java.lang.String version) { throw new RuntimeException("Stub!"); }

public java.lang.String getType() { throw new RuntimeException("Stub!"); }

public void setType(java.lang.String type) { throw new RuntimeException("Stub!"); }

public java.lang.String getTargetLevel() { throw new RuntimeException("Stub!"); }

public void setTargetLevel(java.lang.String targetLevel) { throw new RuntimeException("Stub!"); }
}

