
package hal.manifest;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Hal {

public Hal() { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public hal.manifest.Hal.Transport getTransport() { throw new RuntimeException("Stub!"); }

public void setTransport(hal.manifest.Hal.Transport transport) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.String> getVersion() { throw new RuntimeException("Stub!"); }

public java.util.List<hal.manifest.Interface> get_interface() { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.String> getFqname() { throw new RuntimeException("Stub!"); }

public java.lang.String getFormat() { throw new RuntimeException("Stub!"); }

public void setFormat(java.lang.String format) { throw new RuntimeException("Stub!"); }

public java.lang.String getOverride() { throw new RuntimeException("Stub!"); }

public void setOverride(java.lang.String override) { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Transport {

public Transport() { throw new RuntimeException("Stub!"); }

public java.lang.String getArch() { throw new RuntimeException("Stub!"); }

public void setArch(java.lang.String arch) { throw new RuntimeException("Stub!"); }

public java.lang.String getValue() { throw new RuntimeException("Stub!"); }

public void setValue(java.lang.String value) { throw new RuntimeException("Stub!"); }
}

}

