#ifndef AIDL_GENERATED_ANDROID_ASHMEMD_BN_ASHMEM_DEVICE_SERVICE_H_
#define AIDL_GENERATED_ANDROID_ASHMEMD_BN_ASHMEM_DEVICE_SERVICE_H_

#include <binder/IInterface.h>
#include <android/ashmemd/IAshmemDeviceService.h>

namespace android {

namespace ashmemd {

class BnAshmemDeviceService : public ::android::BnInterface<IAshmemDeviceService> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnAshmemDeviceService

}  // namespace ashmemd

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_ASHMEMD_BN_ASHMEM_DEVICE_SERVICE_H_
