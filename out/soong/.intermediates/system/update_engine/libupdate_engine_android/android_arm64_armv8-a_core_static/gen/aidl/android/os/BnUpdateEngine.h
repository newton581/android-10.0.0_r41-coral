#ifndef AIDL_GENERATED_ANDROID_OS_BN_UPDATE_ENGINE_H_
#define AIDL_GENERATED_ANDROID_OS_BN_UPDATE_ENGINE_H_

#include <binder/IInterface.h>
#include <android/os/IUpdateEngine.h>

namespace android {

namespace os {

class BnUpdateEngine : public ::android::BnInterface<IUpdateEngine> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnUpdateEngine

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_BN_UPDATE_ENGINE_H_
