#ifndef AIDL_GENERATED_ANDROID_OS_BN_VOLD_H_
#define AIDL_GENERATED_ANDROID_OS_BN_VOLD_H_

#include <binder/IInterface.h>
#include <android/os/IVold.h>

namespace android {

namespace os {

class BnVold : public ::android::BnInterface<IVold> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnVold

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_BN_VOLD_H_
