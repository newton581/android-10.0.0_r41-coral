#ifndef AIDL_GENERATED_ANDROID_OS_STORAGED_BN_STORAGED_PRIVATE_H_
#define AIDL_GENERATED_ANDROID_OS_STORAGED_BN_STORAGED_PRIVATE_H_

#include <binder/IInterface.h>
#include <android/os/storaged/IStoragedPrivate.h>

namespace android {

namespace os {

namespace storaged {

class BnStoragedPrivate : public ::android::BnInterface<IStoragedPrivate> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnStoragedPrivate

}  // namespace storaged

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_STORAGED_BN_STORAGED_PRIVATE_H_
