#ifndef AIDL_GENERATED_COM_GOOGLE_ANDROID_STARTOP_IORAP_I_IORAP_H_
#define AIDL_GENERATED_COM_GOOGLE_ANDROID_STARTOP_IORAP_I_IORAP_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <binder/app_intent_event.h>
#include <binder/app_launch_event.h>
#include <binder/package_event.h>
#include <binder/request_id.h>
#include <binder/system_service_event.h>
#include <binder/system_service_user_event.h>
#include <com/google/android/startop/iorap/ITaskListener.h>
#include <utils/StrongPointer.h>

namespace com {

namespace google {

namespace android {

namespace startop {

namespace iorap {

class IIorap : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(Iorap)
  virtual ::android::binder::Status setTaskListener(const ::android::sp<::com::google::android::startop::iorap::ITaskListener>& listener) = 0;
  virtual ::android::binder::Status onAppLaunchEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::AppLaunchEvent& event) = 0;
  virtual ::android::binder::Status onPackageEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::PackageEvent& event) = 0;
  virtual ::android::binder::Status onAppIntentEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::AppIntentEvent& event) = 0;
  virtual ::android::binder::Status onSystemServiceEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::SystemServiceEvent& event) = 0;
  virtual ::android::binder::Status onSystemServiceUserEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::SystemServiceUserEvent& event) = 0;
};  // class IIorap

class IIorapDefault : public IIorap {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status setTaskListener(const ::android::sp<::com::google::android::startop::iorap::ITaskListener>& listener) override;
  ::android::binder::Status onAppLaunchEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::AppLaunchEvent& event) override;
  ::android::binder::Status onPackageEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::PackageEvent& event) override;
  ::android::binder::Status onAppIntentEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::AppIntentEvent& event) override;
  ::android::binder::Status onSystemServiceEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::SystemServiceEvent& event) override;
  ::android::binder::Status onSystemServiceUserEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::SystemServiceUserEvent& event) override;

};

}  // namespace iorap

}  // namespace startop

}  // namespace android

}  // namespace google

}  // namespace com

#endif  // AIDL_GENERATED_COM_GOOGLE_ANDROID_STARTOP_IORAP_I_IORAP_H_
