#include <com/google/android/startop/iorap/IIorap.h>
#include <com/google/android/startop/iorap/BpIorap.h>

namespace com {

namespace google {

namespace android {

namespace startop {

namespace iorap {

IMPLEMENT_META_INTERFACE(Iorap, "com.google.android.startop.iorap.IIorap")

::android::IBinder* IIorapDefault::onAsBinder() {
  return nullptr;
}

::android::binder::Status IIorapDefault::setTaskListener(const ::android::sp<::com::google::android::startop::iorap::ITaskListener>&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

::android::binder::Status IIorapDefault::onAppLaunchEvent(const ::com::google::android::startop::iorap::RequestId&, const ::com::google::android::startop::iorap::AppLaunchEvent&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

::android::binder::Status IIorapDefault::onPackageEvent(const ::com::google::android::startop::iorap::RequestId&, const ::com::google::android::startop::iorap::PackageEvent&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

::android::binder::Status IIorapDefault::onAppIntentEvent(const ::com::google::android::startop::iorap::RequestId&, const ::com::google::android::startop::iorap::AppIntentEvent&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

::android::binder::Status IIorapDefault::onSystemServiceEvent(const ::com::google::android::startop::iorap::RequestId&, const ::com::google::android::startop::iorap::SystemServiceEvent&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

::android::binder::Status IIorapDefault::onSystemServiceUserEvent(const ::com::google::android::startop::iorap::RequestId&, const ::com::google::android::startop::iorap::SystemServiceUserEvent&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

}  // namespace iorap

}  // namespace startop

}  // namespace android

}  // namespace google

}  // namespace com
#include <com/google/android/startop/iorap/BpIorap.h>
#include <binder/Parcel.h>
#include <android-base/macros.h>

namespace com {

namespace google {

namespace android {

namespace startop {

namespace iorap {

BpIorap::BpIorap(const ::android::sp<::android::IBinder>& _aidl_impl)
    : BpInterface<IIorap>(_aidl_impl){
}

::android::binder::Status BpIorap::setTaskListener(const ::android::sp<::com::google::android::startop::iorap::ITaskListener>& listener) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeStrongBinder(::com::google::android::startop::iorap::ITaskListener::asBinder(listener));
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* setTaskListener */, _aidl_data, &_aidl_reply, ::android::IBinder::FLAG_ONEWAY);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IIorap::getDefaultImpl())) {
     return IIorap::getDefaultImpl()->setTaskListener(listener);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

::android::binder::Status BpIorap::onAppLaunchEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::AppLaunchEvent& event) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(request);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(event);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 1 /* onAppLaunchEvent */, _aidl_data, &_aidl_reply, ::android::IBinder::FLAG_ONEWAY);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IIorap::getDefaultImpl())) {
     return IIorap::getDefaultImpl()->onAppLaunchEvent(request, event);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

::android::binder::Status BpIorap::onPackageEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::PackageEvent& event) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(request);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(event);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 2 /* onPackageEvent */, _aidl_data, &_aidl_reply, ::android::IBinder::FLAG_ONEWAY);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IIorap::getDefaultImpl())) {
     return IIorap::getDefaultImpl()->onPackageEvent(request, event);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

::android::binder::Status BpIorap::onAppIntentEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::AppIntentEvent& event) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(request);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(event);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 3 /* onAppIntentEvent */, _aidl_data, &_aidl_reply, ::android::IBinder::FLAG_ONEWAY);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IIorap::getDefaultImpl())) {
     return IIorap::getDefaultImpl()->onAppIntentEvent(request, event);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

::android::binder::Status BpIorap::onSystemServiceEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::SystemServiceEvent& event) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(request);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(event);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 4 /* onSystemServiceEvent */, _aidl_data, &_aidl_reply, ::android::IBinder::FLAG_ONEWAY);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IIorap::getDefaultImpl())) {
     return IIorap::getDefaultImpl()->onSystemServiceEvent(request, event);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

::android::binder::Status BpIorap::onSystemServiceUserEvent(const ::com::google::android::startop::iorap::RequestId& request, const ::com::google::android::startop::iorap::SystemServiceUserEvent& event) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(request);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(event);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 5 /* onSystemServiceUserEvent */, _aidl_data, &_aidl_reply, ::android::IBinder::FLAG_ONEWAY);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IIorap::getDefaultImpl())) {
     return IIorap::getDefaultImpl()->onSystemServiceUserEvent(request, event);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

}  // namespace iorap

}  // namespace startop

}  // namespace android

}  // namespace google

}  // namespace com
#include <com/google/android/startop/iorap/BnIorap.h>
#include <binder/Parcel.h>

namespace com {

namespace google {

namespace android {

namespace startop {

namespace iorap {

::android::status_t BnIorap::onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) {
  ::android::status_t _aidl_ret_status = ::android::OK;
  switch (_aidl_code) {
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* setTaskListener */:
  {
    ::android::sp<::com::google::android::startop::iorap::ITaskListener> in_listener;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readStrongBinder(&in_listener);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(setTaskListener(in_listener));
  }
  break;
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 1 /* onAppLaunchEvent */:
  {
    ::com::google::android::startop::iorap::RequestId in_request;
    ::com::google::android::startop::iorap::AppLaunchEvent in_event;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_request);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_event);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(onAppLaunchEvent(in_request, in_event));
  }
  break;
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 2 /* onPackageEvent */:
  {
    ::com::google::android::startop::iorap::RequestId in_request;
    ::com::google::android::startop::iorap::PackageEvent in_event;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_request);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_event);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(onPackageEvent(in_request, in_event));
  }
  break;
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 3 /* onAppIntentEvent */:
  {
    ::com::google::android::startop::iorap::RequestId in_request;
    ::com::google::android::startop::iorap::AppIntentEvent in_event;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_request);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_event);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(onAppIntentEvent(in_request, in_event));
  }
  break;
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 4 /* onSystemServiceEvent */:
  {
    ::com::google::android::startop::iorap::RequestId in_request;
    ::com::google::android::startop::iorap::SystemServiceEvent in_event;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_request);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_event);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(onSystemServiceEvent(in_request, in_event));
  }
  break;
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 5 /* onSystemServiceUserEvent */:
  {
    ::com::google::android::startop::iorap::RequestId in_request;
    ::com::google::android::startop::iorap::SystemServiceUserEvent in_event;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_request);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_event);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(onSystemServiceUserEvent(in_request, in_event));
  }
  break;
  default:
  {
    _aidl_ret_status = ::android::BBinder::onTransact(_aidl_code, _aidl_data, _aidl_reply, _aidl_flags);
  }
  break;
  }
  if (_aidl_ret_status == ::android::UNEXPECTED_NULL) {
    _aidl_ret_status = ::android::binder::Status::fromExceptionCode(::android::binder::Status::EX_NULL_POINTER).writeToParcel(_aidl_reply);
  }
  return _aidl_ret_status;
}

}  // namespace iorap

}  // namespace startop

}  // namespace android

}  // namespace google

}  // namespace com
