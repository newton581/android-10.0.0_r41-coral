
package predefined.types;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DateTypes {

public DateTypes() { throw new RuntimeException("Stub!"); }

public javax.xml.datatype.XMLGregorianCalendar getDate() { throw new RuntimeException("Stub!"); }

public void setDate(javax.xml.datatype.XMLGregorianCalendar date) { throw new RuntimeException("Stub!"); }

public javax.xml.datatype.XMLGregorianCalendar getDateTime() { throw new RuntimeException("Stub!"); }

public void setDateTime(javax.xml.datatype.XMLGregorianCalendar dateTime) { throw new RuntimeException("Stub!"); }

public javax.xml.datatype.Duration getDuration() { throw new RuntimeException("Stub!"); }

public void setDuration(javax.xml.datatype.Duration duration) { throw new RuntimeException("Stub!"); }

public javax.xml.datatype.XMLGregorianCalendar getGDay() { throw new RuntimeException("Stub!"); }

public void setGDay(javax.xml.datatype.XMLGregorianCalendar gDay) { throw new RuntimeException("Stub!"); }

public javax.xml.datatype.XMLGregorianCalendar getGMonth() { throw new RuntimeException("Stub!"); }

public void setGMonth(javax.xml.datatype.XMLGregorianCalendar gMonth) { throw new RuntimeException("Stub!"); }

public javax.xml.datatype.XMLGregorianCalendar getGMonthDay() { throw new RuntimeException("Stub!"); }

public void setGMonthDay(javax.xml.datatype.XMLGregorianCalendar gMonthDay) { throw new RuntimeException("Stub!"); }

public javax.xml.datatype.XMLGregorianCalendar getGYear() { throw new RuntimeException("Stub!"); }

public void setGYear(javax.xml.datatype.XMLGregorianCalendar gYear) { throw new RuntimeException("Stub!"); }

public javax.xml.datatype.XMLGregorianCalendar getGYearMonth() { throw new RuntimeException("Stub!"); }

public void setGYearMonth(javax.xml.datatype.XMLGregorianCalendar gYearMonth) { throw new RuntimeException("Stub!"); }

public javax.xml.datatype.XMLGregorianCalendar getTime() { throw new RuntimeException("Stub!"); }

public void setTime(javax.xml.datatype.XMLGregorianCalendar time) { throw new RuntimeException("Stub!"); }
}

