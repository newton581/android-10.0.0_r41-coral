
package predefined.types;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class StringTypes {

public StringTypes() { throw new RuntimeException("Stub!"); }

public java.lang.String getString() { throw new RuntimeException("Stub!"); }

public void setString(java.lang.String string) { throw new RuntimeException("Stub!"); }

public java.lang.String getToken() { throw new RuntimeException("Stub!"); }

public void setToken(java.lang.String token) { throw new RuntimeException("Stub!"); }

public java.lang.String getNormalizedString() { throw new RuntimeException("Stub!"); }

public void setNormalizedString(java.lang.String normalizedString) { throw new RuntimeException("Stub!"); }

public java.lang.String getLanguage() { throw new RuntimeException("Stub!"); }

public void setLanguage(java.lang.String language) { throw new RuntimeException("Stub!"); }

public java.lang.String getEntity() { throw new RuntimeException("Stub!"); }

public void setEntity(java.lang.String entity) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.String> getEntities() { throw new RuntimeException("Stub!"); }

public void setEntities(java.util.List<java.lang.String> entities) { throw new RuntimeException("Stub!"); }

public java.lang.String getId() { throw new RuntimeException("Stub!"); }

public void setId(java.lang.String id) { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public java.lang.String getNcname() { throw new RuntimeException("Stub!"); }

public void setNcname(java.lang.String ncname) { throw new RuntimeException("Stub!"); }

public java.lang.String getNmtoken() { throw new RuntimeException("Stub!"); }

public void setNmtoken(java.lang.String nmtoken) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.String> getNmtokens() { throw new RuntimeException("Stub!"); }

public void setNmtokens(java.util.List<java.lang.String> nmtokens) { throw new RuntimeException("Stub!"); }
}

