
package nested.type;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Employee {

public Employee() { throw new RuntimeException("Stub!"); }

public byte getId() { throw new RuntimeException("Stub!"); }

public void setId(byte id) { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public nested.type.Employee.Address getAddress() { throw new RuntimeException("Stub!"); }

public void setAddress(nested.type.Employee.Address address) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.String> getAssets() { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Address {

public Address() { throw new RuntimeException("Stub!"); }

public java.lang.String getCountry() { throw new RuntimeException("Stub!"); }

public void setCountry(java.lang.String country) { throw new RuntimeException("Stub!"); }

public java.lang.String getState() { throw new RuntimeException("Stub!"); }

public void setState(java.lang.String state) { throw new RuntimeException("Stub!"); }

public short getZip() { throw new RuntimeException("Stub!"); }

public void setZip(short zip) { throw new RuntimeException("Stub!"); }

public nested.type.Employee.Address.Extra getExtra() { throw new RuntimeException("Stub!"); }

public void setExtra(nested.type.Employee.Address.Extra extra) { throw new RuntimeException("Stub!"); }

public nested.type.Employee.Address.ExtraAddress getExtra_address() { throw new RuntimeException("Stub!"); }

public void setExtra_address(nested.type.Employee.Address.ExtraAddress extra_address) { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Extra {

public Extra() { throw new RuntimeException("Stub!"); }

public java.lang.String getLine1() { throw new RuntimeException("Stub!"); }

public void setLine1(java.lang.String line1) { throw new RuntimeException("Stub!"); }

public java.lang.String getLine2() { throw new RuntimeException("Stub!"); }

public void setLine2(java.lang.String line2) { throw new RuntimeException("Stub!"); }
}

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ExtraAddress {

public ExtraAddress() { throw new RuntimeException("Stub!"); }

public java.lang.String getLine1_all() { throw new RuntimeException("Stub!"); }

public void setLine1_all(java.lang.String line1_all) { throw new RuntimeException("Stub!"); }

public java.lang.String getLine2_all() { throw new RuntimeException("Stub!"); }

public void setLine2_all(java.lang.String line2_all) { throw new RuntimeException("Stub!"); }
}

}

}

