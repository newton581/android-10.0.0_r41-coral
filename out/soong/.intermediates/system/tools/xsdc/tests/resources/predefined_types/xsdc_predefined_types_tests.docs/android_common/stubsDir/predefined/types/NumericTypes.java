
package predefined.types;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class NumericTypes {

public NumericTypes() { throw new RuntimeException("Stub!"); }

public java.math.BigDecimal getDecimal() { throw new RuntimeException("Stub!"); }

public void setDecimal(java.math.BigDecimal decimal) { throw new RuntimeException("Stub!"); }

public java.math.BigInteger getInteger() { throw new RuntimeException("Stub!"); }

public void setInteger(java.math.BigInteger integer) { throw new RuntimeException("Stub!"); }

public long get_long() { throw new RuntimeException("Stub!"); }

public void set_long(long _long) { throw new RuntimeException("Stub!"); }

public int get_int() { throw new RuntimeException("Stub!"); }

public void set_int(int _int) { throw new RuntimeException("Stub!"); }

public short get_short() { throw new RuntimeException("Stub!"); }

public void set_short(short _short) { throw new RuntimeException("Stub!"); }

public byte get_byte() { throw new RuntimeException("Stub!"); }

public void set_byte(byte _byte) { throw new RuntimeException("Stub!"); }

public java.math.BigInteger getNegativeInteger() { throw new RuntimeException("Stub!"); }

public void setNegativeInteger(java.math.BigInteger negativeInteger) { throw new RuntimeException("Stub!"); }

public java.math.BigInteger getNonNegativeInteger() { throw new RuntimeException("Stub!"); }

public void setNonNegativeInteger(java.math.BigInteger nonNegativeInteger) { throw new RuntimeException("Stub!"); }

public java.math.BigInteger getPositiveInteger() { throw new RuntimeException("Stub!"); }

public void setPositiveInteger(java.math.BigInteger positiveInteger) { throw new RuntimeException("Stub!"); }

public java.math.BigInteger getNonPositiveInteger() { throw new RuntimeException("Stub!"); }

public void setNonPositiveInteger(java.math.BigInteger nonPositiveInteger) { throw new RuntimeException("Stub!"); }

public java.math.BigInteger getUnsignedLong() { throw new RuntimeException("Stub!"); }

public void setUnsignedLong(java.math.BigInteger unsignedLong) { throw new RuntimeException("Stub!"); }

public long getUnsignedInt() { throw new RuntimeException("Stub!"); }

public void setUnsignedInt(long unsignedInt) { throw new RuntimeException("Stub!"); }

public int getUnsignedShort() { throw new RuntimeException("Stub!"); }

public void setUnsignedShort(int unsignedShort) { throw new RuntimeException("Stub!"); }

public short getUnsignedByte() { throw new RuntimeException("Stub!"); }

public void setUnsignedByte(short unsignedByte) { throw new RuntimeException("Stub!"); }
}

