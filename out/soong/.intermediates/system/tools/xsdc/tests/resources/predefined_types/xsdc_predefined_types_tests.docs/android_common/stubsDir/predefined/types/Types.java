
package predefined.types;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Types {

public Types() { throw new RuntimeException("Stub!"); }

public predefined.types.StringTypes getStringTypes() { throw new RuntimeException("Stub!"); }

public void setStringTypes(predefined.types.StringTypes stringTypes) { throw new RuntimeException("Stub!"); }

public predefined.types.DateTypes getDateTypes() { throw new RuntimeException("Stub!"); }

public void setDateTypes(predefined.types.DateTypes dateTypes) { throw new RuntimeException("Stub!"); }

public predefined.types.NumericTypes getNumericTypes() { throw new RuntimeException("Stub!"); }

public void setNumericTypes(predefined.types.NumericTypes numericTypes) { throw new RuntimeException("Stub!"); }

public predefined.types.MiscTypes getMiscTypes() { throw new RuntimeException("Stub!"); }

public void setMiscTypes(predefined.types.MiscTypes miscTypes) { throw new RuntimeException("Stub!"); }

public predefined.types.ListPrimitiveTypes getListPrimitiveTypes() { throw new RuntimeException("Stub!"); }

public void setListPrimitiveTypes(predefined.types.ListPrimitiveTypes listPrimitiveTypes) { throw new RuntimeException("Stub!"); }
}

