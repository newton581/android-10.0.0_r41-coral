
package predefined.types;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MiscTypes {

public MiscTypes() { throw new RuntimeException("Stub!"); }

public double get_double() { throw new RuntimeException("Stub!"); }

public void set_double(double _double) { throw new RuntimeException("Stub!"); }

public float get_float() { throw new RuntimeException("Stub!"); }

public void set_float(float _float) { throw new RuntimeException("Stub!"); }

public java.lang.String getAnyURI() { throw new RuntimeException("Stub!"); }

public void setAnyURI(java.lang.String anyURI) { throw new RuntimeException("Stub!"); }

public byte[] getBase64Binary() { throw new RuntimeException("Stub!"); }

public void setBase64Binary(byte[] base64Binary) { throw new RuntimeException("Stub!"); }

public boolean get_boolean() { throw new RuntimeException("Stub!"); }

public void set_boolean(boolean _boolean) { throw new RuntimeException("Stub!"); }

public java.math.BigInteger getHexBinary() { throw new RuntimeException("Stub!"); }

public void setHexBinary(java.math.BigInteger hexBinary) { throw new RuntimeException("Stub!"); }

public java.lang.String getQName() { throw new RuntimeException("Stub!"); }

public void setQName(java.lang.String qName) { throw new RuntimeException("Stub!"); }

public java.lang.String getIDREF() { throw new RuntimeException("Stub!"); }

public void setIDREF(java.lang.String iDREF) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.String> getIDREFS() { throw new RuntimeException("Stub!"); }

public void setIDREFS(java.util.List<java.lang.String> iDREFS) { throw new RuntimeException("Stub!"); }

public java.lang.String getAnyType() { throw new RuntimeException("Stub!"); }

public void setAnyType(java.lang.String anyType) { throw new RuntimeException("Stub!"); }
}

