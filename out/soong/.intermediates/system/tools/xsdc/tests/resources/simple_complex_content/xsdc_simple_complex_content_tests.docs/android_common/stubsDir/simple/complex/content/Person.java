
package simple.complex.content;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Person {

public Person() { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public simple.complex.content.USAddressP getUSAddressP() { throw new RuntimeException("Stub!"); }

public void setUSAddressP(simple.complex.content.USAddressP uSAddressP) { throw new RuntimeException("Stub!"); }

public simple.complex.content.KRAddress getKRAddress() { throw new RuntimeException("Stub!"); }

public void setKRAddress(simple.complex.content.KRAddress kRAddress) { throw new RuntimeException("Stub!"); }

public simple.complex.content.SubAddress getSubAddress() { throw new RuntimeException("Stub!"); }

public void setSubAddress(simple.complex.content.SubAddress subAddress) { throw new RuntimeException("Stub!"); }
}

