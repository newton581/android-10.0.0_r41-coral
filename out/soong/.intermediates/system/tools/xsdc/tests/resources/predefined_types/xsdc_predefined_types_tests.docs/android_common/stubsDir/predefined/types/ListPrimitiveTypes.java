
package predefined.types;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ListPrimitiveTypes {

public ListPrimitiveTypes() { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.Long> getListLong() { throw new RuntimeException("Stub!"); }

public void setListLong(java.util.List<java.lang.Long> listLong) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.Integer> getListInt() { throw new RuntimeException("Stub!"); }

public void setListInt(java.util.List<java.lang.Integer> listInt) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.Short> getListShort() { throw new RuntimeException("Stub!"); }

public void setListShort(java.util.List<java.lang.Short> listShort) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.Byte> getListByte() { throw new RuntimeException("Stub!"); }

public void setListByte(java.util.List<java.lang.Byte> listByte) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.Double> getListDouble() { throw new RuntimeException("Stub!"); }

public void setListDouble(java.util.List<java.lang.Double> listDouble) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.Float> getListFloat() { throw new RuntimeException("Stub!"); }

public void setListFloat(java.util.List<java.lang.Float> listFloat) { throw new RuntimeException("Stub!"); }

public java.util.List<java.lang.Boolean> getListBoolean() { throw new RuntimeException("Stub!"); }

public void setListBoolean(java.util.List<java.lang.Boolean> listBoolean) { throw new RuntimeException("Stub!"); }
}

