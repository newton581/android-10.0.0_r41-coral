#ifndef AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_I_SUSPEND_CONTROL_SERVICE_H_
#define AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_I_SUSPEND_CONTROL_SERVICE_H_

#include <android/system/suspend/ISuspendCallback.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <utils/StrongPointer.h>

namespace android {

namespace system {

namespace suspend {

class ISuspendControlService : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(SuspendControlService)
  virtual ::android::binder::Status enableAutosuspend(bool* _aidl_return) = 0;
  virtual ::android::binder::Status registerCallback(const ::android::sp<::android::system::suspend::ISuspendCallback>& callback, bool* _aidl_return) = 0;
  virtual ::android::binder::Status forceSuspend(bool* _aidl_return) = 0;
};  // class ISuspendControlService

class ISuspendControlServiceDefault : public ISuspendControlService {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status enableAutosuspend(bool* _aidl_return) override;
  ::android::binder::Status registerCallback(const ::android::sp<::android::system::suspend::ISuspendCallback>& callback, bool* _aidl_return) override;
  ::android::binder::Status forceSuspend(bool* _aidl_return) override;

};

}  // namespace suspend

}  // namespace system

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_I_SUSPEND_CONTROL_SERVICE_H_
