#ifndef AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BN_SUSPEND_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BN_SUSPEND_CALLBACK_H_

#include <binder/IInterface.h>
#include <android/system/suspend/ISuspendCallback.h>

namespace android {

namespace system {

namespace suspend {

class BnSuspendCallback : public ::android::BnInterface<ISuspendCallback> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnSuspendCallback

}  // namespace suspend

}  // namespace system

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BN_SUSPEND_CALLBACK_H_
