#ifndef AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_I_SUSPEND_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_I_SUSPEND_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <utils/StrongPointer.h>

namespace android {

namespace system {

namespace suspend {

class ISuspendCallback : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(SuspendCallback)
  virtual ::android::binder::Status notifyWakeup(bool success) = 0;
};  // class ISuspendCallback

class ISuspendCallbackDefault : public ISuspendCallback {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status notifyWakeup(bool success) override;

};

}  // namespace suspend

}  // namespace system

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_I_SUSPEND_CALLBACK_H_
