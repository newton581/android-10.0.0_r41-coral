#ifndef AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BN_SUSPEND_CONTROL_SERVICE_H_
#define AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BN_SUSPEND_CONTROL_SERVICE_H_

#include <binder/IInterface.h>
#include <android/system/suspend/ISuspendControlService.h>

namespace android {

namespace system {

namespace suspend {

class BnSuspendControlService : public ::android::BnInterface<ISuspendControlService> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnSuspendControlService

}  // namespace suspend

}  // namespace system

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BN_SUSPEND_CONTROL_SERVICE_H_
