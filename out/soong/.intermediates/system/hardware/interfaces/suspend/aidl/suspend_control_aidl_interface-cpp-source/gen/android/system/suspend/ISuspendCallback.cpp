#include <android/system/suspend/ISuspendCallback.h>
#include <android/system/suspend/BpSuspendCallback.h>

namespace android {

namespace system {

namespace suspend {

IMPLEMENT_META_INTERFACE(SuspendCallback, "android.system.suspend.ISuspendCallback")

::android::IBinder* ISuspendCallbackDefault::onAsBinder() {
  return nullptr;
}

::android::binder::Status ISuspendCallbackDefault::notifyWakeup(bool) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

}  // namespace suspend

}  // namespace system

}  // namespace android
#include <android/system/suspend/BpSuspendCallback.h>
#include <binder/Parcel.h>
#include <android-base/macros.h>

namespace android {

namespace system {

namespace suspend {

BpSuspendCallback::BpSuspendCallback(const ::android::sp<::android::IBinder>& _aidl_impl)
    : BpInterface<ISuspendCallback>(_aidl_impl){
}

::android::binder::Status BpSuspendCallback::notifyWakeup(bool success) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeBool(success);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* notifyWakeup */, _aidl_data, &_aidl_reply);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && ISuspendCallback::getDefaultImpl())) {
     return ISuspendCallback::getDefaultImpl()->notifyWakeup(success);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_status.readFromParcel(_aidl_reply);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  if (!_aidl_status.isOk()) {
    return _aidl_status;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

}  // namespace suspend

}  // namespace system

}  // namespace android
#include <android/system/suspend/BnSuspendCallback.h>
#include <binder/Parcel.h>

namespace android {

namespace system {

namespace suspend {

::android::status_t BnSuspendCallback::onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) {
  ::android::status_t _aidl_ret_status = ::android::OK;
  switch (_aidl_code) {
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* notifyWakeup */:
  {
    bool in_success;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readBool(&in_success);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(notifyWakeup(in_success));
    _aidl_ret_status = _aidl_status.writeToParcel(_aidl_reply);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    if (!_aidl_status.isOk()) {
      break;
    }
  }
  break;
  default:
  {
    _aidl_ret_status = ::android::BBinder::onTransact(_aidl_code, _aidl_data, _aidl_reply, _aidl_flags);
  }
  break;
  }
  if (_aidl_ret_status == ::android::UNEXPECTED_NULL) {
    _aidl_ret_status = ::android::binder::Status::fromExceptionCode(::android::binder::Status::EX_NULL_POINTER).writeToParcel(_aidl_reply);
  }
  return _aidl_ret_status;
}

}  // namespace suspend

}  // namespace system

}  // namespace android
