#ifndef AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BP_SUSPEND_CONTROL_SERVICE_H_
#define AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BP_SUSPEND_CONTROL_SERVICE_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/system/suspend/ISuspendControlService.h>

namespace android {

namespace system {

namespace suspend {

class BpSuspendControlService : public ::android::BpInterface<ISuspendControlService> {
public:
  explicit BpSuspendControlService(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpSuspendControlService() = default;
  ::android::binder::Status enableAutosuspend(bool* _aidl_return) override;
  ::android::binder::Status registerCallback(const ::android::sp<::android::system::suspend::ISuspendCallback>& callback, bool* _aidl_return) override;
  ::android::binder::Status forceSuspend(bool* _aidl_return) override;
};  // class BpSuspendControlService

}  // namespace suspend

}  // namespace system

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BP_SUSPEND_CONTROL_SERVICE_H_
