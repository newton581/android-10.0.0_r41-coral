#ifndef AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BP_SUSPEND_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BP_SUSPEND_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/system/suspend/ISuspendCallback.h>

namespace android {

namespace system {

namespace suspend {

class BpSuspendCallback : public ::android::BpInterface<ISuspendCallback> {
public:
  explicit BpSuspendCallback(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpSuspendCallback() = default;
  ::android::binder::Status notifyWakeup(bool success) override;
};  // class BpSuspendCallback

}  // namespace suspend

}  // namespace system

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SYSTEM_SUSPEND_BP_SUSPEND_CALLBACK_H_
