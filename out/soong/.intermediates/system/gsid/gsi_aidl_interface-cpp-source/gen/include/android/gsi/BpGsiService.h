#ifndef AIDL_GENERATED_ANDROID_GSI_BP_GSI_SERVICE_H_
#define AIDL_GENERATED_ANDROID_GSI_BP_GSI_SERVICE_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/gsi/IGsiService.h>

namespace android {

namespace gsi {

class BpGsiService : public ::android::BpInterface<IGsiService> {
public:
  explicit BpGsiService(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpGsiService() = default;
  ::android::binder::Status startGsiInstall(int64_t gsiSize, int64_t userdataSize, bool wipeUserdata, int32_t* _aidl_return) override;
  ::android::binder::Status commitGsiChunkFromStream(const ::android::os::ParcelFileDescriptor& stream, int64_t bytes, bool* _aidl_return) override;
  ::android::binder::Status getInstallProgress(::android::gsi::GsiProgress* _aidl_return) override;
  ::android::binder::Status commitGsiChunkFromMemory(const ::std::vector<uint8_t>& bytes, bool* _aidl_return) override;
  ::android::binder::Status setGsiBootable(bool oneShot, int32_t* _aidl_return) override;
  ::android::binder::Status isGsiEnabled(bool* _aidl_return) override;
  ::android::binder::Status cancelGsiInstall(bool* _aidl_return) override;
  ::android::binder::Status isGsiInstallInProgress(bool* _aidl_return) override;
  ::android::binder::Status removeGsiInstall(bool* _aidl_return) override;
  ::android::binder::Status disableGsiInstall(bool* _aidl_return) override;
  ::android::binder::Status getUserdataImageSize(int64_t* _aidl_return) override;
  ::android::binder::Status isGsiRunning(bool* _aidl_return) override;
  ::android::binder::Status isGsiInstalled(bool* _aidl_return) override;
  ::android::binder::Status getGsiBootStatus(int32_t* _aidl_return) override;
  ::android::binder::Status getInstalledGsiImageDir(::std::string* _aidl_return) override;
  ::android::binder::Status beginGsiInstall(const ::android::gsi::GsiInstallParams& params, int32_t* _aidl_return) override;
  ::android::binder::Status wipeGsiUserdata(int32_t* _aidl_return) override;
};  // class BpGsiService

}  // namespace gsi

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_GSI_BP_GSI_SERVICE_H_
