#ifndef AIDL_GENERATED_ANDROID_GSI_GSI_INSTALL_PARAMS_H_
#define AIDL_GENERATED_ANDROID_GSI_GSI_INSTALL_PARAMS_H_

#include <binder/Parcel.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>

namespace android {

namespace gsi {

class GsiInstallParams : public ::android::Parcelable {
public:
  ::std::string installDir;
  int64_t gsiSize;
  int64_t userdataSize;
  bool wipeUserdata;
  ::android::status_t readFromParcel(const ::android::Parcel* _aidl_parcel) override final;
  ::android::status_t writeToParcel(::android::Parcel* _aidl_parcel) const override final;
};  // class GsiInstallParams

}  // namespace gsi

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_GSI_GSI_INSTALL_PARAMS_H_
