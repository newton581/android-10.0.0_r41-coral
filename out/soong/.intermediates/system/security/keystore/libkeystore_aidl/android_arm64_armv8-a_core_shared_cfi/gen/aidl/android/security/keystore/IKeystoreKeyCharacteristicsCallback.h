#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_KEY_CHARACTERISTICS_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_KEY_CHARACTERISTICS_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <keystore/KeyCharacteristics.h>
#include <keystore/KeystoreResponse.h>
#include <utils/StrongPointer.h>

namespace android {

namespace security {

namespace keystore {

class IKeystoreKeyCharacteristicsCallback : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(KeystoreKeyCharacteristicsCallback)
  virtual ::android::binder::Status onFinished(const ::android::security::keystore::KeystoreResponse& response, const ::android::security::keymaster::KeyCharacteristics& charactersistics) = 0;
};  // class IKeystoreKeyCharacteristicsCallback

class IKeystoreKeyCharacteristicsCallbackDefault : public IKeystoreKeyCharacteristicsCallback {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onFinished(const ::android::security::keystore::KeystoreResponse& response, const ::android::security::keymaster::KeyCharacteristics& charactersistics) override;

};

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_KEY_CHARACTERISTICS_CALLBACK_H_
