#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_EXPORT_KEY_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_EXPORT_KEY_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <keystore/ExportResult.h>
#include <utils/StrongPointer.h>

namespace android {

namespace security {

namespace keystore {

class IKeystoreExportKeyCallback : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(KeystoreExportKeyCallback)
  virtual ::android::binder::Status onFinished(const ::android::security::keymaster::ExportResult& result) = 0;
};  // class IKeystoreExportKeyCallback

class IKeystoreExportKeyCallbackDefault : public IKeystoreExportKeyCallback {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onFinished(const ::android::security::keymaster::ExportResult& result) override;

};

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_EXPORT_KEY_CALLBACK_H_
