#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_OPERATION_RESULT_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_OPERATION_RESULT_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/security/keystore/IKeystoreOperationResultCallback.h>

namespace android {

namespace security {

namespace keystore {

class BpKeystoreOperationResultCallback : public ::android::BpInterface<IKeystoreOperationResultCallback> {
public:
  explicit BpKeystoreOperationResultCallback(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpKeystoreOperationResultCallback() = default;
  ::android::binder::Status onFinished(const ::android::security::keymaster::OperationResult& result) override;
};  // class BpKeystoreOperationResultCallback

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BP_KEYSTORE_OPERATION_RESULT_CALLBACK_H_
