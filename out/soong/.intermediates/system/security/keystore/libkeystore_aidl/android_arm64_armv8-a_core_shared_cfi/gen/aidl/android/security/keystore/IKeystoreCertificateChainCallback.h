#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_CERTIFICATE_CHAIN_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_CERTIFICATE_CHAIN_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <keystore/KeymasterCertificateChain.h>
#include <keystore/KeystoreResponse.h>
#include <utils/StrongPointer.h>

namespace android {

namespace security {

namespace keystore {

class IKeystoreCertificateChainCallback : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(KeystoreCertificateChainCallback)
  virtual ::android::binder::Status onFinished(const ::android::security::keystore::KeystoreResponse& response, const ::android::security::keymaster::KeymasterCertificateChain& chain) = 0;
};  // class IKeystoreCertificateChainCallback

class IKeystoreCertificateChainCallbackDefault : public IKeystoreCertificateChainCallback {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onFinished(const ::android::security::keystore::KeystoreResponse& response, const ::android::security::keymaster::KeymasterCertificateChain& chain) override;

};

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_CERTIFICATE_CHAIN_CALLBACK_H_
