#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BN_KEYSTORE_EXPORT_KEY_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BN_KEYSTORE_EXPORT_KEY_CALLBACK_H_

#include <binder/IInterface.h>
#include <android/security/keystore/IKeystoreExportKeyCallback.h>

namespace android {

namespace security {

namespace keystore {

class BnKeystoreExportKeyCallback : public ::android::BnInterface<IKeystoreExportKeyCallback> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnKeystoreExportKeyCallback

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_BN_KEYSTORE_EXPORT_KEY_CALLBACK_H_
