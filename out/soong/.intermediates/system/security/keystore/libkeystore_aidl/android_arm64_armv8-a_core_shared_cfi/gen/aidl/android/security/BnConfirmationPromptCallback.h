#ifndef AIDL_GENERATED_ANDROID_SECURITY_BN_CONFIRMATION_PROMPT_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_BN_CONFIRMATION_PROMPT_CALLBACK_H_

#include <binder/IInterface.h>
#include <android/security/IConfirmationPromptCallback.h>

namespace android {

namespace security {

class BnConfirmationPromptCallback : public ::android::BnInterface<IConfirmationPromptCallback> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnConfirmationPromptCallback

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_BN_CONFIRMATION_PROMPT_CALLBACK_H_
