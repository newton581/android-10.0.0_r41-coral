#include <android/security/keystore/IKeystoreOperationResultCallback.h>
#include <android/security/keystore/BpKeystoreOperationResultCallback.h>

namespace android {

namespace security {

namespace keystore {

IMPLEMENT_META_INTERFACE(KeystoreOperationResultCallback, "android.security.keystore.IKeystoreOperationResultCallback")

::android::IBinder* IKeystoreOperationResultCallbackDefault::onAsBinder() {
  return nullptr;
}

::android::binder::Status IKeystoreOperationResultCallbackDefault::onFinished(const ::android::security::keymaster::OperationResult&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

}  // namespace keystore

}  // namespace security

}  // namespace android
#include <android/security/keystore/BpKeystoreOperationResultCallback.h>
#include <binder/Parcel.h>
#include <android-base/macros.h>

namespace android {

namespace security {

namespace keystore {

BpKeystoreOperationResultCallback::BpKeystoreOperationResultCallback(const ::android::sp<::android::IBinder>& _aidl_impl)
    : BpInterface<IKeystoreOperationResultCallback>(_aidl_impl){
}

::android::binder::Status BpKeystoreOperationResultCallback::onFinished(const ::android::security::keymaster::OperationResult& result) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeParcelable(result);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* onFinished */, _aidl_data, &_aidl_reply, ::android::IBinder::FLAG_ONEWAY);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IKeystoreOperationResultCallback::getDefaultImpl())) {
     return IKeystoreOperationResultCallback::getDefaultImpl()->onFinished(result);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

}  // namespace keystore

}  // namespace security

}  // namespace android
#include <android/security/keystore/BnKeystoreOperationResultCallback.h>
#include <binder/Parcel.h>

namespace android {

namespace security {

namespace keystore {

::android::status_t BnKeystoreOperationResultCallback::onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) {
  ::android::status_t _aidl_ret_status = ::android::OK;
  switch (_aidl_code) {
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* onFinished */:
  {
    ::android::security::keymaster::OperationResult in_result;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readParcelable(&in_result);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(onFinished(in_result));
  }
  break;
  default:
  {
    _aidl_ret_status = ::android::BBinder::onTransact(_aidl_code, _aidl_data, _aidl_reply, _aidl_flags);
  }
  break;
  }
  if (_aidl_ret_status == ::android::UNEXPECTED_NULL) {
    _aidl_ret_status = ::android::binder::Status::fromExceptionCode(::android::binder::Status::EX_NULL_POINTER).writeToParcel(_aidl_reply);
  }
  return _aidl_ret_status;
}

}  // namespace keystore

}  // namespace security

}  // namespace android
