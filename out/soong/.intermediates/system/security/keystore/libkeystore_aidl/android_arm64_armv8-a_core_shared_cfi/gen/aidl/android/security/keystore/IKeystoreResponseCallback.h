#ifndef AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_RESPONSE_CALLBACK_H_
#define AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_RESPONSE_CALLBACK_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <keystore/KeystoreResponse.h>
#include <utils/StrongPointer.h>

namespace android {

namespace security {

namespace keystore {

class IKeystoreResponseCallback : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(KeystoreResponseCallback)
  virtual ::android::binder::Status onFinished(const ::android::security::keystore::KeystoreResponse& response) = 0;
};  // class IKeystoreResponseCallback

class IKeystoreResponseCallbackDefault : public IKeystoreResponseCallback {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status onFinished(const ::android::security::keystore::KeystoreResponse& response) override;

};

}  // namespace keystore

}  // namespace security

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_SECURITY_KEYSTORE_I_KEYSTORE_RESPONSE_CALLBACK_H_
