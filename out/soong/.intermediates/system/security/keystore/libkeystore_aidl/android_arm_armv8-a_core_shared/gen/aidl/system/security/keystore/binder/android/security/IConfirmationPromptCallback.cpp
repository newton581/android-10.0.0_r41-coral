#include <android/security/IConfirmationPromptCallback.h>
#include <android/security/BpConfirmationPromptCallback.h>

namespace android {

namespace security {

IMPLEMENT_META_INTERFACE(ConfirmationPromptCallback, "android.security.IConfirmationPromptCallback")

::android::IBinder* IConfirmationPromptCallbackDefault::onAsBinder() {
  return nullptr;
}

::android::binder::Status IConfirmationPromptCallbackDefault::onConfirmationPromptCompleted(int32_t, const ::std::vector<uint8_t>&) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

}  // namespace security

}  // namespace android
#include <android/security/BpConfirmationPromptCallback.h>
#include <binder/Parcel.h>
#include <android-base/macros.h>

namespace android {

namespace security {

BpConfirmationPromptCallback::BpConfirmationPromptCallback(const ::android::sp<::android::IBinder>& _aidl_impl)
    : BpInterface<IConfirmationPromptCallback>(_aidl_impl){
}

::android::binder::Status BpConfirmationPromptCallback::onConfirmationPromptCompleted(int32_t result, const ::std::vector<uint8_t>& dataThatWasConfirmed) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeInt32(result);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeByteVector(dataThatWasConfirmed);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* onConfirmationPromptCompleted */, _aidl_data, &_aidl_reply, ::android::IBinder::FLAG_ONEWAY);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && IConfirmationPromptCallback::getDefaultImpl())) {
     return IConfirmationPromptCallback::getDefaultImpl()->onConfirmationPromptCompleted(result, dataThatWasConfirmed);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

}  // namespace security

}  // namespace android
#include <android/security/BnConfirmationPromptCallback.h>
#include <binder/Parcel.h>

namespace android {

namespace security {

::android::status_t BnConfirmationPromptCallback::onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) {
  ::android::status_t _aidl_ret_status = ::android::OK;
  switch (_aidl_code) {
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* onConfirmationPromptCompleted */:
  {
    int32_t in_result;
    ::std::vector<uint8_t> in_dataThatWasConfirmed;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readInt32(&in_result);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    _aidl_ret_status = _aidl_data.readByteVector(&in_dataThatWasConfirmed);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(onConfirmationPromptCompleted(in_result, in_dataThatWasConfirmed));
  }
  break;
  default:
  {
    _aidl_ret_status = ::android::BBinder::onTransact(_aidl_code, _aidl_data, _aidl_reply, _aidl_flags);
  }
  break;
  }
  if (_aidl_ret_status == ::android::UNEXPECTED_NULL) {
    _aidl_ret_status = ::android::binder::Status::fromExceptionCode(::android::binder::Status::EX_NULL_POINTER).writeToParcel(_aidl_reply);
  }
  return _aidl_ret_status;
}

}  // namespace security

}  // namespace android
