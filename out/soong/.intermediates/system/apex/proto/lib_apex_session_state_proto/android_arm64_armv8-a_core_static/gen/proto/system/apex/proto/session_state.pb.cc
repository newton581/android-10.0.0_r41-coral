// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: system/apex/proto/session_state.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "system/apex/proto/session_state.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)

namespace apex {
namespace proto {

namespace {

const ::google::protobuf::Descriptor* SessionState_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  SessionState_reflection_ = NULL;
const ::google::protobuf::EnumDescriptor* SessionState_State_descriptor_ = NULL;

}  // namespace


void protobuf_AssignDesc_system_2fapex_2fproto_2fsession_5fstate_2eproto() {
  protobuf_AddDesc_system_2fapex_2fproto_2fsession_5fstate_2eproto();
  const ::google::protobuf::FileDescriptor* file =
    ::google::protobuf::DescriptorPool::generated_pool()->FindFileByName(
      "system/apex/proto/session_state.proto");
  GOOGLE_CHECK(file != NULL);
  SessionState_descriptor_ = file->message_type(0);
  static const int SessionState_offsets_[4] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(SessionState, id_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(SessionState, state_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(SessionState, child_session_ids_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(SessionState, expected_build_fingerprint_),
  };
  SessionState_reflection_ =
    ::google::protobuf::internal::GeneratedMessageReflection::NewGeneratedMessageReflection(
      SessionState_descriptor_,
      SessionState::default_instance_,
      SessionState_offsets_,
      -1,
      -1,
      -1,
      sizeof(SessionState),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(SessionState, _internal_metadata_),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(SessionState, _is_default_instance_));
  SessionState_State_descriptor_ = SessionState_descriptor_->enum_type(0);
}

namespace {

GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AssignDescriptors_once_);
inline void protobuf_AssignDescriptorsOnce() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AssignDescriptors_once_,
                 &protobuf_AssignDesc_system_2fapex_2fproto_2fsession_5fstate_2eproto);
}

void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
      SessionState_descriptor_, &SessionState::default_instance());
}

}  // namespace

void protobuf_ShutdownFile_system_2fapex_2fproto_2fsession_5fstate_2eproto() {
  delete SessionState::default_instance_;
  delete SessionState_reflection_;
}

void protobuf_AddDesc_system_2fapex_2fproto_2fsession_5fstate_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
    "\n%system/apex/proto/session_state.proto\022"
    "\napex.proto\"\254\002\n\014SessionState\022\n\n\002id\030\001 \001(\005"
    "\022-\n\005state\030\002 \001(\0162\036.apex.proto.SessionStat"
    "e.State\022\031\n\021child_session_ids\030\003 \003(\005\022\"\n\032ex"
    "pected_build_fingerprint\030\004 \001(\t\"\241\001\n\005State"
    "\022\013\n\007UNKNOWN\020\000\022\014\n\010VERIFIED\020\001\022\n\n\006STAGED\020\002\022"
    "\r\n\tACTIVATED\020\003\022\025\n\021ACTIVATION_FAILED\020\004\022\013\n"
    "\007SUCCESS\020\005\022\030\n\024ROLLBACK_IN_PROGRESS\020\006\022\017\n\013"
    "ROLLED_BACK\020\007\022\023\n\017ROLLBACK_FAILED\020\010b\006prot"
    "o3", 362);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "system/apex/proto/session_state.proto", &protobuf_RegisterTypes);
  SessionState::default_instance_ = new SessionState();
  SessionState::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_system_2fapex_2fproto_2fsession_5fstate_2eproto);
}

// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_system_2fapex_2fproto_2fsession_5fstate_2eproto {
  StaticDescriptorInitializer_system_2fapex_2fproto_2fsession_5fstate_2eproto() {
    protobuf_AddDesc_system_2fapex_2fproto_2fsession_5fstate_2eproto();
  }
} static_descriptor_initializer_system_2fapex_2fproto_2fsession_5fstate_2eproto_;

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

const ::google::protobuf::EnumDescriptor* SessionState_State_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return SessionState_State_descriptor_;
}
bool SessionState_State_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const SessionState_State SessionState::UNKNOWN;
const SessionState_State SessionState::VERIFIED;
const SessionState_State SessionState::STAGED;
const SessionState_State SessionState::ACTIVATED;
const SessionState_State SessionState::ACTIVATION_FAILED;
const SessionState_State SessionState::SUCCESS;
const SessionState_State SessionState::ROLLBACK_IN_PROGRESS;
const SessionState_State SessionState::ROLLED_BACK;
const SessionState_State SessionState::ROLLBACK_FAILED;
const SessionState_State SessionState::State_MIN;
const SessionState_State SessionState::State_MAX;
const int SessionState::State_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int SessionState::kIdFieldNumber;
const int SessionState::kStateFieldNumber;
const int SessionState::kChildSessionIdsFieldNumber;
const int SessionState::kExpectedBuildFingerprintFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

SessionState::SessionState()
  : ::google::protobuf::Message(), _internal_metadata_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:apex.proto.SessionState)
}

void SessionState::InitAsDefaultInstance() {
  _is_default_instance_ = true;
}

SessionState::SessionState(const SessionState& from)
  : ::google::protobuf::Message(),
    _internal_metadata_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:apex.proto.SessionState)
}

void SessionState::SharedCtor() {
    _is_default_instance_ = false;
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  id_ = 0;
  state_ = 0;
  expected_build_fingerprint_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

SessionState::~SessionState() {
  // @@protoc_insertion_point(destructor:apex.proto.SessionState)
  SharedDtor();
}

void SessionState::SharedDtor() {
  expected_build_fingerprint_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  if (this != default_instance_) {
  }
}

void SessionState::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* SessionState::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return SessionState_descriptor_;
}

const SessionState& SessionState::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_system_2fapex_2fproto_2fsession_5fstate_2eproto();
  return *default_instance_; /* NOLINT */
}

SessionState* SessionState::default_instance_ = NULL;

SessionState* SessionState::New(::google::protobuf::Arena* arena) const {
  SessionState* n = new SessionState;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void SessionState::Clear() {
// @@protoc_insertion_point(message_clear_start:apex.proto.SessionState)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(SessionState, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<SessionState*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  ZR_(id_, state_);
  expected_build_fingerprint_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());

#undef ZR_HELPER_
#undef ZR_

  child_session_ids_.Clear();
}

bool SessionState::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  // @@protoc_insertion_point(parse_start:apex.proto.SessionState)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional int32 id = 1;
      case 1: {
        if (tag == 8) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &id_)));

        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(16)) goto parse_state;
        break;
      }

      // optional .apex.proto.SessionState.State state = 2;
      case 2: {
        if (tag == 16) {
         parse_state:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          set_state(static_cast< ::apex::proto::SessionState_State >(value));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(26)) goto parse_child_session_ids;
        break;
      }

      // repeated int32 child_session_ids = 3;
      case 3: {
        if (tag == 26) {
         parse_child_session_ids:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, this->mutable_child_session_ids())));
        } else if (tag == 24) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadRepeatedPrimitiveNoInline<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 1, 26, input, this->mutable_child_session_ids())));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(34)) goto parse_expected_build_fingerprint;
        break;
      }

      // optional string expected_build_fingerprint = 4;
      case 4: {
        if (tag == 34) {
         parse_expected_build_fingerprint:
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_expected_build_fingerprint()));
          DO_(::google::protobuf::internal::WireFormatLite::VerifyUtf8String(
            this->expected_build_fingerprint().data(), this->expected_build_fingerprint().length(),
            ::google::protobuf::internal::WireFormatLite::PARSE,
            "apex.proto.SessionState.expected_build_fingerprint"));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(input, tag));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:apex.proto.SessionState)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:apex.proto.SessionState)
  return false;
#undef DO_
}

void SessionState::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:apex.proto.SessionState)
  // optional int32 id = 1;
  if (this->id() != 0) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(1, this->id(), output);
  }

  // optional .apex.proto.SessionState.State state = 2;
  if (this->state() != 0) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      2, this->state(), output);
  }

  // repeated int32 child_session_ids = 3;
  if (this->child_session_ids_size() > 0) {
    ::google::protobuf::internal::WireFormatLite::WriteTag(3, ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED, output);
    output->WriteVarint32(_child_session_ids_cached_byte_size_);
  }
  for (int i = 0; i < this->child_session_ids_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32NoTag(
      this->child_session_ids(i), output);
  }

  // optional string expected_build_fingerprint = 4;
  if (this->expected_build_fingerprint().size() > 0) {
    ::google::protobuf::internal::WireFormatLite::VerifyUtf8String(
      this->expected_build_fingerprint().data(), this->expected_build_fingerprint().length(),
      ::google::protobuf::internal::WireFormatLite::SERIALIZE,
      "apex.proto.SessionState.expected_build_fingerprint");
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      4, this->expected_build_fingerprint(), output);
  }

  // @@protoc_insertion_point(serialize_end:apex.proto.SessionState)
}

::google::protobuf::uint8* SessionState::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // @@protoc_insertion_point(serialize_to_array_start:apex.proto.SessionState)
  // optional int32 id = 1;
  if (this->id() != 0) {
    target = ::google::protobuf::internal::WireFormatLite::WriteInt32ToArray(1, this->id(), target);
  }

  // optional .apex.proto.SessionState.State state = 2;
  if (this->state() != 0) {
    target = ::google::protobuf::internal::WireFormatLite::WriteEnumToArray(
      2, this->state(), target);
  }

  // repeated int32 child_session_ids = 3;
  if (this->child_session_ids_size() > 0) {
    target = ::google::protobuf::internal::WireFormatLite::WriteTagToArray(
      3,
      ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED,
      target);
    target = ::google::protobuf::io::CodedOutputStream::WriteVarint32ToArray(
      _child_session_ids_cached_byte_size_, target);
  }
  for (int i = 0; i < this->child_session_ids_size(); i++) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteInt32NoTagToArray(this->child_session_ids(i), target);
  }

  // optional string expected_build_fingerprint = 4;
  if (this->expected_build_fingerprint().size() > 0) {
    ::google::protobuf::internal::WireFormatLite::VerifyUtf8String(
      this->expected_build_fingerprint().data(), this->expected_build_fingerprint().length(),
      ::google::protobuf::internal::WireFormatLite::SERIALIZE,
      "apex.proto.SessionState.expected_build_fingerprint");
    target =
      ::google::protobuf::internal::WireFormatLite::WriteStringToArray(
        4, this->expected_build_fingerprint(), target);
  }

  // @@protoc_insertion_point(serialize_to_array_end:apex.proto.SessionState)
  return target;
}

int SessionState::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:apex.proto.SessionState)
  int total_size = 0;

  // optional int32 id = 1;
  if (this->id() != 0) {
    total_size += 1 +
      ::google::protobuf::internal::WireFormatLite::Int32Size(
        this->id());
  }

  // optional .apex.proto.SessionState.State state = 2;
  if (this->state() != 0) {
    total_size += 1 +
      ::google::protobuf::internal::WireFormatLite::EnumSize(this->state());
  }

  // optional string expected_build_fingerprint = 4;
  if (this->expected_build_fingerprint().size() > 0) {
    total_size += 1 +
      ::google::protobuf::internal::WireFormatLite::StringSize(
        this->expected_build_fingerprint());
  }

  // repeated int32 child_session_ids = 3;
  {
    int data_size = 0;
    for (int i = 0; i < this->child_session_ids_size(); i++) {
      data_size += ::google::protobuf::internal::WireFormatLite::
        Int32Size(this->child_session_ids(i));
    }
    if (data_size > 0) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(data_size);
    }
    GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
    _child_session_ids_cached_byte_size_ = data_size;
    GOOGLE_SAFE_CONCURRENT_WRITES_END();
    total_size += data_size;
  }

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void SessionState::MergeFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:apex.proto.SessionState)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  const SessionState* source = 
      ::google::protobuf::internal::DynamicCastToGenerated<const SessionState>(
          &from);
  if (source == NULL) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:apex.proto.SessionState)
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:apex.proto.SessionState)
    MergeFrom(*source);
  }
}

void SessionState::MergeFrom(const SessionState& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:apex.proto.SessionState)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  child_session_ids_.MergeFrom(from.child_session_ids_);
  if (from.id() != 0) {
    set_id(from.id());
  }
  if (from.state() != 0) {
    set_state(from.state());
  }
  if (from.expected_build_fingerprint().size() > 0) {

    expected_build_fingerprint_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.expected_build_fingerprint_);
  }
}

void SessionState::CopyFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:apex.proto.SessionState)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void SessionState::CopyFrom(const SessionState& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:apex.proto.SessionState)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool SessionState::IsInitialized() const {

  return true;
}

void SessionState::Swap(SessionState* other) {
  if (other == this) return;
  InternalSwap(other);
}
void SessionState::InternalSwap(SessionState* other) {
  std::swap(id_, other->id_);
  std::swap(state_, other->state_);
  child_session_ids_.UnsafeArenaSwap(&other->child_session_ids_);
  expected_build_fingerprint_.Swap(&other->expected_build_fingerprint_);
  _internal_metadata_.Swap(&other->_internal_metadata_);
  std::swap(_cached_size_, other->_cached_size_);
}

::google::protobuf::Metadata SessionState::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = SessionState_descriptor_;
  metadata.reflection = SessionState_reflection_;
  return metadata;
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// SessionState

// optional int32 id = 1;
void SessionState::clear_id() {
  id_ = 0;
}
 ::google::protobuf::int32 SessionState::id() const {
  // @@protoc_insertion_point(field_get:apex.proto.SessionState.id)
  return id_;
}
 void SessionState::set_id(::google::protobuf::int32 value) {
  
  id_ = value;
  // @@protoc_insertion_point(field_set:apex.proto.SessionState.id)
}

// optional .apex.proto.SessionState.State state = 2;
void SessionState::clear_state() {
  state_ = 0;
}
 ::apex::proto::SessionState_State SessionState::state() const {
  // @@protoc_insertion_point(field_get:apex.proto.SessionState.state)
  return static_cast< ::apex::proto::SessionState_State >(state_);
}
 void SessionState::set_state(::apex::proto::SessionState_State value) {
  
  state_ = value;
  // @@protoc_insertion_point(field_set:apex.proto.SessionState.state)
}

// repeated int32 child_session_ids = 3;
int SessionState::child_session_ids_size() const {
  return child_session_ids_.size();
}
void SessionState::clear_child_session_ids() {
  child_session_ids_.Clear();
}
 ::google::protobuf::int32 SessionState::child_session_ids(int index) const {
  // @@protoc_insertion_point(field_get:apex.proto.SessionState.child_session_ids)
  return child_session_ids_.Get(index);
}
 void SessionState::set_child_session_ids(int index, ::google::protobuf::int32 value) {
  child_session_ids_.Set(index, value);
  // @@protoc_insertion_point(field_set:apex.proto.SessionState.child_session_ids)
}
 void SessionState::add_child_session_ids(::google::protobuf::int32 value) {
  child_session_ids_.Add(value);
  // @@protoc_insertion_point(field_add:apex.proto.SessionState.child_session_ids)
}
 const ::google::protobuf::RepeatedField< ::google::protobuf::int32 >&
SessionState::child_session_ids() const {
  // @@protoc_insertion_point(field_list:apex.proto.SessionState.child_session_ids)
  return child_session_ids_;
}
 ::google::protobuf::RepeatedField< ::google::protobuf::int32 >*
SessionState::mutable_child_session_ids() {
  // @@protoc_insertion_point(field_mutable_list:apex.proto.SessionState.child_session_ids)
  return &child_session_ids_;
}

// optional string expected_build_fingerprint = 4;
void SessionState::clear_expected_build_fingerprint() {
  expected_build_fingerprint_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 const ::std::string& SessionState::expected_build_fingerprint() const {
  // @@protoc_insertion_point(field_get:apex.proto.SessionState.expected_build_fingerprint)
  return expected_build_fingerprint_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void SessionState::set_expected_build_fingerprint(const ::std::string& value) {
  
  expected_build_fingerprint_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:apex.proto.SessionState.expected_build_fingerprint)
}
 void SessionState::set_expected_build_fingerprint(const char* value) {
  
  expected_build_fingerprint_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:apex.proto.SessionState.expected_build_fingerprint)
}
 void SessionState::set_expected_build_fingerprint(const char* value, size_t size) {
  
  expected_build_fingerprint_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:apex.proto.SessionState.expected_build_fingerprint)
}
 ::std::string* SessionState::mutable_expected_build_fingerprint() {
  
  // @@protoc_insertion_point(field_mutable:apex.proto.SessionState.expected_build_fingerprint)
  return expected_build_fingerprint_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 ::std::string* SessionState::release_expected_build_fingerprint() {
  // @@protoc_insertion_point(field_release:apex.proto.SessionState.expected_build_fingerprint)
  
  return expected_build_fingerprint_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void SessionState::set_allocated_expected_build_fingerprint(::std::string* expected_build_fingerprint) {
  if (expected_build_fingerprint != NULL) {
    
  } else {
    
  }
  expected_build_fingerprint_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), expected_build_fingerprint);
  // @@protoc_insertion_point(field_set_allocated:apex.proto.SessionState.expected_build_fingerprint)
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace proto
}  // namespace apex

// @@protoc_insertion_point(global_scope)
