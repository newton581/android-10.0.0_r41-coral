#ifndef AIDL_GENERATED_ANDROID_APEX_APEX_INFO_LIST_H_
#define AIDL_GENERATED_ANDROID_APEX_APEX_INFO_LIST_H_

#include <android/apex/ApexInfo.h>
#include <binder/Parcel.h>
#include <binder/Status.h>
#include <vector>

namespace android {

namespace apex {

class ApexInfoList : public ::android::Parcelable {
public:
  ::std::vector<::android::apex::ApexInfo> apexInfos;
  ::android::status_t readFromParcel(const ::android::Parcel* _aidl_parcel) override final;
  ::android::status_t writeToParcel(::android::Parcel* _aidl_parcel) const override final;
};  // class ApexInfoList

}  // namespace apex

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_APEX_APEX_INFO_LIST_H_
