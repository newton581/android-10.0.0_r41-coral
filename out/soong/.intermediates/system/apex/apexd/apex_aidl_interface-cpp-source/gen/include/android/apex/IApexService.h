#ifndef AIDL_GENERATED_ANDROID_APEX_I_APEX_SERVICE_H_
#define AIDL_GENERATED_ANDROID_APEX_I_APEX_SERVICE_H_

#include <android/apex/ApexInfo.h>
#include <android/apex/ApexInfoList.h>
#include <android/apex/ApexSessionInfo.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <string>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace apex {

class IApexService : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(ApexService)
  virtual ::android::binder::Status submitStagedSession(int32_t session_id, const ::std::vector<int32_t>& child_session_ids, ::android::apex::ApexInfoList* packages, bool* _aidl_return) = 0;
  virtual ::android::binder::Status markStagedSessionReady(int32_t session_id, bool* _aidl_return) = 0;
  virtual ::android::binder::Status markStagedSessionSuccessful(int32_t session_id) = 0;
  virtual ::android::binder::Status getSessions(::std::vector<::android::apex::ApexSessionInfo>* _aidl_return) = 0;
  virtual ::android::binder::Status getStagedSessionInfo(int32_t session_id, ::android::apex::ApexSessionInfo* _aidl_return) = 0;
  virtual ::android::binder::Status getActivePackages(::std::vector<::android::apex::ApexInfo>* _aidl_return) = 0;
  virtual ::android::binder::Status getAllPackages(::std::vector<::android::apex::ApexInfo>* _aidl_return) = 0;
  virtual ::android::binder::Status abortActiveSession() = 0;
  virtual ::android::binder::Status unstagePackages(const ::std::vector<::std::string>& active_package_paths) = 0;
  virtual ::android::binder::Status getActivePackage(const ::std::string& package_name, ::android::apex::ApexInfo* _aidl_return) = 0;
  virtual ::android::binder::Status activatePackage(const ::std::string& package_path) = 0;
  virtual ::android::binder::Status deactivatePackage(const ::std::string& package_path) = 0;
  virtual ::android::binder::Status preinstallPackages(const ::std::vector<::std::string>& package_tmp_paths) = 0;
  virtual ::android::binder::Status postinstallPackages(const ::std::vector<::std::string>& package_tmp_paths) = 0;
  virtual ::android::binder::Status stagePackage(const ::std::string& package_tmp_path, bool* _aidl_return) = 0;
  virtual ::android::binder::Status stagePackages(const ::std::vector<::std::string>& package_tmp_paths, bool* _aidl_return) = 0;
  virtual ::android::binder::Status rollbackActiveSession() = 0;
  virtual ::android::binder::Status resumeRollbackIfNeeded() = 0;
};  // class IApexService

class IApexServiceDefault : public IApexService {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status submitStagedSession(int32_t session_id, const ::std::vector<int32_t>& child_session_ids, ::android::apex::ApexInfoList* packages, bool* _aidl_return) override;
  ::android::binder::Status markStagedSessionReady(int32_t session_id, bool* _aidl_return) override;
  ::android::binder::Status markStagedSessionSuccessful(int32_t session_id) override;
  ::android::binder::Status getSessions(::std::vector<::android::apex::ApexSessionInfo>* _aidl_return) override;
  ::android::binder::Status getStagedSessionInfo(int32_t session_id, ::android::apex::ApexSessionInfo* _aidl_return) override;
  ::android::binder::Status getActivePackages(::std::vector<::android::apex::ApexInfo>* _aidl_return) override;
  ::android::binder::Status getAllPackages(::std::vector<::android::apex::ApexInfo>* _aidl_return) override;
  ::android::binder::Status abortActiveSession() override;
  ::android::binder::Status unstagePackages(const ::std::vector<::std::string>& active_package_paths) override;
  ::android::binder::Status getActivePackage(const ::std::string& package_name, ::android::apex::ApexInfo* _aidl_return) override;
  ::android::binder::Status activatePackage(const ::std::string& package_path) override;
  ::android::binder::Status deactivatePackage(const ::std::string& package_path) override;
  ::android::binder::Status preinstallPackages(const ::std::vector<::std::string>& package_tmp_paths) override;
  ::android::binder::Status postinstallPackages(const ::std::vector<::std::string>& package_tmp_paths) override;
  ::android::binder::Status stagePackage(const ::std::string& package_tmp_path, bool* _aidl_return) override;
  ::android::binder::Status stagePackages(const ::std::vector<::std::string>& package_tmp_paths, bool* _aidl_return) override;
  ::android::binder::Status rollbackActiveSession() override;
  ::android::binder::Status resumeRollbackIfNeeded() override;

};

}  // namespace apex

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_APEX_I_APEX_SERVICE_H_
