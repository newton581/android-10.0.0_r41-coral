#ifndef AIDL_GENERATED_ANDROID_APEX_APEX_SESSION_INFO_H_
#define AIDL_GENERATED_ANDROID_APEX_APEX_SESSION_INFO_H_

#include <binder/Parcel.h>
#include <binder/Status.h>
#include <cstdint>

namespace android {

namespace apex {

class ApexSessionInfo : public ::android::Parcelable {
public:
  int32_t sessionId;
  bool isUnknown;
  bool isVerified;
  bool isStaged;
  bool isActivated;
  bool isRollbackInProgress;
  bool isActivationFailed;
  bool isSuccess;
  bool isRolledBack;
  bool isRollbackFailed;
  ::android::status_t readFromParcel(const ::android::Parcel* _aidl_parcel) override final;
  ::android::status_t writeToParcel(::android::Parcel* _aidl_parcel) const override final;
};  // class ApexSessionInfo

}  // namespace apex

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_APEX_APEX_SESSION_INFO_H_
