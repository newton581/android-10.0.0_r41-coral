#ifndef AIDL_GENERATED_ANDROID_LPDUMP_BP_LPDUMP_H_
#define AIDL_GENERATED_ANDROID_LPDUMP_BP_LPDUMP_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/lpdump/ILpdump.h>
#include <chrono>
#include <functional>
#include <json/value.h>

namespace android {

namespace lpdump {

class BpLpdump : public ::android::BpInterface<ILpdump> {
public:
  explicit BpLpdump(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpLpdump() = default;
  ::android::binder::Status run(const ::std::vector<::std::string>& args, ::std::string* _aidl_return) override;
  static std::function<void(const Json::Value&)> logFunc;
};  // class BpLpdump

}  // namespace lpdump

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_LPDUMP_BP_LPDUMP_H_
