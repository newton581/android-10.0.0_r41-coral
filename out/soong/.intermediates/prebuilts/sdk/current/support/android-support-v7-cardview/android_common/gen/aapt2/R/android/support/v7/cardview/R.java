/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package android.support.v7.cardview;

public final class R {
  public static final class attr {
    /**
     * Background color for CardView.
     * <p>May be a color value, in the form of "<code>#<i>rgb</i></code>",
     * "<code>#<i>argb</i></code>", "<code>#<i>rrggbb</i></code>", or
     * "<code>#<i>aarrggbb</i></code>".
     */
    public static int cardBackgroundColor=0x00000000;
    /**
     * Corner radius for CardView.
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static int cardCornerRadius=0x00000000;
    /**
     * Elevation for CardView.
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static int cardElevation=0x00000000;
    /**
     * Maximum Elevation for CardView.
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static int cardMaxElevation=0x00000000;
    /**
     * Add padding to CardView on v20 and before to prevent intersections between the Card content and rounded corners.
     * <p>May be a boolean value, such as "<code>true</code>" or
     * "<code>false</code>".
     */
    public static int cardPreventCornerOverlap=0x00000000;
    /**
     * Add padding in API v21+ as well to have the same measurements with previous versions.
     * <p>May be a boolean value, such as "<code>true</code>" or
     * "<code>false</code>".
     */
    public static int cardUseCompatPadding=0x00000000;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static int cardViewStyle=0x00000000;
    /**
     * Inner padding between the edges of the Card and children of the CardView.
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static int contentPadding=0x00000000;
    /**
     * Inner padding between the bottom edge of the Card and children of the CardView.
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static int contentPaddingBottom=0x00000000;
    /**
     * Inner padding between the left edge of the Card and children of the CardView.
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static int contentPaddingLeft=0x00000000;
    /**
     * Inner padding between the right edge of the Card and children of the CardView.
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static int contentPaddingRight=0x00000000;
    /**
     * Inner padding between the top edge of the Card and children of the CardView.
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     */
    public static int contentPaddingTop=0x00000000;
  }
  public static final class color {
    public static int cardview_dark_background=0x00000000;
    public static int cardview_light_background=0x00000000;
    public static int cardview_shadow_end_color=0x00000000;
    public static int cardview_shadow_start_color=0x00000000;
  }
  public static final class dimen {
    public static int cardview_compat_inset_shadow=0x00000000;
    public static int cardview_default_elevation=0x00000000;
    public static int cardview_default_radius=0x00000000;
  }
  public static final class style {
    public static int Base_CardView=0x00000000;
    public static int CardView=0x00000000;
    public static int CardView_Dark=0x00000000;
    public static int CardView_Light=0x00000000;
  }
  public static final class styleable {
    /**
     * Attributes that can be used with a CardView.
     * <p>Includes the following attributes:</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Attribute</th><th>Description</th></tr>
     * <tr><td><code>{@link #CardView_cardBackgroundColor android.support.v7.cardview:cardBackgroundColor}</code></td><td>Background color for CardView.</td></tr>
     * <tr><td><code>{@link #CardView_cardCornerRadius android.support.v7.cardview:cardCornerRadius}</code></td><td>Corner radius for CardView.</td></tr>
     * <tr><td><code>{@link #CardView_cardElevation android.support.v7.cardview:cardElevation}</code></td><td>Elevation for CardView.</td></tr>
     * <tr><td><code>{@link #CardView_cardMaxElevation android.support.v7.cardview:cardMaxElevation}</code></td><td>Maximum Elevation for CardView.</td></tr>
     * <tr><td><code>{@link #CardView_cardPreventCornerOverlap android.support.v7.cardview:cardPreventCornerOverlap}</code></td><td>Add padding to CardView on v20 and before to prevent intersections between the Card content and rounded corners.</td></tr>
     * <tr><td><code>{@link #CardView_cardUseCompatPadding android.support.v7.cardview:cardUseCompatPadding}</code></td><td>Add padding in API v21+ as well to have the same measurements with previous versions.</td></tr>
     * <tr><td><code>{@link #CardView_contentPadding android.support.v7.cardview:contentPadding}</code></td><td>Inner padding between the edges of the Card and children of the CardView.</td></tr>
     * <tr><td><code>{@link #CardView_contentPaddingBottom android.support.v7.cardview:contentPaddingBottom}</code></td><td>Inner padding between the bottom edge of the Card and children of the CardView.</td></tr>
     * <tr><td><code>{@link #CardView_contentPaddingLeft android.support.v7.cardview:contentPaddingLeft}</code></td><td>Inner padding between the left edge of the Card and children of the CardView.</td></tr>
     * <tr><td><code>{@link #CardView_contentPaddingRight android.support.v7.cardview:contentPaddingRight}</code></td><td>Inner padding between the right edge of the Card and children of the CardView.</td></tr>
     * <tr><td><code>{@link #CardView_contentPaddingTop android.support.v7.cardview:contentPaddingTop}</code></td><td>Inner padding between the top edge of the Card and children of the CardView.</td></tr>
     * <tr><td><code>{@link #CardView_android_minWidth android:minWidth}</code></td><td></td></tr>
     * <tr><td><code>{@link #CardView_android_minHeight android:minHeight}</code></td><td></td></tr>
     * </table>
     * @see #CardView_cardBackgroundColor
     * @see #CardView_cardCornerRadius
     * @see #CardView_cardElevation
     * @see #CardView_cardMaxElevation
     * @see #CardView_cardPreventCornerOverlap
     * @see #CardView_cardUseCompatPadding
     * @see #CardView_contentPadding
     * @see #CardView_contentPaddingBottom
     * @see #CardView_contentPaddingLeft
     * @see #CardView_contentPaddingRight
     * @see #CardView_contentPaddingTop
     * @see #CardView_android_minWidth
     * @see #CardView_android_minHeight
     */
    public static final int[] CardView={
      0x00000000, 0x00000000, 0x00000000, 0x00000000, 
      0x00000000, 0x00000000, 0x00000000, 0x00000000, 
      0x00000000, 0x00000000, 0x00000000, 0x0101013f, 
      0x01010140
    };
    /**
     * <p>
     * @attr description
     * Background color for CardView.
     *
     * <p>May be a color value, in the form of "<code>#<i>rgb</i></code>",
     * "<code>#<i>argb</i></code>", "<code>#<i>rrggbb</i></code>", or
     * "<code>#<i>aarrggbb</i></code>".
     *
     * @attr name android.support.v7.cardview:cardBackgroundColor
     */
    public static int CardView_cardBackgroundColor=0;
    /**
     * <p>
     * @attr description
     * Corner radius for CardView.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android.support.v7.cardview:cardCornerRadius
     */
    public static int CardView_cardCornerRadius=1;
    /**
     * <p>
     * @attr description
     * Elevation for CardView.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android.support.v7.cardview:cardElevation
     */
    public static int CardView_cardElevation=2;
    /**
     * <p>
     * @attr description
     * Maximum Elevation for CardView.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android.support.v7.cardview:cardMaxElevation
     */
    public static int CardView_cardMaxElevation=3;
    /**
     * <p>
     * @attr description
     * Add padding to CardView on v20 and before to prevent intersections between the Card content and rounded corners.
     *
     * <p>May be a boolean value, such as "<code>true</code>" or
     * "<code>false</code>".
     *
     * @attr name android.support.v7.cardview:cardPreventCornerOverlap
     */
    public static int CardView_cardPreventCornerOverlap=4;
    /**
     * <p>
     * @attr description
     * Add padding in API v21+ as well to have the same measurements with previous versions.
     *
     * <p>May be a boolean value, such as "<code>true</code>" or
     * "<code>false</code>".
     *
     * @attr name android.support.v7.cardview:cardUseCompatPadding
     */
    public static int CardView_cardUseCompatPadding=5;
    /**
     * <p>
     * @attr description
     * Inner padding between the edges of the Card and children of the CardView.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android.support.v7.cardview:contentPadding
     */
    public static int CardView_contentPadding=6;
    /**
     * <p>
     * @attr description
     * Inner padding between the bottom edge of the Card and children of the CardView.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android.support.v7.cardview:contentPaddingBottom
     */
    public static int CardView_contentPaddingBottom=7;
    /**
     * <p>
     * @attr description
     * Inner padding between the left edge of the Card and children of the CardView.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android.support.v7.cardview:contentPaddingLeft
     */
    public static int CardView_contentPaddingLeft=8;
    /**
     * <p>
     * @attr description
     * Inner padding between the right edge of the Card and children of the CardView.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android.support.v7.cardview:contentPaddingRight
     */
    public static int CardView_contentPaddingRight=9;
    /**
     * <p>
     * @attr description
     * Inner padding between the top edge of the Card and children of the CardView.
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android.support.v7.cardview:contentPaddingTop
     */
    public static int CardView_contentPaddingTop=10;
    /**
     * <p>
     * @attr description
     * Workaround to read user defined minimum width
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android:minWidth
     */
    public static int CardView_android_minWidth=11;
    /**
     * <p>
     * @attr description
     * Workaround to read user defined minimum height
     *
     * <p>May be a dimension value, which is a floating point number appended with a
     * unit such as "<code>14.5sp</code>".
     * Available units are: px (pixels), dp (density-independent pixels),
     * sp (scaled pixels based on preferred font size), in (inches), and
     * mm (millimeters).
     *
     * @attr name android:minHeight
     */
    public static int CardView_android_minHeight=12;
  }
}