/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package com.android.ims.internal;
/**
 * @hide
 */
public interface IRcsPresence extends android.os.IInterface
{
  /** Default implementation for IRcsPresence. */
  public static class Default implements com.android.ims.internal.IRcsPresence
  {
    /**
         * Send the request to the server to get the capability.
         *   1. If the presence service sent the request to network successfully
         * then it will return the request ID (>0). It will not wait for the response from
         * network. The response from network will be returned by callback onSuccess() or onError().
         *   2. If the presence service failed to send the request to network then it will return error
         *  code which is defined by RcsManager.ResultCode (<0).
         *   3. If the network returns "200 OK" for a request then the listener.onSuccess() will be
         * called by presence service.
         *   4. If the network resturns "404" for a single target number then it means the target
         * number is not VoLte capable, so the listener.onSuccess() will be called and intent
         * ACTION_PRESENCE_CHANGED will be broadcasted by presence service.
         *   5. If the network returns other error then the listener.onError() will be called by
         * presence service.
         *   6. If the network returns "200 OK" then we can expect the presence service receives notify
         * from network. If the presence service receives notify then it will broadcast the
         * intent ACTION_PRESENCE_CHANGED. If the notify state is "terminated" then the
         * listener.onFinish() will be called by presence service as well.
         *   7. If the presence service doesn't get response after "Subscribe Expiration + T1" then the
         * listener.onTimeout() will be called by presence service.
         *
         * @param contactsNumber the contact number list which will be requested.
         * @param listener the IRcsPresenceListener which will return the status and response.
         *
         * @return the request ID if it is >0. Or it is RcsManager.ResultCode for error.
         *
         * @see IRcsPresenceListener
         * @see RcsManager.ResultCode
         */
    @Override public int requestCapability(java.util.List<java.lang.String> contactsNumber, com.android.ims.IRcsPresenceListener listener) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Send the request to the server to get the availability.
         *   1. If the presence service sent the request to network successfully then it will return
         * the request ID (>0).
         *   2. If the presence serive failed to send the request to network then it will return error
         * code which is defined by RcsManager.ResultCode (<0).
         *   3. If the network returns "200 OK" for a request then the listener.onSuccess() will be
         * called by presence service.
         *   4. If the network resturns "404" then it means the target number is not VoLte capable,
         * so the listener.onSuccess() will be called and intent ACTION_PRESENCE_CHANGED will be
         * broadcasted by presence service.
         *   5. If the network returns other error code then the listener.onError() will be called by
         * presence service.
         *   6. If the network returns "200 OK" then we can expect the presence service receives notify
         * from network. If the presence service receives notify then it will broadcast the intent
         * ACTION_PRESENCE_CHANGED. If the notify state is "terminated" then the listener.onFinish()
         * will be called by presence service as well.
         *   7. If the presence service doesn't get response after "Subscribe Expiration + T1" then it
         * will call listener.onTimeout().
         *
         * @param contactNumber the contact which will request the availability.
         *     Only support phone number at present.
         * @param listener the IRcsPresenceListener to get the response.
         *
         * @return the request ID if it is >0. Or it is RcsManager.ResultCode for error.
         *
         * @see IRcsPresenceListener
         * @see RcsManager.ResultCode
         * @see RcsPresence.ACTION_PRESENCE_CHANGED
         */
    @Override public int requestAvailability(java.lang.String contactNumber, com.android.ims.IRcsPresenceListener listener) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Same as requestAvailability. but requestAvailability will consider throttle to avoid too
         * fast call. Which means it will not send the request to network in next 60s for the same
         * request.
         * The error code SUBSCRIBE_TOO_FREQUENTLY will be returned under the case.
         * But for this funcation it will always send the request to network.
         *
         * @see IRcsPresenceListener
         * @see RcsManager.ResultCode
         * @see RcsPresence.ACTION_PRESENCE_CHANGED
         * @see ResultCode.SUBSCRIBE_TOO_FREQUENTLY
         */
    @Override public int requestAvailabilityNoThrottle(java.lang.String contactNumber, com.android.ims.IRcsPresenceListener listener) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Get the latest publish state.
         *
         * @see RcsPresence.PublishState
         */
    @Override public int getPublishState() throws android.os.RemoteException
    {
      return 0;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements com.android.ims.internal.IRcsPresence
  {
    private static final java.lang.String DESCRIPTOR = "com.android.ims.internal.IRcsPresence";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an com.android.ims.internal.IRcsPresence interface,
     * generating a proxy if needed.
     */
    public static com.android.ims.internal.IRcsPresence asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof com.android.ims.internal.IRcsPresence))) {
        return ((com.android.ims.internal.IRcsPresence)iin);
      }
      return new com.android.ims.internal.IRcsPresence.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_requestCapability:
        {
          data.enforceInterface(descriptor);
          java.util.List<java.lang.String> _arg0;
          _arg0 = data.createStringArrayList();
          com.android.ims.IRcsPresenceListener _arg1;
          _arg1 = com.android.ims.IRcsPresenceListener.Stub.asInterface(data.readStrongBinder());
          int _result = this.requestCapability(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_requestAvailability:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          com.android.ims.IRcsPresenceListener _arg1;
          _arg1 = com.android.ims.IRcsPresenceListener.Stub.asInterface(data.readStrongBinder());
          int _result = this.requestAvailability(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_requestAvailabilityNoThrottle:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          com.android.ims.IRcsPresenceListener _arg1;
          _arg1 = com.android.ims.IRcsPresenceListener.Stub.asInterface(data.readStrongBinder());
          int _result = this.requestAvailabilityNoThrottle(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getPublishState:
        {
          data.enforceInterface(descriptor);
          int _result = this.getPublishState();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements com.android.ims.internal.IRcsPresence
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Send the request to the server to get the capability.
           *   1. If the presence service sent the request to network successfully
           * then it will return the request ID (>0). It will not wait for the response from
           * network. The response from network will be returned by callback onSuccess() or onError().
           *   2. If the presence service failed to send the request to network then it will return error
           *  code which is defined by RcsManager.ResultCode (<0).
           *   3. If the network returns "200 OK" for a request then the listener.onSuccess() will be
           * called by presence service.
           *   4. If the network resturns "404" for a single target number then it means the target
           * number is not VoLte capable, so the listener.onSuccess() will be called and intent
           * ACTION_PRESENCE_CHANGED will be broadcasted by presence service.
           *   5. If the network returns other error then the listener.onError() will be called by
           * presence service.
           *   6. If the network returns "200 OK" then we can expect the presence service receives notify
           * from network. If the presence service receives notify then it will broadcast the
           * intent ACTION_PRESENCE_CHANGED. If the notify state is "terminated" then the
           * listener.onFinish() will be called by presence service as well.
           *   7. If the presence service doesn't get response after "Subscribe Expiration + T1" then the
           * listener.onTimeout() will be called by presence service.
           *
           * @param contactsNumber the contact number list which will be requested.
           * @param listener the IRcsPresenceListener which will return the status and response.
           *
           * @return the request ID if it is >0. Or it is RcsManager.ResultCode for error.
           *
           * @see IRcsPresenceListener
           * @see RcsManager.ResultCode
           */
      @Override public int requestCapability(java.util.List<java.lang.String> contactsNumber, com.android.ims.IRcsPresenceListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStringList(contactsNumber);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_requestCapability, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().requestCapability(contactsNumber, listener);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Send the request to the server to get the availability.
           *   1. If the presence service sent the request to network successfully then it will return
           * the request ID (>0).
           *   2. If the presence serive failed to send the request to network then it will return error
           * code which is defined by RcsManager.ResultCode (<0).
           *   3. If the network returns "200 OK" for a request then the listener.onSuccess() will be
           * called by presence service.
           *   4. If the network resturns "404" then it means the target number is not VoLte capable,
           * so the listener.onSuccess() will be called and intent ACTION_PRESENCE_CHANGED will be
           * broadcasted by presence service.
           *   5. If the network returns other error code then the listener.onError() will be called by
           * presence service.
           *   6. If the network returns "200 OK" then we can expect the presence service receives notify
           * from network. If the presence service receives notify then it will broadcast the intent
           * ACTION_PRESENCE_CHANGED. If the notify state is "terminated" then the listener.onFinish()
           * will be called by presence service as well.
           *   7. If the presence service doesn't get response after "Subscribe Expiration + T1" then it
           * will call listener.onTimeout().
           *
           * @param contactNumber the contact which will request the availability.
           *     Only support phone number at present.
           * @param listener the IRcsPresenceListener to get the response.
           *
           * @return the request ID if it is >0. Or it is RcsManager.ResultCode for error.
           *
           * @see IRcsPresenceListener
           * @see RcsManager.ResultCode
           * @see RcsPresence.ACTION_PRESENCE_CHANGED
           */
      @Override public int requestAvailability(java.lang.String contactNumber, com.android.ims.IRcsPresenceListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(contactNumber);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_requestAvailability, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().requestAvailability(contactNumber, listener);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Same as requestAvailability. but requestAvailability will consider throttle to avoid too
           * fast call. Which means it will not send the request to network in next 60s for the same
           * request.
           * The error code SUBSCRIBE_TOO_FREQUENTLY will be returned under the case.
           * But for this funcation it will always send the request to network.
           *
           * @see IRcsPresenceListener
           * @see RcsManager.ResultCode
           * @see RcsPresence.ACTION_PRESENCE_CHANGED
           * @see ResultCode.SUBSCRIBE_TOO_FREQUENTLY
           */
      @Override public int requestAvailabilityNoThrottle(java.lang.String contactNumber, com.android.ims.IRcsPresenceListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(contactNumber);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_requestAvailabilityNoThrottle, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().requestAvailabilityNoThrottle(contactNumber, listener);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Get the latest publish state.
           *
           * @see RcsPresence.PublishState
           */
      @Override public int getPublishState() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPublishState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPublishState();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static com.android.ims.internal.IRcsPresence sDefaultImpl;
    }
    static final int TRANSACTION_requestCapability = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_requestAvailability = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_requestAvailabilityNoThrottle = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_getPublishState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    public static boolean setDefaultImpl(com.android.ims.internal.IRcsPresence impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static com.android.ims.internal.IRcsPresence getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /**
       * Send the request to the server to get the capability.
       *   1. If the presence service sent the request to network successfully
       * then it will return the request ID (>0). It will not wait for the response from
       * network. The response from network will be returned by callback onSuccess() or onError().
       *   2. If the presence service failed to send the request to network then it will return error
       *  code which is defined by RcsManager.ResultCode (<0).
       *   3. If the network returns "200 OK" for a request then the listener.onSuccess() will be
       * called by presence service.
       *   4. If the network resturns "404" for a single target number then it means the target
       * number is not VoLte capable, so the listener.onSuccess() will be called and intent
       * ACTION_PRESENCE_CHANGED will be broadcasted by presence service.
       *   5. If the network returns other error then the listener.onError() will be called by
       * presence service.
       *   6. If the network returns "200 OK" then we can expect the presence service receives notify
       * from network. If the presence service receives notify then it will broadcast the
       * intent ACTION_PRESENCE_CHANGED. If the notify state is "terminated" then the
       * listener.onFinish() will be called by presence service as well.
       *   7. If the presence service doesn't get response after "Subscribe Expiration + T1" then the
       * listener.onTimeout() will be called by presence service.
       *
       * @param contactsNumber the contact number list which will be requested.
       * @param listener the IRcsPresenceListener which will return the status and response.
       *
       * @return the request ID if it is >0. Or it is RcsManager.ResultCode for error.
       *
       * @see IRcsPresenceListener
       * @see RcsManager.ResultCode
       */
  public int requestCapability(java.util.List<java.lang.String> contactsNumber, com.android.ims.IRcsPresenceListener listener) throws android.os.RemoteException;
  /**
       * Send the request to the server to get the availability.
       *   1. If the presence service sent the request to network successfully then it will return
       * the request ID (>0).
       *   2. If the presence serive failed to send the request to network then it will return error
       * code which is defined by RcsManager.ResultCode (<0).
       *   3. If the network returns "200 OK" for a request then the listener.onSuccess() will be
       * called by presence service.
       *   4. If the network resturns "404" then it means the target number is not VoLte capable,
       * so the listener.onSuccess() will be called and intent ACTION_PRESENCE_CHANGED will be
       * broadcasted by presence service.
       *   5. If the network returns other error code then the listener.onError() will be called by
       * presence service.
       *   6. If the network returns "200 OK" then we can expect the presence service receives notify
       * from network. If the presence service receives notify then it will broadcast the intent
       * ACTION_PRESENCE_CHANGED. If the notify state is "terminated" then the listener.onFinish()
       * will be called by presence service as well.
       *   7. If the presence service doesn't get response after "Subscribe Expiration + T1" then it
       * will call listener.onTimeout().
       *
       * @param contactNumber the contact which will request the availability.
       *     Only support phone number at present.
       * @param listener the IRcsPresenceListener to get the response.
       *
       * @return the request ID if it is >0. Or it is RcsManager.ResultCode for error.
       *
       * @see IRcsPresenceListener
       * @see RcsManager.ResultCode
       * @see RcsPresence.ACTION_PRESENCE_CHANGED
       */
  public int requestAvailability(java.lang.String contactNumber, com.android.ims.IRcsPresenceListener listener) throws android.os.RemoteException;
  /**
       * Same as requestAvailability. but requestAvailability will consider throttle to avoid too
       * fast call. Which means it will not send the request to network in next 60s for the same
       * request.
       * The error code SUBSCRIBE_TOO_FREQUENTLY will be returned under the case.
       * But for this funcation it will always send the request to network.
       *
       * @see IRcsPresenceListener
       * @see RcsManager.ResultCode
       * @see RcsPresence.ACTION_PRESENCE_CHANGED
       * @see ResultCode.SUBSCRIBE_TOO_FREQUENTLY
       */
  public int requestAvailabilityNoThrottle(java.lang.String contactNumber, com.android.ims.IRcsPresenceListener listener) throws android.os.RemoteException;
  /**
       * Get the latest publish state.
       *
       * @see RcsPresence.PublishState
       */
  public int getPublishState() throws android.os.RemoteException;
}
