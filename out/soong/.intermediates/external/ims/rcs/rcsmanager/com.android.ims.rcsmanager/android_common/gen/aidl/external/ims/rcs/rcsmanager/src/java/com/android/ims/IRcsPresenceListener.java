/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package com.android.ims;
/**
 * RcsPresenceListener is used by requestCapability and requestAvailability to return the
 * status. The reqId which returned by requestCapability and requestAvailability can be
 * used to match the result with request when share the implementation of IRcsPresenceListener.
 *
 * There are following normal cases:
 *   1> The presence service got terminated notify after submitted the SUBSCRIBE request.
 *       onSuccess() ---> onFinish()
 *   2> The presence service submitted the request to network and got "200 OK" for SIP response.
 *   but it didn't get "terminated notify" in expries + T1 timer.
 *       onSuccess() ---> onTimeout()
 *   3> The presence service got error from SIP response for SUBSCRIBE request.
 *       onError()
 *
 * @hide
 */
public interface IRcsPresenceListener extends android.os.IInterface
{
  /** Default implementation for IRcsPresenceListener. */
  public static class Default implements com.android.ims.IRcsPresenceListener
  {
    /**
         * It is called when it gets "200 OK" from network for SIP request or
         * it gets "404 xxxx" for SIP request of single contact number (means not capable).
         *
         * @param reqId the request ID which returned by requestCapability or
         * requestAvailability
         *
         * @see RcsPresence#requestCapability
         * @see RcsPresence#requestAvailability
         */
    @Override public void onSuccess(int reqId) throws android.os.RemoteException
    {
    }
    /**
         * It is called when it gets local error or gets SIP error from network.
         *
         * @param reqId the request ID which returned by requestCapability or
         * requestAvailability
         * @param resultCode the result code which defined in RcsManager.ResultCode.
         *
         * @see RcsPresence#requestCapability
         * @see RcsPresence#requestAvailability
         * @see RcsManager.ResultCode
         */
    @Override public void onError(int reqId, int resultCode) throws android.os.RemoteException
    {
    }
    /**
         * It is called when it gets the "terminated notify" from network.
         * The presence service will not receive notify for the request any more after it got
         * "terminated notify" from network. So onFinish means the requestCapability or
         * requestAvailability had been finished completely.
         *
         * @param reqId the request ID which returned by requestCapability or
         * requestAvailability
         *
         * @see RcsPresence#requestCapability
         * @see RcsPresence#requestAvailability
         */
    @Override public void onFinish(int reqId) throws android.os.RemoteException
    {
    }
    /**
         * It is called when it is timeout to wait for the "terminated notify" from network.
         *
         * @param reqId the request ID which returned by requestCapability or
         * requestAvailability
         *
         * @see RcsPresence#requestCapability
         * @see RcsPresence#requestAvailability
         */
    @Override public void onTimeout(int reqId) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements com.android.ims.IRcsPresenceListener
  {
    private static final java.lang.String DESCRIPTOR = "com.android.ims.IRcsPresenceListener";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an com.android.ims.IRcsPresenceListener interface,
     * generating a proxy if needed.
     */
    public static com.android.ims.IRcsPresenceListener asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof com.android.ims.IRcsPresenceListener))) {
        return ((com.android.ims.IRcsPresenceListener)iin);
      }
      return new com.android.ims.IRcsPresenceListener.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onSuccess:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onSuccess(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onError:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.onError(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onFinish:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onFinish(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_onTimeout:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onTimeout(_arg0);
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements com.android.ims.IRcsPresenceListener
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * It is called when it gets "200 OK" from network for SIP request or
           * it gets "404 xxxx" for SIP request of single contact number (means not capable).
           *
           * @param reqId the request ID which returned by requestCapability or
           * requestAvailability
           *
           * @see RcsPresence#requestCapability
           * @see RcsPresence#requestAvailability
           */
      @Override public void onSuccess(int reqId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(reqId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onSuccess, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onSuccess(reqId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * It is called when it gets local error or gets SIP error from network.
           *
           * @param reqId the request ID which returned by requestCapability or
           * requestAvailability
           * @param resultCode the result code which defined in RcsManager.ResultCode.
           *
           * @see RcsPresence#requestCapability
           * @see RcsPresence#requestAvailability
           * @see RcsManager.ResultCode
           */
      @Override public void onError(int reqId, int resultCode) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(reqId);
          _data.writeInt(resultCode);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onError, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onError(reqId, resultCode);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * It is called when it gets the "terminated notify" from network.
           * The presence service will not receive notify for the request any more after it got
           * "terminated notify" from network. So onFinish means the requestCapability or
           * requestAvailability had been finished completely.
           *
           * @param reqId the request ID which returned by requestCapability or
           * requestAvailability
           *
           * @see RcsPresence#requestCapability
           * @see RcsPresence#requestAvailability
           */
      @Override public void onFinish(int reqId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(reqId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onFinish, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onFinish(reqId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /**
           * It is called when it is timeout to wait for the "terminated notify" from network.
           *
           * @param reqId the request ID which returned by requestCapability or
           * requestAvailability
           *
           * @see RcsPresence#requestCapability
           * @see RcsPresence#requestAvailability
           */
      @Override public void onTimeout(int reqId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(reqId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onTimeout, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onTimeout(reqId);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static com.android.ims.IRcsPresenceListener sDefaultImpl;
    }
    static final int TRANSACTION_onSuccess = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onError = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_onFinish = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_onTimeout = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    public static boolean setDefaultImpl(com.android.ims.IRcsPresenceListener impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static com.android.ims.IRcsPresenceListener getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /**
       * It is called when it gets "200 OK" from network for SIP request or
       * it gets "404 xxxx" for SIP request of single contact number (means not capable).
       *
       * @param reqId the request ID which returned by requestCapability or
       * requestAvailability
       *
       * @see RcsPresence#requestCapability
       * @see RcsPresence#requestAvailability
       */
  public void onSuccess(int reqId) throws android.os.RemoteException;
  /**
       * It is called when it gets local error or gets SIP error from network.
       *
       * @param reqId the request ID which returned by requestCapability or
       * requestAvailability
       * @param resultCode the result code which defined in RcsManager.ResultCode.
       *
       * @see RcsPresence#requestCapability
       * @see RcsPresence#requestAvailability
       * @see RcsManager.ResultCode
       */
  public void onError(int reqId, int resultCode) throws android.os.RemoteException;
  /**
       * It is called when it gets the "terminated notify" from network.
       * The presence service will not receive notify for the request any more after it got
       * "terminated notify" from network. So onFinish means the requestCapability or
       * requestAvailability had been finished completely.
       *
       * @param reqId the request ID which returned by requestCapability or
       * requestAvailability
       *
       * @see RcsPresence#requestCapability
       * @see RcsPresence#requestAvailability
       */
  public void onFinish(int reqId) throws android.os.RemoteException;
  /**
       * It is called when it is timeout to wait for the "terminated notify" from network.
       *
       * @param reqId the request ID which returned by requestCapability or
       * requestAvailability
       *
       * @see RcsPresence#requestCapability
       * @see RcsPresence#requestAvailability
       */
  public void onTimeout(int reqId) throws android.os.RemoteException;
}
