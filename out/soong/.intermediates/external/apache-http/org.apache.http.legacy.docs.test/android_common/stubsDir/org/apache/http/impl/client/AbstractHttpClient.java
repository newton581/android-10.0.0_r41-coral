/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/client/AbstractHttpClient.java $
 * $Revision: 677250 $
 * $Date: 2008-07-16 04:45:47 -0700 (Wed, 16 Jul 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.client;

import java.net.URI;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.protocol.HttpContext;
import org.apache.http.client.HttpClient;

/**
 * Convenience base class for HTTP client implementations.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version   $Revision: 677250 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract class AbstractHttpClient implements org.apache.http.client.HttpClient {

/**
 * Creates a new HTTP client.
 *
 * @param conman    the connection manager
 * @param params    the parameters
 */

@Deprecated
protected AbstractHttpClient(org.apache.http.conn.ClientConnectionManager conman, org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

@Deprecated
protected abstract org.apache.http.params.HttpParams createHttpParams();

@Deprecated
protected abstract org.apache.http.protocol.HttpContext createHttpContext();

@Deprecated
protected abstract org.apache.http.protocol.HttpRequestExecutor createRequestExecutor();

@Deprecated
protected abstract org.apache.http.conn.ClientConnectionManager createClientConnectionManager();

@Deprecated
protected abstract org.apache.http.auth.AuthSchemeRegistry createAuthSchemeRegistry();

@Deprecated
protected abstract org.apache.http.cookie.CookieSpecRegistry createCookieSpecRegistry();

@Deprecated
protected abstract org.apache.http.ConnectionReuseStrategy createConnectionReuseStrategy();

@Deprecated
protected abstract org.apache.http.conn.ConnectionKeepAliveStrategy createConnectionKeepAliveStrategy();

@Deprecated
protected abstract org.apache.http.protocol.BasicHttpProcessor createHttpProcessor();

@Deprecated
protected abstract org.apache.http.client.HttpRequestRetryHandler createHttpRequestRetryHandler();

@Deprecated
protected abstract org.apache.http.client.RedirectHandler createRedirectHandler();

@Deprecated
protected abstract org.apache.http.client.AuthenticationHandler createTargetAuthenticationHandler();

@Deprecated
protected abstract org.apache.http.client.AuthenticationHandler createProxyAuthenticationHandler();

@Deprecated
protected abstract org.apache.http.client.CookieStore createCookieStore();

@Deprecated
protected abstract org.apache.http.client.CredentialsProvider createCredentialsProvider();

@Deprecated
protected abstract org.apache.http.conn.routing.HttpRoutePlanner createHttpRoutePlanner();

@Deprecated
protected abstract org.apache.http.client.UserTokenHandler createUserTokenHandler();

@Deprecated
public final synchronized org.apache.http.params.HttpParams getParams() { throw new RuntimeException("Stub!"); }

/**
 * Replaces the parameters.
 * The implementation here does not update parameters of dependent objects.
 *
 * @param params    the new default parameters
 */

@Deprecated
public synchronized void setParams(org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.conn.ClientConnectionManager getConnectionManager() { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.protocol.HttpRequestExecutor getRequestExecutor() { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.auth.AuthSchemeRegistry getAuthSchemes() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setAuthSchemes(org.apache.http.auth.AuthSchemeRegistry authSchemeRegistry) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.cookie.CookieSpecRegistry getCookieSpecs() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setCookieSpecs(org.apache.http.cookie.CookieSpecRegistry cookieSpecRegistry) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.ConnectionReuseStrategy getConnectionReuseStrategy() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setReuseStrategy(org.apache.http.ConnectionReuseStrategy reuseStrategy) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.conn.ConnectionKeepAliveStrategy getConnectionKeepAliveStrategy() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setKeepAliveStrategy(org.apache.http.conn.ConnectionKeepAliveStrategy keepAliveStrategy) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.client.HttpRequestRetryHandler getHttpRequestRetryHandler() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setHttpRequestRetryHandler(org.apache.http.client.HttpRequestRetryHandler retryHandler) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.client.RedirectHandler getRedirectHandler() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setRedirectHandler(org.apache.http.client.RedirectHandler redirectHandler) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.client.AuthenticationHandler getTargetAuthenticationHandler() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setTargetAuthenticationHandler(org.apache.http.client.AuthenticationHandler targetAuthHandler) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.client.AuthenticationHandler getProxyAuthenticationHandler() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setProxyAuthenticationHandler(org.apache.http.client.AuthenticationHandler proxyAuthHandler) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.client.CookieStore getCookieStore() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setCookieStore(org.apache.http.client.CookieStore cookieStore) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.client.CredentialsProvider getCredentialsProvider() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setCredentialsProvider(org.apache.http.client.CredentialsProvider credsProvider) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.conn.routing.HttpRoutePlanner getRoutePlanner() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setRoutePlanner(org.apache.http.conn.routing.HttpRoutePlanner routePlanner) { throw new RuntimeException("Stub!"); }

@Deprecated
public final synchronized org.apache.http.client.UserTokenHandler getUserTokenHandler() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void setUserTokenHandler(org.apache.http.client.UserTokenHandler userTokenHandler) { throw new RuntimeException("Stub!"); }

@Deprecated
protected final synchronized org.apache.http.protocol.BasicHttpProcessor getHttpProcessor() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void addResponseInterceptor(org.apache.http.HttpResponseInterceptor itcp) { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void addResponseInterceptor(org.apache.http.HttpResponseInterceptor itcp, int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized org.apache.http.HttpResponseInterceptor getResponseInterceptor(int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized int getResponseInterceptorCount() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void clearResponseInterceptors() { throw new RuntimeException("Stub!"); }

@Deprecated
public void removeResponseInterceptorByClass(java.lang.Class<? extends org.apache.http.HttpResponseInterceptor> clazz) { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void addRequestInterceptor(org.apache.http.HttpRequestInterceptor itcp) { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void addRequestInterceptor(org.apache.http.HttpRequestInterceptor itcp, int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized org.apache.http.HttpRequestInterceptor getRequestInterceptor(int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized int getRequestInterceptorCount() { throw new RuntimeException("Stub!"); }

@Deprecated
public synchronized void clearRequestInterceptors() { throw new RuntimeException("Stub!"); }

@Deprecated
public void removeRequestInterceptorByClass(java.lang.Class<? extends org.apache.http.HttpRequestInterceptor> clazz) { throw new RuntimeException("Stub!"); }

@Deprecated
public final org.apache.http.HttpResponse execute(org.apache.http.client.methods.HttpUriRequest request) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Maps to {@link HttpClient#execute(HttpHost,HttpRequest,HttpContext)
 *                           execute(target, request, context)}.
 * The target is determined from the URI of the request.
 *
 * @param request   the request to execute
 * @param context   the request-specific execution context,
 *                  or <code>null</code> to use a default context
 */

@Deprecated
public final org.apache.http.HttpResponse execute(org.apache.http.client.methods.HttpUriRequest request, org.apache.http.protocol.HttpContext context) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public final org.apache.http.HttpResponse execute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public final org.apache.http.HttpResponse execute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.protocol.HttpContext context) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
protected org.apache.http.client.RequestDirector createClientRequestDirector(org.apache.http.protocol.HttpRequestExecutor requestExec, org.apache.http.conn.ClientConnectionManager conman, org.apache.http.ConnectionReuseStrategy reustrat, org.apache.http.conn.ConnectionKeepAliveStrategy kastrat, org.apache.http.conn.routing.HttpRoutePlanner rouplan, org.apache.http.protocol.HttpProcessor httpProcessor, org.apache.http.client.HttpRequestRetryHandler retryHandler, org.apache.http.client.RedirectHandler redirectHandler, org.apache.http.client.AuthenticationHandler targetAuthHandler, org.apache.http.client.AuthenticationHandler proxyAuthHandler, org.apache.http.client.UserTokenHandler stateHandler, org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

/**
 * Obtains parameters for executing a request.
 * The default implementation in this class creates a new
 * {@link ClientParamsStack} from the request parameters
 * and the client parameters.
 * <br/>
 * This method is called by the default implementation of
 * {@link #execute(HttpHost,HttpRequest,HttpContext)}
 * to obtain the parameters for the
 * {@link DefaultRequestDirector}.
 *
 * @param req    the request that will be executed
 *
 * @return  the parameters to use
 */

@Deprecated
protected org.apache.http.params.HttpParams determineParams(org.apache.http.HttpRequest req) { throw new RuntimeException("Stub!"); }

@Deprecated
public <T> T execute(org.apache.http.client.methods.HttpUriRequest request, org.apache.http.client.ResponseHandler<? extends T> responseHandler) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public <T> T execute(org.apache.http.client.methods.HttpUriRequest request, org.apache.http.client.ResponseHandler<? extends T> responseHandler, org.apache.http.protocol.HttpContext context) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public <T> T execute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.client.ResponseHandler<? extends T> responseHandler) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public <T> T execute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.client.ResponseHandler<? extends T> responseHandler, org.apache.http.protocol.HttpContext context) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }
}

