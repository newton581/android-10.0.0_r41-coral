/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/entity/HttpEntityWrapper.java $
 * $Revision: 496070 $
 * $Date: 2007-01-14 04:18:34 -0800 (Sun, 14 Jan 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.entity;


/**
 * Base class for wrapping entities.
 * Keeps a {@link #wrappedEntity wrappedEntity} and delegates all
 * calls to it. Implementations of wrapping entities can derive
 * from this class and need to override only those methods that
 * should not be delegated to the wrapped entity.
 *
 * @version $Revision: 496070 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class HttpEntityWrapper implements org.apache.http.HttpEntity {

/**
 * Creates a new entity wrapper.
 *
 * @param wrapped   the entity to wrap
 */

@Deprecated
public HttpEntityWrapper(org.apache.http.HttpEntity wrapped) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isRepeatable() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isChunked() { throw new RuntimeException("Stub!"); }

@Deprecated
public long getContentLength() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.Header getContentType() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.Header getContentEncoding() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.io.InputStream getContent() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void writeTo(java.io.OutputStream outstream) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isStreaming() { throw new RuntimeException("Stub!"); }

@Deprecated
public void consumeContent() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/** The wrapped entity. */

@Deprecated protected org.apache.http.HttpEntity wrappedEntity;
}

