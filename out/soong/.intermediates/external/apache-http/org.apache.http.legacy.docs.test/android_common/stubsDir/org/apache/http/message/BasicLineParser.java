/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/message/BasicLineParser.java $
 * $Revision: 591798 $
 * $Date: 2007-11-04 08:19:29 -0800 (Sun, 04 Nov 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.message;

import org.apache.http.protocol.HTTP;
import org.apache.http.ParseException;

/**
 * Basic parser for lines in the head section of an HTTP message.
 * There are individual methods for parsing a request line, a
 * status line, or a header line.
 * The lines to parse are passed in memory, the parser does not depend
 * on any specific IO mechanism.
 * Instances of this class are stateless and thread-safe.
 * Derived classes MUST maintain these properties.
 *
 * <p>
 * Note: This class was created by refactoring parsing code located in
 * various other classes. The author tags from those other classes have
 * been replicated here, although the association with the parsing code
 * taken from there has not been traced.
 * </p>
 *
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:oleg@ural.ru">Oleg Kalnichevski</a>
 * @author and others
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicLineParser implements org.apache.http.message.LineParser {

/**
 * Creates a new line parser for the given HTTP-like protocol.
 *
 * @param proto     a version of the protocol to parse, or
 *                  <code>null</code> for HTTP. The actual version
 *                  is not relevant, only the protocol name.
 */

@Deprecated
public BasicLineParser(org.apache.http.ProtocolVersion proto) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new line parser for HTTP.
 */

@Deprecated
public BasicLineParser() { throw new RuntimeException("Stub!"); }

@Deprecated
public static final org.apache.http.ProtocolVersion parseProtocolVersion(java.lang.String value, org.apache.http.message.LineParser parser) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.ProtocolVersion parseProtocolVersion(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.message.ParserCursor cursor) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

/**
 * Creates a protocol version.
 * Called from {@link #parseProtocolVersion}.
 *
 * @param major     the major version number, for example 1 in HTTP/1.0
 * @param minor     the minor version number, for example 0 in HTTP/1.0
 *
 * @return  the protocol version
 */

@Deprecated
protected org.apache.http.ProtocolVersion createProtocolVersion(int major, int minor) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean hasProtocolVersion(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.message.ParserCursor cursor) { throw new RuntimeException("Stub!"); }

@Deprecated
public static final org.apache.http.RequestLine parseRequestLine(java.lang.String value, org.apache.http.message.LineParser parser) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

/**
 * Parses a request line.
 *
 * @param buffer    a buffer holding the line to parse
 *
 * @return  the parsed request line
 *
 * @throws ParseException        in case of a parse error
 */

@Deprecated
public org.apache.http.RequestLine parseRequestLine(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.message.ParserCursor cursor) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

/**
 * Instantiates a new request line.
 * Called from {@link #parseRequestLine}.
 *
 * @param method    the request method
 * @param uri       the requested URI
 * @param ver       the protocol version
 *
 * @return  a new status line with the given data
 */

@Deprecated
protected org.apache.http.RequestLine createRequestLine(java.lang.String method, java.lang.String uri, org.apache.http.ProtocolVersion ver) { throw new RuntimeException("Stub!"); }

@Deprecated
public static final org.apache.http.StatusLine parseStatusLine(java.lang.String value, org.apache.http.message.LineParser parser) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.StatusLine parseStatusLine(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.message.ParserCursor cursor) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

/**
 * Instantiates a new status line.
 * Called from {@link #parseStatusLine}.
 *
 * @param ver       the protocol version
 * @param status    the status code
 * @param reason    the reason phrase
 *
 * @return  a new status line with the given data
 */

@Deprecated
protected org.apache.http.StatusLine createStatusLine(org.apache.http.ProtocolVersion ver, int status, java.lang.String reason) { throw new RuntimeException("Stub!"); }

@Deprecated
public static final org.apache.http.Header parseHeader(java.lang.String value, org.apache.http.message.LineParser parser) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.Header parseHeader(org.apache.http.util.CharArrayBuffer buffer) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

/**
 * Helper to skip whitespace.
 */

@Deprecated
protected void skipWhitespace(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.message.ParserCursor cursor) { throw new RuntimeException("Stub!"); }

/**
 * A default instance of this class, for use as default or fallback.
 * Note that {@link BasicLineParser} is not a singleton, there can
 * be many instances of the class itself and of derived classes.
 * The instance here provides non-customized, default behavior.
 */

@Deprecated public static final org.apache.http.message.BasicLineParser DEFAULT;
static { DEFAULT = null; }

/**
 * A version of the protocol to parse.
 * The version is typically not relevant, but the protocol name.
 */

@Deprecated protected final org.apache.http.ProtocolVersion protocol;
{ protocol = null; }
}

