/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.language;

import org.apache.commons.codec.EncoderException;

/**
 * Encodes a string into a Soundex value. Soundex is an encoding used to relate similar names, but can also be used as a
 * general purpose scheme to find word with similar phonemes.
 *
 * @author Apache Software Foundation
 * @version $Id: Soundex.java,v 1.26 2004/07/07 23:15:24 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class Soundex implements org.apache.commons.codec.StringEncoder {

/**
 * Creates an instance using US_ENGLISH_MAPPING
 *
 * @see Soundex#Soundex(char[])
 * @see Soundex#US_ENGLISH_MAPPING
 */

@Deprecated
public Soundex() { throw new RuntimeException("Stub!"); }

/**
 * Creates a soundex instance using the given mapping. This constructor can be used to provide an internationalized
 * mapping for a non-Western character set.
 *
 * Every letter of the alphabet is "mapped" to a numerical value. This char array holds the values to which each
 * letter is mapped. This implementation contains a default map for US_ENGLISH
 *
 * @param mapping
 *                  Mapping array to use when finding the corresponding code for a given character
 */

@Deprecated
public Soundex(char[] mapping) { throw new RuntimeException("Stub!"); }

/**
 * Encodes the Strings and returns the number of characters in the two encoded Strings that are the same. This
 * return value ranges from 0 through 4: 0 indicates little or no similarity, and 4 indicates strong similarity or
 * identical values.
 *
 * @param s1
 *                  A String that will be encoded and compared.
 * @param s2
 *                  A String that will be encoded and compared.
 * @return The number of characters in the two encoded Strings that are the same from 0 to 4.
 *
 * @see <a href="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/tsqlref/ts_de-dz_8co5.asp"> MS
 *          T-SQL DIFFERENCE </a>
 *
 * @throws EncoderException
 *                  if an error occurs encoding one of the strings
 * @since 1.3
 */

@Deprecated
public int difference(java.lang.String s1, java.lang.String s2) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes an Object using the soundex algorithm. This method is provided in order to satisfy the requirements of
 * the Encoder interface, and will throw an EncoderException if the supplied object is not of type java.lang.String.
 *
 * @param pObject
 *                  Object to encode
 * @return An object (or type java.lang.String) containing the soundex code which corresponds to the String
 *             supplied.
 * @throws EncoderException
 *                  if the parameter supplied is not of type java.lang.String
 * @throws IllegalArgumentException
 *                  if a character is not mapped
 */

@Deprecated
public java.lang.Object encode(java.lang.Object pObject) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a String using the soundex algorithm.
 *
 * @param pString
 *                  A String object to encode
 * @return A Soundex code corresponding to the String supplied
 * @throws IllegalArgumentException
 *                  if a character is not mapped
 */

@Deprecated
public java.lang.String encode(java.lang.String pString) { throw new RuntimeException("Stub!"); }

/**
 * Returns the maxLength. Standard Soundex
 *
 * @deprecated This feature is not needed since the encoding size must be constant. Will be removed in 2.0.
 * @return int
 */

@Deprecated
public int getMaxLength() { throw new RuntimeException("Stub!"); }

/**
 * Sets the maxLength.
 *
 * @deprecated This feature is not needed since the encoding size must be constant. Will be removed in 2.0.
 * @param maxLength
 *                  The maxLength to set
 */

@Deprecated
public void setMaxLength(int maxLength) { throw new RuntimeException("Stub!"); }

/**
 * Retreives the Soundex code for a given String object.
 *
 * @param str
 *                  String to encode using the Soundex algorithm
 * @return A soundex code for the String supplied
 * @throws IllegalArgumentException
 *                  if a character is not mapped
 */

@Deprecated
public java.lang.String soundex(java.lang.String str) { throw new RuntimeException("Stub!"); }

/**
 * An instance of Soundex using the US_ENGLISH_MAPPING mapping.
 *
 * @see #US_ENGLISH_MAPPING
 */

@Deprecated public static final org.apache.commons.codec.language.Soundex US_ENGLISH;
static { US_ENGLISH = null; }

/**
 * This is a default mapping of the 26 letters used in US English. A value of <code>0</code> for a letter position
 * means do not encode.
 *
 * @see Soundex#Soundex(char[])
 */

@Deprecated public static final char[] US_ENGLISH_MAPPING;
static { US_ENGLISH_MAPPING = new char[0]; }

/**
 * This is a default mapping of the 26 letters used in US English. A value of <code>0</code> for a letter position
 * means do not encode.
 * <p>
 * (This constant is provided as both an implementation convenience and to allow Javadoc to pick
 * up the value for the constant values page.)
 * </p>
 *
 * @see #US_ENGLISH_MAPPING
 */

@Deprecated public static final java.lang.String US_ENGLISH_MAPPING_STRING = "01230120022455012623010202";
}

