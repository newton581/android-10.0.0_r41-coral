/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 



package org.apache.commons.logging.impl;

import org.apache.commons.logging.Log;

/**
 * <p>Trivial implementation of Log that throws away all messages.  No
 * configurable system properties are supported.</p>
 *
 * @author <a href="mailto:sanders@apache.org">Scott Sanders</a>
 * @author Rod Waldhoff
 * @version $Id: NoOpLog.java 155426 2005-02-26 13:10:49Z dirkv $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class NoOpLog implements org.apache.commons.logging.Log, java.io.Serializable {

/** Convenience constructor */

@Deprecated
public NoOpLog() { throw new RuntimeException("Stub!"); }

/** Base constructor */

@Deprecated
public NoOpLog(java.lang.String name) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void trace(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void trace(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void debug(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void debug(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void info(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void info(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void warn(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void warn(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void error(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void error(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void fatal(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/** Do nothing */

@Deprecated
public void fatal(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/**
 * Debug is never enabled.
 *
 * @return false
 */

@Deprecated
public final boolean isDebugEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Error is never enabled.
 *
 * @return false
 */

@Deprecated
public final boolean isErrorEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Fatal is never enabled.
 *
 * @return false
 */

@Deprecated
public final boolean isFatalEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Info is never enabled.
 *
 * @return false
 */

@Deprecated
public final boolean isInfoEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Trace is never enabled.
 *
 * @return false
 */

@Deprecated
public final boolean isTraceEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Warn is never enabled.
 *
 * @return false
 */

@Deprecated
public final boolean isWarnEnabled() { throw new RuntimeException("Stub!"); }
}

