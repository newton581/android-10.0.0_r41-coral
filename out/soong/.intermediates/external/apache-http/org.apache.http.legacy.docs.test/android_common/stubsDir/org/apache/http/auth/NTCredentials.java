/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/auth/NTCredentials.java $
 * $Revision: 658430 $
 * $Date: 2008-05-20 14:04:27 -0700 (Tue, 20 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.auth;


/** {@link Credentials} specific to the Windows platform.
 *
 * @author <a href="mailto:adrian@ephox.com">Adrian Sutton</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @since 2.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class NTCredentials implements org.apache.http.auth.Credentials {

/**
 * The constructor with the fully qualified username and password combined
 * string argument.
 *
 * @param usernamePassword the domain/username:password formed string
 */

@Deprecated
public NTCredentials(java.lang.String usernamePassword) { throw new RuntimeException("Stub!"); }

/**
 * Constructor.
 * @param userName The user name.  This should not include the domain to authenticate with.
 * For example: "user" is correct whereas "DOMAIN\\user" is not.
 * @param password The password.
 * @param workstation The workstation the authentication request is originating from.
 * Essentially, the computer name for this machine.
 * @param domain The domain to authenticate within.
 */

@Deprecated
public NTCredentials(java.lang.String userName, java.lang.String password, java.lang.String workstation, java.lang.String domain) { throw new RuntimeException("Stub!"); }

@Deprecated
public java.security.Principal getUserPrincipal() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String getUserName() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String getPassword() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the name to authenticate with.
 *
 * @return String the domain these credentials are intended to authenticate with.
 */

@Deprecated
public java.lang.String getDomain() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the workstation name of the computer originating the request.
 *
 * @return String the workstation the user is logged into.
 */

@Deprecated
public java.lang.String getWorkstation() { throw new RuntimeException("Stub!"); }

@Deprecated
public int hashCode() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

