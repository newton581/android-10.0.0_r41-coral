/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.net;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.DecoderException;

/**
 * <p>
 * Identical to the Base64 encoding defined by <a href="http://www.ietf.org/rfc/rfc1521.txt">RFC
 * 1521</a> and allows a character set to be specified.
 * </p>
 *
 * <p>
 * <a href="http://www.ietf.org/rfc/rfc1522.txt">RFC 1522</a> describes techniques to allow the encoding of non-ASCII
 * text in various portions of a RFC 822 [2] message header, in a manner which is unlikely to confuse existing message
 * handling software.
 * </p>
 *
 * @see <a href="http://www.ietf.org/rfc/rfc1522.txt">MIME (Multipurpose Internet Mail Extensions) Part Two: Message
 *          Header Extensions for Non-ASCII Text</a>
 *
 * @author Apache Software Foundation
 * @since 1.3
 * @version $Id: BCodec.java,v 1.5 2004/04/13 22:46:37 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BCodec implements org.apache.commons.codec.StringEncoder, org.apache.commons.codec.StringDecoder {

/**
 * Default constructor.
 */

@Deprecated
public BCodec() { throw new RuntimeException("Stub!"); }

/**
 * Constructor which allows for the selection of a default charset
 *
 * @param charset
 *                  the default string charset to use.
 *
 * @see <a href="http://java.sun.com/j2se/1.3/docs/api/java/lang/package-summary.html#charenc">JRE character
 *          encoding names</a>
 */

@Deprecated
public BCodec(java.lang.String charset) { throw new RuntimeException("Stub!"); }

@Deprecated
protected java.lang.String getEncoding() { throw new RuntimeException("Stub!"); }

@Deprecated
protected byte[] doEncoding(byte[] bytes) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

@Deprecated
protected byte[] doDecoding(byte[] bytes) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a string into its Base64 form using the specified charset. Unsafe characters are escaped.
 *
 * @param value
 *                  string to convert to Base64 form
 * @param charset
 *                  the charset for pString
 * @return Base64 string
 *
 * @throws EncoderException
 *                  thrown if a failure condition is encountered during the encoding process.
 */

@Deprecated
public java.lang.String encode(java.lang.String value, java.lang.String charset) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a string into its Base64 form using the default charset. Unsafe characters are escaped.
 *
 * @param value
 *                  string to convert to Base64 form
 * @return Base64 string
 *
 * @throws EncoderException
 *                  thrown if a failure condition is encountered during the encoding process.
 */

@Deprecated
public java.lang.String encode(java.lang.String value) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a Base64 string into its original form. Escaped characters are converted back to their original
 * representation.
 *
 * @param value
 *                  Base64 string to convert into its original form
 *
 * @return original string
 *
 * @throws DecoderException
 *                  A decoder exception is thrown if a failure condition is encountered during the decode process.
 */

@Deprecated
public java.lang.String decode(java.lang.String value) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes an object into its Base64 form using the default charset. Unsafe characters are escaped.
 *
 * @param value
 *                  object to convert to Base64 form
 * @return Base64 object
 *
 * @throws EncoderException
 *                  thrown if a failure condition is encountered during the encoding process.
 */

@Deprecated
public java.lang.Object encode(java.lang.Object value) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a Base64 object into its original form. Escaped characters are converted back to their original
 * representation.
 *
 * @param value
 *                  Base64 object to convert into its original form
 *
 * @return original object
 *
 * @throws DecoderException
 *                  A decoder exception is thrown if a failure condition is encountered during the decode process.
 */

@Deprecated
public java.lang.Object decode(java.lang.Object value) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * The default charset used for string decoding and encoding.
 *
 * @return the default string charset.
 */

@Deprecated
public java.lang.String getDefaultCharset() { throw new RuntimeException("Stub!"); }
}

