/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/params/BasicHttpParams.java $
 * $Revision: 610464 $
 * $Date: 2008-01-09 09:10:55 -0800 (Wed, 09 Jan 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.params;

import org.apache.http.params.HttpParams;
import java.util.Map;

/**
 * This class represents a collection of HTTP protocol parameters.
 * Protocol parameters may be linked together to form a hierarchy.
 * If a particular parameter value has not been explicitly defined
 * in the collection itself, its value will be drawn from the parent
 * collection of parameters.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 610464 $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class BasicHttpParams extends org.apache.http.params.AbstractHttpParams implements java.io.Serializable, java.lang.Cloneable {

@Deprecated
public BasicHttpParams() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object getParameter(java.lang.String name) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.params.HttpParams setParameter(java.lang.String name, java.lang.Object value) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean removeParameter(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Assigns the value to all the parameter with the given names
 *
 * @param names array of parameter name
 * @param value parameter value
 */

@Deprecated
public void setParameters(java.lang.String[] names, java.lang.Object value) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isParameterSet(java.lang.String name) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isParameterSetLocally(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Removes all parameters from this collection.
 */

@Deprecated
public void clear() { throw new RuntimeException("Stub!"); }

/**
 * Creates a copy of these parameters.
 * The implementation here instantiates {@link BasicHttpParams},
 * then calls {@link #copyParams(HttpParams)} to populate the copy.
 *
 * @return  a new set of params holding a copy of the
 *          <i>local</i> parameters in this object.
 */

@Deprecated
public org.apache.http.params.HttpParams copy() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object clone() throws java.lang.CloneNotSupportedException { throw new RuntimeException("Stub!"); }

/**
 * Copies the locally defined parameters to the argument parameters.
 * This method is called from {@link #copy()}.
 *
 * @param target    the parameters to which to copy
 */

@Deprecated
protected void copyParams(org.apache.http.params.HttpParams target) { throw new RuntimeException("Stub!"); }
}

