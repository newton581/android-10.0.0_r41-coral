/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/HttpVersion.java $
 * $Revision: 609106 $
 * $Date: 2008-01-05 01:15:42 -0800 (Sat, 05 Jan 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http;


/**
 * Represents an HTTP version, as specified in RFC 2616.
 *
 * @author <a href="mailto:oleg@ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 609106 $ $Date: 2008-01-05 01:15:42 -0800 (Sat, 05 Jan 2008) $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class HttpVersion extends org.apache.http.ProtocolVersion implements java.io.Serializable {

/**
 * Create an HTTP protocol version designator.
 *
 * @param major   the major version number of the HTTP protocol
 * @param minor   the minor version number of the HTTP protocol
 *
 * @throws IllegalArgumentException if either major or minor version number is negative
 */

@Deprecated
public HttpVersion(int major, int minor) { super(null, 0, 0); throw new RuntimeException("Stub!"); }

/**
 * Obtains a specific HTTP version.
 *
 * @param major     the major version
 * @param minor     the minor version
 *
 * @return  an instance of {@link HttpVersion} with the argument version
 */

@Deprecated
public org.apache.http.ProtocolVersion forVersion(int major, int minor) { throw new RuntimeException("Stub!"); }

/** The protocol name. */

@Deprecated public static final java.lang.String HTTP = "HTTP";

/** HTTP protocol version 0.9 */

@Deprecated public static final org.apache.http.HttpVersion HTTP_0_9;
static { HTTP_0_9 = null; }

/** HTTP protocol version 1.0 */

@Deprecated public static final org.apache.http.HttpVersion HTTP_1_0;
static { HTTP_1_0 = null; }

/** HTTP protocol version 1.1 */

@Deprecated public static final org.apache.http.HttpVersion HTTP_1_1;
static { HTTP_1_1 = null; }
}

