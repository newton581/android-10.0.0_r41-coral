/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.http;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.client.HttpClient;
import java.io.IOException;

/**
 * Implementation of the Apache {@link DefaultHttpClient} that is configured with
 * reasonable default settings and registered schemes for Android.
 * Don't create this directly, use the {@link #newInstance} factory method.
 *
 * <p>This client processes cookies but does not retain them by default.
 * To retain cookies, simply add a cookie store to the HttpContext:</p>
 *
 * <pre>context.setAttribute(ClientContext.COOKIE_STORE, cookieStore);</pre>
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AndroidHttpClient implements org.apache.http.client.HttpClient {

AndroidHttpClient(org.apache.http.conn.ClientConnectionManager ccm, org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

/**
 * Create a new HttpClient with reasonable defaults (which you can update).
 *
 * @param userAgent to report in your HTTP requests
 * @param context to use for caching SSL sessions (may be null for no caching)
 * @return AndroidHttpClient for you to use for all your requests.
 */

public static android.net.http.AndroidHttpClient newInstance(java.lang.String userAgent, android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Create a new HttpClient with reasonable defaults (which you can update).
 * @param userAgent to report in your HTTP requests.
 * @return AndroidHttpClient for you to use for all your requests.
 */

public static android.net.http.AndroidHttpClient newInstance(java.lang.String userAgent) { throw new RuntimeException("Stub!"); }

protected void finalize() throws java.lang.Throwable { throw new RuntimeException("Stub!"); }

/**
 * Modifies a request to indicate to the server that we would like a
 * gzipped response.  (Uses the "Accept-Encoding" HTTP header.)
 * @param request the request to modify
 * @see #getUngzippedContent
 */

public static void modifyRequestToAcceptGzipResponse(org.apache.http.HttpRequest request) { throw new RuntimeException("Stub!"); }

/**
 * Gets the input stream from a response entity.  If the entity is gzipped
 * then this will get a stream over the uncompressed data.
 *
 * @param entity the entity whose content should be read
 * @return the input stream to read from
 * @throws IOException
 */

public static java.io.InputStream getUngzippedContent(org.apache.http.HttpEntity entity) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Release resources associated with this client.  You must call this,
 * or significant resources (sockets and memory) may be leaked.
 */

public void close() { throw new RuntimeException("Stub!"); }

public org.apache.http.params.HttpParams getParams() { throw new RuntimeException("Stub!"); }

public org.apache.http.conn.ClientConnectionManager getConnectionManager() { throw new RuntimeException("Stub!"); }

public org.apache.http.HttpResponse execute(org.apache.http.client.methods.HttpUriRequest request) throws java.io.IOException { throw new RuntimeException("Stub!"); }

public org.apache.http.HttpResponse execute(org.apache.http.client.methods.HttpUriRequest request, org.apache.http.protocol.HttpContext context) throws java.io.IOException { throw new RuntimeException("Stub!"); }

public org.apache.http.HttpResponse execute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request) throws java.io.IOException { throw new RuntimeException("Stub!"); }

public org.apache.http.HttpResponse execute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.protocol.HttpContext context) throws java.io.IOException { throw new RuntimeException("Stub!"); }

public <T> T execute(org.apache.http.client.methods.HttpUriRequest request, org.apache.http.client.ResponseHandler<? extends T> responseHandler) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

public <T> T execute(org.apache.http.client.methods.HttpUriRequest request, org.apache.http.client.ResponseHandler<? extends T> responseHandler, org.apache.http.protocol.HttpContext context) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

public <T> T execute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.client.ResponseHandler<? extends T> responseHandler) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

public <T> T execute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.client.ResponseHandler<? extends T> responseHandler, org.apache.http.protocol.HttpContext context) throws org.apache.http.client.ClientProtocolException, java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Compress data to send to server.
 * Creates a Http Entity holding the gzipped data.
 * The data will not be compressed if it is too short.
 * @param data The bytes to compress
 * @return Entity holding the data
 */

public static org.apache.http.entity.AbstractHttpEntity getCompressedEntity(byte[] data, android.content.ContentResolver resolver) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the minimum size for compressing data.
 * Shorter data will not be compressed.
 */

public static long getMinGzipSize(android.content.ContentResolver resolver) { throw new RuntimeException("Stub!"); }

/**
 * Enables cURL request logging for this client.
 *
 * @param name to log messages with
 * @param level at which to log messages (see {@link android.util.Log})
 */

public void enableCurlLogging(java.lang.String name, int level) { throw new RuntimeException("Stub!"); }

/**
 * Disables cURL logging for this client.
 */

public void disableCurlLogging() { throw new RuntimeException("Stub!"); }

/**
 * Returns the date of the given HTTP date string. This method can identify
 * and parse the date formats emitted by common HTTP servers, such as
 * <a href="http://www.ietf.org/rfc/rfc0822.txt">RFC 822</a>,
 * <a href="http://www.ietf.org/rfc/rfc0850.txt">RFC 850</a>,
 * <a href="http://www.ietf.org/rfc/rfc1036.txt">RFC 1036</a>,
 * <a href="http://www.ietf.org/rfc/rfc1123.txt">RFC 1123</a> and
 * <a href="http://www.opengroup.org/onlinepubs/007908799/xsh/asctime.html">ANSI
 * C's asctime()</a>.
 *
 * @return the number of milliseconds since Jan. 1, 1970, midnight GMT.
 * @throws IllegalArgumentException if {@code dateString} is not a date or
 *     of an unsupported format.
 */

public static long parseDate(java.lang.String dateString) { throw new RuntimeException("Stub!"); }

public static long DEFAULT_SYNC_MIN_GZIP_BYTES = 256; // 0x100
}

