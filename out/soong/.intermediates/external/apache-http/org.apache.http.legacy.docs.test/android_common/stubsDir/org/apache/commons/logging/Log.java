/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 



package org.apache.commons.logging;


/**
 * <p>A simple logging interface abstracting logging APIs.  In order to be
 * instantiated successfully by {@link LogFactory}, classes that implement
 * this interface must have a constructor that takes a single String
 * parameter representing the "name" of this Log.</p>
 *
 * <p> The six logging levels used by <code>Log</code> are (in order):
 * <ol>
 * <li>trace (the least serious)</li>
 * <li>debug</li>
 * <li>info</li>
 * <li>warn</li>
 * <li>error</li>
 * <li>fatal (the most serious)</li>
 * </ol>
 * The mapping of these log levels to the concepts used by the underlying
 * logging system is implementation dependent.
 * The implemention should ensure, though, that this ordering behaves
 * as expected.</p>
 *
 * <p>Performance is often a logging concern.
 * By examining the appropriate property,
 * a component can avoid expensive operations (producing information
 * to be logged).</p>
 *
 * <p> For example,
 * <code><pre>
 *    if (log.isDebugEnabled()) {
 *        ... do something expensive ...
 *        log.debug(theResult);
 *    }
 * </pre></code>
 * </p>
 *
 * <p>Configuration of the underlying logging system will generally be done
 * external to the Logging APIs, through whatever mechanism is supported by
 * that system.</p>
 *
 * @author <a href="mailto:sanders@apache.org">Scott Sanders</a>
 * @author Rod Waldhoff
 * @version $Id: Log.java 381838 2006-02-28 23:57:11Z skitching $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public interface Log {

/**
 * <p> Is debug logging currently enabled? </p>
 *
 * <p> Call this method to prevent having to perform expensive operations
 * (for example, <code>String</code> concatenation)
 * when the log level is more than debug. </p>
 *
 * @return true if debug is enabled in the underlying logger.
 */

@Deprecated
public boolean isDebugEnabled();

/**
 * <p> Is error logging currently enabled? </p>
 *
 * <p> Call this method to prevent having to perform expensive operations
 * (for example, <code>String</code> concatenation)
 * when the log level is more than error. </p>
 *
 * @return true if error is enabled in the underlying logger.
 */

@Deprecated
public boolean isErrorEnabled();

/**
 * <p> Is fatal logging currently enabled? </p>
 *
 * <p> Call this method to prevent having to perform expensive operations
 * (for example, <code>String</code> concatenation)
 * when the log level is more than fatal. </p>
 *
 * @return true if fatal is enabled in the underlying logger.
 */

@Deprecated
public boolean isFatalEnabled();

/**
 * <p> Is info logging currently enabled? </p>
 *
 * <p> Call this method to prevent having to perform expensive operations
 * (for example, <code>String</code> concatenation)
 * when the log level is more than info. </p>
 *
 * @return true if info is enabled in the underlying logger.
 */

@Deprecated
public boolean isInfoEnabled();

/**
 * <p> Is trace logging currently enabled? </p>
 *
 * <p> Call this method to prevent having to perform expensive operations
 * (for example, <code>String</code> concatenation)
 * when the log level is more than trace. </p>
 *
 * @return true if trace is enabled in the underlying logger.
 */

@Deprecated
public boolean isTraceEnabled();

/**
 * <p> Is warn logging currently enabled? </p>
 *
 * <p> Call this method to prevent having to perform expensive operations
 * (for example, <code>String</code> concatenation)
 * when the log level is more than warn. </p>
 *
 * @return true if warn is enabled in the underlying logger.
 */

@Deprecated
public boolean isWarnEnabled();

/**
 * <p> Log a message with trace log level. </p>
 *
 * @param message log this message
 */

@Deprecated
public void trace(java.lang.Object message);

/**
 * <p> Log an error with trace log level. </p>
 *
 * @param message log this message
 * @param t log this cause
 */

@Deprecated
public void trace(java.lang.Object message, java.lang.Throwable t);

/**
 * <p> Log a message with debug log level. </p>
 *
 * @param message log this message
 */

@Deprecated
public void debug(java.lang.Object message);

/**
 * <p> Log an error with debug log level. </p>
 *
 * @param message log this message
 * @param t log this cause
 */

@Deprecated
public void debug(java.lang.Object message, java.lang.Throwable t);

/**
 * <p> Log a message with info log level. </p>
 *
 * @param message log this message
 */

@Deprecated
public void info(java.lang.Object message);

/**
 * <p> Log an error with info log level. </p>
 *
 * @param message log this message
 * @param t log this cause
 */

@Deprecated
public void info(java.lang.Object message, java.lang.Throwable t);

/**
 * <p> Log a message with warn log level. </p>
 *
 * @param message log this message
 */

@Deprecated
public void warn(java.lang.Object message);

/**
 * <p> Log an error with warn log level. </p>
 *
 * @param message log this message
 * @param t log this cause
 */

@Deprecated
public void warn(java.lang.Object message, java.lang.Throwable t);

/**
 * <p> Log a message with error log level. </p>
 *
 * @param message log this message
 */

@Deprecated
public void error(java.lang.Object message);

/**
 * <p> Log an error with error log level. </p>
 *
 * @param message log this message
 * @param t log this cause
 */

@Deprecated
public void error(java.lang.Object message, java.lang.Throwable t);

/**
 * <p> Log a message with fatal log level. </p>
 *
 * @param message log this message
 */

@Deprecated
public void fatal(java.lang.Object message);

/**
 * <p> Log an error with fatal log level. </p>
 *
 * @param message log this message
 * @param t log this cause
 */

@Deprecated
public void fatal(java.lang.Object message, java.lang.Throwable t);
}

