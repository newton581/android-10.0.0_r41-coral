/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/protocol/BasicHttpProcessor.java $
 * $Revision: 613298 $
 * $Date: 2008-01-18 14:09:22 -0800 (Fri, 18 Jan 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.protocol;

import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponseInterceptor;

/**
 * Keeps lists of interceptors for processing requests and responses.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 * @author Andrea Selva
 *
 * @version $Revision: 613298 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class BasicHttpProcessor implements org.apache.http.protocol.HttpProcessor, org.apache.http.protocol.HttpRequestInterceptorList, org.apache.http.protocol.HttpResponseInterceptorList, java.lang.Cloneable {

@Deprecated
public BasicHttpProcessor() { throw new RuntimeException("Stub!"); }

@Deprecated
public void addRequestInterceptor(org.apache.http.HttpRequestInterceptor itcp) { throw new RuntimeException("Stub!"); }

@Deprecated
public void addRequestInterceptor(org.apache.http.HttpRequestInterceptor itcp, int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public void addResponseInterceptor(org.apache.http.HttpResponseInterceptor itcp, int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public void removeRequestInterceptorByClass(java.lang.Class clazz) { throw new RuntimeException("Stub!"); }

@Deprecated
public void removeResponseInterceptorByClass(java.lang.Class clazz) { throw new RuntimeException("Stub!"); }

/**
 * Same as {@link #addRequestInterceptor(HttpRequestInterceptor) addRequestInterceptor}.
 *
 * @param interceptor       the interceptor to add
 */

@Deprecated
public void addInterceptor(org.apache.http.HttpRequestInterceptor interceptor) { throw new RuntimeException("Stub!"); }

@Deprecated
public void addInterceptor(org.apache.http.HttpRequestInterceptor interceptor, int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public int getRequestInterceptorCount() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpRequestInterceptor getRequestInterceptor(int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public void clearRequestInterceptors() { throw new RuntimeException("Stub!"); }

@Deprecated
public void addResponseInterceptor(org.apache.http.HttpResponseInterceptor itcp) { throw new RuntimeException("Stub!"); }

/**
 * Same as {@link #addResponseInterceptor(HttpResponseInterceptor) addResponseInterceptor}.
 *
 * @param interceptor       the interceptor to add
 */

@Deprecated
public void addInterceptor(org.apache.http.HttpResponseInterceptor interceptor) { throw new RuntimeException("Stub!"); }

@Deprecated
public void addInterceptor(org.apache.http.HttpResponseInterceptor interceptor, int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public int getResponseInterceptorCount() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpResponseInterceptor getResponseInterceptor(int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public void clearResponseInterceptors() { throw new RuntimeException("Stub!"); }

/**
 * Sets the interceptor lists.
 * First, both interceptor lists maintained by this processor
 * will be cleared.
 * Subsequently,
 * elements of the argument list that are request interceptors will be
 * added to the request interceptor list.
 * Elements that are response interceptors will be
 * added to the response interceptor list.
 * Elements that are both request and response interceptor will be
 * added to both lists.
 * Elements that are neither request nor response interceptor
 * will be ignored.
 *
 * @param list      the list of request and response interceptors
 *                  from which to initialize
 */

@Deprecated
public void setInterceptors(java.util.List list) { throw new RuntimeException("Stub!"); }

/**
 * Clears both interceptor lists maintained by this processor.
 */

@Deprecated
public void clearInterceptors() { throw new RuntimeException("Stub!"); }

@Deprecated
public void process(org.apache.http.HttpRequest request, org.apache.http.protocol.HttpContext context) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void process(org.apache.http.HttpResponse response, org.apache.http.protocol.HttpContext context) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
protected void copyInterceptors(org.apache.http.protocol.BasicHttpProcessor target) { throw new RuntimeException("Stub!"); }

/**
 * Creates a copy of this instance
 *
 * @return new instance of the BasicHttpProcessor
 */

@Deprecated
public org.apache.http.protocol.BasicHttpProcessor copy() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object clone() throws java.lang.CloneNotSupportedException { throw new RuntimeException("Stub!"); }

@Deprecated protected java.util.List requestInterceptors;

@Deprecated protected java.util.List responseInterceptors;
}

