/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/message/BasicHeaderValueParser.java $
 * $Revision: 595670 $
 * $Date: 2007-11-16 06:15:01 -0800 (Fri, 16 Nov 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.message;


/**
 * Basic implementation for parsing header values into elements.
 * Instances of this class are stateless and thread-safe.
 * Derived classes are expected to maintain these properties.
 *
 * @author <a href="mailto:bcholmes@interlog.com">B.C. Holmes</a>
 * @author <a href="mailto:jericho@thinkfree.com">Park, Sung-Gu</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:oleg at ural.com">Oleg Kalnichevski</a>
 * @author and others
 *
 *
 * <!-- empty lines above to avoid 'svn diff' context problems -->
 * @version $Revision: 595670 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicHeaderValueParser implements org.apache.http.message.HeaderValueParser {

@Deprecated
public BasicHeaderValueParser() { throw new RuntimeException("Stub!"); }

/**
 * Parses elements with the given parser.
 *
 * @param value     the header value to parse
 * @param parser    the parser to use, or <code>null</code> for default
 *
 * @return  array holding the header elements, never <code>null</code>
 */

@Deprecated
public static final org.apache.http.HeaderElement[] parseElements(java.lang.String value, org.apache.http.message.HeaderValueParser parser) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HeaderElement[] parseElements(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.message.ParserCursor cursor) { throw new RuntimeException("Stub!"); }

/**
 * Parses an element with the given parser.
 *
 * @param value     the header element to parse
 * @param parser    the parser to use, or <code>null</code> for default
 *
 * @return  the parsed header element
 */

@Deprecated
public static final org.apache.http.HeaderElement parseHeaderElement(java.lang.String value, org.apache.http.message.HeaderValueParser parser) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HeaderElement parseHeaderElement(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.message.ParserCursor cursor) { throw new RuntimeException("Stub!"); }

/**
 * Creates a header element.
 * Called from {@link #parseHeaderElement}.
 *
 * @return  a header element representing the argument
 */

@Deprecated
protected org.apache.http.HeaderElement createHeaderElement(java.lang.String name, java.lang.String value, org.apache.http.NameValuePair[] params) { throw new RuntimeException("Stub!"); }

/**
 * Parses parameters with the given parser.
 *
 * @param value     the parameter list to parse
 * @param parser    the parser to use, or <code>null</code> for default
 *
 * @return  array holding the parameters, never <code>null</code>
 */

@Deprecated
public static final org.apache.http.NameValuePair[] parseParameters(java.lang.String value, org.apache.http.message.HeaderValueParser parser) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.NameValuePair[] parseParameters(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.message.ParserCursor cursor) { throw new RuntimeException("Stub!"); }

/**
 * Parses a name-value-pair with the given parser.
 *
 * @param value     the NVP to parse
 * @param parser    the parser to use, or <code>null</code> for default
 *
 * @return  the parsed name-value pair
 */

@Deprecated
public static final org.apache.http.NameValuePair parseNameValuePair(java.lang.String value, org.apache.http.message.HeaderValueParser parser) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.NameValuePair parseNameValuePair(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.message.ParserCursor cursor) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.NameValuePair parseNameValuePair(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.message.ParserCursor cursor, char[] delimiters) { throw new RuntimeException("Stub!"); }

/**
 * Creates a name-value pair.
 * Called from {@link #parseNameValuePair}.
 *
 * @param name      the name
 * @param value     the value, or <code>null</code>
 *
 * @return  a name-value pair representing the arguments
 */

@Deprecated
protected org.apache.http.NameValuePair createNameValuePair(java.lang.String name, java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * A default instance of this class, for use as default or fallback.
 * Note that {@link BasicHeaderValueParser} is not a singleton, there
 * can be many instances of the class itself and of derived classes.
 * The instance here provides non-customized, default behavior.
 */

@Deprecated public static final org.apache.http.message.BasicHeaderValueParser DEFAULT;
static { DEFAULT = null; }
}

