/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 



package org.apache.commons.logging.impl;

import org.apache.commons.logging.Log;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * <p>Implementation of the <code>org.apache.commons.logging.Log</code>
 * interface that wraps the standard JDK logging mechanisms that were
 * introduced in the Merlin release (JDK 1.4).</p>
 *
 * @author <a href="mailto:sanders@apache.org">Scott Sanders</a>
 * @author <a href="mailto:bloritsch@apache.org">Berin Loritsch</a>
 * @author <a href="mailto:donaldp@apache.org">Peter Donald</a>
 * @version $Revision: 370652 $ $Date: 2006-01-19 22:23:48 +0000 (Thu, 19 Jan 2006) $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class Jdk14Logger implements org.apache.commons.logging.Log, java.io.Serializable {

/**
 * Construct a named instance of this Logger.
 *
 * @param name Name of the logger to be constructed
 */

@Deprecated
public Jdk14Logger(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.FINE</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#debug(Object)
 */

@Deprecated
public void debug(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.FINE</code>.
 *
 * @param message to log
 * @param exception log this cause
 * @see org.apache.commons.logging.Log#debug(Object, Throwable)
 */

@Deprecated
public void debug(java.lang.Object message, java.lang.Throwable exception) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.SEVERE</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#error(Object)
 */

@Deprecated
public void error(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.SEVERE</code>.
 *
 * @param message to log
 * @param exception log this cause
 * @see org.apache.commons.logging.Log#error(Object, Throwable)
 */

@Deprecated
public void error(java.lang.Object message, java.lang.Throwable exception) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.SEVERE</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#fatal(Object)
 */

@Deprecated
public void fatal(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.SEVERE</code>.
 *
 * @param message to log
 * @param exception log this cause
 * @see org.apache.commons.logging.Log#fatal(Object, Throwable)
 */

@Deprecated
public void fatal(java.lang.Object message, java.lang.Throwable exception) { throw new RuntimeException("Stub!"); }

/**
 * Return the native Logger instance we are using.
 */

@Deprecated
public java.util.logging.Logger getLogger() { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.INFO</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#info(Object)
 */

@Deprecated
public void info(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.INFO</code>.
 *
 * @param message to log
 * @param exception log this cause
 * @see org.apache.commons.logging.Log#info(Object, Throwable)
 */

@Deprecated
public void info(java.lang.Object message, java.lang.Throwable exception) { throw new RuntimeException("Stub!"); }

/**
 * Is debug logging currently enabled?
 */

@Deprecated
public boolean isDebugEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Is error logging currently enabled?
 */

@Deprecated
public boolean isErrorEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Is fatal logging currently enabled?
 */

@Deprecated
public boolean isFatalEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Is info logging currently enabled?
 */

@Deprecated
public boolean isInfoEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Is trace logging currently enabled?
 */

@Deprecated
public boolean isTraceEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Is warn logging currently enabled?
 */

@Deprecated
public boolean isWarnEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.FINEST</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#trace(Object)
 */

@Deprecated
public void trace(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.FINEST</code>.
 *
 * @param message to log
 * @param exception log this cause
 * @see org.apache.commons.logging.Log#trace(Object, Throwable)
 */

@Deprecated
public void trace(java.lang.Object message, java.lang.Throwable exception) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.WARNING</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#warn(Object)
 */

@Deprecated
public void warn(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with <code>java.util.logging.Level.WARNING</code>.
 *
 * @param message to log
 * @param exception log this cause
 * @see org.apache.commons.logging.Log#warn(Object, Throwable)
 */

@Deprecated
public void warn(java.lang.Object message, java.lang.Throwable exception) { throw new RuntimeException("Stub!"); }

/**
 * This member variable simply ensures that any attempt to initialise
 * this class in a pre-1.4 JVM will result in an ExceptionInInitializerError.
 * It must not be private, as an optimising compiler could detect that it
 * is not used and optimise it away.
 */

@Deprecated protected static final java.util.logging.Level dummyLevel;
static { dummyLevel = null; }

/**
 * The underlying Logger implementation we are using.
 */

@Deprecated protected transient java.util.logging.Logger logger;

/**
 * The name of the logger we are wrapping.
 */

@Deprecated protected java.lang.String name;
}

