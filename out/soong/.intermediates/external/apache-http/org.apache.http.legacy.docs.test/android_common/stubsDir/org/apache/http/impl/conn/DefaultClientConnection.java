/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/DefaultClientConnection.java $
 * $Revision: 673450 $
 * $Date: 2008-07-02 10:35:05 -0700 (Wed, 02 Jul 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn;

import java.io.IOException;

/**
 * Default implementation of an operated client connection.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version   $Revision: 673450 $ $Date: 2008-07-02 10:35:05 -0700 (Wed, 02 Jul 2008) $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class DefaultClientConnection extends org.apache.http.impl.SocketHttpClientConnection implements org.apache.http.conn.OperatedClientConnection {

@Deprecated
public DefaultClientConnection() { throw new RuntimeException("Stub!"); }

@Deprecated
public final org.apache.http.HttpHost getTargetHost() { throw new RuntimeException("Stub!"); }

@Deprecated
public final boolean isSecure() { throw new RuntimeException("Stub!"); }

@Deprecated
public final java.net.Socket getSocket() { throw new RuntimeException("Stub!"); }

@Deprecated
public void opening(java.net.Socket sock, org.apache.http.HttpHost target) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void openCompleted(boolean secure, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Force-closes this connection.
 * If the connection is still in the process of being open (the method
 * {@link #opening opening} was already called but
 * {@link #openCompleted openCompleted} was not), the associated
 * socket that is being connected to a remote address will be closed.
 * That will interrupt a thread that is blocked on connecting
 * the socket.
 * If the connection is not yet open, this will prevent the connection
 * from being opened.
 *
 * @throws IOException      in case of a problem
 */

@Deprecated
public void shutdown() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
protected org.apache.http.io.SessionInputBuffer createSessionInputBuffer(java.net.Socket socket, int buffersize, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
protected org.apache.http.io.SessionOutputBuffer createSessionOutputBuffer(java.net.Socket socket, int buffersize, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
protected org.apache.http.io.HttpMessageParser createResponseParser(org.apache.http.io.SessionInputBuffer buffer, org.apache.http.HttpResponseFactory responseFactory, org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

@Deprecated
public void update(java.net.Socket sock, org.apache.http.HttpHost target, boolean secure, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpResponse receiveResponseHeader() throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void sendRequestHeader(org.apache.http.HttpRequest request) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }
}

