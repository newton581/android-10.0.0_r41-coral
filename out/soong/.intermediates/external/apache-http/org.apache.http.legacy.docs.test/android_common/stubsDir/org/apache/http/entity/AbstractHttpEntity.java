/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/entity/AbstractHttpEntity.java $
 * $Revision: 496070 $
 * $Date: 2007-01-14 04:18:34 -0800 (Sun, 14 Jan 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.entity;

import org.apache.http.HttpEntity;
import org.apache.http.Header;
import java.io.IOException;

/**
 * Abstract base class for entities.
 * Provides the commonly used attributes for streamed and self-contained
 * implementations of {@link HttpEntity HttpEntity}.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 496070 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract class AbstractHttpEntity implements org.apache.http.HttpEntity {

/**
 * Protected default constructor.
 * The attributes of the created object remain
 * <code>null</code> and <code>false</code>, respectively.
 */

@Deprecated
protected AbstractHttpEntity() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the Content-Type header.
 * The default implementation returns the value of the
 * {@link #contentType contentType} attribute.
 *
 * @return  the Content-Type header, or <code>null</code>
 */

@Deprecated
public org.apache.http.Header getContentType() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the Content-Encoding header.
 * The default implementation returns the value of the
 * {@link #contentEncoding contentEncoding} attribute.
 *
 * @return  the Content-Encoding header, or <code>null</code>
 */

@Deprecated
public org.apache.http.Header getContentEncoding() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the 'chunked' flag.
 * The default implementation returns the value of the
 * {@link #chunked chunked} attribute.
 *
 * @return  the 'chunked' flag
 */

@Deprecated
public boolean isChunked() { throw new RuntimeException("Stub!"); }

/**
 * Specifies the Content-Type header.
 * The default implementation sets the value of the
 * {@link #contentType contentType} attribute.
 *
 * @param contentType       the new Content-Encoding header, or
 *                          <code>null</code> to unset
 */

@Deprecated
public void setContentType(org.apache.http.Header contentType) { throw new RuntimeException("Stub!"); }

/**
 * Specifies the Content-Type header, as a string.
 * The default implementation calls
 * {@link #setContentType(Header) setContentType(Header)}.
 *
 * @param ctString     the new Content-Type header, or
 *                     <code>null</code> to unset
 */

@Deprecated
public void setContentType(java.lang.String ctString) { throw new RuntimeException("Stub!"); }

/**
 * Specifies the Content-Encoding header.
 * The default implementation sets the value of the
 * {@link #contentEncoding contentEncoding} attribute.
 *
 * @param contentEncoding   the new Content-Encoding header, or
 *                          <code>null</code> to unset
 */

@Deprecated
public void setContentEncoding(org.apache.http.Header contentEncoding) { throw new RuntimeException("Stub!"); }

/**
 * Specifies the Content-Encoding header, as a string.
 * The default implementation calls
 * {@link #setContentEncoding(Header) setContentEncoding(Header)}.
 *
 * @param ceString     the new Content-Encoding header, or
 *                     <code>null</code> to unset
 */

@Deprecated
public void setContentEncoding(java.lang.String ceString) { throw new RuntimeException("Stub!"); }

/**
 * Specifies the 'chunked' flag.
 * The default implementation sets the value of the
 * {@link #chunked chunked} attribute.
 *
 * @param b         the new 'chunked' flag
 */

@Deprecated
public void setChunked(boolean b) { throw new RuntimeException("Stub!"); }

/**
 * Does not consume anything.
 * The default implementation does nothing if
 * {@link HttpEntity#isStreaming isStreaming}
 * returns <code>false</code>, and throws an exception
 * if it returns <code>true</code>.
 * This removes the burden of implementing
 * an empty method for non-streaming entities.
 *
 * @throws IOException      in case of an I/O problem
 * @throws UnsupportedOperationException
 *          if a streaming subclass does not override this method
 */

@Deprecated
public void consumeContent() throws java.io.IOException, java.lang.UnsupportedOperationException { throw new RuntimeException("Stub!"); }

/**
 * The 'chunked' flag.
 * Returned by {@link #isChunked isChunked},
 * unless that method is overridden.
 */

@Deprecated protected boolean chunked;

/**
 * The Content-Encoding header.
 * Returned by {@link #getContentEncoding getContentEncoding},
 * unless that method is overridden.
 */

@Deprecated protected org.apache.http.Header contentEncoding;

/**
 * The Content-Type header.
 * Returned by {@link #getContentType getContentType},
 * unless that method is overridden.
 */

@Deprecated protected org.apache.http.Header contentType;
}

