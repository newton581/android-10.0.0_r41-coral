/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec;

import java.util.Comparator;

/**
 * Strings are comparable, and this comparator allows
 * you to configure it with an instance of a class
 * which implements StringEncoder.  This comparator
 * is used to sort Strings by an encoding scheme such
 * as Soundex, Metaphone, etc.  This class can come in
 * handy if one need to sort Strings by an encoded
 * form of a name such as Soundex.
 *
 * @author Apache Software Foundation
 * @version $Id: StringEncoderComparator.java,v 1.14 2004/06/21 23:24:17 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class StringEncoderComparator implements java.util.Comparator {

/**
 * Constructs a new instance.
 */

@Deprecated
public StringEncoderComparator() { throw new RuntimeException("Stub!"); }

/**
 * Constructs a new instance with the given algorithm.
 * @param stringEncoder the StringEncoder used for comparisons.
 */

@Deprecated
public StringEncoderComparator(org.apache.commons.codec.StringEncoder stringEncoder) { throw new RuntimeException("Stub!"); }

/**
 * Compares two strings based not on the strings
 * themselves, but on an encoding of the two
 * strings using the StringEncoder this Comparator
 * was created with.
 *
 * If an {@link EncoderException} is encountered, return <code>0</code>.
 *
 * @param o1 the object to compare
 * @param o2 the object to compare to
 * @return the Comparable.compareTo() return code or 0 if an encoding error was caught.
 * @see Comparable
 */

@Deprecated
public int compare(java.lang.Object o1, java.lang.Object o2) { throw new RuntimeException("Stub!"); }
}

