/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.net;

import org.apache.commons.codec.DecoderException;
import java.io.UnsupportedEncodingException;
import org.apache.commons.codec.EncoderException;
import java.util.BitSet;

/**
 * <p>Implements the 'www-form-urlencoded' encoding scheme,
 * also misleadingly known as URL encoding.</p>
 *
 * <p>For more detailed information please refer to
 * <a href="http://www.w3.org/TR/html4/interact/forms.html#h-17.13.4.1">
 * Chapter 17.13.4 'Form content types'</a> of the
 * <a href="http://www.w3.org/TR/html4/">HTML 4.01 Specification<a></p>
 *
 * <p>
 * This codec is meant to be a replacement for standard Java classes
 * {@link java.net.URLEncoder} and {@link java.net.URLDecoder}
 * on older Java platforms, as these classes in Java versions below
 * 1.4 rely on the platform's default charset encoding.
 * </p>
 *
 * @author Apache Software Foundation
 * @since 1.2
 * @version $Id: URLCodec.java,v 1.19 2004/03/29 07:59:00 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class URLCodec implements org.apache.commons.codec.BinaryEncoder, org.apache.commons.codec.BinaryDecoder, org.apache.commons.codec.StringEncoder, org.apache.commons.codec.StringDecoder {

/**
 * Default constructor.
 */

@Deprecated
public URLCodec() { throw new RuntimeException("Stub!"); }

/**
 * Constructor which allows for the selection of a default charset
 *
 * @param charset the default string charset to use.
 */

@Deprecated
public URLCodec(java.lang.String charset) { throw new RuntimeException("Stub!"); }

/**
 * Encodes an array of bytes into an array of URL safe 7-bit
 * characters. Unsafe characters are escaped.
 *
 * @param urlsafe bitset of characters deemed URL safe
 * @param bytes array of bytes to convert to URL safe characters
 * @return array of bytes containing URL safe characters
 */

@Deprecated
public static final byte[] encodeUrl(java.util.BitSet urlsafe, byte[] bytes) { throw new RuntimeException("Stub!"); }

/**
 * Decodes an array of URL safe 7-bit characters into an array of
 * original bytes. Escaped characters are converted back to their
 * original representation.
 *
 * @param bytes array of URL safe characters
 * @return array of original bytes
 * @throws DecoderException Thrown if URL decoding is unsuccessful
 */

@Deprecated
public static final byte[] decodeUrl(byte[] bytes) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes an array of bytes into an array of URL safe 7-bit
 * characters. Unsafe characters are escaped.
 *
 * @param bytes array of bytes to convert to URL safe characters
 * @return array of bytes containing URL safe characters
 */

@Deprecated
public byte[] encode(byte[] bytes) { throw new RuntimeException("Stub!"); }

/**
 * Decodes an array of URL safe 7-bit characters into an array of
 * original bytes. Escaped characters are converted back to their
 * original representation.
 *
 * @param bytes array of URL safe characters
 * @return array of original bytes
 * @throws DecoderException Thrown if URL decoding is unsuccessful
 */

@Deprecated
public byte[] decode(byte[] bytes) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a string into its URL safe form using the specified
 * string charset. Unsafe characters are escaped.
 *
 * @param pString string to convert to a URL safe form
 * @param charset the charset for pString
 * @return URL safe string
 * @throws UnsupportedEncodingException Thrown if charset is not
 *                                      supported
 */

@Deprecated
public java.lang.String encode(java.lang.String pString, java.lang.String charset) throws java.io.UnsupportedEncodingException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a string into its URL safe form using the default string
 * charset. Unsafe characters are escaped.
 *
 * @param pString string to convert to a URL safe form
 * @return URL safe string
 * @throws EncoderException Thrown if URL encoding is unsuccessful
 *
 * @see #getDefaultCharset()
 */

@Deprecated
public java.lang.String encode(java.lang.String pString) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a URL safe string into its original form using the
 * specified encoding. Escaped characters are converted back
 * to their original representation.
 *
 * @param pString URL safe string to convert into its original form
 * @param charset the original string charset
 * @return original string
 * @throws DecoderException Thrown if URL decoding is unsuccessful
 * @throws UnsupportedEncodingException Thrown if charset is not
 *                                      supported
 */

@Deprecated
public java.lang.String decode(java.lang.String pString, java.lang.String charset) throws org.apache.commons.codec.DecoderException, java.io.UnsupportedEncodingException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a URL safe string into its original form using the default
 * string charset. Escaped characters are converted back to their
 * original representation.
 *
 * @param pString URL safe string to convert into its original form
 * @return original string
 * @throws DecoderException Thrown if URL decoding is unsuccessful
 *
 * @see #getDefaultCharset()
 */

@Deprecated
public java.lang.String decode(java.lang.String pString) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes an object into its URL safe form. Unsafe characters are
 * escaped.
 *
 * @param pObject string to convert to a URL safe form
 * @return URL safe object
 * @throws EncoderException Thrown if URL encoding is not
 *                          applicable to objects of this type or
 *                          if encoding is unsuccessful
 */

@Deprecated
public java.lang.Object encode(java.lang.Object pObject) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a URL safe object into its original form. Escaped
 * characters are converted back to their original representation.
 *
 * @param pObject URL safe object to convert into its original form
 * @return original object
 * @throws DecoderException Thrown if URL decoding is not
 *                          applicable to objects of this type
 *                          if decoding is unsuccessful
 */

@Deprecated
public java.lang.Object decode(java.lang.Object pObject) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * The <code>String</code> encoding used for decoding and encoding.
 *
 * @return Returns the encoding.
 *
 * @deprecated use #getDefaultCharset()
 */

@Deprecated
public java.lang.String getEncoding() { throw new RuntimeException("Stub!"); }

/**
 * The default charset used for string decoding and encoding.
 *
 * @return the default string charset.
 */

@Deprecated
public java.lang.String getDefaultCharset() { throw new RuntimeException("Stub!"); }

@Deprecated protected static byte ESCAPE_CHAR = 37; // 0x0025 '%'

/**
 * BitSet of www-form-url safe characters.
 */

@Deprecated protected static final java.util.BitSet WWW_FORM_URL;
static { WWW_FORM_URL = null; }

/**
 * The default charset used for string decoding and encoding.
 */

@Deprecated protected java.lang.String charset = "UTF-8";
}

