/*
 * $Header: /home/jerenkrantz/tmp/commons/commons-convert/cvs/home/cvs/jakarta-commons//httpclient/src/java/org/apache/commons/httpclient/methods/multipart/Part.java,v 1.16 2005/01/14 21:16:40 olegk Exp $
 * $Revision: 480424 $
 * $Date: 2006-11-29 06:56:49 +0100 (Wed, 29 Nov 2006) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package com.android.internal.http.multipart;

import java.io.OutputStream;
import java.io.IOException;
import org.apache.commons.logging.Log;

/**
 * Abstract class for one Part of a multipart post object.
 *
 * @author <a href="mailto:mattalbright@yahoo.com">Matthew Albright</a>
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 * @author <a href="mailto:adrian@ephox.com">Adrian Sutton</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:oleg@ural.ru">Oleg Kalnichevski</a>
 *
 * @since 2.0
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class Part {

public Part() { throw new RuntimeException("Stub!"); }

/**
 * Return the boundary string.
 * @return the boundary string
 * @deprecated uses a constant string. Rather use {@link #getPartBoundary}
 */

@Deprecated
public static java.lang.String getBoundary() { throw new RuntimeException("Stub!"); }

/**
 * Return the name of this part.
 * @return The name.
 */

public abstract java.lang.String getName();

/**
 * Returns the content type of this part.
 * @return the content type, or <code>null</code> to exclude the content type header
 */

public abstract java.lang.String getContentType();

/**
 * Return the character encoding of this part.
 * @return the character encoding, or <code>null</code> to exclude the character
 * encoding header
 */

public abstract java.lang.String getCharSet();

/**
 * Return the transfer encoding of this part.
 * @return the transfer encoding, or <code>null</code> to exclude the transfer encoding header
 */

public abstract java.lang.String getTransferEncoding();

/**
 * Gets the part boundary to be used.
 * @return the part boundary as an array of bytes.
 *
 * @since 3.0
 */

protected byte[] getPartBoundary() { throw new RuntimeException("Stub!"); }

/**
 * Tests if this part can be sent more than once.
 * @return <code>true</code> if {@link #sendData(OutputStream)} can be successfully called
 * more than once.
 * @since 3.0
 */

public boolean isRepeatable() { throw new RuntimeException("Stub!"); }

/**
 * Write the start to the specified output stream
 * @param out The output stream
 * @throws IOException If an IO problem occurs.
 */

protected void sendStart(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Write the content disposition header to the specified output stream
 *
 * @param out The output stream
 * @throws IOException If an IO problem occurs.
 */

protected void sendDispositionHeader(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Write the content type header to the specified output stream
 * @param out The output stream
 * @throws IOException If an IO problem occurs.
 */

protected void sendContentTypeHeader(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Write the content transfer encoding header to the specified
 * output stream
 *
 * @param out The output stream
 * @throws IOException If an IO problem occurs.
 */

protected void sendTransferEncodingHeader(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Write the end of the header to the output stream
 * @param out The output stream
 * @throws IOException If an IO problem occurs.
 */

protected void sendEndOfHeader(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Write the data to the specified output stream
 * @param out The output stream
 * @throws IOException If an IO problem occurs.
 */

protected abstract void sendData(java.io.OutputStream out) throws java.io.IOException;

/**
 * Return the length of the main content
 *
 * @return long The length.
 * @throws IOException If an IO problem occurs
 */

protected abstract long lengthOfData() throws java.io.IOException;

/**
 * Write the end data to the output stream.
 * @param out The output stream
 * @throws IOException If an IO problem occurs.
 */

protected void sendEnd(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Write all the data to the output stream.
 * If you override this method make sure to override
 * #length() as well
 *
 * @param out The output stream
 * @throws IOException If an IO problem occurs.
 */

public void send(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Return the full length of all the data.
 * If you override this method make sure to override
 * #send(OutputStream) as well
 *
 * @return long The length.
 * @throws IOException If an IO problem occurs
 */

public long length() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Return a string representation of this object.
 * @return A string representation of this object.
 * @see java.lang.Object#toString()
 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Write all parts and the last boundary to the specified output stream.
 *
 * @param out The stream to write to.
 * @param parts The parts to write.
 *
 * @throws IOException If an I/O error occurs while writing the parts.
 */

public static void sendParts(java.io.OutputStream out, com.android.internal.http.multipart.Part[] parts) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Write all parts and the last boundary to the specified output stream.
 *
 * @param out The stream to write to.
 * @param parts The parts to write.
 * @param partBoundary The ASCII bytes to use as the part boundary.
 *
 * @throws IOException If an I/O error occurs while writing the parts.
 *
 * @since 3.0
 */

public static void sendParts(java.io.OutputStream out, com.android.internal.http.multipart.Part[] parts, byte[] partBoundary) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Return the total sum of all parts and that of the last boundary
 *
 * @param parts The parts.
 * @return The total length
 *
 * @throws IOException If an I/O error occurs while writing the parts.
 */

public static long getLengthOfParts(com.android.internal.http.multipart.Part[] parts) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Gets the length of the multipart message including the given parts.
 *
 * @param parts The parts.
 * @param partBoundary The ASCII bytes to use as the part boundary.
 * @return The total length
 *
 * @throws IOException If an I/O error occurs while writing the parts.
 *
 * @since 3.0
 */

public static long getLengthOfParts(com.android.internal.http.multipart.Part[] parts, byte[] partBoundary) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/** 
 * The boundary
 * @deprecated use {@link org.apache.http.client.methods.multipart#MULTIPART_BOUNDARY}
 */

@Deprecated protected static final java.lang.String BOUNDARY = "----------------314159265358979323846";

/** 
 * The boundary as a byte array.
 * @deprecated
 */

@Deprecated protected static final byte[] BOUNDARY_BYTES;
static { BOUNDARY_BYTES = new byte[0]; }

/** Content charset */

protected static final java.lang.String CHARSET = "; charset=";

/** Content charset as a byte array */

protected static final byte[] CHARSET_BYTES;
static { CHARSET_BYTES = new byte[0]; }

/** Content dispostion characters */

protected static final java.lang.String CONTENT_DISPOSITION = "Content-Disposition: form-data; name=";

/** Content dispostion as a byte array */

protected static final byte[] CONTENT_DISPOSITION_BYTES;
static { CONTENT_DISPOSITION_BYTES = new byte[0]; }

/** Content type header */

protected static final java.lang.String CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding: ";

/** Content type header as a byte array */

protected static final byte[] CONTENT_TRANSFER_ENCODING_BYTES;
static { CONTENT_TRANSFER_ENCODING_BYTES = new byte[0]; }

/** Content type header */

protected static final java.lang.String CONTENT_TYPE = "Content-Type: ";

/** Content type header as a byte array */

protected static final byte[] CONTENT_TYPE_BYTES;
static { CONTENT_TYPE_BYTES = new byte[0]; }

/** Carriage return/linefeed */

protected static final java.lang.String CRLF = "\r\n";

/** Carriage return/linefeed as a byte array */

protected static final byte[] CRLF_BYTES;
static { CRLF_BYTES = new byte[0]; }

/** Extra characters */

protected static final java.lang.String EXTRA = "--";

/** Extra characters as a byte array */

protected static final byte[] EXTRA_BYTES;
static { EXTRA_BYTES = new byte[0]; }

/** Content dispostion characters */

protected static final java.lang.String QUOTE = "\"";

/** Content dispostion as a byte array */

protected static final byte[] QUOTE_BYTES;
static { QUOTE_BYTES = new byte[0]; }
}

