/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/conn/scheme/PlainSocketFactory.java $
 * $Revision: 659194 $
 * $Date: 2008-05-22 11:33:47 -0700 (Thu, 22 May 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.conn.scheme;


/**
 * The default class for creating sockets.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 * @author Michael Becke
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class PlainSocketFactory implements org.apache.http.conn.scheme.SocketFactory {

@Deprecated
public PlainSocketFactory(org.apache.http.conn.scheme.HostNameResolver nameResolver) { throw new RuntimeException("Stub!"); }

@Deprecated
public PlainSocketFactory() { throw new RuntimeException("Stub!"); }

/**
 * Gets the singleton instance of this class.
 * @return the one and only plain socket factory
 */

@Deprecated
public static org.apache.http.conn.scheme.PlainSocketFactory getSocketFactory() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.net.Socket createSocket() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.net.Socket connectSocket(java.net.Socket sock, java.lang.String host, int port, java.net.InetAddress localAddress, int localPort, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Checks whether a socket connection is secure.
 * This factory creates plain socket connections
 * which are not considered secure.
 *
 * @param sock      the connected socket
 *
 * @return  <code>false</code>
 *
 * @throws IllegalArgumentException if the argument is invalid
 */

@Deprecated
public boolean isSecure(java.net.Socket sock) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Compares this factory with an object.
 * There is only one instance of this class.
 *
 * @param obj       the object to compare with
 *
 * @return  iff the argument is this object
 */

@Deprecated
public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * Obtains a hash code for this object.
 * All instances of this class have the same hash code.
 * There is only one instance of this class.
 */

@Deprecated
public int hashCode() { throw new RuntimeException("Stub!"); }
}

