/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.http;

import java.security.cert.Certificate;

/**
 * Class responsible for all server certificate validation functionality
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class CertificateChainValidator {

/**
 * Creates a new certificate chain validator. This is a private constructor.
 * If you need a Certificate chain validator, call getInstance().
 */

CertificateChainValidator() { throw new RuntimeException("Stub!"); }

/**
 * @return The singleton instance of the certificates chain validator
 */

public static android.net.http.CertificateChainValidator getInstance() { throw new RuntimeException("Stub!"); }

/**
 * Performs the handshake and server certificates validation
 * Notice a new chain will be rebuilt by tracing the issuer and subject
 * before calling checkServerTrusted().
 * And if the last traced certificate is self issued and it is expired, it
 * will be dropped.
 * @param sslSocket The secure connection socket
 * @param domain The website domain
 * @return An SSL error object if there is an error and null otherwise
 */

public android.net.http.SslError doHandshakeAndValidateServerCertificates(android.net.http.HttpsConnection connection, javax.net.ssl.SSLSocket sslSocket, java.lang.String domain) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Similar to doHandshakeAndValidateServerCertificates but exposed to JNI for use
 * by Chromium HTTPS stack to validate the cert chain.
 * @param certChain The bytes for certificates in ASN.1 DER encoded certificates format.
 * @param domain The full website hostname and domain
 * @param authType The authentication type for the cert chain
 * @return An SSL error object if there is an error and null otherwise
 */

public static android.net.http.SslError verifyServerCertificates(byte[][] certChain, java.lang.String domain, java.lang.String authType) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Handles updates to credential storage.
 */

public static void handleTrustStorageUpdate() { throw new RuntimeException("Stub!"); }
}

