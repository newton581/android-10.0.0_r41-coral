/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.logging;


/**
 * <p>An exception that is thrown only if a suitable <code>LogFactory</code>
 * or <code>Log</code> instance cannot be created by the corresponding
 * factory methods.</p>
 *
 * @author Craig R. McClanahan
 * @version $Revision: 155426 $ $Date: 2005-02-26 13:10:49 +0000 (Sat, 26 Feb 2005) $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class LogConfigurationException extends java.lang.RuntimeException {

/**
 * Construct a new exception with <code>null</code> as its detail message.
 */

@Deprecated
public LogConfigurationException() { throw new RuntimeException("Stub!"); }

/**
 * Construct a new exception with the specified detail message.
 *
 * @param message The detail message
 */

@Deprecated
public LogConfigurationException(java.lang.String message) { throw new RuntimeException("Stub!"); }

/**
 * Construct a new exception with the specified cause and a derived
 * detail message.
 *
 * @param cause The underlying cause
 */

@Deprecated
public LogConfigurationException(java.lang.Throwable cause) { throw new RuntimeException("Stub!"); }

/**
 * Construct a new exception with the specified detail message and cause.
 *
 * @param message The detail message
 * @param cause The underlying cause
 */

@Deprecated
public LogConfigurationException(java.lang.String message, java.lang.Throwable cause) { throw new RuntimeException("Stub!"); }

/**
 * Return the underlying cause of this exception (if any).
 */

@Deprecated
public java.lang.Throwable getCause() { throw new RuntimeException("Stub!"); }

/**
 * The underlying cause of this exception.
 */

@Deprecated protected java.lang.Throwable cause;
}

