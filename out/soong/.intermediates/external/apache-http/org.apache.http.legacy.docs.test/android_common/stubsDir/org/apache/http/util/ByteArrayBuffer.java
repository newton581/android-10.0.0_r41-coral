/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/util/ByteArrayBuffer.java $
 * $Revision: 496070 $
 * $Date: 2007-01-14 04:18:34 -0800 (Sun, 14 Jan 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.util;


/**
 * A resizable byte array.
 *
 * @author <a href="mailto:oleg@ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 496070 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class ByteArrayBuffer {

@Deprecated
public ByteArrayBuffer(int capacity) { throw new RuntimeException("Stub!"); }

@Deprecated
public void append(byte[] b, int off, int len) { throw new RuntimeException("Stub!"); }

@Deprecated
public void append(int b) { throw new RuntimeException("Stub!"); }

@Deprecated
public void append(char[] b, int off, int len) { throw new RuntimeException("Stub!"); }

@Deprecated
public void append(org.apache.http.util.CharArrayBuffer b, int off, int len) { throw new RuntimeException("Stub!"); }

@Deprecated
public void clear() { throw new RuntimeException("Stub!"); }

@Deprecated
public byte[] toByteArray() { throw new RuntimeException("Stub!"); }

@Deprecated
public int byteAt(int i) { throw new RuntimeException("Stub!"); }

@Deprecated
public int capacity() { throw new RuntimeException("Stub!"); }

@Deprecated
public int length() { throw new RuntimeException("Stub!"); }

@Deprecated
public byte[] buffer() { throw new RuntimeException("Stub!"); }

@Deprecated
public void setLength(int len) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isEmpty() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isFull() { throw new RuntimeException("Stub!"); }
}

