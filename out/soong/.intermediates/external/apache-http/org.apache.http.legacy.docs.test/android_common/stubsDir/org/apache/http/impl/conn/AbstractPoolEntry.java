/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/AbstractPoolEntry.java $
 * $Revision: 658775 $
 * $Date: 2008-05-21 10:30:45 -0700 (Wed, 21 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn;

import org.apache.http.conn.routing.HttpRoute;
import java.io.IOException;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

/**
 * A pool entry for use by connection manager implementations.
 * Pool entries work in conjunction with an
 * {@link AbstractClientConnAdapter adapter}.
 * The adapter is handed out to applications that obtain a connection.
 * The pool entry stores the underlying connection and tracks the
 * {@link HttpRoute route} established.
 * The adapter delegates methods for establishing the route to
 * it's pool entry.
 * <br/>
 * If the managed connections is released or revoked, the adapter
 * gets disconnected, but the pool entry still contains the
 * underlying connection and the established route.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 * @author <a href="mailto:becke@u.washington.edu">Michael Becke</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version   $Revision: 658775 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract class AbstractPoolEntry {

/**
 * Creates a new pool entry.
 *
 * @param connOperator     the Connection Operator for this entry
 * @param route   the planned route for the connection,
 *                or <code>null</code>
 */

@Deprecated
protected AbstractPoolEntry(org.apache.http.conn.ClientConnectionOperator connOperator, org.apache.http.conn.routing.HttpRoute route) { throw new RuntimeException("Stub!"); }

/**
 * Returns the state object associated with this pool entry.
 *
 * @return The state object
 */

@Deprecated
public java.lang.Object getState() { throw new RuntimeException("Stub!"); }

/**
 * Assigns a state object to this pool entry.
 *
 * @param state The state object
 */

@Deprecated
public void setState(java.lang.Object state) { throw new RuntimeException("Stub!"); }

/**
 * Opens the underlying connection.
 *
 * @param route         the route along which to open the connection
 * @param context       the context for opening the connection
 * @param params        the parameters for opening the connection
 *
 * @throws IOException  in case of a problem
 */

@Deprecated
public void open(org.apache.http.conn.routing.HttpRoute route, org.apache.http.protocol.HttpContext context, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Tracks tunnelling of the connection to the target.
 * The tunnel has to be established outside by sending a CONNECT
 * request to the (last) proxy.
 *
 * @param secure    <code>true</code> if the tunnel should be
 *                  considered secure, <code>false</code> otherwise
 * @param params    the parameters for tunnelling the connection
 *
 * @throws IOException  in case of a problem
 */

@Deprecated
public void tunnelTarget(boolean secure, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Tracks tunnelling of the connection to a chained proxy.
 * The tunnel has to be established outside by sending a CONNECT
 * request to the previous proxy.
 *
 * @param next      the proxy to which the tunnel was established.
 *  See {@link org.apache.http.conn.ManagedClientConnection#tunnelProxy
 *                                  ManagedClientConnection.tunnelProxy}
 *                  for details.
 * @param secure    <code>true</code> if the tunnel should be
 *                  considered secure, <code>false</code> otherwise
 * @param params    the parameters for tunnelling the connection
 *
 * @throws IOException  in case of a problem
 */

@Deprecated
public void tunnelProxy(org.apache.http.HttpHost next, boolean secure, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Layers a protocol on top of an established tunnel.
 *
 * @param context   the context for layering
 * @param params    the parameters for layering
 *
 * @throws IOException  in case of a problem
 */

@Deprecated
public void layerProtocol(org.apache.http.protocol.HttpContext context, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Shuts down the entry.
 *
 * If {@link #open(HttpRoute, HttpContext, HttpParams)} is in progress,
 * this will cause that open to possibly throw an {@link IOException}.
 */

@Deprecated
protected void shutdownEntry() { throw new RuntimeException("Stub!"); }

/** The connection operator. */

@Deprecated protected final org.apache.http.conn.ClientConnectionOperator connOperator;
{ connOperator = null; }

/** The underlying connection being pooled or used. */

@Deprecated protected final org.apache.http.conn.OperatedClientConnection connection;
{ connection = null; }

/** The route for which this entry gets allocated. */

@Deprecated protected volatile org.apache.http.conn.routing.HttpRoute route;

/** Connection state object */

@Deprecated protected volatile java.lang.Object state;

/** The tracked route, or <code>null</code> before tracking starts. */

@Deprecated protected volatile org.apache.http.conn.routing.RouteTracker tracker;
}

