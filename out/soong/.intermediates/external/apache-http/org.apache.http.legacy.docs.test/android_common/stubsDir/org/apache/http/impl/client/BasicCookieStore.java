/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/client/BasicCookieStore.java $
 * $Revision: 653041 $
 * $Date: 2008-05-03 03:39:28 -0700 (Sat, 03 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.client;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import java.util.Date;

/**
 * Default implementation of {@link CookieStore}
 *
 * @author <a href="mailto:remm@apache.org">Remy Maucherat</a>
 * @author Rodney Waldhoff
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 * @author Sean C. Sullivan
 * @author <a href="mailto:becke@u.washington.edu">Michael Becke</a>
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:adrian@intencha.com">Adrian Sutton</a>
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicCookieStore implements org.apache.http.client.CookieStore {

/**
 * Default constructor.
 */

@Deprecated
public BasicCookieStore() { throw new RuntimeException("Stub!"); }

/**
 * Adds an {@link Cookie HTTP cookie}, replacing any existing equivalent cookies.
 * If the given cookie has already expired it will not be added, but existing
 * values will still be removed.
 *
 * @param cookie the {@link Cookie cookie} to be added
 *
 * @see #addCookies(Cookie[])
 *
 */

@Deprecated
public synchronized void addCookie(org.apache.http.cookie.Cookie cookie) { throw new RuntimeException("Stub!"); }

/**
 * Adds an array of {@link Cookie HTTP cookies}. Cookies are added individually and
 * in the given array order. If any of the given cookies has already expired it will
 * not be added, but existing values will still be removed.
 *
 * @param cookies the {@link Cookie cookies} to be added
 *
 * @see #addCookie(Cookie)
 *
 */

@Deprecated
public synchronized void addCookies(org.apache.http.cookie.Cookie[] cookies) { throw new RuntimeException("Stub!"); }

/**
 * Returns an immutable array of {@link Cookie cookies} that this HTTP
 * state currently contains.
 *
 * @return an array of {@link Cookie cookies}.
 */

@Deprecated
public synchronized java.util.List<org.apache.http.cookie.Cookie> getCookies() { throw new RuntimeException("Stub!"); }

/**
 * Removes all of {@link Cookie cookies} in this HTTP state
 * that have expired by the specified {@link java.util.Date date}.
 *
 * @return true if any cookies were purged.
 *
 * @see Cookie#isExpired(Date)
 */

@Deprecated
public synchronized boolean clearExpired(java.util.Date date) { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Clears all cookies.
 */

@Deprecated
public synchronized void clear() { throw new RuntimeException("Stub!"); }
}

