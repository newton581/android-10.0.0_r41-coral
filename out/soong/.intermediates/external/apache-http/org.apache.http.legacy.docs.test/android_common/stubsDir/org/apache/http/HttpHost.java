/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/HttpHost.java $
 * $Revision: 653058 $
 * $Date: 2008-05-03 05:01:10 -0700 (Sat, 03 May 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http;


/**
 * Holds all of the variables needed to describe an HTTP connection to a host.
 * This includes remote host name, port and scheme.
 *
 * @author <a href="mailto:becke@u.washington.edu">Michael Becke</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 * @author Laura Werner
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class HttpHost implements java.lang.Cloneable {

/**
 * Creates a new {@link HttpHost HttpHost}, specifying all values.
 * Constructor for HttpHost.
 *
 * @param hostname  the hostname (IP or DNS name)
 * @param port      the port number.
 *                  <code>-1</code> indicates the scheme default port.
 * @param scheme    the name of the scheme.
 *                  <code>null</code> indicates the
 *                  {@link #DEFAULT_SCHEME_NAME default scheme}
 */

@Deprecated
public HttpHost(java.lang.String hostname, int port, java.lang.String scheme) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new {@link HttpHost HttpHost}, with default scheme.
 *
 * @param hostname  the hostname (IP or DNS name)
 * @param port      the port number.
 *                  <code>-1</code> indicates the scheme default port.
 */

@Deprecated
public HttpHost(java.lang.String hostname, int port) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new {@link HttpHost HttpHost}, with default scheme and port.
 *
 * @param hostname  the hostname (IP or DNS name)
 */

@Deprecated
public HttpHost(java.lang.String hostname) { throw new RuntimeException("Stub!"); }

/**
 * Copy constructor for {@link HttpHost HttpHost}.
 *
 * @param httphost the HTTP host to copy details from
 */

@Deprecated
public HttpHost(org.apache.http.HttpHost httphost) { throw new RuntimeException("Stub!"); }

/**
 * Returns the host name.
 *
 * @return the host name (IP or DNS name)
 */

@Deprecated
public java.lang.String getHostName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the port.
 *
 * @return the host port, or <code>-1</code> if not set
 */

@Deprecated
public int getPort() { throw new RuntimeException("Stub!"); }

/**
 * Returns the scheme name.
 *
 * @return the scheme name
 */

@Deprecated
public java.lang.String getSchemeName() { throw new RuntimeException("Stub!"); }

/**
 * Return the host URI, as a string.
 *
 * @return the host URI
 */

@Deprecated
public java.lang.String toURI() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the host string, without scheme prefix.
 *
 * @return  the host string, for example <code>localhost:8080</code>
 */

@Deprecated
public java.lang.String toHostString() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * @see java.lang.Object#hashCode()
 */

@Deprecated
public int hashCode() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object clone() throws java.lang.CloneNotSupportedException { throw new RuntimeException("Stub!"); }

/** The default scheme is "http". */

@Deprecated public static final java.lang.String DEFAULT_SCHEME_NAME = "http";

/** The host to use. */

@Deprecated protected final java.lang.String hostname;
{ hostname = null; }

/** The lowercase host, for {@link #equals} and {@link #hashCode}. */

@Deprecated protected final java.lang.String lcHostname;
{ lcHostname = null; }

/** The port to use. */

@Deprecated protected final int port;
{ port = 0; }

/** The scheme */

@Deprecated protected final java.lang.String schemeName;
{ schemeName = null; }
}

