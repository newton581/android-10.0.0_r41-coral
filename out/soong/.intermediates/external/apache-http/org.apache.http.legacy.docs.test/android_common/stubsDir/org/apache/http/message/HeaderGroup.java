/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/message/HeaderGroup.java $
 * $Revision: 659185 $
 * $Date: 2008-05-22 11:07:36 -0700 (Thu, 22 May 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.message;

import org.apache.http.Header;

/**
 * A class for combining a set of headers.
 * This class allows for multiple headers with the same name and
 * keeps track of the order in which headers were added.
 *
 * @author Michael Becke
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class HeaderGroup implements java.lang.Cloneable {

/**
 * Constructor for HeaderGroup.
 */

@Deprecated
public HeaderGroup() { throw new RuntimeException("Stub!"); }

/**
 * Removes any contained headers.
 */

@Deprecated
public void clear() { throw new RuntimeException("Stub!"); }

/**
 * Adds the given header to the group.  The order in which this header was
 * added is preserved.
 *
 * @param header the header to add
 */

@Deprecated
public void addHeader(org.apache.http.Header header) { throw new RuntimeException("Stub!"); }

/**
 * Removes the given header.
 *
 * @param header the header to remove
 */

@Deprecated
public void removeHeader(org.apache.http.Header header) { throw new RuntimeException("Stub!"); }

/**
 * Replaces the first occurence of the header with the same name. If no header with
 * the same name is found the given header is added to the end of the list.
 *
 * @param header the new header that should replace the first header with the same
 * name if present in the list.
 */

@Deprecated
public void updateHeader(org.apache.http.Header header) { throw new RuntimeException("Stub!"); }

/**
 * Sets all of the headers contained within this group overriding any
 * existing headers. The headers are added in the order in which they appear
 * in the array.
 *
 * @param headers the headers to set
 */

@Deprecated
public void setHeaders(org.apache.http.Header[] headers) { throw new RuntimeException("Stub!"); }

/**
 * Gets a header representing all of the header values with the given name.
 * If more that one header with the given name exists the values will be
 * combined with a "," as per RFC 2616.
 *
 * <p>Header name comparison is case insensitive.
 *
 * @param name the name of the header(s) to get
 * @return a header with a condensed value or <code>null</code> if no
 * headers by the given name are present
 */

@Deprecated
public org.apache.http.Header getCondensedHeader(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Gets all of the headers with the given name.  The returned array
 * maintains the relative order in which the headers were added.
 *
 * <p>Header name comparison is case insensitive.
 *
 * @param name the name of the header(s) to get
 *
 * @return an array of length >= 0
 */

@Deprecated
public org.apache.http.Header[] getHeaders(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Gets the first header with the given name.
 *
 * <p>Header name comparison is case insensitive.
 *
 * @param name the name of the header to get
 * @return the first header or <code>null</code>
 */

@Deprecated
public org.apache.http.Header getFirstHeader(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Gets the last header with the given name.
 *
 * <p>Header name comparison is case insensitive.
 *
 * @param name the name of the header to get
 * @return the last header or <code>null</code>
 */

@Deprecated
public org.apache.http.Header getLastHeader(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Gets all of the headers contained within this group.
 *
 * @return an array of length >= 0
 */

@Deprecated
public org.apache.http.Header[] getAllHeaders() { throw new RuntimeException("Stub!"); }

/**
 * Tests if headers with the given name are contained within this group.
 *
 * <p>Header name comparison is case insensitive.
 *
 * @param name the header name to test for
 * @return <code>true</code> if at least one header with the name is
 * contained, <code>false</code> otherwise
 */

@Deprecated
public boolean containsHeader(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Returns an iterator over this group of headers.
 *
 * @return iterator over this group of headers.
 *
 * @since 4.0
 */

@Deprecated
public org.apache.http.HeaderIterator iterator() { throw new RuntimeException("Stub!"); }

/**
 * Returns an iterator over the headers with a given name in this group.
 *
 * @param name      the name of the headers over which to iterate, or
 *                  <code>null</code> for all headers
 *
 * @return iterator over some headers in this group.
 *
 * @since 4.0
 */

@Deprecated
public org.apache.http.HeaderIterator iterator(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Returns a copy of this object
 *
 * @return copy of this object
 */

@Deprecated
public org.apache.http.message.HeaderGroup copy() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object clone() throws java.lang.CloneNotSupportedException { throw new RuntimeException("Stub!"); }
}

