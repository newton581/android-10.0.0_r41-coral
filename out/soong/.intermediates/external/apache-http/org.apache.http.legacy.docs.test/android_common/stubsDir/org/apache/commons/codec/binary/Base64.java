/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.binary;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;

/**
 * Provides Base64 encoding and decoding as defined by RFC 2045.
 *
 * <p>This class implements section <cite>6.8. Base64 Content-Transfer-Encoding</cite>
 * from RFC 2045 <cite>Multipurpose Internet Mail Extensions (MIME) Part One:
 * Format of Internet Message Bodies</cite> by Freed and Borenstein.</p>
 *
 * @see <a href="http://www.ietf.org/rfc/rfc2045.txt">RFC 2045</a>
 * @author Apache Software Foundation
 * @since 1.0-dev
 * @version $Id: Base64.java,v 1.20 2004/05/24 00:21:24 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class Base64 implements org.apache.commons.codec.BinaryEncoder, org.apache.commons.codec.BinaryDecoder {

@Deprecated
public Base64() { throw new RuntimeException("Stub!"); }

/**
 * Tests a given byte array to see if it contains
 * only valid characters within the Base64 alphabet.
 *
 * @param arrayOctect byte array to test
 * @return true if all bytes are valid characters in the Base64
 *         alphabet or if the byte array is empty; false, otherwise
 */

@Deprecated
public static boolean isArrayByteBase64(byte[] arrayOctect) { throw new RuntimeException("Stub!"); }

/**
 * Encodes binary data using the base64 algorithm but
 * does not chunk the output.
 *
 * @param binaryData binary data to encode
 * @return Base64 characters
 */

@Deprecated
public static byte[] encodeBase64(byte[] binaryData) { throw new RuntimeException("Stub!"); }

/**
 * Encodes binary data using the base64 algorithm and chunks
 * the encoded output into 76 character blocks
 *
 * @param binaryData binary data to encode
 * @return Base64 characters chunked in 76 character blocks
 */

@Deprecated
public static byte[] encodeBase64Chunked(byte[] binaryData) { throw new RuntimeException("Stub!"); }

/**
 * Decodes an Object using the base64 algorithm.  This method
 * is provided in order to satisfy the requirements of the
 * Decoder interface, and will throw a DecoderException if the
 * supplied object is not of type byte[].
 *
 * @param pObject Object to decode
 * @return An object (of type byte[]) containing the
 *         binary data which corresponds to the byte[] supplied.
 * @throws DecoderException if the parameter supplied is not
 *                          of type byte[]
 */

@Deprecated
public java.lang.Object decode(java.lang.Object pObject) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a byte[] containing containing
 * characters in the Base64 alphabet.
 *
 * @param pArray A byte array containing Base64 character data
 * @return a byte array containing binary data
 */

@Deprecated
public byte[] decode(byte[] pArray) { throw new RuntimeException("Stub!"); }

/**
 * Encodes binary data using the base64 algorithm, optionally
 * chunking the output into 76 character blocks.
 *
 * @param binaryData Array containing binary data to encode.
 * @param isChunked if isChunked is true this encoder will chunk
 *                  the base64 output into 76 character blocks
 * @return Base64-encoded data.
 */

@Deprecated
public static byte[] encodeBase64(byte[] binaryData, boolean isChunked) { throw new RuntimeException("Stub!"); }

/**
 * Decodes Base64 data into octects
 *
 * @param base64Data Byte array containing Base64 data
 * @return Array containing decoded data.
 */

@Deprecated
public static byte[] decodeBase64(byte[] base64Data) { throw new RuntimeException("Stub!"); }

/**
 * Encodes an Object using the base64 algorithm.  This method
 * is provided in order to satisfy the requirements of the
 * Encoder interface, and will throw an EncoderException if the
 * supplied object is not of type byte[].
 *
 * @param pObject Object to encode
 * @return An object (of type byte[]) containing the
 *         base64 encoded data which corresponds to the byte[] supplied.
 * @throws EncoderException if the parameter supplied is not
 *                          of type byte[]
 */

@Deprecated
public java.lang.Object encode(java.lang.Object pObject) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a byte[] containing binary data, into a byte[] containing
 * characters in the Base64 alphabet.
 *
 * @param pArray a byte array containing binary data
 * @return A byte array containing only Base64 character data
 */

@Deprecated
public byte[] encode(byte[] pArray) { throw new RuntimeException("Stub!"); }
}

