/*
 * $Header: /home/jerenkrantz/tmp/commons/commons-convert/cvs/home/cvs/jakarta-commons//httpclient/src/java/org/apache/commons/httpclient/methods/multipart/StringPart.java,v 1.11 2004/04/18 23:51:37 jsdever Exp $
 * $Revision: 480424 $
 * $Date: 2006-11-29 06:56:49 +0100 (Wed, 29 Nov 2006) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package com.android.internal.http.multipart;

import java.io.OutputStream;
import java.io.IOException;
import org.apache.commons.logging.Log;

/**
 * Simple string parameter for a multipart post
 *
 * @author <a href="mailto:mattalbright@yahoo.com">Matthew Albright</a>
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:oleg@ural.ru">Oleg Kalnichevski</a>
 *
 * @since 2.0
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class StringPart extends com.android.internal.http.multipart.PartBase {

/**
 * Constructor.
 *
 * @param name The name of the part
 * @param value the string to post
 * @param charset the charset to be used to encode the string, if <code>null</code>
 * the {@link #DEFAULT_CHARSET default} is used
 */

public StringPart(java.lang.String name, java.lang.String value, java.lang.String charset) { super(null, null, null, null); throw new RuntimeException("Stub!"); }

/**
 * Constructor.
 *
 * @param name The name of the part
 * @param value the string to post
 */

public StringPart(java.lang.String name, java.lang.String value) { super(null, null, null, null); throw new RuntimeException("Stub!"); }

/**
 * Writes the data to the given OutputStream.
 * @param out the OutputStream to write to
 * @throws IOException if there is a write error
 */

protected void sendData(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Return the length of the data.
 * @return The length of the data.
 * @see Part#lengthOfData()
 */

protected long lengthOfData() { throw new RuntimeException("Stub!"); }

public void setCharSet(java.lang.String charSet) { throw new RuntimeException("Stub!"); }

/** Default charset of string parameters*/

public static final java.lang.String DEFAULT_CHARSET = "US-ASCII";

/** Default content encoding of string parameters. */

public static final java.lang.String DEFAULT_CONTENT_TYPE = "text/plain";

/** Default transfer encoding of string parameters*/

public static final java.lang.String DEFAULT_TRANSFER_ENCODING = "8bit";
}

