/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager.java $
 * $Revision: 673450 $
 * $Date: 2008-07-02 10:35:05 -0700 (Wed, 02 Jul 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn.tsccm;

import org.apache.http.conn.OperatedClientConnection;
import org.apache.http.impl.conn.DefaultClientConnectionOperator;

/**
 * Manages a pool of {@link OperatedClientConnection client connections}.
 * <p>
 * This class is derived from <code>MultiThreadedHttpConnectionManager</code>
 * in HttpClient 3. See there for original authors.
 * </p>
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 * @author <a href="mailto:becke@u.washington.edu">Michael Becke</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version $Revision: 673450 $ $Date: 2008-07-02 10:35:05 -0700 (Wed, 02 Jul 2008) $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class ThreadSafeClientConnManager implements org.apache.http.conn.ClientConnectionManager {

/**
 * Creates a new thread safe connection manager.
 *
 * @param params    the parameters for this manager
 * @param schreg    the scheme registry
 */

@Deprecated
public ThreadSafeClientConnManager(org.apache.http.params.HttpParams params, org.apache.http.conn.scheme.SchemeRegistry schreg) { throw new RuntimeException("Stub!"); }

@Deprecated
protected void finalize() throws java.lang.Throwable { throw new RuntimeException("Stub!"); }

/**
 * Hook for creating the connection pool.
 *
 * @return  the connection pool to use
 */

@Deprecated
protected org.apache.http.impl.conn.tsccm.AbstractConnPool createConnectionPool(org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

/**
 * Hook for creating the connection operator.
 * It is called by the constructor.
 * Derived classes can override this method to change the
 * instantiation of the operator.
 * The default implementation here instantiates
 * {@link DefaultClientConnectionOperator DefaultClientConnectionOperator}.
 *
 * @param schreg    the scheme registry to use, or <code>null</code>
 *
 * @return  the connection operator to use
 */

@Deprecated
protected org.apache.http.conn.ClientConnectionOperator createConnectionOperator(org.apache.http.conn.scheme.SchemeRegistry schreg) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.conn.scheme.SchemeRegistry getSchemeRegistry() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.conn.ClientConnectionRequest requestConnection(org.apache.http.conn.routing.HttpRoute route, java.lang.Object state) { throw new RuntimeException("Stub!"); }

@Deprecated
public void releaseConnection(org.apache.http.conn.ManagedClientConnection conn, long validDuration, java.util.concurrent.TimeUnit timeUnit) { throw new RuntimeException("Stub!"); }

@Deprecated
public void shutdown() { throw new RuntimeException("Stub!"); }

/**
 * Gets the total number of pooled connections for the given route.
 * This is the total number of connections that have been created and
 * are still in use by this connection manager for the route.
 * This value will not exceed the maximum number of connections per host.
 *
 * @param route     the route in question
 *
 * @return  the total number of pooled connections for that route
 */

@Deprecated
public int getConnectionsInPool(org.apache.http.conn.routing.HttpRoute route) { throw new RuntimeException("Stub!"); }

/**
 * Gets the total number of pooled connections.  This is the total number of
 * connections that have been created and are still in use by this connection
 * manager.  This value will not exceed the maximum number of connections
 * in total.
 *
 * @return the total number of pooled connections
 */

@Deprecated
public int getConnectionsInPool() { throw new RuntimeException("Stub!"); }

@Deprecated
public void closeIdleConnections(long idleTimeout, java.util.concurrent.TimeUnit tunit) { throw new RuntimeException("Stub!"); }

@Deprecated
public void closeExpiredConnections() { throw new RuntimeException("Stub!"); }

/** The operator for opening and updating connections. */

@Deprecated protected org.apache.http.conn.ClientConnectionOperator connOperator;

/** The pool of connections being managed. */

@Deprecated protected final org.apache.http.impl.conn.tsccm.AbstractConnPool connectionPool;
{ connectionPool = null; }

/** The schemes supported by this connection manager. */

@Deprecated protected org.apache.http.conn.scheme.SchemeRegistry schemeRegistry;
}

