/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/impl/SocketHttpClientConnection.java $
 * $Revision: 561083 $
 * $Date: 2007-07-30 11:31:17 -0700 (Mon, 30 Jul 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl;

import java.net.Socket;

/**
 * Implementation of a client-side HTTP connection that can be bound to a
 * network Socket in order to receive and transmit data.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 561083 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class SocketHttpClientConnection extends org.apache.http.impl.AbstractHttpClientConnection implements org.apache.http.HttpInetConnection {

@Deprecated
public SocketHttpClientConnection() { throw new RuntimeException("Stub!"); }

@Deprecated
protected void assertNotOpen() { throw new RuntimeException("Stub!"); }

@Deprecated
protected void assertOpen() { throw new RuntimeException("Stub!"); }

@Deprecated
protected org.apache.http.io.SessionInputBuffer createSessionInputBuffer(java.net.Socket socket, int buffersize, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
protected org.apache.http.io.SessionOutputBuffer createSessionOutputBuffer(java.net.Socket socket, int buffersize, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
protected void bind(java.net.Socket socket, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isOpen() { throw new RuntimeException("Stub!"); }

@Deprecated
protected java.net.Socket getSocket() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.net.InetAddress getLocalAddress() { throw new RuntimeException("Stub!"); }

@Deprecated
public int getLocalPort() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.net.InetAddress getRemoteAddress() { throw new RuntimeException("Stub!"); }

@Deprecated
public int getRemotePort() { throw new RuntimeException("Stub!"); }

@Deprecated
public void setSocketTimeout(int timeout) { throw new RuntimeException("Stub!"); }

@Deprecated
public int getSocketTimeout() { throw new RuntimeException("Stub!"); }

@Deprecated
public void shutdown() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }
}

