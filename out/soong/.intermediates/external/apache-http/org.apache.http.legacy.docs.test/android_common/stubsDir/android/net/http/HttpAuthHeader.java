/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.http;


/**
 * HttpAuthHeader: a class to store HTTP authentication-header parameters.
 * For more information, see: RFC 2617: HTTP Authentication.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HttpAuthHeader {

/**
 * Creates a new HTTP-authentication header object from the
 * input header string.
 * The header string is assumed to contain parameters of at
 * most one authentication-scheme (ensured by the caller).
 */

public HttpAuthHeader(java.lang.String header) { throw new RuntimeException("Stub!"); }

/**
 * @return True iff this is a proxy authentication header.
 */

public boolean isProxy() { throw new RuntimeException("Stub!"); }

/**
 * Marks this header as a proxy authentication header.
 */

public void setProxy() { throw new RuntimeException("Stub!"); }

/**
 * @return The username string.
 */

public java.lang.String getUsername() { throw new RuntimeException("Stub!"); }

/**
 * Sets the username string.
 */

public void setUsername(java.lang.String username) { throw new RuntimeException("Stub!"); }

/**
 * @return The password string.
 */

public java.lang.String getPassword() { throw new RuntimeException("Stub!"); }

/**
 * Sets the password string.
 */

public void setPassword(java.lang.String password) { throw new RuntimeException("Stub!"); }

/**
 * @return True iff this is the  BASIC-authentication request.
 */

public boolean isBasic() { throw new RuntimeException("Stub!"); }

/**
 * @return True iff this is the DIGEST-authentication request.
 */

public boolean isDigest() { throw new RuntimeException("Stub!"); }

/**
 * @return The authentication scheme requested. We currently
 * support two schemes:
 * HttpAuthHeader.BASIC  - basic, and
 * HttpAuthHeader.DIGEST - digest (algorithm=MD5, QOP="auth").
 */

public int getScheme() { throw new RuntimeException("Stub!"); }

/**
 * @return True if indicating that the previous request from
 * the client was rejected because the nonce value was stale.
 */

public boolean getStale() { throw new RuntimeException("Stub!"); }

/**
 * @return The realm value or null if there is none.
 */

public java.lang.String getRealm() { throw new RuntimeException("Stub!"); }

/**
 * @return The nonce value or null if there is none.
 */

public java.lang.String getNonce() { throw new RuntimeException("Stub!"); }

/**
 * @return The opaque value or null if there is none.
 */

public java.lang.String getOpaque() { throw new RuntimeException("Stub!"); }

/**
 * @return The QOP ("quality-of_protection") value or null if
 * there is none. The QOP value is always lower-case.
 */

public java.lang.String getQop() { throw new RuntimeException("Stub!"); }

/**
 * @return The name of the algorithm used or null if there is
 * none. By default, MD5 is used.
 */

public java.lang.String getAlgorithm() { throw new RuntimeException("Stub!"); }

/**
 * @return True iff the authentication scheme requested by the
 * server is supported; currently supported schemes:
 * BASIC,
 * DIGEST (only algorithm="md5", no qop or qop="auth).
 */

public boolean isSupportedScheme() { throw new RuntimeException("Stub!"); }

public static final int BASIC = 1; // 0x1

/**
 * Possible HTTP-authentication header tokens to search for:
 */

public static final java.lang.String BASIC_TOKEN = "Basic";

public static final int DIGEST = 2; // 0x2

public static final java.lang.String DIGEST_TOKEN = "Digest";

public static final int UNKNOWN = 0; // 0x0
}

