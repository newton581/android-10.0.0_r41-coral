/*
 * $Header: /home/jerenkrantz/tmp/commons/commons-convert/cvs/home/cvs/jakarta-commons//httpclient/src/java/org/apache/commons/httpclient/methods/multipart/PartBase.java,v 1.5 2004/04/18 23:51:37 jsdever Exp $
 * $Revision: 480424 $
 * $Date: 2006-11-29 06:56:49 +0100 (Wed, 29 Nov 2006) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package com.android.internal.http.multipart;


/**
 * Provides setters and getters for the basic Part properties.
 *
 * @author Michael Becke
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class PartBase extends com.android.internal.http.multipart.Part {

/**
 * Constructor.
 *
 * @param name The name of the part
 * @param contentType The content type, or <code>null</code>
 * @param charSet The character encoding, or <code>null</code>
 * @param transferEncoding The transfer encoding, or <code>null</code>
 */

public PartBase(java.lang.String name, java.lang.String contentType, java.lang.String charSet, java.lang.String transferEncoding) { throw new RuntimeException("Stub!"); }

/**
 * Returns the name.
 * @return The name.
 * @see Part#getName()
 */

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the content type of this part.
 * @return String The name.
 */

public java.lang.String getContentType() { throw new RuntimeException("Stub!"); }

/**
 * Return the character encoding of this part.
 * @return String The name.
 */

public java.lang.String getCharSet() { throw new RuntimeException("Stub!"); }

/**
 * Returns the transfer encoding of this part.
 * @return String The name.
 */

public java.lang.String getTransferEncoding() { throw new RuntimeException("Stub!"); }

/**
 * Sets the character encoding.
 *
 * @param charSet the character encoding, or <code>null</code> to exclude the character
 * encoding header
 */

public void setCharSet(java.lang.String charSet) { throw new RuntimeException("Stub!"); }

/**
 * Sets the content type.
 *
 * @param contentType the content type, or <code>null</code> to exclude the content type header
 */

public void setContentType(java.lang.String contentType) { throw new RuntimeException("Stub!"); }

/**
 * Sets the part name.
 *
 * @param name
 */

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Sets the transfer encoding.
 *
 * @param transferEncoding the transfer encoding, or <code>null</code> to exclude the
 * transfer encoding header
 */

public void setTransferEncoding(java.lang.String transferEncoding) { throw new RuntimeException("Stub!"); }
}

