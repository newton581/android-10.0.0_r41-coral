/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/impl/DefaultConnectionReuseStrategy.java $
 * $Revision: 602537 $
 * $Date: 2007-12-08 11:42:06 -0800 (Sat, 08 Dec 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl;

import org.apache.http.protocol.HTTP;

/**
 * Default implementation of a strategy deciding about connection re-use.
 * The default implementation first checks some basics, for example
 * whether the connection is still open or whether the end of the
 * request entity can be determined without closing the connection.
 * If these checks pass, the tokens in the "Connection" header will
 * be examined. In the absence of a "Connection" header, the
 * non-standard but commonly used "Proxy-Connection" header takes
 * it's role. A token "close" indicates that the connection cannot
 * be reused. If there is no such token, a token "keep-alive" indicates
 * that the connection should be re-used. If neither token is found,
 * or if there are no "Connection" headers, the default policy for
 * the HTTP version is applied. Since HTTP/1.1, connections are re-used
 * by default. Up until HTTP/1.0, connections are not re-used by default.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 * @version $Revision: 602537 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class DefaultConnectionReuseStrategy implements org.apache.http.ConnectionReuseStrategy {

@Deprecated
public DefaultConnectionReuseStrategy() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean keepAlive(org.apache.http.HttpResponse response, org.apache.http.protocol.HttpContext context) { throw new RuntimeException("Stub!"); }

/**
 * Creates a token iterator from a header iterator.
 * This method can be overridden to replace the implementation of
 * the token iterator.
 *
 * @param hit       the header iterator
 *
 * @return  the token iterator
 */

@Deprecated
protected org.apache.http.TokenIterator createTokenIterator(org.apache.http.HeaderIterator hit) { throw new RuntimeException("Stub!"); }
}

