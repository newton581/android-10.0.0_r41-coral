/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/cookie/NetscapeDraftSpec.java $
 * $Revision: 677240 $
 * $Date: 2008-07-16 04:25:47 -0700 (Wed, 16 Jul 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.cookie;

import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.MalformedCookieException;
import org.apache.http.Header;

/**
 * Netscape cookie draft compliant cookie policy
 *
 * @author  B.C. Holmes
 * @author <a href="mailto:jericho@thinkfree.com">Park, Sung-Gu</a>
 * @author <a href="mailto:dsale@us.britannica.com">Doug Sale</a>
 * @author Rod Waldhoff
 * @author dIon Gillard
 * @author Sean C. Sullivan
 * @author <a href="mailto:JEvans@Cyveillance.com">John Evans</a>
 * @author Marc A. Saegesser
 * @author <a href="mailto:oleg@ural.ru">Oleg Kalnichevski</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class NetscapeDraftSpec extends org.apache.http.impl.cookie.CookieSpecBase {

/** Default constructor */

@Deprecated
public NetscapeDraftSpec(java.lang.String[] datepatterns) { throw new RuntimeException("Stub!"); }

/** Default constructor */

@Deprecated
public NetscapeDraftSpec() { throw new RuntimeException("Stub!"); }

/**
 * Parses the Set-Cookie value into an array of <tt>Cookie</tt>s.
 *
 * <p>Syntax of the Set-Cookie HTTP Response Header:</p>
 *
 * <p>This is the format a CGI script would use to add to
 * the HTTP headers a new piece of data which is to be stored by
 * the client for later retrieval.</p>
 *
 * <PRE>
 *  Set-Cookie: NAME=VALUE; expires=DATE; path=PATH; domain=DOMAIN_NAME; secure
 * </PRE>
 *
 * <p>Please note that Netscape draft specification does not fully
 * conform to the HTTP header format. Netscape draft does not specify
 * whether multiple cookies may be sent in one header. Hence, comma
 * character may be present in unquoted cookie value or unquoted
 * parameter value.</p>
 *
 * @see <a href="http://wp.netscape.com/newsref/std/cookie_spec.html">
 *  The Cookie Spec.</a>
 *
 * @param header the <tt>Set-Cookie</tt> received from the server
 * @return an array of <tt>Cookie</tt>s parsed from the Set-Cookie value
 * @throws MalformedCookieException if an exception occurs during parsing
 */

@Deprecated
public java.util.List<org.apache.http.cookie.Cookie> parse(org.apache.http.Header header, org.apache.http.cookie.CookieOrigin origin) throws org.apache.http.cookie.MalformedCookieException { throw new RuntimeException("Stub!"); }

@Deprecated
public java.util.List<org.apache.http.Header> formatCookies(java.util.List<org.apache.http.cookie.Cookie> cookies) { throw new RuntimeException("Stub!"); }

@Deprecated
public int getVersion() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.Header getVersionHeader() { throw new RuntimeException("Stub!"); }

@Deprecated protected static final java.lang.String EXPIRES_PATTERN = "EEE, dd-MMM-yyyy HH:mm:ss z";
}

