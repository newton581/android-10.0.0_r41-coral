/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/tsccm/ConnPoolByRoute.java $
 * $Revision: 677240 $
 * $Date: 2008-07-16 04:25:47 -0700 (Wed, 16 Jul 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn.tsccm;

import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.conn.routing.HttpRoute;

/**
 * A connection pool that maintains connections by route.
 * This class is derived from <code>MultiThreadedHttpConnectionManager</code>
 * in HttpClient 3.x, see there for original authors. It implements the same
 * algorithm for connection re-use and connection-per-host enforcement:
 * <ul>
 * <li>connections are re-used only for the exact same route</li>
 * <li>connection limits are enforced per route rather than per host</li>
 * </ul>
 * Note that access to the pool datastructures is synchronized via the
 * {@link AbstractConnPool#poolLock poolLock} in the base class,
 * not via <code>synchronized</code> methods.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 * @author <a href="mailto:becke@u.washington.edu">Michael Becke</a>
 * @author and others
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class ConnPoolByRoute extends org.apache.http.impl.conn.tsccm.AbstractConnPool {

/**
 * Creates a new connection pool, managed by route.
 */

@Deprecated
public ConnPoolByRoute(org.apache.http.conn.ClientConnectionOperator operator, org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

/**
 * Creates the queue for {@link #freeConnections}.
 * Called once by the constructor.
 *
 * @return  a queue
 */

@Deprecated
protected java.util.Queue<org.apache.http.impl.conn.tsccm.BasicPoolEntry> createFreeConnQueue() { throw new RuntimeException("Stub!"); }

/**
 * Creates the queue for {@link #waitingThreads}.
 * Called once by the constructor.
 *
 * @return  a queue
 */

@Deprecated
protected java.util.Queue<org.apache.http.impl.conn.tsccm.WaitingThread> createWaitingThreadQueue() { throw new RuntimeException("Stub!"); }

/**
 * Creates the map for {@link #routeToPool}.
 * Called once by the constructor.
 *
 * @return  a map
 */

@Deprecated
protected java.util.Map<org.apache.http.conn.routing.HttpRoute,org.apache.http.impl.conn.tsccm.RouteSpecificPool> createRouteToPoolMap() { throw new RuntimeException("Stub!"); }

/**
 * Creates a new route-specific pool.
 * Called by {@link #getRoutePool} when necessary.
 *
 * @param route     the route
 *
 * @return  the new pool
 */

@Deprecated
protected org.apache.http.impl.conn.tsccm.RouteSpecificPool newRouteSpecificPool(org.apache.http.conn.routing.HttpRoute route) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new waiting thread.
 * Called by {@link #getRoutePool} when necessary.
 *
 * @param cond      the condition to wait for
 * @param rospl     the route specific pool, or <code>null</code>
 *
 * @return  a waiting thread representation
 */

@Deprecated
protected org.apache.http.impl.conn.tsccm.WaitingThread newWaitingThread(java.util.concurrent.locks.Condition cond, org.apache.http.impl.conn.tsccm.RouteSpecificPool rospl) { throw new RuntimeException("Stub!"); }

/**
 * Get a route-specific pool of available connections.
 *
 * @param route   the route
 * @param create    whether to create the pool if it doesn't exist
 *
 * @return  the pool for the argument route,
 *     never <code>null</code> if <code>create</code> is <code>true</code>
 */

@Deprecated
protected org.apache.http.impl.conn.tsccm.RouteSpecificPool getRoutePool(org.apache.http.conn.routing.HttpRoute route, boolean create) { throw new RuntimeException("Stub!"); }

@Deprecated
public int getConnectionsInPool(org.apache.http.conn.routing.HttpRoute route) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.impl.conn.tsccm.PoolEntryRequest requestPoolEntry(org.apache.http.conn.routing.HttpRoute route, java.lang.Object state) { throw new RuntimeException("Stub!"); }

/**
 * Obtains a pool entry with a connection within the given timeout.
 * If a {@link WaitingThread} is used to block, {@link WaitingThreadAborter#setWaitingThread(WaitingThread)}
 * must be called before blocking, to allow the thread to be interrupted.
 *
 * @param route     the route for which to get the connection
 * @param timeout   the timeout, 0 or negative for no timeout
 * @param tunit     the unit for the <code>timeout</code>,
 *                  may be <code>null</code> only if there is no timeout
 * @param aborter   an object which can abort a {@link WaitingThread}.
 *
 * @return  pool entry holding a connection for the route
 *
 * @throws ConnectionPoolTimeoutException
 *         if the timeout expired
 * @throws InterruptedException
 *         if the calling thread was interrupted
 */

@Deprecated
protected org.apache.http.impl.conn.tsccm.BasicPoolEntry getEntryBlocking(org.apache.http.conn.routing.HttpRoute route, java.lang.Object state, long timeout, java.util.concurrent.TimeUnit tunit, org.apache.http.impl.conn.tsccm.WaitingThreadAborter aborter) throws org.apache.http.conn.ConnectionPoolTimeoutException, java.lang.InterruptedException { throw new RuntimeException("Stub!"); }

@Deprecated
public void freeEntry(org.apache.http.impl.conn.tsccm.BasicPoolEntry entry, boolean reusable, long validDuration, java.util.concurrent.TimeUnit timeUnit) { throw new RuntimeException("Stub!"); }

/**
 * If available, get a free pool entry for a route.
 *
 * @param rospl       the route-specific pool from which to get an entry
 *
 * @return  an available pool entry for the given route, or
 *          <code>null</code> if none is available
 */

@Deprecated
protected org.apache.http.impl.conn.tsccm.BasicPoolEntry getFreeEntry(org.apache.http.impl.conn.tsccm.RouteSpecificPool rospl, java.lang.Object state) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new pool entry.
 * This method assumes that the new connection will be handed
 * out immediately.
 *
 * @param rospl       the route-specific pool for which to create the entry
 * @param op        the operator for creating a connection
 *
 * @return  the new pool entry for a new connection
 */

@Deprecated
protected org.apache.http.impl.conn.tsccm.BasicPoolEntry createEntry(org.apache.http.impl.conn.tsccm.RouteSpecificPool rospl, org.apache.http.conn.ClientConnectionOperator op) { throw new RuntimeException("Stub!"); }

/**
 * Deletes a given pool entry.
 * This closes the pooled connection and removes all references,
 * so that it can be GCed.
 *
 * <p><b>Note:</b> Does not remove the entry from the freeConnections list.
 * It is assumed that the caller has already handled this step.</p>
 * <!-- @@@ is that a good idea? or rather fix it? -->
 *
 * @param entry         the pool entry for the connection to delete
 */

@Deprecated
protected void deleteEntry(org.apache.http.impl.conn.tsccm.BasicPoolEntry entry) { throw new RuntimeException("Stub!"); }

/**
 * Delete an old, free pool entry to make room for a new one.
 * Used to replace pool entries with ones for a different route.
 */

@Deprecated
protected void deleteLeastUsedEntry() { throw new RuntimeException("Stub!"); }

@Deprecated
protected void handleLostEntry(org.apache.http.conn.routing.HttpRoute route) { throw new RuntimeException("Stub!"); }

/**
 * Notifies a waiting thread that a connection is available.
 * This will wake a thread waiting in the specific route pool,
 * if there is one.
 * Otherwise, a thread in the connection pool will be notified.
 *
 * @param rospl     the pool in which to notify, or <code>null</code>
 */

@Deprecated
protected void notifyWaitingThread(org.apache.http.impl.conn.tsccm.RouteSpecificPool rospl) { throw new RuntimeException("Stub!"); }

@Deprecated
public void deleteClosedConnections() { throw new RuntimeException("Stub!"); }

@Deprecated
public void shutdown() { throw new RuntimeException("Stub!"); }

/** The list of free connections */

@Deprecated protected java.util.Queue<org.apache.http.impl.conn.tsccm.BasicPoolEntry> freeConnections;

@Deprecated protected final int maxTotalConnections;
{ maxTotalConnections = 0; }

/** Connection operator for this pool */

@Deprecated protected final org.apache.http.conn.ClientConnectionOperator operator;
{ operator = null; }

/**
 * A map of route-specific pools.
 * Keys are of class {@link HttpRoute},
 * values of class {@link RouteSpecificPool}.
 */

@Deprecated protected final java.util.Map<org.apache.http.conn.routing.HttpRoute,org.apache.http.impl.conn.tsccm.RouteSpecificPool> routeToPool;
{ routeToPool = null; }

/** The list of WaitingThreads waiting for a connection */

@Deprecated protected java.util.Queue<org.apache.http.impl.conn.tsccm.WaitingThread> waitingThreads;
}

