/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/message/BasicListHeaderIterator.java $
 * $Revision: 584542 $
 * $Date: 2007-10-14 06:29:34 -0700 (Sun, 14 Oct 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.message;

import java.util.List;
import org.apache.http.HeaderIterator;
import java.util.NoSuchElementException;

/**
 * Implementation of a {@link HeaderIterator} based on a {@link List}.
 * For use by {@link HeaderGroup}.
 *
 * @version $Revision: 584542 $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicListHeaderIterator implements org.apache.http.HeaderIterator {

/**
 * Creates a new header iterator.
 *
 * @param headers   a list of headers over which to iterate
 * @param name      the name of the headers over which to iterate, or
 *                  <code>null</code> for any
 */

@Deprecated
public BasicListHeaderIterator(java.util.List headers, java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Determines the index of the next header.
 *
 * @param from      one less than the index to consider first,
 *                  -1 to search for the first header
 *
 * @return  the index of the next header that matches the filter name,
 *          or negative if there are no more headers
 */

@Deprecated
protected int findNext(int from) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether a header is part of the iteration.
 *
 * @param index     the index of the header to check
 *
 * @return  <code>true</code> if the header should be part of the
 *          iteration, <code>false</code> to skip
 */

@Deprecated
protected boolean filterHeader(int index) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean hasNext() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the next header from this iteration.
 *
 * @return  the next header in this iteration
 *
 * @throws NoSuchElementException   if there are no more headers
 */

@Deprecated
public org.apache.http.Header nextHeader() throws java.util.NoSuchElementException { throw new RuntimeException("Stub!"); }

/**
 * Returns the next header.
 * Same as {@link #nextHeader nextHeader}, but not type-safe.
 *
 * @return  the next header in this iteration
 *
 * @throws NoSuchElementException   if there are no more headers
 */

@Deprecated
public final java.lang.Object next() throws java.util.NoSuchElementException { throw new RuntimeException("Stub!"); }

/**
 * Removes the header that was returned last.
 */

@Deprecated
public void remove() throws java.lang.UnsupportedOperationException { throw new RuntimeException("Stub!"); }

/**
 * A list of headers to iterate over.
 * Not all elements of this array are necessarily part of the iteration.
 */

@Deprecated protected final java.util.List allHeaders;
{ allHeaders = null; }

/**
 * The position of the next header in {@link #allHeaders allHeaders}.
 * Negative if the iteration is over.
 */

@Deprecated protected int currentIndex;

/**
 * The header name to filter by.
 * <code>null</code> to iterate over all headers in the array.
 */

@Deprecated protected java.lang.String headerName;

/**
 * The position of the last returned header.
 * Negative if none has been returned so far.
 */

@Deprecated protected int lastIndex;
}

