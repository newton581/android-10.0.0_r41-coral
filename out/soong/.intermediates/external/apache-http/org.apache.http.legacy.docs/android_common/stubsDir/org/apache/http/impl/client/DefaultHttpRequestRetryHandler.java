/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/client/DefaultHttpRequestRetryHandler.java $
 * $Revision: 652726 $
 * $Date: 2008-05-01 18:16:51 -0700 (Thu, 01 May 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.client;

import org.apache.http.client.HttpRequestRetryHandler;

/**
 * The default {@link HttpRequestRetryHandler} used by request executors.
 *
 * @author Michael Becke
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class DefaultHttpRequestRetryHandler implements org.apache.http.client.HttpRequestRetryHandler {

/**
 * Default constructor
 */

@Deprecated
public DefaultHttpRequestRetryHandler(int retryCount, boolean requestSentRetryEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Default constructor
 */

@Deprecated
public DefaultHttpRequestRetryHandler() { throw new RuntimeException("Stub!"); }

/** 
 * Used <code>retryCount</code> and <code>requestSentRetryEnabled</code> to determine
 * if the given method should be retried.
 */

@Deprecated
public boolean retryRequest(java.io.IOException exception, int executionCount, org.apache.http.protocol.HttpContext context) { throw new RuntimeException("Stub!"); }

/**
 * @return <code>true</code> if this handler will retry methods that have
 * successfully sent their request, <code>false</code> otherwise
 */

@Deprecated
public boolean isRequestSentRetryEnabled() { throw new RuntimeException("Stub!"); }

/**
 * @return the maximum number of times a method will be retried
 */

@Deprecated
public int getRetryCount() { throw new RuntimeException("Stub!"); }
}

