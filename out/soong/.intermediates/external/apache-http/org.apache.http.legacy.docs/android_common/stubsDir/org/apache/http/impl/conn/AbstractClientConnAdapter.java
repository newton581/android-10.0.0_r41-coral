/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/AbstractClientConnAdapter.java $
 * $Revision: 672969 $
 * $Date: 2008-06-30 18:09:50 -0700 (Mon, 30 Jun 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn;

import org.apache.http.conn.ManagedClientConnection;
import org.apache.http.conn.OperatedClientConnection;
import java.io.InterruptedIOException;

/**
 * Abstract adapter from {@link OperatedClientConnection operated} to
 * {@link ManagedClientConnection managed} client connections.
 * Read and write methods are delegated to the wrapped connection.
 * Operations affecting the connection state have to be implemented
 * by derived classes. Operations for querying the connection state
 * are delegated to the wrapped connection if there is one, or
 * return a default value if there is none.
 * <br/>
 * This adapter tracks the checkpoints for reusable communication states,
 * as indicated by {@link #markReusable markReusable} and queried by
 * {@link #isMarkedReusable isMarkedReusable}.
 * All send and receive operations will automatically clear the mark.
 * <br/>
 * Connection release calls are delegated to the connection manager,
 * if there is one. {@link #abortConnection abortConnection} will
 * clear the reusability mark first. The connection manager is
 * expected to tolerate multiple calls to the release method.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version   $Revision: 672969 $ $Date: 2008-06-30 18:09:50 -0700 (Mon, 30 Jun 2008) $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract class AbstractClientConnAdapter implements org.apache.http.conn.ManagedClientConnection {

/**
 * Creates a new connection adapter.
 * The adapter is initially <i>not</i>
 * {@link #isMarkedReusable marked} as reusable.
 *
 * @param mgr       the connection manager, or <code>null</code>
 * @param conn      the connection to wrap, or <code>null</code>
 */

@Deprecated
protected AbstractClientConnAdapter(org.apache.http.conn.ClientConnectionManager mgr, org.apache.http.conn.OperatedClientConnection conn) { throw new RuntimeException("Stub!"); }

/**
 * Detaches this adapter from the wrapped connection.
 * This adapter becomes useless.
 */

@Deprecated
protected void detach() { throw new RuntimeException("Stub!"); }

@Deprecated
protected org.apache.http.conn.OperatedClientConnection getWrappedConnection() { throw new RuntimeException("Stub!"); }

@Deprecated
protected org.apache.http.conn.ClientConnectionManager getManager() { throw new RuntimeException("Stub!"); }

/**
 * Asserts that the connection has not been aborted.
 *
 * @throws InterruptedIOException   if the connection has been aborted
 */

@Deprecated
protected final void assertNotAborted() throws java.io.InterruptedIOException { throw new RuntimeException("Stub!"); }

/**
 * Asserts that there is a wrapped connection to delegate to.
 *
 * @throws IllegalStateException    if there is no wrapped connection
 *                                  or connection has been aborted
 */

@Deprecated
protected final void assertValid(org.apache.http.conn.OperatedClientConnection wrappedConn) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isOpen() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isStale() { throw new RuntimeException("Stub!"); }

@Deprecated
public void setSocketTimeout(int timeout) { throw new RuntimeException("Stub!"); }

@Deprecated
public int getSocketTimeout() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpConnectionMetrics getMetrics() { throw new RuntimeException("Stub!"); }

@Deprecated
public void flush() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isResponseAvailable(int timeout) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void receiveResponseEntity(org.apache.http.HttpResponse response) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpResponse receiveResponseHeader() throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void sendRequestEntity(org.apache.http.HttpEntityEnclosingRequest request) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void sendRequestHeader(org.apache.http.HttpRequest request) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public java.net.InetAddress getLocalAddress() { throw new RuntimeException("Stub!"); }

@Deprecated
public int getLocalPort() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.net.InetAddress getRemoteAddress() { throw new RuntimeException("Stub!"); }

@Deprecated
public int getRemotePort() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isSecure() { throw new RuntimeException("Stub!"); }

@Deprecated
public javax.net.ssl.SSLSession getSSLSession() { throw new RuntimeException("Stub!"); }

@Deprecated
public void markReusable() { throw new RuntimeException("Stub!"); }

@Deprecated
public void unmarkReusable() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isMarkedReusable() { throw new RuntimeException("Stub!"); }

@Deprecated
public void setIdleDuration(long duration, java.util.concurrent.TimeUnit unit) { throw new RuntimeException("Stub!"); }

@Deprecated
public void releaseConnection() { throw new RuntimeException("Stub!"); }

@Deprecated
public void abortConnection() { throw new RuntimeException("Stub!"); }
}

