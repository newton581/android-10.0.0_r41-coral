/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/message/BasicHeaderValueFormatter.java $
 * $Revision: 574185 $
 * $Date: 2007-09-10 02:19:47 -0700 (Mon, 10 Sep 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.message;


/**
 * Basic implementation for formatting header value elements.
 * Instances of this class are stateless and thread-safe.
 * Derived classes are expected to maintain these properties.
 *
 * @author <a href="mailto:oleg at ural.com">Oleg Kalnichevski</a>
 * @author and others
 *
 *
 * <!-- empty lines above to avoid 'svn diff' context problems -->
 * @version $Revision: 574185 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicHeaderValueFormatter implements org.apache.http.message.HeaderValueFormatter {

@Deprecated
public BasicHeaderValueFormatter() { throw new RuntimeException("Stub!"); }

/**
 * Formats an array of header elements.
 *
 * @param elems     the header elements to format
 * @param quote     <code>true</code> to always format with quoted values,
 *                  <code>false</code> to use quotes only when necessary
 * @param formatter         the formatter to use, or <code>null</code>
 *                          for the {@link #DEFAULT default}
 *
 * @return  the formatted header elements
 */

@Deprecated
public static final java.lang.String formatElements(org.apache.http.HeaderElement[] elems, boolean quote, org.apache.http.message.HeaderValueFormatter formatter) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.util.CharArrayBuffer formatElements(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.HeaderElement[] elems, boolean quote) { throw new RuntimeException("Stub!"); }

/**
 * Estimates the length of formatted header elements.
 *
 * @param elems     the header elements to format, or <code>null</code>
 *
 * @return  a length estimate, in number of characters
 */

@Deprecated
protected int estimateElementsLen(org.apache.http.HeaderElement[] elems) { throw new RuntimeException("Stub!"); }

/**
 * Formats a header element.
 *
 * @param elem      the header element to format
 * @param quote     <code>true</code> to always format with quoted values,
 *                  <code>false</code> to use quotes only when necessary
 * @param formatter         the formatter to use, or <code>null</code>
 *                          for the {@link #DEFAULT default}
 *
 * @return  the formatted header element
 */

@Deprecated
public static final java.lang.String formatHeaderElement(org.apache.http.HeaderElement elem, boolean quote, org.apache.http.message.HeaderValueFormatter formatter) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.util.CharArrayBuffer formatHeaderElement(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.HeaderElement elem, boolean quote) { throw new RuntimeException("Stub!"); }

/**
 * Estimates the length of a formatted header element.
 *
 * @param elem      the header element to format, or <code>null</code>
 *
 * @return  a length estimate, in number of characters
 */

@Deprecated
protected int estimateHeaderElementLen(org.apache.http.HeaderElement elem) { throw new RuntimeException("Stub!"); }

/**
 * Formats a set of parameters.
 *
 * @param nvps      the parameters to format
 * @param quote     <code>true</code> to always format with quoted values,
 *                  <code>false</code> to use quotes only when necessary
 * @param formatter         the formatter to use, or <code>null</code>
 *                          for the {@link #DEFAULT default}
 *
 * @return  the formatted parameters
 */

@Deprecated
public static final java.lang.String formatParameters(org.apache.http.NameValuePair[] nvps, boolean quote, org.apache.http.message.HeaderValueFormatter formatter) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.util.CharArrayBuffer formatParameters(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.NameValuePair[] nvps, boolean quote) { throw new RuntimeException("Stub!"); }

/**
 * Estimates the length of formatted parameters.
 *
 * @param nvps      the parameters to format, or <code>null</code>
 *
 * @return  a length estimate, in number of characters
 */

@Deprecated
protected int estimateParametersLen(org.apache.http.NameValuePair[] nvps) { throw new RuntimeException("Stub!"); }

/**
 * Formats a name-value pair.
 *
 * @param nvp       the name-value pair to format
 * @param quote     <code>true</code> to always format with a quoted value,
 *                  <code>false</code> to use quotes only when necessary
 * @param formatter         the formatter to use, or <code>null</code>
 *                          for the {@link #DEFAULT default}
 *
 * @return  the formatted name-value pair
 */

@Deprecated
public static final java.lang.String formatNameValuePair(org.apache.http.NameValuePair nvp, boolean quote, org.apache.http.message.HeaderValueFormatter formatter) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.util.CharArrayBuffer formatNameValuePair(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.NameValuePair nvp, boolean quote) { throw new RuntimeException("Stub!"); }

/**
 * Estimates the length of a formatted name-value pair.
 *
 * @param nvp       the name-value pair to format, or <code>null</code>
 *
 * @return  a length estimate, in number of characters
 */

@Deprecated
protected int estimateNameValuePairLen(org.apache.http.NameValuePair nvp) { throw new RuntimeException("Stub!"); }

/**
 * Actually formats the value of a name-value pair.
 * This does not include a leading = character.
 * Called from {@link #formatNameValuePair formatNameValuePair}.
 *
 * @param buffer    the buffer to append to, never <code>null</code>
 * @param value     the value to append, never <code>null</code>
 * @param quote     <code>true</code> to always format with quotes,
 *                  <code>false</code> to use quotes only when necessary
 */

@Deprecated
protected void doFormatValue(org.apache.http.util.CharArrayBuffer buffer, java.lang.String value, boolean quote) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether a character is a {@link #SEPARATORS separator}.
 *
 * @param ch        the character to check
 *
 * @return  <code>true</code> if the character is a separator,
 *          <code>false</code> otherwise
 */

@Deprecated
protected boolean isSeparator(char ch) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether a character is {@link #UNSAFE_CHARS unsafe}.
 *
 * @param ch        the character to check
 *
 * @return  <code>true</code> if the character is unsafe,
 *          <code>false</code> otherwise
 */

@Deprecated
protected boolean isUnsafe(char ch) { throw new RuntimeException("Stub!"); }

/**
 * A default instance of this class, for use as default or fallback.
 * Note that {@link BasicHeaderValueFormatter} is not a singleton, there
 * can be many instances of the class itself and of derived classes.
 * The instance here provides non-customized, default behavior.
 */

@Deprecated public static final org.apache.http.message.BasicHeaderValueFormatter DEFAULT;
static { DEFAULT = null; }

/**
 * Special characters that can be used as separators in HTTP parameters.
 * These special characters MUST be in a quoted string to be used within
 * a parameter value .
 */

@Deprecated public static final java.lang.String SEPARATORS = " ;,:@()<>\\\"/[]?={}\t";

/**
 * Unsafe special characters that must be escaped using the backslash
 * character
 */

@Deprecated public static final java.lang.String UNSAFE_CHARS = "\"\\";
}

