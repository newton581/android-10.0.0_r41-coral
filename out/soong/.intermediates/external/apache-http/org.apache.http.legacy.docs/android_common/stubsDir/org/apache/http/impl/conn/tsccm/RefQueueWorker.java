/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/tsccm/RefQueueWorker.java $
 * $Revision: 673450 $
 * $Date: 2008-07-02 10:35:05 -0700 (Wed, 02 Jul 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn.tsccm;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.Reference;

/**
 * A worker thread for processing queued references.
 * {@link Reference Reference}s can be
 * {@link ReferenceQueue queued}
 * automatically by the garbage collector.
 * If that feature is used, a daemon thread should be executing
 * this worker. It will pick up the queued references and pass them
 * on to a handler for appropriate processing.
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class RefQueueWorker implements java.lang.Runnable {

/**
 * Instantiates a new worker to listen for lost connections.
 *
 * @param queue     the queue on which to wait for references
 * @param handler   the handler to pass the references to
 */

@Deprecated
public RefQueueWorker(java.lang.ref.ReferenceQueue<?> queue, org.apache.http.impl.conn.tsccm.RefQueueHandler handler) { throw new RuntimeException("Stub!"); }

/**
 * The main loop of this worker.
 * If initialization succeeds, this method will only return
 * after {@link #shutdown shutdown()}. Only one thread can
 * execute the main loop at any time.
 */

@Deprecated
public void run() { throw new RuntimeException("Stub!"); }

/**
 * Shuts down this worker.
 * It can be re-started afterwards by another call to {@link #run run()}.
 */

@Deprecated
public void shutdown() { throw new RuntimeException("Stub!"); }

/**
 * Obtains a description of this worker.
 *
 * @return  a descriptive string for this worker
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** The handler for the references found. */

@Deprecated protected final org.apache.http.impl.conn.tsccm.RefQueueHandler refHandler;
{ refHandler = null; }

/** The reference queue to monitor. */

@Deprecated protected final java.lang.ref.ReferenceQueue<?> refQueue;
{ refQueue = null; }

/**
 * The thread executing this handler.
 * This attribute is also used as a shutdown indicator.
 */

@Deprecated protected volatile java.lang.Thread workerThread;
}

