/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.language;

import org.apache.commons.codec.EncoderException;

/**
 * Encodes a string into a metaphone value.
 * <p>
 * Initial Java implementation by <CITE>William B. Brogden. December, 1997</CITE>.
 * Permission given by <CITE>wbrogden</CITE> for code to be used anywhere.
 * </p>
 * <p>
 * <CITE>Hanging on the Metaphone</CITE> by <CITE>Lawrence Philips</CITE> in <CITE>Computer Language of Dec. 1990, p
 * 39.</CITE>
 * </p>
 *
 * @author Apache Software Foundation
 * @version $Id: Metaphone.java,v 1.20 2004/06/05 18:32:04 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class Metaphone implements org.apache.commons.codec.StringEncoder {

/**
 * Creates an instance of the Metaphone encoder
 */

@Deprecated
public Metaphone() { throw new RuntimeException("Stub!"); }

/**
 * Find the metaphone value of a String. This is similar to the
 * soundex algorithm, but better at finding similar sounding words.
 * All input is converted to upper case.
 * Limitations: Input format is expected to be a single ASCII word
 * with only characters in the A - Z range, no punctuation or numbers.
 *
 * @param txt String to find the metaphone code for
 * @return A metaphone code corresponding to the String supplied
 */

@Deprecated
public java.lang.String metaphone(java.lang.String txt) { throw new RuntimeException("Stub!"); }

/**
 * Encodes an Object using the metaphone algorithm.  This method
 * is provided in order to satisfy the requirements of the
 * Encoder interface, and will throw an EncoderException if the
 * supplied object is not of type java.lang.String.
 *
 * @param pObject Object to encode
 * @return An object (or type java.lang.String) containing the
 *         metaphone code which corresponds to the String supplied.
 * @throws EncoderException if the parameter supplied is not
 *                          of type java.lang.String
 */

@Deprecated
public java.lang.Object encode(java.lang.Object pObject) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a String using the Metaphone algorithm.
 *
 * @param pString String object to encode
 * @return The metaphone code corresponding to the String supplied
 */

@Deprecated
public java.lang.String encode(java.lang.String pString) { throw new RuntimeException("Stub!"); }

/**
 * Tests is the metaphones of two strings are identical.
 *
 * @param str1 First of two strings to compare
 * @param str2 Second of two strings to compare
 * @return true if the metaphones of these strings are identical,
 *         false otherwise.
 */

@Deprecated
public boolean isMetaphoneEqual(java.lang.String str1, java.lang.String str2) { throw new RuntimeException("Stub!"); }

/**
 * Returns the maxCodeLen.
 * @return int
 */

@Deprecated
public int getMaxCodeLen() { throw new RuntimeException("Stub!"); }

/**
 * Sets the maxCodeLen.
 * @param maxCodeLen The maxCodeLen to set
 */

@Deprecated
public void setMaxCodeLen(int maxCodeLen) { throw new RuntimeException("Stub!"); }
}

