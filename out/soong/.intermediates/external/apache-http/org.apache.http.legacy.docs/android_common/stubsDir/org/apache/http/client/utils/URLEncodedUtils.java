/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/client/utils/URLEncodedUtils.java $
 * $Revision: 655107 $
 * $Date: 2008-05-10 08:20:42 -0700 (Sat, 10 May 2008) $
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.client.utils;

import org.apache.http.NameValuePair;
import org.apache.http.protocol.HTTP;
import java.net.URI;
import org.apache.http.HttpEntity;
import java.io.IOException;
import java.util.Scanner;
import java.util.List;

/**
 * A collection of utilities for encoding URLs.
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class URLEncodedUtils {

@Deprecated
public URLEncodedUtils() { throw new RuntimeException("Stub!"); }

/**
 * Returns a list of {@link NameValuePair NameValuePairs} as built from the
 * URI's query portion. For example, a URI of
 * http://example.org/path/to/file?a=1&b=2&c=3 would return a list of three
 * NameValuePairs, one for a=1, one for b=2, and one for c=3.
 * <p>
 * This is typically useful while parsing an HTTP PUT.
 *
 * @param uri
 *            uri to parse
 * @param encoding
 *            encoding to use while parsing the query
 */

@Deprecated
public static java.util.List<org.apache.http.NameValuePair> parse(java.net.URI uri, java.lang.String encoding) { throw new RuntimeException("Stub!"); }

/**
 * Returns a list of {@link NameValuePair NameValuePairs} as parsed from an
 * {@link HttpEntity}. The encoding is taken from the entity's
 * Content-Encoding header.
 * <p>
 * This is typically used while parsing an HTTP POST.
 *
 * @param entity
 *            The entity to parse
 * @throws IOException
 *             If there was an exception getting the entity's data.
 */

@Deprecated
public static java.util.List<org.apache.http.NameValuePair> parse(org.apache.http.HttpEntity entity) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Returns true if the entity's Content-Type header is
 * <code>application/x-www-form-urlencoded</code>.
 */

@Deprecated
public static boolean isEncoded(org.apache.http.HttpEntity entity) { throw new RuntimeException("Stub!"); }

/**
 * Adds all parameters within the Scanner to the list of
 * <code>parameters</code>, as encoded by <code>encoding</code>. For
 * example, a scanner containing the string <code>a=1&b=2&c=3</code> would
 * add the {@link NameValuePair NameValuePairs} a=1, b=2, and c=3 to the
 * list of parameters.
 *
 * @param parameters
 *            List to add parameters to.
 * @param scanner
 *            Input that contains the parameters to parse.
 * @param encoding
 *            Encoding to use when decoding the parameters.
 */

@Deprecated
public static void parse(java.util.List<org.apache.http.NameValuePair> parameters, java.util.Scanner scanner, java.lang.String encoding) { throw new RuntimeException("Stub!"); }

/**
 * Returns a String that is suitable for use as an <code>application/x-www-form-urlencoded</code>
 * list of parameters in an HTTP PUT or HTTP POST.
 *
 * @param parameters  The parameters to include.
 * @param encoding The encoding to use.
 */

@Deprecated
public static java.lang.String format(java.util.List<? extends org.apache.http.NameValuePair> parameters, java.lang.String encoding) { throw new RuntimeException("Stub!"); }

@Deprecated public static final java.lang.String CONTENT_TYPE = "application/x-www-form-urlencoded";
}

