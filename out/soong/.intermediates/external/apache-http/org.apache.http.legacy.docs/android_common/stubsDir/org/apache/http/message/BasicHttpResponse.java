/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/message/BasicHttpResponse.java $
 * $Revision: 573864 $
 * $Date: 2007-09-08 08:53:25 -0700 (Sat, 08 Sep 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.message;


/**
 * Basic implementation of an HTTP response that can be modified.
 * This implementation makes sure that there always is a status line.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 573864 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicHttpResponse extends org.apache.http.message.AbstractHttpMessage implements org.apache.http.HttpResponse {

/**
 * Creates a new response.
 * This is the constructor to which all others map.
 *
 * @param statusline        the status line
 * @param catalog           the reason phrase catalog, or
 *                          <code>null</code> to disable automatic
 *                          reason phrase lookup
 * @param locale            the locale for looking up reason phrases, or
 *                          <code>null</code> for the system locale
 */

@Deprecated
public BasicHttpResponse(org.apache.http.StatusLine statusline, org.apache.http.ReasonPhraseCatalog catalog, java.util.Locale locale) { throw new RuntimeException("Stub!"); }

/**
 * Creates a response from a status line.
 * The response will not have a reason phrase catalog and
 * use the system default locale.
 *
 * @param statusline        the status line
 */

@Deprecated
public BasicHttpResponse(org.apache.http.StatusLine statusline) { throw new RuntimeException("Stub!"); }

/**
 * Creates a response from elements of a status line.
 * The response will not have a reason phrase catalog and
 * use the system default locale.
 *
 * @param ver       the protocol version of the response
 * @param code      the status code of the response
 * @param reason    the reason phrase to the status code, or
 *                  <code>null</code>
 */

@Deprecated
public BasicHttpResponse(org.apache.http.ProtocolVersion ver, int code, java.lang.String reason) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.ProtocolVersion getProtocolVersion() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.StatusLine getStatusLine() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpEntity getEntity() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.util.Locale getLocale() { throw new RuntimeException("Stub!"); }

@Deprecated
public void setStatusLine(org.apache.http.StatusLine statusline) { throw new RuntimeException("Stub!"); }

@Deprecated
public void setStatusLine(org.apache.http.ProtocolVersion ver, int code) { throw new RuntimeException("Stub!"); }

@Deprecated
public void setStatusLine(org.apache.http.ProtocolVersion ver, int code, java.lang.String reason) { throw new RuntimeException("Stub!"); }

@Deprecated
public void setStatusCode(int code) { throw new RuntimeException("Stub!"); }

@Deprecated
public void setReasonPhrase(java.lang.String reason) { throw new RuntimeException("Stub!"); }

@Deprecated
public void setEntity(org.apache.http.HttpEntity entity) { throw new RuntimeException("Stub!"); }

@Deprecated
public void setLocale(java.util.Locale loc) { throw new RuntimeException("Stub!"); }

/**
 * Looks up a reason phrase.
 * This method evaluates the currently set catalog and locale.
 * It also handles a missing catalog.
 *
 * @param code      the status code for which to look up the reason
 *
 * @return  the reason phrase, or <code>null</code> if there is none
 */

@Deprecated
protected java.lang.String getReason(int code) { throw new RuntimeException("Stub!"); }
}

