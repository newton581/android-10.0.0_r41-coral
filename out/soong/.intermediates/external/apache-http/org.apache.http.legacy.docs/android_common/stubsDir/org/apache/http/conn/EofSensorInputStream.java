/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/conn/EofSensorInputStream.java $
 * $Revision: 672367 $
 * $Date: 2008-06-27 12:49:20 -0700 (Fri, 27 Jun 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.conn;

import java.io.IOException;

/**
 * A stream wrapper that triggers actions on {@link #close close()} and EOF.
 * Primarily used to auto-release an underlying
 * {@link ManagedClientConnection connection}
 * when the response body is consumed or no longer needed.
 *
 * <p>
 * This class is based on <code>AutoCloseInputStream</code> in HttpClient 3.1,
 * but has notable differences. It does not allow mark/reset, distinguishes
 * different kinds of event, and does not always close the underlying stream
 * on EOF. That decision is left to the {@link EofSensorWatcher watcher}.
 * </p>
 *
 * @see EofSensorWatcher EofSensorWatcher
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 * @author Ortwin Glueck
 * @author Eric Johnson
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version $Revision: 672367 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class EofSensorInputStream extends java.io.InputStream implements org.apache.http.conn.ConnectionReleaseTrigger {

/**
 * Creates a new EOF sensor.
 * If no watcher is passed, the underlying stream will simply be
 * closed when EOF is detected or {@link #close close} is called.
 * Otherwise, the watcher decides whether the underlying stream
 * should be closed before detaching from it.
 *
 * @param in        the wrapped stream
 * @param watcher   the watcher for events, or <code>null</code> for
 *                  auto-close behavior without notification
 */

@Deprecated
public EofSensorInputStream(java.io.InputStream in, org.apache.http.conn.EofSensorWatcher watcher) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether the underlying stream can be read from.
 *
 * @return  <code>true</code> if the underlying stream is accessible,
 *          <code>false</code> if this stream is in EOF mode and
 *          detached from the underlying stream
 *
 * @throws IOException      if this stream is already closed
 */

@Deprecated
protected boolean isReadAllowed() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public int read() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public int read(byte[] b, int off, int len) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public int read(byte[] b) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public int available() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Detects EOF and notifies the watcher.
 * This method should only be called while the underlying stream is
 * still accessible. Use {@link #isReadAllowed isReadAllowed} to
 * check that condition.
 * <br/>
 * If EOF is detected, the watcher will be notified and this stream
 * is detached from the underlying stream. This prevents multiple
 * notifications from this stream.
 *
 * @param eof       the result of the calling read operation.
 *                  A negative value indicates that EOF is reached.
 *
 * @throws IOException
 *          in case of an IO problem on closing the underlying stream
 */

@Deprecated
protected void checkEOF(int eof) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Detects stream close and notifies the watcher.
 * There's not much to detect since this is called by {@link #close close}.
 * The watcher will only be notified if this stream is closed
 * for the first time and before EOF has been detected.
 * This stream will be detached from the underlying stream to prevent
 * multiple notifications to the watcher.
 *
 * @throws IOException
 *          in case of an IO problem on closing the underlying stream
 */

@Deprecated
protected void checkClose() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Detects stream abort and notifies the watcher.
 * There's not much to detect since this is called by
 * {@link #abortConnection abortConnection}.
 * The watcher will only be notified if this stream is aborted
 * for the first time and before EOF has been detected or the
 * stream has been {@link #close closed} gracefully.
 * This stream will be detached from the underlying stream to prevent
 * multiple notifications to the watcher.
 *
 * @throws IOException
 *          in case of an IO problem on closing the underlying stream
 */

@Deprecated
protected void checkAbort() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Same as {@link #close close()}.
 */

@Deprecated
public void releaseConnection() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Aborts this stream.
 * This is a special version of {@link #close close()} which prevents
 * re-use of the underlying connection, if any. Calling this method
 * indicates that there should be no attempt to read until the end of
 * the stream.
 */

@Deprecated
public void abortConnection() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * The wrapped input stream, while accessible.
 * The value changes to <code>null</code> when the wrapped stream
 * becomes inaccessible.
 */

@Deprecated protected java.io.InputStream wrappedStream;
}

