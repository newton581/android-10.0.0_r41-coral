/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/message/BasicLineFormatter.java $
 * $Revision: 574185 $
 * $Date: 2007-09-10 02:19:47 -0700 (Mon, 10 Sep 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.message;


/**
 * Interface for formatting elements of the HEAD section of an HTTP message.
 * This is the complement to {@link LineParser}.
 * There are individual methods for formatting a request line, a
 * status line, or a header line. The formatting does <i>not</i> include the
 * trailing line break sequence CR-LF.
 * The formatted lines are returned in memory, the formatter does not depend
 * on any specific IO mechanism.
 * Instances of this interface are expected to be stateless and thread-safe.
 *
 * @author <a href="mailto:remm@apache.org">Remy Maucherat</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 * @author and others
 *
 *
 * <!-- empty lines above to avoid 'svn diff' context problems -->
 * @version $Revision: 574185 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicLineFormatter implements org.apache.http.message.LineFormatter {

@Deprecated
public BasicLineFormatter() { throw new RuntimeException("Stub!"); }

/**
 * Obtains a buffer for formatting.
 *
 * @param buffer    a buffer already available, or <code>null</code>
 *
 * @return  the cleared argument buffer if there is one, or
 *          a new empty buffer that can be used for formatting
 */

@Deprecated
protected org.apache.http.util.CharArrayBuffer initBuffer(org.apache.http.util.CharArrayBuffer buffer) { throw new RuntimeException("Stub!"); }

/**
 * Formats a protocol version.
 *
 * @param version           the protocol version to format
 * @param formatter         the formatter to use, or
 *                          <code>null</code> for the
 *                          {@link #DEFAULT default}
 *
 * @return  the formatted protocol version
 */

@Deprecated
public static final java.lang.String formatProtocolVersion(org.apache.http.ProtocolVersion version, org.apache.http.message.LineFormatter formatter) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.util.CharArrayBuffer appendProtocolVersion(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.ProtocolVersion version) { throw new RuntimeException("Stub!"); }

/**
 * Guesses the length of a formatted protocol version.
 * Needed to guess the length of a formatted request or status line.
 *
 * @param version   the protocol version to format, or <code>null</code>
 *
 * @return  the estimated length of the formatted protocol version,
 *          in characters
 */

@Deprecated
protected int estimateProtocolVersionLen(org.apache.http.ProtocolVersion version) { throw new RuntimeException("Stub!"); }

/**
 * Formats a request line.
 *
 * @param reqline           the request line to format
 * @param formatter         the formatter to use, or
 *                          <code>null</code> for the
 *                          {@link #DEFAULT default}
 *
 * @return  the formatted request line
 */

@Deprecated
public static final java.lang.String formatRequestLine(org.apache.http.RequestLine reqline, org.apache.http.message.LineFormatter formatter) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.util.CharArrayBuffer formatRequestLine(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.RequestLine reqline) { throw new RuntimeException("Stub!"); }

/**
 * Actually formats a request line.
 * Called from {@link #formatRequestLine}.
 *
 * @param buffer    the empty buffer into which to format,
 *                  never <code>null</code>
 * @param reqline   the request line to format, never <code>null</code>
 */

@Deprecated
protected void doFormatRequestLine(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.RequestLine reqline) { throw new RuntimeException("Stub!"); }

/**
 * Formats a status line.
 *
 * @param statline          the status line to format
 * @param formatter         the formatter to use, or
 *                          <code>null</code> for the
 *                          {@link #DEFAULT default}
 *
 * @return  the formatted status line
 */

@Deprecated
public static final java.lang.String formatStatusLine(org.apache.http.StatusLine statline, org.apache.http.message.LineFormatter formatter) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.util.CharArrayBuffer formatStatusLine(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.StatusLine statline) { throw new RuntimeException("Stub!"); }

/**
 * Actually formats a status line.
 * Called from {@link #formatStatusLine}.
 *
 * @param buffer    the empty buffer into which to format,
 *                  never <code>null</code>
 * @param statline  the status line to format, never <code>null</code>
 */

@Deprecated
protected void doFormatStatusLine(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.StatusLine statline) { throw new RuntimeException("Stub!"); }

/**
 * Formats a header.
 *
 * @param header            the header to format
 * @param formatter         the formatter to use, or
 *                          <code>null</code> for the
 *                          {@link #DEFAULT default}
 *
 * @return  the formatted header
 */

@Deprecated
public static final java.lang.String formatHeader(org.apache.http.Header header, org.apache.http.message.LineFormatter formatter) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.util.CharArrayBuffer formatHeader(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.Header header) { throw new RuntimeException("Stub!"); }

/**
 * Actually formats a header.
 * Called from {@link #formatHeader}.
 *
 * @param buffer    the empty buffer into which to format,
 *                  never <code>null</code>
 * @param header    the header to format, never <code>null</code>
 */

@Deprecated
protected void doFormatHeader(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.Header header) { throw new RuntimeException("Stub!"); }

/**
 * A default instance of this class, for use as default or fallback.
 * Note that {@link BasicLineFormatter} is not a singleton, there can
 * be many instances of the class itself and of derived classes.
 * The instance here provides non-customized, default behavior.
 */

@Deprecated public static final org.apache.http.message.BasicLineFormatter DEFAULT;
static { DEFAULT = null; }
}

