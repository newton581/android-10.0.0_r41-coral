/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/client/BasicCredentialsProvider.java $
 * $Revision: 653041 $
 * $Date: 2008-05-03 03:39:28 -0700 (Sat, 03 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.client;

import org.apache.http.client.CredentialsProvider;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;

/**
 * Default implementation of {@link CredentialsProvider}
 *
 * @author <a href="mailto:remm@apache.org">Remy Maucherat</a>
 * @author Rodney Waldhoff
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 * @author Sean C. Sullivan
 * @author <a href="mailto:becke@u.washington.edu">Michael Becke</a>
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:adrian@intencha.com">Adrian Sutton</a>
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicCredentialsProvider implements org.apache.http.client.CredentialsProvider {

/**
 * Default constructor.
 */

@Deprecated
public BasicCredentialsProvider() { throw new RuntimeException("Stub!"); }

/** 
 * Sets the {@link Credentials credentials} for the given authentication
 * scope. Any previous credentials for the given scope will be overwritten.
 *
 * @param authscope the {@link AuthScope authentication scope}
 * @param credentials the authentication {@link Credentials credentials}
 * for the given scope.
 *
 * @see #getCredentials(AuthScope)
 */

@Deprecated
public synchronized void setCredentials(org.apache.http.auth.AuthScope authscope, org.apache.http.auth.Credentials credentials) { throw new RuntimeException("Stub!"); }

/**
 * Get the {@link Credentials credentials} for the given authentication scope.
 *
 * @param authscope the {@link AuthScope authentication scope}
 * @return the credentials
 *
 * @see #setCredentials(AuthScope, Credentials)
 */

@Deprecated
public synchronized org.apache.http.auth.Credentials getCredentials(org.apache.http.auth.AuthScope authscope) { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Clears all credentials.
 */

@Deprecated
public synchronized void clear() { throw new RuntimeException("Stub!"); }
}

