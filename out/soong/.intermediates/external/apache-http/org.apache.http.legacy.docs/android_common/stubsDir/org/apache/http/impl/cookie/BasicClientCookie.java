/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/cookie/BasicClientCookie.java $
 * $Revision: 659191 $
 * $Date: 2008-05-22 11:26:53 -0700 (Thu, 22 May 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.cookie;

import java.util.Date;

/**
 * HTTP "magic-cookie" represents a piece of state information
 * that the HTTP agent and the target server can exchange to maintain
 * a session.
 *
 * @author B.C. Holmes
 * @author <a href="mailto:jericho@thinkfree.com">Park, Sung-Gu</a>
 * @author <a href="mailto:dsale@us.britannica.com">Doug Sale</a>
 * @author Rod Waldhoff
 * @author dIon Gillard
 * @author Sean C. Sullivan
 * @author <a href="mailto:JEvans@Cyveillance.com">John Evans</a>
 * @author Marc A. Saegesser
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 *
 * @version $Revision: 659191 $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicClientCookie implements org.apache.http.cookie.SetCookie, org.apache.http.cookie.ClientCookie, java.lang.Cloneable {

/**
 * Default Constructor taking a name and a value. The value may be null.
 *
 * @param name The name.
 * @param value The value.
 */

@Deprecated
public BasicClientCookie(java.lang.String name, java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Returns the name.
 *
 * @return String name The name
 */

@Deprecated
public java.lang.String getName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the value.
 *
 * @return String value The current value.
 */

@Deprecated
public java.lang.String getValue() { throw new RuntimeException("Stub!"); }

/**
 * Sets the value
 *
 * @param value
 */

@Deprecated
public void setValue(java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Returns the comment describing the purpose of this cookie, or
 * <tt>null</tt> if no such comment has been defined.
 *
 * @return comment
 *
 * @see #setComment(String)
 */

@Deprecated
public java.lang.String getComment() { throw new RuntimeException("Stub!"); }

/**
 * If a user agent (web browser) presents this cookie to a user, the
 * cookie's purpose will be described using this comment.
 *
 * @param comment
 *
 * @see #getComment()
 */

@Deprecated
public void setComment(java.lang.String comment) { throw new RuntimeException("Stub!"); }

/**
 * Returns null. Cookies prior to RFC2965 do not set this attribute
 */

@Deprecated
public java.lang.String getCommentURL() { throw new RuntimeException("Stub!"); }

/**
 * Returns the expiration {@link Date} of the cookie, or <tt>null</tt>
 * if none exists.
 * <p><strong>Note:</strong> the object returned by this method is
 * considered immutable. Changing it (e.g. using setTime()) could result
 * in undefined behaviour. Do so at your peril. </p>
 * @return Expiration {@link Date}, or <tt>null</tt>.
 *
 * @see #setExpiryDate(java.util.Date)
 *
 */

@Deprecated
public java.util.Date getExpiryDate() { throw new RuntimeException("Stub!"); }

/**
 * Sets expiration date.
 * <p><strong>Note:</strong> the object returned by this method is considered
 * immutable. Changing it (e.g. using setTime()) could result in undefined
 * behaviour. Do so at your peril.</p>
 *
 * @param expiryDate the {@link Date} after which this cookie is no longer valid.
 *
 * @see #getExpiryDate
 *
 */

@Deprecated
public void setExpiryDate(java.util.Date expiryDate) { throw new RuntimeException("Stub!"); }

/**
 * Returns <tt>false</tt> if the cookie should be discarded at the end
 * of the "session"; <tt>true</tt> otherwise.
 *
 * @return <tt>false</tt> if the cookie should be discarded at the end
 *         of the "session"; <tt>true</tt> otherwise
 */

@Deprecated
public boolean isPersistent() { throw new RuntimeException("Stub!"); }

/**
 * Returns domain attribute of the cookie.
 *
 * @return the value of the domain attribute
 *
 * @see #setDomain(java.lang.String)
 */

@Deprecated
public java.lang.String getDomain() { throw new RuntimeException("Stub!"); }

/**
 * Sets the domain attribute.
 *
 * @param domain The value of the domain attribute
 *
 * @see #getDomain
 */

@Deprecated
public void setDomain(java.lang.String domain) { throw new RuntimeException("Stub!"); }

/**
 * Returns the path attribute of the cookie
 *
 * @return The value of the path attribute.
 *
 * @see #setPath(java.lang.String)
 */

@Deprecated
public java.lang.String getPath() { throw new RuntimeException("Stub!"); }

/**
 * Sets the path attribute.
 *
 * @param path The value of the path attribute
 *
 * @see #getPath
 *
 */

@Deprecated
public void setPath(java.lang.String path) { throw new RuntimeException("Stub!"); }

/**
 * @return <code>true</code> if this cookie should only be sent over secure connections.
 * @see #setSecure(boolean)
 */

@Deprecated
public boolean isSecure() { throw new RuntimeException("Stub!"); }

/**
 * Sets the secure attribute of the cookie.
 * <p>
 * When <tt>true</tt> the cookie should only be sent
 * using a secure protocol (https).  This should only be set when
 * the cookie's originating server used a secure protocol to set the
 * cookie's value.
 *
 * @param secure The value of the secure attribute
 *
 * @see #isSecure()
 */

@Deprecated
public void setSecure(boolean secure) { throw new RuntimeException("Stub!"); }

/**
 * Returns null. Cookies prior to RFC2965 do not set this attribute
 */

@Deprecated
public int[] getPorts() { throw new RuntimeException("Stub!"); }

/**
 * Returns the version of the cookie specification to which this
 * cookie conforms.
 *
 * @return the version of the cookie.
 *
 * @see #setVersion(int)
 *
 */

@Deprecated
public int getVersion() { throw new RuntimeException("Stub!"); }

/**
 * Sets the version of the cookie specification to which this
 * cookie conforms.
 *
 * @param version the version of the cookie.
 *
 * @see #getVersion
 */

@Deprecated
public void setVersion(int version) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if this cookie has expired.
 * @param date Current time
 *
 * @return <tt>true</tt> if the cookie has expired.
 */

@Deprecated
public boolean isExpired(java.util.Date date) { throw new RuntimeException("Stub!"); }

@Deprecated
public void setAttribute(java.lang.String name, java.lang.String value) { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String getAttribute(java.lang.String name) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean containsAttribute(java.lang.String name) { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object clone() throws java.lang.CloneNotSupportedException { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

