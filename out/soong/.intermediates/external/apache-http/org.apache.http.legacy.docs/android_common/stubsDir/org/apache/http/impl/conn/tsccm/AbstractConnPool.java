/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/tsccm/AbstractConnPool.java $
 * $Revision: 673450 $
 * $Date: 2008-07-02 10:35:05 -0700 (Wed, 02 Jul 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn.tsccm;

import org.apache.http.conn.ConnectionPoolTimeoutException;

/**
 * An abstract connection pool.
 * It is used by the {@link ThreadSafeClientConnManager}.
 * The abstract pool includes a {@link #poolLock}, which is used to
 * synchronize access to the internal pool datastructures.
 * Don't use <code>synchronized</code> for that purpose!
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract class AbstractConnPool implements org.apache.http.impl.conn.tsccm.RefQueueHandler {

/**
 * Creates a new connection pool.
 */

@Deprecated
protected AbstractConnPool() { throw new RuntimeException("Stub!"); }

/**
 * Enables connection garbage collection (GC).
 * This method must be called immediately after creating the
 * connection pool. It is not possible to enable connection GC
 * after pool entries have been created. Neither is it possible
 * to disable connection GC.
 *
 * @throws IllegalStateException
 *         if connection GC is already enabled, or if it cannot be
 *         enabled because there already are pool entries
 */

@Deprecated
public void enableConnectionGC() throws java.lang.IllegalStateException { throw new RuntimeException("Stub!"); }

/**
 * Obtains a pool entry with a connection within the given timeout.
 *
 * @param route     the route for which to get the connection
 * @param timeout   the timeout, 0 or negative for no timeout
 * @param tunit     the unit for the <code>timeout</code>,
 *                  may be <code>null</code> only if there is no timeout
 *
 * @return  pool entry holding a connection for the route
 *
 * @throws ConnectionPoolTimeoutException
 *         if the timeout expired
 * @throws InterruptedException
 *         if the calling thread was interrupted
 */

@Deprecated
public final org.apache.http.impl.conn.tsccm.BasicPoolEntry getEntry(org.apache.http.conn.routing.HttpRoute route, java.lang.Object state, long timeout, java.util.concurrent.TimeUnit tunit) throws org.apache.http.conn.ConnectionPoolTimeoutException, java.lang.InterruptedException { throw new RuntimeException("Stub!"); }

/**
 * Returns a new {@link PoolEntryRequest}, from which a {@link BasicPoolEntry}
 * can be obtained, or the request can be aborted.
 */

@Deprecated
public abstract org.apache.http.impl.conn.tsccm.PoolEntryRequest requestPoolEntry(org.apache.http.conn.routing.HttpRoute route, java.lang.Object state);

/**
 * Returns an entry into the pool.
 * The connection of the entry is expected to be in a suitable state,
 * either open and re-usable, or closed. The pool will not make any
 * attempt to determine whether it can be re-used or not.
 *
 * @param entry     the entry for the connection to release
 * @param reusable  <code>true</code> if the entry is deemed
 *                  reusable, <code>false</code> otherwise.
 * @param validDuration The duration that the entry should remain free and reusable.
 * @param timeUnit The unit of time the duration is measured in.
 */

@Deprecated
public abstract void freeEntry(org.apache.http.impl.conn.tsccm.BasicPoolEntry entry, boolean reusable, long validDuration, java.util.concurrent.TimeUnit timeUnit);

@Deprecated
public void handleReference(java.lang.ref.Reference ref) { throw new RuntimeException("Stub!"); }

/**
 * Handles cleaning up for a lost pool entry with the given route.
 * A lost pool entry corresponds to a connection that was
 * garbage collected instead of being properly released.
 *
 * @param route     the route of the pool entry that was lost
 */

@Deprecated
protected abstract void handleLostEntry(org.apache.http.conn.routing.HttpRoute route);

/**
 * Closes idle connections.
 *
 * @param idletime  the time the connections should have been idle
 *                  in order to be closed now
 * @param tunit     the unit for the <code>idletime</code>
 */

@Deprecated
public void closeIdleConnections(long idletime, java.util.concurrent.TimeUnit tunit) { throw new RuntimeException("Stub!"); }

@Deprecated
public void closeExpiredConnections() { throw new RuntimeException("Stub!"); }

/**
 * Deletes all entries for closed connections.
 */

@Deprecated
public abstract void deleteClosedConnections();

/**
 * Shuts down this pool and all associated resources.
 * Overriding methods MUST call the implementation here!
 */

@Deprecated
public void shutdown() { throw new RuntimeException("Stub!"); }

/**
 * Closes a connection from this pool.
 *
 * @param conn      the connection to close, or <code>null</code>
 */

@Deprecated
protected void closeConnection(org.apache.http.conn.OperatedClientConnection conn) { throw new RuntimeException("Stub!"); }

/** The handler for idle connections. */

@Deprecated protected org.apache.http.impl.conn.IdleConnectionHandler idleConnHandler;

/** Indicates whether this pool is shut down. */

@Deprecated protected volatile boolean isShutDown;

/**
 * References to issued connections.
 * Objects in this set are of class
 * {@link BasicPoolEntryRef BasicPoolEntryRef},
 * and point to the pool entry for the issued connection.
 * GCed connections are detected by the missing pool entries.
 */

@Deprecated protected java.util.Set<org.apache.http.impl.conn.tsccm.BasicPoolEntryRef> issuedConnections;

/** The current total number of connections. */

@Deprecated protected int numConnections;

/**
 * The global lock for this pool.
 */

@Deprecated protected final java.util.concurrent.locks.Lock poolLock;
{ poolLock = null; }

/**
 * A reference queue to track loss of pool entries to GC.
 * The same queue is used to track loss of the connection manager,
 * so we cannot specialize the type.
 */

@Deprecated protected java.lang.ref.ReferenceQueue<java.lang.Object> refQueue;
}

