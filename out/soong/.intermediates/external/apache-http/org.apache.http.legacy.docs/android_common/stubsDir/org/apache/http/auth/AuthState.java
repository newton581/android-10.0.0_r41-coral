/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/auth/AuthState.java $
 * $Revision: 659971 $
 * $Date: 2008-05-25 05:01:22 -0700 (Sun, 25 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.auth;


/**
 * This class provides detailed information about the state of the
 * authentication process.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class AuthState {

/**
 * Default constructor.
 *
 */

@Deprecated
public AuthState() { throw new RuntimeException("Stub!"); }

/**
 * Invalidates the authentication state by resetting its parameters.
 */

@Deprecated
public void invalidate() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isValid() { throw new RuntimeException("Stub!"); }

/**
 * Assigns the given {@link AuthScheme authentication scheme}.
 *
 * @param authScheme the {@link AuthScheme authentication scheme}
 */

@Deprecated
public void setAuthScheme(org.apache.http.auth.AuthScheme authScheme) { throw new RuntimeException("Stub!"); }

/**
 * Returns the {@link AuthScheme authentication scheme}.
 *
 * @return {@link AuthScheme authentication scheme}
 */

@Deprecated
public org.apache.http.auth.AuthScheme getAuthScheme() { throw new RuntimeException("Stub!"); }

/** 
 * Returns user {@link Credentials} selected for authentication if available
 *
 * @return user credentials if available, <code>null</code otherwise
 */

@Deprecated
public org.apache.http.auth.Credentials getCredentials() { throw new RuntimeException("Stub!"); }

/** 
 * Sets user {@link Credentials} to be used for authentication
 *
 * @param credentials User credentials
 */

@Deprecated
public void setCredentials(org.apache.http.auth.Credentials credentials) { throw new RuntimeException("Stub!"); }

/** 
 * Returns actual {@link AuthScope} if available
 *
 * @return actual authentication scope if available, <code>null</code otherwise
 */

@Deprecated
public org.apache.http.auth.AuthScope getAuthScope() { throw new RuntimeException("Stub!"); }

/** 
 * Sets actual {@link AuthScope}.
 *
 * @param authScope Authentication scope
 */

@Deprecated
public void setAuthScope(org.apache.http.auth.AuthScope authScope) { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

