/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/conn/BasicManagedEntity.java $
 * $Revision $
 * $Date: 2008-06-27 12:49:20 -0700 (Fri, 27 Jun 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.conn;

import java.io.IOException;

/**
 * An entity that releases a {@link ManagedClientConnection connection}.
 * A {@link ManagedClientConnection} will
 * typically <i>not</i> return a managed entity, but you can replace
 * the unmanaged entity in the response with a managed one.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version $Revision: 672367 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicManagedEntity extends org.apache.http.entity.HttpEntityWrapper implements org.apache.http.conn.ConnectionReleaseTrigger, org.apache.http.conn.EofSensorWatcher {

/**
 * Creates a new managed entity that can release a connection.
 *
 * @param entity    the entity of which to wrap the content.
 *                  Note that the argument entity can no longer be used
 *                  afterwards, since the content will be taken by this
 *                  managed entity.
 * @param conn      the connection to release
 * @param reuse     whether the connection should be re-used
 */

@Deprecated
public BasicManagedEntity(org.apache.http.HttpEntity entity, org.apache.http.conn.ManagedClientConnection conn, boolean reuse) { super(null); throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isRepeatable() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.io.InputStream getContent() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void consumeContent() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void writeTo(java.io.OutputStream outstream) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void releaseConnection() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void abortConnection() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean eofDetected(java.io.InputStream wrapped) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean streamClosed(java.io.InputStream wrapped) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean streamAbort(java.io.InputStream wrapped) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Releases the connection gracefully.
 * The connection attribute will be nullified.
 * Subsequent invocations are no-ops.
 *
 * @throws IOException      in case of an IO problem.
 *         The connection attribute will be nullified anyway.
 */

@Deprecated
protected void releaseManagedConnection() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/** Whether to keep the connection alive. */

@Deprecated protected final boolean attemptReuse;
{ attemptReuse = false; }

/** The connection to release. */

@Deprecated protected org.apache.http.conn.ManagedClientConnection managedConn;
}

