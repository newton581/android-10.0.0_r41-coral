/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.http;

import java.io.IOException;
import org.apache.http.params.HttpParams;
import org.apache.http.HttpException;
import org.apache.http.HttpClientConnection;
import org.apache.http.StatusLine;
import org.apache.http.HttpResponse;
import org.apache.http.HttpConnectionMetrics;

/**
 * A alternate class for (@link DefaultHttpClientConnection).
 * It has better performance than DefaultHttpClientConnection
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AndroidHttpClientConnection implements org.apache.http.HttpInetConnection, org.apache.http.HttpConnection {

public AndroidHttpClientConnection() { throw new RuntimeException("Stub!"); }

/**
 * Bind socket and set HttpParams to AndroidHttpClientConnection
 * @param socket outgoing socket
 * @param params HttpParams
 * @throws IOException
 */

public void bind(java.net.Socket socket, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public boolean isOpen() { throw new RuntimeException("Stub!"); }

public java.net.InetAddress getLocalAddress() { throw new RuntimeException("Stub!"); }

public int getLocalPort() { throw new RuntimeException("Stub!"); }

public java.net.InetAddress getRemoteAddress() { throw new RuntimeException("Stub!"); }

public int getRemotePort() { throw new RuntimeException("Stub!"); }

public void setSocketTimeout(int timeout) { throw new RuntimeException("Stub!"); }

public int getSocketTimeout() { throw new RuntimeException("Stub!"); }

public void shutdown() throws java.io.IOException { throw new RuntimeException("Stub!"); }

public void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Sends the request line and all headers over the connection.
 * @param request the request whose headers to send.
 * @throws HttpException
 * @throws IOException
 */

public void sendRequestHeader(org.apache.http.HttpRequest request) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Sends the request entity over the connection.
 * @param request the request whose entity to send.
 * @throws HttpException
 * @throws IOException
 */

public void sendRequestEntity(org.apache.http.HttpEntityEnclosingRequest request) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

protected void doFlush() throws java.io.IOException { throw new RuntimeException("Stub!"); }

public void flush() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Parses the response headers and adds them to the
 * given {@code headers} object, and returns the response StatusLine
 * @param headers store parsed header to headers.
 * @throws IOException
 * @return StatusLine
 * @see HttpClientConnection#receiveResponseHeader()
 */

public org.apache.http.StatusLine parseResponseHeader(android.net.http.Headers headers) throws java.io.IOException, org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

/**
 * Return the next response entity.
 * @param headers contains values for parsing entity
 * @see HttpClientConnection#receiveResponseEntity(HttpResponse response)
 */

public org.apache.http.HttpEntity receiveResponseEntity(android.net.http.Headers headers) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether this connection has gone down.
 * Network connections may get closed during some time of inactivity
 * for several reasons. The next time a read is attempted on such a
 * connection it will throw an IOException.
 * This method tries to alleviate this inconvenience by trying to
 * find out if a connection is still usable. Implementations may do
 * that by attempting a read with a very small timeout. Thus this
 * method may block for a small amount of time before returning a result.
 * It is therefore an <i>expensive</i> operation.
 *
 * @return  <code>true</code> if attempts to use this connection are
 *          likely to succeed, or <code>false</code> if they are likely
 *          to fail and this connection should be closed
 */

public boolean isStale() { throw new RuntimeException("Stub!"); }

/**
 * Returns a collection of connection metrcis
 * @return HttpConnectionMetrics
 */

public org.apache.http.HttpConnectionMetrics getMetrics() { throw new RuntimeException("Stub!"); }
}

