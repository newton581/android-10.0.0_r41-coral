/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/client/utils/URIUtils.java $
 * $Revision: 653041 $
 * $Date: 2008-05-03 03:39:28 -0700 (Sat, 03 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

package org.apache.http.client.utils;

import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.HttpHost;

/**
 * A collection of utilities for {@link URI URIs}, to workaround
 * bugs within the class or for ease-of-use features.
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class URIUtils {

/**
 * This class should not be instantiated.
 */

URIUtils() { throw new RuntimeException("Stub!"); }

/**
 * Constructs a {@link URI} using all the parameters. This should be
 * used instead of
 * {@link URI#URI(String, String, String, int, String, String, String)}
 * or any of the other URI multi-argument URI constructors.
 *
 * See <a
 * href="https://issues.apache.org/jira/browse/HTTPCLIENT-730">HTTPCLIENT-730</a>
 * for more information.
 *
 * @param scheme
 *            Scheme name
 * @param host
 *            Host name
 * @param port
 *            Port number
 * @param path
 *            Path
 * @param query
 *            Query
 * @param fragment
 *            Fragment
 *
 * @throws URISyntaxException
 *             If both a scheme and a path are given but the path is
 *             relative, if the URI string constructed from the given
 *             components violates RFC&nbsp;2396, or if the authority
 *             component of the string is present but cannot be parsed
 *             as a server-based authority
 */

@Deprecated
public static java.net.URI createURI(java.lang.String scheme, java.lang.String host, int port, java.lang.String path, java.lang.String query, java.lang.String fragment) throws java.net.URISyntaxException { throw new RuntimeException("Stub!"); }

/**
 * A convenience method for creating a new {@link URI} whose scheme, host
 * and port are taken from the target host, but whose path, query and
 * fragment are taken from the existing URI. The fragment is only used if
 * dropFragment is false.
 *
 * @param uri
 *            Contains the path, query and fragment to use.
 * @param target
 *            Contains the scheme, host and port to use.
 * @param dropFragment
 *            True if the fragment should not be copied.
 *
 * @throws URISyntaxException
 *             If the resulting URI is invalid.
 */

@Deprecated
public static java.net.URI rewriteURI(java.net.URI uri, org.apache.http.HttpHost target, boolean dropFragment) throws java.net.URISyntaxException { throw new RuntimeException("Stub!"); }

/**
 * A convenience method for
 * {@link URIUtils#rewriteURI(URI, HttpHost, boolean)} that always keeps the
 * fragment.
 */

@Deprecated
public static java.net.URI rewriteURI(java.net.URI uri, org.apache.http.HttpHost target) throws java.net.URISyntaxException { throw new RuntimeException("Stub!"); }

/**
 * Resolves a URI reference against a base URI. Work-around for bug in
 * java.net.URI (<http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4708535>)
 *
 * @param baseURI the base URI
 * @param reference the URI reference
 * @return the resulting URI
 */

@Deprecated
public static java.net.URI resolve(java.net.URI baseURI, java.lang.String reference) { throw new RuntimeException("Stub!"); }

/**
 * Resolves a URI reference against a base URI. Work-around for bug in
 * java.net.URI (<http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4708535>)
 *
 * @param baseURI the base URI
 * @param reference the URI reference
 * @return the resulting URI
 */

@Deprecated
public static java.net.URI resolve(java.net.URI baseURI, java.net.URI reference) { throw new RuntimeException("Stub!"); }
}

