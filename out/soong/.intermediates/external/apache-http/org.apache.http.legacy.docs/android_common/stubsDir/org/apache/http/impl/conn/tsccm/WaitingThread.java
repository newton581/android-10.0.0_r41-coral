/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/tsccm/WaitingThread.java $
 * $Revision: 649217 $
 * $Date: 2008-04-17 11:32:32 -0700 (Thu, 17 Apr 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn.tsccm;


/**
 * Represents a thread waiting for a connection.
 * This class implements throwaway objects. It is instantiated whenever
 * a thread needs to wait. Instances are not re-used, except if the
 * waiting thread experiences a spurious wakeup and continues to wait.
 * <br/>
 * All methods assume external synchronization on the condition
 * passed to the constructor.
 * Instances of this class do <i>not</i> synchronize access!
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class WaitingThread {

/**
 * Creates a new entry for a waiting thread.
 *
 * @param cond      the condition for which to wait
 * @param pool      the pool on which the thread will be waiting,
 *                  or <code>null</code>
 */

@Deprecated
public WaitingThread(java.util.concurrent.locks.Condition cond, org.apache.http.impl.conn.tsccm.RouteSpecificPool pool) { throw new RuntimeException("Stub!"); }

/**
 * Obtains the condition.
 *
 * @return  the condition on which to wait, never <code>null</code>
 */

@Deprecated
public final java.util.concurrent.locks.Condition getCondition() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the pool, if there is one.
 *
 * @return  the pool on which a thread is or was waiting,
 *          or <code>null</code>
 */

@Deprecated
public final org.apache.http.impl.conn.tsccm.RouteSpecificPool getPool() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the thread, if there is one.
 *
 * @return  the thread which is waiting, or <code>null</code>
 */

@Deprecated
public final java.lang.Thread getThread() { throw new RuntimeException("Stub!"); }

/**
 * Blocks the calling thread.
 * This method returns when the thread is notified or interrupted,
 * if a timeout occurrs, or if there is a spurious wakeup.
 * <br/>
 * This method assumes external synchronization.
 *
 * @param deadline  when to time out, or <code>null</code> for no timeout
 *
 * @return  <code>true</code> if the condition was satisfied,
 *          <code>false</code> in case of a timeout.
 *          Typically, a call to {@link #wakeup} is used to indicate
 *          that the condition was satisfied. Since the condition is
 *          accessible outside, this cannot be guaranteed though.
 *
 * @throws InterruptedException     if the waiting thread was interrupted
 *
 * @see #wakeup
 */

@Deprecated
public boolean await(java.util.Date deadline) throws java.lang.InterruptedException { throw new RuntimeException("Stub!"); }

/**
 * Wakes up the waiting thread.
 * <br/>
 * This method assumes external synchronization.
 */

@Deprecated
public void wakeup() { throw new RuntimeException("Stub!"); }

@Deprecated
public void interrupt() { throw new RuntimeException("Stub!"); }
}

