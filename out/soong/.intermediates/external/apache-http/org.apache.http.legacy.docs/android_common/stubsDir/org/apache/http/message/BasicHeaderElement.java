/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/message/BasicHeaderElement.java $
 * $Revision: 604625 $
 * $Date: 2007-12-16 06:11:11 -0800 (Sun, 16 Dec 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.message;

import org.apache.http.NameValuePair;

/**
 * One element of an HTTP header's value.
 * <p>
 * Some HTTP headers (such as the set-cookie header) have values that
 * can be decomposed into multiple elements.  Such headers must be in the
 * following form:
 * </p>
 * <pre>
 * header  = [ element ] *( "," [ element ] )
 * element = name [ "=" [ value ] ] *( ";" [ param ] )
 * param   = name [ "=" [ value ] ]
 *
 * name    = token
 * value   = ( token | quoted-string )
 *
 * token         = 1*&lt;any char except "=", ",", ";", &lt;"&gt; and
 *                       white space&gt;
 * quoted-string = &lt;"&gt; *( text | quoted-char ) &lt;"&gt;
 * text          = any char except &lt;"&gt;
 * quoted-char   = "\" char
 * </pre>
 * <p>
 * Any amount of white space is allowed between any part of the
 * header, element or param and is ignored. A missing value in any
 * element or param will be stored as the empty {@link String};
 * if the "=" is also missing <var>null</var> will be stored instead.
 * </p>
 * <p>
 * This class represents an individual header element, containing
 * both a name/value pair (value may be <tt>null</tt>) and optionally
 * a set of additional parameters.
 * </p>
 *
 * @author <a href="mailto:bcholmes@interlog.com">B.C. Holmes</a>
 * @author <a href="mailto:jericho@thinkfree.com">Park, Sung-Gu</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:oleg at ural.com">Oleg Kalnichevski</a>
 *
 *
 * <!-- empty lines above to avoid 'svn diff' context problems -->
 * @version $Revision: 604625 $ $Date: 2007-12-16 06:11:11 -0800 (Sun, 16 Dec 2007) $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicHeaderElement implements org.apache.http.HeaderElement, java.lang.Cloneable {

/**
 * Constructor with name, value and parameters.
 *
 * @param name header element name
 * @param value header element value. May be <tt>null</tt>
 * @param parameters header element parameters. May be <tt>null</tt>.
 *   Parameters are copied by reference, not by value
 */

@Deprecated
public BasicHeaderElement(java.lang.String name, java.lang.String value, org.apache.http.NameValuePair[] parameters) { throw new RuntimeException("Stub!"); }

/**
 * Constructor with name and value.
 *
 * @param name header element name
 * @param value header element value. May be <tt>null</tt>
 */

@Deprecated
public BasicHeaderElement(java.lang.String name, java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Returns the name.
 *
 * @return String name The name
 */

@Deprecated
public java.lang.String getName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the value.
 *
 * @return String value The current value.
 */

@Deprecated
public java.lang.String getValue() { throw new RuntimeException("Stub!"); }

/**
 * Get parameters, if any.
 * The returned array is created for each invocation and can
 * be modified by the caller without affecting this header element.
 *
 * @return parameters as an array of {@link NameValuePair}s
 */

@Deprecated
public org.apache.http.NameValuePair[] getParameters() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the number of parameters.
 *
 * @return  the number of parameters
 */

@Deprecated
public int getParameterCount() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the parameter with the given index.
 *
 * @param index     the index of the parameter, 0-based
 *
 * @return  the parameter with the given index
 */

@Deprecated
public org.apache.http.NameValuePair getParameter(int index) { throw new RuntimeException("Stub!"); }

/**
 * Returns parameter with the given name, if found. Otherwise null
 * is returned
 *
 * @param name The name to search by.
 * @return NameValuePair parameter with the given name
 */

@Deprecated
public org.apache.http.NameValuePair getParameterByName(java.lang.String name) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean equals(java.lang.Object object) { throw new RuntimeException("Stub!"); }

@Deprecated
public int hashCode() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object clone() throws java.lang.CloneNotSupportedException { throw new RuntimeException("Stub!"); }
}

