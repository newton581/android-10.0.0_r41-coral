/*
 * $Header: /home/jerenkrantz/tmp/commons/commons-convert/cvs/home/cvs/jakarta-commons//httpclient/src/java/org/apache/commons/httpclient/methods/multipart/FilePart.java,v 1.19 2004/04/18 23:51:37 jsdever Exp $
 * $Revision: 480424 $
 * $Date: 2006-11-29 06:56:49 +0100 (Wed, 29 Nov 2006) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package com.android.internal.http.multipart;

import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.IOException;
import org.apache.commons.logging.Log;

/**
 * This class implements a part of a Multipart post object that
 * consists of a file.
 *
 * @author <a href="mailto:mattalbright@yahoo.com">Matthew Albright</a>
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 * @author <a href="mailto:adrian@ephox.com">Adrian Sutton</a>
 * @author <a href="mailto:becke@u.washington.edu">Michael Becke</a>
 * @author <a href="mailto:mdiggory@latte.harvard.edu">Mark Diggory</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:oleg@ural.ru">Oleg Kalnichevski</a>
 *
 * @since 2.0
 *
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class FilePart extends com.android.internal.http.multipart.PartBase {

/**
 * FilePart Constructor.
 *
 * @param name the name for this part
 * @param partSource the source for this part
 * @param contentType the content type for this part, if <code>null</code> the
 * {@link #DEFAULT_CONTENT_TYPE default} is used
 * @param charset the charset encoding for this part, if <code>null</code> the
 * {@link #DEFAULT_CHARSET default} is used
 */

public FilePart(java.lang.String name, com.android.internal.http.multipart.PartSource partSource, java.lang.String contentType, java.lang.String charset) { super(null, null, null, null); throw new RuntimeException("Stub!"); }

/**
 * FilePart Constructor.
 *
 * @param name the name for this part
 * @param partSource the source for this part
 */

public FilePart(java.lang.String name, com.android.internal.http.multipart.PartSource partSource) { super(null, null, null, null); throw new RuntimeException("Stub!"); }

/**
 * FilePart Constructor.
 *
 * @param name the name of the file part
 * @param file the file to post
 *
 * @throws FileNotFoundException if the <i>file</i> is not a normal
 * file or if it is not readable.
 */

public FilePart(java.lang.String name, java.io.File file) throws java.io.FileNotFoundException { super(null, null, null, null); throw new RuntimeException("Stub!"); }

/**
 * FilePart Constructor.
 *
 * @param name the name of the file part
 * @param file the file to post
 * @param contentType the content type for this part, if <code>null</code> the
 * {@link #DEFAULT_CONTENT_TYPE default} is used
 * @param charset the charset encoding for this part, if <code>null</code> the
 * {@link #DEFAULT_CHARSET default} is used
 *
 * @throws FileNotFoundException if the <i>file</i> is not a normal
 * file or if it is not readable.
 */

public FilePart(java.lang.String name, java.io.File file, java.lang.String contentType, java.lang.String charset) throws java.io.FileNotFoundException { super(null, null, null, null); throw new RuntimeException("Stub!"); }

/**
 * FilePart Constructor.
 *
 * @param name the name of the file part
 * @param fileName the file name
 * @param file the file to post
 *
 * @throws FileNotFoundException if the <i>file</i> is not a normal
 * file or if it is not readable.
 */

public FilePart(java.lang.String name, java.lang.String fileName, java.io.File file) throws java.io.FileNotFoundException { super(null, null, null, null); throw new RuntimeException("Stub!"); }

/**
 * FilePart Constructor.
 *
 * @param name the name of the file part
 * @param fileName the file name
 * @param file the file to post
 * @param contentType the content type for this part, if <code>null</code> the
 * {@link #DEFAULT_CONTENT_TYPE default} is used
 * @param charset the charset encoding for this part, if <code>null</code> the
 * {@link #DEFAULT_CHARSET default} is used
 *
 * @throws FileNotFoundException if the <i>file</i> is not a normal
 * file or if it is not readable.
 */

public FilePart(java.lang.String name, java.lang.String fileName, java.io.File file, java.lang.String contentType, java.lang.String charset) throws java.io.FileNotFoundException { super(null, null, null, null); throw new RuntimeException("Stub!"); }

/**
 * Write the disposition header to the output stream
 * @param out The output stream
 * @throws IOException If an IO problem occurs
 * @see Part#sendDispositionHeader(OutputStream)
 */

protected void sendDispositionHeader(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Write the data in "source" to the specified stream.
 * @param out The output stream.
 * @throws IOException if an IO problem occurs.
 * @see Part#sendData(OutputStream)
 */

protected void sendData(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/** 
 * Returns the source of the file part.
 *
 * @return The source.
 */

protected com.android.internal.http.multipart.PartSource getSource() { throw new RuntimeException("Stub!"); }

/**
 * Return the length of the data.
 * @return The length.
 * @see Part#lengthOfData()
 */

protected long lengthOfData() { throw new RuntimeException("Stub!"); }

/** Default charset of file attachments. */

public static final java.lang.String DEFAULT_CHARSET = "ISO-8859-1";

/** Default content encoding of file attachments. */

public static final java.lang.String DEFAULT_CONTENT_TYPE = "application/octet-stream";

/** Default transfer encoding of file attachments. */

public static final java.lang.String DEFAULT_TRANSFER_ENCODING = "binary";

/** Attachment's file name */

protected static final java.lang.String FILE_NAME = "; filename=";
}

