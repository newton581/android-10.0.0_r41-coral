/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/cookie/DateUtils.java $
 * $Revision: 677240 $
 * $Date: 2008-07-16 04:25:47 -0700 (Wed, 16 Jul 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.cookie;

import java.util.Date;

/**
 * A utility class for parsing and formatting HTTP dates as used in cookies and
 * other headers.  This class handles dates as defined by RFC 2616 section
 * 3.3.1 as well as some other common non-standard formats.
 *
 * @author Christopher Brown
 * @author Michael Becke
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class DateUtils {

/** This class should not be instantiated. */

DateUtils() { throw new RuntimeException("Stub!"); }

/**
 * Parses a date value.  The formats used for parsing the date value are retrieved from
 * the default http params.
 *
 * @param dateValue the date value to parse
 *
 * @return the parsed date
 *
 * @throws DateParseException if the value could not be parsed using any of the
 * supported date formats
 */

@Deprecated
public static java.util.Date parseDate(java.lang.String dateValue) throws org.apache.http.impl.cookie.DateParseException { throw new RuntimeException("Stub!"); }

/**
 * Parses the date value using the given date formats.
 *
 * @param dateValue the date value to parse
 * @param dateFormats the date formats to use
 *
 * @return the parsed date
 *
 * @throws DateParseException if none of the dataFormats could parse the dateValue
 */

@Deprecated
public static java.util.Date parseDate(java.lang.String dateValue, java.lang.String[] dateFormats) throws org.apache.http.impl.cookie.DateParseException { throw new RuntimeException("Stub!"); }

/**
 * Parses the date value using the given date formats.
 *
 * @param dateValue the date value to parse
 * @param dateFormats the date formats to use
 * @param startDate During parsing, two digit years will be placed in the range
 * <code>startDate</code> to <code>startDate + 100 years</code>. This value may
 * be <code>null</code>. When <code>null</code> is given as a parameter, year
 * <code>2000</code> will be used.
 *
 * @return the parsed date
 *
 * @throws DateParseException if none of the dataFormats could parse the dateValue
 */

@Deprecated
public static java.util.Date parseDate(java.lang.String dateValue, java.lang.String[] dateFormats, java.util.Date startDate) throws org.apache.http.impl.cookie.DateParseException { throw new RuntimeException("Stub!"); }

/**
 * Formats the given date according to the RFC 1123 pattern.
 *
 * @param date The date to format.
 * @return An RFC 1123 formatted date string.
 *
 * @see #PATTERN_RFC1123
 */

@Deprecated
public static java.lang.String formatDate(java.util.Date date) { throw new RuntimeException("Stub!"); }

/**
 * Formats the given date according to the specified pattern.  The pattern
 * must conform to that used by the {@link SimpleDateFormat simple date
 * format} class.
 *
 * @param date The date to format.
 * @param pattern The pattern to use for formatting the date.
 * @return A formatted date string.
 *
 * @throws IllegalArgumentException If the given date pattern is invalid.
 *
 * @see SimpleDateFormat
 */

@Deprecated
public static java.lang.String formatDate(java.util.Date date, java.lang.String pattern) { throw new RuntimeException("Stub!"); }

@Deprecated public static final java.util.TimeZone GMT;
static { GMT = null; }

/**
 * Date format pattern used to parse HTTP date headers in ANSI C
 * <code>asctime()</code> format.
 */

@Deprecated public static final java.lang.String PATTERN_ASCTIME = "EEE MMM d HH:mm:ss yyyy";

/**
 * Date format pattern used to parse HTTP date headers in RFC 1036 format.
 */

@Deprecated public static final java.lang.String PATTERN_RFC1036 = "EEEE, dd-MMM-yy HH:mm:ss zzz";

/**
 * Date format pattern used to parse HTTP date headers in RFC 1123 format.
 */

@Deprecated public static final java.lang.String PATTERN_RFC1123 = "EEE, dd MMM yyyy HH:mm:ss zzz";
}

