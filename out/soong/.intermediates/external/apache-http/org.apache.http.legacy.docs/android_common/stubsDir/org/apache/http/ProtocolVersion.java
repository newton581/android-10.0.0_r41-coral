/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/ProtocolVersion.java $
 * $Revision: 609106 $
 * $Date: 2008-01-05 01:15:42 -0800 (Sat, 05 Jan 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http;


/**
 * Represents a protocol version, as specified in RFC 2616.
 * RFC 2616 specifies only HTTP versions, like "HTTP/1.1" and "HTTP/1.0".
 * RFC 3261 specifies a message format that is identical to HTTP except
 * for the protocol name. It defines a protocol version "SIP/2.0".
 * There are some nitty-gritty differences between the interpretation
 * of versions in HTTP and SIP. In those cases, HTTP takes precedence.
 * <p>
 * This class defines a protocol version as a combination of
 * protocol name, major version number, and minor version number.
 * Note that {@link #equals} and {@link #hashCode} are defined as
 * final here, they cannot be overridden in derived classes.
 *
 * @author <a href="mailto:oleg@ural.ru">Oleg Kalnichevski</a>
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 * @version $Revision: 609106 $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class ProtocolVersion implements java.io.Serializable, java.lang.Cloneable {

/**
 * Create a protocol version designator.
 *
 * @param protocol   the name of the protocol, for example "HTTP"
 * @param major      the major version number of the protocol
 * @param minor      the minor version number of the protocol
 */

@Deprecated
public ProtocolVersion(java.lang.String protocol, int major, int minor) { throw new RuntimeException("Stub!"); }

/**
 * Returns the name of the protocol.
 *
 * @return the protocol name
 */

@Deprecated
public final java.lang.String getProtocol() { throw new RuntimeException("Stub!"); }

/**
 * Returns the major version number of the protocol.
 *
 * @return the major version number.
 */

@Deprecated
public final int getMajor() { throw new RuntimeException("Stub!"); }

/**
 * Returns the minor version number of the HTTP protocol.
 *
 * @return the minor version number.
 */

@Deprecated
public final int getMinor() { throw new RuntimeException("Stub!"); }

/**
 * Obtains a specific version of this protocol.
 * This can be used by derived classes to instantiate themselves instead
 * of the base class, and to define constants for commonly used versions.
 * <br/>
 * The default implementation in this class returns <code>this</code>
 * if the version matches, and creates a new {@link ProtocolVersion}
 * otherwise.
 *
 * @param major     the major version
 * @param minor     the minor version
 *
 * @return  a protocol version with the same protocol name
 *          and the argument version
 */

@Deprecated
public org.apache.http.ProtocolVersion forVersion(int major, int minor) { throw new RuntimeException("Stub!"); }

/**
 * Obtains a hash code consistent with {@link #equals}.
 *
 * @return  the hashcode of this protocol version
 */

@Deprecated
public final int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * Checks equality of this protocol version with an object.
 * The object is equal if it is a protocl version with the same
 * protocol name, major version number, and minor version number.
 * The specific class of the object is <i>not</i> relevant,
 * instances of derived classes with identical attributes are
 * equal to instances of the base class and vice versa.
 *
 * @param obj       the object to compare with
 *
 * @return  <code>true</code> if the argument is the same protocol version,
 *          <code>false</code> otherwise
 */

@Deprecated
public final boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether this protocol can be compared to another one.
 * Only protocol versions with the same protocol name can be
 * {@link #compareToVersion compared}.
 *
 * @param that      the protocol version to consider
 *
 * @return  <code>true</code> if {@link #compareToVersion compareToVersion}
 *          can be called with the argument, <code>false</code> otherwise
 */

@Deprecated
public boolean isComparable(org.apache.http.ProtocolVersion that) { throw new RuntimeException("Stub!"); }

/**
 * Compares this protocol version with another one.
 * Only protocol versions with the same protocol name can be compared.
 * This method does <i>not</i> define a total ordering, as it would be
 * required for {@link java.lang.Comparable}.
 *
 * @param that      the protocl version to compare with
 *
 * @return   a negative integer, zero, or a positive integer
 *           as this version is less than, equal to, or greater than
 *           the argument version.
 *
 * @throws IllegalArgumentException
 *         if the argument has a different protocol name than this object,
 *         or if the argument is <code>null</code>
 */

@Deprecated
public int compareToVersion(org.apache.http.ProtocolVersion that) { throw new RuntimeException("Stub!"); }

/**
 * Tests if this protocol version is greater or equal to the given one.
 *
 * @param version   the version against which to check this version
 *
 * @return  <code>true</code> if this protocol version is
 *          {@link #isComparable comparable} to the argument
 *          and {@link #compareToVersion compares} as greater or equal,
 *          <code>false</code> otherwise
 */

@Deprecated
public final boolean greaterEquals(org.apache.http.ProtocolVersion version) { throw new RuntimeException("Stub!"); }

/**
 * Tests if this protocol version is less or equal to the given one.
 *
 * @param version   the version against which to check this version
 *
 * @return  <code>true</code> if this protocol version is
 *          {@link #isComparable comparable} to the argument
 *          and {@link #compareToVersion compares} as less or equal,
 *          <code>false</code> otherwise
 */

@Deprecated
public final boolean lessEquals(org.apache.http.ProtocolVersion version) { throw new RuntimeException("Stub!"); }

/**
 * Converts this protocol version to a string.
 *
 * @return  a protocol version string, like "HTTP/1.1"
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object clone() throws java.lang.CloneNotSupportedException { throw new RuntimeException("Stub!"); }

/** Major version number of the protocol */

@Deprecated protected final int major;
{ major = 0; }

/** Minor version number of the protocol */

@Deprecated protected final int minor;
{ minor = 0; }

/** Name of the protocol. */

@Deprecated protected final java.lang.String protocol;
{ protocol = null; }
}

