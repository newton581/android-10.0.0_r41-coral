/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.http;

import java.io.IOException;

/**
 * A Connection connecting to a secure http server or tunneling through
 * a http proxy server to a https server.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HttpsConnection {

HttpsConnection() { throw new RuntimeException("Stub!"); }

/**
 * @param sessionDir directory to cache SSL sessions
 */

public static void initializeEngine(java.io.File sessionDir) { throw new RuntimeException("Stub!"); }

/**
 * Prints request queue to log, for debugging.
 * returns request count
 */

public synchronized java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

