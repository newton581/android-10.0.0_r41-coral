/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * High level HTTP Interface
 * Queues requests as necessary
 */


package android.net.http;

import java.io.InputStream;
import org.apache.http.HttpHost;

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class RequestQueue {

/**
 * A RequestQueue class instance maintains a set of queued
 * requests.  It orders them, makes the requests against HTTP
 * servers, and makes callbacks to supplied eventHandlers as data
 * is read.  It supports request prioritization, connection reuse
 * and pipelining.
 *
 * @param context application context
 */

public RequestQueue(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * A RequestQueue class instance maintains a set of queued
 * requests.  It orders them, makes the requests against HTTP
 * servers, and makes callbacks to supplied eventHandlers as data
 * is read.  It supports request prioritization, connection reuse
 * and pipelining.
 *
 * @param context application context
 * @param connectionCount The number of simultaneous connections
 */

public RequestQueue(android.content.Context context, int connectionCount) { throw new RuntimeException("Stub!"); }

/**
 * Enables data state and proxy tracking
 */

public synchronized void enablePlatformNotifications() { throw new RuntimeException("Stub!"); }

/**
 * If platform notifications have been enabled, call this method
 * to disable before destroying RequestQueue
 */

public synchronized void disablePlatformNotifications() { throw new RuntimeException("Stub!"); }

/**
 * used by webkit
 * @return proxy host if set, null otherwise
 */

public org.apache.http.HttpHost getProxyHost() { throw new RuntimeException("Stub!"); }

/**
 * Queues an HTTP request
 * @param url The url to load.
 * @param method "GET" or "POST."
 * @param headers A hashmap of http headers.
 * @param eventHandler The event handler for handling returned
 * data.  Callbacks will be made on the supplied instance.
 * @param bodyProvider InputStream providing HTTP body, null if none
 * @param bodyLength length of body, must be 0 if bodyProvider is null
 */

public android.net.http.RequestHandle queueRequest(java.lang.String url, java.lang.String method, java.util.Map<java.lang.String,java.lang.String> headers, android.net.http.EventHandler eventHandler, java.io.InputStream bodyProvider, int bodyLength) { throw new RuntimeException("Stub!"); }

/**
 * Queues an HTTP request
 * @param url The url to load.
 * @param uri The uri of the url to load.
 * @param method "GET" or "POST."
 * @param headers A hashmap of http headers.
 * @param eventHandler The event handler for handling returned
 * data.  Callbacks will be made on the supplied instance.
 * @param bodyProvider InputStream providing HTTP body, null if none
 * @param bodyLength length of body, must be 0 if bodyProvider is null
 */

public android.net.http.RequestHandle queueRequest(java.lang.String url, android.net.compatibility.WebAddress uri, java.lang.String method, java.util.Map<java.lang.String,java.lang.String> headers, android.net.http.EventHandler eventHandler, java.io.InputStream bodyProvider, int bodyLength) { throw new RuntimeException("Stub!"); }

public android.net.http.RequestHandle queueSynchronousRequest(java.lang.String url, android.net.compatibility.WebAddress uri, java.lang.String method, java.util.Map<java.lang.String,java.lang.String> headers, android.net.http.EventHandler eventHandler, java.io.InputStream bodyProvider, int bodyLength) { throw new RuntimeException("Stub!"); }

public synchronized android.net.http.Request getRequest() { throw new RuntimeException("Stub!"); }

/**
 * @return a request for given host if possible
 */

public synchronized android.net.http.Request getRequest(org.apache.http.HttpHost host) { throw new RuntimeException("Stub!"); }

/**
 * @return true if a request for this host is available
 */

public synchronized boolean haveRequest(org.apache.http.HttpHost host) { throw new RuntimeException("Stub!"); }

/**
 * Put request back on head of queue
 */

public void requeueRequest(android.net.http.Request request) { throw new RuntimeException("Stub!"); }

/**
 * This must be called to cleanly shutdown RequestQueue
 */

public void shutdown() { throw new RuntimeException("Stub!"); }

protected synchronized void queueRequest(android.net.http.Request request, boolean head) { throw new RuntimeException("Stub!"); }

public void startTiming() { throw new RuntimeException("Stub!"); }

public void stopTiming() { throw new RuntimeException("Stub!"); }
}

