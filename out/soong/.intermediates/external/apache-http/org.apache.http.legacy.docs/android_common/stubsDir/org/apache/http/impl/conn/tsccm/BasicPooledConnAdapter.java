/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/tsccm/BasicPooledConnAdapter.java $
 * $Revision: 653214 $
 * $Date: 2008-05-04 07:12:13 -0700 (Sun, 04 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn.tsccm;


/**
 * A connection wrapper and callback handler.
 * All connections given out by the manager are wrappers which
 * can be {@link #detach detach}ed to prevent further use on release.
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicPooledConnAdapter extends org.apache.http.impl.conn.AbstractPooledConnAdapter {

/**
 * Creates a new adapter.
 *
 * @param tsccm   the connection manager
 * @param entry   the pool entry for the connection being wrapped
 */

@Deprecated
protected BasicPooledConnAdapter(org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager tsccm, org.apache.http.impl.conn.AbstractPoolEntry entry) { super(null, null); throw new RuntimeException("Stub!"); }

@Deprecated
protected org.apache.http.conn.ClientConnectionManager getManager() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the pool entry.
 *
 * @return  the pool entry, or <code>null</code> if detached
 */

@Deprecated
protected org.apache.http.impl.conn.AbstractPoolEntry getPoolEntry() { throw new RuntimeException("Stub!"); }

@Deprecated
protected void detach() { throw new RuntimeException("Stub!"); }
}

