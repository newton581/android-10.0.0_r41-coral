/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/impl/EnglishReasonPhraseCatalog.java $
 * $Revision: 505744 $
 * $Date: 2007-02-10 10:58:45 -0800 (Sat, 10 Feb 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl;


/**
 * English reason phrases for HTTP status codes.
 * All status codes defined in RFC1945 (HTTP/1.0), RFC2616 (HTTP/1.1), and
 * RFC2518 (WebDAV) are supported.
 *
 * @author Unascribed
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 *
 * @version $Revision: 505744 $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class EnglishReasonPhraseCatalog implements org.apache.http.ReasonPhraseCatalog {

/**
 * Restricted default constructor, for derived classes.
 * If you need an instance of this class, use {@link #INSTANCE INSTANCE}.
 */

@Deprecated
protected EnglishReasonPhraseCatalog() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the reason phrase for a status code.
 *
 * @param status    the status code, in the range 100-599
 * @param loc       ignored
 *
 * @return  the reason phrase, or <code>null</code>
 */

@Deprecated
public java.lang.String getReason(int status, java.util.Locale loc) { throw new RuntimeException("Stub!"); }

/**
 * The default instance of this catalog.
 * This catalog is thread safe, so there typically
 * is no need to create other instances.
 */

@Deprecated public static final org.apache.http.impl.EnglishReasonPhraseCatalog INSTANCE;
static { INSTANCE = null; }
}

