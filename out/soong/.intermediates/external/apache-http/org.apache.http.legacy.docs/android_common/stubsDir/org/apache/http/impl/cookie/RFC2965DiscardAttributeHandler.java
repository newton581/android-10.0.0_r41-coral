/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/cookie/RFC2965DiscardAttributeHandler.java $
 * $Revision: 590695 $
 * $Date: 2007-10-31 07:55:41 -0700 (Wed, 31 Oct 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.cookie;


/**
  * <tt>"Discard"</tt> cookie attribute handler for RFC 2965 cookie spec.
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
  */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class RFC2965DiscardAttributeHandler implements org.apache.http.cookie.CookieAttributeHandler {

@Deprecated
public RFC2965DiscardAttributeHandler() { throw new RuntimeException("Stub!"); }

@Deprecated
public void parse(org.apache.http.cookie.SetCookie cookie, java.lang.String commenturl) throws org.apache.http.cookie.MalformedCookieException { throw new RuntimeException("Stub!"); }

@Deprecated
public void validate(org.apache.http.cookie.Cookie cookie, org.apache.http.cookie.CookieOrigin origin) throws org.apache.http.cookie.MalformedCookieException { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean match(org.apache.http.cookie.Cookie cookie, org.apache.http.cookie.CookieOrigin origin) { throw new RuntimeException("Stub!"); }
}

