/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/cookie/RFC2965DomainAttributeHandler.java $
 * $Revision: 653041 $
 * $Date: 2008-05-03 03:39:28 -0700 (Sat, 03 May 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.cookie;


/**
 * <tt>"Domain"</tt> cookie attribute handler for RFC 2965 cookie spec.
 *
 * @author jain.samit@gmail.com (Samit Jain)
 *
 * @since 3.1
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class RFC2965DomainAttributeHandler implements org.apache.http.cookie.CookieAttributeHandler {

@Deprecated
public RFC2965DomainAttributeHandler() { throw new RuntimeException("Stub!"); }

/**
 * Parse cookie domain attribute.
 */

@Deprecated
public void parse(org.apache.http.cookie.SetCookie cookie, java.lang.String domain) throws org.apache.http.cookie.MalformedCookieException { throw new RuntimeException("Stub!"); }

/**
 * Performs domain-match as defined by the RFC2965.
 * <p>
 * Host A's name domain-matches host B's if
 * <ol>
 *   <ul>their host name strings string-compare equal; or</ul>
 *   <ul>A is a HDN string and has the form NB, where N is a non-empty
 *       name string, B has the form .B', and B' is a HDN string.  (So,
 *       x.y.com domain-matches .Y.com but not Y.com.)</ul>
 * </ol>
 *
 * @param host host name where cookie is received from or being sent to.
 * @param domain The cookie domain attribute.
 * @return true if the specified host matches the given domain.
 */

@Deprecated
public boolean domainMatch(java.lang.String host, java.lang.String domain) { throw new RuntimeException("Stub!"); }

/**
 * Validate cookie domain attribute.
 */

@Deprecated
public void validate(org.apache.http.cookie.Cookie cookie, org.apache.http.cookie.CookieOrigin origin) throws org.apache.http.cookie.MalformedCookieException { throw new RuntimeException("Stub!"); }

/**
 * Match cookie domain attribute.
 */

@Deprecated
public boolean match(org.apache.http.cookie.Cookie cookie, org.apache.http.cookie.CookieOrigin origin) { throw new RuntimeException("Stub!"); }
}

