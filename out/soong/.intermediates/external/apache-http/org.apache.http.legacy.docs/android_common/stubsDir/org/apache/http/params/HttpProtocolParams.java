/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/params/HttpProtocolParams.java $
 * $Revision: 576089 $
 * $Date: 2007-09-16 05:39:56 -0700 (Sun, 16 Sep 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.params;

import org.apache.http.protocol.HTTP;
import org.apache.http.ProtocolVersion;

/**
 * This class implements an adaptor around the {@link HttpParams} interface
 * to simplify manipulation of the HTTP protocol specific parameters.
 * <br/>
 * Note that the <i>implements</i> relation to {@link CoreProtocolPNames}
 * is for compatibility with existing application code only. References to
 * the parameter names should use the interface, not this class.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 576089 $
 *
 * @since 4.0
 *
 * @see CoreProtocolPNames
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class HttpProtocolParams implements org.apache.http.params.CoreProtocolPNames {

/**
 */

HttpProtocolParams() { throw new RuntimeException("Stub!"); }

/**
 * Returns the charset to be used for writing HTTP headers.
 * @return The charset
 */

@Deprecated
public static java.lang.String getHttpElementCharset(org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

/**
 * Sets the charset to be used for writing HTTP headers.
 * @param charset The charset
 */

@Deprecated
public static void setHttpElementCharset(org.apache.http.params.HttpParams params, java.lang.String charset) { throw new RuntimeException("Stub!"); }

/**
 * Returns the default charset to be used for writing content body,
 * when no charset explicitly specified.
 * @return The charset
 */

@Deprecated
public static java.lang.String getContentCharset(org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

/**
 * Sets the default charset to be used for writing content body,
 * when no charset explicitly specified.
 * @param charset The charset
 */

@Deprecated
public static void setContentCharset(org.apache.http.params.HttpParams params, java.lang.String charset) { throw new RuntimeException("Stub!"); }

/**
 * Returns {@link ProtocolVersion protocol version} to be used per default.
 *
 * @return {@link ProtocolVersion protocol version}
 */

@Deprecated
public static org.apache.http.ProtocolVersion getVersion(org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

/**
 * Assigns the {@link ProtocolVersion protocol version} to be used by the
 * HTTP methods that this collection of parameters applies to.
 *
 * @param version the {@link ProtocolVersion protocol version}
 */

@Deprecated
public static void setVersion(org.apache.http.params.HttpParams params, org.apache.http.ProtocolVersion version) { throw new RuntimeException("Stub!"); }

@Deprecated
public static java.lang.String getUserAgent(org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

@Deprecated
public static void setUserAgent(org.apache.http.params.HttpParams params, java.lang.String useragent) { throw new RuntimeException("Stub!"); }

@Deprecated
public static boolean useExpectContinue(org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

@Deprecated
public static void setUseExpectContinue(org.apache.http.params.HttpParams params, boolean b) { throw new RuntimeException("Stub!"); }
}

