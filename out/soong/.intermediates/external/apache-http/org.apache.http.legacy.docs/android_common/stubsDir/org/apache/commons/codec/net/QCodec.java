/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.net;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.DecoderException;
import java.util.BitSet;

/**
 * <p>
 * Similar to the Quoted-Printable content-transfer-encoding defined in <a
 * href="http://www.ietf.org/rfc/rfc1521.txt">RFC 1521</a> and designed to allow text containing mostly ASCII
 * characters to be decipherable on an ASCII terminal without decoding.
 * </p>
 *
 * <p>
 * <a href="http://www.ietf.org/rfc/rfc1522.txt">RFC 1522</a> describes techniques to allow the encoding of non-ASCII
 * text in various portions of a RFC 822 [2] message header, in a manner which is unlikely to confuse existing message
 * handling software.
 * </p>
 *
 * @see <a href="http://www.ietf.org/rfc/rfc1522.txt">MIME (Multipurpose Internet Mail Extensions) Part Two: Message
 *          Header Extensions for Non-ASCII Text</a>
 *
 * @author Apache Software Foundation
 * @since 1.3
 * @version $Id: QCodec.java,v 1.6 2004/05/24 00:24:32 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class QCodec implements org.apache.commons.codec.StringEncoder, org.apache.commons.codec.StringDecoder {

/**
 * Default constructor.
 */

@Deprecated
public QCodec() { throw new RuntimeException("Stub!"); }

/**
 * Constructor which allows for the selection of a default charset
 *
 * @param charset
 *                  the default string charset to use.
 *
 * @see <a href="http://java.sun.com/j2se/1.3/docs/api/java/lang/package-summary.html#charenc">JRE character
 *          encoding names</a>
 */

@Deprecated
public QCodec(java.lang.String charset) { throw new RuntimeException("Stub!"); }

@Deprecated
protected java.lang.String getEncoding() { throw new RuntimeException("Stub!"); }

@Deprecated
protected byte[] doEncoding(byte[] bytes) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

@Deprecated
protected byte[] doDecoding(byte[] bytes) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a string into its quoted-printable form using the specified charset. Unsafe characters are escaped.
 *
 * @param pString
 *                  string to convert to quoted-printable form
 * @param charset
 *                  the charset for pString
 * @return quoted-printable string
 *
 * @throws EncoderException
 *                  thrown if a failure condition is encountered during the encoding process.
 */

@Deprecated
public java.lang.String encode(java.lang.String pString, java.lang.String charset) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a string into its quoted-printable form using the default charset. Unsafe characters are escaped.
 *
 * @param pString
 *                  string to convert to quoted-printable form
 * @return quoted-printable string
 *
 * @throws EncoderException
 *                  thrown if a failure condition is encountered during the encoding process.
 */

@Deprecated
public java.lang.String encode(java.lang.String pString) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a quoted-printable string into its original form. Escaped characters are converted back to their original
 * representation.
 *
 * @param pString
 *                  quoted-printable string to convert into its original form
 *
 * @return original string
 *
 * @throws DecoderException
 *                  A decoder exception is thrown if a failure condition is encountered during the decode process.
 */

@Deprecated
public java.lang.String decode(java.lang.String pString) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes an object into its quoted-printable form using the default charset. Unsafe characters are escaped.
 *
 * @param pObject
 *                  object to convert to quoted-printable form
 * @return quoted-printable object
 *
 * @throws EncoderException
 *                  thrown if a failure condition is encountered during the encoding process.
 */

@Deprecated
public java.lang.Object encode(java.lang.Object pObject) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a quoted-printable object into its original form. Escaped characters are converted back to their original
 * representation.
 *
 * @param pObject
 *                  quoted-printable object to convert into its original form
 *
 * @return original object
 *
 * @throws DecoderException
 *                  A decoder exception is thrown if a failure condition is encountered during the decode process.
 */

@Deprecated
public java.lang.Object decode(java.lang.Object pObject) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * The default charset used for string decoding and encoding.
 *
 * @return the default string charset.
 */

@Deprecated
public java.lang.String getDefaultCharset() { throw new RuntimeException("Stub!"); }

/**
 * Tests if optional tranformation of SPACE characters is to be used
 *
 * @return <code>true</code> if SPACE characters are to be transformed, <code>false</code> otherwise
 */

@Deprecated
public boolean isEncodeBlanks() { throw new RuntimeException("Stub!"); }

/**
 * Defines whether optional tranformation of SPACE characters is to be used
 *
 * @param b
 *                  <code>true</code> if SPACE characters are to be transformed, <code>false</code> otherwise
 */

@Deprecated
public void setEncodeBlanks(boolean b) { throw new RuntimeException("Stub!"); }
}

