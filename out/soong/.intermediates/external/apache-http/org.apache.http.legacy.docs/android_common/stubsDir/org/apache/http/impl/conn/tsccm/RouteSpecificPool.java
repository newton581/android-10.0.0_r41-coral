/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/tsccm/RouteSpecificPool.java $
 * $Revision: 677240 $
 * $Date: 2008-07-16 04:25:47 -0700 (Wed, 16 Jul 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn.tsccm;


/**
 * A connection sub-pool for a specific route, used by {@link ConnPoolByRoute}.
 * The methods in this class are unsynchronized. It is expected that the
 * containing pool takes care of synchronization.
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class RouteSpecificPool {

/**
 * Creates a new route-specific pool.
 *
 * @param route the route for which to pool
 * @param maxEntries the maximum number of entries allowed for this pool
 */

@Deprecated
public RouteSpecificPool(org.apache.http.conn.routing.HttpRoute route, int maxEntries) { throw new RuntimeException("Stub!"); }

/**
 * Obtains the route for which this pool is specific.
 *
 * @return  the route
 */

@Deprecated
public final org.apache.http.conn.routing.HttpRoute getRoute() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the maximum number of entries allowed for this pool.
 *
 * @return  the max entry number
 */

@Deprecated
public final int getMaxEntries() { throw new RuntimeException("Stub!"); }

/**
 * Indicates whether this pool is unused.
 * A pool is unused if there is neither an entry nor a waiting thread.
 * All entries count, not only the free but also the allocated ones.
 *
 * @return  <code>true</code> if this pool is unused,
 *          <code>false</code> otherwise
 */

@Deprecated
public boolean isUnused() { throw new RuntimeException("Stub!"); }

/**
 * Return remaining capacity of this pool
 *
 * @return capacity
 */

@Deprecated
public int getCapacity() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the number of entries.
 * This includes not only the free entries, but also those that
 * have been created and are currently issued to an application.
 *
 * @return  the number of entries for the route of this pool
 */

@Deprecated
public final int getEntryCount() { throw new RuntimeException("Stub!"); }

/**
 * Obtains a free entry from this pool, if one is available.
 *
 * @return an available pool entry, or <code>null</code> if there is none
 */

@Deprecated
public org.apache.http.impl.conn.tsccm.BasicPoolEntry allocEntry(java.lang.Object state) { throw new RuntimeException("Stub!"); }

/**
 * Returns an allocated entry to this pool.
 *
 * @param entry     the entry obtained from {@link #allocEntry allocEntry}
 *                  or presented to {@link #createdEntry createdEntry}
 */

@Deprecated
public void freeEntry(org.apache.http.impl.conn.tsccm.BasicPoolEntry entry) { throw new RuntimeException("Stub!"); }

/**
 * Indicates creation of an entry for this pool.
 * The entry will <i>not</i> be added to the list of free entries,
 * it is only recognized as belonging to this pool now. It can then
 * be passed to {@link #freeEntry freeEntry}.
 *
 * @param entry     the entry that was created for this pool
 */

@Deprecated
public void createdEntry(org.apache.http.impl.conn.tsccm.BasicPoolEntry entry) { throw new RuntimeException("Stub!"); }

/**
 * Deletes an entry from this pool.
 * Only entries that are currently free in this pool can be deleted.
 * Allocated entries can not be deleted.
 *
 * @param entry     the entry to delete from this pool
 *
 * @return  <code>true</code> if the entry was found and deleted, or
 *          <code>false</code> if the entry was not found
 */

@Deprecated
public boolean deleteEntry(org.apache.http.impl.conn.tsccm.BasicPoolEntry entry) { throw new RuntimeException("Stub!"); }

/**
 * Forgets about an entry from this pool.
 * This method is used to indicate that an entry
 * {@link #allocEntry allocated}
 * from this pool has been lost and will not be returned.
 */

@Deprecated
public void dropEntry() { throw new RuntimeException("Stub!"); }

/**
 * Adds a waiting thread.
 * This pool makes no attempt to match waiting threads with pool entries.
 * It is the caller's responsibility to check that there is no entry
 * before adding a waiting thread.
 *
 * @param wt        the waiting thread
 */

@Deprecated
public void queueThread(org.apache.http.impl.conn.tsccm.WaitingThread wt) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether there is a waiting thread in this pool.
 *
 * @return  <code>true</code> if there is a waiting thread,
 *          <code>false</code> otherwise
 */

@Deprecated
public boolean hasThread() { throw new RuntimeException("Stub!"); }

/**
 * Returns the next thread in the queue.
 *
 * @return  a waiting thread, or <code>null</code> if there is none
 */

@Deprecated
public org.apache.http.impl.conn.tsccm.WaitingThread nextThread() { throw new RuntimeException("Stub!"); }

/**
 * Removes a waiting thread, if it is queued.
 *
 * @param wt        the waiting thread
 */

@Deprecated
public void removeThread(org.apache.http.impl.conn.tsccm.WaitingThread wt) { throw new RuntimeException("Stub!"); }

/**
 * The list of free entries.
 * This list is managed LIFO, to increase idle times and
 * allow for closing connections that are not really needed.
 */

@Deprecated protected final java.util.LinkedList<org.apache.http.impl.conn.tsccm.BasicPoolEntry> freeEntries;
{ freeEntries = null; }

/** the maximum number of entries allowed for this pool */

@Deprecated protected final int maxEntries;
{ maxEntries = 0; }

/** The number of created entries. */

@Deprecated protected int numEntries;

/** The route this pool is for. */

@Deprecated protected final org.apache.http.conn.routing.HttpRoute route;
{ route = null; }

/** The list of threads waiting for this pool. */

@Deprecated protected final java.util.Queue<org.apache.http.impl.conn.tsccm.WaitingThread> waitingThreads;
{ waitingThreads = null; }
}

