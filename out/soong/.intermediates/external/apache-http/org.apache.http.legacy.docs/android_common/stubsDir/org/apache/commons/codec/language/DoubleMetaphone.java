/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.language;

import org.apache.commons.codec.EncoderException;

/**
 * Encodes a string into a double metaphone value.
 * This Implementation is based on the algorithm by <CITE>Lawrence Philips</CITE>.
 * <ul>
 * <li>Original Article: <a
 * href="http://www.cuj.com/documents/s=8038/cuj0006philips/">
 * http://www.cuj.com/documents/s=8038/cuj0006philips/</a></li>
 * <li>Original Source Code: <a href="ftp://ftp.cuj.com/pub/2000/1806/philips.zip">
 * ftp://ftp.cuj.com/pub/2000/1806/philips.zip</a></li>
 * </ul>
 *
 * @author Apache Software Foundation
 * @version $Id: DoubleMetaphone.java,v 1.24 2004/06/05 18:32:04 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class DoubleMetaphone implements org.apache.commons.codec.StringEncoder {

/**
 * Creates an instance of this DoubleMetaphone encoder
 */

@Deprecated
public DoubleMetaphone() { throw new RuntimeException("Stub!"); }

/**
 * Encode a value with Double Metaphone
 *
 * @param value String to encode
 * @return an encoded string
 */

@Deprecated
public java.lang.String doubleMetaphone(java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Encode a value with Double Metaphone, optionally using the alternate
 * encoding.
 *
 * @param value String to encode
 * @param alternate use alternate encode
 * @return an encoded string
 */

@Deprecated
public java.lang.String doubleMetaphone(java.lang.String value, boolean alternate) { throw new RuntimeException("Stub!"); }

/**
 * Encode the value using DoubleMetaphone.  It will only work if
 * <code>obj</code> is a <code>String</code> (like <code>Metaphone</code>).
 *
 * @param obj Object to encode (should be of type String)
 * @return An encoded Object (will be of type String)
 * @throws EncoderException encode parameter is not of type String
 */

@Deprecated
public java.lang.Object encode(java.lang.Object obj) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Encode the value using DoubleMetaphone.
 *
 * @param value String to encode
 * @return An encoded String
 */

@Deprecated
public java.lang.String encode(java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Check if the Double Metaphone values of two <code>String</code> values
 * are equal.
 *
 * @param value1 The left-hand side of the encoded {@link String#equals(Object)}.
 * @param value2 The right-hand side of the encoded {@link String#equals(Object)}.
 * @return <code>true</code> if the encoded <code>String</code>s are equal;
 *          <code>false</code> otherwise.
 * @see #isDoubleMetaphoneEqual(String,String,boolean)
 */

@Deprecated
public boolean isDoubleMetaphoneEqual(java.lang.String value1, java.lang.String value2) { throw new RuntimeException("Stub!"); }

/**
 * Check if the Double Metaphone values of two <code>String</code> values
 * are equal, optionally using the alternate value.
 *
 * @param value1 The left-hand side of the encoded {@link String#equals(Object)}.
 * @param value2 The right-hand side of the encoded {@link String#equals(Object)}.
 * @param alternate use the alternate value if <code>true</code>.
 * @return <code>true</code> if the encoded <code>String</code>s are equal;
 *          <code>false</code> otherwise.
 */

@Deprecated
public boolean isDoubleMetaphoneEqual(java.lang.String value1, java.lang.String value2, boolean alternate) { throw new RuntimeException("Stub!"); }

/**
 * Returns the maxCodeLen.
 * @return int
 */

@Deprecated
public int getMaxCodeLen() { throw new RuntimeException("Stub!"); }

/**
 * Sets the maxCodeLen.
 * @param maxCodeLen The maxCodeLen to set
 */

@Deprecated
public void setMaxCodeLen(int maxCodeLen) { throw new RuntimeException("Stub!"); }

/**
 * Gets the character at index <code>index</code> if available, otherwise
 * it returns <code>Character.MIN_VALUE</code> so that there is some sort
 * of a default
 */

@Deprecated
protected char charAt(java.lang.String value, int index) { throw new RuntimeException("Stub!"); }

/**
 * Determines whether <code>value</code> contains any of the criteria
 starting
 * at index <code>start</code> and matching up to length <code>length</code>
 */

@Deprecated
protected static boolean contains(java.lang.String value, int start, int length, java.lang.String[] criteria) { throw new RuntimeException("Stub!"); }

/**
 * Maximum length of an encoding, default is 4
 */

@Deprecated protected int maxCodeLen = 4; // 0x4
/**
 * Inner class for storing results, since there is the optional alternate
 * encoding.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class DoubleMetaphoneResult {

@Deprecated
public DoubleMetaphoneResult(int maxLength) { throw new RuntimeException("Stub!"); }

@Deprecated
public void append(char value) { throw new RuntimeException("Stub!"); }

@Deprecated
public void append(char primary, char alternate) { throw new RuntimeException("Stub!"); }

@Deprecated
public void appendPrimary(char value) { throw new RuntimeException("Stub!"); }

@Deprecated
public void appendAlternate(char value) { throw new RuntimeException("Stub!"); }

@Deprecated
public void append(java.lang.String value) { throw new RuntimeException("Stub!"); }

@Deprecated
public void append(java.lang.String primary, java.lang.String alternate) { throw new RuntimeException("Stub!"); }

@Deprecated
public void appendPrimary(java.lang.String value) { throw new RuntimeException("Stub!"); }

@Deprecated
public void appendAlternate(java.lang.String value) { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String getPrimary() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.String getAlternate() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isComplete() { throw new RuntimeException("Stub!"); }
}

}

