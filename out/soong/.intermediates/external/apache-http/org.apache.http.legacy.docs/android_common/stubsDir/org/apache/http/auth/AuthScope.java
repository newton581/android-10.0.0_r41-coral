/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/auth/AuthScope.java $
 * $Revision: 652950 $
 * $Date: 2008-05-02 16:49:48 -0700 (Fri, 02 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.auth;


/** 
 * The class represents an authentication scope consisting of a host name,
 * a port number, a realm name and an authentication scheme name which
 * {@link Credentials Credentials} apply to.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 * @author <a href="mailto:adrian@intencha.com">Adrian Sutton</a>
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class AuthScope {

/** Creates a new credentials scope for the given 
 * <tt>host</tt>, <tt>port</tt>, <tt>realm</tt>, and
 * <tt>authentication scheme</tt>.
 *
 * @param host the host the credentials apply to. May be set
 *   to <tt>null</tt> if credenticals are applicable to
 *   any host.
 * @param port the port the credentials apply to. May be set
 *   to negative value if credenticals are applicable to
 *   any port.
 * @param realm the realm the credentials apply to. May be set
 *   to <tt>null</tt> if credenticals are applicable to
 *   any realm.
 * @param scheme the authentication scheme the credentials apply to.
 *   May be set to <tt>null</tt> if credenticals are applicable to
 *   any authentication scheme.
 */

@Deprecated
public AuthScope(java.lang.String host, int port, java.lang.String realm, java.lang.String scheme) { throw new RuntimeException("Stub!"); }

/** Creates a new credentials scope for the given 
 * <tt>host</tt>, <tt>port</tt>, <tt>realm</tt>, and any
 * authentication scheme.
 *
 * @param host the host the credentials apply to. May be set
 *   to <tt>null</tt> if credenticals are applicable to
 *   any host.
 * @param port the port the credentials apply to. May be set
 *   to negative value if credenticals are applicable to
 *   any port.
 * @param realm the realm the credentials apply to. May be set
 *   to <tt>null</tt> if credenticals are applicable to
 *   any realm.
 */

@Deprecated
public AuthScope(java.lang.String host, int port, java.lang.String realm) { throw new RuntimeException("Stub!"); }

/** Creates a new credentials scope for the given 
 * <tt>host</tt>, <tt>port</tt>, any realm name, and any
 * authentication scheme.
 *
 * @param host the host the credentials apply to. May be set
 *   to <tt>null</tt> if credenticals are applicable to
 *   any host.
 * @param port the port the credentials apply to. May be set
 *   to negative value if credenticals are applicable to
 *   any port.
 */

@Deprecated
public AuthScope(java.lang.String host, int port) { throw new RuntimeException("Stub!"); }

/** 
 * Creates a copy of the given credentials scope.
 */

@Deprecated
public AuthScope(org.apache.http.auth.AuthScope authscope) { throw new RuntimeException("Stub!"); }

/**
 * @return the host
 */

@Deprecated
public java.lang.String getHost() { throw new RuntimeException("Stub!"); }

/**
 * @return the port
 */

@Deprecated
public int getPort() { throw new RuntimeException("Stub!"); }

/**
 * @return the realm name
 */

@Deprecated
public java.lang.String getRealm() { throw new RuntimeException("Stub!"); }

/**
 * @return the scheme type
 */

@Deprecated
public java.lang.String getScheme() { throw new RuntimeException("Stub!"); }

/**
 * Tests if the authentication scopes match.
 *
 * @return the match factor. Negative value signifies no match.
 *    Non-negative signifies a match. The greater the returned value
 *    the closer the match.
 */

@Deprecated
public int match(org.apache.http.auth.AuthScope that) { throw new RuntimeException("Stub!"); }

/**
 * @see java.lang.Object#equals(Object)
 */

@Deprecated
public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * @see java.lang.Object#toString()
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * @see java.lang.Object#hashCode()
 */

@Deprecated
public int hashCode() { throw new RuntimeException("Stub!"); }

/** 
 * Default scope matching any host, port, realm and authentication scheme.
 * In the future versions of HttpClient the use of this parameter will be
 * discontinued.
 */

@Deprecated public static final org.apache.http.auth.AuthScope ANY;
static { ANY = null; }

/** 
 * The <tt>null</tt> value represents any host. In the future versions of
 * HttpClient the use of this parameter will be discontinued.
 */

@Deprecated public static final java.lang.String ANY_HOST;
static { ANY_HOST = null; }

/** 
 * The <tt>-1</tt> value represents any port.
 */

@Deprecated public static final int ANY_PORT = -1; // 0xffffffff

/** 
 * The <tt>null</tt> value represents any realm.
 */

@Deprecated public static final java.lang.String ANY_REALM;
static { ANY_REALM = null; }

/** 
 * The <tt>null</tt> value represents any authentication scheme.
 */

@Deprecated public static final java.lang.String ANY_SCHEME;
static { ANY_SCHEME = null; }
}

