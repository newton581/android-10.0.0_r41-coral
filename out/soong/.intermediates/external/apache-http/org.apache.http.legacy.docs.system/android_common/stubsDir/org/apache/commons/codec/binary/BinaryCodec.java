/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.binary;

import org.apache.commons.codec.BinaryEncoder;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.DecoderException;

/**
 * Translates between byte arrays and strings of "0"s and "1"s.
 *
 * <b>TODO:</b> may want to add more bit vector functions like and/or/xor/nand.
 * <B>TODO:</b> also might be good to generate boolean[]
 * from byte[] et. cetera.
 *
 * @author Apache Software Foundation
 * @since 1.3
 * @version $Id $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BinaryCodec implements org.apache.commons.codec.BinaryDecoder, org.apache.commons.codec.BinaryEncoder {

@Deprecated
public BinaryCodec() { throw new RuntimeException("Stub!"); }

/**
 * Converts an array of raw binary data into an array of ascii 0 and 1 characters.
 *
 * @param raw
 *                  the raw binary data to convert
 * @return 0 and 1 ascii character bytes one for each bit of the argument
 * @see org.apache.commons.codec.BinaryEncoder#encode(byte[])
 */

@Deprecated
public byte[] encode(byte[] raw) { throw new RuntimeException("Stub!"); }

/**
 * Converts an array of raw binary data into an array of ascii 0 and 1 chars.
 *
 * @param raw
 *                  the raw binary data to convert
 * @return 0 and 1 ascii character chars one for each bit of the argument
 * @throws EncoderException
 *                  if the argument is not a byte[]
 * @see org.apache.commons.codec.Encoder#encode(java.lang.Object)
 */

@Deprecated
public java.lang.Object encode(java.lang.Object raw) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a byte array where each byte represents an ascii '0' or '1'.
 *
 * @param ascii
 *                  each byte represents an ascii '0' or '1'
 * @return the raw encoded binary where each bit corresponds to a byte in the byte array argument
 * @throws DecoderException
 *                  if argument is not a byte[], char[] or String
 * @see org.apache.commons.codec.Decoder#decode(java.lang.Object)
 */

@Deprecated
public java.lang.Object decode(java.lang.Object ascii) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a byte array where each byte represents an ascii '0' or '1'.
 *
 * @param ascii
 *                  each byte represents an ascii '0' or '1'
 * @return the raw encoded binary where each bit corresponds to a byte in the byte array argument
 * @see org.apache.commons.codec.Decoder#decode(Object)
 */

@Deprecated
public byte[] decode(byte[] ascii) { throw new RuntimeException("Stub!"); }

/**
 * Decodes a String where each char of the String represents an ascii '0' or '1'.
 *
 * @param ascii
 *                  String of '0' and '1' characters
 * @return the raw encoded binary where each bit corresponds to a byte in the byte array argument
 * @see org.apache.commons.codec.Decoder#decode(Object)
 */

@Deprecated
public byte[] toByteArray(java.lang.String ascii) { throw new RuntimeException("Stub!"); }

/**
 * Decodes a byte array where each char represents an ascii '0' or '1'.
 *
 * @param ascii
 *                  each char represents an ascii '0' or '1'
 * @return the raw encoded binary where each bit corresponds to a char in the char array argument
 */

@Deprecated
public static byte[] fromAscii(char[] ascii) { throw new RuntimeException("Stub!"); }

/**
 * Decodes a byte array where each byte represents an ascii '0' or '1'.
 *
 * @param ascii
 *                  each byte represents an ascii '0' or '1'
 * @return the raw encoded binary where each bit corresponds to a byte in the byte array argument
 */

@Deprecated
public static byte[] fromAscii(byte[] ascii) { throw new RuntimeException("Stub!"); }

/**
 * Converts an array of raw binary data into an array of ascii 0 and 1 character bytes - each byte is a truncated
 * char.
 *
 * @param raw
 *                  the raw binary data to convert
 * @return an array of 0 and 1 character bytes for each bit of the argument
 * @see org.apache.commons.codec.BinaryEncoder#encode(byte[])
 */

@Deprecated
public static byte[] toAsciiBytes(byte[] raw) { throw new RuntimeException("Stub!"); }

/**
 * Converts an array of raw binary data into an array of ascii 0 and 1 characters.
 *
 * @param raw
 *                  the raw binary data to convert
 * @return an array of 0 and 1 characters for each bit of the argument
 * @see org.apache.commons.codec.BinaryEncoder#encode(byte[])
 */

@Deprecated
public static char[] toAsciiChars(byte[] raw) { throw new RuntimeException("Stub!"); }

/**
 * Converts an array of raw binary data into a String of ascii 0 and 1 characters.
 *
 * @param raw
 *                  the raw binary data to convert
 * @return a String of 0 and 1 characters representing the binary data
 * @see org.apache.commons.codec.BinaryEncoder#encode(byte[])
 */

@Deprecated
public static java.lang.String toAsciiString(byte[] raw) { throw new RuntimeException("Stub!"); }
}

