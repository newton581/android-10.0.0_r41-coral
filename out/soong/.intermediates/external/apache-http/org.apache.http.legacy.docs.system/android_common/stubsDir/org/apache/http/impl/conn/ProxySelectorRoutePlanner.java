/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/ProxySelectorRoutePlanner.java $
 * $Revision: 658785 $
 * $Date: 2008-05-21 10:47:40 -0700 (Wed, 21 May 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn;

import org.apache.http.conn.params.ConnRoutePNames;
import java.net.ProxySelector;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.HttpException;
import java.net.InetSocketAddress;

/**
 * Default implementation of an {@link HttpRoutePlanner}.
 * This implementation is based on {@link java.net.ProxySelector}.
 * By default, it will pick up the proxy settings of the JVM, either
 * from system properties or from the browser running the application.
 * Additionally, it interprets some
 * {@link org.apache.http.conn.params.ConnRoutePNames parameters},
 * though not the {@link
 * org.apache.http.conn.params.ConnRoutePNames#DEFAULT_PROXY DEFAULT_PROXY}.
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class ProxySelectorRoutePlanner implements org.apache.http.conn.routing.HttpRoutePlanner {

/**
 * Creates a new proxy selector route planner.
 *
 * @param schreg    the scheme registry
 * @param prosel    the proxy selector, or
 *                  <code>null</code> for the system default
 */

@Deprecated
public ProxySelectorRoutePlanner(org.apache.http.conn.scheme.SchemeRegistry schreg, java.net.ProxySelector prosel) { throw new RuntimeException("Stub!"); }

/**
 * Obtains the proxy selector to use.
 *
 * @return the proxy selector, or <code>null</code> for the system default
 */

@Deprecated
public java.net.ProxySelector getProxySelector() { throw new RuntimeException("Stub!"); }

/**
 * Sets the proxy selector to use.
 *
 * @param prosel    the proxy selector, or
 *                  <code>null</code> to use the system default
 */

@Deprecated
public void setProxySelector(java.net.ProxySelector prosel) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.conn.routing.HttpRoute determineRoute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.protocol.HttpContext context) throws org.apache.http.HttpException { throw new RuntimeException("Stub!"); }

/**
 * Determines a proxy for the given target.
 *
 * @param target    the planned target, never <code>null</code>
 * @param request   the request to be sent, never <code>null</code>
 * @param context   the context, or <code>null</code>
 *
 * @return  the proxy to use, or <code>null</code> for a direct route
 *
 * @throws HttpException
 *         in case of system proxy settings that cannot be handled
 */

@Deprecated
protected org.apache.http.HttpHost determineProxy(org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.protocol.HttpContext context) throws org.apache.http.HttpException { throw new RuntimeException("Stub!"); }

/**
 * Obtains a host from an {@link InetSocketAddress}.
 *
 * @param isa       the socket address
 *
 * @return  a host string, either as a symbolic name or
 *          as a literal IP address string
 * <br/>
 * (TODO: determine format for IPv6 addresses, with or without [brackets])
 */

@Deprecated
protected java.lang.String getHost(java.net.InetSocketAddress isa) { throw new RuntimeException("Stub!"); }

@Deprecated
protected java.net.Proxy chooseProxy(java.util.List<java.net.Proxy> proxies, org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.protocol.HttpContext context) { throw new RuntimeException("Stub!"); }

/** The proxy selector to use, or <code>null</code> for system default. */

@Deprecated protected java.net.ProxySelector proxySelector;

/** The scheme registry. */

@Deprecated protected org.apache.http.conn.scheme.SchemeRegistry schemeRegistry;
}

