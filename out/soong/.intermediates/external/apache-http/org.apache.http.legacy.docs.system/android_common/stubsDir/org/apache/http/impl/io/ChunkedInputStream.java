/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/impl/io/ChunkedInputStream.java $
 * $Revision: 569843 $
 * $Date: 2007-08-26 10:05:40 -0700 (Sun, 26 Aug 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.io;

import org.apache.http.protocol.HTTP;
import java.io.IOException;
import java.io.InputStream;

/**
 * Implements chunked transfer coding.
 * See <a href="http://www.w3.org/Protocols/rfc2616/rfc2616.txt">RFC 2616</a>,
 * <a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.6">section 3.6.1</a>.
 * It transparently coalesces chunks of a HTTP stream that uses chunked
 * transfer coding. After the stream is read to the end, it provides access
 * to the trailers, if any.
 * <p>
 * Note that this class NEVER closes the underlying stream, even when close
 * gets called.  Instead, it will read until the "end" of its chunking on
 * close, which allows for the seamless execution of subsequent HTTP 1.1
 * requests, while not requiring the client to remember to read the entire
 * contents of the response.
 * </p>
 *
 * @author Ortwin Glueck
 * @author Sean C. Sullivan
 * @author Martin Elwin
 * @author Eric Johnson
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author Michael Becke
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @since 4.0
 *
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class ChunkedInputStream extends java.io.InputStream {

@Deprecated
public ChunkedInputStream(org.apache.http.io.SessionInputBuffer in) { throw new RuntimeException("Stub!"); }

/**
 * <p> Returns all the data in a chunked stream in coalesced form. A chunk
 * is followed by a CRLF. The method returns -1 as soon as a chunksize of 0
 * is detected.</p>
 *
 * <p> Trailer headers are read automcatically at the end of the stream and
 * can be obtained with the getResponseFooters() method.</p>
 *
 * @return -1 of the end of the stream has been reached or the next data
 * byte
 * @throws IOException If an IO problem occurs
 */

@Deprecated
public int read() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Read some bytes from the stream.
 * @param b The byte array that will hold the contents from the stream.
 * @param off The offset into the byte array at which bytes will start to be
 * placed.
 * @param len the maximum number of bytes that can be returned.
 * @return The number of bytes returned or -1 if the end of stream has been
 * reached.
 * @see java.io.InputStream#read(byte[], int, int)
 * @throws IOException if an IO problem occurs.
 */

@Deprecated
public int read(byte[] b, int off, int len) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Read some bytes from the stream.
 * @param b The byte array that will hold the contents from the stream.
 * @return The number of bytes returned or -1 if the end of stream has been
 * reached.
 * @see java.io.InputStream#read(byte[])
 * @throws IOException if an IO problem occurs.
 */

@Deprecated
public int read(byte[] b) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Upon close, this reads the remainder of the chunked message,
 * leaving the underlying socket at a position to start reading the
 * next response without scanning.
 * @throws IOException If an IO problem occurs.
 */

@Deprecated
public void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.Header[] getFooters() { throw new RuntimeException("Stub!"); }
}

