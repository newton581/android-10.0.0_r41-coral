/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/impl/DefaultHttpResponseFactory.java $
 * $Revision: 618367 $
 * $Date: 2008-02-04 10:26:06 -0800 (Mon, 04 Feb 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl;

import org.apache.http.impl.EnglishReasonPhraseCatalog;

/**
 * Default implementation of a factory for creating response objects.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 618367 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class DefaultHttpResponseFactory implements org.apache.http.HttpResponseFactory {

/**
 * Creates a new response factory with the given catalog.
 *
 * @param catalog   the catalog of reason phrases
 */

@Deprecated
public DefaultHttpResponseFactory(org.apache.http.ReasonPhraseCatalog catalog) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new response factory with the default catalog.
 * The default catalog is
 * {@link EnglishReasonPhraseCatalog EnglishReasonPhraseCatalog}.
 */

@Deprecated
public DefaultHttpResponseFactory() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpResponse newHttpResponse(org.apache.http.ProtocolVersion ver, int status, org.apache.http.protocol.HttpContext context) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpResponse newHttpResponse(org.apache.http.StatusLine statusline, org.apache.http.protocol.HttpContext context) { throw new RuntimeException("Stub!"); }

/**
 * Determines the locale of the response.
 * The implementation in this class always returns the default locale.
 *
 * @param context   the context from which to determine the locale, or
 *                  <code>null</code> to use the default locale
 *
 * @return  the locale for the response, never <code>null</code>
 */

@Deprecated
protected java.util.Locale determineLocale(org.apache.http.protocol.HttpContext context) { throw new RuntimeException("Stub!"); }

/** The catalog for looking up reason phrases. */

@Deprecated protected final org.apache.http.ReasonPhraseCatalog reasonCatalog;
{ reasonCatalog = null; }
}

