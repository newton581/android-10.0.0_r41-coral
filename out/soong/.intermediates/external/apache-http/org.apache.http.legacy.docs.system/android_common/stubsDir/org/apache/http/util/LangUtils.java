/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/util/LangUtils.java $
 * $Revision: 503413 $
 * $Date: 2007-02-04 06:22:14 -0800 (Sun, 04 Feb 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.util;


/**
 * A set of utility methods to help produce consistent
 * {@link Object#equals equals} and {@link Object#hashCode hashCode} methods.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class LangUtils {

/** Disabled default constructor. */

LangUtils() { throw new RuntimeException("Stub!"); }

@Deprecated
public static int hashCode(int seed, int hashcode) { throw new RuntimeException("Stub!"); }

@Deprecated
public static int hashCode(int seed, boolean b) { throw new RuntimeException("Stub!"); }

@Deprecated
public static int hashCode(int seed, java.lang.Object obj) { throw new RuntimeException("Stub!"); }

@Deprecated
public static boolean equals(java.lang.Object obj1, java.lang.Object obj2) { throw new RuntimeException("Stub!"); }

@Deprecated
public static boolean equals(java.lang.Object[] a1, java.lang.Object[] a2) { throw new RuntimeException("Stub!"); }

@Deprecated public static final int HASH_OFFSET = 37; // 0x25

@Deprecated public static final int HASH_SEED = 17; // 0x11
}

