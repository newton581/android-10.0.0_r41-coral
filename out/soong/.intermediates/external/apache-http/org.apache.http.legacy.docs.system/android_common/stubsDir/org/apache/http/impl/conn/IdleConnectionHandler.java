/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/IdleConnectionHandler.java $
 * $Revision: 673450 $
 * $Date: 2008-07-02 10:35:05 -0700 (Wed, 02 Jul 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

package org.apache.http.impl.conn;


/**
 * A helper class for connection managers to track idle connections.
 *
 * <p>This class is not synchronized.</p>
 *
 * @see org.apache.http.conn.ClientConnectionManager#closeIdleConnections
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class IdleConnectionHandler {

@Deprecated
public IdleConnectionHandler() { throw new RuntimeException("Stub!"); }

/**
 * Registers the given connection with this handler.  The connection will be held until
 * {@link #remove} or {@link #closeIdleConnections} is called.
 *
 * @param connection the connection to add
 *
 * @see #remove
 */

@Deprecated
public void add(org.apache.http.HttpConnection connection, long validDuration, java.util.concurrent.TimeUnit unit) { throw new RuntimeException("Stub!"); }

/**
 * Removes the given connection from the list of connections to be closed when idle.
 * This will return true if the connection is still valid, and false
 * if the connection should be considered expired and not used.
 *
 * @param connection
 * @return True if the connection is still valid.
 */

@Deprecated
public boolean remove(org.apache.http.HttpConnection connection) { throw new RuntimeException("Stub!"); }

/**
 * Removes all connections referenced by this handler.
 */

@Deprecated
public void removeAll() { throw new RuntimeException("Stub!"); }

/**
 * Closes connections that have been idle for at least the given amount of time.
 *
 * @param idleTime the minimum idle time, in milliseconds, for connections to be closed
 */

@Deprecated
public void closeIdleConnections(long idleTime) { throw new RuntimeException("Stub!"); }

@Deprecated
public void closeExpiredConnections() { throw new RuntimeException("Stub!"); }
}

