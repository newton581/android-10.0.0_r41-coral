/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/util/VersionInfo.java $
 * $Revision: 554888 $
 * $Date: 2007-07-10 02:46:36 -0700 (Tue, 10 Jul 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.util;

import java.util.Properties;

/**
 * Provides access to version information for HTTP components.
 * Instances of this class provide version information for a single module
 * or informal unit, as explained
 * <a href="http://wiki.apache.org/jakarta-httpclient/HttpComponents">here</a>.
 * Static methods are used to extract version information from property
 * files that are automatically packaged with HTTP component release JARs.
 * <br/>
 * All available version information is provided in strings, where
 * the string format is informal and subject to change without notice.
 * Version information is provided for debugging output and interpretation
 * by humans, not for automated processing in applications.
 *
 * @author <a href="mailto:oleg@ural.ru">Oleg Kalnichevski</a>
 * @author and others
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class VersionInfo {

/**
 * Instantiates version information.
 *
 * @param pckg      the package
 * @param module    the module, or <code>null</code>
 * @param release   the release, or <code>null</code>
 * @param time      the build time, or <code>null</code>
 * @param clsldr    the class loader, or <code>null</code>
 */

@Deprecated
protected VersionInfo(java.lang.String pckg, java.lang.String module, java.lang.String release, java.lang.String time, java.lang.String clsldr) { throw new RuntimeException("Stub!"); }

/**
 * Obtains the package name.
 * The package name identifies the module or informal unit.
 *
 * @return  the package name, never <code>null</code>
 */

@Deprecated
public final java.lang.String getPackage() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the name of the versioned module or informal unit.
 * This data is read from the version information for the package.
 *
 * @return  the module name, never <code>null</code>
 */

@Deprecated
public final java.lang.String getModule() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the release of the versioned module or informal unit.
 * This data is read from the version information for the package.
 *
 * @return  the release version, never <code>null</code>
 */

@Deprecated
public final java.lang.String getRelease() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the timestamp of the versioned module or informal unit.
 * This data is read from the version information for the package.
 *
 * @return  the timestamp, never <code>null</code>
 */

@Deprecated
public final java.lang.String getTimestamp() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the classloader used to read the version information.
 * This is just the <code>toString</code> output of the classloader,
 * since the version information should not keep a reference to
 * the classloader itself. That could prevent garbage collection.
 *
 * @return  the classloader description, never <code>null</code>
 */

@Deprecated
public final java.lang.String getClassloader() { throw new RuntimeException("Stub!"); }

/**
 * Provides the version information in human-readable format.
 *
 * @return  a string holding this version information
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Loads version information for a list of packages.
 *
 * @param pckgs     the packages for which to load version info
 * @param clsldr    the classloader to load from, or
 *                  <code>null</code> for the thread context classloader
 *
 * @return  the version information for all packages found,
 *          never <code>null</code>
 */

@Deprecated
public static final org.apache.http.util.VersionInfo[] loadVersionInfo(java.lang.String[] pckgs, java.lang.ClassLoader clsldr) { throw new RuntimeException("Stub!"); }

/**
 * Loads version information for a package.
 *
 * @param pckg      the package for which to load version information,
 *                  for example "org.apache.http".
 *                  The package name should NOT end with a dot.
 * @param clsldr    the classloader to load from, or
 *                  <code>null</code> for the thread context classloader
 *
 * @return  the version information for the argument package, or
 *          <code>null</code> if not available
 */

@Deprecated
public static final org.apache.http.util.VersionInfo loadVersionInfo(java.lang.String pckg, java.lang.ClassLoader clsldr) { throw new RuntimeException("Stub!"); }

/**
 * Instantiates version information from properties.
 *
 * @param pckg      the package for the version information
 * @param info      the map from string keys to string values,
 *                  for example {@link java.util.Properties}
 * @param clsldr    the classloader, or <code>null</code>
 *
 * @return  the version information
 */

@Deprecated
protected static final org.apache.http.util.VersionInfo fromMap(java.lang.String pckg, java.util.Map info, java.lang.ClassLoader clsldr) { throw new RuntimeException("Stub!"); }

@Deprecated public static final java.lang.String PROPERTY_MODULE = "info.module";

@Deprecated public static final java.lang.String PROPERTY_RELEASE = "info.release";

@Deprecated public static final java.lang.String PROPERTY_TIMESTAMP = "info.timestamp";

/** A string constant for unavailable information. */

@Deprecated public static final java.lang.String UNAVAILABLE = "UNAVAILABLE";

/** The filename of the version information files. */

@Deprecated public static final java.lang.String VERSION_PROPERTY_FILE = "version.properties";
}

