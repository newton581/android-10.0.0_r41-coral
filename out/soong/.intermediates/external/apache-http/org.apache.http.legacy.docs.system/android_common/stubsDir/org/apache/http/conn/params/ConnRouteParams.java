/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/conn/params/ConnRouteParams.java $
 * $Revision: 658785 $
 * $Date: 2008-05-21 10:47:40 -0700 (Wed, 21 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.conn.params;

import org.apache.http.params.HttpParams;

/**
 * An adaptor for accessing route related parameters in {@link HttpParams}.
 * See {@link ConnRoutePNames} for parameter name definitions.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 * @version $Revision: 658785 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class ConnRouteParams implements org.apache.http.conn.params.ConnRoutePNames {

/** Disabled default constructor. */

ConnRouteParams() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the {@link ConnRoutePNames#DEFAULT_PROXY DEFAULT_PROXY}
 * parameter value.
 * {@link #NO_HOST} will be mapped to <code>null</code>,
 * to allow unsetting in a hierarchy.
 *
 * @param params    the parameters in which to look up
 *
 * @return  the default proxy set in the argument parameters, or
 *          <code>null</code> if not set
 */

@Deprecated
public static org.apache.http.HttpHost getDefaultProxy(org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

/**
 * Sets the {@link ConnRoutePNames#DEFAULT_PROXY DEFAULT_PROXY}
 * parameter value.
 *
 * @param params    the parameters in which to set the value
 * @param proxy     the value to set, may be <code>null</code>.
 *                  Note that {@link #NO_HOST} will be mapped to
 *                  <code>null</code> by {@link #getDefaultProxy},
 *                  to allow for explicit unsetting in hierarchies.
 */

@Deprecated
public static void setDefaultProxy(org.apache.http.params.HttpParams params, org.apache.http.HttpHost proxy) { throw new RuntimeException("Stub!"); }

/**
 * Obtains the {@link ConnRoutePNames#FORCED_ROUTE FORCED_ROUTE}
 * parameter value.
 * {@link #NO_ROUTE} will be mapped to <code>null</code>,
 * to allow unsetting in a hierarchy.
 *
 * @param params    the parameters in which to look up
 *
 * @return  the forced route set in the argument parameters, or
 *          <code>null</code> if not set
 */

@Deprecated
public static org.apache.http.conn.routing.HttpRoute getForcedRoute(org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

/**
 * Sets the {@link ConnRoutePNames#FORCED_ROUTE FORCED_ROUTE}
 * parameter value.
 *
 * @param params    the parameters in which to set the value
 * @param route     the value to set, may be <code>null</code>.
 *                  Note that {@link #NO_ROUTE} will be mapped to
 *                  <code>null</code> by {@link #getForcedRoute},
 *                  to allow for explicit unsetting in hierarchies.
 */

@Deprecated
public static void setForcedRoute(org.apache.http.params.HttpParams params, org.apache.http.conn.routing.HttpRoute route) { throw new RuntimeException("Stub!"); }

/**
 * Obtains the {@link ConnRoutePNames#LOCAL_ADDRESS LOCAL_ADDRESS}
 * parameter value.
 * There is no special value that would automatically be mapped to
 * <code>null</code>. You can use the wildcard address (0.0.0.0 for IPv4,
 * :: for IPv6) to override a specific local address in a hierarchy.
 *
 * @param params    the parameters in which to look up
 *
 * @return  the local address set in the argument parameters, or
 *          <code>null</code> if not set
 */

@Deprecated
public static java.net.InetAddress getLocalAddress(org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

/**
 * Sets the {@link ConnRoutePNames#LOCAL_ADDRESS LOCAL_ADDRESS}
 * parameter value.
 *
 * @param params    the parameters in which to set the value
 * @param local     the value to set, may be <code>null</code>
 */

@Deprecated
public static void setLocalAddress(org.apache.http.params.HttpParams params, java.net.InetAddress local) { throw new RuntimeException("Stub!"); }

/**
 * A special value indicating "no host".
 * This relies on a nonsense scheme name to avoid conflicts
 * with actual hosts. Note that this is a <i>valid</i> host.
 */

@Deprecated public static final org.apache.http.HttpHost NO_HOST;
static { NO_HOST = null; }

/**
 * A special value indicating "no route".
 * This is a route with {@link #NO_HOST} as the target.
 */

@Deprecated public static final org.apache.http.conn.routing.HttpRoute NO_ROUTE;
static { NO_ROUTE = null; }
}

