/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/entity/InputStreamEntity.java $
 * $Revision: 617591 $
 * $Date: 2008-02-01 10:21:17 -0800 (Fri, 01 Feb 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.entity;

import java.io.InputStream;

/**
 * A streamed entity obtaining content from an {@link InputStream InputStream}.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 617591 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class InputStreamEntity extends org.apache.http.entity.AbstractHttpEntity {

@Deprecated
public InputStreamEntity(java.io.InputStream instream, long length) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isRepeatable() { throw new RuntimeException("Stub!"); }

@Deprecated
public long getContentLength() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.io.InputStream getContent() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void writeTo(java.io.OutputStream outstream) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isStreaming() { throw new RuntimeException("Stub!"); }

@Deprecated
public void consumeContent() throws java.io.IOException { throw new RuntimeException("Stub!"); }
}

