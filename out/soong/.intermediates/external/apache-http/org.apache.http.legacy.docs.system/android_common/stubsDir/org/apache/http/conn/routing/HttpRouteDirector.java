/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/conn/routing/HttpRouteDirector.java $
 * $Revision: 620255 $
 * $Date: 2008-02-10 02:23:55 -0800 (Sun, 10 Feb 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.conn.routing;


/**
 * Provides directions on establishing a route.
 * Implementations of this interface compare a planned route with
 * a tracked route and indicate the next step required.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version $Revision: 620255 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public interface HttpRouteDirector {

/**
 * Provides the next step.
 *
 * @param plan      the planned route
 * @param fact      the currently established route, or
 *                  <code>null</code> if nothing is established
 *
 * @return  one of the constants defined in this interface, indicating
 *          either the next step to perform, or success, or failure.
 *          0 is for success, a negative value for failure.
 */

@Deprecated
public int nextStep(org.apache.http.conn.routing.RouteInfo plan, org.apache.http.conn.routing.RouteInfo fact);

/** Indicates that the route is complete. */

@Deprecated public static final int COMPLETE = 0; // 0x0

/** Step: open connection to proxy. */

@Deprecated public static final int CONNECT_PROXY = 2; // 0x2

/** Step: open connection to target. */

@Deprecated public static final int CONNECT_TARGET = 1; // 0x1

/** Step: layer protocol (over tunnel). */

@Deprecated public static final int LAYER_PROTOCOL = 5; // 0x5

/** Step: tunnel through proxy to other proxy. */

@Deprecated public static final int TUNNEL_PROXY = 4; // 0x4

/** Step: tunnel through proxy to target. */

@Deprecated public static final int TUNNEL_TARGET = 3; // 0x3

/** Indicates that the route can not be established at all. */

@Deprecated public static final int UNREACHABLE = -1; // 0xffffffff
}

