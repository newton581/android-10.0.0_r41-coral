/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/cookie/RFC2109Spec.java $
 * $Revision: 677240 $
 * $Date: 2008-07-16 04:25:47 -0700 (Wed, 16 Jul 2008) $
 * 
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.cookie;

import org.apache.http.cookie.Cookie;

/**
 * RFC 2109 compliant cookie policy
 *
 * @author  B.C. Holmes
 * @author <a href="mailto:jericho@thinkfree.com">Park, Sung-Gu</a>
 * @author <a href="mailto:dsale@us.britannica.com">Doug Sale</a>
 * @author Rod Waldhoff
 * @author dIon Gillard
 * @author Sean C. Sullivan
 * @author <a href="mailto:JEvans@Cyveillance.com">John Evans</a>
 * @author Marc A. Saegesser
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class RFC2109Spec extends org.apache.http.impl.cookie.CookieSpecBase {

/** Default constructor */

@Deprecated
public RFC2109Spec(java.lang.String[] datepatterns, boolean oneHeader) { throw new RuntimeException("Stub!"); }

/** Default constructor */

@Deprecated
public RFC2109Spec() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.util.List<org.apache.http.cookie.Cookie> parse(org.apache.http.Header header, org.apache.http.cookie.CookieOrigin origin) throws org.apache.http.cookie.MalformedCookieException { throw new RuntimeException("Stub!"); }

@Deprecated
public void validate(org.apache.http.cookie.Cookie cookie, org.apache.http.cookie.CookieOrigin origin) throws org.apache.http.cookie.MalformedCookieException { throw new RuntimeException("Stub!"); }

@Deprecated
public java.util.List<org.apache.http.Header> formatCookies(java.util.List<org.apache.http.cookie.Cookie> cookies) { throw new RuntimeException("Stub!"); }

/**
 * Return a name/value string suitable for sending in a <tt>"Cookie"</tt>
 * header as defined in RFC 2109 for backward compatibility with cookie
 * version 0
 * @param buffer The char array buffer to use for output
 * @param name The cookie name
 * @param value The cookie value
 * @param version The cookie version
 */

@Deprecated
protected void formatParamAsVer(org.apache.http.util.CharArrayBuffer buffer, java.lang.String name, java.lang.String value, int version) { throw new RuntimeException("Stub!"); }

/**
 * Return a string suitable for sending in a <tt>"Cookie"</tt> header
 * as defined in RFC 2109 for backward compatibility with cookie version 0
 * @param buffer The char array buffer to use for output
 * @param cookie The {@link Cookie} to be formatted as string
 * @param version The version to use.
 */

@Deprecated
protected void formatCookieAsVer(org.apache.http.util.CharArrayBuffer buffer, org.apache.http.cookie.Cookie cookie, int version) { throw new RuntimeException("Stub!"); }

@Deprecated
public int getVersion() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.Header getVersionHeader() { throw new RuntimeException("Stub!"); }
}

