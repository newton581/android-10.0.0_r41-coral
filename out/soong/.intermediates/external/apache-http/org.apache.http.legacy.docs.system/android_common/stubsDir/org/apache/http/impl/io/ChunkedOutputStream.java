/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/impl/io/ChunkedOutputStream.java $
 * $Revision: 645081 $
 * $Date: 2008-04-05 04:36:42 -0700 (Sat, 05 Apr 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.io;

import java.io.IOException;

/**
 * Implements chunked transfer coding.
 * See <a href="http://www.w3.org/Protocols/rfc2616/rfc2616.txt">RFC 2616</a>,
 * <a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.6">section 3.6.1</a>.
 * Writes are buffered to an internal buffer (2048 default size).
 *
 * @author Mohammad Rezaei (Goldman, Sachs &amp; Co.)
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class ChunkedOutputStream extends java.io.OutputStream {

/**
 * Wraps a session output buffer and chunks the output.
 * @param out the session output buffer to wrap
 * @param bufferSize minimum chunk size (excluding last chunk)
 * @throws IOException
 */

@Deprecated
public ChunkedOutputStream(org.apache.http.io.SessionOutputBuffer out, int bufferSize) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Wraps a session output buffer and chunks the output. The default buffer
 * size of 2048 was chosen because the chunk overhead is less than 0.5%
 *
 * @param out       the output buffer to wrap
 * @throws IOException
 */

@Deprecated
public ChunkedOutputStream(org.apache.http.io.SessionOutputBuffer out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Writes the cache out onto the underlying stream
 * @throws IOException
 */

@Deprecated
protected void flushCache() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Writes the cache and bufferToAppend to the underlying stream
 * as one large chunk
 * @param bufferToAppend
 * @param off
 * @param len
 * @throws IOException
 */

@Deprecated
protected void flushCacheWithAppend(byte[] bufferToAppend, int off, int len) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
protected void writeClosingChunk() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Must be called to ensure the internal cache is flushed and the closing chunk is written.
 * @throws IOException
 */

@Deprecated
public void finish() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void write(int b) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Writes the array. If the array does not fit within the buffer, it is
 * not split, but rather written out as one large chunk.
 * @param b
 * @throws IOException
 */

@Deprecated
public void write(byte[] b) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void write(byte[] src, int off, int len) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Flushes the content buffer and the underlying stream.
 * @throws IOException
 */

@Deprecated
public void flush() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Finishes writing to the underlying stream, but does NOT close the underlying stream.
 * @throws IOException
 */

@Deprecated
public void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }
}

