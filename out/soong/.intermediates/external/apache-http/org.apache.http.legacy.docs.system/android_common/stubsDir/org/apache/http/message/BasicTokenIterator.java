/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/message/BasicTokenIterator.java $
 * $Revision: 602520 $
 * $Date: 2007-12-08 09:42:26 -0800 (Sat, 08 Dec 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.message;

import org.apache.http.TokenIterator;
import org.apache.http.ParseException;
import java.util.NoSuchElementException;

/**
 * Basic implementation of a {@link TokenIterator}.
 * This implementation parses <tt>#token<tt> sequences as
 * defined by RFC 2616, section 2.
 * It extends that definition somewhat beyond US-ASCII.
 *
 * @version $Revision: 602520 $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicTokenIterator implements org.apache.http.TokenIterator {

/**
 * Creates a new instance of {@link BasicTokenIterator}.
 *
 * @param headerIterator    the iterator for the headers to tokenize
 */

@Deprecated
public BasicTokenIterator(org.apache.http.HeaderIterator headerIterator) { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean hasNext() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the next token from this iteration.
 *
 * @return  the next token in this iteration
 *
 * @throws NoSuchElementException   if the iteration is already over
 * @throws ParseException   if an invalid header value is encountered
 */

@Deprecated
public java.lang.String nextToken() throws java.util.NoSuchElementException, org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

/**
 * Returns the next token.
 * Same as {@link #nextToken}, but with generic return type.
 *
 * @return  the next token in this iteration
 *
 * @throws NoSuchElementException   if there are no more tokens
 * @throws ParseException   if an invalid header value is encountered
 */

@Deprecated
public final java.lang.Object next() throws java.util.NoSuchElementException, org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

/**
 * Removing tokens is not supported.
 *
 * @throws UnsupportedOperationException    always
 */

@Deprecated
public final void remove() throws java.lang.UnsupportedOperationException { throw new RuntimeException("Stub!"); }

/**
 * Determines the next token.
 * If found, the token is stored in {@link #currentToken}.
 * The return value indicates the position after the token
 * in {@link #currentHeader}. If necessary, the next header
 * will be obtained from {@link #headerIt}.
 * If not found, {@link #currentToken} is set to <code>null</code>.
 *
 * @param from      the position in the current header at which to
 *                  start the search, -1 to search in the first header
 *
 * @return  the position after the found token in the current header, or
 *          negative if there was no next token
 *
 * @throws ParseException   if an invalid header value is encountered
 */

@Deprecated
protected int findNext(int from) throws org.apache.http.ParseException { throw new RuntimeException("Stub!"); }

/**
 * Creates a new token to be returned.
 * Called from {@link #findNext findNext} after the token is identified.
 * The default implementation simply calls
 * {@link java.lang.String#substring String.substring}.
 * <br/>
 * If header values are significantly longer than tokens, and some
 * tokens are permanently referenced by the application, there can
 * be problems with garbage collection. A substring will hold a
 * reference to the full characters of the original string and
 * therefore occupies more memory than might be expected.
 * To avoid this, override this method and create a new string
 * instead of a substring.
 *
 * @param value     the full header value from which to create a token
 * @param start     the index of the first token character
 * @param end       the index after the last token character
 *
 * @return  a string representing the token identified by the arguments
 */

@Deprecated
protected java.lang.String createToken(java.lang.String value, int start, int end) { throw new RuntimeException("Stub!"); }

/**
 * Determines the starting position of the next token.
 * This method will iterate over headers if necessary.
 *
 * @param from      the position in the current header at which to
 *                  start the search
 *
 * @return  the position of the token start in the current header,
 *          negative if no token start could be found
 */

@Deprecated
protected int findTokenStart(int from) { throw new RuntimeException("Stub!"); }

/**
 * Determines the position of the next token separator.
 * Because of multi-header joining rules, the end of a
 * header value is a token separator. This method does
 * therefore not need to iterate over headers.
 *
 * @param from      the position in the current header at which to
 *                  start the search
 *
 * @return  the position of a token separator in the current header,
 *          or at the end
 *
 * @throws ParseException
 *         if a new token is found before a token separator.
 *         RFC 2616, section 2.1 explicitly requires a comma between
 *         tokens for <tt>#</tt>.
 */

@Deprecated
protected int findTokenSeparator(int from) { throw new RuntimeException("Stub!"); }

/**
 * Determines the ending position of the current token.
 * This method will not leave the current header value,
 * since the end of the header value is a token boundary.
 *
 * @param from      the position of the first character of the token
 *
 * @return  the position after the last character of the token.
 *          The behavior is undefined if <code>from</code> does not
 *          point to a token character in the current header value.
 */

@Deprecated
protected int findTokenEnd(int from) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether a character is a token separator.
 * RFC 2616, section 2.1 defines comma as the separator for
 * <tt>#token</tt> sequences. The end of a header value will
 * also separate tokens, but that is not a character check.
 *
 * @param ch        the character to check
 *
 * @return  <code>true</code> if the character is a token separator,
 *          <code>false</code> otherwise
 */

@Deprecated
protected boolean isTokenSeparator(char ch) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether a character is a whitespace character.
 * RFC 2616, section 2.2 defines space and horizontal tab as whitespace.
 * The optional preceeding line break is irrelevant, since header
 * continuation is handled transparently when parsing messages.
 *
 * @param ch        the character to check
 *
 * @return  <code>true</code> if the character is whitespace,
 *          <code>false</code> otherwise
 */

@Deprecated
protected boolean isWhitespace(char ch) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether a character is a valid token character.
 * Whitespace, control characters, and HTTP separators are not
 * valid token characters. The HTTP specification (RFC 2616, section 2.2)
 * defines tokens only for the US-ASCII character set, this
 * method extends the definition to other character sets.
 *
 * @param ch        the character to check
 *
 * @return  <code>true</code> if the character is a valid token start,
 *          <code>false</code> otherwise
 */

@Deprecated
protected boolean isTokenChar(char ch) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether a character is an HTTP separator.
 * The implementation in this class checks only for the HTTP separators
 * defined in RFC 2616, section 2.2. If you need to detect other
 * separators beyond the US-ASCII character set, override this method.
 *
 * @param ch        the character to check
 *
 * @return  <code>true</code> if the character is an HTTP separator
 */

@Deprecated
protected boolean isHttpSeparator(char ch) { throw new RuntimeException("Stub!"); }

/** The HTTP separator characters. Defined in RFC 2616, section 2.2. */

@Deprecated public static final java.lang.String HTTP_SEPARATORS = " ,;=()<>@:\\\"/[]?{}\t";

/**
 * The value of the current header.
 * This is the header value that includes {@link #currentToken}.
 * Undefined if the iteration is over.
 */

@Deprecated protected java.lang.String currentHeader;

/**
 * The token to be returned by the next call to {@link #currentToken}.
 * <code>null</code> if the iteration is over.
 */

@Deprecated protected java.lang.String currentToken;

/** The iterator from which to obtain the next header. */

@Deprecated protected final org.apache.http.HeaderIterator headerIt;
{ headerIt = null; }

/**
 * The position after {@link #currentToken} in {@link #currentHeader}.
 * Undefined if the iteration is over.
 */

@Deprecated protected int searchPos;
}

