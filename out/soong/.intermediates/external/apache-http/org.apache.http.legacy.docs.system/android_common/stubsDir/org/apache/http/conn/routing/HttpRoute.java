/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/conn/routing/HttpRoute.java $
 * $Revision: 653041 $
 * $Date: 2008-05-03 03:39:28 -0700 (Sat, 03 May 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.conn.routing;


/**
 * The route for a request.
 * Instances of this class are unmodifiable and therefore suitable
 * for use as lookup keys.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version $Revision: 653041 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class HttpRoute implements org.apache.http.conn.routing.RouteInfo, java.lang.Cloneable {

/**
 * Creates a new route with all attributes specified explicitly.
 *
 * @param target    the host to which to route
 * @param local     the local address to route from, or
 *                  <code>null</code> for the default
 * @param proxies   the proxy chain to use, or
 *                  <code>null</code> for a direct route
 * @param secure    <code>true</code> if the route is (to be) secure,
 *                  <code>false</code> otherwise
 * @param tunnelled the tunnel type of this route
 * @param layered   the layering type of this route
 */

@Deprecated
public HttpRoute(org.apache.http.HttpHost target, java.net.InetAddress local, org.apache.http.HttpHost[] proxies, boolean secure, org.apache.http.conn.routing.RouteInfo.TunnelType tunnelled, org.apache.http.conn.routing.RouteInfo.LayerType layered) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new route with at most one proxy.
 *
 * @param target    the host to which to route
 * @param local     the local address to route from, or
 *                  <code>null</code> for the default
 * @param proxy     the proxy to use, or
 *                  <code>null</code> for a direct route
 * @param secure    <code>true</code> if the route is (to be) secure,
 *                  <code>false</code> otherwise
 * @param tunnelled <code>true</code> if the route is (to be) tunnelled
 *                  via the proxy,
 *                  <code>false</code> otherwise
 * @param layered   <code>true</code> if the route includes a
 *                  layered protocol,
 *                  <code>false</code> otherwise
 */

@Deprecated
public HttpRoute(org.apache.http.HttpHost target, java.net.InetAddress local, org.apache.http.HttpHost proxy, boolean secure, org.apache.http.conn.routing.RouteInfo.TunnelType tunnelled, org.apache.http.conn.routing.RouteInfo.LayerType layered) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new direct route.
 * That is a route without a proxy.
 *
 * @param target    the host to which to route
 * @param local     the local address to route from, or
 *                  <code>null</code> for the default
 * @param secure    <code>true</code> if the route is (to be) secure,
 *                  <code>false</code> otherwise
 */

@Deprecated
public HttpRoute(org.apache.http.HttpHost target, java.net.InetAddress local, boolean secure) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new direct insecure route.
 *
 * @param target    the host to which to route
 */

@Deprecated
public HttpRoute(org.apache.http.HttpHost target) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new route through a proxy.
 * When using this constructor, the <code>proxy</code> MUST be given.
 * For convenience, it is assumed that a secure connection will be
 * layered over a tunnel through the proxy.
 *
 * @param target    the host to which to route
 * @param local     the local address to route from, or
 *                  <code>null</code> for the default
 * @param proxy     the proxy to use
 * @param secure    <code>true</code> if the route is (to be) secure,
 *                  <code>false</code> otherwise
 */

@Deprecated
public HttpRoute(org.apache.http.HttpHost target, java.net.InetAddress local, org.apache.http.HttpHost proxy, boolean secure) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpHost getTargetHost() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.net.InetAddress getLocalAddress() { throw new RuntimeException("Stub!"); }

@Deprecated
public int getHopCount() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpHost getHopTarget(int hop) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpHost getProxyHost() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.conn.routing.RouteInfo.TunnelType getTunnelType() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isTunnelled() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.conn.routing.RouteInfo.LayerType getLayerType() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isLayered() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isSecure() { throw new RuntimeException("Stub!"); }

/**
 * Compares this route to another.
 *
 * @param o         the object to compare with
 *
 * @return  <code>true</code> if the argument is the same route,
 *          <code>false</code>
 */

@Deprecated
public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * Generates a hash code for this route.
 *
 * @return  the hash code
 */

@Deprecated
public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * Obtains a description of this route.
 *
 * @return  a human-readable representation of this route
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object clone() throws java.lang.CloneNotSupportedException { throw new RuntimeException("Stub!"); }
}

