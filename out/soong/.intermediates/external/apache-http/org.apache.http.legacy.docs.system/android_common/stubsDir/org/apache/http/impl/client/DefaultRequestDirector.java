/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/client/DefaultRequestDirector.java $
 * $Revision: 676023 $
 * $Date: 2008-07-11 09:40:56 -0700 (Fri, 11 Jul 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.client;

import org.apache.http.client.RequestDirector;
import org.apache.http.HttpException;
import java.io.IOException;
import org.apache.http.protocol.HTTP;

/**
 * Default implementation of {@link RequestDirector}.
 * <br/>
 * This class replaces the <code>HttpMethodDirector</code> in HttpClient 3.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version $Revision: 676023 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class DefaultRequestDirector implements org.apache.http.client.RequestDirector {

@Deprecated
public DefaultRequestDirector(org.apache.http.protocol.HttpRequestExecutor requestExec, org.apache.http.conn.ClientConnectionManager conman, org.apache.http.ConnectionReuseStrategy reustrat, org.apache.http.conn.ConnectionKeepAliveStrategy kastrat, org.apache.http.conn.routing.HttpRoutePlanner rouplan, org.apache.http.protocol.HttpProcessor httpProcessor, org.apache.http.client.HttpRequestRetryHandler retryHandler, org.apache.http.client.RedirectHandler redirectHandler, org.apache.http.client.AuthenticationHandler targetAuthHandler, org.apache.http.client.AuthenticationHandler proxyAuthHandler, org.apache.http.client.UserTokenHandler userTokenHandler, org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

@Deprecated
protected void rewriteRequestURI(org.apache.http.impl.client.RequestWrapper request, org.apache.http.conn.routing.HttpRoute route) throws org.apache.http.ProtocolException { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpResponse execute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.protocol.HttpContext context) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Returns the connection back to the connection manager
 * and prepares for retrieving a new connection during
 * the next request.
 */

@Deprecated
protected void releaseConnection() { throw new RuntimeException("Stub!"); }

/**
 * Determines the route for a request.
 * Called by {@link #execute}
 * to determine the route for either the original or a followup request.
 *
 * @param target    the target host for the request.
 *                  Implementations may accept <code>null</code>
 *                  if they can still determine a route, for example
 *                  to a default target or by inspecting the request.
 * @param request   the request to execute
 * @param context   the context to use for the execution,
 *                  never <code>null</code>
 *
 * @return  the route the request should take
 *
 * @throws HttpException    in case of a problem
 */

@Deprecated
protected org.apache.http.conn.routing.HttpRoute determineRoute(org.apache.http.HttpHost target, org.apache.http.HttpRequest request, org.apache.http.protocol.HttpContext context) throws org.apache.http.HttpException { throw new RuntimeException("Stub!"); }

/**
 * Establishes the target route.
 *
 * @param route     the route to establish
 * @param context   the context for the request execution
 *
 * @throws HttpException    in case of a problem
 * @throws IOException      in case of an IO problem
 */

@Deprecated
protected void establishRoute(org.apache.http.conn.routing.HttpRoute route, org.apache.http.protocol.HttpContext context) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Creates a tunnel to the target server.
 * The connection must be established to the (last) proxy.
 * A CONNECT request for tunnelling through the proxy will
 * be created and sent, the response received and checked.
 * This method does <i>not</i> update the connection with
 * information about the tunnel, that is left to the caller.
 *
 * @param route     the route to establish
 * @param context   the context for request execution
 *
 * @return  <code>true</code> if the tunnelled route is secure,
 *          <code>false</code> otherwise.
 *          The implementation here always returns <code>false</code>,
 *          but derived classes may override.
 *
 * @throws HttpException    in case of a problem
 * @throws IOException      in case of an IO problem
 */

@Deprecated
protected boolean createTunnelToTarget(org.apache.http.conn.routing.HttpRoute route, org.apache.http.protocol.HttpContext context) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Creates a tunnel to an intermediate proxy.
 * This method is <i>not</i> implemented in this class.
 * It just throws an exception here.
 *
 * @param route     the route to establish
 * @param hop       the hop in the route to establish now.
 *                  <code>route.getHopTarget(hop)</code>
 *                  will return the proxy to tunnel to.
 * @param context   the context for request execution
 *
 * @return  <code>true</code> if the partially tunnelled connection
 *          is secure, <code>false</code> otherwise.
 *
 * @throws HttpException    in case of a problem
 * @throws IOException      in case of an IO problem
 */

@Deprecated
protected boolean createTunnelToProxy(org.apache.http.conn.routing.HttpRoute route, int hop, org.apache.http.protocol.HttpContext context) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Creates the CONNECT request for tunnelling.
 * Called by {@link #createTunnelToTarget createTunnelToTarget}.
 *
 * @param route     the route to establish
 * @param context   the context for request execution
 *
 * @return  the CONNECT request for tunnelling
 */

@Deprecated
protected org.apache.http.HttpRequest createConnectRequest(org.apache.http.conn.routing.HttpRoute route, org.apache.http.protocol.HttpContext context) { throw new RuntimeException("Stub!"); }

/**
 * Analyzes a response to check need for a followup.
 *
 * @param roureq    the request and route.
 * @param response  the response to analayze
 * @param context   the context used for the current request execution
 *
 * @return  the followup request and route if there is a followup, or
 *          <code>null</code> if the response should be returned as is
 *
 * @throws HttpException    in case of a problem
 * @throws IOException      in case of an IO problem
 */

@Deprecated
protected org.apache.http.impl.client.RoutedRequest handleResponse(org.apache.http.impl.client.RoutedRequest roureq, org.apache.http.HttpResponse response, org.apache.http.protocol.HttpContext context) throws org.apache.http.HttpException, java.io.IOException { throw new RuntimeException("Stub!"); }

/** The connection manager. */

@Deprecated protected final org.apache.http.conn.ClientConnectionManager connManager;
{ connManager = null; }

/** The HTTP protocol processor. */

@Deprecated protected final org.apache.http.protocol.HttpProcessor httpProcessor;
{ httpProcessor = null; }

/** The keep-alive duration strategy. */

@Deprecated protected final org.apache.http.conn.ConnectionKeepAliveStrategy keepAliveStrategy;
{ keepAliveStrategy = null; }

/** The currently allocated connection. */

@Deprecated protected org.apache.http.conn.ManagedClientConnection managedConn;

/** The HTTP parameters. */

@Deprecated protected final org.apache.http.params.HttpParams params;
{ params = null; }

/** The redirect handler. */

@Deprecated protected final org.apache.http.client.RedirectHandler redirectHandler;
{ redirectHandler = null; }

/** The request executor. */

@Deprecated protected final org.apache.http.protocol.HttpRequestExecutor requestExec;
{ requestExec = null; }

/** The request retry handler. */

@Deprecated protected final org.apache.http.client.HttpRequestRetryHandler retryHandler;
{ retryHandler = null; }

/** The connection re-use strategy. */

@Deprecated protected final org.apache.http.ConnectionReuseStrategy reuseStrategy;
{ reuseStrategy = null; }

/** The route planner. */

@Deprecated protected final org.apache.http.conn.routing.HttpRoutePlanner routePlanner;
{ routePlanner = null; }
}

