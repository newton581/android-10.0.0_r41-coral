/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.logging.impl;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogConfigurationException;
import java.lang.reflect.Constructor;

/**
 * <p>Concrete subclass of {@link LogFactory} that implements the
 * following algorithm to dynamically select a logging implementation
 * class to instantiate a wrapper for.</p>
 * <ul>
 * <li>Use a factory configuration attribute named
 *     <code>org.apache.commons.logging.Log</code> to identify the
 *     requested implementation class.</li>
 * <li>Use the <code>org.apache.commons.logging.Log</code> system property
 *     to identify the requested implementation class.</li>
 * <li>If <em>Log4J</em> is available, return an instance of
 *     <code>org.apache.commons.logging.impl.Log4JLogger</code>.</li>
 * <li>If <em>JDK 1.4 or later</em> is available, return an instance of
 *     <code>org.apache.commons.logging.impl.Jdk14Logger</code>.</li>
 * <li>Otherwise, return an instance of
 *     <code>org.apache.commons.logging.impl.SimpleLog</code>.</li>
 * </ul>
 *
 * <p>If the selected {@link Log} implementation class has a
 * <code>setLogFactory()</code> method that accepts a {@link LogFactory}
 * parameter, this method will be called on each newly created instance
 * to identify the associated factory.  This makes factory configuration
 * attributes available to the Log instance, if it so desires.</p>
 *
 * <p>This factory will remember previously created <code>Log</code> instances
 * for the same name, and will return them on repeated requests to the
 * <code>getInstance()</code> method.</p>
 *
 * @author Rod Waldhoff
 * @author Craig R. McClanahan
 * @author Richard A. Sitze
 * @author Brian Stansberry
 * @version $Revision: 399224 $ $Date: 2006-05-03 10:25:54 +0100 (Wed, 03 May 2006) $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class LogFactoryImpl extends org.apache.commons.logging.LogFactory {

/**
 * Public no-arguments constructor required by the lookup mechanism.
 */

@Deprecated
public LogFactoryImpl() { throw new RuntimeException("Stub!"); }

/**
 * Return the configuration attribute with the specified name (if any),
 * or <code>null</code> if there is no such attribute.
 *
 * @param name Name of the attribute to return
 */

@Deprecated
public java.lang.Object getAttribute(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Return an array containing the names of all currently defined
 * configuration attributes.  If there are no such attributes, a zero
 * length array is returned.
 */

@Deprecated
public java.lang.String[] getAttributeNames() { throw new RuntimeException("Stub!"); }

/**
 * Convenience method to derive a name from the specified class and
 * call <code>getInstance(String)</code> with it.
 *
 * @param clazz Class for which a suitable Log name will be derived
 *
 * @exception LogConfigurationException if a suitable <code>Log</code>
 *  instance cannot be returned
 */

@Deprecated
public org.apache.commons.logging.Log getInstance(java.lang.Class clazz) throws org.apache.commons.logging.LogConfigurationException { throw new RuntimeException("Stub!"); }

/**
 * <p>Construct (if necessary) and return a <code>Log</code> instance,
 * using the factory's current set of configuration attributes.</p>
 *
 * <p><strong>NOTE</strong> - Depending upon the implementation of
 * the <code>LogFactory</code> you are using, the <code>Log</code>
 * instance you are returned may or may not be local to the current
 * application, and may or may not be returned again on a subsequent
 * call with the same name argument.</p>
 *
 * @param name Logical name of the <code>Log</code> instance to be
 *  returned (the meaning of this name is only known to the underlying
 *  logging implementation that is being wrapped)
 *
 * @exception LogConfigurationException if a suitable <code>Log</code>
 *  instance cannot be returned
 */

@Deprecated
public org.apache.commons.logging.Log getInstance(java.lang.String name) throws org.apache.commons.logging.LogConfigurationException { throw new RuntimeException("Stub!"); }

/**
 * Release any internal references to previously created
 * {@link org.apache.commons.logging.Log}
 * instances returned by this factory.  This is useful in environments
 * like servlet containers, which implement application reloading by
 * throwing away a ClassLoader.  Dangling references to objects in that
 * class loader would prevent garbage collection.
 */

@Deprecated
public void release() { throw new RuntimeException("Stub!"); }

/**
 * Remove any configuration attribute associated with the specified name.
 * If there is no such attribute, no action is taken.
 *
 * @param name Name of the attribute to remove
 */

@Deprecated
public void removeAttribute(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Set the configuration attribute with the specified name.  Calling
 * this with a <code>null</code> value is equivalent to calling
 * <code>removeAttribute(name)</code>.
 * <p>
 * This method can be used to set logging configuration programmatically
 * rather than via system properties. It can also be used in code running
 * within a container (such as a webapp) to configure behaviour on a
 * per-component level instead of globally as system properties would do.
 * To use this method instead of a system property, call
 * <pre>
 * LogFactory.getFactory().setAttribute(...)
 * </pre>
 * This must be done before the first Log object is created; configuration
 * changes after that point will be ignored.
 * <p>
 * This method is also called automatically if LogFactory detects a
 * commons-logging.properties file; every entry in that file is set
 * automatically as an attribute here.
 *
 * @param name Name of the attribute to set
 * @param value Value of the attribute to set, or <code>null</code>
 *  to remove any setting for this attribute
 */

@Deprecated
public void setAttribute(java.lang.String name, java.lang.Object value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the context classloader.
 * This method is a workaround for a java 1.2 compiler bug.
 * @since 1.1
 */

@Deprecated
protected static java.lang.ClassLoader getContextClassLoader() throws org.apache.commons.logging.LogConfigurationException { throw new RuntimeException("Stub!"); }

/**
 * Workaround for bug in Java1.2; in theory this method is not needed.
 * See LogFactory.isDiagnosticsEnabled.
 */

@Deprecated
protected static boolean isDiagnosticsEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Workaround for bug in Java1.2; in theory this method is not needed.
 * See LogFactory.getClassLoader.
 * @since 1.1
 */

@Deprecated
protected static java.lang.ClassLoader getClassLoader(java.lang.Class clazz) { throw new RuntimeException("Stub!"); }

/**
 * Output a diagnostic message to a user-specified destination (if the
 * user has enabled diagnostic logging).
 *
 * @param msg diagnostic message
 * @since 1.1
 */

@Deprecated
protected void logDiagnostic(java.lang.String msg) { throw new RuntimeException("Stub!"); }

/**
 * Return the fully qualified Java classname of the {@link Log}
 * implementation we will be using.
 *
 * @deprecated  Never invoked by this class; subclasses should not assume
 *              it will be.
 */

@Deprecated
protected java.lang.String getLogClassName() { throw new RuntimeException("Stub!"); }

/**
 * <p>Return the <code>Constructor</code> that can be called to instantiate
 * new {@link org.apache.commons.logging.Log} instances.</p>
 *
 * <p><strong>IMPLEMENTATION NOTE</strong> - Race conditions caused by
 * calling this method from more than one thread are ignored, because
 * the same <code>Constructor</code> instance will ultimately be derived
 * in all circumstances.</p>
 *
 * @exception LogConfigurationException if a suitable constructor
 *  cannot be returned
 *
 * @deprecated  Never invoked by this class; subclasses should not assume
 *              it will be.
 */

@Deprecated
protected java.lang.reflect.Constructor getLogConstructor() throws org.apache.commons.logging.LogConfigurationException { throw new RuntimeException("Stub!"); }

/**
 * Is <em>JDK 1.3 with Lumberjack</em> logging available?
 *
 * @deprecated  Never invoked by this class; subclasses should not assume
 *              it will be.
 */

@Deprecated
protected boolean isJdk13LumberjackAvailable() { throw new RuntimeException("Stub!"); }

/**
 * <p>Return <code>true</code> if <em>JDK 1.4 or later</em> logging
 * is available.  Also checks that the <code>Throwable</code> class
 * supports <code>getStackTrace()</code>, which is required by
 * Jdk14Logger.</p>
 *
 * @deprecated  Never invoked by this class; subclasses should not assume
 *              it will be.
 */

@Deprecated
protected boolean isJdk14Available() { throw new RuntimeException("Stub!"); }

/**
 * Is a <em>Log4J</em> implementation available?
 *
 * @deprecated  Never invoked by this class; subclasses should not assume
 *              it will be.
 */

@Deprecated
protected boolean isLog4JAvailable() { throw new RuntimeException("Stub!"); }

/**
 * Create and return a new {@link org.apache.commons.logging.Log}
 * instance for the specified name.
 *
 * @param name Name of the new logger
 *
 * @exception LogConfigurationException if a new instance cannot
 *  be created
 */

@Deprecated
protected org.apache.commons.logging.Log newInstance(java.lang.String name) throws org.apache.commons.logging.LogConfigurationException { throw new RuntimeException("Stub!"); }

/**
 * The name (<code>org.apache.commons.logging.Log.allowFlawedContext</code>)
 * of the system property which can be set true/false to
 * determine system behaviour when a bad context-classloader is encountered.
 * When set to false, a LogConfigurationException is thrown if
 * LogFactoryImpl is loaded via a child classloader of the TCCL (this
 * should never happen in sane systems).
 *
 * Default behaviour: true (tolerates bad context classloaders)
 *
 * See also method setAttribute.
 */

@Deprecated public static final java.lang.String ALLOW_FLAWED_CONTEXT_PROPERTY = "org.apache.commons.logging.Log.allowFlawedContext";

/**
 * The name (<code>org.apache.commons.logging.Log.allowFlawedDiscovery</code>)
 * of the system property which can be set true/false to
 * determine system behaviour when a bad logging adapter class is
 * encountered during logging discovery. When set to false, an
 * exception will be thrown and the app will fail to start. When set
 * to true, discovery will continue (though the user might end up
 * with a different logging implementation than they expected).
 *
 * Default behaviour: true (tolerates bad logging adapters)
 *
 * See also method setAttribute.
 */

@Deprecated public static final java.lang.String ALLOW_FLAWED_DISCOVERY_PROPERTY = "org.apache.commons.logging.Log.allowFlawedDiscovery";

/**
 * The name (<code>org.apache.commons.logging.Log.allowFlawedHierarchy</code>)
 * of the system property which can be set true/false to
 * determine system behaviour when a logging adapter class is
 * encountered which has bound to the wrong Log class implementation.
 * When set to false, an exception will be thrown and the app will fail
 * to start. When set to true, discovery will continue (though the user
 * might end up with a different logging implementation than they expected).
 *
 * Default behaviour: true (tolerates bad Log class hierarchy)
 *
 * See also method setAttribute.
 */

@Deprecated public static final java.lang.String ALLOW_FLAWED_HIERARCHY_PROPERTY = "org.apache.commons.logging.Log.allowFlawedHierarchy";

/**
 * The name (<code>org.apache.commons.logging.Log</code>) of the system
 * property identifying our {@link Log} implementation class.
 */

@Deprecated public static final java.lang.String LOG_PROPERTY = "org.apache.commons.logging.Log";

/**
 * The deprecated system property used for backwards compatibility with
 * old versions of JCL.
 */

@Deprecated protected static final java.lang.String LOG_PROPERTY_OLD = "org.apache.commons.logging.log";

/**
 * Configuration attributes.
 */

@Deprecated protected java.util.Hashtable attributes;

/**
 * The {@link org.apache.commons.logging.Log} instances that have
 * already been created, keyed by logger name.
 */

@Deprecated protected java.util.Hashtable instances;

/**
 * The one-argument constructor of the
 * {@link org.apache.commons.logging.Log}
 * implementation class that will be used to create new instances.
 * This value is initialized by <code>getLogConstructor()</code>,
 * and then returned repeatedly.
 */

@Deprecated protected java.lang.reflect.Constructor logConstructor;

/**
 * The signature of the Constructor to be used.
 */

@Deprecated protected java.lang.Class[] logConstructorSignature;

/**
 * The one-argument <code>setLogFactory</code> method of the selected
 * {@link org.apache.commons.logging.Log} method, if it exists.
 */

@Deprecated protected java.lang.reflect.Method logMethod;

/**
 * The signature of the <code>setLogFactory</code> method to be used.
 */

@Deprecated protected java.lang.Class[] logMethodSignature;
}

