/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/params/DefaultedHttpParams.java $
 * $Revision: 610763 $
 * $Date: 2008-01-10 04:01:13 -0800 (Thu, 10 Jan 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.params;

import org.apache.http.params.HttpParams;

/**
 * {@link HttpParams} implementation that delegates resolution of a parameter
 * to the given default {@link HttpParams} instance if the parameter is not
 * present in the local one. The state of the local collection can be mutated,
 * whereas the default collection is treated as read-only.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 610763 $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class DefaultedHttpParams extends org.apache.http.params.AbstractHttpParams {

@Deprecated
public DefaultedHttpParams(org.apache.http.params.HttpParams local, org.apache.http.params.HttpParams defaults) { throw new RuntimeException("Stub!"); }

/**
 * Creates a copy of the local collection with the same default
 */

@Deprecated
public org.apache.http.params.HttpParams copy() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the value of the parameter from the local collection and, if the
 * parameter is not set locally, delegates its resolution to the default
 * collection.
 */

@Deprecated
public java.lang.Object getParameter(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Attempts to remove the parameter from the local collection. This method
 * <i>does not</i> modify the default collection.
 */

@Deprecated
public boolean removeParameter(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Sets the parameter in the local collection. This method <i>does not</i>
 * modify the default collection.
 */

@Deprecated
public org.apache.http.params.HttpParams setParameter(java.lang.String name, java.lang.Object value) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.params.HttpParams getDefaults() { throw new RuntimeException("Stub!"); }
}

