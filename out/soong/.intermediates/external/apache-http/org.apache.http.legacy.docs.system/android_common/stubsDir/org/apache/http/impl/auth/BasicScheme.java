/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/auth/BasicScheme.java $
 * $Revision: 658430 $
 * $Date: 2008-05-20 14:04:27 -0700 (Tue, 20 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.auth;

import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.Credentials;

/**
 * <p>
 * Basic authentication scheme as defined in RFC 2617.
 * </p>
 *
 * @author <a href="mailto:remm@apache.org">Remy Maucherat</a>
 * @author Rodney Waldhoff
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 * @author Ortwin Glueck
 * @author Sean C. Sullivan
 * @author <a href="mailto:adrian@ephox.com">Adrian Sutton</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class BasicScheme extends org.apache.http.impl.auth.RFC2617Scheme {

/**
 * Default constructor for the basic authetication scheme.
 */

@Deprecated
public BasicScheme() { throw new RuntimeException("Stub!"); }

/**
 * Returns textual designation of the basic authentication scheme.
 *
 * @return <code>basic</code>
 */

@Deprecated
public java.lang.String getSchemeName() { throw new RuntimeException("Stub!"); }

/**
 * Processes the Basic challenge.
 *
 * @param header the challenge header
 *
 * @throws MalformedChallengeException is thrown if the authentication challenge
 * is malformed
 */

@Deprecated
public void processChallenge(org.apache.http.Header header) throws org.apache.http.auth.MalformedChallengeException { throw new RuntimeException("Stub!"); }

/**
 * Tests if the Basic authentication process has been completed.
 *
 * @return <tt>true</tt> if Basic authorization has been processed,
 *   <tt>false</tt> otherwise.
 */

@Deprecated
public boolean isComplete() { throw new RuntimeException("Stub!"); }

/**
 * Returns <tt>false</tt>. Basic authentication scheme is request based.
 *
 * @return <tt>false</tt>.
 */

@Deprecated
public boolean isConnectionBased() { throw new RuntimeException("Stub!"); }

/**
 * Produces basic authorization header for the given set of {@link Credentials}.
 *
 * @param credentials The set of credentials to be used for athentication
 * @param request The request being authenticated
 * @throws org.apache.http.auth.InvalidCredentialsException if authentication credentials
 *         are not valid or not applicable for this authentication scheme
 * @throws AuthenticationException if authorization string cannot
 *   be generated due to an authentication failure
 *
 * @return a basic authorization string
 */

@Deprecated
public org.apache.http.Header authenticate(org.apache.http.auth.Credentials credentials, org.apache.http.HttpRequest request) throws org.apache.http.auth.AuthenticationException { throw new RuntimeException("Stub!"); }

/**
 * Returns a basic <tt>Authorization</tt> header value for the given
 * {@link Credentials} and charset.
 *
 * @param credentials The credentials to encode.
 * @param charset The charset to use for encoding the credentials
 *
 * @return a basic authorization header
 */

@Deprecated
public static org.apache.http.Header authenticate(org.apache.http.auth.Credentials credentials, java.lang.String charset, boolean proxy) { throw new RuntimeException("Stub!"); }
}

