/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.binary;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;

/**
 * Hex encoder and decoder.
 *
 * @since 1.1
 * @author Apache Software Foundation
 * @version $Id: Hex.java,v 1.13 2004/04/18 18:22:33 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class Hex implements org.apache.commons.codec.BinaryEncoder, org.apache.commons.codec.BinaryDecoder {

@Deprecated
public Hex() { throw new RuntimeException("Stub!"); }

/**
 * Converts an array of characters representing hexidecimal values into an
 * array of bytes of those same values. The returned array will be half the
 * length of the passed array, as it takes two characters to represent any
 * given byte. An exception is thrown if the passed char array has an odd
 * number of elements.
 *
 * @param data An array of characters containing hexidecimal digits
 * @return A byte array containing binary data decoded from
 *         the supplied char array.
 * @throws DecoderException Thrown if an odd number or illegal of characters
 *         is supplied
 */

@Deprecated
public static byte[] decodeHex(char[] data) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Converts a hexadecimal character to an integer.
 *
 * @param ch A character to convert to an integer digit
 * @param index The index of the character in the source
 * @return An integer
 * @throws DecoderException Thrown if ch is an illegal hex character
 */

@Deprecated
protected static int toDigit(char ch, int index) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Converts an array of bytes into an array of characters representing the hexidecimal values of each byte in order.
 * The returned array will be double the length of the passed array, as it takes two characters to represent any
 * given byte.
 *
 * @param data
 *                  a byte[] to convert to Hex characters
 * @return A char[] containing hexidecimal characters
 */

@Deprecated
public static char[] encodeHex(byte[] data) { throw new RuntimeException("Stub!"); }

/**
 * Converts an array of character bytes representing hexidecimal values into an
 * array of bytes of those same values. The returned array will be half the
 * length of the passed array, as it takes two characters to represent any
 * given byte. An exception is thrown if the passed char array has an odd
 * number of elements.
 *
 * @param array An array of character bytes containing hexidecimal digits
 * @return A byte array containing binary data decoded from
 *         the supplied byte array (representing characters).
 * @throws DecoderException Thrown if an odd number of characters is supplied
 *                   to this function
 * @see #decodeHex(char[])
 */

@Deprecated
public byte[] decode(byte[] array) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Converts a String or an array of character bytes representing hexidecimal values into an
 * array of bytes of those same values. The returned array will be half the
 * length of the passed String or array, as it takes two characters to represent any
 * given byte. An exception is thrown if the passed char array has an odd
 * number of elements.
 *
 * @param object A String or, an array of character bytes containing hexidecimal digits
 * @return A byte array containing binary data decoded from
 *         the supplied byte array (representing characters).
 * @throws DecoderException Thrown if an odd number of characters is supplied
 *                   to this function or the object is not a String or char[]
 * @see #decodeHex(char[])
 */

@Deprecated
public java.lang.Object decode(java.lang.Object object) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Converts an array of bytes into an array of bytes for the characters representing the
 * hexidecimal values of each byte in order. The returned array will be
 * double the length of the passed array, as it takes two characters to
 * represent any given byte.
 *
 * @param array a byte[] to convert to Hex characters
 * @return A byte[] containing the bytes of the hexidecimal characters
 * @see #encodeHex(byte[])
 */

@Deprecated
public byte[] encode(byte[] array) { throw new RuntimeException("Stub!"); }

/**
 * Converts a String or an array of bytes into an array of characters representing the
 * hexidecimal values of each byte in order. The returned array will be
 * double the length of the passed String or array, as it takes two characters to
 * represent any given byte.
 *
 * @param object a String, or byte[] to convert to Hex characters
 * @return A char[] containing hexidecimal characters
 * @throws EncoderException Thrown if the given object is not a String or byte[]
 * @see #encodeHex(byte[])
 */

@Deprecated
public java.lang.Object encode(java.lang.Object object) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }
}

