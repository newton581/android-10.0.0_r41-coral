/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/conn/routing/RouteTracker.java $
 * $Revision: 620254 $
 * $Date: 2008-02-10 02:18:48 -0800 (Sun, 10 Feb 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.conn.routing;


/**
 * Helps tracking the steps in establishing a route.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version $Revision: 620254 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class RouteTracker implements org.apache.http.conn.routing.RouteInfo, java.lang.Cloneable {

/**
 * Creates a new route tracker.
 * The target and origin need to be specified at creation time.
 *
 * @param target    the host to which to route
 * @param local     the local address to route from, or
 *                  <code>null</code> for the default
 */

@Deprecated
public RouteTracker(org.apache.http.HttpHost target, java.net.InetAddress local) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new tracker for the given route.
 * Only target and origin are taken from the route,
 * everything else remains to be tracked.
 *
 * @param route     the route to track
 */

@Deprecated
public RouteTracker(org.apache.http.conn.routing.HttpRoute route) { throw new RuntimeException("Stub!"); }

/**
 * Tracks connecting to the target.
 *
 * @param secure    <code>true</code> if the route is secure,
 *                  <code>false</code> otherwise
 */

@Deprecated
public void connectTarget(boolean secure) { throw new RuntimeException("Stub!"); }

/**
 * Tracks connecting to the first proxy.
 *
 * @param proxy     the proxy connected to
 * @param secure    <code>true</code> if the route is secure,
 *                  <code>false</code> otherwise
 */

@Deprecated
public void connectProxy(org.apache.http.HttpHost proxy, boolean secure) { throw new RuntimeException("Stub!"); }

/**
 * Tracks tunnelling to the target.
 *
 * @param secure    <code>true</code> if the route is secure,
 *                  <code>false</code> otherwise
 */

@Deprecated
public void tunnelTarget(boolean secure) { throw new RuntimeException("Stub!"); }

/**
 * Tracks tunnelling to a proxy in a proxy chain.
 * This will extend the tracked proxy chain, but it does not mark
 * the route as tunnelled. Only end-to-end tunnels are considered there.
 *
 * @param proxy     the proxy tunnelled to
 * @param secure    <code>true</code> if the route is secure,
 *                  <code>false</code> otherwise
 */

@Deprecated
public void tunnelProxy(org.apache.http.HttpHost proxy, boolean secure) { throw new RuntimeException("Stub!"); }

/**
 * Tracks layering a protocol.
 *
 * @param secure    <code>true</code> if the route is secure,
 *                  <code>false</code> otherwise
 */

@Deprecated
public void layerProtocol(boolean secure) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpHost getTargetHost() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.net.InetAddress getLocalAddress() { throw new RuntimeException("Stub!"); }

@Deprecated
public int getHopCount() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpHost getHopTarget(int hop) { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.HttpHost getProxyHost() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isConnected() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.conn.routing.RouteInfo.TunnelType getTunnelType() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isTunnelled() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.conn.routing.RouteInfo.LayerType getLayerType() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isLayered() { throw new RuntimeException("Stub!"); }

@Deprecated
public boolean isSecure() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the tracked route.
 * If a route has been tracked, it is {@link #isConnected connected}.
 * If not connected, nothing has been tracked so far.
 *
 * @return  the tracked route, or
 *          <code>null</code> if nothing has been tracked so far
 */

@Deprecated
public org.apache.http.conn.routing.HttpRoute toRoute() { throw new RuntimeException("Stub!"); }

/**
 * Compares this tracked route to another.
 *
 * @param o         the object to compare with
 *
 * @return  <code>true</code> if the argument is the same tracked route,
 *          <code>false</code>
 */

@Deprecated
public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * Generates a hash code for this tracked route.
 * Route trackers are modifiable and should therefore not be used
 * as lookup keys. Use {@link #toRoute toRoute} to obtain an
 * unmodifiable representation of the tracked route.
 *
 * @return  the hash code
 */

@Deprecated
public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * Obtains a description of the tracked route.
 *
 * @return  a human-readable representation of the tracked route
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object clone() throws java.lang.CloneNotSupportedException { throw new RuntimeException("Stub!"); }
}

