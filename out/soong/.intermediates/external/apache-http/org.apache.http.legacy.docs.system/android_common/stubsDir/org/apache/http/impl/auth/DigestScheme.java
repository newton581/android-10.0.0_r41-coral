/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/auth/DigestScheme.java $
 * $Revision: 659595 $
 * $Date: 2008-05-23 09:47:14 -0700 (Fri, 23 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.auth;

import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.Credentials;

/**
 * <p>
 * Digest authentication scheme as defined in RFC 2617.
 * Both MD5 (default) and MD5-sess are supported.
 * Currently only qop=auth or no qop is supported. qop=auth-int
 * is unsupported. If auth and auth-int are provided, auth is
 * used.
 * </p>
 * <p>
 * Credential charset is configured via the
 * {@link org.apache.http.auth.params.AuthPNames#CREDENTIAL_CHARSET
 *        credential charset} parameter.
 * Since the digest username is included as clear text in the generated
 * Authentication header, the charset of the username must be compatible
 * with the
 * {@link org.apache.http.params.CoreProtocolPNames#HTTP_ELEMENT_CHARSET
 *        http element charset}.
 * </p>
 *
 * @author <a href="mailto:remm@apache.org">Remy Maucherat</a>
 * @author Rodney Waldhoff
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 * @author Ortwin Glueck
 * @author Sean C. Sullivan
 * @author <a href="mailto:adrian@ephox.com">Adrian Sutton</a>
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class DigestScheme extends org.apache.http.impl.auth.RFC2617Scheme {

/**
 * Default constructor for the digest authetication scheme.
 */

@Deprecated
public DigestScheme() { throw new RuntimeException("Stub!"); }

/**
 * Processes the Digest challenge.
 *
 * @param header the challenge header
 *
 * @throws MalformedChallengeException is thrown if the authentication challenge
 * is malformed
 */

@Deprecated
public void processChallenge(org.apache.http.Header header) throws org.apache.http.auth.MalformedChallengeException { throw new RuntimeException("Stub!"); }

/**
 * Tests if the Digest authentication process has been completed.
 *
 * @return <tt>true</tt> if Digest authorization has been processed,
 *   <tt>false</tt> otherwise.
 */

@Deprecated
public boolean isComplete() { throw new RuntimeException("Stub!"); }

/**
 * Returns textual designation of the digest authentication scheme.
 *
 * @return <code>digest</code>
 */

@Deprecated
public java.lang.String getSchemeName() { throw new RuntimeException("Stub!"); }

/**
 * Returns <tt>false</tt>. Digest authentication scheme is request based.
 *
 * @return <tt>false</tt>.
 */

@Deprecated
public boolean isConnectionBased() { throw new RuntimeException("Stub!"); }

@Deprecated
public void overrideParamter(java.lang.String name, java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Produces a digest authorization string for the given set of
 * {@link Credentials}, method name and URI.
 *
 * @param credentials A set of credentials to be used for athentication
 * @param request    The request being authenticated
 *
 * @throws org.apache.http.auth.InvalidCredentialsException if authentication credentials
 *         are not valid or not applicable for this authentication scheme
 * @throws AuthenticationException if authorization string cannot
 *   be generated due to an authentication failure
 *
 * @return a digest authorization string
 */

@Deprecated
public org.apache.http.Header authenticate(org.apache.http.auth.Credentials credentials, org.apache.http.HttpRequest request) throws org.apache.http.auth.AuthenticationException { throw new RuntimeException("Stub!"); }

/**
 * Creates a random cnonce value based on the current time.
 *
 * @return The cnonce value as String.
 * @throws UnsupportedDigestAlgorithmException if MD5 algorithm is not supported.
 */

@Deprecated
public static java.lang.String createCnonce() { throw new RuntimeException("Stub!"); }
}

