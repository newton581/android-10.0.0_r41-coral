/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/SingleClientConnManager.java $
 * $Revision: 673450 $
 * $Date: 2008-07-02 10:35:05 -0700 (Wed, 02 Jul 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn;


/**
 * A connection "manager" for a single connection.
 * This manager is good only for single-threaded use.
 * Allocation <i>always</i> returns the connection immediately,
 * even if it has not been released after the previous allocation.
 * In that case, a {@link #MISUSE_MESSAGE warning} is logged
 * and the previously issued connection is revoked.
 * <p>
 * This class is derived from <code>SimpleHttpConnectionManager</code>
 * in HttpClient 3. See there for original authors.
 * </p>
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 * @author <a href="mailto:becke@u.washington.edu">Michael Becke</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version   $Revision: 673450 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class SingleClientConnManager implements org.apache.http.conn.ClientConnectionManager {

/**
 * Creates a new simple connection manager.
 *
 * @param params    the parameters for this manager
 * @param schreg    the scheme registry
 */

@Deprecated
public SingleClientConnManager(org.apache.http.params.HttpParams params, org.apache.http.conn.scheme.SchemeRegistry schreg) { throw new RuntimeException("Stub!"); }

@Deprecated
protected void finalize() throws java.lang.Throwable { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.conn.scheme.SchemeRegistry getSchemeRegistry() { throw new RuntimeException("Stub!"); }

/**
 * Hook for creating the connection operator.
 * It is called by the constructor.
 * Derived classes can override this method to change the
 * instantiation of the operator.
 * The default implementation here instantiates
 * {@link DefaultClientConnectionOperator DefaultClientConnectionOperator}.
 *
 * @param schreg    the scheme registry to use, or <code>null</code>
 *
 * @return  the connection operator to use
 */

@Deprecated
protected org.apache.http.conn.ClientConnectionOperator createConnectionOperator(org.apache.http.conn.scheme.SchemeRegistry schreg) { throw new RuntimeException("Stub!"); }

/**
 * Asserts that this manager is not shut down.
 *
 * @throws IllegalStateException    if this manager is shut down
 */

@Deprecated
protected final void assertStillUp() throws java.lang.IllegalStateException { throw new RuntimeException("Stub!"); }

@Deprecated
public final org.apache.http.conn.ClientConnectionRequest requestConnection(org.apache.http.conn.routing.HttpRoute route, java.lang.Object state) { throw new RuntimeException("Stub!"); }

/**
 * Obtains a connection.
 * This method does not block.
 *
 * @param route     where the connection should point to
 *
 * @return  a connection that can be used to communicate
 *          along the given route
 */

@Deprecated
public org.apache.http.conn.ManagedClientConnection getConnection(org.apache.http.conn.routing.HttpRoute route, java.lang.Object state) { throw new RuntimeException("Stub!"); }

@Deprecated
public void releaseConnection(org.apache.http.conn.ManagedClientConnection conn, long validDuration, java.util.concurrent.TimeUnit timeUnit) { throw new RuntimeException("Stub!"); }

@Deprecated
public void closeExpiredConnections() { throw new RuntimeException("Stub!"); }

@Deprecated
public void closeIdleConnections(long idletime, java.util.concurrent.TimeUnit tunit) { throw new RuntimeException("Stub!"); }

@Deprecated
public void shutdown() { throw new RuntimeException("Stub!"); }

/**
 * Revokes the currently issued connection.
 * The adapter gets disconnected, the connection will be shut down.
 */

@Deprecated
protected void revokeConnection() { throw new RuntimeException("Stub!"); }

/** The message to be logged on multiple allocation. */

@Deprecated public static final java.lang.String MISUSE_MESSAGE = "Invalid use of SingleClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.";

/** Whether the connection should be shut down  on release. */

@Deprecated protected boolean alwaysShutDown;

/** The operator for opening and updating connections. */

@Deprecated protected org.apache.http.conn.ClientConnectionOperator connOperator;

/** The time the last released connection expires and shouldn't be reused. */

@Deprecated protected long connectionExpiresTime;

/** Indicates whether this connection manager is shut down. */

@Deprecated protected volatile boolean isShutDown;

/** The time of the last connection release, or -1. */

@Deprecated protected long lastReleaseTime;

/** The currently issued managed connection, if any. */

@Deprecated protected org.apache.http.impl.conn.SingleClientConnManager.ConnAdapter managedConn;

/** The schemes supported by this connection manager. */

@Deprecated protected org.apache.http.conn.scheme.SchemeRegistry schemeRegistry;

/** The one and only entry in this pool. */

@Deprecated protected org.apache.http.impl.conn.SingleClientConnManager.PoolEntry uniquePoolEntry;
/**
 * The connection adapter used by this manager.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
protected class ConnAdapter extends org.apache.http.impl.conn.AbstractPooledConnAdapter {

/**
 * Creates a new connection adapter.
 *
 * @param entry   the pool entry for the connection being wrapped
 * @param route   the planned route for this connection
 */

@Deprecated
protected ConnAdapter(org.apache.http.impl.conn.SingleClientConnManager.PoolEntry entry, org.apache.http.conn.routing.HttpRoute route) { super(null, null); throw new RuntimeException("Stub!"); }
}

/**
 * The pool entry for this connection manager.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
protected class PoolEntry extends org.apache.http.impl.conn.AbstractPoolEntry {

/**
 * Creates a new pool entry.
 *
 */

@Deprecated
protected PoolEntry() { super(null, null); throw new RuntimeException("Stub!"); }

/**
 * Closes the connection in this pool entry.
 */

@Deprecated
protected void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Shuts down the connection in this pool entry.
 */

@Deprecated
protected void shutdown() throws java.io.IOException { throw new RuntimeException("Stub!"); }
}

}

