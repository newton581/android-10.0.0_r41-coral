/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.language;

import org.apache.commons.codec.EncoderException;

/**
 * Encodes a string into a Refined Soundex value. A refined soundex code is
 * optimized for spell checking words. Soundex method originally developed by
 * <CITE>Margaret Odell</CITE> and <CITE>Robert Russell</CITE>.
 *
 * @author Apache Software Foundation
 * @version $Id: RefinedSoundex.java,v 1.21 2004/06/05 18:32:04 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class RefinedSoundex implements org.apache.commons.codec.StringEncoder {

/**
 * Creates an instance of the RefinedSoundex object using the default US
 * English mapping.
 */

@Deprecated
public RefinedSoundex() { throw new RuntimeException("Stub!"); }

/**
 * Creates a refined soundex instance using a custom mapping. This
 * constructor can be used to customize the mapping, and/or possibly
 * provide an internationalized mapping for a non-Western character set.
 *
 * @param mapping
 *                  Mapping array to use when finding the corresponding code for
 *                  a given character
 */

@Deprecated
public RefinedSoundex(char[] mapping) { throw new RuntimeException("Stub!"); }

/**
 * Returns the number of characters in the two encoded Strings that are the
 * same. This return value ranges from 0 to the length of the shortest
 * encoded String: 0 indicates little or no similarity, and 4 out of 4 (for
 * example) indicates strong similarity or identical values. For refined
 * Soundex, the return value can be greater than 4.
 *
 * @param s1
 *                  A String that will be encoded and compared.
 * @param s2
 *                  A String that will be encoded and compared.
 * @return The number of characters in the two encoded Strings that are the
 *             same from 0 to to the length of the shortest encoded String.
 *
 * @see <a href="http://msdn.microsoft.com/library/default.asp?url=/library/en-us/tsqlref/ts_de-dz_8co5.asp">
 *          MS T-SQL DIFFERENCE</a>
 *
 * @throws EncoderException
 *                  if an error occurs encoding one of the strings
 * @since 1.3
 */

@Deprecated
public int difference(java.lang.String s1, java.lang.String s2) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes an Object using the refined soundex algorithm. This method is
 * provided in order to satisfy the requirements of the Encoder interface,
 * and will throw an EncoderException if the supplied object is not of type
 * java.lang.String.
 *
 * @param pObject
 *                  Object to encode
 * @return An object (or type java.lang.String) containing the refined
 *             soundex code which corresponds to the String supplied.
 * @throws EncoderException
 *                  if the parameter supplied is not of type java.lang.String
 */

@Deprecated
public java.lang.Object encode(java.lang.Object pObject) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a String using the refined soundex algorithm.
 *
 * @param pString
 *                  A String object to encode
 * @return A Soundex code corresponding to the String supplied
 */

@Deprecated
public java.lang.String encode(java.lang.String pString) { throw new RuntimeException("Stub!"); }

/**
 * Retreives the Refined Soundex code for a given String object.
 *
 * @param str
 *                  String to encode using the Refined Soundex algorithm
 * @return A soundex code for the String supplied
 */

@Deprecated
public java.lang.String soundex(java.lang.String str) { throw new RuntimeException("Stub!"); }

/**
 * This static variable contains an instance of the RefinedSoundex using
 * the US_ENGLISH mapping.
 */

@Deprecated public static final org.apache.commons.codec.language.RefinedSoundex US_ENGLISH;
static { US_ENGLISH = null; }

/**
 * RefinedSoundex is *refined* for a number of reasons one being that the
 * mappings have been altered. This implementation contains default
 * mappings for US English.
 */

@Deprecated public static final char[] US_ENGLISH_MAPPING;
static { US_ENGLISH_MAPPING = new char[0]; }
}

