/*
 * $Header: /home/jerenkrantz/tmp/commons/commons-convert/cvs/home/cvs/jakarta-commons//httpclient/src/java/org/apache/commons/httpclient/methods/multipart/MultipartRequestEntity.java,v 1.1 2004/10/06 03:39:59 mbecke Exp $
 * $Revision: 502647 $
 * $Date: 2007-02-02 17:22:54 +0100 (Fri, 02 Feb 2007) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package com.android.internal.http.multipart;

import org.apache.http.protocol.HTTP;

/**
 * Implements a request entity suitable for an HTTP multipart POST method.
 * <p>
 * The HTTP multipart POST method is defined in section 3.3 of
 * <a href="http://www.ietf.org/rfc/rfc1867.txt">RFC1867</a>:
 * <blockquote>
 * The media-type multipart/form-data follows the rules of all multipart
 * MIME data streams as outlined in RFC 1521. The multipart/form-data contains
 * a series of parts. Each part is expected to contain a content-disposition
 * header where the value is "form-data" and a name attribute specifies
 * the field name within the form, e.g., 'content-disposition: form-data;
 * name="xxxxx"', where xxxxx is the field name corresponding to that field.
 * Field names originally in non-ASCII character sets may be encoded using
 * the method outlined in RFC 1522.
 * </blockquote>
 * </p>
 * <p>This entity is designed to be used in conjunction with the
 * {@link org.apache.http.HttpRequest} to provide
 * multipart posts.  Example usage:</p>
 * <pre>
 *  File f = new File("/path/fileToUpload.txt");
 *  HttpRequest request = new HttpRequest("http://host/some_path");
 *  Part[] parts = {
 *      new StringPart("param_name", "value"),
 *      new FilePart(f.getName(), f)
 *  };
 *  filePost.setEntity(
 *      new MultipartRequestEntity(parts, filePost.getParams())
 *      );
 *  HttpClient client = new HttpClient();
 *  int status = client.executeMethod(filePost);
 * </pre>
 *
 * @since 3.0
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MultipartEntity extends org.apache.http.entity.AbstractHttpEntity {

/**
 * Creates a new multipart entity containing the given parts.
 * @param parts The parts to include.
 * @param params The params of the HttpMethod using this entity.
 */

public MultipartEntity(com.android.internal.http.multipart.Part[] parts, org.apache.http.params.HttpParams params) { throw new RuntimeException("Stub!"); }

public MultipartEntity(com.android.internal.http.multipart.Part[] parts) { throw new RuntimeException("Stub!"); }

/**
 * Returns the MIME boundary string that is used to demarcate boundaries of
 * this part. The first call to this method will implicitly create a new
 * boundary string. To create a boundary string first the
 * HttpMethodParams.MULTIPART_BOUNDARY parameter is considered. Otherwise
 * a random one is generated.
 *
 * @return The boundary string of this entity in ASCII encoding.
 */

protected byte[] getMultipartBoundary() { throw new RuntimeException("Stub!"); }

/**
 * Returns <code>true</code> if all parts are repeatable, <code>false</code> otherwise.
 */

public boolean isRepeatable() { throw new RuntimeException("Stub!"); }

public void writeTo(java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

public org.apache.http.Header getContentType() { throw new RuntimeException("Stub!"); }

public long getContentLength() { throw new RuntimeException("Stub!"); }

public java.io.InputStream getContent() throws java.io.IOException, java.lang.IllegalStateException { throw new RuntimeException("Stub!"); }

public boolean isStreaming() { throw new RuntimeException("Stub!"); }

/**
 * Sets the value to use as the multipart boundary.
 * <p>
 * This parameter expects a value if type {@link String}.
 * </p>
 */

public static final java.lang.String MULTIPART_BOUNDARY = "http.method.multipart.boundary";

/** The MIME parts as set by the constructor */

protected com.android.internal.http.multipart.Part[] parts;
}

