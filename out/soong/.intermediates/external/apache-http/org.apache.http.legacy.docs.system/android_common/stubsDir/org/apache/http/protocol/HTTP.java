/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/protocol/HTTP.java $
 * $Revision: 555989 $
 * $Date: 2007-07-13 06:33:39 -0700 (Fri, 13 Jul 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.protocol;


/**
 * Constants and static helpers related to the HTTP protocol.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @version $Revision: 555989 $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class HTTP {

HTTP() { throw new RuntimeException("Stub!"); }

@Deprecated
public static boolean isWhitespace(char ch) { throw new RuntimeException("Stub!"); }

@Deprecated public static final java.lang.String ASCII = "ASCII";

@Deprecated public static final java.lang.String CHARSET_PARAM = "; charset=";

/** Transfer encoding definitions */

@Deprecated public static final java.lang.String CHUNK_CODING = "chunked";

/** HTTP connection control */

@Deprecated public static final java.lang.String CONN_CLOSE = "Close";

@Deprecated public static final java.lang.String CONN_DIRECTIVE = "Connection";

@Deprecated public static final java.lang.String CONN_KEEP_ALIVE = "Keep-Alive";

@Deprecated public static final java.lang.String CONTENT_ENCODING = "Content-Encoding";

@Deprecated public static final java.lang.String CONTENT_LEN = "Content-Length";

@Deprecated public static final java.lang.String CONTENT_TYPE = "Content-Type";

@Deprecated public static final int CR = 13; // 0xd

@Deprecated public static final java.lang.String DATE_HEADER = "Date";

/** Default charsets */

@Deprecated public static final java.lang.String DEFAULT_CONTENT_CHARSET = "ISO-8859-1";

/** Default content type */

@Deprecated public static final java.lang.String DEFAULT_CONTENT_TYPE = "application/octet-stream";

@Deprecated public static final java.lang.String DEFAULT_PROTOCOL_CHARSET = "US-ASCII";

/** HTTP expectations */

@Deprecated public static final java.lang.String EXPECT_CONTINUE = "100-continue";

@Deprecated public static final java.lang.String EXPECT_DIRECTIVE = "Expect";

@Deprecated public static final int HT = 9; // 0x9

@Deprecated public static final java.lang.String IDENTITY_CODING = "identity";

@Deprecated public static final java.lang.String ISO_8859_1 = "ISO-8859-1";

@Deprecated public static final int LF = 10; // 0xa

/** Content type definitions */

@Deprecated public static final java.lang.String OCTET_STREAM_TYPE = "application/octet-stream";

@Deprecated public static final java.lang.String PLAIN_TEXT_TYPE = "text/plain";

@Deprecated public static final java.lang.String SERVER_HEADER = "Server";

@Deprecated public static final int SP = 32; // 0x20

@Deprecated public static final java.lang.String TARGET_HOST = "Host";

/** HTTP header definitions */

@Deprecated public static final java.lang.String TRANSFER_ENCODING = "Transfer-Encoding";

@Deprecated public static final java.lang.String USER_AGENT = "User-Agent";

@Deprecated public static final java.lang.String US_ASCII = "US-ASCII";

@Deprecated public static final java.lang.String UTF_16 = "UTF-16";

/** Common charset definitions */

@Deprecated public static final java.lang.String UTF_8 = "UTF-8";
}

