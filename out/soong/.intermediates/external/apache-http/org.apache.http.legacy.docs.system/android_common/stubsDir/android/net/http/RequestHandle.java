/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.http;


/**
 * RequestHandle: handles a request session that may include multiple
 * redirects, HTTP authentication requests, etc.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class RequestHandle {

/**
 * Creates a new request session.
 */

public RequestHandle(android.net.http.RequestQueue requestQueue, java.lang.String url, android.net.compatibility.WebAddress uri, java.lang.String method, java.util.Map<java.lang.String,java.lang.String> headers, java.io.InputStream bodyProvider, int bodyLength, android.net.http.Request request) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new request session with a given Connection. This connection
 * is used during a synchronous load to handle this request.
 */

public RequestHandle(android.net.http.RequestQueue requestQueue, java.lang.String url, android.net.compatibility.WebAddress uri, java.lang.String method, java.util.Map<java.lang.String,java.lang.String> headers, java.io.InputStream bodyProvider, int bodyLength, android.net.http.Request request, android.net.http.Connection conn) { throw new RuntimeException("Stub!"); }

/**
 * Cancels this request
 */

public void cancel() { throw new RuntimeException("Stub!"); }

/**
 * Pauses the loading of this request. For example, called from the WebCore thread
 * when the plugin can take no more data.
 */

public void pauseRequest(boolean pause) { throw new RuntimeException("Stub!"); }

/**
 * Handles SSL error(s) on the way down from the user (the user
 * has already provided their feedback).
 */

public void handleSslErrorResponse(boolean proceed) { throw new RuntimeException("Stub!"); }

/**
 * @return true if we've hit the max redirect count
 */

public boolean isRedirectMax() { throw new RuntimeException("Stub!"); }

public int getRedirectCount() { throw new RuntimeException("Stub!"); }

public void setRedirectCount(int count) { throw new RuntimeException("Stub!"); }

/**
 * Create and queue a redirect request.
 *
 * @param redirectTo URL to redirect to
 * @param statusCode HTTP status code returned from original request
 * @param cacheHeaders Cache header for redirect URL
 * @return true if setup succeeds, false otherwise (redirect loop
 * count exceeded, body provider unable to rewind on 307 redirect)
 */

public boolean setupRedirect(java.lang.String redirectTo, int statusCode, java.util.Map<java.lang.String,java.lang.String> cacheHeaders) { throw new RuntimeException("Stub!"); }

/**
 * Create and queue an HTTP authentication-response (basic) request.
 */

public void setupBasicAuthResponse(boolean isProxy, java.lang.String username, java.lang.String password) { throw new RuntimeException("Stub!"); }

/**
 * Create and queue an HTTP authentication-response (digest) request.
 */

public void setupDigestAuthResponse(boolean isProxy, java.lang.String username, java.lang.String password, java.lang.String realm, java.lang.String nonce, java.lang.String QOP, java.lang.String algorithm, java.lang.String opaque) { throw new RuntimeException("Stub!"); }

/**
 * @return HTTP request method (GET, PUT, etc).
 */

public java.lang.String getMethod() { throw new RuntimeException("Stub!"); }

/**
 * @return Basic-scheme authentication response: BASE64(username:password).
 */

public static java.lang.String computeBasicAuthResponse(java.lang.String username, java.lang.String password) { throw new RuntimeException("Stub!"); }

public void waitUntilComplete() { throw new RuntimeException("Stub!"); }

public void processRequest() { throw new RuntimeException("Stub!"); }

/**
 * @return The right authorization header (dependeing on whether it is a proxy or not).
 */

public static java.lang.String authorizationHeader(boolean isProxy) { throw new RuntimeException("Stub!"); }

public static final int MAX_REDIRECT_COUNT = 16; // 0x10
}

