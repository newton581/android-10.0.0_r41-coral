/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.codec.net;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.EncoderException;
import java.io.UnsupportedEncodingException;
import java.util.BitSet;

/**
 * <p>
 * Codec for the Quoted-Printable section of <a href="http://www.ietf.org/rfc/rfc1521.txt">RFC 1521 </a>.
 * </p>
 * <p>
 * The Quoted-Printable encoding is intended to represent data that largely consists of octets that correspond to
 * printable characters in the ASCII character set. It encodes the data in such a way that the resulting octets are
 * unlikely to be modified by mail transport. If the data being encoded are mostly ASCII text, the encoded form of the
 * data remains largely recognizable by humans. A body which is entirely ASCII may also be encoded in Quoted-Printable
 * to ensure the integrity of the data should the message pass through a character- translating, and/or line-wrapping
 * gateway.
 * </p>
 *
 * <p>
 * Note:
 * </p>
 * <p>
 * Rules #3, #4, and #5 of the quoted-printable spec are not implemented yet because the complete quoted-printable spec
 * does not lend itself well into the byte[] oriented codec framework. Complete the codec once the steamable codec
 * framework is ready. The motivation behind providing the codec in a partial form is that it can already come in handy
 * for those applications that do not require quoted-printable line formatting (rules #3, #4, #5), for instance Q codec.
 * </p>
 *
 * @see <a href="http://www.ietf.org/rfc/rfc1521.txt"> RFC 1521 MIME (Multipurpose Internet Mail Extensions) Part One:
 *          Mechanisms for Specifying and Describing the Format of Internet Message Bodies </a>
 *
 * @author Apache Software Foundation
 * @since 1.3
 * @version $Id: QuotedPrintableCodec.java,v 1.7 2004/04/09 22:21:07 ggregory Exp $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class QuotedPrintableCodec implements org.apache.commons.codec.BinaryEncoder, org.apache.commons.codec.BinaryDecoder, org.apache.commons.codec.StringEncoder, org.apache.commons.codec.StringDecoder {

/**
 * Default constructor.
 */

@Deprecated
public QuotedPrintableCodec() { throw new RuntimeException("Stub!"); }

/**
 * Constructor which allows for the selection of a default charset
 *
 * @param charset
 *                  the default string charset to use.
 */

@Deprecated
public QuotedPrintableCodec(java.lang.String charset) { throw new RuntimeException("Stub!"); }

/**
 * Encodes an array of bytes into an array of quoted-printable 7-bit characters. Unsafe characters are escaped.
 *
 * <p>
 * This function implements a subset of quoted-printable encoding specification (rule #1 and rule #2) as defined in
 * RFC 1521 and is suitable for encoding binary data and unformatted text.
 * </p>
 *
 * @param printable
 *                  bitset of characters deemed quoted-printable
 * @param bytes
 *                  array of bytes to be encoded
 * @return array of bytes containing quoted-printable data
 */

@Deprecated
public static final byte[] encodeQuotedPrintable(java.util.BitSet printable, byte[] bytes) { throw new RuntimeException("Stub!"); }

/**
 * Decodes an array quoted-printable characters into an array of original bytes. Escaped characters are converted
 * back to their original representation.
 *
 * <p>
 * This function implements a subset of quoted-printable encoding specification (rule #1 and rule #2) as defined in
 * RFC 1521.
 * </p>
 *
 * @param bytes
 *                  array of quoted-printable characters
 * @return array of original bytes
 * @throws DecoderException
 *                  Thrown if quoted-printable decoding is unsuccessful
 */

@Deprecated
public static final byte[] decodeQuotedPrintable(byte[] bytes) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes an array of bytes into an array of quoted-printable 7-bit characters. Unsafe characters are escaped.
 *
 * <p>
 * This function implements a subset of quoted-printable encoding specification (rule #1 and rule #2) as defined in
 * RFC 1521 and is suitable for encoding binary data and unformatted text.
 * </p>
 *
 * @param bytes
 *                  array of bytes to be encoded
 * @return array of bytes containing quoted-printable data
 */

@Deprecated
public byte[] encode(byte[] bytes) { throw new RuntimeException("Stub!"); }

/**
 * Decodes an array of quoted-printable characters into an array of original bytes. Escaped characters are converted
 * back to their original representation.
 *
 * <p>
 * This function implements a subset of quoted-printable encoding specification (rule #1 and rule #2) as defined in
 * RFC 1521.
 * </p>
 *
 * @param bytes
 *                  array of quoted-printable characters
 * @return array of original bytes
 * @throws DecoderException
 *                  Thrown if quoted-printable decoding is unsuccessful
 */

@Deprecated
public byte[] decode(byte[] bytes) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes a string into its quoted-printable form using the default string charset. Unsafe characters are escaped.
 *
 * <p>
 * This function implements a subset of quoted-printable encoding specification (rule #1 and rule #2) as defined in
 * RFC 1521 and is suitable for encoding binary data.
 * </p>
 *
 * @param pString
 *                  string to convert to quoted-printable form
 * @return quoted-printable string
 *
 * @throws EncoderException
 *                  Thrown if quoted-printable encoding is unsuccessful
 *
 * @see #getDefaultCharset()
 */

@Deprecated
public java.lang.String encode(java.lang.String pString) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a quoted-printable string into its original form using the specified string charset. Escaped characters
 * are converted back to their original representation.
 *
 * @param pString
 *                  quoted-printable string to convert into its original form
 * @param charset
 *                  the original string charset
 * @return original string
 * @throws DecoderException
 *                  Thrown if quoted-printable decoding is unsuccessful
 * @throws UnsupportedEncodingException
 *                  Thrown if charset is not supported
 */

@Deprecated
public java.lang.String decode(java.lang.String pString, java.lang.String charset) throws org.apache.commons.codec.DecoderException, java.io.UnsupportedEncodingException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a quoted-printable string into its original form using the default string charset. Escaped characters are
 * converted back to their original representation.
 *
 * @param pString
 *                  quoted-printable string to convert into its original form
 * @return original string
 * @throws DecoderException
 *                  Thrown if quoted-printable decoding is unsuccessful
 * @throws UnsupportedEncodingException
 *                  Thrown if charset is not supported
 * @see #getDefaultCharset()
 */

@Deprecated
public java.lang.String decode(java.lang.String pString) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Encodes an object into its quoted-printable safe form. Unsafe characters are escaped.
 *
 * @param pObject
 *                  string to convert to a quoted-printable form
 * @return quoted-printable object
 * @throws EncoderException
 *                  Thrown if quoted-printable encoding is not applicable to objects of this type or if encoding is
 *                  unsuccessful
 */

@Deprecated
public java.lang.Object encode(java.lang.Object pObject) throws org.apache.commons.codec.EncoderException { throw new RuntimeException("Stub!"); }

/**
 * Decodes a quoted-printable object into its original form. Escaped characters are converted back to their original
 * representation.
 *
 * @param pObject
 *                  quoted-printable object to convert into its original form
 * @return original object
 * @throws DecoderException
 *                  Thrown if quoted-printable decoding is not applicable to objects of this type if decoding is
 *                  unsuccessful
 */

@Deprecated
public java.lang.Object decode(java.lang.Object pObject) throws org.apache.commons.codec.DecoderException { throw new RuntimeException("Stub!"); }

/**
 * Returns the default charset used for string decoding and encoding.
 *
 * @return the default string charset.
 */

@Deprecated
public java.lang.String getDefaultCharset() { throw new RuntimeException("Stub!"); }

/**
 * Encodes a string into its quoted-printable form using the specified charset. Unsafe characters are escaped.
 *
 * <p>
 * This function implements a subset of quoted-printable encoding specification (rule #1 and rule #2) as defined in
 * RFC 1521 and is suitable for encoding binary data and unformatted text.
 * </p>
 *
 * @param pString
 *                  string to convert to quoted-printable form
 * @param charset
 *                  the charset for pString
 * @return quoted-printable string
 *
 * @throws UnsupportedEncodingException
 *                  Thrown if the charset is not supported
 */

@Deprecated
public java.lang.String encode(java.lang.String pString, java.lang.String charset) throws java.io.UnsupportedEncodingException { throw new RuntimeException("Stub!"); }
}

