/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpcore/trunk/module-main/src/main/java/org/apache/http/HttpStatus.java $
 * $Revision: 503381 $
 * $Date: 2007-02-04 02:59:10 -0800 (Sun, 04 Feb 2007) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http;


/**
 * Constants enumerating the HTTP status codes.
 * All status codes defined in RFC1945 (HTTP/1.0), RFC2616 (HTTP/1.1), and
 * RFC2518 (WebDAV) are listed.
 *
 * @see StatusLine
 * @author Unascribed
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 * @author <a href="mailto:jsdever@apache.org">Jeff Dever</a>
 *
 * @version $Revision: 503381 $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public interface HttpStatus {

/** <tt>202 Accepted</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_ACCEPTED = 202; // 0xca

/** <tt>502 Bad Gateway</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_BAD_GATEWAY = 502; // 0x1f6

/** <tt>400 Bad Request</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_BAD_REQUEST = 400; // 0x190

/** <tt>409 Conflict</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_CONFLICT = 409; // 0x199

/** <tt>100 Continue</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_CONTINUE = 100; // 0x64

/** <tt>201 Created</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_CREATED = 201; // 0xc9

/** <tt>417 Expectation Failed</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_EXPECTATION_FAILED = 417; // 0x1a1

/** <tt>424 Failed Dependency</tt> (WebDAV - RFC 2518) */

@Deprecated public static final int SC_FAILED_DEPENDENCY = 424; // 0x1a8

/** <tt>403 Forbidden</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_FORBIDDEN = 403; // 0x193

/** <tt>504 Gateway Timeout</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_GATEWAY_TIMEOUT = 504; // 0x1f8

/** <tt>410 Gone</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_GONE = 410; // 0x19a

/** <tt>505 HTTP Version Not Supported</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_HTTP_VERSION_NOT_SUPPORTED = 505; // 0x1f9

/**
 * Static constant for a 419 error.
 * <tt>419 Insufficient Space on Resource</tt>
 * (WebDAV - draft-ietf-webdav-protocol-05?)
 * or <tt>419 Proxy Reauthentication Required</tt>
 * (HTTP/1.1 drafts?)
 */

@Deprecated public static final int SC_INSUFFICIENT_SPACE_ON_RESOURCE = 419; // 0x1a3

/** <tt>507 Insufficient Storage</tt> (WebDAV - RFC 2518) */

@Deprecated public static final int SC_INSUFFICIENT_STORAGE = 507; // 0x1fb

/** <tt>500 Server Error</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_INTERNAL_SERVER_ERROR = 500; // 0x1f4

/** <tt>411 Length Required</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_LENGTH_REQUIRED = 411; // 0x19b

/** <tt>423 Locked</tt> (WebDAV - RFC 2518) */

@Deprecated public static final int SC_LOCKED = 423; // 0x1a7

/**
 * Static constant for a 420 error.
 * <tt>420 Method Failure</tt>
 * (WebDAV - draft-ietf-webdav-protocol-05?)
 */

@Deprecated public static final int SC_METHOD_FAILURE = 420; // 0x1a4

/** <tt>405 Method Not Allowed</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_METHOD_NOT_ALLOWED = 405; // 0x195

/** <tt>301 Moved Permanently</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_MOVED_PERMANENTLY = 301; // 0x12d

/** <tt>302 Moved Temporarily</tt> (Sometimes <tt>Found</tt>) (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_MOVED_TEMPORARILY = 302; // 0x12e

/** <tt>300 Mutliple Choices</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_MULTIPLE_CHOICES = 300; // 0x12c

/** 
 * <tt>207 Multi-Status</tt> (WebDAV - RFC 2518) or <tt>207 Partial Update
 * OK</tt> (HTTP/1.1 - draft-ietf-http-v11-spec-rev-01?)
 */

@Deprecated public static final int SC_MULTI_STATUS = 207; // 0xcf

/** <tt>203 Non Authoritative Information</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_NON_AUTHORITATIVE_INFORMATION = 203; // 0xcb

/** <tt>406 Not Acceptable</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_NOT_ACCEPTABLE = 406; // 0x196

/** <tt>404 Not Found</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_NOT_FOUND = 404; // 0x194

/** <tt>501 Not Implemented</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_NOT_IMPLEMENTED = 501; // 0x1f5

/** <tt>304 Not Modified</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_NOT_MODIFIED = 304; // 0x130

/** <tt>204 No Content</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_NO_CONTENT = 204; // 0xcc

/** <tt>200 OK</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_OK = 200; // 0xc8

/** <tt>206 Partial Content</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_PARTIAL_CONTENT = 206; // 0xce

/** <tt>402 Payment Required</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_PAYMENT_REQUIRED = 402; // 0x192

/** <tt>412 Precondition Failed</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_PRECONDITION_FAILED = 412; // 0x19c

/** <tt>102 Processing</tt> (WebDAV - RFC 2518) */

@Deprecated public static final int SC_PROCESSING = 102; // 0x66

/** <tt>407 Proxy Authentication Required</tt> (HTTP/1.1 - RFC 2616)*/

@Deprecated public static final int SC_PROXY_AUTHENTICATION_REQUIRED = 407; // 0x197

/** <tt>416 Requested Range Not Satisfiable</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_REQUESTED_RANGE_NOT_SATISFIABLE = 416; // 0x1a0

/** <tt>408 Request Timeout</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_REQUEST_TIMEOUT = 408; // 0x198

/** <tt>413 Request Entity Too Large</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_REQUEST_TOO_LONG = 413; // 0x19d

/** <tt>414 Request-URI Too Long</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_REQUEST_URI_TOO_LONG = 414; // 0x19e

/** <tt>205 Reset Content</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_RESET_CONTENT = 205; // 0xcd

/** <tt>303 See Other</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_SEE_OTHER = 303; // 0x12f

/** <tt>503 Service Unavailable</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_SERVICE_UNAVAILABLE = 503; // 0x1f7

/** <tt>101 Switching Protocols</tt> (HTTP/1.1 - RFC 2616)*/

@Deprecated public static final int SC_SWITCHING_PROTOCOLS = 101; // 0x65

/** <tt>307 Temporary Redirect</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_TEMPORARY_REDIRECT = 307; // 0x133

/** <tt>401 Unauthorized</tt> (HTTP/1.0 - RFC 1945) */

@Deprecated public static final int SC_UNAUTHORIZED = 401; // 0x191

/** <tt>422 Unprocessable Entity</tt> (WebDAV - RFC 2518) */

@Deprecated public static final int SC_UNPROCESSABLE_ENTITY = 422; // 0x1a6

/** <tt>415 Unsupported Media Type</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_UNSUPPORTED_MEDIA_TYPE = 415; // 0x19f

/** <tt>305 Use Proxy</tt> (HTTP/1.1 - RFC 2616) */

@Deprecated public static final int SC_USE_PROXY = 305; // 0x131
}

