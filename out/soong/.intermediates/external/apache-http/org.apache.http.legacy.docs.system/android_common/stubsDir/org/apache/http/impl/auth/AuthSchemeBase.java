/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/auth/AuthSchemeBase.java $
 * $Revision: 653867 $
 * $Date: 2008-05-06 11:17:29 -0700 (Tue, 06 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.auth;

import org.apache.http.auth.MalformedChallengeException;

/**
 * Abstract authentication scheme class that serves as a basis
 * for all authentication schemes supported by HttpClient. This class
 * defines the generic way of parsing an authentication challenge. It
 * does not make any assumptions regarding the format of the challenge
 * nor does it impose any specific way of responding to that challenge.
 *
 * @author <a href="mailto:oleg at ural.ru">Oleg Kalnichevski</a>
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract class AuthSchemeBase implements org.apache.http.auth.AuthScheme {

@Deprecated
public AuthSchemeBase() { throw new RuntimeException("Stub!"); }

/**
 * Processes the given challenge token. Some authentication schemes
 * may involve multiple challenge-response exchanges. Such schemes must be able
 * to maintain the state information when dealing with sequential challenges
 *
 * @param header the challenge header
 *
 * @throws MalformedChallengeException is thrown if the authentication challenge
 * is malformed
 */

@Deprecated
public void processChallenge(org.apache.http.Header header) throws org.apache.http.auth.MalformedChallengeException { throw new RuntimeException("Stub!"); }

@Deprecated
protected abstract void parseChallenge(org.apache.http.util.CharArrayBuffer buffer, int pos, int len) throws org.apache.http.auth.MalformedChallengeException;

/**
 * Returns <code>true</code> if authenticating against a proxy, <code>false</code>
 * otherwise.
 *
 * @return <code>true</code> if authenticating against a proxy, <code>false</code>
 * otherwise
 */

@Deprecated
public boolean isProxy() { throw new RuntimeException("Stub!"); }
}

