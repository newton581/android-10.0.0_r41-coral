/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 


package org.apache.commons.logging;

import org.apache.commons.logging.impl.NoOpLog;
import java.lang.reflect.Constructor;

/**
 * <p>Factory for creating {@link Log} instances.  Applications should call
 * the <code>makeNewLogInstance()</code> method to instantiate new instances
 * of the configured {@link Log} implementation class.</p>
 *
 * <p>By default, calling <code>getInstance()</code> will use the following
 * algorithm:</p>
 * <ul>
 * <li>If Log4J is available, return an instance of
 *     <code>org.apache.commons.logging.impl.Log4JLogger</code>.</li>
 * <li>If JDK 1.4 or later is available, return an instance of
 *     <code>org.apache.commons.logging.impl.Jdk14Logger</code>.</li>
 * <li>Otherwise, return an instance of
 *     <code>org.apache.commons.logging.impl.NoOpLog</code>.</li>
 * </ul>
 *
 * <p>You can change the default behavior in one of two ways:</p>
 * <ul>
 * <li>On the startup command line, set the system property
 *     <code>org.apache.commons.logging.log</code> to the name of the
 *     <code>org.apache.commons.logging.Log</code> implementation class
 *     you want to use.</li>
 * <li>At runtime, call <code>LogSource.setLogImplementation()</code>.</li>
 * </ul>
 *
 * @deprecated Use {@link LogFactory} instead - The default factory
 *  implementation performs exactly the same algorithm as this class did
 *
 * @author Rod Waldhoff
 * @version $Id: LogSource.java 155426 2005-02-26 13:10:49Z dirkv $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class LogSource {

/** Don't allow others to create instances */

LogSource() { throw new RuntimeException("Stub!"); }

/**
 * Set the log implementation/log implementation factory
 * by the name of the class.  The given class
 * must implement {@link Log}, and provide a constructor that
 * takes a single {@link String} argument (containing the name
 * of the log).
 */

@Deprecated
public static void setLogImplementation(java.lang.String classname) throws java.lang.ClassNotFoundException, java.lang.ExceptionInInitializerError, java.lang.LinkageError, java.lang.NoSuchMethodException, java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/**
 * Set the log implementation/log implementation factory
 * by class.  The given class must implement {@link Log},
 * and provide a constructor that takes a single {@link String}
 * argument (containing the name of the log).
 */

@Deprecated
public static void setLogImplementation(java.lang.Class logclass) throws java.lang.ExceptionInInitializerError, java.lang.LinkageError, java.lang.NoSuchMethodException, java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/** Get a <code>Log</code> instance by class name */

@Deprecated
public static org.apache.commons.logging.Log getInstance(java.lang.String name) { throw new RuntimeException("Stub!"); }

/** Get a <code>Log</code> instance by class */

@Deprecated
public static org.apache.commons.logging.Log getInstance(java.lang.Class clazz) { throw new RuntimeException("Stub!"); }

/**
 * Create a new {@link Log} implementation, based
 * on the given <i>name</i>.
 * <p>
 * The specific {@link Log} implementation returned
 * is determined by the value of the
 * <tt>org.apache.commons.logging.log</tt> property.
 * The value of <tt>org.apache.commons.logging.log</tt> may be set to
 * the fully specified name of a class that implements
 * the {@link Log} interface.  This class must also
 * have a public constructor that takes a single
 * {@link String} argument (containing the <i>name</i>
 * of the {@link Log} to be constructed.
 * <p>
 * When <tt>org.apache.commons.logging.log</tt> is not set,
 * or when no corresponding class can be found,
 * this method will return a Log4JLogger
 * if the log4j Logger class is
 * available in the {@link LogSource}'s classpath, or a
 * Jdk14Logger if we are on a JDK 1.4 or later system, or
 * NoOpLog if neither of the above conditions is true.
 *
 * @param name the log name (or category)
 */

@Deprecated
public static org.apache.commons.logging.Log makeNewLogInstance(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Returns a {@link String} array containing the names of
 * all logs known to me.
 */

@Deprecated
public static java.lang.String[] getLogNames() { throw new RuntimeException("Stub!"); }

/** Is JDK 1.4 logging available */

@Deprecated protected static boolean jdk14IsAvailable = false;

/** Is log4j available (in the current classpath) */

@Deprecated protected static boolean log4jIsAvailable = false;

/** Constructor for current log class */

@Deprecated protected static java.lang.reflect.Constructor logImplctor;

@Deprecated protected static java.util.Hashtable logs;
}

