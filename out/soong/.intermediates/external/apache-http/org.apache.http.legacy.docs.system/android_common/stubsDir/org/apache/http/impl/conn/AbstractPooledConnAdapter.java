/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/impl/conn/AbstractPooledConnAdapter.java $
 * $Revision: 658775 $
 * $Date: 2008-05-21 10:30:45 -0700 (Wed, 21 May 2008) $
 *
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */


package org.apache.http.impl.conn;


/**
 * Abstract adapter from pool {@link AbstractPoolEntry entries} to
 * {@link org.apache.http.conn.ManagedClientConnection managed}
 * client connections.
 * The connection in the pool entry is used to initialize the base class.
 * In addition, methods to establish a route are delegated to the
 * pool entry. {@link #shutdown shutdown} and {@link #close close}
 * will clear the tracked route in the pool entry and call the
 * respective method of the wrapped connection.
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 *
 *
 * <!-- empty lines to avoid svn diff problems -->
 * @version   $Revision: 658775 $ $Date: 2008-05-21 10:30:45 -0700 (Wed, 21 May 2008) $
 *
 * @since 4.0
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract class AbstractPooledConnAdapter extends org.apache.http.impl.conn.AbstractClientConnAdapter {

/**
 * Creates a new connection adapter.
 *
 * @param manager   the connection manager
 * @param entry     the pool entry for the connection being wrapped
 */

@Deprecated
protected AbstractPooledConnAdapter(org.apache.http.conn.ClientConnectionManager manager, org.apache.http.impl.conn.AbstractPoolEntry entry) { super(null, null); throw new RuntimeException("Stub!"); }

/**
 * Asserts that this adapter is still attached.
 *
 * @throws IllegalStateException
 *      if it is {@link #detach detach}ed
 */

@Deprecated
protected final void assertAttached() { throw new RuntimeException("Stub!"); }

/**
 * Detaches this adapter from the wrapped connection.
 * This adapter becomes useless.
 */

@Deprecated
protected void detach() { throw new RuntimeException("Stub!"); }

@Deprecated
public org.apache.http.conn.routing.HttpRoute getRoute() { throw new RuntimeException("Stub!"); }

@Deprecated
public void open(org.apache.http.conn.routing.HttpRoute route, org.apache.http.protocol.HttpContext context, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void tunnelTarget(boolean secure, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void tunnelProxy(org.apache.http.HttpHost next, boolean secure, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void layerProtocol(org.apache.http.protocol.HttpContext context, org.apache.http.params.HttpParams params) throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public void shutdown() throws java.io.IOException { throw new RuntimeException("Stub!"); }

@Deprecated
public java.lang.Object getState() { throw new RuntimeException("Stub!"); }

@Deprecated
public void setState(java.lang.Object state) { throw new RuntimeException("Stub!"); }

/** The wrapped pool entry. */

@Deprecated protected volatile org.apache.http.impl.conn.AbstractPoolEntry poolEntry;
}

