/*
 * $HeadURL: http://svn.apache.org/repos/asf/httpcomponents/httpclient/trunk/module-client/src/main/java/org/apache/http/conn/scheme/Scheme.java $
 * $Revision: 652950 $
 * $Date: 2008-05-02 16:49:48 -0700 (Fri, 02 May 2008) $
 *
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

package org.apache.http.conn.scheme;


/**
 * Encapsulates specifics of a protocol scheme such as "http" or "https".
 * Schemes are identified by lowercase names.
 * Supported schemes are typically collected in a
 * {@link SchemeRegistry SchemeRegistry}.
 *
 * <p>
 * For example, to configure support for "https://" URLs,
 * you could write code like the following:
 * </p>
 * <pre>
 * Scheme https = new Scheme("https", new MySecureSocketFactory(), 443);
 * SchemeRegistry.DEFAULT.register(https);
 * </pre>
 *
 * @author <a href="mailto:rolandw at apache.org">Roland Weber</a>
 * @author Michael Becke
 * @author Jeff Dever
 * @author <a href="mailto:mbowler@GargoyleSoftware.com">Mike Bowler</a>
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class Scheme {

/**
 * Creates a new scheme.
 * Whether the created scheme allows for layered connections
 * depends on the class of <code>factory</code>.
 *
 * @param name      the scheme name, for example "http".
 *                  The name will be converted to lowercase.
 * @param factory   the factory for creating sockets for communication
 *                  with this scheme
 * @param port      the default port for this scheme
 */

@Deprecated
public Scheme(java.lang.String name, org.apache.http.conn.scheme.SocketFactory factory, int port) { throw new RuntimeException("Stub!"); }

/**
 * Obtains the default port.
 *
 * @return  the default port for this scheme
 */

@Deprecated
public int getDefaultPort() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the socket factory.
 * If this scheme is {@link #isLayered layered}, the factory implements
 * {@link LayeredSocketFactory LayeredSocketFactory}.
 *
 * @return  the socket factory for this scheme
 */

@Deprecated
public org.apache.http.conn.scheme.SocketFactory getSocketFactory() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the scheme name.
 *
 * @return  the name of this scheme, in lowercase
 */

@Deprecated
public java.lang.String getName() { throw new RuntimeException("Stub!"); }

/**
 * Indicates whether this scheme allows for layered connections.
 *
 * @return <code>true</code> if layered connections are possible,
 *         <code>false</code> otherwise
 */

@Deprecated
public boolean isLayered() { throw new RuntimeException("Stub!"); }

/**
 * Resolves the correct port for this scheme.
 * Returns the given port if it is valid, the default port otherwise.
 *
 * @param port      the port to be resolved,
 *                  a negative number to obtain the default port
 *
 * @return the given port or the defaultPort
 */

@Deprecated
public int resolvePort(int port) { throw new RuntimeException("Stub!"); }

/**
 * Return a string representation of this object.
 *
 * @return  a human-readable string description of this scheme
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Compares this scheme to an object.
 *
 * @param obj       the object to compare with
 *
 * @return  <code>true</code> iff the argument is equal to this scheme
 */

@Deprecated
public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * Obtains a hash code for this scheme.
 *
 * @return  the hash code
 */

@Deprecated
public int hashCode() { throw new RuntimeException("Stub!"); }
}

