/*
 * Copyright 2001-2004 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 



package org.apache.commons.logging.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogConfigurationException;
import java.util.Properties;

/**
 * <p>Simple implementation of Log that sends all enabled log messages,
 * for all defined loggers, to System.err.  The following system properties
 * are supported to configure the behavior of this logger:</p>
 * <ul>
 * <li><code>org.apache.commons.logging.simplelog.defaultlog</code> -
 *     Default logging detail level for all instances of SimpleLog.
 *     Must be one of ("trace", "debug", "info", "warn", "error", or "fatal").
 *     If not specified, defaults to "info". </li>
 * <li><code>org.apache.commons.logging.simplelog.log.xxxxx</code> -
 *     Logging detail level for a SimpleLog instance named "xxxxx".
 *     Must be one of ("trace", "debug", "info", "warn", "error", or "fatal").
 *     If not specified, the default logging detail level is used.</li>
 * <li><code>org.apache.commons.logging.simplelog.showlogname</code> -
 *     Set to <code>true</code> if you want the Log instance name to be
 *     included in output messages. Defaults to <code>false</code>.</li>
 * <li><code>org.apache.commons.logging.simplelog.showShortLogname</code> -
 *     Set to <code>true</code> if you want the last component of the name to be
 *     included in output messages. Defaults to <code>true</code>.</li>
 * <li><code>org.apache.commons.logging.simplelog.showdatetime</code> -
 *     Set to <code>true</code> if you want the current date and time
 *     to be included in output messages. Default is <code>false</code>.</li>
 * <li><code>org.apache.commons.logging.simplelog.dateTimeFormat</code> -
 *     The date and time format to be used in the output messages.
 *     The pattern describing the date and time format is the same that is
 *     used in <code>java.text.SimpleDateFormat</code>. If the format is not
 *     specified or is invalid, the default format is used.
 *     The default format is <code>yyyy/MM/dd HH:mm:ss:SSS zzz</code>.</li>
 * </ul>
 *
 * <p>In addition to looking for system properties with the names specified
 * above, this implementation also checks for a class loader resource named
 * <code>"simplelog.properties"</code>, and includes any matching definitions
 * from this resource (if it exists).</p>
 *
 * @author <a href="mailto:sanders@apache.org">Scott Sanders</a>
 * @author Rod Waldhoff
 * @author Robert Burrell Donkin
 *
 * @version $Id: SimpleLog.java 399221 2006-05-03 09:20:24Z dennisl $
 *
 * @deprecated Please use {@link java.net.URL#openConnection} instead.
 *     Please visit <a href="http://android-developers.blogspot.com/2011/09/androids-http-clients.html">this webpage</a>
 *     for further details.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class SimpleLog implements org.apache.commons.logging.Log, java.io.Serializable {

/**
 * Construct a simple log with given name.
 *
 * @param name log name
 */

@Deprecated
public SimpleLog(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * <p> Set logging level. </p>
 *
 * @param currentLogLevel new logging level
 */

@Deprecated
public void setLevel(int currentLogLevel) { throw new RuntimeException("Stub!"); }

/**
 * <p> Get logging level. </p>
 */

@Deprecated
public int getLevel() { throw new RuntimeException("Stub!"); }

/**
 * <p> Do the actual logging.
 * This method assembles the message
 * and then calls <code>write()</code> to cause it to be written.</p>
 *
 * @param type One of the LOG_LEVEL_XXX constants defining the log level
 * @param message The message itself (typically a String)
 * @param t The exception whose stack trace should be logged
 */

@Deprecated
protected void log(int type, java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/**
 * <p>Write the content of the message accumulated in the specified
 * <code>StringBuffer</code> to the appropriate output destination.  The
 * default implementation writes to <code>System.err</code>.</p>
 *
 * @param buffer A <code>StringBuffer</code> containing the accumulated
 *  text to be logged
 */

@Deprecated
protected void write(java.lang.StringBuffer buffer) { throw new RuntimeException("Stub!"); }

/**
 * Is the given log level currently enabled?
 *
 * @param logLevel is this level enabled?
 */

@Deprecated
protected boolean isLevelEnabled(int logLevel) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_DEBUG</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#debug(Object)
 */

@Deprecated
public final void debug(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_DEBUG</code>.
 *
 * @param message to log
 * @param t log this cause
 * @see org.apache.commons.logging.Log#debug(Object, Throwable)
 */

@Deprecated
public final void debug(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_TRACE</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#trace(Object)
 */

@Deprecated
public final void trace(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_TRACE</code>.
 *
 * @param message to log
 * @param t log this cause
 * @see org.apache.commons.logging.Log#trace(Object, Throwable)
 */

@Deprecated
public final void trace(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_INFO</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#info(Object)
 */

@Deprecated
public final void info(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_INFO</code>.
 *
 * @param message to log
 * @param t log this cause
 * @see org.apache.commons.logging.Log#info(Object, Throwable)
 */

@Deprecated
public final void info(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_WARN</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#warn(Object)
 */

@Deprecated
public final void warn(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_WARN</code>.
 *
 * @param message to log
 * @param t log this cause
 * @see org.apache.commons.logging.Log#warn(Object, Throwable)
 */

@Deprecated
public final void warn(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_ERROR</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#error(Object)
 */

@Deprecated
public final void error(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_ERROR</code>.
 *
 * @param message to log
 * @param t log this cause
 * @see org.apache.commons.logging.Log#error(Object, Throwable)
 */

@Deprecated
public final void error(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/**
 * Log a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_FATAL</code>.
 *
 * @param message to log
 * @see org.apache.commons.logging.Log#fatal(Object)
 */

@Deprecated
public final void fatal(java.lang.Object message) { throw new RuntimeException("Stub!"); }

/**
 * Logs a message with
 * <code>org.apache.commons.logging.impl.SimpleLog.LOG_LEVEL_FATAL</code>.
 *
 * @param message to log
 * @param t log this cause
 * @see org.apache.commons.logging.Log#fatal(Object, Throwable)
 */

@Deprecated
public final void fatal(java.lang.Object message, java.lang.Throwable t) { throw new RuntimeException("Stub!"); }

/**
 * <p> Are debug messages currently enabled? </p>
 *
 * <p> This allows expensive operations such as <code>String</code>
 * concatenation to be avoided when the message will be ignored by the
 * logger. </p>
 */

@Deprecated
public final boolean isDebugEnabled() { throw new RuntimeException("Stub!"); }

/**
 * <p> Are error messages currently enabled? </p>
 *
 * <p> This allows expensive operations such as <code>String</code>
 * concatenation to be avoided when the message will be ignored by the
 * logger. </p>
 */

@Deprecated
public final boolean isErrorEnabled() { throw new RuntimeException("Stub!"); }

/**
 * <p> Are fatal messages currently enabled? </p>
 *
 * <p> This allows expensive operations such as <code>String</code>
 * concatenation to be avoided when the message will be ignored by the
 * logger. </p>
 */

@Deprecated
public final boolean isFatalEnabled() { throw new RuntimeException("Stub!"); }

/**
 * <p> Are info messages currently enabled? </p>
 *
 * <p> This allows expensive operations such as <code>String</code>
 * concatenation to be avoided when the message will be ignored by the
 * logger. </p>
 */

@Deprecated
public final boolean isInfoEnabled() { throw new RuntimeException("Stub!"); }

/**
 * <p> Are trace messages currently enabled? </p>
 *
 * <p> This allows expensive operations such as <code>String</code>
 * concatenation to be avoided when the message will be ignored by the
 * logger. </p>
 */

@Deprecated
public final boolean isTraceEnabled() { throw new RuntimeException("Stub!"); }

/**
 * <p> Are warn messages currently enabled? </p>
 *
 * <p> This allows expensive operations such as <code>String</code>
 * concatenation to be avoided when the message will be ignored by the
 * logger. </p>
 */

@Deprecated
public final boolean isWarnEnabled() { throw new RuntimeException("Stub!"); }

/** The default format to use when formating dates */

@Deprecated protected static final java.lang.String DEFAULT_DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm:ss:SSS zzz";

/** Enable all logging levels */

@Deprecated public static final int LOG_LEVEL_ALL = 0; // 0x0

/** "Debug" level logging. */

@Deprecated public static final int LOG_LEVEL_DEBUG = 2; // 0x2

/** "Error" level logging. */

@Deprecated public static final int LOG_LEVEL_ERROR = 5; // 0x5

/** "Fatal" level logging. */

@Deprecated public static final int LOG_LEVEL_FATAL = 6; // 0x6

/** "Info" level logging. */

@Deprecated public static final int LOG_LEVEL_INFO = 3; // 0x3

/** Enable no logging levels */

@Deprecated public static final int LOG_LEVEL_OFF = 7; // 0x7

/** "Trace" level logging. */

@Deprecated public static final int LOG_LEVEL_TRACE = 1; // 0x1

/** "Warn" level logging. */

@Deprecated public static final int LOG_LEVEL_WARN = 4; // 0x4

/** The current log level */

@Deprecated protected int currentLogLevel;

/** Used to format times */

@Deprecated protected static java.text.DateFormat dateFormatter;

/** The date and time format to use in the log message */

@Deprecated protected static java.lang.String dateTimeFormat = "yyyy/MM/dd HH:mm:ss:SSS zzz";

/** The name of this simple log instance */

@Deprecated protected java.lang.String logName;

/** Include the current time in the log message */

@Deprecated protected static boolean showDateTime = false;

/** Include the instance name in the log message? */

@Deprecated protected static boolean showLogName = false;

/** Include the short name ( last component ) of the logger in the log
 *  message. Defaults to true - otherwise we'll be lost in a flood of
 *  messages without knowing who sends them.
 */

@Deprecated protected static boolean showShortName = true;

/** Properties loaded from simplelog.properties */

@Deprecated protected static final java.util.Properties simpleLogProps;
static { simpleLogProps = null; }

/** All system properties used by <code>SimpleLog</code> start with this */

@Deprecated protected static final java.lang.String systemPrefix = "org.apache.commons.logging.simplelog.";
}

