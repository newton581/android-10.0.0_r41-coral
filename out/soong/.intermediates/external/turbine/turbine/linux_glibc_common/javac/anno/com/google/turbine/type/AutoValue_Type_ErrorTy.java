
package com.google.turbine.type;

import javax.annotation.Generated;

@Generated("com.google.auto.value.processor.AutoValueProcessor")
 final class AutoValue_Type_ErrorTy extends Type.ErrorTy {

  AutoValue_Type_ErrorTy(
 ) {
  }

  @Override
  public String toString() {
    return "ErrorTy{"
        + "}";
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof Type.ErrorTy) {
      return true;
    }
    return false;
  }

  @Override
  public int hashCode() {
    int h = 1;
    return h;
  }

}
