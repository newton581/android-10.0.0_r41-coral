
package com.google.turbine.type;

import com.google.common.collect.ImmutableList;
import javax.annotation.Generated;

@Generated("com.google.auto.value.processor.AutoValueProcessor")
 final class AutoValue_Type_ClassTy extends Type.ClassTy {

  private final ImmutableList<Type.ClassTy.SimpleClassTy> classes;

  AutoValue_Type_ClassTy(
      ImmutableList<Type.ClassTy.SimpleClassTy> classes) {
    if (classes == null) {
      throw new NullPointerException("Null classes");
    }
    this.classes = classes;
  }

  @Override
  public ImmutableList<Type.ClassTy.SimpleClassTy> classes() {
    return classes;
  }

  @Override
  public boolean equals(Object o) {
    if (o == this) {
      return true;
    }
    if (o instanceof Type.ClassTy) {
      Type.ClassTy that = (Type.ClassTy) o;
      return (this.classes.equals(that.classes()));
    }
    return false;
  }

  @Override
  public int hashCode() {
    int h = 1;
    h *= 1000003;
    h ^= this.classes.hashCode();
    return h;
  }

}
