// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: perfetto/config/process_stats/process_stats_config.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "perfetto/config/process_stats/process_stats_config.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace perfetto {
namespace protos {

void protobuf_ShutdownFile_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto() {
  delete ProcessStatsConfig::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ProcessStatsConfig::default_instance_ = new ProcessStatsConfig();
  ProcessStatsConfig::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto_once_);
void protobuf_AddDesc_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto_once_,
                 &protobuf_AddDesc_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto {
  StaticDescriptorInitializer_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto() {
    protobuf_AddDesc_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto();
  }
} static_descriptor_initializer_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForProcessStatsConfig(
    ProcessStatsConfig* ptr) {
  return ptr->mutable_unknown_fields();
}

bool ProcessStatsConfig_Quirks_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const ProcessStatsConfig_Quirks ProcessStatsConfig::QUIRKS_UNSPECIFIED;
const ProcessStatsConfig_Quirks ProcessStatsConfig::DISABLE_INITIAL_DUMP;
const ProcessStatsConfig_Quirks ProcessStatsConfig::DISABLE_ON_DEMAND;
const ProcessStatsConfig_Quirks ProcessStatsConfig::Quirks_MIN;
const ProcessStatsConfig_Quirks ProcessStatsConfig::Quirks_MAX;
const int ProcessStatsConfig::Quirks_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int ProcessStatsConfig::kQuirksFieldNumber;
const int ProcessStatsConfig::kScanAllProcessesOnStartFieldNumber;
const int ProcessStatsConfig::kRecordThreadNamesFieldNumber;
const int ProcessStatsConfig::kProcStatsPollMsFieldNumber;
const int ProcessStatsConfig::kProcStatsCacheTtlMsFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

ProcessStatsConfig::ProcessStatsConfig()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:perfetto.protos.ProcessStatsConfig)
}

void ProcessStatsConfig::InitAsDefaultInstance() {
}

ProcessStatsConfig::ProcessStatsConfig(const ProcessStatsConfig& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:perfetto.protos.ProcessStatsConfig)
}

void ProcessStatsConfig::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  scan_all_processes_on_start_ = false;
  record_thread_names_ = false;
  proc_stats_poll_ms_ = 0u;
  proc_stats_cache_ttl_ms_ = 0u;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

ProcessStatsConfig::~ProcessStatsConfig() {
  // @@protoc_insertion_point(destructor:perfetto.protos.ProcessStatsConfig)
  SharedDtor();
}

void ProcessStatsConfig::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void ProcessStatsConfig::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ProcessStatsConfig& ProcessStatsConfig::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_perfetto_2fconfig_2fprocess_5fstats_2fprocess_5fstats_5fconfig_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

ProcessStatsConfig* ProcessStatsConfig::default_instance_ = NULL;

ProcessStatsConfig* ProcessStatsConfig::New(::google::protobuf::Arena* arena) const {
  ProcessStatsConfig* n = new ProcessStatsConfig;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void ProcessStatsConfig::Clear() {
// @@protoc_insertion_point(message_clear_start:perfetto.protos.ProcessStatsConfig)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(ProcessStatsConfig, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<ProcessStatsConfig*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  ZR_(scan_all_processes_on_start_, proc_stats_cache_ttl_ms_);

#undef ZR_HELPER_
#undef ZR_

  quirks_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool ProcessStatsConfig::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForProcessStatsConfig, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:perfetto.protos.ProcessStatsConfig)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // repeated .perfetto.protos.ProcessStatsConfig.Quirks quirks = 1;
      case 1: {
        if (tag == 8) {
         parse_quirks:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          if (::perfetto::protos::ProcessStatsConfig_Quirks_IsValid(value)) {
            add_quirks(static_cast< ::perfetto::protos::ProcessStatsConfig_Quirks >(value));
          } else {
            unknown_fields_stream.WriteVarint32(tag);
            unknown_fields_stream.WriteVarint32(value);
          }
        } else if (tag == 10) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedEnumPreserveUnknowns(
                 input,
                 1,
                 ::perfetto::protos::ProcessStatsConfig_Quirks_IsValid,
                 &unknown_fields_stream,
                 this->mutable_quirks())));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(8)) goto parse_quirks;
        if (input->ExpectTag(16)) goto parse_scan_all_processes_on_start;
        break;
      }

      // optional bool scan_all_processes_on_start = 2;
      case 2: {
        if (tag == 16) {
         parse_scan_all_processes_on_start:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   bool, ::google::protobuf::internal::WireFormatLite::TYPE_BOOL>(
                 input, &scan_all_processes_on_start_)));
          set_has_scan_all_processes_on_start();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(24)) goto parse_record_thread_names;
        break;
      }

      // optional bool record_thread_names = 3;
      case 3: {
        if (tag == 24) {
         parse_record_thread_names:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   bool, ::google::protobuf::internal::WireFormatLite::TYPE_BOOL>(
                 input, &record_thread_names_)));
          set_has_record_thread_names();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(32)) goto parse_proc_stats_poll_ms;
        break;
      }

      // optional uint32 proc_stats_poll_ms = 4;
      case 4: {
        if (tag == 32) {
         parse_proc_stats_poll_ms:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::uint32, ::google::protobuf::internal::WireFormatLite::TYPE_UINT32>(
                 input, &proc_stats_poll_ms_)));
          set_has_proc_stats_poll_ms();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(48)) goto parse_proc_stats_cache_ttl_ms;
        break;
      }

      // optional uint32 proc_stats_cache_ttl_ms = 6;
      case 6: {
        if (tag == 48) {
         parse_proc_stats_cache_ttl_ms:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::uint32, ::google::protobuf::internal::WireFormatLite::TYPE_UINT32>(
                 input, &proc_stats_cache_ttl_ms_)));
          set_has_proc_stats_cache_ttl_ms();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:perfetto.protos.ProcessStatsConfig)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:perfetto.protos.ProcessStatsConfig)
  return false;
#undef DO_
}

void ProcessStatsConfig::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:perfetto.protos.ProcessStatsConfig)
  // repeated .perfetto.protos.ProcessStatsConfig.Quirks quirks = 1;
  for (int i = 0; i < this->quirks_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      1, this->quirks(i), output);
  }

  // optional bool scan_all_processes_on_start = 2;
  if (has_scan_all_processes_on_start()) {
    ::google::protobuf::internal::WireFormatLite::WriteBool(2, this->scan_all_processes_on_start(), output);
  }

  // optional bool record_thread_names = 3;
  if (has_record_thread_names()) {
    ::google::protobuf::internal::WireFormatLite::WriteBool(3, this->record_thread_names(), output);
  }

  // optional uint32 proc_stats_poll_ms = 4;
  if (has_proc_stats_poll_ms()) {
    ::google::protobuf::internal::WireFormatLite::WriteUInt32(4, this->proc_stats_poll_ms(), output);
  }

  // optional uint32 proc_stats_cache_ttl_ms = 6;
  if (has_proc_stats_cache_ttl_ms()) {
    ::google::protobuf::internal::WireFormatLite::WriteUInt32(6, this->proc_stats_cache_ttl_ms(), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:perfetto.protos.ProcessStatsConfig)
}

int ProcessStatsConfig::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:perfetto.protos.ProcessStatsConfig)
  int total_size = 0;

  if (_has_bits_[1 / 32] & 30u) {
    // optional bool scan_all_processes_on_start = 2;
    if (has_scan_all_processes_on_start()) {
      total_size += 1 + 1;
    }

    // optional bool record_thread_names = 3;
    if (has_record_thread_names()) {
      total_size += 1 + 1;
    }

    // optional uint32 proc_stats_poll_ms = 4;
    if (has_proc_stats_poll_ms()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::UInt32Size(
          this->proc_stats_poll_ms());
    }

    // optional uint32 proc_stats_cache_ttl_ms = 6;
    if (has_proc_stats_cache_ttl_ms()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::UInt32Size(
          this->proc_stats_cache_ttl_ms());
    }

  }
  // repeated .perfetto.protos.ProcessStatsConfig.Quirks quirks = 1;
  {
    int data_size = 0;
    for (int i = 0; i < this->quirks_size(); i++) {
      data_size += ::google::protobuf::internal::WireFormatLite::EnumSize(
        this->quirks(i));
    }
    total_size += 1 * this->quirks_size() + data_size;
  }

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void ProcessStatsConfig::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const ProcessStatsConfig*>(&from));
}

void ProcessStatsConfig::MergeFrom(const ProcessStatsConfig& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:perfetto.protos.ProcessStatsConfig)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  quirks_.MergeFrom(from.quirks_);
  if (from._has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    if (from.has_scan_all_processes_on_start()) {
      set_scan_all_processes_on_start(from.scan_all_processes_on_start());
    }
    if (from.has_record_thread_names()) {
      set_record_thread_names(from.record_thread_names());
    }
    if (from.has_proc_stats_poll_ms()) {
      set_proc_stats_poll_ms(from.proc_stats_poll_ms());
    }
    if (from.has_proc_stats_cache_ttl_ms()) {
      set_proc_stats_cache_ttl_ms(from.proc_stats_cache_ttl_ms());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void ProcessStatsConfig::CopyFrom(const ProcessStatsConfig& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:perfetto.protos.ProcessStatsConfig)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool ProcessStatsConfig::IsInitialized() const {

  return true;
}

void ProcessStatsConfig::Swap(ProcessStatsConfig* other) {
  if (other == this) return;
  InternalSwap(other);
}
void ProcessStatsConfig::InternalSwap(ProcessStatsConfig* other) {
  quirks_.UnsafeArenaSwap(&other->quirks_);
  std::swap(scan_all_processes_on_start_, other->scan_all_processes_on_start_);
  std::swap(record_thread_names_, other->record_thread_names_);
  std::swap(proc_stats_poll_ms_, other->proc_stats_poll_ms_);
  std::swap(proc_stats_cache_ttl_ms_, other->proc_stats_cache_ttl_ms_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string ProcessStatsConfig::GetTypeName() const {
  return "perfetto.protos.ProcessStatsConfig";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// ProcessStatsConfig

// repeated .perfetto.protos.ProcessStatsConfig.Quirks quirks = 1;
int ProcessStatsConfig::quirks_size() const {
  return quirks_.size();
}
void ProcessStatsConfig::clear_quirks() {
  quirks_.Clear();
}
 ::perfetto::protos::ProcessStatsConfig_Quirks ProcessStatsConfig::quirks(int index) const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ProcessStatsConfig.quirks)
  return static_cast< ::perfetto::protos::ProcessStatsConfig_Quirks >(quirks_.Get(index));
}
 void ProcessStatsConfig::set_quirks(int index, ::perfetto::protos::ProcessStatsConfig_Quirks value) {
  assert(::perfetto::protos::ProcessStatsConfig_Quirks_IsValid(value));
  quirks_.Set(index, value);
  // @@protoc_insertion_point(field_set:perfetto.protos.ProcessStatsConfig.quirks)
}
 void ProcessStatsConfig::add_quirks(::perfetto::protos::ProcessStatsConfig_Quirks value) {
  assert(::perfetto::protos::ProcessStatsConfig_Quirks_IsValid(value));
  quirks_.Add(value);
  // @@protoc_insertion_point(field_add:perfetto.protos.ProcessStatsConfig.quirks)
}
 const ::google::protobuf::RepeatedField<int>&
ProcessStatsConfig::quirks() const {
  // @@protoc_insertion_point(field_list:perfetto.protos.ProcessStatsConfig.quirks)
  return quirks_;
}
 ::google::protobuf::RepeatedField<int>*
ProcessStatsConfig::mutable_quirks() {
  // @@protoc_insertion_point(field_mutable_list:perfetto.protos.ProcessStatsConfig.quirks)
  return &quirks_;
}

// optional bool scan_all_processes_on_start = 2;
bool ProcessStatsConfig::has_scan_all_processes_on_start() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void ProcessStatsConfig::set_has_scan_all_processes_on_start() {
  _has_bits_[0] |= 0x00000002u;
}
void ProcessStatsConfig::clear_has_scan_all_processes_on_start() {
  _has_bits_[0] &= ~0x00000002u;
}
void ProcessStatsConfig::clear_scan_all_processes_on_start() {
  scan_all_processes_on_start_ = false;
  clear_has_scan_all_processes_on_start();
}
 bool ProcessStatsConfig::scan_all_processes_on_start() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ProcessStatsConfig.scan_all_processes_on_start)
  return scan_all_processes_on_start_;
}
 void ProcessStatsConfig::set_scan_all_processes_on_start(bool value) {
  set_has_scan_all_processes_on_start();
  scan_all_processes_on_start_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.ProcessStatsConfig.scan_all_processes_on_start)
}

// optional bool record_thread_names = 3;
bool ProcessStatsConfig::has_record_thread_names() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
void ProcessStatsConfig::set_has_record_thread_names() {
  _has_bits_[0] |= 0x00000004u;
}
void ProcessStatsConfig::clear_has_record_thread_names() {
  _has_bits_[0] &= ~0x00000004u;
}
void ProcessStatsConfig::clear_record_thread_names() {
  record_thread_names_ = false;
  clear_has_record_thread_names();
}
 bool ProcessStatsConfig::record_thread_names() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ProcessStatsConfig.record_thread_names)
  return record_thread_names_;
}
 void ProcessStatsConfig::set_record_thread_names(bool value) {
  set_has_record_thread_names();
  record_thread_names_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.ProcessStatsConfig.record_thread_names)
}

// optional uint32 proc_stats_poll_ms = 4;
bool ProcessStatsConfig::has_proc_stats_poll_ms() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
void ProcessStatsConfig::set_has_proc_stats_poll_ms() {
  _has_bits_[0] |= 0x00000008u;
}
void ProcessStatsConfig::clear_has_proc_stats_poll_ms() {
  _has_bits_[0] &= ~0x00000008u;
}
void ProcessStatsConfig::clear_proc_stats_poll_ms() {
  proc_stats_poll_ms_ = 0u;
  clear_has_proc_stats_poll_ms();
}
 ::google::protobuf::uint32 ProcessStatsConfig::proc_stats_poll_ms() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ProcessStatsConfig.proc_stats_poll_ms)
  return proc_stats_poll_ms_;
}
 void ProcessStatsConfig::set_proc_stats_poll_ms(::google::protobuf::uint32 value) {
  set_has_proc_stats_poll_ms();
  proc_stats_poll_ms_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.ProcessStatsConfig.proc_stats_poll_ms)
}

// optional uint32 proc_stats_cache_ttl_ms = 6;
bool ProcessStatsConfig::has_proc_stats_cache_ttl_ms() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
void ProcessStatsConfig::set_has_proc_stats_cache_ttl_ms() {
  _has_bits_[0] |= 0x00000010u;
}
void ProcessStatsConfig::clear_has_proc_stats_cache_ttl_ms() {
  _has_bits_[0] &= ~0x00000010u;
}
void ProcessStatsConfig::clear_proc_stats_cache_ttl_ms() {
  proc_stats_cache_ttl_ms_ = 0u;
  clear_has_proc_stats_cache_ttl_ms();
}
 ::google::protobuf::uint32 ProcessStatsConfig::proc_stats_cache_ttl_ms() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ProcessStatsConfig.proc_stats_cache_ttl_ms)
  return proc_stats_cache_ttl_ms_;
}
 void ProcessStatsConfig::set_proc_stats_cache_ttl_ms(::google::protobuf::uint32 value) {
  set_has_proc_stats_cache_ttl_ms();
  proc_stats_cache_ttl_ms_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.ProcessStatsConfig.proc_stats_cache_ttl_ms)
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace protos
}  // namespace perfetto

// @@protoc_insertion_point(global_scope)
