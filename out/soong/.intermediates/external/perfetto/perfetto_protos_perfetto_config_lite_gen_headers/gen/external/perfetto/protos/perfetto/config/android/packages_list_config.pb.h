// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: perfetto/config/android/packages_list_config.proto

#ifndef PROTOBUF_perfetto_2fconfig_2fandroid_2fpackages_5flist_5fconfig_2eproto__INCLUDED
#define PROTOBUF_perfetto_2fconfig_2fandroid_2fpackages_5flist_5fconfig_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
// @@protoc_insertion_point(includes)

namespace perfetto {
namespace protos {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_perfetto_2fconfig_2fandroid_2fpackages_5flist_5fconfig_2eproto();
void protobuf_AssignDesc_perfetto_2fconfig_2fandroid_2fpackages_5flist_5fconfig_2eproto();
void protobuf_ShutdownFile_perfetto_2fconfig_2fandroid_2fpackages_5flist_5fconfig_2eproto();

class PackagesListConfig;

// ===================================================================

class PackagesListConfig : public ::google::protobuf::MessageLite {
 public:
  PackagesListConfig();
  virtual ~PackagesListConfig();

  PackagesListConfig(const PackagesListConfig& from);

  inline PackagesListConfig& operator=(const PackagesListConfig& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const PackagesListConfig& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const PackagesListConfig* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(PackagesListConfig* other);

  // implements Message ----------------------------------------------

  inline PackagesListConfig* New() const { return New(NULL); }

  PackagesListConfig* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const PackagesListConfig& from);
  void MergeFrom(const PackagesListConfig& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(PackagesListConfig* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // repeated string package_name_filter = 1;
  int package_name_filter_size() const;
  void clear_package_name_filter();
  static const int kPackageNameFilterFieldNumber = 1;
  const ::std::string& package_name_filter(int index) const;
  ::std::string* mutable_package_name_filter(int index);
  void set_package_name_filter(int index, const ::std::string& value);
  void set_package_name_filter(int index, const char* value);
  void set_package_name_filter(int index, const char* value, size_t size);
  ::std::string* add_package_name_filter();
  void add_package_name_filter(const ::std::string& value);
  void add_package_name_filter(const char* value);
  void add_package_name_filter(const char* value, size_t size);
  const ::google::protobuf::RepeatedPtrField< ::std::string>& package_name_filter() const;
  ::google::protobuf::RepeatedPtrField< ::std::string>* mutable_package_name_filter();

  // @@protoc_insertion_point(class_scope:perfetto.protos.PackagesListConfig)
 private:

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::RepeatedPtrField< ::std::string> package_name_filter_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2fconfig_2fandroid_2fpackages_5flist_5fconfig_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2fconfig_2fandroid_2fpackages_5flist_5fconfig_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2fconfig_2fandroid_2fpackages_5flist_5fconfig_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2fconfig_2fandroid_2fpackages_5flist_5fconfig_2eproto();

  void InitAsDefaultInstance();
  static PackagesListConfig* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// PackagesListConfig

// repeated string package_name_filter = 1;
inline int PackagesListConfig::package_name_filter_size() const {
  return package_name_filter_.size();
}
inline void PackagesListConfig::clear_package_name_filter() {
  package_name_filter_.Clear();
}
inline const ::std::string& PackagesListConfig::package_name_filter(int index) const {
  // @@protoc_insertion_point(field_get:perfetto.protos.PackagesListConfig.package_name_filter)
  return package_name_filter_.Get(index);
}
inline ::std::string* PackagesListConfig::mutable_package_name_filter(int index) {
  // @@protoc_insertion_point(field_mutable:perfetto.protos.PackagesListConfig.package_name_filter)
  return package_name_filter_.Mutable(index);
}
inline void PackagesListConfig::set_package_name_filter(int index, const ::std::string& value) {
  // @@protoc_insertion_point(field_set:perfetto.protos.PackagesListConfig.package_name_filter)
  package_name_filter_.Mutable(index)->assign(value);
}
inline void PackagesListConfig::set_package_name_filter(int index, const char* value) {
  package_name_filter_.Mutable(index)->assign(value);
  // @@protoc_insertion_point(field_set_char:perfetto.protos.PackagesListConfig.package_name_filter)
}
inline void PackagesListConfig::set_package_name_filter(int index, const char* value, size_t size) {
  package_name_filter_.Mutable(index)->assign(
    reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:perfetto.protos.PackagesListConfig.package_name_filter)
}
inline ::std::string* PackagesListConfig::add_package_name_filter() {
  // @@protoc_insertion_point(field_add_mutable:perfetto.protos.PackagesListConfig.package_name_filter)
  return package_name_filter_.Add();
}
inline void PackagesListConfig::add_package_name_filter(const ::std::string& value) {
  package_name_filter_.Add()->assign(value);
  // @@protoc_insertion_point(field_add:perfetto.protos.PackagesListConfig.package_name_filter)
}
inline void PackagesListConfig::add_package_name_filter(const char* value) {
  package_name_filter_.Add()->assign(value);
  // @@protoc_insertion_point(field_add_char:perfetto.protos.PackagesListConfig.package_name_filter)
}
inline void PackagesListConfig::add_package_name_filter(const char* value, size_t size) {
  package_name_filter_.Add()->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_add_pointer:perfetto.protos.PackagesListConfig.package_name_filter)
}
inline const ::google::protobuf::RepeatedPtrField< ::std::string>&
PackagesListConfig::package_name_filter() const {
  // @@protoc_insertion_point(field_list:perfetto.protos.PackagesListConfig.package_name_filter)
  return package_name_filter_;
}
inline ::google::protobuf::RepeatedPtrField< ::std::string>*
PackagesListConfig::mutable_package_name_filter() {
  // @@protoc_insertion_point(field_mutable_list:perfetto.protos.PackagesListConfig.package_name_filter)
  return &package_name_filter_;
}

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace protos
}  // namespace perfetto

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_perfetto_2fconfig_2fandroid_2fpackages_5flist_5fconfig_2eproto__INCLUDED
