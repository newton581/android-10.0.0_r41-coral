// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: perfetto/trace/track_event/thread_descriptor.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "perfetto/trace/track_event/thread_descriptor.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace perfetto {
namespace protos {

void protobuf_ShutdownFile_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto() {
  delete ThreadDescriptor::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ThreadDescriptor::default_instance_ = new ThreadDescriptor();
  ThreadDescriptor::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto_once_);
void protobuf_AddDesc_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto_once_,
                 &protobuf_AddDesc_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto {
  StaticDescriptorInitializer_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto() {
    protobuf_AddDesc_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto();
  }
} static_descriptor_initializer_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForThreadDescriptor(
    ThreadDescriptor* ptr) {
  return ptr->mutable_unknown_fields();
}

bool ThreadDescriptor_ChromeThreadType_IsValid(int value) {
  switch(value) {
    case 0:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const ThreadDescriptor_ChromeThreadType ThreadDescriptor::THREAD_UNSPECIFIED;
const ThreadDescriptor_ChromeThreadType ThreadDescriptor::ChromeThreadType_MIN;
const ThreadDescriptor_ChromeThreadType ThreadDescriptor::ChromeThreadType_MAX;
const int ThreadDescriptor::ChromeThreadType_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int ThreadDescriptor::kPidFieldNumber;
const int ThreadDescriptor::kTidFieldNumber;
const int ThreadDescriptor::kLegacySortIndexFieldNumber;
const int ThreadDescriptor::kChromeThreadTypeFieldNumber;
const int ThreadDescriptor::kThreadNameFieldNumber;
const int ThreadDescriptor::kReferenceTimestampUsFieldNumber;
const int ThreadDescriptor::kReferenceThreadTimeUsFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

ThreadDescriptor::ThreadDescriptor()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:perfetto.protos.ThreadDescriptor)
}

void ThreadDescriptor::InitAsDefaultInstance() {
}

ThreadDescriptor::ThreadDescriptor(const ThreadDescriptor& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:perfetto.protos.ThreadDescriptor)
}

void ThreadDescriptor::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  pid_ = 0;
  tid_ = 0;
  legacy_sort_index_ = 0;
  chrome_thread_type_ = 0;
  thread_name_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  reference_timestamp_us_ = GOOGLE_LONGLONG(0);
  reference_thread_time_us_ = GOOGLE_LONGLONG(0);
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

ThreadDescriptor::~ThreadDescriptor() {
  // @@protoc_insertion_point(destructor:perfetto.protos.ThreadDescriptor)
  SharedDtor();
}

void ThreadDescriptor::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  thread_name_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void ThreadDescriptor::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ThreadDescriptor& ThreadDescriptor::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_perfetto_2ftrace_2ftrack_5fevent_2fthread_5fdescriptor_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

ThreadDescriptor* ThreadDescriptor::default_instance_ = NULL;

ThreadDescriptor* ThreadDescriptor::New(::google::protobuf::Arena* arena) const {
  ThreadDescriptor* n = new ThreadDescriptor;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void ThreadDescriptor::Clear() {
// @@protoc_insertion_point(message_clear_start:perfetto.protos.ThreadDescriptor)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(ThreadDescriptor, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<ThreadDescriptor*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  if (_has_bits_[0 / 32] & 127u) {
    ZR_(pid_, chrome_thread_type_);
    ZR_(reference_timestamp_us_, reference_thread_time_us_);
    if (has_thread_name()) {
      thread_name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    }
  }

#undef ZR_HELPER_
#undef ZR_

  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool ThreadDescriptor::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForThreadDescriptor, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:perfetto.protos.ThreadDescriptor)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional int32 pid = 1;
      case 1: {
        if (tag == 8) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &pid_)));
          set_has_pid();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(16)) goto parse_tid;
        break;
      }

      // optional int32 tid = 2;
      case 2: {
        if (tag == 16) {
         parse_tid:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &tid_)));
          set_has_tid();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(24)) goto parse_legacy_sort_index;
        break;
      }

      // optional int32 legacy_sort_index = 3;
      case 3: {
        if (tag == 24) {
         parse_legacy_sort_index:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &legacy_sort_index_)));
          set_has_legacy_sort_index();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(32)) goto parse_chrome_thread_type;
        break;
      }

      // optional .perfetto.protos.ThreadDescriptor.ChromeThreadType chrome_thread_type = 4;
      case 4: {
        if (tag == 32) {
         parse_chrome_thread_type:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          if (::perfetto::protos::ThreadDescriptor_ChromeThreadType_IsValid(value)) {
            set_chrome_thread_type(static_cast< ::perfetto::protos::ThreadDescriptor_ChromeThreadType >(value));
          } else {
            unknown_fields_stream.WriteVarint32(32);
            unknown_fields_stream.WriteVarint32(value);
          }
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(42)) goto parse_thread_name;
        break;
      }

      // optional string thread_name = 5;
      case 5: {
        if (tag == 42) {
         parse_thread_name:
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_thread_name()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(48)) goto parse_reference_timestamp_us;
        break;
      }

      // optional int64 reference_timestamp_us = 6;
      case 6: {
        if (tag == 48) {
         parse_reference_timestamp_us:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int64, ::google::protobuf::internal::WireFormatLite::TYPE_INT64>(
                 input, &reference_timestamp_us_)));
          set_has_reference_timestamp_us();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(56)) goto parse_reference_thread_time_us;
        break;
      }

      // optional int64 reference_thread_time_us = 7;
      case 7: {
        if (tag == 56) {
         parse_reference_thread_time_us:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int64, ::google::protobuf::internal::WireFormatLite::TYPE_INT64>(
                 input, &reference_thread_time_us_)));
          set_has_reference_thread_time_us();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:perfetto.protos.ThreadDescriptor)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:perfetto.protos.ThreadDescriptor)
  return false;
#undef DO_
}

void ThreadDescriptor::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:perfetto.protos.ThreadDescriptor)
  // optional int32 pid = 1;
  if (has_pid()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(1, this->pid(), output);
  }

  // optional int32 tid = 2;
  if (has_tid()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(2, this->tid(), output);
  }

  // optional int32 legacy_sort_index = 3;
  if (has_legacy_sort_index()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(3, this->legacy_sort_index(), output);
  }

  // optional .perfetto.protos.ThreadDescriptor.ChromeThreadType chrome_thread_type = 4;
  if (has_chrome_thread_type()) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      4, this->chrome_thread_type(), output);
  }

  // optional string thread_name = 5;
  if (has_thread_name()) {
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      5, this->thread_name(), output);
  }

  // optional int64 reference_timestamp_us = 6;
  if (has_reference_timestamp_us()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt64(6, this->reference_timestamp_us(), output);
  }

  // optional int64 reference_thread_time_us = 7;
  if (has_reference_thread_time_us()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt64(7, this->reference_thread_time_us(), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:perfetto.protos.ThreadDescriptor)
}

int ThreadDescriptor::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:perfetto.protos.ThreadDescriptor)
  int total_size = 0;

  if (_has_bits_[0 / 32] & 127u) {
    // optional int32 pid = 1;
    if (has_pid()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->pid());
    }

    // optional int32 tid = 2;
    if (has_tid()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->tid());
    }

    // optional int32 legacy_sort_index = 3;
    if (has_legacy_sort_index()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->legacy_sort_index());
    }

    // optional .perfetto.protos.ThreadDescriptor.ChromeThreadType chrome_thread_type = 4;
    if (has_chrome_thread_type()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::EnumSize(this->chrome_thread_type());
    }

    // optional string thread_name = 5;
    if (has_thread_name()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::StringSize(
          this->thread_name());
    }

    // optional int64 reference_timestamp_us = 6;
    if (has_reference_timestamp_us()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int64Size(
          this->reference_timestamp_us());
    }

    // optional int64 reference_thread_time_us = 7;
    if (has_reference_thread_time_us()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int64Size(
          this->reference_thread_time_us());
    }

  }
  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void ThreadDescriptor::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const ThreadDescriptor*>(&from));
}

void ThreadDescriptor::MergeFrom(const ThreadDescriptor& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:perfetto.protos.ThreadDescriptor)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_pid()) {
      set_pid(from.pid());
    }
    if (from.has_tid()) {
      set_tid(from.tid());
    }
    if (from.has_legacy_sort_index()) {
      set_legacy_sort_index(from.legacy_sort_index());
    }
    if (from.has_chrome_thread_type()) {
      set_chrome_thread_type(from.chrome_thread_type());
    }
    if (from.has_thread_name()) {
      set_has_thread_name();
      thread_name_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.thread_name_);
    }
    if (from.has_reference_timestamp_us()) {
      set_reference_timestamp_us(from.reference_timestamp_us());
    }
    if (from.has_reference_thread_time_us()) {
      set_reference_thread_time_us(from.reference_thread_time_us());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void ThreadDescriptor::CopyFrom(const ThreadDescriptor& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:perfetto.protos.ThreadDescriptor)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool ThreadDescriptor::IsInitialized() const {

  return true;
}

void ThreadDescriptor::Swap(ThreadDescriptor* other) {
  if (other == this) return;
  InternalSwap(other);
}
void ThreadDescriptor::InternalSwap(ThreadDescriptor* other) {
  std::swap(pid_, other->pid_);
  std::swap(tid_, other->tid_);
  std::swap(legacy_sort_index_, other->legacy_sort_index_);
  std::swap(chrome_thread_type_, other->chrome_thread_type_);
  thread_name_.Swap(&other->thread_name_);
  std::swap(reference_timestamp_us_, other->reference_timestamp_us_);
  std::swap(reference_thread_time_us_, other->reference_thread_time_us_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string ThreadDescriptor::GetTypeName() const {
  return "perfetto.protos.ThreadDescriptor";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// ThreadDescriptor

// optional int32 pid = 1;
bool ThreadDescriptor::has_pid() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
void ThreadDescriptor::set_has_pid() {
  _has_bits_[0] |= 0x00000001u;
}
void ThreadDescriptor::clear_has_pid() {
  _has_bits_[0] &= ~0x00000001u;
}
void ThreadDescriptor::clear_pid() {
  pid_ = 0;
  clear_has_pid();
}
 ::google::protobuf::int32 ThreadDescriptor::pid() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ThreadDescriptor.pid)
  return pid_;
}
 void ThreadDescriptor::set_pid(::google::protobuf::int32 value) {
  set_has_pid();
  pid_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.ThreadDescriptor.pid)
}

// optional int32 tid = 2;
bool ThreadDescriptor::has_tid() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void ThreadDescriptor::set_has_tid() {
  _has_bits_[0] |= 0x00000002u;
}
void ThreadDescriptor::clear_has_tid() {
  _has_bits_[0] &= ~0x00000002u;
}
void ThreadDescriptor::clear_tid() {
  tid_ = 0;
  clear_has_tid();
}
 ::google::protobuf::int32 ThreadDescriptor::tid() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ThreadDescriptor.tid)
  return tid_;
}
 void ThreadDescriptor::set_tid(::google::protobuf::int32 value) {
  set_has_tid();
  tid_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.ThreadDescriptor.tid)
}

// optional int32 legacy_sort_index = 3;
bool ThreadDescriptor::has_legacy_sort_index() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
void ThreadDescriptor::set_has_legacy_sort_index() {
  _has_bits_[0] |= 0x00000004u;
}
void ThreadDescriptor::clear_has_legacy_sort_index() {
  _has_bits_[0] &= ~0x00000004u;
}
void ThreadDescriptor::clear_legacy_sort_index() {
  legacy_sort_index_ = 0;
  clear_has_legacy_sort_index();
}
 ::google::protobuf::int32 ThreadDescriptor::legacy_sort_index() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ThreadDescriptor.legacy_sort_index)
  return legacy_sort_index_;
}
 void ThreadDescriptor::set_legacy_sort_index(::google::protobuf::int32 value) {
  set_has_legacy_sort_index();
  legacy_sort_index_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.ThreadDescriptor.legacy_sort_index)
}

// optional .perfetto.protos.ThreadDescriptor.ChromeThreadType chrome_thread_type = 4;
bool ThreadDescriptor::has_chrome_thread_type() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
void ThreadDescriptor::set_has_chrome_thread_type() {
  _has_bits_[0] |= 0x00000008u;
}
void ThreadDescriptor::clear_has_chrome_thread_type() {
  _has_bits_[0] &= ~0x00000008u;
}
void ThreadDescriptor::clear_chrome_thread_type() {
  chrome_thread_type_ = 0;
  clear_has_chrome_thread_type();
}
 ::perfetto::protos::ThreadDescriptor_ChromeThreadType ThreadDescriptor::chrome_thread_type() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ThreadDescriptor.chrome_thread_type)
  return static_cast< ::perfetto::protos::ThreadDescriptor_ChromeThreadType >(chrome_thread_type_);
}
 void ThreadDescriptor::set_chrome_thread_type(::perfetto::protos::ThreadDescriptor_ChromeThreadType value) {
  assert(::perfetto::protos::ThreadDescriptor_ChromeThreadType_IsValid(value));
  set_has_chrome_thread_type();
  chrome_thread_type_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.ThreadDescriptor.chrome_thread_type)
}

// optional string thread_name = 5;
bool ThreadDescriptor::has_thread_name() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
void ThreadDescriptor::set_has_thread_name() {
  _has_bits_[0] |= 0x00000010u;
}
void ThreadDescriptor::clear_has_thread_name() {
  _has_bits_[0] &= ~0x00000010u;
}
void ThreadDescriptor::clear_thread_name() {
  thread_name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_thread_name();
}
 const ::std::string& ThreadDescriptor::thread_name() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ThreadDescriptor.thread_name)
  return thread_name_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void ThreadDescriptor::set_thread_name(const ::std::string& value) {
  set_has_thread_name();
  thread_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:perfetto.protos.ThreadDescriptor.thread_name)
}
 void ThreadDescriptor::set_thread_name(const char* value) {
  set_has_thread_name();
  thread_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:perfetto.protos.ThreadDescriptor.thread_name)
}
 void ThreadDescriptor::set_thread_name(const char* value, size_t size) {
  set_has_thread_name();
  thread_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:perfetto.protos.ThreadDescriptor.thread_name)
}
 ::std::string* ThreadDescriptor::mutable_thread_name() {
  set_has_thread_name();
  // @@protoc_insertion_point(field_mutable:perfetto.protos.ThreadDescriptor.thread_name)
  return thread_name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 ::std::string* ThreadDescriptor::release_thread_name() {
  // @@protoc_insertion_point(field_release:perfetto.protos.ThreadDescriptor.thread_name)
  clear_has_thread_name();
  return thread_name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void ThreadDescriptor::set_allocated_thread_name(::std::string* thread_name) {
  if (thread_name != NULL) {
    set_has_thread_name();
  } else {
    clear_has_thread_name();
  }
  thread_name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), thread_name);
  // @@protoc_insertion_point(field_set_allocated:perfetto.protos.ThreadDescriptor.thread_name)
}

// optional int64 reference_timestamp_us = 6;
bool ThreadDescriptor::has_reference_timestamp_us() const {
  return (_has_bits_[0] & 0x00000020u) != 0;
}
void ThreadDescriptor::set_has_reference_timestamp_us() {
  _has_bits_[0] |= 0x00000020u;
}
void ThreadDescriptor::clear_has_reference_timestamp_us() {
  _has_bits_[0] &= ~0x00000020u;
}
void ThreadDescriptor::clear_reference_timestamp_us() {
  reference_timestamp_us_ = GOOGLE_LONGLONG(0);
  clear_has_reference_timestamp_us();
}
 ::google::protobuf::int64 ThreadDescriptor::reference_timestamp_us() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ThreadDescriptor.reference_timestamp_us)
  return reference_timestamp_us_;
}
 void ThreadDescriptor::set_reference_timestamp_us(::google::protobuf::int64 value) {
  set_has_reference_timestamp_us();
  reference_timestamp_us_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.ThreadDescriptor.reference_timestamp_us)
}

// optional int64 reference_thread_time_us = 7;
bool ThreadDescriptor::has_reference_thread_time_us() const {
  return (_has_bits_[0] & 0x00000040u) != 0;
}
void ThreadDescriptor::set_has_reference_thread_time_us() {
  _has_bits_[0] |= 0x00000040u;
}
void ThreadDescriptor::clear_has_reference_thread_time_us() {
  _has_bits_[0] &= ~0x00000040u;
}
void ThreadDescriptor::clear_reference_thread_time_us() {
  reference_thread_time_us_ = GOOGLE_LONGLONG(0);
  clear_has_reference_thread_time_us();
}
 ::google::protobuf::int64 ThreadDescriptor::reference_thread_time_us() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.ThreadDescriptor.reference_thread_time_us)
  return reference_thread_time_us_;
}
 void ThreadDescriptor::set_reference_thread_time_us(::google::protobuf::int64 value) {
  set_has_reference_thread_time_us();
  reference_thread_time_us_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.ThreadDescriptor.reference_thread_time_us)
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace protos
}  // namespace perfetto

// @@protoc_insertion_point(global_scope)
