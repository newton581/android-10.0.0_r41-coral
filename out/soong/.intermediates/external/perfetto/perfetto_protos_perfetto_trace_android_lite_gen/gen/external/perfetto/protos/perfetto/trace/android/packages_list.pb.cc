// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: perfetto/trace/android/packages_list.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "perfetto/trace/android/packages_list.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace perfetto {
namespace protos {

void protobuf_ShutdownFile_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto() {
  delete PackagesList::default_instance_;
  delete PackagesList_PackageInfo::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  PackagesList::default_instance_ = new PackagesList();
  PackagesList_PackageInfo::default_instance_ = new PackagesList_PackageInfo();
  PackagesList::default_instance_->InitAsDefaultInstance();
  PackagesList_PackageInfo::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto_once_);
void protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto_once_,
                 &protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto {
  StaticDescriptorInitializer_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto() {
    protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto();
  }
} static_descriptor_initializer_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForPackagesList(
    PackagesList* ptr) {
  return ptr->mutable_unknown_fields();
}

static ::std::string* MutableUnknownFieldsForPackagesList_PackageInfo(
    PackagesList_PackageInfo* ptr) {
  return ptr->mutable_unknown_fields();
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int PackagesList_PackageInfo::kNameFieldNumber;
const int PackagesList_PackageInfo::kUidFieldNumber;
const int PackagesList_PackageInfo::kDebuggableFieldNumber;
const int PackagesList_PackageInfo::kProfileableFromShellFieldNumber;
const int PackagesList_PackageInfo::kVersionCodeFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

PackagesList_PackageInfo::PackagesList_PackageInfo()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:perfetto.protos.PackagesList.PackageInfo)
}

void PackagesList_PackageInfo::InitAsDefaultInstance() {
}

PackagesList_PackageInfo::PackagesList_PackageInfo(const PackagesList_PackageInfo& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:perfetto.protos.PackagesList.PackageInfo)
}

void PackagesList_PackageInfo::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  name_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  uid_ = GOOGLE_ULONGLONG(0);
  debuggable_ = false;
  profileable_from_shell_ = false;
  version_code_ = GOOGLE_LONGLONG(0);
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

PackagesList_PackageInfo::~PackagesList_PackageInfo() {
  // @@protoc_insertion_point(destructor:perfetto.protos.PackagesList.PackageInfo)
  SharedDtor();
}

void PackagesList_PackageInfo::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  name_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void PackagesList_PackageInfo::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const PackagesList_PackageInfo& PackagesList_PackageInfo::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

PackagesList_PackageInfo* PackagesList_PackageInfo::default_instance_ = NULL;

PackagesList_PackageInfo* PackagesList_PackageInfo::New(::google::protobuf::Arena* arena) const {
  PackagesList_PackageInfo* n = new PackagesList_PackageInfo;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void PackagesList_PackageInfo::Clear() {
// @@protoc_insertion_point(message_clear_start:perfetto.protos.PackagesList.PackageInfo)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(PackagesList_PackageInfo, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<PackagesList_PackageInfo*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  if (_has_bits_[0 / 32] & 31u) {
    ZR_(uid_, profileable_from_shell_);
    if (has_name()) {
      name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    }
  }

#undef ZR_HELPER_
#undef ZR_

  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool PackagesList_PackageInfo::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForPackagesList_PackageInfo, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:perfetto.protos.PackagesList.PackageInfo)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional string name = 1;
      case 1: {
        if (tag == 10) {
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_name()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(16)) goto parse_uid;
        break;
      }

      // optional uint64 uid = 2;
      case 2: {
        if (tag == 16) {
         parse_uid:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::uint64, ::google::protobuf::internal::WireFormatLite::TYPE_UINT64>(
                 input, &uid_)));
          set_has_uid();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(24)) goto parse_debuggable;
        break;
      }

      // optional bool debuggable = 3;
      case 3: {
        if (tag == 24) {
         parse_debuggable:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   bool, ::google::protobuf::internal::WireFormatLite::TYPE_BOOL>(
                 input, &debuggable_)));
          set_has_debuggable();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(32)) goto parse_profileable_from_shell;
        break;
      }

      // optional bool profileable_from_shell = 4;
      case 4: {
        if (tag == 32) {
         parse_profileable_from_shell:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   bool, ::google::protobuf::internal::WireFormatLite::TYPE_BOOL>(
                 input, &profileable_from_shell_)));
          set_has_profileable_from_shell();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(40)) goto parse_version_code;
        break;
      }

      // optional int64 version_code = 5;
      case 5: {
        if (tag == 40) {
         parse_version_code:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int64, ::google::protobuf::internal::WireFormatLite::TYPE_INT64>(
                 input, &version_code_)));
          set_has_version_code();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:perfetto.protos.PackagesList.PackageInfo)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:perfetto.protos.PackagesList.PackageInfo)
  return false;
#undef DO_
}

void PackagesList_PackageInfo::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:perfetto.protos.PackagesList.PackageInfo)
  // optional string name = 1;
  if (has_name()) {
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      1, this->name(), output);
  }

  // optional uint64 uid = 2;
  if (has_uid()) {
    ::google::protobuf::internal::WireFormatLite::WriteUInt64(2, this->uid(), output);
  }

  // optional bool debuggable = 3;
  if (has_debuggable()) {
    ::google::protobuf::internal::WireFormatLite::WriteBool(3, this->debuggable(), output);
  }

  // optional bool profileable_from_shell = 4;
  if (has_profileable_from_shell()) {
    ::google::protobuf::internal::WireFormatLite::WriteBool(4, this->profileable_from_shell(), output);
  }

  // optional int64 version_code = 5;
  if (has_version_code()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt64(5, this->version_code(), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:perfetto.protos.PackagesList.PackageInfo)
}

int PackagesList_PackageInfo::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:perfetto.protos.PackagesList.PackageInfo)
  int total_size = 0;

  if (_has_bits_[0 / 32] & 31u) {
    // optional string name = 1;
    if (has_name()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::StringSize(
          this->name());
    }

    // optional uint64 uid = 2;
    if (has_uid()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::UInt64Size(
          this->uid());
    }

    // optional bool debuggable = 3;
    if (has_debuggable()) {
      total_size += 1 + 1;
    }

    // optional bool profileable_from_shell = 4;
    if (has_profileable_from_shell()) {
      total_size += 1 + 1;
    }

    // optional int64 version_code = 5;
    if (has_version_code()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int64Size(
          this->version_code());
    }

  }
  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void PackagesList_PackageInfo::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const PackagesList_PackageInfo*>(&from));
}

void PackagesList_PackageInfo::MergeFrom(const PackagesList_PackageInfo& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:perfetto.protos.PackagesList.PackageInfo)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_name()) {
      set_has_name();
      name_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.name_);
    }
    if (from.has_uid()) {
      set_uid(from.uid());
    }
    if (from.has_debuggable()) {
      set_debuggable(from.debuggable());
    }
    if (from.has_profileable_from_shell()) {
      set_profileable_from_shell(from.profileable_from_shell());
    }
    if (from.has_version_code()) {
      set_version_code(from.version_code());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void PackagesList_PackageInfo::CopyFrom(const PackagesList_PackageInfo& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:perfetto.protos.PackagesList.PackageInfo)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool PackagesList_PackageInfo::IsInitialized() const {

  return true;
}

void PackagesList_PackageInfo::Swap(PackagesList_PackageInfo* other) {
  if (other == this) return;
  InternalSwap(other);
}
void PackagesList_PackageInfo::InternalSwap(PackagesList_PackageInfo* other) {
  name_.Swap(&other->name_);
  std::swap(uid_, other->uid_);
  std::swap(debuggable_, other->debuggable_);
  std::swap(profileable_from_shell_, other->profileable_from_shell_);
  std::swap(version_code_, other->version_code_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string PackagesList_PackageInfo::GetTypeName() const {
  return "perfetto.protos.PackagesList.PackageInfo";
}


// -------------------------------------------------------------------

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int PackagesList::kPackagesFieldNumber;
const int PackagesList::kParseErrorFieldNumber;
const int PackagesList::kReadErrorFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

PackagesList::PackagesList()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:perfetto.protos.PackagesList)
}

void PackagesList::InitAsDefaultInstance() {
}

PackagesList::PackagesList(const PackagesList& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:perfetto.protos.PackagesList)
}

void PackagesList::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  parse_error_ = false;
  read_error_ = false;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

PackagesList::~PackagesList() {
  // @@protoc_insertion_point(destructor:perfetto.protos.PackagesList)
  SharedDtor();
}

void PackagesList::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void PackagesList::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const PackagesList& PackagesList::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_perfetto_2ftrace_2fandroid_2fpackages_5flist_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

PackagesList* PackagesList::default_instance_ = NULL;

PackagesList* PackagesList::New(::google::protobuf::Arena* arena) const {
  PackagesList* n = new PackagesList;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void PackagesList::Clear() {
// @@protoc_insertion_point(message_clear_start:perfetto.protos.PackagesList)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(PackagesList, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<PackagesList*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  ZR_(parse_error_, read_error_);

#undef ZR_HELPER_
#undef ZR_

  packages_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool PackagesList::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForPackagesList, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:perfetto.protos.PackagesList)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // repeated .perfetto.protos.PackagesList.PackageInfo packages = 1;
      case 1: {
        if (tag == 10) {
          DO_(input->IncrementRecursionDepth());
         parse_loop_packages:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtualNoRecursionDepth(
                input, add_packages()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(10)) goto parse_loop_packages;
        input->UnsafeDecrementRecursionDepth();
        if (input->ExpectTag(16)) goto parse_parse_error;
        break;
      }

      // optional bool parse_error = 2;
      case 2: {
        if (tag == 16) {
         parse_parse_error:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   bool, ::google::protobuf::internal::WireFormatLite::TYPE_BOOL>(
                 input, &parse_error_)));
          set_has_parse_error();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(24)) goto parse_read_error;
        break;
      }

      // optional bool read_error = 3;
      case 3: {
        if (tag == 24) {
         parse_read_error:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   bool, ::google::protobuf::internal::WireFormatLite::TYPE_BOOL>(
                 input, &read_error_)));
          set_has_read_error();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:perfetto.protos.PackagesList)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:perfetto.protos.PackagesList)
  return false;
#undef DO_
}

void PackagesList::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:perfetto.protos.PackagesList)
  // repeated .perfetto.protos.PackagesList.PackageInfo packages = 1;
  for (unsigned int i = 0, n = this->packages_size(); i < n; i++) {
    ::google::protobuf::internal::WireFormatLite::WriteMessage(
      1, this->packages(i), output);
  }

  // optional bool parse_error = 2;
  if (has_parse_error()) {
    ::google::protobuf::internal::WireFormatLite::WriteBool(2, this->parse_error(), output);
  }

  // optional bool read_error = 3;
  if (has_read_error()) {
    ::google::protobuf::internal::WireFormatLite::WriteBool(3, this->read_error(), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:perfetto.protos.PackagesList)
}

int PackagesList::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:perfetto.protos.PackagesList)
  int total_size = 0;

  if (_has_bits_[1 / 32] & 6u) {
    // optional bool parse_error = 2;
    if (has_parse_error()) {
      total_size += 1 + 1;
    }

    // optional bool read_error = 3;
    if (has_read_error()) {
      total_size += 1 + 1;
    }

  }
  // repeated .perfetto.protos.PackagesList.PackageInfo packages = 1;
  total_size += 1 * this->packages_size();
  for (int i = 0; i < this->packages_size(); i++) {
    total_size +=
      ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
        this->packages(i));
  }

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void PackagesList::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const PackagesList*>(&from));
}

void PackagesList::MergeFrom(const PackagesList& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:perfetto.protos.PackagesList)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  packages_.MergeFrom(from.packages_);
  if (from._has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    if (from.has_parse_error()) {
      set_parse_error(from.parse_error());
    }
    if (from.has_read_error()) {
      set_read_error(from.read_error());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void PackagesList::CopyFrom(const PackagesList& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:perfetto.protos.PackagesList)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool PackagesList::IsInitialized() const {

  return true;
}

void PackagesList::Swap(PackagesList* other) {
  if (other == this) return;
  InternalSwap(other);
}
void PackagesList::InternalSwap(PackagesList* other) {
  packages_.UnsafeArenaSwap(&other->packages_);
  std::swap(parse_error_, other->parse_error_);
  std::swap(read_error_, other->read_error_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string PackagesList::GetTypeName() const {
  return "perfetto.protos.PackagesList";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// PackagesList_PackageInfo

// optional string name = 1;
bool PackagesList_PackageInfo::has_name() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
void PackagesList_PackageInfo::set_has_name() {
  _has_bits_[0] |= 0x00000001u;
}
void PackagesList_PackageInfo::clear_has_name() {
  _has_bits_[0] &= ~0x00000001u;
}
void PackagesList_PackageInfo::clear_name() {
  name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_name();
}
 const ::std::string& PackagesList_PackageInfo::name() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.PackagesList.PackageInfo.name)
  return name_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void PackagesList_PackageInfo::set_name(const ::std::string& value) {
  set_has_name();
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:perfetto.protos.PackagesList.PackageInfo.name)
}
 void PackagesList_PackageInfo::set_name(const char* value) {
  set_has_name();
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:perfetto.protos.PackagesList.PackageInfo.name)
}
 void PackagesList_PackageInfo::set_name(const char* value, size_t size) {
  set_has_name();
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:perfetto.protos.PackagesList.PackageInfo.name)
}
 ::std::string* PackagesList_PackageInfo::mutable_name() {
  set_has_name();
  // @@protoc_insertion_point(field_mutable:perfetto.protos.PackagesList.PackageInfo.name)
  return name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 ::std::string* PackagesList_PackageInfo::release_name() {
  // @@protoc_insertion_point(field_release:perfetto.protos.PackagesList.PackageInfo.name)
  clear_has_name();
  return name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void PackagesList_PackageInfo::set_allocated_name(::std::string* name) {
  if (name != NULL) {
    set_has_name();
  } else {
    clear_has_name();
  }
  name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), name);
  // @@protoc_insertion_point(field_set_allocated:perfetto.protos.PackagesList.PackageInfo.name)
}

// optional uint64 uid = 2;
bool PackagesList_PackageInfo::has_uid() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void PackagesList_PackageInfo::set_has_uid() {
  _has_bits_[0] |= 0x00000002u;
}
void PackagesList_PackageInfo::clear_has_uid() {
  _has_bits_[0] &= ~0x00000002u;
}
void PackagesList_PackageInfo::clear_uid() {
  uid_ = GOOGLE_ULONGLONG(0);
  clear_has_uid();
}
 ::google::protobuf::uint64 PackagesList_PackageInfo::uid() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.PackagesList.PackageInfo.uid)
  return uid_;
}
 void PackagesList_PackageInfo::set_uid(::google::protobuf::uint64 value) {
  set_has_uid();
  uid_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.PackagesList.PackageInfo.uid)
}

// optional bool debuggable = 3;
bool PackagesList_PackageInfo::has_debuggable() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
void PackagesList_PackageInfo::set_has_debuggable() {
  _has_bits_[0] |= 0x00000004u;
}
void PackagesList_PackageInfo::clear_has_debuggable() {
  _has_bits_[0] &= ~0x00000004u;
}
void PackagesList_PackageInfo::clear_debuggable() {
  debuggable_ = false;
  clear_has_debuggable();
}
 bool PackagesList_PackageInfo::debuggable() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.PackagesList.PackageInfo.debuggable)
  return debuggable_;
}
 void PackagesList_PackageInfo::set_debuggable(bool value) {
  set_has_debuggable();
  debuggable_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.PackagesList.PackageInfo.debuggable)
}

// optional bool profileable_from_shell = 4;
bool PackagesList_PackageInfo::has_profileable_from_shell() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
void PackagesList_PackageInfo::set_has_profileable_from_shell() {
  _has_bits_[0] |= 0x00000008u;
}
void PackagesList_PackageInfo::clear_has_profileable_from_shell() {
  _has_bits_[0] &= ~0x00000008u;
}
void PackagesList_PackageInfo::clear_profileable_from_shell() {
  profileable_from_shell_ = false;
  clear_has_profileable_from_shell();
}
 bool PackagesList_PackageInfo::profileable_from_shell() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.PackagesList.PackageInfo.profileable_from_shell)
  return profileable_from_shell_;
}
 void PackagesList_PackageInfo::set_profileable_from_shell(bool value) {
  set_has_profileable_from_shell();
  profileable_from_shell_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.PackagesList.PackageInfo.profileable_from_shell)
}

// optional int64 version_code = 5;
bool PackagesList_PackageInfo::has_version_code() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
void PackagesList_PackageInfo::set_has_version_code() {
  _has_bits_[0] |= 0x00000010u;
}
void PackagesList_PackageInfo::clear_has_version_code() {
  _has_bits_[0] &= ~0x00000010u;
}
void PackagesList_PackageInfo::clear_version_code() {
  version_code_ = GOOGLE_LONGLONG(0);
  clear_has_version_code();
}
 ::google::protobuf::int64 PackagesList_PackageInfo::version_code() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.PackagesList.PackageInfo.version_code)
  return version_code_;
}
 void PackagesList_PackageInfo::set_version_code(::google::protobuf::int64 value) {
  set_has_version_code();
  version_code_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.PackagesList.PackageInfo.version_code)
}

// -------------------------------------------------------------------

// PackagesList

// repeated .perfetto.protos.PackagesList.PackageInfo packages = 1;
int PackagesList::packages_size() const {
  return packages_.size();
}
void PackagesList::clear_packages() {
  packages_.Clear();
}
const ::perfetto::protos::PackagesList_PackageInfo& PackagesList::packages(int index) const {
  // @@protoc_insertion_point(field_get:perfetto.protos.PackagesList.packages)
  return packages_.Get(index);
}
::perfetto::protos::PackagesList_PackageInfo* PackagesList::mutable_packages(int index) {
  // @@protoc_insertion_point(field_mutable:perfetto.protos.PackagesList.packages)
  return packages_.Mutable(index);
}
::perfetto::protos::PackagesList_PackageInfo* PackagesList::add_packages() {
  // @@protoc_insertion_point(field_add:perfetto.protos.PackagesList.packages)
  return packages_.Add();
}
::google::protobuf::RepeatedPtrField< ::perfetto::protos::PackagesList_PackageInfo >*
PackagesList::mutable_packages() {
  // @@protoc_insertion_point(field_mutable_list:perfetto.protos.PackagesList.packages)
  return &packages_;
}
const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::PackagesList_PackageInfo >&
PackagesList::packages() const {
  // @@protoc_insertion_point(field_list:perfetto.protos.PackagesList.packages)
  return packages_;
}

// optional bool parse_error = 2;
bool PackagesList::has_parse_error() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void PackagesList::set_has_parse_error() {
  _has_bits_[0] |= 0x00000002u;
}
void PackagesList::clear_has_parse_error() {
  _has_bits_[0] &= ~0x00000002u;
}
void PackagesList::clear_parse_error() {
  parse_error_ = false;
  clear_has_parse_error();
}
 bool PackagesList::parse_error() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.PackagesList.parse_error)
  return parse_error_;
}
 void PackagesList::set_parse_error(bool value) {
  set_has_parse_error();
  parse_error_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.PackagesList.parse_error)
}

// optional bool read_error = 3;
bool PackagesList::has_read_error() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
void PackagesList::set_has_read_error() {
  _has_bits_[0] |= 0x00000004u;
}
void PackagesList::clear_has_read_error() {
  _has_bits_[0] &= ~0x00000004u;
}
void PackagesList::clear_read_error() {
  read_error_ = false;
  clear_has_read_error();
}
 bool PackagesList::read_error() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.PackagesList.read_error)
  return read_error_;
}
 void PackagesList::set_read_error(bool value) {
  set_has_read_error();
  read_error_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.PackagesList.read_error)
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace protos
}  // namespace perfetto

// @@protoc_insertion_point(global_scope)
