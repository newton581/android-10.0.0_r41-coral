// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: perfetto/trace/trace.proto

#ifndef PROTOBUF_perfetto_2ftrace_2ftrace_2eproto__INCLUDED
#define PROTOBUF_perfetto_2ftrace_2ftrace_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include "perfetto/trace/trace_packet.pb.h"
// @@protoc_insertion_point(includes)

namespace perfetto {
namespace protos {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_perfetto_2ftrace_2ftrace_2eproto();
void protobuf_AssignDesc_perfetto_2ftrace_2ftrace_2eproto();
void protobuf_ShutdownFile_perfetto_2ftrace_2ftrace_2eproto();

class Trace;

// ===================================================================

class Trace : public ::google::protobuf::MessageLite {
 public:
  Trace();
  virtual ~Trace();

  Trace(const Trace& from);

  inline Trace& operator=(const Trace& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const Trace& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const Trace* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(Trace* other);

  // implements Message ----------------------------------------------

  inline Trace* New() const { return New(NULL); }

  Trace* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const Trace& from);
  void MergeFrom(const Trace& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(Trace* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // repeated .perfetto.protos.TracePacket packet = 1;
  int packet_size() const;
  void clear_packet();
  static const int kPacketFieldNumber = 1;
  const ::perfetto::protos::TracePacket& packet(int index) const;
  ::perfetto::protos::TracePacket* mutable_packet(int index);
  ::perfetto::protos::TracePacket* add_packet();
  ::google::protobuf::RepeatedPtrField< ::perfetto::protos::TracePacket >*
      mutable_packet();
  const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::TracePacket >&
      packet() const;

  // @@protoc_insertion_point(class_scope:perfetto.protos.Trace)
 private:

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::RepeatedPtrField< ::perfetto::protos::TracePacket > packet_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2ftrace_2ftrace_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2ftrace_2ftrace_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2ftrace_2ftrace_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2ftrace_2ftrace_2eproto();

  void InitAsDefaultInstance();
  static Trace* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// Trace

// repeated .perfetto.protos.TracePacket packet = 1;
inline int Trace::packet_size() const {
  return packet_.size();
}
inline void Trace::clear_packet() {
  packet_.Clear();
}
inline const ::perfetto::protos::TracePacket& Trace::packet(int index) const {
  // @@protoc_insertion_point(field_get:perfetto.protos.Trace.packet)
  return packet_.Get(index);
}
inline ::perfetto::protos::TracePacket* Trace::mutable_packet(int index) {
  // @@protoc_insertion_point(field_mutable:perfetto.protos.Trace.packet)
  return packet_.Mutable(index);
}
inline ::perfetto::protos::TracePacket* Trace::add_packet() {
  // @@protoc_insertion_point(field_add:perfetto.protos.Trace.packet)
  return packet_.Add();
}
inline ::google::protobuf::RepeatedPtrField< ::perfetto::protos::TracePacket >*
Trace::mutable_packet() {
  // @@protoc_insertion_point(field_mutable_list:perfetto.protos.Trace.packet)
  return &packet_;
}
inline const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::TracePacket >&
Trace::packet() const {
  // @@protoc_insertion_point(field_list:perfetto.protos.Trace.packet)
  return packet_;
}

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace protos
}  // namespace perfetto

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_perfetto_2ftrace_2ftrace_2eproto__INCLUDED
