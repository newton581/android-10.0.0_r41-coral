// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: perfetto/trace/trusted_packet.proto

#ifndef PROTOBUF_perfetto_2ftrace_2ftrusted_5fpacket_2eproto__INCLUDED
#define PROTOBUF_perfetto_2ftrace_2ftrusted_5fpacket_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include "perfetto/common/trace_stats.pb.h"
#include "perfetto/config/trace_config.pb.h"
#include "perfetto/trace/clock_snapshot.pb.h"
#include "perfetto/trace/system_info.pb.h"
#include "perfetto/trace/trigger.pb.h"
// @@protoc_insertion_point(includes)

namespace perfetto {
namespace protos {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_perfetto_2ftrace_2ftrusted_5fpacket_2eproto();
void protobuf_AssignDesc_perfetto_2ftrace_2ftrusted_5fpacket_2eproto();
void protobuf_ShutdownFile_perfetto_2ftrace_2ftrusted_5fpacket_2eproto();

class TrustedPacket;

// ===================================================================

class TrustedPacket : public ::google::protobuf::MessageLite {
 public:
  TrustedPacket();
  virtual ~TrustedPacket();

  TrustedPacket(const TrustedPacket& from);

  inline TrustedPacket& operator=(const TrustedPacket& from) {
    CopyFrom(from);
    return *this;
  }

  static const TrustedPacket& default_instance();

  enum OptionalTrustedUidCase {
    kTrustedUid = 3,
    OPTIONAL_TRUSTED_UID_NOT_SET = 0,
  };

  enum OptionalTrustedPacketSequenceIdCase {
    kTrustedPacketSequenceId = 10,
    OPTIONAL_TRUSTED_PACKET_SEQUENCE_ID_NOT_SET = 0,
  };

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const TrustedPacket* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(TrustedPacket* other);

  // implements Message ----------------------------------------------

  inline TrustedPacket* New() const { return New(NULL); }

  TrustedPacket* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const TrustedPacket& from);
  void MergeFrom(const TrustedPacket& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(TrustedPacket* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional int32 trusted_uid = 3;
  private:
  bool has_trusted_uid() const;
  public:
  void clear_trusted_uid();
  static const int kTrustedUidFieldNumber = 3;
  ::google::protobuf::int32 trusted_uid() const;
  void set_trusted_uid(::google::protobuf::int32 value);

  // optional uint32 trusted_packet_sequence_id = 10;
  private:
  bool has_trusted_packet_sequence_id() const;
  public:
  void clear_trusted_packet_sequence_id();
  static const int kTrustedPacketSequenceIdFieldNumber = 10;
  ::google::protobuf::uint32 trusted_packet_sequence_id() const;
  void set_trusted_packet_sequence_id(::google::protobuf::uint32 value);

  // optional .perfetto.protos.ClockSnapshot clock_snapshot = 6;
  bool has_clock_snapshot() const;
  void clear_clock_snapshot();
  static const int kClockSnapshotFieldNumber = 6;
  const ::perfetto::protos::ClockSnapshot& clock_snapshot() const;
  ::perfetto::protos::ClockSnapshot* mutable_clock_snapshot();
  ::perfetto::protos::ClockSnapshot* release_clock_snapshot();
  void set_allocated_clock_snapshot(::perfetto::protos::ClockSnapshot* clock_snapshot);

  // optional uint64 timestamp = 8;
  void clear_timestamp();
  static const int kTimestampFieldNumber = 8;
  ::google::protobuf::uint64 timestamp() const;
  void set_timestamp(::google::protobuf::uint64 value);

  // optional .perfetto.protos.TraceConfig trace_config = 33;
  bool has_trace_config() const;
  void clear_trace_config();
  static const int kTraceConfigFieldNumber = 33;
  const ::perfetto::protos::TraceConfig& trace_config() const;
  ::perfetto::protos::TraceConfig* mutable_trace_config();
  ::perfetto::protos::TraceConfig* release_trace_config();
  void set_allocated_trace_config(::perfetto::protos::TraceConfig* trace_config);

  // optional .perfetto.protos.TraceStats trace_stats = 35;
  bool has_trace_stats() const;
  void clear_trace_stats();
  static const int kTraceStatsFieldNumber = 35;
  const ::perfetto::protos::TraceStats& trace_stats() const;
  ::perfetto::protos::TraceStats* mutable_trace_stats();
  ::perfetto::protos::TraceStats* release_trace_stats();
  void set_allocated_trace_stats(::perfetto::protos::TraceStats* trace_stats);

  // optional bytes synchronization_marker = 36;
  void clear_synchronization_marker();
  static const int kSynchronizationMarkerFieldNumber = 36;
  const ::std::string& synchronization_marker() const;
  void set_synchronization_marker(const ::std::string& value);
  void set_synchronization_marker(const char* value);
  void set_synchronization_marker(const void* value, size_t size);
  ::std::string* mutable_synchronization_marker();
  ::std::string* release_synchronization_marker();
  void set_allocated_synchronization_marker(::std::string* synchronization_marker);

  // optional bool previous_packet_dropped = 42;
  void clear_previous_packet_dropped();
  static const int kPreviousPacketDroppedFieldNumber = 42;
  bool previous_packet_dropped() const;
  void set_previous_packet_dropped(bool value);

  // optional .perfetto.protos.SystemInfo system_info = 45;
  bool has_system_info() const;
  void clear_system_info();
  static const int kSystemInfoFieldNumber = 45;
  const ::perfetto::protos::SystemInfo& system_info() const;
  ::perfetto::protos::SystemInfo* mutable_system_info();
  ::perfetto::protos::SystemInfo* release_system_info();
  void set_allocated_system_info(::perfetto::protos::SystemInfo* system_info);

  // optional .perfetto.protos.Trigger trigger = 46;
  bool has_trigger() const;
  void clear_trigger();
  static const int kTriggerFieldNumber = 46;
  const ::perfetto::protos::Trigger& trigger() const;
  ::perfetto::protos::Trigger* mutable_trigger();
  ::perfetto::protos::Trigger* release_trigger();
  void set_allocated_trigger(::perfetto::protos::Trigger* trigger);

  OptionalTrustedUidCase optional_trusted_uid_case() const;
  OptionalTrustedPacketSequenceIdCase optional_trusted_packet_sequence_id_case() const;
  // @@protoc_insertion_point(class_scope:perfetto.protos.TrustedPacket)
 private:
  inline void set_has_trusted_uid();
  inline void set_has_trusted_packet_sequence_id();

  inline bool has_optional_trusted_uid() const;
  void clear_optional_trusted_uid();
  inline void clear_has_optional_trusted_uid();

  inline bool has_optional_trusted_packet_sequence_id() const;
  void clear_optional_trusted_packet_sequence_id();
  inline void clear_has_optional_trusted_packet_sequence_id();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  bool _is_default_instance_;
  ::perfetto::protos::ClockSnapshot* clock_snapshot_;
  ::google::protobuf::uint64 timestamp_;
  ::perfetto::protos::TraceConfig* trace_config_;
  ::perfetto::protos::TraceStats* trace_stats_;
  ::google::protobuf::internal::ArenaStringPtr synchronization_marker_;
  ::perfetto::protos::SystemInfo* system_info_;
  ::perfetto::protos::Trigger* trigger_;
  bool previous_packet_dropped_;
  union OptionalTrustedUidUnion {
    OptionalTrustedUidUnion() {}
    ::google::protobuf::int32 trusted_uid_;
  } optional_trusted_uid_;
  union OptionalTrustedPacketSequenceIdUnion {
    OptionalTrustedPacketSequenceIdUnion() {}
    ::google::protobuf::uint32 trusted_packet_sequence_id_;
  } optional_trusted_packet_sequence_id_;
  mutable int _cached_size_;
  ::google::protobuf::uint32 _oneof_case_[2];

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2ftrace_2ftrusted_5fpacket_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2ftrace_2ftrusted_5fpacket_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2ftrace_2ftrusted_5fpacket_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2ftrace_2ftrusted_5fpacket_2eproto();

  void InitAsDefaultInstance();
  static TrustedPacket* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// TrustedPacket

// optional int32 trusted_uid = 3;
inline bool TrustedPacket::has_trusted_uid() const {
  return optional_trusted_uid_case() == kTrustedUid;
}
inline void TrustedPacket::set_has_trusted_uid() {
  _oneof_case_[0] = kTrustedUid;
}
inline void TrustedPacket::clear_trusted_uid() {
  if (has_trusted_uid()) {
    optional_trusted_uid_.trusted_uid_ = 0;
    clear_has_optional_trusted_uid();
  }
}
inline ::google::protobuf::int32 TrustedPacket::trusted_uid() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.TrustedPacket.trusted_uid)
  if (has_trusted_uid()) {
    return optional_trusted_uid_.trusted_uid_;
  }
  return 0;
}
inline void TrustedPacket::set_trusted_uid(::google::protobuf::int32 value) {
  if (!has_trusted_uid()) {
    clear_optional_trusted_uid();
    set_has_trusted_uid();
  }
  optional_trusted_uid_.trusted_uid_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.TrustedPacket.trusted_uid)
}

// optional uint32 trusted_packet_sequence_id = 10;
inline bool TrustedPacket::has_trusted_packet_sequence_id() const {
  return optional_trusted_packet_sequence_id_case() == kTrustedPacketSequenceId;
}
inline void TrustedPacket::set_has_trusted_packet_sequence_id() {
  _oneof_case_[1] = kTrustedPacketSequenceId;
}
inline void TrustedPacket::clear_trusted_packet_sequence_id() {
  if (has_trusted_packet_sequence_id()) {
    optional_trusted_packet_sequence_id_.trusted_packet_sequence_id_ = 0u;
    clear_has_optional_trusted_packet_sequence_id();
  }
}
inline ::google::protobuf::uint32 TrustedPacket::trusted_packet_sequence_id() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.TrustedPacket.trusted_packet_sequence_id)
  if (has_trusted_packet_sequence_id()) {
    return optional_trusted_packet_sequence_id_.trusted_packet_sequence_id_;
  }
  return 0u;
}
inline void TrustedPacket::set_trusted_packet_sequence_id(::google::protobuf::uint32 value) {
  if (!has_trusted_packet_sequence_id()) {
    clear_optional_trusted_packet_sequence_id();
    set_has_trusted_packet_sequence_id();
  }
  optional_trusted_packet_sequence_id_.trusted_packet_sequence_id_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.TrustedPacket.trusted_packet_sequence_id)
}

// optional .perfetto.protos.ClockSnapshot clock_snapshot = 6;
inline bool TrustedPacket::has_clock_snapshot() const {
  return !_is_default_instance_ && clock_snapshot_ != NULL;
}
inline void TrustedPacket::clear_clock_snapshot() {
  if (GetArenaNoVirtual() == NULL && clock_snapshot_ != NULL) delete clock_snapshot_;
  clock_snapshot_ = NULL;
}
inline const ::perfetto::protos::ClockSnapshot& TrustedPacket::clock_snapshot() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.TrustedPacket.clock_snapshot)
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  return clock_snapshot_ != NULL ? *clock_snapshot_ : *default_instance().clock_snapshot_;
#else
  return clock_snapshot_ != NULL ? *clock_snapshot_ : *default_instance_->clock_snapshot_;
#endif
}
inline ::perfetto::protos::ClockSnapshot* TrustedPacket::mutable_clock_snapshot() {
  
  if (clock_snapshot_ == NULL) {
    clock_snapshot_ = new ::perfetto::protos::ClockSnapshot;
  }
  // @@protoc_insertion_point(field_mutable:perfetto.protos.TrustedPacket.clock_snapshot)
  return clock_snapshot_;
}
inline ::perfetto::protos::ClockSnapshot* TrustedPacket::release_clock_snapshot() {
  // @@protoc_insertion_point(field_release:perfetto.protos.TrustedPacket.clock_snapshot)
  
  ::perfetto::protos::ClockSnapshot* temp = clock_snapshot_;
  clock_snapshot_ = NULL;
  return temp;
}
inline void TrustedPacket::set_allocated_clock_snapshot(::perfetto::protos::ClockSnapshot* clock_snapshot) {
  delete clock_snapshot_;
  clock_snapshot_ = clock_snapshot;
  if (clock_snapshot) {
    
  } else {
    
  }
  // @@protoc_insertion_point(field_set_allocated:perfetto.protos.TrustedPacket.clock_snapshot)
}

// optional uint64 timestamp = 8;
inline void TrustedPacket::clear_timestamp() {
  timestamp_ = GOOGLE_ULONGLONG(0);
}
inline ::google::protobuf::uint64 TrustedPacket::timestamp() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.TrustedPacket.timestamp)
  return timestamp_;
}
inline void TrustedPacket::set_timestamp(::google::protobuf::uint64 value) {
  
  timestamp_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.TrustedPacket.timestamp)
}

// optional .perfetto.protos.TraceConfig trace_config = 33;
inline bool TrustedPacket::has_trace_config() const {
  return !_is_default_instance_ && trace_config_ != NULL;
}
inline void TrustedPacket::clear_trace_config() {
  if (GetArenaNoVirtual() == NULL && trace_config_ != NULL) delete trace_config_;
  trace_config_ = NULL;
}
inline const ::perfetto::protos::TraceConfig& TrustedPacket::trace_config() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.TrustedPacket.trace_config)
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  return trace_config_ != NULL ? *trace_config_ : *default_instance().trace_config_;
#else
  return trace_config_ != NULL ? *trace_config_ : *default_instance_->trace_config_;
#endif
}
inline ::perfetto::protos::TraceConfig* TrustedPacket::mutable_trace_config() {
  
  if (trace_config_ == NULL) {
    trace_config_ = new ::perfetto::protos::TraceConfig;
  }
  // @@protoc_insertion_point(field_mutable:perfetto.protos.TrustedPacket.trace_config)
  return trace_config_;
}
inline ::perfetto::protos::TraceConfig* TrustedPacket::release_trace_config() {
  // @@protoc_insertion_point(field_release:perfetto.protos.TrustedPacket.trace_config)
  
  ::perfetto::protos::TraceConfig* temp = trace_config_;
  trace_config_ = NULL;
  return temp;
}
inline void TrustedPacket::set_allocated_trace_config(::perfetto::protos::TraceConfig* trace_config) {
  delete trace_config_;
  trace_config_ = trace_config;
  if (trace_config) {
    
  } else {
    
  }
  // @@protoc_insertion_point(field_set_allocated:perfetto.protos.TrustedPacket.trace_config)
}

// optional .perfetto.protos.TraceStats trace_stats = 35;
inline bool TrustedPacket::has_trace_stats() const {
  return !_is_default_instance_ && trace_stats_ != NULL;
}
inline void TrustedPacket::clear_trace_stats() {
  if (GetArenaNoVirtual() == NULL && trace_stats_ != NULL) delete trace_stats_;
  trace_stats_ = NULL;
}
inline const ::perfetto::protos::TraceStats& TrustedPacket::trace_stats() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.TrustedPacket.trace_stats)
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  return trace_stats_ != NULL ? *trace_stats_ : *default_instance().trace_stats_;
#else
  return trace_stats_ != NULL ? *trace_stats_ : *default_instance_->trace_stats_;
#endif
}
inline ::perfetto::protos::TraceStats* TrustedPacket::mutable_trace_stats() {
  
  if (trace_stats_ == NULL) {
    trace_stats_ = new ::perfetto::protos::TraceStats;
  }
  // @@protoc_insertion_point(field_mutable:perfetto.protos.TrustedPacket.trace_stats)
  return trace_stats_;
}
inline ::perfetto::protos::TraceStats* TrustedPacket::release_trace_stats() {
  // @@protoc_insertion_point(field_release:perfetto.protos.TrustedPacket.trace_stats)
  
  ::perfetto::protos::TraceStats* temp = trace_stats_;
  trace_stats_ = NULL;
  return temp;
}
inline void TrustedPacket::set_allocated_trace_stats(::perfetto::protos::TraceStats* trace_stats) {
  delete trace_stats_;
  trace_stats_ = trace_stats;
  if (trace_stats) {
    
  } else {
    
  }
  // @@protoc_insertion_point(field_set_allocated:perfetto.protos.TrustedPacket.trace_stats)
}

// optional bytes synchronization_marker = 36;
inline void TrustedPacket::clear_synchronization_marker() {
  synchronization_marker_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& TrustedPacket::synchronization_marker() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.TrustedPacket.synchronization_marker)
  return synchronization_marker_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void TrustedPacket::set_synchronization_marker(const ::std::string& value) {
  
  synchronization_marker_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:perfetto.protos.TrustedPacket.synchronization_marker)
}
inline void TrustedPacket::set_synchronization_marker(const char* value) {
  
  synchronization_marker_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:perfetto.protos.TrustedPacket.synchronization_marker)
}
inline void TrustedPacket::set_synchronization_marker(const void* value, size_t size) {
  
  synchronization_marker_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:perfetto.protos.TrustedPacket.synchronization_marker)
}
inline ::std::string* TrustedPacket::mutable_synchronization_marker() {
  
  // @@protoc_insertion_point(field_mutable:perfetto.protos.TrustedPacket.synchronization_marker)
  return synchronization_marker_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* TrustedPacket::release_synchronization_marker() {
  // @@protoc_insertion_point(field_release:perfetto.protos.TrustedPacket.synchronization_marker)
  
  return synchronization_marker_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void TrustedPacket::set_allocated_synchronization_marker(::std::string* synchronization_marker) {
  if (synchronization_marker != NULL) {
    
  } else {
    
  }
  synchronization_marker_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), synchronization_marker);
  // @@protoc_insertion_point(field_set_allocated:perfetto.protos.TrustedPacket.synchronization_marker)
}

// optional bool previous_packet_dropped = 42;
inline void TrustedPacket::clear_previous_packet_dropped() {
  previous_packet_dropped_ = false;
}
inline bool TrustedPacket::previous_packet_dropped() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.TrustedPacket.previous_packet_dropped)
  return previous_packet_dropped_;
}
inline void TrustedPacket::set_previous_packet_dropped(bool value) {
  
  previous_packet_dropped_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.TrustedPacket.previous_packet_dropped)
}

// optional .perfetto.protos.SystemInfo system_info = 45;
inline bool TrustedPacket::has_system_info() const {
  return !_is_default_instance_ && system_info_ != NULL;
}
inline void TrustedPacket::clear_system_info() {
  if (GetArenaNoVirtual() == NULL && system_info_ != NULL) delete system_info_;
  system_info_ = NULL;
}
inline const ::perfetto::protos::SystemInfo& TrustedPacket::system_info() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.TrustedPacket.system_info)
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  return system_info_ != NULL ? *system_info_ : *default_instance().system_info_;
#else
  return system_info_ != NULL ? *system_info_ : *default_instance_->system_info_;
#endif
}
inline ::perfetto::protos::SystemInfo* TrustedPacket::mutable_system_info() {
  
  if (system_info_ == NULL) {
    system_info_ = new ::perfetto::protos::SystemInfo;
  }
  // @@protoc_insertion_point(field_mutable:perfetto.protos.TrustedPacket.system_info)
  return system_info_;
}
inline ::perfetto::protos::SystemInfo* TrustedPacket::release_system_info() {
  // @@protoc_insertion_point(field_release:perfetto.protos.TrustedPacket.system_info)
  
  ::perfetto::protos::SystemInfo* temp = system_info_;
  system_info_ = NULL;
  return temp;
}
inline void TrustedPacket::set_allocated_system_info(::perfetto::protos::SystemInfo* system_info) {
  delete system_info_;
  system_info_ = system_info;
  if (system_info) {
    
  } else {
    
  }
  // @@protoc_insertion_point(field_set_allocated:perfetto.protos.TrustedPacket.system_info)
}

// optional .perfetto.protos.Trigger trigger = 46;
inline bool TrustedPacket::has_trigger() const {
  return !_is_default_instance_ && trigger_ != NULL;
}
inline void TrustedPacket::clear_trigger() {
  if (GetArenaNoVirtual() == NULL && trigger_ != NULL) delete trigger_;
  trigger_ = NULL;
}
inline const ::perfetto::protos::Trigger& TrustedPacket::trigger() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.TrustedPacket.trigger)
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  return trigger_ != NULL ? *trigger_ : *default_instance().trigger_;
#else
  return trigger_ != NULL ? *trigger_ : *default_instance_->trigger_;
#endif
}
inline ::perfetto::protos::Trigger* TrustedPacket::mutable_trigger() {
  
  if (trigger_ == NULL) {
    trigger_ = new ::perfetto::protos::Trigger;
  }
  // @@protoc_insertion_point(field_mutable:perfetto.protos.TrustedPacket.trigger)
  return trigger_;
}
inline ::perfetto::protos::Trigger* TrustedPacket::release_trigger() {
  // @@protoc_insertion_point(field_release:perfetto.protos.TrustedPacket.trigger)
  
  ::perfetto::protos::Trigger* temp = trigger_;
  trigger_ = NULL;
  return temp;
}
inline void TrustedPacket::set_allocated_trigger(::perfetto::protos::Trigger* trigger) {
  delete trigger_;
  trigger_ = trigger;
  if (trigger) {
    
  } else {
    
  }
  // @@protoc_insertion_point(field_set_allocated:perfetto.protos.TrustedPacket.trigger)
}

inline bool TrustedPacket::has_optional_trusted_uid() const {
  return optional_trusted_uid_case() != OPTIONAL_TRUSTED_UID_NOT_SET;
}
inline void TrustedPacket::clear_has_optional_trusted_uid() {
  _oneof_case_[0] = OPTIONAL_TRUSTED_UID_NOT_SET;
}
inline bool TrustedPacket::has_optional_trusted_packet_sequence_id() const {
  return optional_trusted_packet_sequence_id_case() != OPTIONAL_TRUSTED_PACKET_SEQUENCE_ID_NOT_SET;
}
inline void TrustedPacket::clear_has_optional_trusted_packet_sequence_id() {
  _oneof_case_[1] = OPTIONAL_TRUSTED_PACKET_SEQUENCE_ID_NOT_SET;
}
inline TrustedPacket::OptionalTrustedUidCase TrustedPacket::optional_trusted_uid_case() const {
  return TrustedPacket::OptionalTrustedUidCase(_oneof_case_[0]);
}
inline TrustedPacket::OptionalTrustedPacketSequenceIdCase TrustedPacket::optional_trusted_packet_sequence_id_case() const {
  return TrustedPacket::OptionalTrustedPacketSequenceIdCase(_oneof_case_[1]);
}
#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace protos
}  // namespace perfetto

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_perfetto_2ftrace_2ftrusted_5fpacket_2eproto__INCLUDED
