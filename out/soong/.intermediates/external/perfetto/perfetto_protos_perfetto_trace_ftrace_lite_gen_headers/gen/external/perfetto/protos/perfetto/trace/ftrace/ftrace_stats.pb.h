// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: perfetto/trace/ftrace/ftrace_stats.proto

#ifndef PROTOBUF_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto__INCLUDED
#define PROTOBUF_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_util.h>
// @@protoc_insertion_point(includes)

namespace perfetto {
namespace protos {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto();
void protobuf_AssignDesc_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto();
void protobuf_ShutdownFile_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto();

class FtraceCpuStats;
class FtraceStats;

enum FtraceStats_Phase {
  FtraceStats_Phase_UNSPECIFIED = 0,
  FtraceStats_Phase_START_OF_TRACE = 1,
  FtraceStats_Phase_END_OF_TRACE = 2
};
bool FtraceStats_Phase_IsValid(int value);
const FtraceStats_Phase FtraceStats_Phase_Phase_MIN = FtraceStats_Phase_UNSPECIFIED;
const FtraceStats_Phase FtraceStats_Phase_Phase_MAX = FtraceStats_Phase_END_OF_TRACE;
const int FtraceStats_Phase_Phase_ARRAYSIZE = FtraceStats_Phase_Phase_MAX + 1;

// ===================================================================

class FtraceCpuStats : public ::google::protobuf::MessageLite {
 public:
  FtraceCpuStats();
  virtual ~FtraceCpuStats();

  FtraceCpuStats(const FtraceCpuStats& from);

  inline FtraceCpuStats& operator=(const FtraceCpuStats& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const FtraceCpuStats& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const FtraceCpuStats* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(FtraceCpuStats* other);

  // implements Message ----------------------------------------------

  inline FtraceCpuStats* New() const { return New(NULL); }

  FtraceCpuStats* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const FtraceCpuStats& from);
  void MergeFrom(const FtraceCpuStats& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(FtraceCpuStats* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional uint64 cpu = 1;
  bool has_cpu() const;
  void clear_cpu();
  static const int kCpuFieldNumber = 1;
  ::google::protobuf::uint64 cpu() const;
  void set_cpu(::google::protobuf::uint64 value);

  // optional uint64 entries = 2;
  bool has_entries() const;
  void clear_entries();
  static const int kEntriesFieldNumber = 2;
  ::google::protobuf::uint64 entries() const;
  void set_entries(::google::protobuf::uint64 value);

  // optional uint64 overrun = 3;
  bool has_overrun() const;
  void clear_overrun();
  static const int kOverrunFieldNumber = 3;
  ::google::protobuf::uint64 overrun() const;
  void set_overrun(::google::protobuf::uint64 value);

  // optional uint64 commit_overrun = 4;
  bool has_commit_overrun() const;
  void clear_commit_overrun();
  static const int kCommitOverrunFieldNumber = 4;
  ::google::protobuf::uint64 commit_overrun() const;
  void set_commit_overrun(::google::protobuf::uint64 value);

  // optional uint64 bytes_read = 5;
  bool has_bytes_read() const;
  void clear_bytes_read();
  static const int kBytesReadFieldNumber = 5;
  ::google::protobuf::uint64 bytes_read() const;
  void set_bytes_read(::google::protobuf::uint64 value);

  // optional double oldest_event_ts = 6;
  bool has_oldest_event_ts() const;
  void clear_oldest_event_ts();
  static const int kOldestEventTsFieldNumber = 6;
  double oldest_event_ts() const;
  void set_oldest_event_ts(double value);

  // optional double now_ts = 7;
  bool has_now_ts() const;
  void clear_now_ts();
  static const int kNowTsFieldNumber = 7;
  double now_ts() const;
  void set_now_ts(double value);

  // optional uint64 dropped_events = 8;
  bool has_dropped_events() const;
  void clear_dropped_events();
  static const int kDroppedEventsFieldNumber = 8;
  ::google::protobuf::uint64 dropped_events() const;
  void set_dropped_events(::google::protobuf::uint64 value);

  // optional uint64 read_events = 9;
  bool has_read_events() const;
  void clear_read_events();
  static const int kReadEventsFieldNumber = 9;
  ::google::protobuf::uint64 read_events() const;
  void set_read_events(::google::protobuf::uint64 value);

  // @@protoc_insertion_point(class_scope:perfetto.protos.FtraceCpuStats)
 private:
  inline void set_has_cpu();
  inline void clear_has_cpu();
  inline void set_has_entries();
  inline void clear_has_entries();
  inline void set_has_overrun();
  inline void clear_has_overrun();
  inline void set_has_commit_overrun();
  inline void clear_has_commit_overrun();
  inline void set_has_bytes_read();
  inline void clear_has_bytes_read();
  inline void set_has_oldest_event_ts();
  inline void clear_has_oldest_event_ts();
  inline void set_has_now_ts();
  inline void clear_has_now_ts();
  inline void set_has_dropped_events();
  inline void clear_has_dropped_events();
  inline void set_has_read_events();
  inline void clear_has_read_events();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::uint64 cpu_;
  ::google::protobuf::uint64 entries_;
  ::google::protobuf::uint64 overrun_;
  ::google::protobuf::uint64 commit_overrun_;
  ::google::protobuf::uint64 bytes_read_;
  double oldest_event_ts_;
  double now_ts_;
  ::google::protobuf::uint64 dropped_events_;
  ::google::protobuf::uint64 read_events_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto();

  void InitAsDefaultInstance();
  static FtraceCpuStats* default_instance_;
};
// -------------------------------------------------------------------

class FtraceStats : public ::google::protobuf::MessageLite {
 public:
  FtraceStats();
  virtual ~FtraceStats();

  FtraceStats(const FtraceStats& from);

  inline FtraceStats& operator=(const FtraceStats& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const FtraceStats& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const FtraceStats* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(FtraceStats* other);

  // implements Message ----------------------------------------------

  inline FtraceStats* New() const { return New(NULL); }

  FtraceStats* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const FtraceStats& from);
  void MergeFrom(const FtraceStats& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(FtraceStats* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  typedef FtraceStats_Phase Phase;
  static const Phase UNSPECIFIED =
    FtraceStats_Phase_UNSPECIFIED;
  static const Phase START_OF_TRACE =
    FtraceStats_Phase_START_OF_TRACE;
  static const Phase END_OF_TRACE =
    FtraceStats_Phase_END_OF_TRACE;
  static inline bool Phase_IsValid(int value) {
    return FtraceStats_Phase_IsValid(value);
  }
  static const Phase Phase_MIN =
    FtraceStats_Phase_Phase_MIN;
  static const Phase Phase_MAX =
    FtraceStats_Phase_Phase_MAX;
  static const int Phase_ARRAYSIZE =
    FtraceStats_Phase_Phase_ARRAYSIZE;

  // accessors -------------------------------------------------------

  // optional .perfetto.protos.FtraceStats.Phase phase = 1;
  bool has_phase() const;
  void clear_phase();
  static const int kPhaseFieldNumber = 1;
  ::perfetto::protos::FtraceStats_Phase phase() const;
  void set_phase(::perfetto::protos::FtraceStats_Phase value);

  // repeated .perfetto.protos.FtraceCpuStats cpu_stats = 2;
  int cpu_stats_size() const;
  void clear_cpu_stats();
  static const int kCpuStatsFieldNumber = 2;
  const ::perfetto::protos::FtraceCpuStats& cpu_stats(int index) const;
  ::perfetto::protos::FtraceCpuStats* mutable_cpu_stats(int index);
  ::perfetto::protos::FtraceCpuStats* add_cpu_stats();
  ::google::protobuf::RepeatedPtrField< ::perfetto::protos::FtraceCpuStats >*
      mutable_cpu_stats();
  const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::FtraceCpuStats >&
      cpu_stats() const;

  // @@protoc_insertion_point(class_scope:perfetto.protos.FtraceStats)
 private:
  inline void set_has_phase();
  inline void clear_has_phase();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::RepeatedPtrField< ::perfetto::protos::FtraceCpuStats > cpu_stats_;
  int phase_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto();

  void InitAsDefaultInstance();
  static FtraceStats* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// FtraceCpuStats

// optional uint64 cpu = 1;
inline bool FtraceCpuStats::has_cpu() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void FtraceCpuStats::set_has_cpu() {
  _has_bits_[0] |= 0x00000001u;
}
inline void FtraceCpuStats::clear_has_cpu() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void FtraceCpuStats::clear_cpu() {
  cpu_ = GOOGLE_ULONGLONG(0);
  clear_has_cpu();
}
inline ::google::protobuf::uint64 FtraceCpuStats::cpu() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceCpuStats.cpu)
  return cpu_;
}
inline void FtraceCpuStats::set_cpu(::google::protobuf::uint64 value) {
  set_has_cpu();
  cpu_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.FtraceCpuStats.cpu)
}

// optional uint64 entries = 2;
inline bool FtraceCpuStats::has_entries() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void FtraceCpuStats::set_has_entries() {
  _has_bits_[0] |= 0x00000002u;
}
inline void FtraceCpuStats::clear_has_entries() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void FtraceCpuStats::clear_entries() {
  entries_ = GOOGLE_ULONGLONG(0);
  clear_has_entries();
}
inline ::google::protobuf::uint64 FtraceCpuStats::entries() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceCpuStats.entries)
  return entries_;
}
inline void FtraceCpuStats::set_entries(::google::protobuf::uint64 value) {
  set_has_entries();
  entries_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.FtraceCpuStats.entries)
}

// optional uint64 overrun = 3;
inline bool FtraceCpuStats::has_overrun() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void FtraceCpuStats::set_has_overrun() {
  _has_bits_[0] |= 0x00000004u;
}
inline void FtraceCpuStats::clear_has_overrun() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void FtraceCpuStats::clear_overrun() {
  overrun_ = GOOGLE_ULONGLONG(0);
  clear_has_overrun();
}
inline ::google::protobuf::uint64 FtraceCpuStats::overrun() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceCpuStats.overrun)
  return overrun_;
}
inline void FtraceCpuStats::set_overrun(::google::protobuf::uint64 value) {
  set_has_overrun();
  overrun_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.FtraceCpuStats.overrun)
}

// optional uint64 commit_overrun = 4;
inline bool FtraceCpuStats::has_commit_overrun() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void FtraceCpuStats::set_has_commit_overrun() {
  _has_bits_[0] |= 0x00000008u;
}
inline void FtraceCpuStats::clear_has_commit_overrun() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void FtraceCpuStats::clear_commit_overrun() {
  commit_overrun_ = GOOGLE_ULONGLONG(0);
  clear_has_commit_overrun();
}
inline ::google::protobuf::uint64 FtraceCpuStats::commit_overrun() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceCpuStats.commit_overrun)
  return commit_overrun_;
}
inline void FtraceCpuStats::set_commit_overrun(::google::protobuf::uint64 value) {
  set_has_commit_overrun();
  commit_overrun_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.FtraceCpuStats.commit_overrun)
}

// optional uint64 bytes_read = 5;
inline bool FtraceCpuStats::has_bytes_read() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
inline void FtraceCpuStats::set_has_bytes_read() {
  _has_bits_[0] |= 0x00000010u;
}
inline void FtraceCpuStats::clear_has_bytes_read() {
  _has_bits_[0] &= ~0x00000010u;
}
inline void FtraceCpuStats::clear_bytes_read() {
  bytes_read_ = GOOGLE_ULONGLONG(0);
  clear_has_bytes_read();
}
inline ::google::protobuf::uint64 FtraceCpuStats::bytes_read() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceCpuStats.bytes_read)
  return bytes_read_;
}
inline void FtraceCpuStats::set_bytes_read(::google::protobuf::uint64 value) {
  set_has_bytes_read();
  bytes_read_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.FtraceCpuStats.bytes_read)
}

// optional double oldest_event_ts = 6;
inline bool FtraceCpuStats::has_oldest_event_ts() const {
  return (_has_bits_[0] & 0x00000020u) != 0;
}
inline void FtraceCpuStats::set_has_oldest_event_ts() {
  _has_bits_[0] |= 0x00000020u;
}
inline void FtraceCpuStats::clear_has_oldest_event_ts() {
  _has_bits_[0] &= ~0x00000020u;
}
inline void FtraceCpuStats::clear_oldest_event_ts() {
  oldest_event_ts_ = 0;
  clear_has_oldest_event_ts();
}
inline double FtraceCpuStats::oldest_event_ts() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceCpuStats.oldest_event_ts)
  return oldest_event_ts_;
}
inline void FtraceCpuStats::set_oldest_event_ts(double value) {
  set_has_oldest_event_ts();
  oldest_event_ts_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.FtraceCpuStats.oldest_event_ts)
}

// optional double now_ts = 7;
inline bool FtraceCpuStats::has_now_ts() const {
  return (_has_bits_[0] & 0x00000040u) != 0;
}
inline void FtraceCpuStats::set_has_now_ts() {
  _has_bits_[0] |= 0x00000040u;
}
inline void FtraceCpuStats::clear_has_now_ts() {
  _has_bits_[0] &= ~0x00000040u;
}
inline void FtraceCpuStats::clear_now_ts() {
  now_ts_ = 0;
  clear_has_now_ts();
}
inline double FtraceCpuStats::now_ts() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceCpuStats.now_ts)
  return now_ts_;
}
inline void FtraceCpuStats::set_now_ts(double value) {
  set_has_now_ts();
  now_ts_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.FtraceCpuStats.now_ts)
}

// optional uint64 dropped_events = 8;
inline bool FtraceCpuStats::has_dropped_events() const {
  return (_has_bits_[0] & 0x00000080u) != 0;
}
inline void FtraceCpuStats::set_has_dropped_events() {
  _has_bits_[0] |= 0x00000080u;
}
inline void FtraceCpuStats::clear_has_dropped_events() {
  _has_bits_[0] &= ~0x00000080u;
}
inline void FtraceCpuStats::clear_dropped_events() {
  dropped_events_ = GOOGLE_ULONGLONG(0);
  clear_has_dropped_events();
}
inline ::google::protobuf::uint64 FtraceCpuStats::dropped_events() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceCpuStats.dropped_events)
  return dropped_events_;
}
inline void FtraceCpuStats::set_dropped_events(::google::protobuf::uint64 value) {
  set_has_dropped_events();
  dropped_events_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.FtraceCpuStats.dropped_events)
}

// optional uint64 read_events = 9;
inline bool FtraceCpuStats::has_read_events() const {
  return (_has_bits_[0] & 0x00000100u) != 0;
}
inline void FtraceCpuStats::set_has_read_events() {
  _has_bits_[0] |= 0x00000100u;
}
inline void FtraceCpuStats::clear_has_read_events() {
  _has_bits_[0] &= ~0x00000100u;
}
inline void FtraceCpuStats::clear_read_events() {
  read_events_ = GOOGLE_ULONGLONG(0);
  clear_has_read_events();
}
inline ::google::protobuf::uint64 FtraceCpuStats::read_events() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceCpuStats.read_events)
  return read_events_;
}
inline void FtraceCpuStats::set_read_events(::google::protobuf::uint64 value) {
  set_has_read_events();
  read_events_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.FtraceCpuStats.read_events)
}

// -------------------------------------------------------------------

// FtraceStats

// optional .perfetto.protos.FtraceStats.Phase phase = 1;
inline bool FtraceStats::has_phase() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void FtraceStats::set_has_phase() {
  _has_bits_[0] |= 0x00000001u;
}
inline void FtraceStats::clear_has_phase() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void FtraceStats::clear_phase() {
  phase_ = 0;
  clear_has_phase();
}
inline ::perfetto::protos::FtraceStats_Phase FtraceStats::phase() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceStats.phase)
  return static_cast< ::perfetto::protos::FtraceStats_Phase >(phase_);
}
inline void FtraceStats::set_phase(::perfetto::protos::FtraceStats_Phase value) {
  assert(::perfetto::protos::FtraceStats_Phase_IsValid(value));
  set_has_phase();
  phase_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.FtraceStats.phase)
}

// repeated .perfetto.protos.FtraceCpuStats cpu_stats = 2;
inline int FtraceStats::cpu_stats_size() const {
  return cpu_stats_.size();
}
inline void FtraceStats::clear_cpu_stats() {
  cpu_stats_.Clear();
}
inline const ::perfetto::protos::FtraceCpuStats& FtraceStats::cpu_stats(int index) const {
  // @@protoc_insertion_point(field_get:perfetto.protos.FtraceStats.cpu_stats)
  return cpu_stats_.Get(index);
}
inline ::perfetto::protos::FtraceCpuStats* FtraceStats::mutable_cpu_stats(int index) {
  // @@protoc_insertion_point(field_mutable:perfetto.protos.FtraceStats.cpu_stats)
  return cpu_stats_.Mutable(index);
}
inline ::perfetto::protos::FtraceCpuStats* FtraceStats::add_cpu_stats() {
  // @@protoc_insertion_point(field_add:perfetto.protos.FtraceStats.cpu_stats)
  return cpu_stats_.Add();
}
inline ::google::protobuf::RepeatedPtrField< ::perfetto::protos::FtraceCpuStats >*
FtraceStats::mutable_cpu_stats() {
  // @@protoc_insertion_point(field_mutable_list:perfetto.protos.FtraceStats.cpu_stats)
  return &cpu_stats_;
}
inline const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::FtraceCpuStats >&
FtraceStats::cpu_stats() const {
  // @@protoc_insertion_point(field_list:perfetto.protos.FtraceStats.cpu_stats)
  return cpu_stats_;
}

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS
// -------------------------------------------------------------------


// @@protoc_insertion_point(namespace_scope)

}  // namespace protos
}  // namespace perfetto

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::perfetto::protos::FtraceStats_Phase> : ::google::protobuf::internal::true_type {};

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_perfetto_2ftrace_2fftrace_2fftrace_5fstats_2eproto__INCLUDED
