// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: perfetto/trace/ftrace/signal.proto

#ifndef PROTOBUF_perfetto_2ftrace_2fftrace_2fsignal_2eproto__INCLUDED
#define PROTOBUF_perfetto_2ftrace_2fftrace_2fsignal_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
// @@protoc_insertion_point(includes)

namespace perfetto {
namespace protos {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_perfetto_2ftrace_2fftrace_2fsignal_2eproto();
void protobuf_AssignDesc_perfetto_2ftrace_2fftrace_2fsignal_2eproto();
void protobuf_ShutdownFile_perfetto_2ftrace_2fftrace_2fsignal_2eproto();

class SignalDeliverFtraceEvent;
class SignalGenerateFtraceEvent;

// ===================================================================

class SignalDeliverFtraceEvent : public ::google::protobuf::MessageLite {
 public:
  SignalDeliverFtraceEvent();
  virtual ~SignalDeliverFtraceEvent();

  SignalDeliverFtraceEvent(const SignalDeliverFtraceEvent& from);

  inline SignalDeliverFtraceEvent& operator=(const SignalDeliverFtraceEvent& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const SignalDeliverFtraceEvent& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const SignalDeliverFtraceEvent* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(SignalDeliverFtraceEvent* other);

  // implements Message ----------------------------------------------

  inline SignalDeliverFtraceEvent* New() const { return New(NULL); }

  SignalDeliverFtraceEvent* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const SignalDeliverFtraceEvent& from);
  void MergeFrom(const SignalDeliverFtraceEvent& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(SignalDeliverFtraceEvent* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional int32 code = 1;
  bool has_code() const;
  void clear_code();
  static const int kCodeFieldNumber = 1;
  ::google::protobuf::int32 code() const;
  void set_code(::google::protobuf::int32 value);

  // optional uint64 sa_flags = 2;
  bool has_sa_flags() const;
  void clear_sa_flags();
  static const int kSaFlagsFieldNumber = 2;
  ::google::protobuf::uint64 sa_flags() const;
  void set_sa_flags(::google::protobuf::uint64 value);

  // optional int32 sig = 3;
  bool has_sig() const;
  void clear_sig();
  static const int kSigFieldNumber = 3;
  ::google::protobuf::int32 sig() const;
  void set_sig(::google::protobuf::int32 value);

  // @@protoc_insertion_point(class_scope:perfetto.protos.SignalDeliverFtraceEvent)
 private:
  inline void set_has_code();
  inline void clear_has_code();
  inline void set_has_sa_flags();
  inline void clear_has_sa_flags();
  inline void set_has_sig();
  inline void clear_has_sig();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::uint64 sa_flags_;
  ::google::protobuf::int32 code_;
  ::google::protobuf::int32 sig_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2ftrace_2fftrace_2fsignal_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2ftrace_2fftrace_2fsignal_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2ftrace_2fftrace_2fsignal_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2ftrace_2fftrace_2fsignal_2eproto();

  void InitAsDefaultInstance();
  static SignalDeliverFtraceEvent* default_instance_;
};
// -------------------------------------------------------------------

class SignalGenerateFtraceEvent : public ::google::protobuf::MessageLite {
 public:
  SignalGenerateFtraceEvent();
  virtual ~SignalGenerateFtraceEvent();

  SignalGenerateFtraceEvent(const SignalGenerateFtraceEvent& from);

  inline SignalGenerateFtraceEvent& operator=(const SignalGenerateFtraceEvent& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const SignalGenerateFtraceEvent& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const SignalGenerateFtraceEvent* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(SignalGenerateFtraceEvent* other);

  // implements Message ----------------------------------------------

  inline SignalGenerateFtraceEvent* New() const { return New(NULL); }

  SignalGenerateFtraceEvent* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const SignalGenerateFtraceEvent& from);
  void MergeFrom(const SignalGenerateFtraceEvent& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(SignalGenerateFtraceEvent* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional int32 code = 1;
  bool has_code() const;
  void clear_code();
  static const int kCodeFieldNumber = 1;
  ::google::protobuf::int32 code() const;
  void set_code(::google::protobuf::int32 value);

  // optional string comm = 2;
  bool has_comm() const;
  void clear_comm();
  static const int kCommFieldNumber = 2;
  const ::std::string& comm() const;
  void set_comm(const ::std::string& value);
  void set_comm(const char* value);
  void set_comm(const char* value, size_t size);
  ::std::string* mutable_comm();
  ::std::string* release_comm();
  void set_allocated_comm(::std::string* comm);

  // optional int32 group = 3;
  bool has_group() const;
  void clear_group();
  static const int kGroupFieldNumber = 3;
  ::google::protobuf::int32 group() const;
  void set_group(::google::protobuf::int32 value);

  // optional int32 pid = 4;
  bool has_pid() const;
  void clear_pid();
  static const int kPidFieldNumber = 4;
  ::google::protobuf::int32 pid() const;
  void set_pid(::google::protobuf::int32 value);

  // optional int32 result = 5;
  bool has_result() const;
  void clear_result();
  static const int kResultFieldNumber = 5;
  ::google::protobuf::int32 result() const;
  void set_result(::google::protobuf::int32 value);

  // optional int32 sig = 6;
  bool has_sig() const;
  void clear_sig();
  static const int kSigFieldNumber = 6;
  ::google::protobuf::int32 sig() const;
  void set_sig(::google::protobuf::int32 value);

  // @@protoc_insertion_point(class_scope:perfetto.protos.SignalGenerateFtraceEvent)
 private:
  inline void set_has_code();
  inline void clear_has_code();
  inline void set_has_comm();
  inline void clear_has_comm();
  inline void set_has_group();
  inline void clear_has_group();
  inline void set_has_pid();
  inline void clear_has_pid();
  inline void set_has_result();
  inline void clear_has_result();
  inline void set_has_sig();
  inline void clear_has_sig();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::internal::ArenaStringPtr comm_;
  ::google::protobuf::int32 code_;
  ::google::protobuf::int32 group_;
  ::google::protobuf::int32 pid_;
  ::google::protobuf::int32 result_;
  ::google::protobuf::int32 sig_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2ftrace_2fftrace_2fsignal_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2ftrace_2fftrace_2fsignal_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2ftrace_2fftrace_2fsignal_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2ftrace_2fftrace_2fsignal_2eproto();

  void InitAsDefaultInstance();
  static SignalGenerateFtraceEvent* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// SignalDeliverFtraceEvent

// optional int32 code = 1;
inline bool SignalDeliverFtraceEvent::has_code() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void SignalDeliverFtraceEvent::set_has_code() {
  _has_bits_[0] |= 0x00000001u;
}
inline void SignalDeliverFtraceEvent::clear_has_code() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void SignalDeliverFtraceEvent::clear_code() {
  code_ = 0;
  clear_has_code();
}
inline ::google::protobuf::int32 SignalDeliverFtraceEvent::code() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.SignalDeliverFtraceEvent.code)
  return code_;
}
inline void SignalDeliverFtraceEvent::set_code(::google::protobuf::int32 value) {
  set_has_code();
  code_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.SignalDeliverFtraceEvent.code)
}

// optional uint64 sa_flags = 2;
inline bool SignalDeliverFtraceEvent::has_sa_flags() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void SignalDeliverFtraceEvent::set_has_sa_flags() {
  _has_bits_[0] |= 0x00000002u;
}
inline void SignalDeliverFtraceEvent::clear_has_sa_flags() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void SignalDeliverFtraceEvent::clear_sa_flags() {
  sa_flags_ = GOOGLE_ULONGLONG(0);
  clear_has_sa_flags();
}
inline ::google::protobuf::uint64 SignalDeliverFtraceEvent::sa_flags() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.SignalDeliverFtraceEvent.sa_flags)
  return sa_flags_;
}
inline void SignalDeliverFtraceEvent::set_sa_flags(::google::protobuf::uint64 value) {
  set_has_sa_flags();
  sa_flags_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.SignalDeliverFtraceEvent.sa_flags)
}

// optional int32 sig = 3;
inline bool SignalDeliverFtraceEvent::has_sig() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void SignalDeliverFtraceEvent::set_has_sig() {
  _has_bits_[0] |= 0x00000004u;
}
inline void SignalDeliverFtraceEvent::clear_has_sig() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void SignalDeliverFtraceEvent::clear_sig() {
  sig_ = 0;
  clear_has_sig();
}
inline ::google::protobuf::int32 SignalDeliverFtraceEvent::sig() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.SignalDeliverFtraceEvent.sig)
  return sig_;
}
inline void SignalDeliverFtraceEvent::set_sig(::google::protobuf::int32 value) {
  set_has_sig();
  sig_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.SignalDeliverFtraceEvent.sig)
}

// -------------------------------------------------------------------

// SignalGenerateFtraceEvent

// optional int32 code = 1;
inline bool SignalGenerateFtraceEvent::has_code() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void SignalGenerateFtraceEvent::set_has_code() {
  _has_bits_[0] |= 0x00000001u;
}
inline void SignalGenerateFtraceEvent::clear_has_code() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void SignalGenerateFtraceEvent::clear_code() {
  code_ = 0;
  clear_has_code();
}
inline ::google::protobuf::int32 SignalGenerateFtraceEvent::code() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.SignalGenerateFtraceEvent.code)
  return code_;
}
inline void SignalGenerateFtraceEvent::set_code(::google::protobuf::int32 value) {
  set_has_code();
  code_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.SignalGenerateFtraceEvent.code)
}

// optional string comm = 2;
inline bool SignalGenerateFtraceEvent::has_comm() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void SignalGenerateFtraceEvent::set_has_comm() {
  _has_bits_[0] |= 0x00000002u;
}
inline void SignalGenerateFtraceEvent::clear_has_comm() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void SignalGenerateFtraceEvent::clear_comm() {
  comm_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_comm();
}
inline const ::std::string& SignalGenerateFtraceEvent::comm() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.SignalGenerateFtraceEvent.comm)
  return comm_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void SignalGenerateFtraceEvent::set_comm(const ::std::string& value) {
  set_has_comm();
  comm_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:perfetto.protos.SignalGenerateFtraceEvent.comm)
}
inline void SignalGenerateFtraceEvent::set_comm(const char* value) {
  set_has_comm();
  comm_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:perfetto.protos.SignalGenerateFtraceEvent.comm)
}
inline void SignalGenerateFtraceEvent::set_comm(const char* value, size_t size) {
  set_has_comm();
  comm_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:perfetto.protos.SignalGenerateFtraceEvent.comm)
}
inline ::std::string* SignalGenerateFtraceEvent::mutable_comm() {
  set_has_comm();
  // @@protoc_insertion_point(field_mutable:perfetto.protos.SignalGenerateFtraceEvent.comm)
  return comm_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* SignalGenerateFtraceEvent::release_comm() {
  // @@protoc_insertion_point(field_release:perfetto.protos.SignalGenerateFtraceEvent.comm)
  clear_has_comm();
  return comm_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void SignalGenerateFtraceEvent::set_allocated_comm(::std::string* comm) {
  if (comm != NULL) {
    set_has_comm();
  } else {
    clear_has_comm();
  }
  comm_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), comm);
  // @@protoc_insertion_point(field_set_allocated:perfetto.protos.SignalGenerateFtraceEvent.comm)
}

// optional int32 group = 3;
inline bool SignalGenerateFtraceEvent::has_group() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void SignalGenerateFtraceEvent::set_has_group() {
  _has_bits_[0] |= 0x00000004u;
}
inline void SignalGenerateFtraceEvent::clear_has_group() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void SignalGenerateFtraceEvent::clear_group() {
  group_ = 0;
  clear_has_group();
}
inline ::google::protobuf::int32 SignalGenerateFtraceEvent::group() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.SignalGenerateFtraceEvent.group)
  return group_;
}
inline void SignalGenerateFtraceEvent::set_group(::google::protobuf::int32 value) {
  set_has_group();
  group_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.SignalGenerateFtraceEvent.group)
}

// optional int32 pid = 4;
inline bool SignalGenerateFtraceEvent::has_pid() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void SignalGenerateFtraceEvent::set_has_pid() {
  _has_bits_[0] |= 0x00000008u;
}
inline void SignalGenerateFtraceEvent::clear_has_pid() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void SignalGenerateFtraceEvent::clear_pid() {
  pid_ = 0;
  clear_has_pid();
}
inline ::google::protobuf::int32 SignalGenerateFtraceEvent::pid() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.SignalGenerateFtraceEvent.pid)
  return pid_;
}
inline void SignalGenerateFtraceEvent::set_pid(::google::protobuf::int32 value) {
  set_has_pid();
  pid_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.SignalGenerateFtraceEvent.pid)
}

// optional int32 result = 5;
inline bool SignalGenerateFtraceEvent::has_result() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
inline void SignalGenerateFtraceEvent::set_has_result() {
  _has_bits_[0] |= 0x00000010u;
}
inline void SignalGenerateFtraceEvent::clear_has_result() {
  _has_bits_[0] &= ~0x00000010u;
}
inline void SignalGenerateFtraceEvent::clear_result() {
  result_ = 0;
  clear_has_result();
}
inline ::google::protobuf::int32 SignalGenerateFtraceEvent::result() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.SignalGenerateFtraceEvent.result)
  return result_;
}
inline void SignalGenerateFtraceEvent::set_result(::google::protobuf::int32 value) {
  set_has_result();
  result_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.SignalGenerateFtraceEvent.result)
}

// optional int32 sig = 6;
inline bool SignalGenerateFtraceEvent::has_sig() const {
  return (_has_bits_[0] & 0x00000020u) != 0;
}
inline void SignalGenerateFtraceEvent::set_has_sig() {
  _has_bits_[0] |= 0x00000020u;
}
inline void SignalGenerateFtraceEvent::clear_has_sig() {
  _has_bits_[0] &= ~0x00000020u;
}
inline void SignalGenerateFtraceEvent::clear_sig() {
  sig_ = 0;
  clear_has_sig();
}
inline ::google::protobuf::int32 SignalGenerateFtraceEvent::sig() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.SignalGenerateFtraceEvent.sig)
  return sig_;
}
inline void SignalGenerateFtraceEvent::set_sig(::google::protobuf::int32 value) {
  set_has_sig();
  sig_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.SignalGenerateFtraceEvent.sig)
}

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS
// -------------------------------------------------------------------


// @@protoc_insertion_point(namespace_scope)

}  // namespace protos
}  // namespace perfetto

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_perfetto_2ftrace_2fftrace_2fsignal_2eproto__INCLUDED
