// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: perfetto/common/commit_data_request.proto

#ifndef PROTOBUF_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto__INCLUDED
#define PROTOBUF_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
// @@protoc_insertion_point(includes)

namespace perfetto {
namespace protos {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();
void protobuf_AssignDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();
void protobuf_ShutdownFile_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();

class CommitDataRequest;
class CommitDataRequest_ChunkToPatch;
class CommitDataRequest_ChunkToPatch_Patch;
class CommitDataRequest_ChunksToMove;

// ===================================================================

class CommitDataRequest_ChunksToMove : public ::google::protobuf::MessageLite {
 public:
  CommitDataRequest_ChunksToMove();
  virtual ~CommitDataRequest_ChunksToMove();

  CommitDataRequest_ChunksToMove(const CommitDataRequest_ChunksToMove& from);

  inline CommitDataRequest_ChunksToMove& operator=(const CommitDataRequest_ChunksToMove& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const CommitDataRequest_ChunksToMove& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const CommitDataRequest_ChunksToMove* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(CommitDataRequest_ChunksToMove* other);

  // implements Message ----------------------------------------------

  inline CommitDataRequest_ChunksToMove* New() const { return New(NULL); }

  CommitDataRequest_ChunksToMove* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const CommitDataRequest_ChunksToMove& from);
  void MergeFrom(const CommitDataRequest_ChunksToMove& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(CommitDataRequest_ChunksToMove* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional uint32 page = 1;
  bool has_page() const;
  void clear_page();
  static const int kPageFieldNumber = 1;
  ::google::protobuf::uint32 page() const;
  void set_page(::google::protobuf::uint32 value);

  // optional uint32 chunk = 2;
  bool has_chunk() const;
  void clear_chunk();
  static const int kChunkFieldNumber = 2;
  ::google::protobuf::uint32 chunk() const;
  void set_chunk(::google::protobuf::uint32 value);

  // optional uint32 target_buffer = 3;
  bool has_target_buffer() const;
  void clear_target_buffer();
  static const int kTargetBufferFieldNumber = 3;
  ::google::protobuf::uint32 target_buffer() const;
  void set_target_buffer(::google::protobuf::uint32 value);

  // @@protoc_insertion_point(class_scope:perfetto.protos.CommitDataRequest.ChunksToMove)
 private:
  inline void set_has_page();
  inline void clear_has_page();
  inline void set_has_chunk();
  inline void clear_has_chunk();
  inline void set_has_target_buffer();
  inline void clear_has_target_buffer();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::uint32 page_;
  ::google::protobuf::uint32 chunk_;
  ::google::protobuf::uint32 target_buffer_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();

  void InitAsDefaultInstance();
  static CommitDataRequest_ChunksToMove* default_instance_;
};
// -------------------------------------------------------------------

class CommitDataRequest_ChunkToPatch_Patch : public ::google::protobuf::MessageLite {
 public:
  CommitDataRequest_ChunkToPatch_Patch();
  virtual ~CommitDataRequest_ChunkToPatch_Patch();

  CommitDataRequest_ChunkToPatch_Patch(const CommitDataRequest_ChunkToPatch_Patch& from);

  inline CommitDataRequest_ChunkToPatch_Patch& operator=(const CommitDataRequest_ChunkToPatch_Patch& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const CommitDataRequest_ChunkToPatch_Patch& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const CommitDataRequest_ChunkToPatch_Patch* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(CommitDataRequest_ChunkToPatch_Patch* other);

  // implements Message ----------------------------------------------

  inline CommitDataRequest_ChunkToPatch_Patch* New() const { return New(NULL); }

  CommitDataRequest_ChunkToPatch_Patch* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const CommitDataRequest_ChunkToPatch_Patch& from);
  void MergeFrom(const CommitDataRequest_ChunkToPatch_Patch& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(CommitDataRequest_ChunkToPatch_Patch* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional uint32 offset = 1;
  bool has_offset() const;
  void clear_offset();
  static const int kOffsetFieldNumber = 1;
  ::google::protobuf::uint32 offset() const;
  void set_offset(::google::protobuf::uint32 value);

  // optional bytes data = 2;
  bool has_data() const;
  void clear_data();
  static const int kDataFieldNumber = 2;
  const ::std::string& data() const;
  void set_data(const ::std::string& value);
  void set_data(const char* value);
  void set_data(const void* value, size_t size);
  ::std::string* mutable_data();
  ::std::string* release_data();
  void set_allocated_data(::std::string* data);

  // @@protoc_insertion_point(class_scope:perfetto.protos.CommitDataRequest.ChunkToPatch.Patch)
 private:
  inline void set_has_offset();
  inline void clear_has_offset();
  inline void set_has_data();
  inline void clear_has_data();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::internal::ArenaStringPtr data_;
  ::google::protobuf::uint32 offset_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();

  void InitAsDefaultInstance();
  static CommitDataRequest_ChunkToPatch_Patch* default_instance_;
};
// -------------------------------------------------------------------

class CommitDataRequest_ChunkToPatch : public ::google::protobuf::MessageLite {
 public:
  CommitDataRequest_ChunkToPatch();
  virtual ~CommitDataRequest_ChunkToPatch();

  CommitDataRequest_ChunkToPatch(const CommitDataRequest_ChunkToPatch& from);

  inline CommitDataRequest_ChunkToPatch& operator=(const CommitDataRequest_ChunkToPatch& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const CommitDataRequest_ChunkToPatch& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const CommitDataRequest_ChunkToPatch* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(CommitDataRequest_ChunkToPatch* other);

  // implements Message ----------------------------------------------

  inline CommitDataRequest_ChunkToPatch* New() const { return New(NULL); }

  CommitDataRequest_ChunkToPatch* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const CommitDataRequest_ChunkToPatch& from);
  void MergeFrom(const CommitDataRequest_ChunkToPatch& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(CommitDataRequest_ChunkToPatch* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  typedef CommitDataRequest_ChunkToPatch_Patch Patch;

  // accessors -------------------------------------------------------

  // optional uint32 target_buffer = 1;
  bool has_target_buffer() const;
  void clear_target_buffer();
  static const int kTargetBufferFieldNumber = 1;
  ::google::protobuf::uint32 target_buffer() const;
  void set_target_buffer(::google::protobuf::uint32 value);

  // optional uint32 writer_id = 2;
  bool has_writer_id() const;
  void clear_writer_id();
  static const int kWriterIdFieldNumber = 2;
  ::google::protobuf::uint32 writer_id() const;
  void set_writer_id(::google::protobuf::uint32 value);

  // optional uint32 chunk_id = 3;
  bool has_chunk_id() const;
  void clear_chunk_id();
  static const int kChunkIdFieldNumber = 3;
  ::google::protobuf::uint32 chunk_id() const;
  void set_chunk_id(::google::protobuf::uint32 value);

  // repeated .perfetto.protos.CommitDataRequest.ChunkToPatch.Patch patches = 4;
  int patches_size() const;
  void clear_patches();
  static const int kPatchesFieldNumber = 4;
  const ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch& patches(int index) const;
  ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch* mutable_patches(int index);
  ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch* add_patches();
  ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch >*
      mutable_patches();
  const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch >&
      patches() const;

  // optional bool has_more_patches = 5;
  bool has_has_more_patches() const;
  void clear_has_more_patches();
  static const int kHasMorePatchesFieldNumber = 5;
  bool has_more_patches() const;
  void set_has_more_patches(bool value);

  // @@protoc_insertion_point(class_scope:perfetto.protos.CommitDataRequest.ChunkToPatch)
 private:
  inline void set_has_target_buffer();
  inline void clear_has_target_buffer();
  inline void set_has_writer_id();
  inline void clear_has_writer_id();
  inline void set_has_chunk_id();
  inline void clear_has_chunk_id();
  inline void set_has_has_more_patches();
  inline void clear_has_has_more_patches();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::uint32 target_buffer_;
  ::google::protobuf::uint32 writer_id_;
  ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch > patches_;
  ::google::protobuf::uint32 chunk_id_;
  bool has_more_patches_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();

  void InitAsDefaultInstance();
  static CommitDataRequest_ChunkToPatch* default_instance_;
};
// -------------------------------------------------------------------

class CommitDataRequest : public ::google::protobuf::MessageLite {
 public:
  CommitDataRequest();
  virtual ~CommitDataRequest();

  CommitDataRequest(const CommitDataRequest& from);

  inline CommitDataRequest& operator=(const CommitDataRequest& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const CommitDataRequest& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const CommitDataRequest* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(CommitDataRequest* other);

  // implements Message ----------------------------------------------

  inline CommitDataRequest* New() const { return New(NULL); }

  CommitDataRequest* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const CommitDataRequest& from);
  void MergeFrom(const CommitDataRequest& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(CommitDataRequest* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  typedef CommitDataRequest_ChunksToMove ChunksToMove;
  typedef CommitDataRequest_ChunkToPatch ChunkToPatch;

  // accessors -------------------------------------------------------

  // repeated .perfetto.protos.CommitDataRequest.ChunksToMove chunks_to_move = 1;
  int chunks_to_move_size() const;
  void clear_chunks_to_move();
  static const int kChunksToMoveFieldNumber = 1;
  const ::perfetto::protos::CommitDataRequest_ChunksToMove& chunks_to_move(int index) const;
  ::perfetto::protos::CommitDataRequest_ChunksToMove* mutable_chunks_to_move(int index);
  ::perfetto::protos::CommitDataRequest_ChunksToMove* add_chunks_to_move();
  ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunksToMove >*
      mutable_chunks_to_move();
  const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunksToMove >&
      chunks_to_move() const;

  // repeated .perfetto.protos.CommitDataRequest.ChunkToPatch chunks_to_patch = 2;
  int chunks_to_patch_size() const;
  void clear_chunks_to_patch();
  static const int kChunksToPatchFieldNumber = 2;
  const ::perfetto::protos::CommitDataRequest_ChunkToPatch& chunks_to_patch(int index) const;
  ::perfetto::protos::CommitDataRequest_ChunkToPatch* mutable_chunks_to_patch(int index);
  ::perfetto::protos::CommitDataRequest_ChunkToPatch* add_chunks_to_patch();
  ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunkToPatch >*
      mutable_chunks_to_patch();
  const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunkToPatch >&
      chunks_to_patch() const;

  // optional uint64 flush_request_id = 3;
  bool has_flush_request_id() const;
  void clear_flush_request_id();
  static const int kFlushRequestIdFieldNumber = 3;
  ::google::protobuf::uint64 flush_request_id() const;
  void set_flush_request_id(::google::protobuf::uint64 value);

  // @@protoc_insertion_point(class_scope:perfetto.protos.CommitDataRequest)
 private:
  inline void set_has_flush_request_id();
  inline void clear_has_flush_request_id();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunksToMove > chunks_to_move_;
  ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunkToPatch > chunks_to_patch_;
  ::google::protobuf::uint64 flush_request_id_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();
  #endif
  friend void protobuf_AssignDesc_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();
  friend void protobuf_ShutdownFile_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto();

  void InitAsDefaultInstance();
  static CommitDataRequest* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// CommitDataRequest_ChunksToMove

// optional uint32 page = 1;
inline bool CommitDataRequest_ChunksToMove::has_page() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void CommitDataRequest_ChunksToMove::set_has_page() {
  _has_bits_[0] |= 0x00000001u;
}
inline void CommitDataRequest_ChunksToMove::clear_has_page() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void CommitDataRequest_ChunksToMove::clear_page() {
  page_ = 0u;
  clear_has_page();
}
inline ::google::protobuf::uint32 CommitDataRequest_ChunksToMove::page() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.ChunksToMove.page)
  return page_;
}
inline void CommitDataRequest_ChunksToMove::set_page(::google::protobuf::uint32 value) {
  set_has_page();
  page_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.CommitDataRequest.ChunksToMove.page)
}

// optional uint32 chunk = 2;
inline bool CommitDataRequest_ChunksToMove::has_chunk() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void CommitDataRequest_ChunksToMove::set_has_chunk() {
  _has_bits_[0] |= 0x00000002u;
}
inline void CommitDataRequest_ChunksToMove::clear_has_chunk() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void CommitDataRequest_ChunksToMove::clear_chunk() {
  chunk_ = 0u;
  clear_has_chunk();
}
inline ::google::protobuf::uint32 CommitDataRequest_ChunksToMove::chunk() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.ChunksToMove.chunk)
  return chunk_;
}
inline void CommitDataRequest_ChunksToMove::set_chunk(::google::protobuf::uint32 value) {
  set_has_chunk();
  chunk_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.CommitDataRequest.ChunksToMove.chunk)
}

// optional uint32 target_buffer = 3;
inline bool CommitDataRequest_ChunksToMove::has_target_buffer() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void CommitDataRequest_ChunksToMove::set_has_target_buffer() {
  _has_bits_[0] |= 0x00000004u;
}
inline void CommitDataRequest_ChunksToMove::clear_has_target_buffer() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void CommitDataRequest_ChunksToMove::clear_target_buffer() {
  target_buffer_ = 0u;
  clear_has_target_buffer();
}
inline ::google::protobuf::uint32 CommitDataRequest_ChunksToMove::target_buffer() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.ChunksToMove.target_buffer)
  return target_buffer_;
}
inline void CommitDataRequest_ChunksToMove::set_target_buffer(::google::protobuf::uint32 value) {
  set_has_target_buffer();
  target_buffer_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.CommitDataRequest.ChunksToMove.target_buffer)
}

// -------------------------------------------------------------------

// CommitDataRequest_ChunkToPatch_Patch

// optional uint32 offset = 1;
inline bool CommitDataRequest_ChunkToPatch_Patch::has_offset() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void CommitDataRequest_ChunkToPatch_Patch::set_has_offset() {
  _has_bits_[0] |= 0x00000001u;
}
inline void CommitDataRequest_ChunkToPatch_Patch::clear_has_offset() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void CommitDataRequest_ChunkToPatch_Patch::clear_offset() {
  offset_ = 0u;
  clear_has_offset();
}
inline ::google::protobuf::uint32 CommitDataRequest_ChunkToPatch_Patch::offset() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.ChunkToPatch.Patch.offset)
  return offset_;
}
inline void CommitDataRequest_ChunkToPatch_Patch::set_offset(::google::protobuf::uint32 value) {
  set_has_offset();
  offset_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.CommitDataRequest.ChunkToPatch.Patch.offset)
}

// optional bytes data = 2;
inline bool CommitDataRequest_ChunkToPatch_Patch::has_data() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void CommitDataRequest_ChunkToPatch_Patch::set_has_data() {
  _has_bits_[0] |= 0x00000002u;
}
inline void CommitDataRequest_ChunkToPatch_Patch::clear_has_data() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void CommitDataRequest_ChunkToPatch_Patch::clear_data() {
  data_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_data();
}
inline const ::std::string& CommitDataRequest_ChunkToPatch_Patch::data() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.ChunkToPatch.Patch.data)
  return data_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void CommitDataRequest_ChunkToPatch_Patch::set_data(const ::std::string& value) {
  set_has_data();
  data_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:perfetto.protos.CommitDataRequest.ChunkToPatch.Patch.data)
}
inline void CommitDataRequest_ChunkToPatch_Patch::set_data(const char* value) {
  set_has_data();
  data_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:perfetto.protos.CommitDataRequest.ChunkToPatch.Patch.data)
}
inline void CommitDataRequest_ChunkToPatch_Patch::set_data(const void* value, size_t size) {
  set_has_data();
  data_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:perfetto.protos.CommitDataRequest.ChunkToPatch.Patch.data)
}
inline ::std::string* CommitDataRequest_ChunkToPatch_Patch::mutable_data() {
  set_has_data();
  // @@protoc_insertion_point(field_mutable:perfetto.protos.CommitDataRequest.ChunkToPatch.Patch.data)
  return data_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* CommitDataRequest_ChunkToPatch_Patch::release_data() {
  // @@protoc_insertion_point(field_release:perfetto.protos.CommitDataRequest.ChunkToPatch.Patch.data)
  clear_has_data();
  return data_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void CommitDataRequest_ChunkToPatch_Patch::set_allocated_data(::std::string* data) {
  if (data != NULL) {
    set_has_data();
  } else {
    clear_has_data();
  }
  data_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), data);
  // @@protoc_insertion_point(field_set_allocated:perfetto.protos.CommitDataRequest.ChunkToPatch.Patch.data)
}

// -------------------------------------------------------------------

// CommitDataRequest_ChunkToPatch

// optional uint32 target_buffer = 1;
inline bool CommitDataRequest_ChunkToPatch::has_target_buffer() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void CommitDataRequest_ChunkToPatch::set_has_target_buffer() {
  _has_bits_[0] |= 0x00000001u;
}
inline void CommitDataRequest_ChunkToPatch::clear_has_target_buffer() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void CommitDataRequest_ChunkToPatch::clear_target_buffer() {
  target_buffer_ = 0u;
  clear_has_target_buffer();
}
inline ::google::protobuf::uint32 CommitDataRequest_ChunkToPatch::target_buffer() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.ChunkToPatch.target_buffer)
  return target_buffer_;
}
inline void CommitDataRequest_ChunkToPatch::set_target_buffer(::google::protobuf::uint32 value) {
  set_has_target_buffer();
  target_buffer_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.CommitDataRequest.ChunkToPatch.target_buffer)
}

// optional uint32 writer_id = 2;
inline bool CommitDataRequest_ChunkToPatch::has_writer_id() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void CommitDataRequest_ChunkToPatch::set_has_writer_id() {
  _has_bits_[0] |= 0x00000002u;
}
inline void CommitDataRequest_ChunkToPatch::clear_has_writer_id() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void CommitDataRequest_ChunkToPatch::clear_writer_id() {
  writer_id_ = 0u;
  clear_has_writer_id();
}
inline ::google::protobuf::uint32 CommitDataRequest_ChunkToPatch::writer_id() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.ChunkToPatch.writer_id)
  return writer_id_;
}
inline void CommitDataRequest_ChunkToPatch::set_writer_id(::google::protobuf::uint32 value) {
  set_has_writer_id();
  writer_id_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.CommitDataRequest.ChunkToPatch.writer_id)
}

// optional uint32 chunk_id = 3;
inline bool CommitDataRequest_ChunkToPatch::has_chunk_id() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void CommitDataRequest_ChunkToPatch::set_has_chunk_id() {
  _has_bits_[0] |= 0x00000004u;
}
inline void CommitDataRequest_ChunkToPatch::clear_has_chunk_id() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void CommitDataRequest_ChunkToPatch::clear_chunk_id() {
  chunk_id_ = 0u;
  clear_has_chunk_id();
}
inline ::google::protobuf::uint32 CommitDataRequest_ChunkToPatch::chunk_id() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.ChunkToPatch.chunk_id)
  return chunk_id_;
}
inline void CommitDataRequest_ChunkToPatch::set_chunk_id(::google::protobuf::uint32 value) {
  set_has_chunk_id();
  chunk_id_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.CommitDataRequest.ChunkToPatch.chunk_id)
}

// repeated .perfetto.protos.CommitDataRequest.ChunkToPatch.Patch patches = 4;
inline int CommitDataRequest_ChunkToPatch::patches_size() const {
  return patches_.size();
}
inline void CommitDataRequest_ChunkToPatch::clear_patches() {
  patches_.Clear();
}
inline const ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch& CommitDataRequest_ChunkToPatch::patches(int index) const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.ChunkToPatch.patches)
  return patches_.Get(index);
}
inline ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch* CommitDataRequest_ChunkToPatch::mutable_patches(int index) {
  // @@protoc_insertion_point(field_mutable:perfetto.protos.CommitDataRequest.ChunkToPatch.patches)
  return patches_.Mutable(index);
}
inline ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch* CommitDataRequest_ChunkToPatch::add_patches() {
  // @@protoc_insertion_point(field_add:perfetto.protos.CommitDataRequest.ChunkToPatch.patches)
  return patches_.Add();
}
inline ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch >*
CommitDataRequest_ChunkToPatch::mutable_patches() {
  // @@protoc_insertion_point(field_mutable_list:perfetto.protos.CommitDataRequest.ChunkToPatch.patches)
  return &patches_;
}
inline const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunkToPatch_Patch >&
CommitDataRequest_ChunkToPatch::patches() const {
  // @@protoc_insertion_point(field_list:perfetto.protos.CommitDataRequest.ChunkToPatch.patches)
  return patches_;
}

// optional bool has_more_patches = 5;
inline bool CommitDataRequest_ChunkToPatch::has_has_more_patches() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
inline void CommitDataRequest_ChunkToPatch::set_has_has_more_patches() {
  _has_bits_[0] |= 0x00000010u;
}
inline void CommitDataRequest_ChunkToPatch::clear_has_has_more_patches() {
  _has_bits_[0] &= ~0x00000010u;
}
inline void CommitDataRequest_ChunkToPatch::clear_has_more_patches() {
  has_more_patches_ = false;
  clear_has_has_more_patches();
}
inline bool CommitDataRequest_ChunkToPatch::has_more_patches() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.ChunkToPatch.has_more_patches)
  return has_more_patches_;
}
inline void CommitDataRequest_ChunkToPatch::set_has_more_patches(bool value) {
  set_has_has_more_patches();
  has_more_patches_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.CommitDataRequest.ChunkToPatch.has_more_patches)
}

// -------------------------------------------------------------------

// CommitDataRequest

// repeated .perfetto.protos.CommitDataRequest.ChunksToMove chunks_to_move = 1;
inline int CommitDataRequest::chunks_to_move_size() const {
  return chunks_to_move_.size();
}
inline void CommitDataRequest::clear_chunks_to_move() {
  chunks_to_move_.Clear();
}
inline const ::perfetto::protos::CommitDataRequest_ChunksToMove& CommitDataRequest::chunks_to_move(int index) const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.chunks_to_move)
  return chunks_to_move_.Get(index);
}
inline ::perfetto::protos::CommitDataRequest_ChunksToMove* CommitDataRequest::mutable_chunks_to_move(int index) {
  // @@protoc_insertion_point(field_mutable:perfetto.protos.CommitDataRequest.chunks_to_move)
  return chunks_to_move_.Mutable(index);
}
inline ::perfetto::protos::CommitDataRequest_ChunksToMove* CommitDataRequest::add_chunks_to_move() {
  // @@protoc_insertion_point(field_add:perfetto.protos.CommitDataRequest.chunks_to_move)
  return chunks_to_move_.Add();
}
inline ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunksToMove >*
CommitDataRequest::mutable_chunks_to_move() {
  // @@protoc_insertion_point(field_mutable_list:perfetto.protos.CommitDataRequest.chunks_to_move)
  return &chunks_to_move_;
}
inline const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunksToMove >&
CommitDataRequest::chunks_to_move() const {
  // @@protoc_insertion_point(field_list:perfetto.protos.CommitDataRequest.chunks_to_move)
  return chunks_to_move_;
}

// repeated .perfetto.protos.CommitDataRequest.ChunkToPatch chunks_to_patch = 2;
inline int CommitDataRequest::chunks_to_patch_size() const {
  return chunks_to_patch_.size();
}
inline void CommitDataRequest::clear_chunks_to_patch() {
  chunks_to_patch_.Clear();
}
inline const ::perfetto::protos::CommitDataRequest_ChunkToPatch& CommitDataRequest::chunks_to_patch(int index) const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.chunks_to_patch)
  return chunks_to_patch_.Get(index);
}
inline ::perfetto::protos::CommitDataRequest_ChunkToPatch* CommitDataRequest::mutable_chunks_to_patch(int index) {
  // @@protoc_insertion_point(field_mutable:perfetto.protos.CommitDataRequest.chunks_to_patch)
  return chunks_to_patch_.Mutable(index);
}
inline ::perfetto::protos::CommitDataRequest_ChunkToPatch* CommitDataRequest::add_chunks_to_patch() {
  // @@protoc_insertion_point(field_add:perfetto.protos.CommitDataRequest.chunks_to_patch)
  return chunks_to_patch_.Add();
}
inline ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunkToPatch >*
CommitDataRequest::mutable_chunks_to_patch() {
  // @@protoc_insertion_point(field_mutable_list:perfetto.protos.CommitDataRequest.chunks_to_patch)
  return &chunks_to_patch_;
}
inline const ::google::protobuf::RepeatedPtrField< ::perfetto::protos::CommitDataRequest_ChunkToPatch >&
CommitDataRequest::chunks_to_patch() const {
  // @@protoc_insertion_point(field_list:perfetto.protos.CommitDataRequest.chunks_to_patch)
  return chunks_to_patch_;
}

// optional uint64 flush_request_id = 3;
inline bool CommitDataRequest::has_flush_request_id() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void CommitDataRequest::set_has_flush_request_id() {
  _has_bits_[0] |= 0x00000004u;
}
inline void CommitDataRequest::clear_has_flush_request_id() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void CommitDataRequest::clear_flush_request_id() {
  flush_request_id_ = GOOGLE_ULONGLONG(0);
  clear_has_flush_request_id();
}
inline ::google::protobuf::uint64 CommitDataRequest::flush_request_id() const {
  // @@protoc_insertion_point(field_get:perfetto.protos.CommitDataRequest.flush_request_id)
  return flush_request_id_;
}
inline void CommitDataRequest::set_flush_request_id(::google::protobuf::uint64 value) {
  set_has_flush_request_id();
  flush_request_id_ = value;
  // @@protoc_insertion_point(field_set:perfetto.protos.CommitDataRequest.flush_request_id)
}

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS
// -------------------------------------------------------------------

// -------------------------------------------------------------------

// -------------------------------------------------------------------


// @@protoc_insertion_point(namespace_scope)

}  // namespace protos
}  // namespace perfetto

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_perfetto_2fcommon_2fcommit_5fdata_5frequest_2eproto__INCLUDED
