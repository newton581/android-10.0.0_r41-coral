#include <iostream>

#include "gc/space/space.h"

// This was automatically generated by /home/gitpod/android/aosp/out/soong/.temp/Soong.python_3kJ6gx/generate_operator_out.py --- do not edit!
namespace art {
namespace gc {
namespace space {
std::ostream& operator<<(std::ostream& os, const SpaceType& rhs) {
  switch (rhs) {
    case kSpaceTypeImageSpace: os << "SpaceTypeImageSpace"; break;
    case kSpaceTypeMallocSpace: os << "SpaceTypeMallocSpace"; break;
    case kSpaceTypeZygoteSpace: os << "SpaceTypeZygoteSpace"; break;
    case kSpaceTypeBumpPointerSpace: os << "SpaceTypeBumpPointerSpace"; break;
    case kSpaceTypeLargeObjectSpace: os << "SpaceTypeLargeObjectSpace"; break;
    case kSpaceTypeRegionSpace: os << "SpaceTypeRegionSpace"; break;
    default: os << "SpaceType[" << static_cast<int>(rhs) << "]"; break;
  }
  return os;
}
}  // namespace space
}  // namespace gc
}  // namespace art

// This was automatically generated by /home/gitpod/android/aosp/out/soong/.temp/Soong.python_3kJ6gx/generate_operator_out.py --- do not edit!
namespace art {
namespace gc {
namespace space {
std::ostream& operator<<(std::ostream& os, const GcRetentionPolicy& rhs) {
  switch (rhs) {
    case kGcRetentionPolicyNeverCollect: os << "GcRetentionPolicyNeverCollect"; break;
    case kGcRetentionPolicyAlwaysCollect: os << "GcRetentionPolicyAlwaysCollect"; break;
    case kGcRetentionPolicyFullCollect: os << "GcRetentionPolicyFullCollect"; break;
    default: os << "GcRetentionPolicy[" << static_cast<int>(rhs) << "]"; break;
  }
  return os;
}
}  // namespace space
}  // namespace gc
}  // namespace art

