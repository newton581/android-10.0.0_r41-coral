#include <iostream>

#include "gc/heap.h"

// This was automatically generated by /home/gitpod/android/aosp/out/soong/.temp/Soong.python_mKlHH4/generate_operator_out.py --- do not edit!
namespace art {
namespace gc {
std::ostream& operator<<(std::ostream& os, const HomogeneousSpaceCompactResult& rhs) {
  switch (rhs) {
    case kSuccess: os << "Success"; break;
    case kErrorReject: os << "ErrorReject"; break;
    case kErrorUnsupported: os << "ErrorUnsupported"; break;
    case kErrorVMShuttingDown: os << "ErrorVMShuttingDown"; break;
    default: os << "HomogeneousSpaceCompactResult[" << static_cast<int>(rhs) << "]"; break;
  }
  return os;
}
}  // namespace gc
}  // namespace art

