#include <iostream>

#include "jdwp_provider.h"

// This was automatically generated by /home/gitpod/android/aosp/out/soong/.temp/Soong.python_qetOd5/generate_operator_out.py --- do not edit!
namespace art {
std::ostream& operator<<(std::ostream& os, const JdwpProvider& rhs) {
  switch (rhs) {
    case JdwpProvider::kNone: os << "None"; break;
    case JdwpProvider::kUnset: os << "Unset"; break;
    case JdwpProvider::kInternal: os << "Internal"; break;
    case JdwpProvider::kAdbConnection: os << "AdbConnection"; break;
  }
  return os;
}
}  // namespace art

