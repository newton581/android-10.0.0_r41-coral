/*
* Copyright (C) 2017 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.heapdump;
public class DiffedFieldValue
{
public static enum Status
{
ADDED(),
DELETED(),
MATCHED();
}
DiffedFieldValue() { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.DiffedFieldValue matched(com.android.ahat.heapdump.FieldValue current, com.android.ahat.heapdump.FieldValue baseline) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.DiffedFieldValue added(com.android.ahat.heapdump.FieldValue current) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.DiffedFieldValue deleted(com.android.ahat.heapdump.FieldValue baseline) { throw new RuntimeException("Stub!"); }
public  boolean equals(java.lang.Object otherObject) { throw new RuntimeException("Stub!"); }
public  java.lang.String toString() { throw new RuntimeException("Stub!"); }
public final com.android.ahat.heapdump.Value baseline;
public final com.android.ahat.heapdump.Value current;
public final java.lang.String name;
public final com.android.ahat.heapdump.DiffedFieldValue.Status status;
public final com.android.ahat.heapdump.Type type;
}
