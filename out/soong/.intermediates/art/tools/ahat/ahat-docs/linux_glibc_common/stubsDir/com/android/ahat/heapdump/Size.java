/*
* Copyright (C) 2017 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.heapdump;
public class Size
{
public  Size(long javaSize, long registeredNativeSize) { throw new RuntimeException("Stub!"); }
public  long getSize() { throw new RuntimeException("Stub!"); }
public  long getJavaSize() { throw new RuntimeException("Stub!"); }
public  long getRegisteredNativeSize() { throw new RuntimeException("Stub!"); }
public  boolean isZero() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Size plus(com.android.ahat.heapdump.Size other) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Size plusRegisteredNativeSize(long size) { throw new RuntimeException("Stub!"); }
public  boolean equals(java.lang.Object other) { throw new RuntimeException("Stub!"); }
public static com.android.ahat.heapdump.Size ZERO;
}
