/*
* Copyright (C) 2016 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.heapdump;
public class AhatArrayInstance
  extends com.android.ahat.heapdump.AhatInstance
{
AhatArrayInstance() { throw new RuntimeException("Stub!"); }
public  int getLength() { throw new RuntimeException("Stub!"); }
public  java.util.List<com.android.ahat.heapdump.Value> getValues() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Value getValue(int index) { throw new RuntimeException("Stub!"); }
public  boolean isArrayInstance() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatArrayInstance asArrayInstance() { throw new RuntimeException("Stub!"); }
public  java.lang.String asString(int maxChars) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatInstance getAssociatedBitmapInstance() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatClassObj getAssociatedClassForOverhead() { throw new RuntimeException("Stub!"); }
public  java.lang.String toString() { throw new RuntimeException("Stub!"); }
}
