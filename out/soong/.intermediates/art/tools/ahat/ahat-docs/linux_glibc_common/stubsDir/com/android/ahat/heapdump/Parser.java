/*
* Copyright (C) 2017 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.heapdump;
public class Parser
{
public  Parser(java.nio.ByteBuffer hprof) { throw new RuntimeException("Stub!"); }
public  Parser(java.io.File hprof) throws java.io.IOException { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Parser map(com.android.ahat.proguard.ProguardMap map) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Parser progress(com.android.ahat.progress.Progress progress) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Parser retained(com.android.ahat.heapdump.Reachability retained) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatSnapshot parse() throws java.io.IOException, com.android.ahat.heapdump.HprofFormatException { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.AhatSnapshot parseHeapDump(java.io.File hprof, com.android.ahat.proguard.ProguardMap map) throws java.io.IOException, com.android.ahat.heapdump.HprofFormatException { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.AhatSnapshot parseHeapDump(java.nio.ByteBuffer hprof, com.android.ahat.proguard.ProguardMap map) throws java.io.IOException, com.android.ahat.heapdump.HprofFormatException { throw new RuntimeException("Stub!"); }
}
