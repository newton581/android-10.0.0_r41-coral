/*
* Copyright (C) 2015 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.heapdump;
public class Site
  implements com.android.ahat.heapdump.Diffable<com.android.ahat.heapdump.Site>
{
public static class ObjectsInfo
  implements com.android.ahat.heapdump.Diffable<com.android.ahat.heapdump.Site.ObjectsInfo>
{
ObjectsInfo() { throw new RuntimeException("Stub!"); }
public  java.lang.String getClassName() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Site.ObjectsInfo getBaseline() { throw new RuntimeException("Stub!"); }
public  boolean isPlaceHolder() { throw new RuntimeException("Stub!"); }
public com.android.ahat.heapdump.AhatClassObj classObj;
public com.android.ahat.heapdump.AhatHeap heap;
public com.android.ahat.heapdump.Size numBytes;
public long numInstances;
}
Site() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Size getSize(com.android.ahat.heapdump.AhatHeap heap) { throw new RuntimeException("Stub!"); }
public  void getObjects(java.lang.String heapName, java.lang.String className, java.util.Collection<com.android.ahat.heapdump.AhatInstance> objects) { throw new RuntimeException("Stub!"); }
public  void getObjects(java.util.function.Predicate<com.android.ahat.heapdump.AhatInstance> predicate, java.util.function.Consumer<com.android.ahat.heapdump.AhatInstance> consumer) { throw new RuntimeException("Stub!"); }
public  java.util.List<com.android.ahat.heapdump.Site.ObjectsInfo> getObjectsInfos() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Size getTotalSize() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Site getParent() { throw new RuntimeException("Stub!"); }
public  java.lang.String getMethodName() { throw new RuntimeException("Stub!"); }
public  java.lang.String getSignature() { throw new RuntimeException("Stub!"); }
public  java.lang.String getFilename() { throw new RuntimeException("Stub!"); }
public  int getLineNumber() { throw new RuntimeException("Stub!"); }
public  long getId() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Site findSite(long id) { throw new RuntimeException("Stub!"); }
public  java.util.List<com.android.ahat.heapdump.Site> getChildren() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Site getBaseline() { throw new RuntimeException("Stub!"); }
public  boolean isPlaceHolder() { throw new RuntimeException("Stub!"); }
}
