/*
* Copyright (C) 2016 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.heapdump;
public abstract class Value
{
public  Value() { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.Value pack(com.android.ahat.heapdump.AhatInstance value) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.Value pack(boolean value) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.Value pack(char value) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.Value pack(float value) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.Value pack(double value) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.Value pack(byte value) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.Value pack(short value) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.Value pack(int value) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.Value pack(long value) { throw new RuntimeException("Stub!"); }
public static  com.android.ahat.heapdump.Type getType(com.android.ahat.heapdump.Value value) { throw new RuntimeException("Stub!"); }
public  boolean isAhatInstance() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatInstance asAhatInstance() { throw new RuntimeException("Stub!"); }
public  boolean isInteger() { throw new RuntimeException("Stub!"); }
public  java.lang.Integer asInteger() { throw new RuntimeException("Stub!"); }
public  boolean isLong() { throw new RuntimeException("Stub!"); }
public  java.lang.Long asLong() { throw new RuntimeException("Stub!"); }
public  java.lang.Byte asByte() { throw new RuntimeException("Stub!"); }
public  java.lang.Character asChar() { throw new RuntimeException("Stub!"); }
public abstract  java.lang.String toString();
public static  com.android.ahat.heapdump.Value getBaseline(com.android.ahat.heapdump.Value value) { throw new RuntimeException("Stub!"); }
public abstract  boolean equals(java.lang.Object other);
}
