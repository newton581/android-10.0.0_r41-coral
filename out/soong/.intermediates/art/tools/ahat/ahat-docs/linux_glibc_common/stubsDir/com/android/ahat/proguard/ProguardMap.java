/*
* Copyright (C) 2016 Google Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.proguard;
public class ProguardMap
{
public static class Frame
{
Frame() { throw new RuntimeException("Stub!"); }
public final java.lang.String filename;
public final int line;
public final java.lang.String method;
public final java.lang.String signature;
}
public  ProguardMap() { throw new RuntimeException("Stub!"); }
public  void readFromFile(java.io.File mapFile) throws java.io.FileNotFoundException, java.io.IOException, java.text.ParseException { throw new RuntimeException("Stub!"); }
public  void readFromReader(java.io.Reader mapReader) throws java.io.IOException, java.text.ParseException { throw new RuntimeException("Stub!"); }
public  java.lang.String getClassName(java.lang.String obfuscatedClassName) { throw new RuntimeException("Stub!"); }
public  java.lang.String getFieldName(java.lang.String clearClass, java.lang.String obfuscatedField) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.proguard.ProguardMap.Frame getFrame(java.lang.String clearClassName, java.lang.String obfuscatedMethodName, java.lang.String obfuscatedSignature, java.lang.String obfuscatedFilename, int obfuscatedLine) { throw new RuntimeException("Stub!"); }
}
