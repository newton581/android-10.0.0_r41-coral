/*
* Copyright (C) 2018 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.progress;
public class NullProgress
  implements com.android.ahat.progress.Progress
{
public  NullProgress() { throw new RuntimeException("Stub!"); }
public  void start(java.lang.String description, long duration) { throw new RuntimeException("Stub!"); }
public  void advance() { throw new RuntimeException("Stub!"); }
public  void advance(long n) { throw new RuntimeException("Stub!"); }
public  void update(long current) { throw new RuntimeException("Stub!"); }
public  void done() { throw new RuntimeException("Stub!"); }
}
