/*
* Copyright (C) 2016 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.heapdump;
public class AhatSnapshot
  implements com.android.ahat.heapdump.Diffable<com.android.ahat.heapdump.AhatSnapshot>
{
AhatSnapshot() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatInstance findInstance(long id) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatClassObj findClassObj(long id) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatHeap getHeap(java.lang.String name) { throw new RuntimeException("Stub!"); }
public  java.util.List<com.android.ahat.heapdump.AhatHeap> getHeaps() { throw new RuntimeException("Stub!"); }
public  java.util.List<com.android.ahat.heapdump.AhatInstance> getRooted() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Site getRootSite() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Site getSite(long id) { throw new RuntimeException("Stub!"); }
public  boolean isDiffed() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatSnapshot getBaseline() { throw new RuntimeException("Stub!"); }
public  boolean isPlaceHolder() { throw new RuntimeException("Stub!"); }
}
