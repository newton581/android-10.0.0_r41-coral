/*
* Copyright (C) 2016 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.heapdump;
public class AhatClassInstance
  extends com.android.ahat.heapdump.AhatInstance
{
AhatClassInstance() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Value getField(java.lang.String fieldName) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatInstance getRefField(java.lang.String fieldName) { throw new RuntimeException("Stub!"); }
public  java.lang.Iterable<com.android.ahat.heapdump.FieldValue> getInstanceFields() { throw new RuntimeException("Stub!"); }
public  java.lang.String asString(int maxChars) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatInstance getReferent() { throw new RuntimeException("Stub!"); }
public  java.lang.String getDexCacheLocation(int maxChars) { throw new RuntimeException("Stub!"); }
public  java.lang.String getBinderProxyInterfaceName() { throw new RuntimeException("Stub!"); }
public  java.lang.String getBinderTokenDescriptor() { throw new RuntimeException("Stub!"); }
public  java.lang.String getBinderStubInterfaceName() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatInstance getAssociatedBitmapInstance() { throw new RuntimeException("Stub!"); }
public  boolean isClassInstance() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatClassInstance asClassInstance() { throw new RuntimeException("Stub!"); }
public  java.lang.String toString() { throw new RuntimeException("Stub!"); }
public  java.awt.image.BufferedImage asBitmap() { throw new RuntimeException("Stub!"); }
}
