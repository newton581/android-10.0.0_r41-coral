/*
* Copyright (C) 2016 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.heapdump;
public abstract class AhatInstance
  implements com.android.ahat.heapdump.Diffable<com.android.ahat.heapdump.AhatInstance>
{
AhatInstance() { throw new RuntimeException("Stub!"); }
public  long getId() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Size getSize() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Size getRetainedSize(com.android.ahat.heapdump.AhatHeap heap) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Size getTotalRetainedSize() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Reachability getReachability() { throw new RuntimeException("Stub!"); }
public  boolean isStronglyReachable() { throw new RuntimeException("Stub!"); }
@java.lang.Deprecated()
public  boolean isWeaklyReachable() { throw new RuntimeException("Stub!"); }
public  boolean isUnreachable() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatHeap getHeap() { throw new RuntimeException("Stub!"); }
public  boolean isRoot() { throw new RuntimeException("Stub!"); }
public  java.util.Collection<com.android.ahat.heapdump.RootType> getRootTypes() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatInstance getImmediateDominator() { throw new RuntimeException("Stub!"); }
public  java.util.List<com.android.ahat.heapdump.AhatInstance> getDominated() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Site getSite() { throw new RuntimeException("Stub!"); }
public  boolean isClassObj() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatClassObj asClassObj() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatClassObj getClassObj() { throw new RuntimeException("Stub!"); }
public  java.lang.String getClassName() { throw new RuntimeException("Stub!"); }
public  boolean isInstanceOfClass(java.lang.String className) { throw new RuntimeException("Stub!"); }
public  boolean isArrayInstance() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatArrayInstance asArrayInstance() { throw new RuntimeException("Stub!"); }
public  boolean isClassInstance() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatClassInstance asClassInstance() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatInstance getReferent() { throw new RuntimeException("Stub!"); }
public  java.util.List<com.android.ahat.heapdump.AhatInstance> getReverseReferences() { throw new RuntimeException("Stub!"); }
@java.lang.Deprecated()
public  java.util.List<com.android.ahat.heapdump.AhatInstance> getHardReverseReferences() { throw new RuntimeException("Stub!"); }
@java.lang.Deprecated()
public  java.util.List<com.android.ahat.heapdump.AhatInstance> getSoftReverseReferences() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.Value getField(java.lang.String fieldName) { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatInstance getRefField(java.lang.String fieldName) { throw new RuntimeException("Stub!"); }
public  java.lang.String getDexCacheLocation(int maxChars) { throw new RuntimeException("Stub!"); }
public  java.lang.String getBinderProxyInterfaceName() { throw new RuntimeException("Stub!"); }
public  java.lang.String getBinderTokenDescriptor() { throw new RuntimeException("Stub!"); }
public  java.lang.String getBinderStubInterfaceName() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatInstance getAssociatedBitmapInstance() { throw new RuntimeException("Stub!"); }
public  com.android.ahat.heapdump.AhatClassObj getAssociatedClassForOverhead() { throw new RuntimeException("Stub!"); }
public  java.lang.String asString(int maxChars) { throw new RuntimeException("Stub!"); }
public  java.lang.String asString() { throw new RuntimeException("Stub!"); }
public  java.awt.image.BufferedImage asBitmap() { throw new RuntimeException("Stub!"); }
public  java.util.List<com.android.ahat.heapdump.PathElement> getPathFromGcRoot() { throw new RuntimeException("Stub!"); }
public abstract  java.lang.String toString();
public  com.android.ahat.heapdump.AhatInstance getBaseline() { throw new RuntimeException("Stub!"); }
public  boolean isPlaceHolder() { throw new RuntimeException("Stub!"); }
}
