/*
* Copyright (C) 2015 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.heapdump;
public class Sort
{
public  Sort() { throw new RuntimeException("Stub!"); }
public static <T> java.util.Comparator<T> withPriority(java.util.Comparator<T>... comparators) { throw new RuntimeException("Stub!"); }
public static  java.util.Comparator<com.android.ahat.heapdump.AhatInstance> defaultInstanceCompare(com.android.ahat.heapdump.AhatSnapshot snapshot) { throw new RuntimeException("Stub!"); }
public static  java.util.Comparator<com.android.ahat.heapdump.Site> defaultSiteCompare(com.android.ahat.heapdump.AhatSnapshot snapshot) { throw new RuntimeException("Stub!"); }
public static final java.util.Comparator<com.android.ahat.heapdump.FieldValue> FIELD_VALUE_BY_NAME;
public static final java.util.Comparator<com.android.ahat.heapdump.FieldValue> FIELD_VALUE_BY_TYPE;
public static final java.util.Comparator<com.android.ahat.heapdump.AhatInstance> INSTANCE_BY_TOTAL_RETAINED_SIZE;
public static final java.util.Comparator<com.android.ahat.heapdump.Site.ObjectsInfo> OBJECTS_INFO_BY_CLASS_NAME;
public static final java.util.Comparator<com.android.ahat.heapdump.Site.ObjectsInfo> OBJECTS_INFO_BY_HEAP_NAME;
public static final java.util.Comparator<com.android.ahat.heapdump.Site.ObjectsInfo> OBJECTS_INFO_BY_SIZE;
public static final java.util.Comparator<com.android.ahat.heapdump.Site> SITE_BY_TOTAL_SIZE;
public static final java.util.Comparator<com.android.ahat.heapdump.Size> SIZE_BY_SIZE;
static { FIELD_VALUE_BY_NAME = null; FIELD_VALUE_BY_TYPE = null; INSTANCE_BY_TOTAL_RETAINED_SIZE = null; OBJECTS_INFO_BY_CLASS_NAME = null; OBJECTS_INFO_BY_HEAP_NAME = null; OBJECTS_INFO_BY_SIZE = null; SITE_BY_TOTAL_SIZE = null; SIZE_BY_SIZE = null; }
}
