/*
* Copyright (C) 2017 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.android.ahat.dominators;
@java.lang.Deprecated()
public class DominatorsComputation
{
public static interface Node
{
public abstract  void setDominatorsComputationState(java.lang.Object state);
public abstract  java.lang.Object getDominatorsComputationState();
public abstract  java.lang.Iterable<? extends com.android.ahat.dominators.DominatorsComputation.Node> getReferencesForDominators();
public abstract  void setDominator(com.android.ahat.dominators.DominatorsComputation.Node dominator);
}
DominatorsComputation() { throw new RuntimeException("Stub!"); }
public static  void computeDominators(com.android.ahat.dominators.DominatorsComputation.Node root) { throw new RuntimeException("Stub!"); }
public static  void computeDominators(com.android.ahat.dominators.DominatorsComputation.Node root, com.android.ahat.progress.Progress progress, long numNodes) { throw new RuntimeException("Stub!"); }
}
