/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.android.providers.media;

public final class R {
  public static final class bool {
    /**
     * True if the ringtone picker should show the ok/cancel buttons. If it is not shown, the
     * ringtone will be automatically selected when the picker is closed.
     * True if the ringtone picker should show the ok/cancel buttons. If it is not shown, the
     * ringtone will be automatically selected when the picker is closed.
     */
    public static final int config_showOkCancelButtons=0x7f010000;
  }
  public static final class dimen {
    /**
     * maximum size for album art. the real size of the albumart will be divided by two
     * until both width and height are below this value.
     * maximum size for album art. the real size of the albumart will be divided by two
     * until both width and height are below this value.
     */
    public static final int maximum_thumb_size=0x7f020000;
  }
  public static final class drawable {
    public static final int ic_add=0x7f030000;
    public static final int ic_add_padded=0x7f030001;
    public static final int ic_search_category_music_album=0x7f030002;
    public static final int ic_search_category_music_artist=0x7f030003;
    public static final int ic_search_category_music_song=0x7f030004;
  }
  public static final class id {
    public static final int add_new_sound_text=0x7f040000;
    public static final int checked_text_view=0x7f040001;
    public static final int work_icon=0x7f040002;
  }
  public static final class layout {
    public static final int add_new_sound_item=0x7f050000;
    public static final int radio_with_work_badge=0x7f050001;
  }
  public static final class mipmap {
    public static final int ic_launcher_gallery=0x7f060000;
  }
  public static final class raw {
    public static final int default_alarm_alert=0x7f070000;
    public static final int default_notification_sound=0x7f070001;
    public static final int default_ringtone=0x7f070002;
  }
  public static final class string {
    /**
     * Text for the RingtonePicker item that allows adding a new alarm.
     */
    public static final int add_alarm_text=0x7f080000;
    /**
     * Text for the RingtonePicker item that allows adding a new notification.
     */
    public static final int add_notification_text=0x7f080001;
    /**
     * Text for the RingtonePicker item that allows adding a new ringtone.
     */
    public static final int add_ringtone_text=0x7f080002;
    /**
     * Choice in the alarm sound picker.  If chosen, the default alarm sound will be used.
     */
    public static final int alarm_sound_default=0x7f080003;
    /**
     * Label to show to user for this package.
     */
    public static final int app_label=0x7f080004;
    /**
     * Description line for music artists in the search/suggestion results
     */
    public static final int artist_label=0x7f080005;
    /**
     * Text for the RingtonePicker item ContextMenu that allows deleting a custom ringtone.
     */
    public static final int delete_ringtone_text=0x7f080006;
    /**
     * Title for the dialog button to allow a permission grant. [CHAR LIMIT=15]
     */
    public static final int grant_dialog_button_allow=0x7f080007;
    /**
     * Title for the dialog button to deny a permission grant. [CHAR LIMIT=15]
     */
    public static final int grant_dialog_button_deny=0x7f080008;
    /**
     * Choice in the notification sound picker.  If chosen, the default notification sound will be
     * used.
     */
    public static final int notification_sound_default=0x7f080009;
    /**
     * Prompt asking if user will allow permission to the audio item shown below this string. [CHAR LIMIT=128]
     */
    public static final int permission_audio=0x7f08000a;
    /**
     * Prompt asking if user will allow permission to the image item shown below this string. [CHAR LIMIT=128]
     */
    public static final int permission_images=0x7f08000b;
    /**
     * Message telling users that the app must request permission before working with a media item. [CHAR LIMIT=128]
     */
    public static final int permission_required=0x7f08000c;
    /**
     * Button text that users can click on to request permission before working with a media item. [CHAR LIMIT=32]
     */
    public static final int permission_required_action=0x7f08000d;
    /**
     * Prompt asking if user will allow permission to the video item shown below this string. [CHAR LIMIT=128]
     */
    public static final int permission_video=0x7f08000e;
    /**
     * Choice in the ringtone picker.  If chosen, the default ringtone will be used.
     */
    public static final int ringtone_default=0x7f08000f;
    /**
     * Title for documents backend that offers audio. [CHAR LIMIT=24]
     */
    public static final int root_audio=0x7f080010;
    /**
     * Title for documents backend that offers images. [CHAR LIMIT=24]
     */
    public static final int root_images=0x7f080011;
    /**
     * Title for documents backend that offers videos. [CHAR LIMIT=24]
     */
    public static final int root_videos=0x7f080012;
    public static final int sound_name_alarm_01=0x7f080013;
    public static final int sound_name_alarm_02=0x7f080014;
    public static final int sound_name_alarm_03=0x7f080015;
    public static final int sound_name_alarm_04=0x7f080016;
    public static final int sound_name_alarm_05=0x7f080017;
    public static final int sound_name_alarm_06=0x7f080018;
    public static final int sound_name_alarm_07=0x7f080019;
    public static final int sound_name_alarm_08=0x7f08001a;
    public static final int sound_name_alarm_09=0x7f08001b;
    public static final int sound_name_alarm_10=0x7f08001c;
    public static final int sound_name_alarm_11=0x7f08001d;
    public static final int sound_name_alarm_12=0x7f08001e;
    public static final int sound_name_alarm_13=0x7f08001f;
    public static final int sound_name_alarm_14=0x7f080020;
    public static final int sound_name_alarm_15=0x7f080021;
    /**
     * Name of alarm in the alarm sound picker: theme:motion, style:ambient tempo:flowing
     */
    public static final int sound_name_awaken=0x7f080022;
    /**
     * Name of alarm in the alarm sound picker: theme:motion, style:soft pure tones
     * tempo:initially downtempo
     */
    public static final int sound_name_bounce=0x7f080023;
    /**
     * Name of alarm in the alarm sound picker: theme:motion, style:traditional tempo:mid
     */
    public static final int sound_name_drip=0x7f080024;
    /**
     * Name of alarm in the alarm sound picker: theme:motion, style:rhythmic tempo:up-tempo
     */
    public static final int sound_name_gallop=0x7f080025;
    public static final int sound_name_notification_01=0x7f080026;
    public static final int sound_name_notification_02=0x7f080027;
    public static final int sound_name_notification_03=0x7f080028;
    public static final int sound_name_notification_04=0x7f080029;
    public static final int sound_name_notification_05=0x7f08002a;
    public static final int sound_name_notification_06=0x7f08002b;
    public static final int sound_name_notification_07=0x7f08002c;
    public static final int sound_name_notification_08=0x7f08002d;
    public static final int sound_name_notification_09=0x7f08002e;
    public static final int sound_name_notification_10=0x7f08002f;
    public static final int sound_name_notification_11=0x7f080030;
    public static final int sound_name_notification_12=0x7f080031;
    public static final int sound_name_notification_13=0x7f080032;
    public static final int sound_name_notification_14=0x7f080033;
    public static final int sound_name_notification_15=0x7f080034;
    /**
     * Name of alarm in the alarm sound picker: theme:motion, style:traditional tempo:mid
     */
    public static final int sound_name_nudge=0x7f080035;
    /**
     * Name of alarm in the alarm sound picker: theme:motion, style:traditional tempo:mid
     */
    public static final int sound_name_orbit=0x7f080036;
    public static final int sound_name_ringtone_01=0x7f080037;
    public static final int sound_name_ringtone_02=0x7f080038;
    public static final int sound_name_ringtone_03=0x7f080039;
    public static final int sound_name_ringtone_04=0x7f08003a;
    public static final int sound_name_ringtone_05=0x7f08003b;
    public static final int sound_name_ringtone_06=0x7f08003c;
    public static final int sound_name_ringtone_07=0x7f08003d;
    public static final int sound_name_ringtone_08=0x7f08003e;
    public static final int sound_name_ringtone_09=0x7f08003f;
    public static final int sound_name_ringtone_10=0x7f080040;
    public static final int sound_name_ringtone_11=0x7f080041;
    public static final int sound_name_ringtone_12=0x7f080042;
    public static final int sound_name_ringtone_13=0x7f080043;
    public static final int sound_name_ringtone_14=0x7f080044;
    public static final int sound_name_ringtone_15=0x7f080045;
    /**
     * Name of alarm in the alarm sound picker: theme:motion, style:digital tempo:mid
     */
    public static final int sound_name_rise=0x7f080046;
    /**
     * Name of alarm in the alarm sound picker: theme:motion, style:musical tempo:swaying
     */
    public static final int sound_name_sway=0x7f080047;
    /**
     * Name of alarm in the alarm sound picker: theme:motion, style:sound design tempo:mid
     */
    public static final int sound_name_wag=0x7f080048;
    /**
     * Label to show client applications a short description of storage location
     */
    public static final int storage_description=0x7f080049;
    /**
     * Label to show to user for all apps using this UID.
     */
    public static final int uid_label=0x7f08004a;
    /**
     * Text for the Toast displayed when adding a custom ringtone fails.
     */
    public static final int unable_to_add_ringtone=0x7f08004b;
    /**
     * Text for the Toast displayed when deleting a custom ringtone fails.
     */
    public static final int unable_to_delete_ringtone=0x7f08004c;
  }
  public static final class style {
    public static final int PickerDialogTheme=0x7f090000;
  }
}