/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.android.certinstaller;

public final class R {
  public static final class array {
    /**
     * Usage type for a credential that the user is installing. The label will restrict the
     * type of use for that credential. [CHAR LIMIT=40]
     */
    public static final int credential_usage=0x7f010000;
  }
  public static final class bool {
    public static final int config_auto_cert_approval=0x7f020000;
  }
  public static final class color {
    public static final int red=0x7f030000;
  }
  public static final class dimen {
    public static final int dialog_normal_text_size=0x7f040000;
  }
  public static final class drawable {
    public static final int signal_wifi_4_bar_lock_black_24dp=0x7f050000;
  }
  public static final class id {
    public static final int credential_capabilities_warning=0x7f060000;
    public static final int credential_info=0x7f060001;
    public static final int credential_installed_content=0x7f060002;
    public static final int credential_name=0x7f060003;
    public static final int credential_password=0x7f060004;
    public static final int credential_usage=0x7f060005;
    public static final int credential_usage_group=0x7f060006;
    public static final int error=0x7f060007;
    public static final int wifi_info=0x7f060008;
  }
  public static final class layout {
    public static final int credentials_installed_dialog=0x7f070000;
    public static final int name_credential_dialog=0x7f070001;
    public static final int password_dialog=0x7f070002;
    public static final int wifi_main_dialog=0x7f070003;
  }
  public static final class string {
    public static final int action_missing_private_key=0x7f080000;
    public static final int action_missing_user_cert=0x7f080001;
    public static final int app_name=0x7f080002;
    /**
     * toast message
     */
    public static final int cert_is_added=0x7f080003;
    /**
     * toast message
     */
    public static final int cert_missing_error=0x7f080004;
    public static final int cert_not_saved=0x7f080005;
    /**
     * toast message
     */
    public static final int cert_read_error=0x7f080006;
    /**
     * toast message
     */
    public static final int cert_too_large_error=0x7f080007;
    /**
     * Explain that the issuer of the CA certificate that is about to be installed can inspect
     * all of the traffic of the device.
     */
    public static final int certificate_capabilities_warning=0x7f080008;
    public static final int config_system_install_component=0x7f080009;
    public static final int credential_info=0x7f08000a;
    /**
     * Description for the credential name input box
     */
    public static final int credential_name=0x7f08000b;
    /**
     * Description for the credential password input box
     */
    public static final int credential_password=0x7f08000c;
    /**
     * Label for spinner that shows the possible usage for a credential. Shown when user is
     * installing a credential [CHAR LIMIT=40]
     */
    public static final int credential_usage_label=0x7f08000d;
    public static final int done_label=0x7f08000e;
    /**
     * message in progress bar when waiting for extracting certs from a pkcs12 package
     */
    public static final int extracting_pkcs12=0x7f08000f;
    public static final int install_done=0x7f080010;
    public static final int install_done_title=0x7f080011;
    public static final int invalid_cert=0x7f080012;
    /**
     * Item found in the PKCS12 keystore being investigated [CHAR LIMIT=NONE]
     */
    public static final int n_cacrts=0x7f080013;
    public static final int name_char_error=0x7f080014;
    /**
     * Title of dialog to name a credential
     */
    public static final int name_credential_dialog_title=0x7f080015;
    public static final int name_empty_error=0x7f080016;
    public static final int no_cert_to_saved=0x7f080017;
    /**
     * Item found in the PKCS12 keystore being investigated [CHAR LIMIT=NONE]
     */
    public static final int one_cacrt=0x7f080018;
    /**
     * Item found in the PKCS12 keystore being investigated [CHAR LIMIT=NONE]
     */
    public static final int one_usercrt=0x7f080019;
    /**
     * Item found in the PKCS12 keystore being investigated [CHAR LIMIT=NONE]
     */
    public static final int one_userkey=0x7f08001a;
    /**
     * Message displayed when a user other than the owner on a multi-user system tries to
     * install a certificate into the certificate store. [CHAR LIMIT=NONE]
     */
    public static final int only_primary_user_allowed=0x7f08001b;
    public static final int p12_description=0x7f08001c;
    public static final int password_empty_error=0x7f08001d;
    public static final int password_error=0x7f08001e;
    /**
     * Title of the file picker screen
     */
    public static final int pick_file_title=0x7f08001f;
    /**
     * Title of dialog to enter password for pkcs12 file
     */
    public static final int pkcs12_file_password_dialog_title=0x7f080020;
    /**
     * Title of dialog to enter password for pkcs12
     */
    public static final int pkcs12_password_dialog_title=0x7f080021;
    public static final int unable_to_save_cert=0x7f080022;
    /**
     * Type of key found in the PKCS12 keystore being investigated [CHAR LIMIT=NONE]
     */
    public static final int userkey_type=0x7f080023;
    public static final int wifi_cancel_label=0x7f080024;
    public static final int wifi_config_text=0x7f080025;
    public static final int wifi_detail_label=0x7f080026;
    public static final int wifi_detail_title=0x7f080027;
    public static final int wifi_dismiss_label=0x7f080028;
    public static final int wifi_install_label=0x7f080029;
    public static final int wifi_installer_detail=0x7f08002a;
    public static final int wifi_installer_download_error=0x7f08002b;
    public static final int wifi_installer_fail=0x7f08002c;
    public static final int wifi_installer_fail_no_wifi=0x7f08002d;
    public static final int wifi_installer_fail_no_wifi_title=0x7f08002e;
    public static final int wifi_installer_fail_title=0x7f08002f;
    /**
     * Toast message to be displayed when installing Hotspot 2.0 credentials [CHAR LIMIT=40]
     */
    public static final int wifi_installing_label=0x7f080030;
    public static final int wifi_no_config=0x7f080031;
    public static final int wifi_sim_config_text=0x7f080032;
    public static final int wifi_title=0x7f080033;
    public static final int wifi_tls_config_text=0x7f080034;
    public static final int wifi_trust_config_text=0x7f080035;
    public static final int wifi_ttls_config_text=0x7f080036;
  }
  public static final class style {
    public static final int Transparent=0x7f090000;
    public static final int dialog_item=0x7f090001;
  }
  public static final class xml {
    public static final int pick_file_pref=0x7f0a0000;
  }
}