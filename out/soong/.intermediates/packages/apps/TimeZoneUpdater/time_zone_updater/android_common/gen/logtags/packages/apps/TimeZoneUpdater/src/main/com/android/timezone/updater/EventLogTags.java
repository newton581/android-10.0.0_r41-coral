/* This file is auto-generated.  DO NOT MODIFY.
 * Source file: packages/apps/TimeZoneUpdater/src/main/com/android/timezone/updater/EventLogTags.logtags
 */

package com.android.timezone.updater;

/**
 * @hide
 */
public class EventLogTags {
  private EventLogTags() { }  // don't instantiate

  /** 51690 timezone_check_trigger_received (token_bytes|3) */
  public static final int TIMEZONE_CHECK_TRIGGER_RECEIVED = 51690;

  /** 51691 timezone_check_read_from_data_app (token_bytes|3) */
  public static final int TIMEZONE_CHECK_READ_FROM_DATA_APP = 51691;

  /** 51692 timezone_check_request_uninstall (token_bytes|3) */
  public static final int TIMEZONE_CHECK_REQUEST_UNINSTALL = 51692;

  /** 51693 timezone_check_request_install (token_bytes|3) */
  public static final int TIMEZONE_CHECK_REQUEST_INSTALL = 51693;

  /** 51694 timezone_check_request_nothing (token_bytes|3), (success|1) */
  public static final int TIMEZONE_CHECK_REQUEST_NOTHING = 51694;

  public static void writeTimezoneCheckTriggerReceived(String tokenBytes) {
    android.util.EventLog.writeEvent(TIMEZONE_CHECK_TRIGGER_RECEIVED, tokenBytes);
  }

  public static void writeTimezoneCheckReadFromDataApp(String tokenBytes) {
    android.util.EventLog.writeEvent(TIMEZONE_CHECK_READ_FROM_DATA_APP, tokenBytes);
  }

  public static void writeTimezoneCheckRequestUninstall(String tokenBytes) {
    android.util.EventLog.writeEvent(TIMEZONE_CHECK_REQUEST_UNINSTALL, tokenBytes);
  }

  public static void writeTimezoneCheckRequestInstall(String tokenBytes) {
    android.util.EventLog.writeEvent(TIMEZONE_CHECK_REQUEST_INSTALL, tokenBytes);
  }

  public static void writeTimezoneCheckRequestNothing(String tokenBytes, int success) {
    android.util.EventLog.writeEvent(TIMEZONE_CHECK_REQUEST_NOTHING, tokenBytes, success);
  }
}
