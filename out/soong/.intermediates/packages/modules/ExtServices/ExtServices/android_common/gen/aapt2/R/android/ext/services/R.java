/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package android.ext.services;

public final class R {
  public static final class array {
    public static final int autofill_field_classification_available_algorithms=0x7f010000;
  }
  public static final class drawable {
    public static final int ic_action_open=0x7f020000;
    public static final int ic_menu_copy_material=0x7f020001;
  }
  public static final class string {
    public static final int app_name=0x7f030000;
    public static final int autofill_field_classification_default_algorithm=0x7f030001;
    /**
     * Toast to display when text is copied to the device clipboard [CHAR LIMIT=64]
     */
    public static final int code_copied_to_clipboard=0x7f030002;
    /**
     * Action chip to copy a one time code to the user's clipboard [CHAR LIMIT=NONE]
     */
    public static final int copy_code_desc=0x7f030003;
    public static final int notification_assistant=0x7f030004;
  }
}