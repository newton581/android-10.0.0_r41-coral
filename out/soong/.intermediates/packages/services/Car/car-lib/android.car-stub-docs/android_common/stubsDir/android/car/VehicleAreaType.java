/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car;


/**
 * Represents vehicle area such as window, door, seat, etc.
 * See also {@link VehicleAreaDoor}, {@link VehicleAreaSeat},
 * {@link VehicleAreaWindow},
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VehicleAreaType {

VehicleAreaType() { throw new RuntimeException("Stub!"); }

/** Area type is Door */

public static final int VEHICLE_AREA_TYPE_DOOR = 4; // 0x4

/** Used for global properties */

public static final int VEHICLE_AREA_TYPE_GLOBAL = 0; // 0x0

/** Area type is Mirror */

public static final int VEHICLE_AREA_TYPE_MIRROR = 5; // 0x5

/** Area type is Seat */

public static final int VEHICLE_AREA_TYPE_SEAT = 3; // 0x3

/** Area type is Wheel */

public static final int VEHICLE_AREA_TYPE_WHEEL = 6; // 0x6

/** Area type is Window */

public static final int VEHICLE_AREA_TYPE_WINDOW = 2; // 0x2
}

