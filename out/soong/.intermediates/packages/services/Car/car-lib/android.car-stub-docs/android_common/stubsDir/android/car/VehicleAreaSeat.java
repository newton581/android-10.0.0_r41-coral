/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car;

import android.car.hardware.CarPropertyValue;

/**
 * VehicleAreaSeat is an abstraction for a seat in a car. Some car APIs like
 * {@link CarPropertyValue} may provide control per seat and
 * values defined here should be used to distinguish different seats.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VehicleAreaSeat {

VehicleAreaSeat() { throw new RuntimeException("Stub!"); }

/** Row 1 center seat*/

public static final int SEAT_ROW_1_CENTER = 2; // 0x2

/** Row 1 left side seat*/

public static final int SEAT_ROW_1_LEFT = 1; // 0x1

/** Row 1 right side seat*/

public static final int SEAT_ROW_1_RIGHT = 4; // 0x4

/** Row 2 center seat*/

public static final int SEAT_ROW_2_CENTER = 32; // 0x20

/** Row 2 left side seat*/

public static final int SEAT_ROW_2_LEFT = 16; // 0x10

/** Row 2 right side seat*/

public static final int SEAT_ROW_2_RIGHT = 64; // 0x40

/** Row 3 center seat*/

public static final int SEAT_ROW_3_CENTER = 512; // 0x200

/** Row 3 left side seat*/

public static final int SEAT_ROW_3_LEFT = 256; // 0x100

/** Row 3 right side seat*/

public static final int SEAT_ROW_3_RIGHT = 1024; // 0x400

/** List of vehicle's seats. */

public static final int SEAT_UNKNOWN = 0; // 0x0
}

