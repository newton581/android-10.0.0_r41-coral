/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car;


/**
 * CarAppFocusManager allows applications to set and listen for the current application focus
 * like active navigation or active voice command. Usually only one instance of such application
 * should run in the system, and other app setting the flag for the matching app should
 * lead into other app to stop.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarAppFocusManager {

/**
 * @hide
 */

CarAppFocusManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Register listener to monitor app focus change.
 * @param listener
 * @param appType Application type to get notification for.

 * Value is {@link android.car.CarAppFocusManager#APP_FOCUS_TYPE_NAVIGATION}
 */

public void addFocusListener(android.car.CarAppFocusManager.OnAppFocusChangedListener listener, int appType) { throw new RuntimeException("Stub!"); }

/**
 * Unregister listener for application type and stop listening focus change events.
 * @param listener
 * @param appType

 * Value is {@link android.car.CarAppFocusManager#APP_FOCUS_TYPE_NAVIGATION}
 */

public void removeFocusListener(android.car.CarAppFocusManager.OnAppFocusChangedListener listener, int appType) { throw new RuntimeException("Stub!"); }

/**
 * Unregister listener and stop listening focus change events.
 * @param listener
 */

public void removeFocusListener(android.car.CarAppFocusManager.OnAppFocusChangedListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Checks if listener is associated with active a focus
 * @param callback
 * @param appType

 * Value is {@link android.car.CarAppFocusManager#APP_FOCUS_TYPE_NAVIGATION}
 */

public boolean isOwningFocus(android.car.CarAppFocusManager.OnAppFocusOwnershipCallback callback, int appType) { throw new RuntimeException("Stub!"); }

/**
 * Requests application focus.
 * By requesting this, the application is becoming owner of the focus, and will get
 * {@link OnAppFocusOwnershipCallback#onAppFocusOwnershipLost(int)}
 * if ownership is given to other app by calling this. Fore-ground app will have higher priority
 * and other app cannot set the same focus while owner is in fore-ground.
 * @param appType
 * @param ownershipCallback
 * @return {@link #APP_FOCUS_REQUEST_FAILED} or {@link #APP_FOCUS_REQUEST_SUCCEEDED}
 * Value is {@link android.car.CarAppFocusManager#APP_FOCUS_REQUEST_FAILED}, or {@link android.car.CarAppFocusManager#APP_FOCUS_REQUEST_SUCCEEDED}
 * @throws SecurityException If owner cannot be changed.
 */

public int requestAppFocus(int appType, android.car.CarAppFocusManager.OnAppFocusOwnershipCallback ownershipCallback) { throw new RuntimeException("Stub!"); }

/**
 * Abandon the given focus, i.e. mark it as inactive. This also involves releasing ownership
 * for the focus.
 * @param ownershipCallback
 * @param appType

 * Value is {@link android.car.CarAppFocusManager#APP_FOCUS_TYPE_NAVIGATION}
 */

public void abandonAppFocus(android.car.CarAppFocusManager.OnAppFocusOwnershipCallback ownershipCallback, int appType) { throw new RuntimeException("Stub!"); }

/**
 * Abandon all focuses, i.e. mark them as inactive. This also involves releasing ownership
 * for the focus.
 * @param ownershipCallback
 */

public void abandonAppFocus(android.car.CarAppFocusManager.OnAppFocusOwnershipCallback ownershipCallback) { throw new RuntimeException("Stub!"); }

/**
 * A failed focus change request.
 */

public static final int APP_FOCUS_REQUEST_FAILED = 0; // 0x0

/**
 * A successful focus change request.
 */

public static final int APP_FOCUS_REQUEST_SUCCEEDED = 1; // 0x1

/**
 * Represents navigation focus.
 */

public static final int APP_FOCUS_TYPE_NAVIGATION = 1; // 0x1

/**
 * Represents voice command focus.
 *
 * @deprecated use {@link android.service.voice.VoiceInteractionService} instead.
 */

@Deprecated public static final int APP_FOCUS_TYPE_VOICE_COMMAND = 2; // 0x2
/**
 * Listener to get notification for app getting information on application type status changes.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnAppFocusChangedListener {

/**
 * Application focus has changed. Note that {@link CarAppFocusManager} instance
 * causing the change will not get this notification.
 * @param appType
 * Value is {@link android.car.CarAppFocusManager#APP_FOCUS_TYPE_NAVIGATION}
 * @param active
 */

public void onAppFocusChanged(int appType, boolean active);
}

/**
 * Listener to get notification for app getting information on app type ownership loss.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnAppFocusOwnershipCallback {

/**
 * Lost ownership for the focus, which happens when other app has set the focus.
 * The app losing focus should stop the action associated with the focus.
 * For example, navigation app currently running active navigation should stop navigation
 * upon getting this for {@link CarAppFocusManager#APP_FOCUS_TYPE_NAVIGATION}.
 * @param appType

 * Value is {@link android.car.CarAppFocusManager#APP_FOCUS_TYPE_NAVIGATION}
 */

public void onAppFocusOwnershipLost(int appType);

/**
 * Granted ownership for the focus, which happens when app has requested the focus.
 * The app getting focus can start the action associated with the focus.
 * For example, navigation app can start navigation
 * upon getting this for {@link CarAppFocusManager#APP_FOCUS_TYPE_NAVIGATION}.
 * @param appType

 * Value is {@link android.car.CarAppFocusManager#APP_FOCUS_TYPE_NAVIGATION}
 */

public void onAppFocusOwnershipGranted(int appType);
}

}

