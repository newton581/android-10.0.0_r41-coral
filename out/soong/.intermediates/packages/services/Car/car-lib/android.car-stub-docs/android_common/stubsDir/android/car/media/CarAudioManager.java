/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.media;

import android.car.Car;

/**
 * APIs for handling audio in a car.
 *
 * In a car environment, we introduced the support to turn audio dynamic routing on /off by
 * setting the "audioUseDynamicRouting" attribute in config.xml
 *
 * When audio dynamic routing is enabled:
 * - Audio devices are grouped into zones
 * - There is at least one primary zone, and extra secondary zones such as RSE
 *   (Reat Seat Entertainment)
 * - Within each zone, audio devices are grouped into volume groups for volume control
 * - Audio is assigned to an audio device based on its AudioAttributes usage
 *
 * When audio dynamic routing is disabled:
 * - There is exactly one audio zone, which is the primary zone
 * - Each volume group represents a controllable STREAM_TYPE, same as AudioManager
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarAudioManager {

/** @hide */

CarAudioManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Registers a {@link CarVolumeCallback} to receive volume change callbacks
 * @param callback {@link CarVolumeCallback} instance, can not be null
 */

public void registerCarVolumeCallback(android.car.media.CarAudioManager.CarVolumeCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Unregisters a {@link CarVolumeCallback} from receiving volume change callbacks
 * @param callback {@link CarVolumeCallback} instance previously registered, can not be null
 */

public void unregisterCarVolumeCallback(android.car.media.CarAudioManager.CarVolumeCallback callback) { throw new RuntimeException("Stub!"); }
/**
 * Callback interface to receive volume change events in a car.
 * Extend this class and register it with {@link #registerCarVolumeCallback(CarVolumeCallback)}
 * and unregister it via {@link #unregisterCarVolumeCallback(CarVolumeCallback)}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class CarVolumeCallback {

public CarVolumeCallback() { throw new RuntimeException("Stub!"); }

/**
 * This is called whenever a group volume is changed.
 * The changed-to volume index is not included, the caller is encouraged to
 * get the current group volume index via CarAudioManager.
 *
 * @param zoneId Id of the audio zone that volume change happens
 * @param groupId Id of the volume group that volume is changed
 * @param flags see {@link android.media.AudioManager} for flag definitions
 */

public void onGroupVolumeChanged(int zoneId, int groupId, int flags) { throw new RuntimeException("Stub!"); }

/**
 * This is called whenever the master mute state is changed.
 * The changed-to master mute state is not included, the caller is encouraged to
 * get the current master mute state via AudioManager.
 *
 * @param zoneId Id of the audio zone that master mute state change happens
 * @param flags see {@link android.media.AudioManager} for flag definitions
 */

public void onMasterMuteChanged(int zoneId, int flags) { throw new RuntimeException("Stub!"); }
}

}

