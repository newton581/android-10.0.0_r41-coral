/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.hardware;


/**
 * A CarSensorEvent object corresponds to a single sensor event coming from the car. The sensor
 * data is stored in a sensor-type specific format in the object's float and byte arrays.
 *
 * To aid unmarshalling the object's data arrays, this class provides static nested classes and
 * conversion methods, for example {@link EnvironmentData} and {@link #getEnvironmentData}. The
 * conversion methods each have an optional data parameter which, if not null, will be used and
 * returned. This parameter should be used to avoid unnecessary object churn whenever possible.
 * Additionally, calling a conversion method on a CarSensorEvent object with an inappropriate type
 * will result in an {@code UnsupportedOperationException} being thrown.
 *
 * @deprecated consider using {@link android.car.hardware.property.CarPropertyEvent} instead.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class CarSensorEvent implements android.os.Parcelable {

/** @hide */

CarSensorEvent(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

@Deprecated
public int describeContents() { throw new RuntimeException("Stub!"); }

@Deprecated
public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @hide */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

@Deprecated public static final android.os.Parcelable.Creator<android.car.hardware.CarSensorEvent> CREATOR;
static { CREATOR = null; }

/**
 * This is for transmission without specific gear number for moving forward like CVT. It tells
 * that car is in a transmission state to move it forward.
 */

@Deprecated public static final int GEAR_DRIVE = 8; // 0x8

/** Gear number 8. */

@Deprecated public static final int GEAR_EIGHTH = 2048; // 0x800

/** Gear number 5. */

@Deprecated public static final int GEAR_FIFTH = 256; // 0x100

/**
 * intValues[0] from 1 to 99 represents transmission gear number for moving forward.
 * GEAR_FIRST is for gear number 1.
 */

@Deprecated public static final int GEAR_FIRST = 16; // 0x10

/** Gear number 4. */

@Deprecated public static final int GEAR_FOURTH = 128; // 0x80

/**
 *  GEAR_* represents meaning of intValues[0] for {@link CarSensorManager#SENSOR_TYPE_GEAR}
 *  sensor type.
 *  GEAR_NEUTRAL means transmission gear is in neutral state, and the car may be moving.
 */

@Deprecated public static final int GEAR_NEUTRAL = 1; // 0x1

/** Gear number 9. */

@Deprecated public static final int GEAR_NINTH = 4096; // 0x1000

/** Gear in parking state */

@Deprecated public static final int GEAR_PARK = 4; // 0x4

/** Gear in reverse */

@Deprecated public static final int GEAR_REVERSE = 2; // 0x2

/** Gear number 2. */

@Deprecated public static final int GEAR_SECOND = 32; // 0x20

/** Gear number 7. */

@Deprecated public static final int GEAR_SEVENTH = 1024; // 0x400

/** Gear number 6. */

@Deprecated public static final int GEAR_SIXTH = 512; // 0x200

/** Gear number 10. */

@Deprecated public static final int GEAR_TENTH = 8192; // 0x2000

/** Gear number 3. */

@Deprecated public static final int GEAR_THIRD = 64; // 0x40

/** Accessory is turned off, but engine is not running yet (for EV car is not ready to move). */

@Deprecated public static final int IGNITION_STATE_ACC = 3; // 0x3

/**
 * Steering wheel is locked.
 */

@Deprecated public static final int IGNITION_STATE_LOCK = 1; // 0x1

/** Typically engine is off, but steering wheel is unlocked. */

@Deprecated public static final int IGNITION_STATE_OFF = 2; // 0x2

/** In this state engine typically is running (for EV, car is ready to move). */

@Deprecated public static final int IGNITION_STATE_ON = 4; // 0x4

/** In this state engine is typically starting (cranking). */

@Deprecated public static final int IGNITION_STATE_START = 5; // 0x5

/**
 * Ignition state is unknown.
 *
 * The constants that starts with IGNITION_STATE_ represent values for
 * {@link CarSensorManager#SENSOR_TYPE_IGNITION_STATE} sensor.
 * */

@Deprecated public static final int IGNITION_STATE_UNDEFINED = 0; // 0x0

/**
 * Index for {@link CarSensorManager#SENSOR_TYPE_ENV_OUTSIDE_TEMPERATURE} in floatValues.
 * Temperature in Celsius degrees.
 */

@Deprecated public static final int INDEX_ENVIRONMENT_TEMPERATURE = 0; // 0x0

@Deprecated public static final int INDEX_WHEEL_DISTANCE_FRONT_LEFT = 1; // 0x1

@Deprecated public static final int INDEX_WHEEL_DISTANCE_FRONT_RIGHT = 2; // 0x2

@Deprecated public static final int INDEX_WHEEL_DISTANCE_REAR_LEFT = 4; // 0x4

@Deprecated public static final int INDEX_WHEEL_DISTANCE_REAR_RIGHT = 3; // 0x3

/**
 * Index for {@link CarSensorManager#SENSOR_TYPE_WHEEL_TICK_DISTANCE} in longValues. RESET_COUNT
 * is incremented whenever the HAL detects that a sensor reset has occurred.  It represents to
 * the upper layer that the WHEEL_DISTANCE values will not be contiguous with other values
 * reported with a different RESET_COUNT.
 */

@Deprecated public static final int INDEX_WHEEL_DISTANCE_RESET_COUNT = 0; // 0x0

/**
 * array holding float type of sensor data. If the sensor has single value, only floatValues[0]
 * should be used. */

@Deprecated public final float[] floatValues;
{ floatValues = new float[0]; }

/** array holding int type of sensor data */

@Deprecated public final int[] intValues;
{ intValues = new int[0]; }

/** array holding long int type of sensor data */

@Deprecated public final long[] longValues;
{ longValues = new long[0]; }

/** Sensor type for this event like {@link CarSensorManager#SENSOR_TYPE_CAR_SPEED}. */

@Deprecated public int sensorType;

/**
 * When this data was received from car. It is elapsed real-time of data reception from car in
 * nanoseconds since system boot.
 */

@Deprecated public long timestamp;
@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class EnvironmentData {

/** @hide */

EnvironmentData() { throw new RuntimeException("Stub!"); }

/** If unsupported by the car, this value is NaN. */

@Deprecated public float temperature;

@Deprecated public long timestamp;
}

}

