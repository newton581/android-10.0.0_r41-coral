/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.content.pm;


/**
 * Provides car specific API related with package management.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarPackageManager {

/** @hide */

CarPackageManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Check if given activity is distraction optimized, i.e, allowed in a
 * restricted driving state
 *
 * @param packageName
 * @param className
 * @return
 */

public boolean isActivityDistractionOptimized(java.lang.String packageName, java.lang.String className) { throw new RuntimeException("Stub!"); }

/**
 * Check if given service is distraction optimized, i.e, allowed in a restricted
 * driving state.
 *
 * @param packageName
 * @param className
 * @return
 */

public boolean isServiceDistractionOptimized(java.lang.String packageName, java.lang.String className) { throw new RuntimeException("Stub!"); }
}

