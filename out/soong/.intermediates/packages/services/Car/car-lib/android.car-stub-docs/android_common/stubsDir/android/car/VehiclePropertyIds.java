/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car;


/**
 * Copy from android.hardware.automotive.vehicle-V2.0-java_gen_java/gen/android/hardware/automotive
 * /vehicle/V2_0. Need to update this file when vehicle propertyId is changed in VHAL.
 * Use it as PropertyId in getProperty() and setProperty() in
 * {@link android.car.hardware.property.CarPropertyManager}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VehiclePropertyIds {

public VehiclePropertyIds() { throw new RuntimeException("Stub!"); }

/**
 * @param o Integer
 * @return String
 */

public static java.lang.String toString(int o) { throw new RuntimeException("Stub!"); }

/**
 * ABS is active
 * Requires permission: {@link Car#PERMISSION_CAR_DYNAMICS_STATE}.
 */

public static final int ABS_ACTIVE = 287310858; // 0x1120040a

/**
 * Property to report bootup reason for the current power on. This is a
 * static property that will not change for the whole duration until power
 * off. For example, even if user presses power on button after automatic
 * power on with door unlock, bootup reason must stay with
 * VehicleApPowerBootupReason#USER_UNLOCK.
 * Requires permission: {@link Car#PERMISSION_CAR_POWER}.
 */

public static final int AP_POWER_BOOTUP_REASON = 289409538; // 0x11400a02

/**
 * Property to report power state of application processor
 *
 * It is assumed that AP's power state is controller by separate power
 * controller.
 * Requires permission: {@link Car#PERMISSION_CAR_POWER}.
 */

public static final int AP_POWER_STATE_REPORT = 289475073; // 0x11410a01

/**
 * Property to control power state of application processor
 *
 * It is assumed that AP's power state is controller by separate power
 * controller.
 * Requires permission: {@link Car#PERMISSION_CAR_POWER}.
 */

public static final int AP_POWER_STATE_REQ = 289475072; // 0x11410a00

/**
 * Cabin lights
 * Requires permission: {@link Car#PERMISSION_READ_INTERIOR_LIGHTS}.
 */

public static final int CABIN_LIGHTS_STATE = 289410817; // 0x11400f01

/**
 * Cabin lights switch
 * Requires permission: {@link Car#PERMISSION_CONTROL_INTERIOR_LIGHTS}.
 */

public static final int CABIN_LIGHTS_SWITCH = 289410818; // 0x11400f02

/**
 * Current gear. In non-manual case, selected gear may not
 * match the current gear. For example, if the selected gear is GEAR_DRIVE,
 * the current gear will be one of GEAR_1, GEAR_2 etc, which reflects
 * the actual gear the transmission is currently running in.
 * Requires permission: {@link Car#PERMISSION_POWERTRAIN}.
 */

public static final int CURRENT_GEAR = 289408001; // 0x11400401

/**
 * Property to represent brightness of the display. Some cars have single
 * control for the brightness of all displays and this property is to share
 * change in that control.
 * Requires permission: {@link Car#PERMISSION_CAR_POWER}.
 */

public static final int DISPLAY_BRIGHTNESS = 289409539; // 0x11400a03

/**
 * Distance units for display
 * Requires permission {@link Car#PERMISSION_READ_DISPLAY_UNITS} to read the property.
 * Requires permission {@link Car#PERMISSION_CONTROL_DISPLAY_UNITS} and
 * {@link Car#PERMISSION_VENDOR_EXTENSION}to write the property.
 */

public static final int DISTANCE_DISPLAY_UNITS = 289408512; // 0x11400600

/**
 * Door lock
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_DOORS}.
 */

public static final int DOOR_LOCK = 371198722; // 0x16200b02

/**
 * Door move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_DOORS}.
 */

public static final int DOOR_MOVE = 373295873; // 0x16400b01

/**
 * Door position
 *
 * This is an integer in case a door may be set to a particular position.
 * Max value indicates fully open, min value (0) indicates fully closed.
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_DOORS}.
 */

public static final int DOOR_POS = 373295872; // 0x16400b00

/**
 * Temperature of engine coolant
 * Requires permission: {@link Car#PERMISSION_CAR_ENGINE_DETAILED}.
 */

public static final int ENGINE_COOLANT_TEMP = 291504897; // 0x11600301

/**
 * Engine oil level
 * Requires permission: {@link Car#PERMISSION_CAR_ENGINE_DETAILED}.
 */

public static final int ENGINE_OIL_LEVEL = 289407747; // 0x11400303

/**
 * Temperature of engine oil
 * Requires permission: {@link Car#PERMISSION_CAR_ENGINE_DETAILED}.
 */

public static final int ENGINE_OIL_TEMP = 291504900; // 0x11600304

/**
 * Engine rpm
 * Requires permission: {@link Car#PERMISSION_CAR_ENGINE_DETAILED}.
 */

public static final int ENGINE_RPM = 291504901; // 0x11600305

/**
 * Outside temperature
 * Requires permission: {@link Car#PERMISSION_EXTERIOR_ENVIRONMENT}.
 */

public static final int ENV_OUTSIDE_TEMPERATURE = 291505923; // 0x11600703

/**
 * EV battery units for display
 * Requires permission {@link Car#PERMISSION_READ_DISPLAY_UNITS} to read the property.
 * Requires permission {@link Car#PERMISSION_CONTROL_DISPLAY_UNITS} and
 * {@link Car#PERMISSION_VENDOR_EXTENSION}to write the property.
 */

public static final int EV_BATTERY_DISPLAY_UNITS = 289408515; // 0x11400603

/**
 * EV instantaneous charge rate in milliwatts
 * Requires permission: {@link Car#PERMISSION_ENERGY}.
 */

public static final int EV_BATTERY_INSTANTANEOUS_CHARGE_RATE = 291504908; // 0x1160030c

/**
 * EV battery level in WH, if EV or hybrid
 * Requires permission: {@link Car#PERMISSION_ENERGY}.
 */

public static final int EV_BATTERY_LEVEL = 291504905; // 0x11600309

/**
 * EV charge port connected
 * Requires permission: {@link Car#PERMISSION_ENERGY_PORTS}.
 */

public static final int EV_CHARGE_PORT_CONNECTED = 287310603; // 0x1120030b

/**
 * EV charge port open
 * Requires permission: {@link Car#PERMISSION_ENERGY_PORTS}.
 */

public static final int EV_CHARGE_PORT_OPEN = 287310602; // 0x1120030a

/**
 * Fog light state
 * Requires permission: {@link Car#PERMISSION_EXTERIOR_LIGHTS}.
 */

public static final int FOG_LIGHTS_STATE = 289410562; // 0x11400e02

/**
 * Fog light switch
 * Requires permission: {@link Car#PERMISSION_CONTROL_EXTERIOR_LIGHTS}.
 */

public static final int FOG_LIGHTS_SWITCH = 289410578; // 0x11400e12

/**
 * Fuel consumption units for display
 * Requires permission {@link Car#PERMISSION_READ_DISPLAY_UNITS} to read the property.
 * Requires permission {@link Car#PERMISSION_CONTROL_DISPLAY_UNITS} and
 * {@link Car#PERMISSION_VENDOR_EXTENSION}to write the property.
 */

public static final int FUEL_CONSUMPTION_UNITS_DISTANCE_OVER_VOLUME = 287311364; // 0x11200604

/**
 * Fuel door open
 * Requires permission: {@link Car#PERMISSION_ENERGY_PORTS}.
 */

public static final int FUEL_DOOR_OPEN = 287310600; // 0x11200308

/**
 * Fuel remaining in the the vehicle, in milliliters
 * Requires permission: {@link Car#PERMISSION_ENERGY}.
 */

public static final int FUEL_LEVEL = 291504903; // 0x11600307

/**
 * Warning for fuel low level.
 * Requires permission: {@link Car#PERMISSION_ENERGY}.
 */

public static final int FUEL_LEVEL_LOW = 287310853; // 0x11200405

/**
 * Fuel volume units for display
 * Requires permission {@link Car#PERMISSION_READ_DISPLAY_UNITS} to read the property.
 * Requires permission {@link Car#PERMISSION_CONTROL_DISPLAY_UNITS} and
 * {@link Car#PERMISSION_VENDOR_EXTENSION}to write the property.
 */

public static final int FUEL_VOLUME_DISPLAY_UNITS = 289408513; // 0x11400601

/**
 * Currently selected gear
 *
 * This is the gear selected by the user.
 * Requires permission: {@link Car#PERMISSION_POWERTRAIN}.
 */

public static final int GEAR_SELECTION = 289408000; // 0x11400400

/**
 * Hazard light status
 * Requires permission: {@link Car#PERMISSION_EXTERIOR_LIGHTS}.
 */

public static final int HAZARD_LIGHTS_STATE = 289410563; // 0x11400e03

/**
 * Hazard light switch
 * Requires permission: {@link Car#PERMISSION_CONTROL_EXTERIOR_LIGHTS}.
 */

public static final int HAZARD_LIGHTS_SWITCH = 289410579; // 0x11400e13

/**
 * Headlights State
 * Requires permission: {@link Car#PERMISSION_EXTERIOR_LIGHTS}.
 */

public static final int HEADLIGHTS_STATE = 289410560; // 0x11400e00

/**
 * Headlight switch
 * Requires permission: {@link Car#PERMISSION_CONTROL_EXTERIOR_LIGHTS}.
 */

public static final int HEADLIGHTS_SWITCH = 289410576; // 0x11400e10

/**
 * High beam lights state
 * Requires permission: {@link Car#PERMISSION_EXTERIOR_LIGHTS}.
 */

public static final int HIGH_BEAM_LIGHTS_STATE = 289410561; // 0x11400e01

/**
 * High beam light switch
 * Requires permission: {@link Car#PERMISSION_CONTROL_EXTERIOR_LIGHTS}.
 */

public static final int HIGH_BEAM_LIGHTS_SWITCH = 289410577; // 0x11400e11

/**
 * Actual fan speed
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_ACTUAL_FAN_SPEED_RPM = 356517135; // 0x1540050f

/**
 * On/off AC for designated areaId
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_AC_ON = 354419973; // 0x15200505

/**
 * On/off automatic mode
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_AUTO_ON = 354419978; // 0x1520050a

/**
 * Automatic recirculation on/off
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_AUTO_RECIRC_ON = 354419986; // 0x15200512

/**
 * On/off defrost for designated window
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_DEFROSTER = 320865540; // 0x13200504

/**
 * Enable temperature coupling between areas.
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_DUAL_ON = 354419977; // 0x15200509

/**
 * Fan direction setting
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_FAN_DIRECTION = 356517121; // 0x15400501

/**
 * Fan Positions Available
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_FAN_DIRECTION_AVAILABLE = 356582673; // 0x15410511

/**
 * Fan speed setting
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_FAN_SPEED = 356517120; // 0x15400500

/**
 * On/off max AC
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_MAX_AC_ON = 354419974; // 0x15200506

/**
 * On/off max defrost
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_MAX_DEFROST_ON = 354419975; // 0x15200507

/**
 * Represents global power state for HVAC. Setting this property to false
 * MAY mark some properties that control individual HVAC features/subsystems
 * to UNAVAILABLE state. Setting this property to true MAY mark some
 * properties that control individual HVAC features/subsystems to AVAILABLE
 * state (unless any/all of them are UNAVAILABLE on their own individual
 * merits).
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_POWER_ON = 354419984; // 0x15200510

/**
 * Recirculation on/off
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_RECIRC_ON = 354419976; // 0x15200508

/**
 * Seat heating/cooling
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_SEAT_TEMPERATURE = 356517131; // 0x1540050b

/**
 * Seat ventilation
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_SEAT_VENTILATION = 356517139; // 0x15400513

/**
 * Side Mirror Heat
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_SIDE_MIRROR_HEAT = 339739916; // 0x1440050c

/**
 * Steering Wheel Heating/Cooling
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_STEERING_WHEEL_HEAT = 289408269; // 0x1140050d

/**
 * HVAC current temperature.
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_TEMPERATURE_CURRENT = 358614274; // 0x15600502

/**
 * Temperature units for display
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_TEMPERATURE_DISPLAY_UNITS = 289408270; // 0x1140050e

/**
 * HVAC, target temperature set.
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_CLIMATE}.
 */

public static final int HVAC_TEMPERATURE_SET = 358614275; // 0x15600503

/**
 * Property to feed H/W input events to android
 */

public static final int HW_KEY_INPUT = 289475088; // 0x11410a10

/**
 * Represents ignition state
 * Requires permission: {@link Car#PERMISSION_POWERTRAIN}.
 */

public static final int IGNITION_STATE = 289408009; // 0x11400409

/**
 * Driver's seat location
 * Requires permission: {@link Car#PERMISSION_CAR_INFO}.
 */

public static final int INFO_DRIVER_SEAT = 356516106; // 0x1540010a

/**
 * Battery capacity of the vehicle, if EV or hybrid.  This is the nominal
 * battery capacity when the vehicle is new.
 * Requires permission: {@link Car#PERMISSION_CAR_INFO}.
 */

public static final int INFO_EV_BATTERY_CAPACITY = 291504390; // 0x11600106

/**
 * List of connectors this EV may use
 * Requires permission: {@link Car#PERMISSION_CAR_INFO}.
 */

public static final int INFO_EV_CONNECTOR_TYPE = 289472775; // 0x11410107

/**
 * EV port location
 * Requires permission: {@link Car#PERMISSION_CAR_INFO}.
 */

public static final int INFO_EV_PORT_LOCATION = 289407241; // 0x11400109

/**
 * Fuel capacity of the vehicle in milliliters
 * Requires permission: {@link Car#PERMISSION_CAR_INFO}.
 */

public static final int INFO_FUEL_CAPACITY = 291504388; // 0x11600104

/**
 * Fuel door location
 * Requires permission: {@link Car#PERMISSION_CAR_INFO}.
 */

public static final int INFO_FUEL_DOOR_LOCATION = 289407240; // 0x11400108

/**
 * List of fuels the vehicle may use
 * Requires permission: {@link Car#PERMISSION_CAR_INFO}.
 */

public static final int INFO_FUEL_TYPE = 289472773; // 0x11410105

/**
 * Manufacturer of vehicle
 * Requires permission: {@link Car#PERMISSION_CAR_INFO}.
 */

public static final int INFO_MAKE = 286261505; // 0x11100101

/**
 * Model of vehicle
 * Requires permission: {@link Car#PERMISSION_CAR_INFO}.
 */

public static final int INFO_MODEL = 286261506; // 0x11100102

/**
 * Model year of vehicle.
 * Requires permission: {@link Car#PERMISSION_CAR_INFO}.
 */

public static final int INFO_MODEL_YEAR = 289407235; // 0x11400103

/**
 * VIN of vehicle
 * Requires permission: {@link Car#PERMISSION_IDENTIFICATION}.
 */

public static final int INFO_VIN = 286261504; // 0x11100100

/**
 * Undefined property.  */

public static final int INVALID = 0; // 0x0

/**
 * Mirror Fold
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_MIRRORS}.
 */

public static final int MIRROR_FOLD = 287312709; // 0x11200b45

/**
 * Mirror Lock
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_MIRRORS}.
 */

public static final int MIRROR_LOCK = 287312708; // 0x11200b44

/**
 * Mirror Y Move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_MIRRORS}.
 */

public static final int MIRROR_Y_MOVE = 339741507; // 0x14400b43

/**
 * Mirror Y Position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_MIRRORS}.
 */

public static final int MIRROR_Y_POS = 339741506; // 0x14400b42

/**
 * Mirror Z Move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_MIRRORS}.
 */

public static final int MIRROR_Z_MOVE = 339741505; // 0x14400b41

/**
 * Mirror Z Position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_MIRRORS}.
 */

public static final int MIRROR_Z_POS = 339741504; // 0x14400b40

/**
 * Night mode
 * Requires permission: {@link Car#PERMISSION_EXTERIOR_ENVIRONMENT}.
 */

public static final int NIGHT_MODE = 287310855; // 0x11200407

/**
 * OBD2 Freeze Frame Sensor Data
 *
 * Reports a snapshot of the value of the OBD2 sensors available at the time that a fault
 * occurred and was detected.
 * Requires permission: {@link Car#PERMISSION_CAR_DIAGNOSTIC_READ_ALL}.
 */

public static final int OBD2_FREEZE_FRAME = 299896065; // 0x11e00d01

/**
 * OBD2 Freeze Frame Clear
 *
 * This property allows deletion of any of the freeze frames stored in
 * vehicle memory, as described by OBD2_FREEZE_FRAME_INFO.
 * Requires permission: {@link Car#PERMISSION_CAR_DIAGNOSTIC_CLEAR}.
 */

public static final int OBD2_FREEZE_FRAME_CLEAR = 299896067; // 0x11e00d03

/**
 * OBD2 Freeze Frame Information
 * Requires permission: {@link Car#PERMISSION_CAR_DIAGNOSTIC_READ_ALL}.
 */

public static final int OBD2_FREEZE_FRAME_INFO = 299896066; // 0x11e00d02

/**
 * OBD2 Live Sensor Data
 *
 * Reports a snapshot of the current (live) values of the OBD2 sensors available.
 * Requires permission: {@link Car#PERMISSION_CAR_DIAGNOSTIC_READ_ALL}.
 */

public static final int OBD2_LIVE_FRAME = 299896064; // 0x11e00d00

/**
 * Auto-apply parking brake.
 * Requires permission: {@link Car#PERMISSION_POWERTRAIN}.
 */

public static final int PARKING_BRAKE_AUTO_APPLY = 287310851; // 0x11200403

/**
 * Parking brake state.
 * Requires permission: {@link Car#PERMISSION_POWERTRAIN}.
 */

public static final int PARKING_BRAKE_ON = 287310850; // 0x11200402

/**
 * Current odometer value of the vehicle
 * Requires permission: {@link Car#PERMISSION_MILEAGE}.
 */

public static final int PERF_ODOMETER = 291504644; // 0x11600204

/**
 * Steering angle of the vehicle
 *
 * Angle is in degrees. Left is negative.
 * Requires permission: {@link Car#PERMISSION_READ_STEERING_STATE}.
 */

public static final int PERF_STEERING_ANGLE = 291504649; // 0x11600209

/**
 * Speed of the vehicle
 * Requires permission: {@link Car#PERMISSION_SPEED}.
 */

public static final int PERF_VEHICLE_SPEED = 291504647; // 0x11600207

/**
 * Speed of the vehicle for displays
 *
 * Some cars display a slightly slower speed than the actual speed. This is
 * usually displayed on the speedometer.
 * Requires permission: {@link Car#PERMISSION_SPEED}.
 */

public static final int PERF_VEHICLE_SPEED_DISPLAY = 291504648; // 0x11600208

/**
 * Range remaining
 * Requires permission: {@link Car#PERMISSION_ENERGY}.
 */

public static final int RANGE_REMAINING = 291504904; // 0x11600308

/**
 * Reading lights
 * Requires permission: {@link Car#PERMISSION_READ_INTERIOR_LIGHTS}.
 */

public static final int READING_LIGHTS_STATE = 356519683; // 0x15400f03

/**
 * Reading lights switch
 * Requires permission: {@link Car#PERMISSION_CONTROL_INTERIOR_LIGHTS}.
 */

public static final int READING_LIGHTS_SWITCH = 356519684; // 0x15400f04

/**
 * Seat backrest angle 1 move
 *
 * Moves the backrest forward or recline.
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_BACKREST_ANGLE_1_MOVE = 356518792; // 0x15400b88

/**
 * Seat backrest angle 1 position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_BACKREST_ANGLE_1_POS = 356518791; // 0x15400b87

/**
 * Seat backrest angle 2 move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_BACKREST_ANGLE_2_MOVE = 356518794; // 0x15400b8a

/**
 * Seat backrest angle 2 position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_BACKREST_ANGLE_2_POS = 356518793; // 0x15400b89

/**
 * Seatbelt buckled
 *
 * True indicates belt is buckled.
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_BELT_BUCKLED = 354421634; // 0x15200b82

/**
 * Seatbelt height move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_BELT_HEIGHT_MOVE = 356518788; // 0x15400b84

/**
 * Seatbelt height position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_BELT_HEIGHT_POS = 356518787; // 0x15400b83

/**
 * Seat depth move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_DEPTH_MOVE = 356518798; // 0x15400b8e

/**
 * Seat depth position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_DEPTH_POS = 356518797; // 0x15400b8d

/**
 * Seat fore/aft move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_FORE_AFT_MOVE = 356518790; // 0x15400b86

/**
 * Seat fore/aft position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_FORE_AFT_POS = 356518789; // 0x15400b85

/**
 * Headrest angle move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_HEADREST_ANGLE_MOVE = 356518808; // 0x15400b98

/**
 * Headrest angle position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_HEADREST_ANGLE_POS = 356518807; // 0x15400b97

/**
 * Headrest fore/aft move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_HEADREST_FORE_AFT_MOVE = 356518810; // 0x15400b9a

/**
 * Headrest fore/aft position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_HEADREST_FORE_AFT_POS = 356518809; // 0x15400b99

/**
 * Headrest height move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_HEADREST_HEIGHT_MOVE = 356518806; // 0x15400b96

/**
 * Headrest height position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_HEADREST_HEIGHT_POS = 289409941; // 0x11400b95

/**
 * Seat height move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_HEIGHT_MOVE = 356518796; // 0x15400b8c

/**
 * Seat height position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_HEIGHT_POS = 356518795; // 0x15400b8b

/**
 * Lumbar fore/aft move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_LUMBAR_FORE_AFT_MOVE = 356518802; // 0x15400b92

/**
 * Lumber fore/aft position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_LUMBAR_FORE_AFT_POS = 356518801; // 0x15400b91

/**
 * Lumbar side support move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_LUMBAR_SIDE_SUPPORT_MOVE = 356518804; // 0x15400b94

/**
 * Lumbar side support position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_LUMBAR_SIDE_SUPPORT_POS = 356518803; // 0x15400b93

/**
 * Seat memory select
 *
 * This parameter selects the memory preset to use to select the seat
 * position. The minValue is always 0, and the maxValue determines the
 * number of seat positions available.
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_MEMORY_SELECT = 356518784; // 0x15400b80

/**
 * Seat memory set
 *
 * This setting allows the user to save the current seat position settings
 * into the selected preset slot.  The maxValue for each seat position
 * must match the maxValue for SEAT_MEMORY_SELECT.
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_MEMORY_SET = 356518785; // 0x15400b81

/**
 * Seat Occupancy
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_OCCUPANCY = 356518832; // 0x15400bb0

/**
 * Seat tilt move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_TILT_MOVE = 356518800; // 0x15400b90

/**
 * Seat tilt position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_SEATS}.
 */

public static final int SEAT_TILT_POS = 356518799; // 0x15400b8f

/**
 * Tire pressure
 *
 * min/max value indicates tire pressure sensor range.  Each tire will have a separate min/max
 * value denoted by its areaConfig.areaId.
 * Requires permission: {@link Car#PERMISSION_TIRES}.
 */

public static final int TIRE_PRESSURE = 392168201; // 0x17600309

/**
 * Tire pressure units for display
 * Requires permission {@link Car#PERMISSION_READ_DISPLAY_UNITS} to read the property.
 * Requires permission {@link Car#PERMISSION_CONTROL_DISPLAY_UNITS} and
 * {@link Car#PERMISSION_VENDOR_EXTENSION}to write the property.
 */

public static final int TIRE_PRESSURE_DISPLAY_UNITS = 289408514; // 0x11400602

/**
 * Traction Control is active
 * Requires permission: {@link Car#PERMISSION_CAR_DYNAMICS_STATE}.
 */

public static final int TRACTION_CONTROL_ACTIVE = 287310859; // 0x1120040b

/**
 * State of the vehicles turn signals
 * Requires permission: {@link Car#PERMISSION_EXTERIOR_LIGHTS}.
 */

public static final int TURN_SIGNAL_STATE = 289408008; // 0x11400408

/**
 * Vehicle Maps Service (VMS) message
 * Requires one of permissions in {@link Car#PERMISSION_VMS_PUBLISHER},
 * {@link Car#PERMISSION_VMS_SUBSCRIBER}.
 */

public static final int VEHICLE_MAP_SERVICE = 299895808; // 0x11e00c00

/**
 * Reports wheel ticks
 * Requires permission: {@link Car#PERMISSION_SPEED}.
 */

public static final int WHEEL_TICK = 290521862; // 0x11510306

/**
 * Window Lock
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_WINDOWS}.
 */

public static final int WINDOW_LOCK = 320867268; // 0x13200bc4

/**
 * Window Move
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_WINDOWS}.
 */

public static final int WINDOW_MOVE = 322964417; // 0x13400bc1

/**
 * Window Position
 * Requires permission: {@link Car#PERMISSION_CONTROL_CAR_WINDOWS}.
 */

public static final int WINDOW_POS = 322964416; // 0x13400bc0
}

