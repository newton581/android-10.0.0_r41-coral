/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.content.pm;


/**
 * Contains application blocking policy
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarAppBlockingPolicy implements android.os.Parcelable {

public CarAppBlockingPolicy(android.car.content.pm.AppBlockingPackageInfo[] whitelists, android.car.content.pm.AppBlockingPackageInfo[] blacklists) { throw new RuntimeException("Stub!"); }

public CarAppBlockingPolicy(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.content.pm.CarAppBlockingPolicy> CREATOR;
static { CREATOR = null; }

public final android.car.content.pm.AppBlockingPackageInfo[] blacklists;
{ blacklists = new android.car.content.pm.AppBlockingPackageInfo[0]; }

public final android.car.content.pm.AppBlockingPackageInfo[] whitelists;
{ whitelists = new android.car.content.pm.AppBlockingPackageInfo[0]; }
}

