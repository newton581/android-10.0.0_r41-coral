/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.vms;

import java.util.concurrent.Executor;

/**
 * API implementation for use by Vehicle Map Service subscribers.
 *
 * Supports a single client callback that can subscribe and unsubscribe to different data layers.
 * {@link #setVmsSubscriberClientCallback} must be called before any subscription operations.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VmsSubscriberManager {

/**
 * Hidden constructor - can only be used internally.
 *
 * @hide
 */

VmsSubscriberManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Sets the subscriber client's callback, for receiving layer availability and data events.
 *
 * @param executor       {@link Executor} to handle the callbacks
 * @param clientCallback subscriber callback that will handle events
 * @throws IllegalStateException if the client callback was already set
 */

public void setVmsSubscriberClientCallback(java.util.concurrent.Executor executor, android.car.vms.VmsSubscriberManager.VmsSubscriberClientCallback clientCallback) { throw new RuntimeException("Stub!"); }

/**
 * Clears the subscriber client's callback.
 */

public void clearVmsSubscriberClientCallback() { throw new RuntimeException("Stub!"); }

/**
 * Gets a publisher's self-reported description information.
 *
 * @param publisherId publisher ID to retrieve information for
 * @return serialized publisher information, in a vendor-specific format
 */

public byte[] getPublisherInfo(int publisherId) { throw new RuntimeException("Stub!"); }

/**
 * Gets all layers available for subscription.
 *
 * @return available layers
 */

public android.car.vms.VmsAvailableLayers getAvailableLayers() { throw new RuntimeException("Stub!"); }

/**
 * Subscribes to data packets for a specific layer.
 *
 * @param layer layer to subscribe to
 * @throws IllegalStateException if the client callback was not set via
 *                               {@link #setVmsSubscriberClientCallback}.
 */

public void subscribe(android.car.vms.VmsLayer layer) { throw new RuntimeException("Stub!"); }

/**
 * Subscribes to data packets for a specific layer from a specific publisher.
 *
 * @param layer       layer to subscribe to
 * @param publisherId a publisher of the layer
 * @throws IllegalStateException if the client callback was not set via
 *                               {@link #setVmsSubscriberClientCallback}.
 */

public void subscribe(android.car.vms.VmsLayer layer, int publisherId) { throw new RuntimeException("Stub!"); }

/**
 * Start monitoring all messages for all layers, regardless of subscriptions.
 */

public void startMonitoring() { throw new RuntimeException("Stub!"); }

/**
 * Unsubscribes from data packets for a specific layer.
 *
 * @param layer layer to unsubscribe from
 * @throws IllegalStateException if the client callback was not set via
 *                               {@link #setVmsSubscriberClientCallback}.
 */

public void unsubscribe(android.car.vms.VmsLayer layer) { throw new RuntimeException("Stub!"); }

/**
 * Unsubscribes from data packets for a specific layer from a specific publisher.
 *
 * @param layer       layer to unsubscribe from
 * @param publisherId a publisher of the layer
 * @throws IllegalStateException if the client callback was not set via
 *                               {@link #setVmsSubscriberClientCallback}.
 */

public void unsubscribe(android.car.vms.VmsLayer layer, int publisherId) { throw new RuntimeException("Stub!"); }

/**
 * Stop monitoring. Only receive messages for layers which have been subscribed to."
 */

public void stopMonitoring() { throw new RuntimeException("Stub!"); }
/**
 * Callback interface for Vehicle Map Service subscribers.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface VmsSubscriberClientCallback {

/**
 * Called when a data packet is received.
 *
 * @param layer   subscribed layer that packet was received for
 * @param payload data packet that was received
 */

public void onVmsMessageReceived(android.car.vms.VmsLayer layer, byte[] payload);

/**
 * Called when set of available data layers changes.
 *
 * @param availableLayers set of available data layers
 */

public void onLayersAvailabilityChanged(android.car.vms.VmsAvailableLayers availableLayers);
}

}

