/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.vms;


/**
 * A Vehicle Map Service layer, which can be offered or subscribed to by clients.
 *
 * The full layer definition is used when routing packets, with each layer having the following
 * properties:
 *
 * <ul>
 * <li>Type: Type of data being published.</li>
 * <li>Subtype: Type of packet being published.</li>
 * <li>Version: Major version of the packet format. Different versions are not guaranteed to be
 * compatible.</li>
 * </ul>
 *
 * See the Vehicle Maps Service partner documentation for the set of valid types and subtypes.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VmsLayer implements android.os.Parcelable {

/**
 * Constructs a new layer definition.
 *
 * @param type    type of data published on the layer
 * @param subtype type of packet published on the layer
 * @param version major version of layer packet format
 */

public VmsLayer(int type, int subtype, int version) { throw new RuntimeException("Stub!"); }

/**
 * @return type of data published on the layer
 */

public int getType() { throw new RuntimeException("Stub!"); }

/**
 * @return type of packet published on the layer
 */

public int getSubtype() { throw new RuntimeException("Stub!"); }

/**
 * @return major version of layer packet format
 */

public int getVersion() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.vms.VmsLayer> CREATOR;
static { CREATOR = null; }
}

