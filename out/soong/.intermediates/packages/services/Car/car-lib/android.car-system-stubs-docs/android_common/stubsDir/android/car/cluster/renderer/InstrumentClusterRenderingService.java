/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.cluster.renderer;

import android.app.Service;
import android.car.Car;
import android.content.ComponentName;
import android.app.ActivityOptions;
import java.util.Set;
import android.net.Uri;
import android.os.Bundle;
import android.content.Intent;

/**
 * A service used for interaction between Car Service and Instrument Cluster. Car Service may
 * provide internal navigation binder interface to Navigation App and all notifications will be
 * eventually land in the {@link NavigationRenderer} returned by {@link #getNavigationRenderer()}.
 *
 * <p>To extend this class, you must declare the service in your manifest file with
 * the {@code android.car.permission.BIND_INSTRUMENT_CLUSTER_RENDERER_SERVICE} permission
 * <pre>
 * &lt;service android:name=".MyInstrumentClusterService"
 *          android:permission="android.car.permission.BIND_INSTRUMENT_CLUSTER_RENDERER_SERVICE">
 * &lt;/service></pre>
 * <p>Also, you will need to register this service in the following configuration file:
 * {@code packages/services/Car/service/res/values/config.xml}
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class InstrumentClusterRenderingService extends android.app.Service {

public InstrumentClusterRenderingService() { throw new RuntimeException("Stub!"); }

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Returns {@link NavigationRenderer} or null if it's not supported. This renderer will be
 * shared with the navigation context owner (application holding navigation focus).
 */

public abstract android.car.cluster.renderer.NavigationRenderer getNavigationRenderer();

/**
 * Called when key event that was addressed to instrument cluster display has been received.
 */

public void onKeyEvent(android.view.KeyEvent keyEvent) { throw new RuntimeException("Stub!"); }

/**
 * Called when a navigation application becomes a context owner (receives navigation focus) and
 * its {@link Car#CATEGORY_NAVIGATION} activity is launched.
 */

public void onNavigationComponentLaunched() { throw new RuntimeException("Stub!"); }

/**
 * Called when the current context owner (application holding navigation focus) releases the
 * focus and its {@link Car#CAR_CATEGORY_NAVIGATION} activity is ready to be replaced by a
 * system default.
 */

public void onNavigationComponentReleased() { throw new RuntimeException("Stub!"); }

/**
 * Starts an activity on the cluster using the given component.
 *
 * @return false if the activity couldn't be started.
 */

protected boolean startNavigationActivity(android.content.ComponentName component) { throw new RuntimeException("Stub!"); }

protected void dump(java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) { throw new RuntimeException("Stub!"); }

/**
 * Fetches a bitmap from the navigation context owner (application holding navigation focus).
 * It returns null if:
 * <ul>
 * <li>there is no navigation context owner
 * <li>or if the {@link Uri} is invalid
 * <li>or if it references a process other than the current navigation context owner
 * </ul>
 * This is a costly operation. Returned bitmaps should be cached and fetching should be done on
 * a secondary thread.
 *
 * Will be deprecated. Replaced by {@link #getBitmap(Uri, int, int)}.
 */

public android.graphics.Bitmap getBitmap(android.net.Uri uri) { throw new RuntimeException("Stub!"); }
}

