/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.drivingstate;

import android.car.Car;
import android.os.Handler;

/**
 * API to register and get driving state related information in a car.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarDrivingStateManager {

/** @hide */

CarDrivingStateManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Register a {@link CarDrivingStateEventListener} to listen for driving state changes.
 *
 * @param listener  {@link CarDrivingStateEventListener}
 *
 * @hide
 */

public synchronized void registerListener(android.car.drivingstate.CarDrivingStateManager.CarDrivingStateEventListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Unregister the registered {@link CarDrivingStateEventListener} for the given driving event
 * type.
 *
 * @hide
 */

public synchronized void unregisterListener() { throw new RuntimeException("Stub!"); }

/**
 * Get the current value of the car's driving state.
 *
 * @return {@link CarDrivingStateEvent} corresponding to the given eventType
 *
 * @hide
 */

public android.car.drivingstate.CarDrivingStateEvent getCurrentCarDrivingState() { throw new RuntimeException("Stub!"); }
/**
 * Listener Interface for clients to implement to get updated on driving state changes.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface CarDrivingStateEventListener {

/**
 * Called when the car's driving state changes.
 * @param event Car's driving state.
 */

public void onDrivingStateChanged(android.car.drivingstate.CarDrivingStateEvent event);
}

}

