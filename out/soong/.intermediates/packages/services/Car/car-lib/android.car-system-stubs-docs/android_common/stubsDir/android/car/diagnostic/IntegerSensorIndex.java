/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/


package android.car.diagnostic;


/**
 * This class is a container for the indices of diagnostic sensors. The values are extracted by
 * running packages/services/Car/tools/update-obd2-sensors.py against types.hal.
 *
 * DO NOT EDIT MANUALLY
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class IntegerSensorIndex {

IntegerSensorIndex() { throw new RuntimeException("Stub!"); }

public static final int ABSOLUTE_BAROMETRIC_PRESSURE = 11; // 0xb

public static final int AMBIENT_AIR_TEMPERATURE = 13; // 0xd

public static final int COMMANDED_SECONDARY_AIR_STATUS = 5; // 0x5

public static final int CONTROL_MODULE_VOLTAGE = 12; // 0xc

public static final int DISTANCE_TRAVELED_SINCE_CODES_CLEARED = 10; // 0xa

public static final int DISTANCE_TRAVELED_WITH_MALFUNCTION_INDICATOR_LIGHT_ON = 8; // 0x8

public static final int DRIVER_DEMAND_PERCENT_TORQUE = 24; // 0x18

public static final int ENGINE_ACTUAL_PERCENT_TORQUE = 25; // 0x19

public static final int ENGINE_OIL_TEMPERATURE = 23; // 0x17

public static final int ENGINE_PERCENT_TORQUE_DATA_IDLE = 27; // 0x1b

public static final int ENGINE_PERCENT_TORQUE_DATA_POINT1 = 28; // 0x1c

public static final int ENGINE_PERCENT_TORQUE_DATA_POINT2 = 29; // 0x1d

public static final int ENGINE_PERCENT_TORQUE_DATA_POINT3 = 30; // 0x1e

public static final int ENGINE_PERCENT_TORQUE_DATA_POINT4 = 31; // 0x1f

public static final int ENGINE_REFERENCE_PERCENT_TORQUE = 26; // 0x1a

public static final int FUEL_RAIL_ABSOLUTE_PRESSURE = 22; // 0x16

public static final int FUEL_SYSTEM_STATUS = 0; // 0x0

public static final int FUEL_TYPE = 21; // 0x15

public static final int IGNITION_MONITORS_SUPPORTED = 2; // 0x2

public static final int IGNITION_SPECIFIC_MONITORS = 3; // 0x3

public static final int INTAKE_AIR_TEMPERATURE = 4; // 0x4

public static final int LAST_SYSTEM = 31; // 0x1f

public static final int MALFUNCTION_INDICATOR_LIGHT_ON = 1; // 0x1

public static final int MAX_AIR_FLOW_RATE_FROM_MASS_AIR_FLOW_SENSOR = 20; // 0x14

public static final int MAX_FUEL_AIR_EQUIVALENCE_RATIO = 16; // 0x10

public static final int MAX_INTAKE_MANIFOLD_ABSOLUTE_PRESSURE = 19; // 0x13

public static final int MAX_OXYGEN_SENSOR_CURRENT = 18; // 0x12

public static final int MAX_OXYGEN_SENSOR_VOLTAGE = 17; // 0x11

public static final int NUM_OXYGEN_SENSORS_PRESENT = 6; // 0x6

public static final int RUNTIME_SINCE_ENGINE_START = 7; // 0x7

public static final int TIME_SINCE_TROUBLE_CODES_CLEARED = 15; // 0xf

public static final int TIME_WITH_MALFUNCTION_LIGHT_ON = 14; // 0xe

public static final int VENDOR_START = 32; // 0x20

public static final int WARMUPS_SINCE_CODES_CLEARED = 9; // 0x9
}

