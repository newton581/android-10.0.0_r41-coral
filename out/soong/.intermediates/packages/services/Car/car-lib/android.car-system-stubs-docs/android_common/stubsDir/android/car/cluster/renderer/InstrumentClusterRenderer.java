/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.cluster.renderer;


/**
 * @deprecated This class is unused. Refer to {@link InstrumentClusterRenderingService} for
 * documentation on how to build a instrument cluster renderer.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract class InstrumentClusterRenderer {

@Deprecated
public InstrumentClusterRenderer() { throw new RuntimeException("Stub!"); }

/**
 * Calls once when instrument cluster should be created.
 */

@Deprecated
public abstract void onCreate(android.content.Context context);

@Deprecated
public abstract void onStart();

@Deprecated
public abstract void onStop();

@Deprecated
protected abstract android.car.cluster.renderer.NavigationRenderer createNavigationRenderer();

/** The method is thread-safe, callers should cache returned object. */

@Deprecated
public synchronized android.car.cluster.renderer.NavigationRenderer getNavigationRenderer() { throw new RuntimeException("Stub!"); }

/**
 * This method is called by car service after onCreateView to initialize private members. The
 * method should not be overridden by subclasses.
 */

@Deprecated
public final synchronized void initialize() { throw new RuntimeException("Stub!"); }
}

