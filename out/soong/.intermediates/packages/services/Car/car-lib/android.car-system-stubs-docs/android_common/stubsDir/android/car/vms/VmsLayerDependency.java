/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.vms;


/**
 * Layer dependencies for single Vehicle Map Service layer.
 *
 * Dependencies are treated as <b>hard</b> dependencies, meaning that an offered layer will not be
 * reported as available until all dependent layers are also available.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VmsLayerDependency implements android.os.Parcelable {

/**
 * Constructs a layer with a dependency on other layers.
 *
 * @param layer layer that has dependencies
 * @param dependencies layers that the given layer depends on
 */

public VmsLayerDependency(android.car.vms.VmsLayer layer, java.util.Set<android.car.vms.VmsLayer> dependencies) { throw new RuntimeException("Stub!"); }

/**
 * Constructs a layer without dependencies.
 *
 * @param layer layer that has no dependencies
 */

public VmsLayerDependency(android.car.vms.VmsLayer layer) { throw new RuntimeException("Stub!"); }

/**
 * @return layer that has zero or more dependencies
 */

public android.car.vms.VmsLayer getLayer() { throw new RuntimeException("Stub!"); }

/**
 * @return all layers that the layer depends on
 */

public java.util.Set<android.car.vms.VmsLayer> getDependencies() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.vms.VmsLayerDependency> CREATOR;
static { CREATOR = null; }
}

