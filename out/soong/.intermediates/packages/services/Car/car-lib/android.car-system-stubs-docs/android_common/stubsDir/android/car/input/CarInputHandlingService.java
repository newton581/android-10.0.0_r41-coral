/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.input;

import android.car.Car;

/**
 * A service that is used for handling of input events.
 *
 * <p>To extend this class, you must declare the service in your manifest file with
 * the {@code android.car.permission.BIND_CAR_INPUT_SERVICE} permission
 * <pre>
 * &lt;service android:name=".MyCarInputService"
 *          android:permission="android.car.permission.BIND_CAR_INPUT_SERVICE">
 * &lt;/service></pre>
 * <p>Also, you will need to register this service in the following configuration file:
 * {@code packages/services/Car/service/res/values/config.xml}
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class CarInputHandlingService extends android.app.Service {

protected CarInputHandlingService(android.car.input.CarInputHandlingService.InputFilter[] handledKeys) { throw new RuntimeException("Stub!"); }

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Called when key event has been received.
 */

protected abstract void onKeyEvent(android.view.KeyEvent keyEvent, int targetDisplay);

protected void dump(java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) { throw new RuntimeException("Stub!"); }

public static final int INPUT_CALLBACK_BINDER_CODE = 1; // 0x1

public static final java.lang.String INPUT_CALLBACK_BINDER_KEY = "callback_binder";
/**
 * Filter for input events that are handled by custom service.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class InputFilter implements android.os.Parcelable {

public InputFilter(int keyCode, int targetDisplay) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator CREATOR;
static { CREATOR = null; }

public final int mKeyCode;
{ mKeyCode = 0; }

public final int mTargetDisplay;
{ mTargetDisplay = 0; }
}

}

