/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.content.pm;

import android.os.Parcelable;
import android.content.pm.Signature;

/**
 * Parcelable to hold information on app blocking whitelist or blacklist for a package.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AppBlockingPackageInfo implements android.os.Parcelable {

/**
 * @param flags Value is either <code>0</code> or a combination of {@link android.car.content.pm.AppBlockingPackageInfo#FLAG_SYSTEM_APP}, and {@link android.car.content.pm.AppBlockingPackageInfo#FLAG_WHOLE_ACTIVITY}
 */

public AppBlockingPackageInfo(java.lang.String packageName, int minRevisionCode, int maxRevisionCode, int flags, android.content.pm.Signature[] signatures, java.lang.String[] activities) { throw new RuntimeException("Stub!"); }

public AppBlockingPackageInfo(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.content.pm.AppBlockingPackageInfo> CREATOR;
static { CREATOR = null; }

/** Represents system app which does not need {@link #signature}. */

public static final int FLAG_SYSTEM_APP = 1; // 0x1

/** Blacklist or whitelist every Activities in the package. When this is set,
 *  {@link #activities} may be null. */

public static final int FLAG_WHOLE_ACTIVITY = 2; // 0x2

/** List of activities (full class name). This can be null if Activity is not blocked or
 *  allowed. Additionally, {@link #FLAG_WHOLE_ACTIVITY} set in {@link #flags} shall have
 *  null for this. */

public final java.lang.String[] activities;
{ activities = new java.lang.String[0]; }

/**
 * flags to give additional information on the package.
 * @see #FLAG_SYSTEM_APP
 * @see #FLAG_WHOLE_ACTIVITY
 */

public final int flags;
{ flags = 0; }

/**
 * Package version should be smaller than this to block or allow.
 * (package version < minRevisionCode)
 * 0 means do not care max version.
 */

public final int maxRevisionCode;
{ maxRevisionCode = 0; }

/**
 * Package version should be bigger than this to block or allow.
 * (package version > minRevisionCode)
 * 0 means do not care min version.
 */

public final int minRevisionCode;
{ minRevisionCode = 0; }

/** Package name for the package to block or allow. */

public final java.lang.String packageName;
{ packageName = null; }

/**
 * Signature of package. This can be null if target package is from system so that package
 * name is enough to uniquely identify it (= {@link #flags} having {@link #FLAG_SYSTEM_APP}.
 * Matching any member of array is considered as matching package.
 */

public final android.content.pm.Signature[] signatures;
{ signatures = new android.content.pm.Signature[0]; }
}

