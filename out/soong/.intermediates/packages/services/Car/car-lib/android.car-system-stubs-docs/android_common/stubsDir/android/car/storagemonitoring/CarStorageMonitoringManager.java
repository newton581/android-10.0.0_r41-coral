/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.storagemonitoring;

import android.car.Car;

/**
 * API for retrieving information and metrics about the flash storage.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarStorageMonitoringManager {

/**
 * @hide
 */

CarStorageMonitoringManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * This method returns the value of the "pre EOL" indicator for the flash storage
 * as retrieved during the current boot cycle.
 *
 * It will return either PRE_EOL_INFO_UNKNOWN if the value can't be determined,
 * or one of PRE_EOL_INFO_{NORMAL|WARNING|URGENT} depending on the device state.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_STORAGE_MONITORING}
 */

public int getPreEolIndicatorStatus() { throw new RuntimeException("Stub!"); }

/**
 * This method returns the value of the wear estimate indicators for the flash storage
 * as retrieved during the current boot cycle.
 *
 * The indicators are guaranteed to be a lower-bound on the actual wear of the storage.
 * Current technology in common automotive usage offers estimates in 10% increments.
 *
 * If either or both indicators are not available, they will be reported as UNKNOWN.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_STORAGE_MONITORING}
 */

public android.car.storagemonitoring.WearEstimate getWearEstimate() { throw new RuntimeException("Stub!"); }

/**
 * This method returns a list of all changes in wear estimate indicators detected during the
 * lifetime of the system.
 *
 * The indicators are not guaranteed to persist across a factory reset.
 *
 * The indicators are guaranteed to be a lower-bound on the actual wear of the storage.
 * Current technology in common automotive usage offers estimates in 10% increments.
 *
 * If no indicators are available, an empty list will be returned.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_STORAGE_MONITORING}
 */

public java.util.List<android.car.storagemonitoring.WearEstimateChange> getWearEstimateHistory() { throw new RuntimeException("Stub!"); }

/**
 * This method returns a list of per user-id I/O activity metrics as collected at the end of
 * system boot.
 *
 * The BOOT_COMPLETE broadcast is used as the trigger to collect this data. The implementation
 * may impose an additional, and even variable across boot cycles, delay between the sending
 * of the broadcast and the collection of the data.
 *
 * If the information is not available, an empty list will be returned.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_STORAGE_MONITORING}
 */

public java.util.List<android.car.storagemonitoring.IoStatsEntry> getBootIoStats() { throw new RuntimeException("Stub!"); }

/**
 * This method returns an approximation of the number of bytes written to disk during
 * the course of the previous system shutdown.
 *
 * <p>For purposes of this API the system shutdown is defined as starting when CarService
 * receives the ACTION_SHUTDOWN or ACTION_REBOOT intent from the system.</p>
 *
 * <p>The information provided by this API does not provide attribution of the disk writes to
 * specific applications or system daemons.</p>
 *
 * <p>The information returned by this call is a best effort guess, whose accuracy depends
 * on the underlying file systems' ability to reliably track and accumulate
 * disk write sizes.</p>
 *
 * <p>A corrupt file system, or one which was not cleanly unmounted during shutdown, may
 * be unable to provide any information, or may provide incorrect data. While the API
 * will attempt to detect these scenarios, the detection may fail and incorrect data
 * may end up being used in calculations.</p>
 *
 * <p>If the information is not available, SHUTDOWN_COST_INFO_MISSING will be returned.</p>s

 * <br>
 * Requires {@link android.car.Car#PERMISSION_STORAGE_MONITORING}
 */

public long getShutdownDiskWriteAmount() { throw new RuntimeException("Stub!"); }

/**
 * This method returns a list of per user-id I/O activity metrics as collected from kernel
 * start until the last snapshot.
 *
 * The samples provided might be as old as the value of the ioStatsRefreshRateSeconds setting.
 *
 * If the information is not available, an empty list will be returned.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_STORAGE_MONITORING}
 */

public java.util.List<android.car.storagemonitoring.IoStatsEntry> getAggregateIoStats() { throw new RuntimeException("Stub!"); }

/**
 * This method returns a list of the I/O stats deltas currently stored by the system.
 *
 * Periodically, the system gathers I/O activity metrics and computes and stores a delta from
 * the previous cycle. The timing and the number of these stored samples are configurable
 * by the OEM.
 *
 * The samples are returned in order from the oldest to the newest.
 *
 * If the information is not available, an empty list will be returned.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_STORAGE_MONITORING}
 */

public java.util.List<android.car.storagemonitoring.IoStats> getIoStatsDeltas() { throw new RuntimeException("Stub!"); }

/**
 * This method registers a new listener to receive I/O stats deltas.
 *
 * The system periodically gathers I/O activity metrics and computes a delta of such
 * activity. Registered listeners will receive those deltas as they are available.
 *
 * The timing of availability of the deltas is configurable by the OEM.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_STORAGE_MONITORING}
 */

public void registerListener(android.car.storagemonitoring.CarStorageMonitoringManager.IoStatsListener listener) { throw new RuntimeException("Stub!"); }

/**
 * This method removes a registered listener of I/O stats deltas.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_STORAGE_MONITORING}
 */

public void unregisterListener(android.car.storagemonitoring.CarStorageMonitoringManager.IoStatsListener listener) { throw new RuntimeException("Stub!"); }

public static final java.lang.String INTENT_EXCESSIVE_IO = "android.car.storagemonitoring.EXCESSIVE_IO";

public static final int PRE_EOL_INFO_NORMAL = 1; // 0x1

public static final int PRE_EOL_INFO_UNKNOWN = 0; // 0x0

public static final int PRE_EOL_INFO_URGENT = 3; // 0x3

public static final int PRE_EOL_INFO_WARNING = 2; // 0x2

public static final long SHUTDOWN_COST_INFO_MISSING = -1L; // 0xffffffffffffffffL
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface IoStatsListener {

public void onSnapshot(android.car.storagemonitoring.IoStats snapshot);
}

}

