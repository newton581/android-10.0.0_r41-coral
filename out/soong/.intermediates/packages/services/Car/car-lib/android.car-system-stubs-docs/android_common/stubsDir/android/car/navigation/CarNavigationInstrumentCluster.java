/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.navigation;


/**
 * Holds options related to navigation for the car's instrument cluster.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarNavigationInstrumentCluster implements android.os.Parcelable {

public CarNavigationInstrumentCluster(android.car.navigation.CarNavigationInstrumentCluster that) { throw new RuntimeException("Stub!"); }

public static android.car.navigation.CarNavigationInstrumentCluster createCluster(int minIntervalMillis) { throw new RuntimeException("Stub!"); }

public static android.car.navigation.CarNavigationInstrumentCluster createCustomImageCluster(int minIntervalMs, int imageWidth, int imageHeight, int imageColorDepthBits) { throw new RuntimeException("Stub!"); }

/** Minimum time between instrument cluster updates in milliseconds.*/

public int getMinIntervalMillis() { throw new RuntimeException("Stub!"); }

/**
 * Type of instrument cluster, can be {@link #CLUSTER_TYPE_CUSTOM_IMAGES_SUPPORTED} or
 * {@link #CLUSTER_TYPE_IMAGE_CODES_ONLY}.

 * @return Value is {@link android.car.navigation.CarNavigationInstrumentCluster#CLUSTER_TYPE_CUSTOM_IMAGES_SUPPORTED}, or {@link android.car.navigation.CarNavigationInstrumentCluster#CLUSTER_TYPE_IMAGE_CODES_ONLY}
 */

public int getType() { throw new RuntimeException("Stub!"); }

/** If instrument cluster is image, width of instrument cluster in pixels. */

public int getImageWidth() { throw new RuntimeException("Stub!"); }

/** If instrument cluster is image, height of instrument cluster in pixels. */

public int getImageHeight() { throw new RuntimeException("Stub!"); }

/**
 * If instrument cluster is image, number of bits of colour depth it supports (8, 16, or 32).
 */

public int getImageColorDepthBits() { throw new RuntimeException("Stub!"); }

/**
 * Whether cluster support custom image or not.
 * @return
 */

public boolean supportsCustomImages() { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** Converts to string for debug purpose */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** Navigation Next Turn messages contain an image, as well as an enum. */

public static final int CLUSTER_TYPE_CUSTOM_IMAGES_SUPPORTED = 1; // 0x1

/** Navigation Next Turn messages contain only an enum. */

public static final int CLUSTER_TYPE_IMAGE_CODES_ONLY = 2; // 0x2

public static final android.os.Parcelable.Creator<android.car.navigation.CarNavigationInstrumentCluster> CREATOR;
static { CREATOR = null; }
}

