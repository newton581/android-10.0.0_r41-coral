/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.vms;


/**
 * Availability of Vehicle Map Service layers.
 *
 * The layer availability is used by subscribers to determine which {@link VmsLayer}s are available
 * for subscription and which publishers are offering to publish data for those layers. However,
 * the Vehicle Map Service will allow subscription requests for unavailable layers.
 *
 * Sequence numbers are used to indicate the succession of availability states, and increase
 * monotonically with each change in layer availability. They must be used by clients to ignore
 * states that are received out-of-order.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VmsAvailableLayers implements android.os.Parcelable {

/**
 * Constructs a new layer availability.
 *
 * @param associatedLayers set of layers available for subscription
 * @param sequence         sequence number of the availability state
 */

public VmsAvailableLayers(java.util.Set<android.car.vms.VmsAssociatedLayer> associatedLayers, int sequence) { throw new RuntimeException("Stub!"); }

/**
 * @return sequence number of the availability state
 */

public int getSequence() { throw new RuntimeException("Stub!"); }

/**
 * @return set of layers available for subscription
 */

public java.util.Set<android.car.vms.VmsAssociatedLayer> getAssociatedLayers() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.vms.VmsAvailableLayers> CREATOR;
static { CREATOR = null; }
}

