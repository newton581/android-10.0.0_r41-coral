/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.storagemonitoring;


/**
 * Information about how many bytes were written to a filesystem during its lifetime.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class LifetimeWriteInfo implements android.os.Parcelable {

public LifetimeWriteInfo(java.lang.String partition, java.lang.String fstype, long writtenBytes) { throw new RuntimeException("Stub!"); }

public LifetimeWriteInfo(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object other) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.storagemonitoring.IoStats> CREATOR;
static { CREATOR = null; }

public final java.lang.String fstype;
{ fstype = null; }

public final java.lang.String partition;
{ partition = null; }

public final long writtenBytes;
{ writtenBytes = 0; }
}

