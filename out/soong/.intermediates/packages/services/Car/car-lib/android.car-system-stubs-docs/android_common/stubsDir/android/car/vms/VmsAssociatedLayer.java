/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.vms;


/**
 * A Vehicle Map Service layer with a list of publisher IDs it is associated with.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VmsAssociatedLayer implements android.os.Parcelable {

/**
 * Constructs a new layer offering.
 *
 * @param layer layer being offered
 * @param publisherIds IDs of publishers associated with the layer
 */

public VmsAssociatedLayer(android.car.vms.VmsLayer layer, java.util.Set<java.lang.Integer> publisherIds) { throw new RuntimeException("Stub!"); }

/**
 * @return layer being offered
 */

public android.car.vms.VmsLayer getVmsLayer() { throw new RuntimeException("Stub!"); }

/**
 * @return IDs of publishers associated with the layer
 */

public java.util.Set<java.lang.Integer> getPublisherIds() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.vms.VmsAssociatedLayer> CREATOR;
static { CREATOR = null; }
}

