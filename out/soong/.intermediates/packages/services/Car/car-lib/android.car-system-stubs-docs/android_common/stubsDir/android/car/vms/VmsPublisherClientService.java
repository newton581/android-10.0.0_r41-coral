/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.vms;

import android.car.Car;
import android.app.Service;

/**
 * API implementation of a Vehicle Map Service publisher client.
 *
 * All publisher clients must inherit from this class and export it as a service, and the service
 * be added to either the {@code vmsPublisherSystemClients} or {@code vmsPublisherUserClients}
 * arrays in the Car service configuration, depending on which user the client will run as.
 *
 * The {@link com.android.car.VmsPublisherService} will then bind to this service, with the
 * {@link #onVmsPublisherServiceReady()} callback notifying the client implementation when the
 * connection is established and publisher operations can be called.
 *
 * Publishers must also register a publisher ID by calling {@link #getPublisherId(byte[])}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class VmsPublisherClientService extends android.app.Service {

public VmsPublisherClientService() { throw new RuntimeException("Stub!"); }

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

public boolean onUnbind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Notifies the client that publisher services are ready.
 */

protected abstract void onVmsPublisherServiceReady();

/**
 * Notifies the client of changes in layer subscriptions.
 *
 * @param subscriptionState state of layer subscriptions
 */

public abstract void onVmsSubscriptionChange(android.car.vms.VmsSubscriptionState subscriptionState);

/**
 * Publishes a data packet to subscribers.
 *
 * Publishers must only publish packets for the layers that they have made offerings for.
 *
 * @param layer       layer to publish to
 * @param publisherId ID of the publisher publishing the message
 * @param payload     data packet to be sent
 * @throws IllegalStateException if publisher services are not available
 */

public final void publish(android.car.vms.VmsLayer layer, int publisherId, byte[] payload) { throw new RuntimeException("Stub!"); }

/**
 * Sets the layers offered by a specific publisher.
 *
 * @param offering layers being offered for subscription by the publisher
 * @throws IllegalStateException if publisher services are not available
 */

public final void setLayersOffering(android.car.vms.VmsLayersOffering offering) { throw new RuntimeException("Stub!"); }

/**
 * Acquires a publisher ID for a serialized publisher description.
 *
 * Multiple calls to this method with the same information will return the same publisher ID.
 *
 * @param publisherInfo serialized publisher description information, in a vendor-specific
 *                      format
 * @return a publisher ID for the given publisher description
 * @throws IllegalStateException if publisher services are not available
 */

public final int getPublisherId(byte[] publisherInfo) { throw new RuntimeException("Stub!"); }

/**
 * Gets the state of layer subscriptions.
 *
 * @return state of layer subscriptions
 * @throws IllegalStateException if publisher services are not available
 */

public final android.car.vms.VmsSubscriptionState getSubscriptions() { throw new RuntimeException("Stub!"); }
}

