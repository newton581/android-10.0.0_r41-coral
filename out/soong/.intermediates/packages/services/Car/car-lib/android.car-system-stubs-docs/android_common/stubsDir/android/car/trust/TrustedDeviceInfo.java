/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.trust;


/**
 * Contains basic info of a trusted device.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class TrustedDeviceInfo implements android.os.Parcelable {

public TrustedDeviceInfo(long handle, java.lang.String address, java.lang.String name) { throw new RuntimeException("Stub!"); }

public TrustedDeviceInfo(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Returns the handle of current device
 *
 * @return handle which is unique for every device
 */

public long getHandle() { throw new RuntimeException("Stub!"); }

/**
 * Get local device name of current device
 *
 * @return local device name
 */

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

/**
 * Get MAC address of current device
 *
 * @return MAC address
 */

public java.lang.String getAddress() { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * Serialize the trusted device info into string
 *
 * @return string contains current trusted device information with certain format
 */

public java.lang.String serialize() { throw new RuntimeException("Stub!"); }

/**
 * Deserialize the string to trusted device info
 *
 * @param deviceInfo string which contains trusted device info, should be originally generated
 *                   by serialize method
 * @return TrustedDeviceInfo object constructed from the trusted device info in the string
 */

public static android.car.trust.TrustedDeviceInfo deserialize(java.lang.String deviceInfo) { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator CREATOR;
static { CREATOR = null; }

public static final java.lang.String DEFAULT_NAME = "Default";
}

