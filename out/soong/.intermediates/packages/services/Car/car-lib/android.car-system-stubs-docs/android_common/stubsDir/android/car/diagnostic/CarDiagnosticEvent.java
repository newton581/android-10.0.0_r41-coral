/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.diagnostic;

import android.util.JsonWriter;

/**
 * A CarDiagnosticEvent object corresponds to a single diagnostic event frame coming from the car.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarDiagnosticEvent implements android.os.Parcelable {

public CarDiagnosticEvent(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Store the contents of this diagnostic event in a JsonWriter.
 *
 * The data is stored as a JSON object, with these fields:
 *  type: either "live" or "freeze" depending on the type of frame;
 *  timestamp: the timestamp at which this frame was generated;
 *  intValues: an array of objects each of which has two elements:
 *    id: the integer identifier of the sensor;
 *    value: the integer value of the sensor;
 *  floatValues: an array of objects each of which has two elements:
 *    id: the integer identifier of the sensor;
 *    value: the floating-point value of the sensor;
 *  stringValue: the DTC for a freeze frame, omitted for a live frame
 */

public void writeToJson(android.util.JsonWriter jsonWriter) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/** Returns true if this object is a live frame, false otherwise */

public boolean isLiveFrame() { throw new RuntimeException("Stub!"); }

/** Returns true if this object is a freeze frame, false otherwise */

public boolean isFreezeFrame() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object otherObject) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Returns the value of the given integer sensor, if present in this frame.
 * Returns defaultValue otherwise.

 * @param sensor Value is {@link android.car.diagnostic.IntegerSensorIndex#FUEL_SYSTEM_STATUS}, {@link android.car.diagnostic.IntegerSensorIndex#MALFUNCTION_INDICATOR_LIGHT_ON}, {@link android.car.diagnostic.IntegerSensorIndex#IGNITION_MONITORS_SUPPORTED}, {@link android.car.diagnostic.IntegerSensorIndex#IGNITION_SPECIFIC_MONITORS}, {@link android.car.diagnostic.IntegerSensorIndex#INTAKE_AIR_TEMPERATURE}, {@link android.car.diagnostic.IntegerSensorIndex#COMMANDED_SECONDARY_AIR_STATUS}, {@link android.car.diagnostic.IntegerSensorIndex#NUM_OXYGEN_SENSORS_PRESENT}, {@link android.car.diagnostic.IntegerSensorIndex#RUNTIME_SINCE_ENGINE_START}, {@link android.car.diagnostic.IntegerSensorIndex#DISTANCE_TRAVELED_WITH_MALFUNCTION_INDICATOR_LIGHT_ON}, {@link android.car.diagnostic.IntegerSensorIndex#WARMUPS_SINCE_CODES_CLEARED}, {@link android.car.diagnostic.IntegerSensorIndex#DISTANCE_TRAVELED_SINCE_CODES_CLEARED}, {@link android.car.diagnostic.IntegerSensorIndex#ABSOLUTE_BAROMETRIC_PRESSURE}, {@link android.car.diagnostic.IntegerSensorIndex#CONTROL_MODULE_VOLTAGE}, {@link android.car.diagnostic.IntegerSensorIndex#AMBIENT_AIR_TEMPERATURE}, {@link android.car.diagnostic.IntegerSensorIndex#TIME_WITH_MALFUNCTION_LIGHT_ON}, {@link android.car.diagnostic.IntegerSensorIndex#TIME_SINCE_TROUBLE_CODES_CLEARED}, {@link android.car.diagnostic.IntegerSensorIndex#MAX_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.IntegerSensorIndex#MAX_OXYGEN_SENSOR_VOLTAGE}, {@link android.car.diagnostic.IntegerSensorIndex#MAX_OXYGEN_SENSOR_CURRENT}, {@link android.car.diagnostic.IntegerSensorIndex#MAX_INTAKE_MANIFOLD_ABSOLUTE_PRESSURE}, {@link android.car.diagnostic.IntegerSensorIndex#MAX_AIR_FLOW_RATE_FROM_MASS_AIR_FLOW_SENSOR}, {@link android.car.diagnostic.IntegerSensorIndex#FUEL_TYPE}, {@link android.car.diagnostic.IntegerSensorIndex#FUEL_RAIL_ABSOLUTE_PRESSURE}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_OIL_TEMPERATURE}, {@link android.car.diagnostic.IntegerSensorIndex#DRIVER_DEMAND_PERCENT_TORQUE}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_ACTUAL_PERCENT_TORQUE}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_REFERENCE_PERCENT_TORQUE}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_PERCENT_TORQUE_DATA_IDLE}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_PERCENT_TORQUE_DATA_POINT1}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_PERCENT_TORQUE_DATA_POINT2}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_PERCENT_TORQUE_DATA_POINT3}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_PERCENT_TORQUE_DATA_POINT4}, {@link android.car.diagnostic.IntegerSensorIndex#LAST_SYSTEM}, or {@link android.car.diagnostic.IntegerSensorIndex#VENDOR_START}
 */

public int getSystemIntegerSensor(int sensor, int defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Returns the value of the given float sensor, if present in this frame.
 * Returns defaultValue otherwise.

 * @param sensor Value is {@link android.car.diagnostic.FloatSensorIndex#CALCULATED_ENGINE_LOAD}, {@link android.car.diagnostic.FloatSensorIndex#ENGINE_COOLANT_TEMPERATURE}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_FUEL_TRIM_BANK1}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_FUEL_TRIM_BANK1}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_FUEL_TRIM_BANK2}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_FUEL_TRIM_BANK2}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#INTAKE_MANIFOLD_ABSOLUTE_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#ENGINE_RPM}, {@link android.car.diagnostic.FloatSensorIndex#VEHICLE_SPEED}, {@link android.car.diagnostic.FloatSensorIndex#TIMING_ADVANCE}, {@link android.car.diagnostic.FloatSensorIndex#MAF_AIR_FLOW_RATE}, {@link android.car.diagnostic.FloatSensorIndex#THROTTLE_POSITION}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR1_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR1_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR1_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR2_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR2_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR2_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR3_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR3_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR3_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR4_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR4_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR4_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR5_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR5_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR5_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR6_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR6_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR6_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR7_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR7_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR7_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR8_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR8_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR8_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_RAIL_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_RAIL_GAUGE_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#COMMANDED_EXHAUST_GAS_RECIRCULATION}, {@link android.car.diagnostic.FloatSensorIndex#EXHAUST_GAS_RECIRCULATION_ERROR}, {@link android.car.diagnostic.FloatSensorIndex#COMMANDED_EVAPORATIVE_PURGE}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_TANK_LEVEL_INPUT}, {@link android.car.diagnostic.FloatSensorIndex#EVAPORATION_SYSTEM_VAPOR_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#CATALYST_TEMPERATURE_BANK1_SENSOR1}, {@link android.car.diagnostic.FloatSensorIndex#CATALYST_TEMPERATURE_BANK2_SENSOR1}, {@link android.car.diagnostic.FloatSensorIndex#CATALYST_TEMPERATURE_BANK1_SENSOR2}, {@link android.car.diagnostic.FloatSensorIndex#CATALYST_TEMPERATURE_BANK2_SENSOR2}, {@link android.car.diagnostic.FloatSensorIndex#ABSOLUTE_LOAD_VALUE}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_AIR_COMMANDED_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#RELATIVE_THROTTLE_POSITION}, {@link android.car.diagnostic.FloatSensorIndex#ABSOLUTE_THROTTLE_POSITION_B}, {@link android.car.diagnostic.FloatSensorIndex#ABSOLUTE_THROTTLE_POSITION_C}, {@link android.car.diagnostic.FloatSensorIndex#ACCELERATOR_PEDAL_POSITION_D}, {@link android.car.diagnostic.FloatSensorIndex#ACCELERATOR_PEDAL_POSITION_E}, {@link android.car.diagnostic.FloatSensorIndex#ACCELERATOR_PEDAL_POSITION_F}, {@link android.car.diagnostic.FloatSensorIndex#COMMANDED_THROTTLE_ACTUATOR}, {@link android.car.diagnostic.FloatSensorIndex#ETHANOL_FUEL_PERCENTAGE}, {@link android.car.diagnostic.FloatSensorIndex#ABSOLUTE_EVAPORATION_SYSTEM_VAPOR_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK1}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK2}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK3}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK4}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK1}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK2}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK3}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK4}, {@link android.car.diagnostic.FloatSensorIndex#RELATIVE_ACCELERATOR_PEDAL_POSITION}, {@link android.car.diagnostic.FloatSensorIndex#HYBRID_BATTERY_PACK_REMAINING_LIFE}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_INJECTION_TIMING}, {@link android.car.diagnostic.FloatSensorIndex#ENGINE_FUEL_RATE}, {@link android.car.diagnostic.FloatSensorIndex#LAST_SYSTEM}, or {@link android.car.diagnostic.FloatSensorIndex#VENDOR_START}
 */

public float getSystemFloatSensor(int sensor, float defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Returns the value of the given integer sensor, if present in this frame.
 * Returns defaultValue otherwise.
 */

public int getVendorIntegerSensor(int sensor, int defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Returns the value of the given float sensor, if present in this frame.
 * Returns defaultValue otherwise.
 */

public float getVendorFloatSensor(int sensor, float defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Returns the value of the given integer sensor, if present in this frame.
 * Returns null otherwise.

 * @param sensor Value is {@link android.car.diagnostic.IntegerSensorIndex#FUEL_SYSTEM_STATUS}, {@link android.car.diagnostic.IntegerSensorIndex#MALFUNCTION_INDICATOR_LIGHT_ON}, {@link android.car.diagnostic.IntegerSensorIndex#IGNITION_MONITORS_SUPPORTED}, {@link android.car.diagnostic.IntegerSensorIndex#IGNITION_SPECIFIC_MONITORS}, {@link android.car.diagnostic.IntegerSensorIndex#INTAKE_AIR_TEMPERATURE}, {@link android.car.diagnostic.IntegerSensorIndex#COMMANDED_SECONDARY_AIR_STATUS}, {@link android.car.diagnostic.IntegerSensorIndex#NUM_OXYGEN_SENSORS_PRESENT}, {@link android.car.diagnostic.IntegerSensorIndex#RUNTIME_SINCE_ENGINE_START}, {@link android.car.diagnostic.IntegerSensorIndex#DISTANCE_TRAVELED_WITH_MALFUNCTION_INDICATOR_LIGHT_ON}, {@link android.car.diagnostic.IntegerSensorIndex#WARMUPS_SINCE_CODES_CLEARED}, {@link android.car.diagnostic.IntegerSensorIndex#DISTANCE_TRAVELED_SINCE_CODES_CLEARED}, {@link android.car.diagnostic.IntegerSensorIndex#ABSOLUTE_BAROMETRIC_PRESSURE}, {@link android.car.diagnostic.IntegerSensorIndex#CONTROL_MODULE_VOLTAGE}, {@link android.car.diagnostic.IntegerSensorIndex#AMBIENT_AIR_TEMPERATURE}, {@link android.car.diagnostic.IntegerSensorIndex#TIME_WITH_MALFUNCTION_LIGHT_ON}, {@link android.car.diagnostic.IntegerSensorIndex#TIME_SINCE_TROUBLE_CODES_CLEARED}, {@link android.car.diagnostic.IntegerSensorIndex#MAX_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.IntegerSensorIndex#MAX_OXYGEN_SENSOR_VOLTAGE}, {@link android.car.diagnostic.IntegerSensorIndex#MAX_OXYGEN_SENSOR_CURRENT}, {@link android.car.diagnostic.IntegerSensorIndex#MAX_INTAKE_MANIFOLD_ABSOLUTE_PRESSURE}, {@link android.car.diagnostic.IntegerSensorIndex#MAX_AIR_FLOW_RATE_FROM_MASS_AIR_FLOW_SENSOR}, {@link android.car.diagnostic.IntegerSensorIndex#FUEL_TYPE}, {@link android.car.diagnostic.IntegerSensorIndex#FUEL_RAIL_ABSOLUTE_PRESSURE}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_OIL_TEMPERATURE}, {@link android.car.diagnostic.IntegerSensorIndex#DRIVER_DEMAND_PERCENT_TORQUE}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_ACTUAL_PERCENT_TORQUE}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_REFERENCE_PERCENT_TORQUE}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_PERCENT_TORQUE_DATA_IDLE}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_PERCENT_TORQUE_DATA_POINT1}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_PERCENT_TORQUE_DATA_POINT2}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_PERCENT_TORQUE_DATA_POINT3}, {@link android.car.diagnostic.IntegerSensorIndex#ENGINE_PERCENT_TORQUE_DATA_POINT4}, {@link android.car.diagnostic.IntegerSensorIndex#LAST_SYSTEM}, or {@link android.car.diagnostic.IntegerSensorIndex#VENDOR_START}
 */

public java.lang.Integer getSystemIntegerSensor(int sensor) { throw new RuntimeException("Stub!"); }

/**
 * Returns the value of the given float sensor, if present in this frame.
 * Returns null otherwise.

 * @param sensor Value is {@link android.car.diagnostic.FloatSensorIndex#CALCULATED_ENGINE_LOAD}, {@link android.car.diagnostic.FloatSensorIndex#ENGINE_COOLANT_TEMPERATURE}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_FUEL_TRIM_BANK1}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_FUEL_TRIM_BANK1}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_FUEL_TRIM_BANK2}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_FUEL_TRIM_BANK2}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#INTAKE_MANIFOLD_ABSOLUTE_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#ENGINE_RPM}, {@link android.car.diagnostic.FloatSensorIndex#VEHICLE_SPEED}, {@link android.car.diagnostic.FloatSensorIndex#TIMING_ADVANCE}, {@link android.car.diagnostic.FloatSensorIndex#MAF_AIR_FLOW_RATE}, {@link android.car.diagnostic.FloatSensorIndex#THROTTLE_POSITION}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR1_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR1_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR1_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR2_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR2_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR2_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR3_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR3_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR3_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR4_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR4_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR4_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR5_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR5_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR5_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR6_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR6_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR6_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR7_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR7_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR7_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR8_VOLTAGE}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR8_SHORT_TERM_FUEL_TRIM}, {@link android.car.diagnostic.FloatSensorIndex#OXYGEN_SENSOR8_FUEL_AIR_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_RAIL_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_RAIL_GAUGE_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#COMMANDED_EXHAUST_GAS_RECIRCULATION}, {@link android.car.diagnostic.FloatSensorIndex#EXHAUST_GAS_RECIRCULATION_ERROR}, {@link android.car.diagnostic.FloatSensorIndex#COMMANDED_EVAPORATIVE_PURGE}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_TANK_LEVEL_INPUT}, {@link android.car.diagnostic.FloatSensorIndex#EVAPORATION_SYSTEM_VAPOR_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#CATALYST_TEMPERATURE_BANK1_SENSOR1}, {@link android.car.diagnostic.FloatSensorIndex#CATALYST_TEMPERATURE_BANK2_SENSOR1}, {@link android.car.diagnostic.FloatSensorIndex#CATALYST_TEMPERATURE_BANK1_SENSOR2}, {@link android.car.diagnostic.FloatSensorIndex#CATALYST_TEMPERATURE_BANK2_SENSOR2}, {@link android.car.diagnostic.FloatSensorIndex#ABSOLUTE_LOAD_VALUE}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_AIR_COMMANDED_EQUIVALENCE_RATIO}, {@link android.car.diagnostic.FloatSensorIndex#RELATIVE_THROTTLE_POSITION}, {@link android.car.diagnostic.FloatSensorIndex#ABSOLUTE_THROTTLE_POSITION_B}, {@link android.car.diagnostic.FloatSensorIndex#ABSOLUTE_THROTTLE_POSITION_C}, {@link android.car.diagnostic.FloatSensorIndex#ACCELERATOR_PEDAL_POSITION_D}, {@link android.car.diagnostic.FloatSensorIndex#ACCELERATOR_PEDAL_POSITION_E}, {@link android.car.diagnostic.FloatSensorIndex#ACCELERATOR_PEDAL_POSITION_F}, {@link android.car.diagnostic.FloatSensorIndex#COMMANDED_THROTTLE_ACTUATOR}, {@link android.car.diagnostic.FloatSensorIndex#ETHANOL_FUEL_PERCENTAGE}, {@link android.car.diagnostic.FloatSensorIndex#ABSOLUTE_EVAPORATION_SYSTEM_VAPOR_PRESSURE}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK1}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK2}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK3}, {@link android.car.diagnostic.FloatSensorIndex#SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK4}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK1}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK2}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK3}, {@link android.car.diagnostic.FloatSensorIndex#LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK4}, {@link android.car.diagnostic.FloatSensorIndex#RELATIVE_ACCELERATOR_PEDAL_POSITION}, {@link android.car.diagnostic.FloatSensorIndex#HYBRID_BATTERY_PACK_REMAINING_LIFE}, {@link android.car.diagnostic.FloatSensorIndex#FUEL_INJECTION_TIMING}, {@link android.car.diagnostic.FloatSensorIndex#ENGINE_FUEL_RATE}, {@link android.car.diagnostic.FloatSensorIndex#LAST_SYSTEM}, or {@link android.car.diagnostic.FloatSensorIndex#VENDOR_START}
 */

public java.lang.Float getSystemFloatSensor(int sensor) { throw new RuntimeException("Stub!"); }

/**
 * Returns the value of the given integer sensor, if present in this frame.
 * Returns null otherwise.
 */

public java.lang.Integer getVendorIntegerSensor(int sensor) { throw new RuntimeException("Stub!"); }

/**
 * Returns the value of the given float sensor, if present in this frame.
 * Returns null otherwise.
 */

public java.lang.Float getVendorFloatSensor(int sensor) { throw new RuntimeException("Stub!"); }

/**
 * Returns the state of the fuel system, if present in this frame.
 * Returns null otherwise.

 * @return Value is {@link android.car.diagnostic.CarDiagnosticEvent.FuelSystemStatus#OPEN_INSUFFICIENT_ENGINE_TEMPERATURE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelSystemStatus#CLOSED_LOOP}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelSystemStatus#OPEN_ENGINE_LOAD_OR_DECELERATION}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelSystemStatus#OPEN_SYSTEM_FAILURE}, or {@link android.car.diagnostic.CarDiagnosticEvent.FuelSystemStatus#CLOSED_LOOP_BUT_FEEDBACK_FAULT}
 */

public java.lang.Integer getFuelSystemStatus() { throw new RuntimeException("Stub!"); }

/**
 * Returns the state of the secondary air system, if present in this frame.
 * Returns null otherwise.

 * @return Value is {@link android.car.diagnostic.CarDiagnosticEvent.SecondaryAirStatus#UPSTREAM}, {@link android.car.diagnostic.CarDiagnosticEvent.SecondaryAirStatus#DOWNSTREAM_OF_CATALYCIC_CONVERTER}, {@link android.car.diagnostic.CarDiagnosticEvent.SecondaryAirStatus#FROM_OUTSIDE_OR_OFF}, or {@link android.car.diagnostic.CarDiagnosticEvent.SecondaryAirStatus#PUMP_ON_FOR_DIAGNOSTICS}
 */

public java.lang.Integer getSecondaryAirStatus() { throw new RuntimeException("Stub!"); }

/**
 * Returns data about the ignition monitors, if present in this frame.
 * Returns null otherwise.
 */

public android.car.diagnostic.CarDiagnosticEvent.CommonIgnitionMonitors getIgnitionMonitors() { throw new RuntimeException("Stub!"); }

/**
 * Returns the fuel type, if present in this frame.
 * Returns null otherwise.

 * @return Value is {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#NOT_AVAILABLE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#GASOLINE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#METHANOL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#ETHANOL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#DIESEL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#LPG}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#CNG}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#PROPANE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#ELECTRIC}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_GASOLINE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_METHANOL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_ETHANOL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_LPG}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_CNG}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_PROPANE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_ELECTRIC}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_ELECTRIC_AND_COMBUSTION}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_GASOLINE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_ETHANOL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_DIESEL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_ELECTRIC}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_RUNNING_ELECTRIC_AND_COMBUSTION}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_REGENERATIVE}, or {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_DIESEL}
 */

public java.lang.Integer getFuelType() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.diagnostic.CarDiagnosticEvent> CREATOR;
static { CREATOR = null; }

/**
 * Diagnostic Troubleshooting Code (DTC) that was detected and caused this frame to be stored
 * (if a freeze frame). Always null for a live frame.
 */

public final java.lang.String dtc;
{ dtc = null; }

/** Whether this frame represents a live or a freeze frame */

public final int frameType;
{ frameType = 0; }

/**
 * When this data was acquired in car or received from car. It is elapsed real-time of data
 * reception from car in nanoseconds since system boot.
 */

public final long timestamp;
{ timestamp = 0; }
/**
 * This class can be used to incrementally construct a CarDiagnosticEvent.
 * CarDiagnosticEvent instances are immutable once built.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

Builder(int type) { throw new RuntimeException("Stub!"); }

/** Returns a new Builder for a live frame */

public static android.car.diagnostic.CarDiagnosticEvent.Builder newLiveFrameBuilder() { throw new RuntimeException("Stub!"); }

/** Returns a new Builder for a freeze frame */

public static android.car.diagnostic.CarDiagnosticEvent.Builder newFreezeFrameBuilder() { throw new RuntimeException("Stub!"); }

/**
 * Sets the timestamp for the frame being built
 * @deprecated Use {@link Builder#setTimeStamp(long)} instead.
 */

@Deprecated
public android.car.diagnostic.CarDiagnosticEvent.Builder atTimestamp(long timestamp) { throw new RuntimeException("Stub!"); }

/**
 * Sets the timestamp for the frame being built
 * @param timeStamp timeStamp for CarDiagnosticEvent
 * @return Builder
 */

public android.car.diagnostic.CarDiagnosticEvent.Builder setTimeStamp(long timeStamp) { throw new RuntimeException("Stub!"); }

/**
 * Adds an integer-valued sensor to the frame being built
 * @deprecated Use {@link Builder#setIntValue(int, int)} instead.
 */

@Deprecated
public android.car.diagnostic.CarDiagnosticEvent.Builder withIntValue(int key, int value) { throw new RuntimeException("Stub!"); }

/**
 * Adds an integer-valued sensor to the frame being built
 * @param key key of integer value
 * @param value int value
 * @return Builder
 */

public android.car.diagnostic.CarDiagnosticEvent.Builder setIntValue(int key, int value) { throw new RuntimeException("Stub!"); }

/**
 * Adds a float-valued sensor to the frame being built
 * @deprecated Use {@link Builder#setFloatValue(int, float)} instead.
 */

@Deprecated
public android.car.diagnostic.CarDiagnosticEvent.Builder withFloatValue(int key, float value) { throw new RuntimeException("Stub!"); }

/**
 * Adds a float-valued sensor to the frame being built
 * @param key key of float value
 * @param value float value
 * @return Builder
 */

public android.car.diagnostic.CarDiagnosticEvent.Builder setFloatValue(int key, float value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the DTC for the frame being built
 * @deprecated Use {@link Builder#setDtc(String)} instead.
 */

@Deprecated
public android.car.diagnostic.CarDiagnosticEvent.Builder withDtc(java.lang.String dtc) { throw new RuntimeException("Stub!"); }

/**
 * Sets the DTC for the frame being built
 * @param dtc string value of CarDiagnosticEvent
 * @return Builder
 */

public android.car.diagnostic.CarDiagnosticEvent.Builder setDtc(java.lang.String dtc) { throw new RuntimeException("Stub!"); }

/** Builds and returns the CarDiagnosticEvent */

public android.car.diagnostic.CarDiagnosticEvent build() { throw new RuntimeException("Stub!"); }
}

/**
 * Contains information about ignition monitors common to all vehicle types.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class CommonIgnitionMonitors {

CommonIgnitionMonitors(int bitmask) { throw new RuntimeException("Stub!"); }

/**
 * Returns data about ignition monitors specific to spark vehicles, if this
 * object represents ignition monitors for a spark vehicle.
 * Returns null otherwise.
 */

public android.car.diagnostic.CarDiagnosticEvent.SparkIgnitionMonitors asSparkIgnitionMonitors() { throw new RuntimeException("Stub!"); }

/**
 * Returns data about ignition monitors specific to compression vehicles, if this
 * object represents ignition monitors for a compression vehicle.
 * Returns null otherwise.
 */

public android.car.diagnostic.CarDiagnosticEvent.CompressionIgnitionMonitors asCompressionIgnitionMonitors() { throw new RuntimeException("Stub!"); }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor components;
{ components = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor fuelSystem;
{ fuelSystem = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor misfire;
{ misfire = null; }
}

/**
 * Contains information about ignition monitors specific to compression vehicles.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class CompressionIgnitionMonitors extends android.car.diagnostic.CarDiagnosticEvent.CommonIgnitionMonitors {

CompressionIgnitionMonitors(int bitmask) { super(0); throw new RuntimeException("Stub!"); }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor EGROrVVT;
{ EGROrVVT = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor NMHCCatalyst;
{ NMHCCatalyst = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor NOxSCR;
{ NOxSCR = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor PMFilter;
{ PMFilter = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor boostPressure;
{ boostPressure = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor exhaustGasSensor;
{ exhaustGasSensor = null; }
}

/**
 * Represents possible states of the fuel system; see {@link
 * android.car.diagnostic.IntegerSensorIndex#FUEL_SYSTEM_STATUS}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class FuelSystemStatus {

FuelSystemStatus() { throw new RuntimeException("Stub!"); }

public static final int CLOSED_LOOP = 2; // 0x2

public static final int CLOSED_LOOP_BUT_FEEDBACK_FAULT = 16; // 0x10

public static final int OPEN_ENGINE_LOAD_OR_DECELERATION = 4; // 0x4

public static final int OPEN_INSUFFICIENT_ENGINE_TEMPERATURE = 1; // 0x1

public static final int OPEN_SYSTEM_FAILURE = 8; // 0x8
/**
 * Value is {@link android.car.diagnostic.CarDiagnosticEvent.FuelSystemStatus#OPEN_INSUFFICIENT_ENGINE_TEMPERATURE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelSystemStatus#CLOSED_LOOP}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelSystemStatus#OPEN_ENGINE_LOAD_OR_DECELERATION}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelSystemStatus#OPEN_SYSTEM_FAILURE}, or {@link android.car.diagnostic.CarDiagnosticEvent.FuelSystemStatus#CLOSED_LOOP_BUT_FEEDBACK_FAULT}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface Status {
}

}

/**
 * Represents possible types of fuel; see {@link
 * android.car.diagnostic.IntegerSensorIndex#FUEL_TYPE}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class FuelType {

FuelType() { throw new RuntimeException("Stub!"); }

public static final int BIFUEL_RUNNING_CNG = 13; // 0xd

public static final int BIFUEL_RUNNING_DIESEL = 23; // 0x17

public static final int BIFUEL_RUNNING_ELECTRIC = 15; // 0xf

public static final int BIFUEL_RUNNING_ELECTRIC_AND_COMBUSTION = 16; // 0x10

public static final int BIFUEL_RUNNING_ETHANOL = 11; // 0xb

public static final int BIFUEL_RUNNING_GASOLINE = 9; // 0x9

public static final int BIFUEL_RUNNING_LPG = 12; // 0xc

public static final int BIFUEL_RUNNING_METHANOL = 10; // 0xa

public static final int BIFUEL_RUNNING_PROPANE = 14; // 0xe

public static final int CNG = 6; // 0x6

public static final int DIESEL = 4; // 0x4

public static final int ELECTRIC = 8; // 0x8

public static final int ETHANOL = 3; // 0x3

public static final int GASOLINE = 1; // 0x1

public static final int HYBRID_DIESEL = 19; // 0x13

public static final int HYBRID_ELECTRIC = 20; // 0x14

public static final int HYBRID_ETHANOL = 18; // 0x12

public static final int HYBRID_GASOLINE = 17; // 0x11

public static final int HYBRID_REGENERATIVE = 22; // 0x16

public static final int HYBRID_RUNNING_ELECTRIC_AND_COMBUSTION = 21; // 0x15

public static final int LPG = 5; // 0x5

public static final int METHANOL = 2; // 0x2

public static final int NOT_AVAILABLE = 0; // 0x0

public static final int PROPANE = 7; // 0x7
/**
 * Value is {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#NOT_AVAILABLE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#GASOLINE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#METHANOL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#ETHANOL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#DIESEL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#LPG}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#CNG}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#PROPANE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#ELECTRIC}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_GASOLINE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_METHANOL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_ETHANOL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_LPG}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_CNG}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_PROPANE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_ELECTRIC}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_ELECTRIC_AND_COMBUSTION}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_GASOLINE}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_ETHANOL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_DIESEL}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_ELECTRIC}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_RUNNING_ELECTRIC_AND_COMBUSTION}, {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#HYBRID_REGENERATIVE}, or {@link android.car.diagnostic.CarDiagnosticEvent.FuelType#BIFUEL_RUNNING_DIESEL}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface Type {
}

}

/**
 * Represents the state of an ignition monitor on a vehicle.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class IgnitionMonitor {

IgnitionMonitor(boolean available, boolean incomplete) { throw new RuntimeException("Stub!"); }

public final boolean available;
{ available = false; }

public final boolean incomplete;
{ incomplete = false; }
}

/**
 * Represents possible states of the secondary air system; see {@link
 * android.car.diagnostic.IntegerSensorIndex#COMMANDED_SECONDARY_AIR_STATUS}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SecondaryAirStatus {

SecondaryAirStatus() { throw new RuntimeException("Stub!"); }

public static final int DOWNSTREAM_OF_CATALYCIC_CONVERTER = 2; // 0x2

public static final int FROM_OUTSIDE_OR_OFF = 4; // 0x4

public static final int PUMP_ON_FOR_DIAGNOSTICS = 8; // 0x8

public static final int UPSTREAM = 1; // 0x1
/**
 * Value is {@link android.car.diagnostic.CarDiagnosticEvent.SecondaryAirStatus#UPSTREAM}, {@link android.car.diagnostic.CarDiagnosticEvent.SecondaryAirStatus#DOWNSTREAM_OF_CATALYCIC_CONVERTER}, {@link android.car.diagnostic.CarDiagnosticEvent.SecondaryAirStatus#FROM_OUTSIDE_OR_OFF}, or {@link android.car.diagnostic.CarDiagnosticEvent.SecondaryAirStatus#PUMP_ON_FOR_DIAGNOSTICS}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface Status {
}

}

/**
 * Contains information about ignition monitors specific to spark vehicles.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SparkIgnitionMonitors extends android.car.diagnostic.CarDiagnosticEvent.CommonIgnitionMonitors {

SparkIgnitionMonitors(int bitmask) { super(0); throw new RuntimeException("Stub!"); }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor ACRefrigerant;
{ ACRefrigerant = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor EGR;
{ EGR = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor catalyst;
{ catalyst = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor evaporativeSystem;
{ evaporativeSystem = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor heatedCatalyst;
{ heatedCatalyst = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor oxygenSensor;
{ oxygenSensor = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor oxygenSensorHeater;
{ oxygenSensorHeater = null; }

public final android.car.diagnostic.CarDiagnosticEvent.IgnitionMonitor secondaryAirSystem;
{ secondaryAirSystem = null; }
}

}

