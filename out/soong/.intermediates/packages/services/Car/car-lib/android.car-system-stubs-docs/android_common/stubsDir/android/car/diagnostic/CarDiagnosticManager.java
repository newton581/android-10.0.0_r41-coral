/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.diagnostic;


/**
 * API for monitoring car diagnostic data.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarDiagnosticManager {

/** @hide */

CarDiagnosticManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

public void onCarDisconnected() { throw new RuntimeException("Stub!"); }

/**
 * Register a new listener for events of a given frame type and rate.
 * @param listener
 * @param frameType
 * Value is {@link android.car.diagnostic.CarDiagnosticManager#FRAME_TYPE_LIVE}, or {@link android.car.diagnostic.CarDiagnosticManager#FRAME_TYPE_FREEZE}
 * @param rate
 * @return true if the registration was successful; false otherwise
 * @throws IllegalArgumentException
 */

public boolean registerListener(android.car.diagnostic.CarDiagnosticManager.OnDiagnosticEventListener listener, int frameType, int rate) { throw new RuntimeException("Stub!"); }

/**
 * Unregister a listener, causing it to stop receiving all diagnostic events.
 * @param listener
 */

public void unregisterListener(android.car.diagnostic.CarDiagnosticManager.OnDiagnosticEventListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the most-recently acquired live frame data from the car.
 * @return A CarDiagnostic event for the most recently known live frame if one is present.
 *         null if no live frame has been recorded by the vehicle.
 */

public android.car.diagnostic.CarDiagnosticEvent getLatestLiveFrame() { throw new RuntimeException("Stub!"); }

/**
 * Return the list of the timestamps for which a freeze frame is currently stored.
 * @return An array containing timestamps at which, at the current time, the vehicle has
 *         a freeze frame stored. If no freeze frames are currently stored, an empty
 *         array will be returned.
 * Because vehicles might have a limited amount of storage for frames, clients cannot
 * assume that a timestamp obtained via this call will be indefinitely valid for retrieval
 * of the actual diagnostic data, and must be prepared to handle a missing frame.
 */

public long[] getFreezeFrameTimestamps() { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the freeze frame event data for a given timestamp, if available.
 * @param timestamp
 * @return A CarDiagnostic event for the frame at the given timestamp, if one is
 *         available. null is returned otherwise.
 * Storage constraints might cause frames to be deleted from vehicle memory.
 * For this reason it cannot be assumed that a timestamp will yield a valid frame,
 * even if it was initially obtained via a call to getFreezeFrameTimestamps().
 */

public android.car.diagnostic.CarDiagnosticEvent getFreezeFrame(long timestamp) { throw new RuntimeException("Stub!"); }

/**
 * Clear the freeze frame information from vehicle memory at the given timestamps.
 * @param timestamps A list of timestamps to delete freeze frames at, or an empty array
 *                   to delete all freeze frames from vehicle memory.
 * @return true if all the required frames were deleted (including if no timestamps are
 *         provided and all frames were cleared); false otherwise.
 * Due to storage constraints, timestamps cannot be assumed to be indefinitely valid, and
 * a false return from this method should be used by the client as cause for invalidating
 * its local knowledge of the vehicle diagnostic state.
 */

public boolean clearFreezeFrames(long... timestamps) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if this vehicle supports sending live frame information.
 * @return
 */

public boolean isLiveFrameSupported() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if this vehicle supports supports sending notifications to
 * registered listeners when new freeze frames happen.
 */

public boolean isFreezeFrameNotificationSupported() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the underlying HAL supports retrieving freeze frames
 * stored in vehicle memory using timestamp.
 */

public boolean isGetFreezeFrameSupported() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if this vehicle supports clearing all freeze frames.
 * This is only meaningful if freeze frame data is also supported.
 *
 * A return value of true for this method indicates that it is supported to call
 * carDiagnosticManager.clearFreezeFrames()
 * to delete all freeze frames stored in vehicle memory.
 *
 * @return
 */

public boolean isClearFreezeFramesSupported() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if this vehicle supports clearing specific freeze frames by timestamp.
 * This is only meaningful if freeze frame data is also supported.
 *
 * A return value of true for this method indicates that it is supported to call
 * carDiagnosticManager.clearFreezeFrames(timestamp1, timestamp2, ...)
 * to delete the freeze frames stored for the provided input timestamps, provided any exist.
 *
 * @return
 */

public boolean isSelectiveClearFreezeFramesSupported() { throw new RuntimeException("Stub!"); }

public static final int FRAME_TYPE_FREEZE = 1; // 0x1

public static final int FRAME_TYPE_LIVE = 0; // 0x0
/**
 * Value is {@link android.car.diagnostic.CarDiagnosticManager#FRAME_TYPE_LIVE}, or {@link android.car.diagnostic.CarDiagnosticManager#FRAME_TYPE_FREEZE}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface FrameType {
}

/** Listener for diagnostic events. Callbacks are called in the Looper context. */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnDiagnosticEventListener {

/**
 * Called when there is a diagnostic event from the car.
 *
 * @param carDiagnosticEvent
 */

public void onDiagnosticEvent(android.car.diagnostic.CarDiagnosticEvent carDiagnosticEvent);
}

}

