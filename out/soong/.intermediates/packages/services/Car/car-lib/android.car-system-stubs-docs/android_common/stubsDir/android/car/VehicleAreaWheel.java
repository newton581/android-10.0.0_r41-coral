/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car;


/**
 * VehicleAreaWheel is an abstraction for the wheels on a car.  It exists to isolate the java APIs
 * from the VHAL definitions.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VehicleAreaWheel {

VehicleAreaWheel() { throw new RuntimeException("Stub!"); }

public static final int WHEEL_LEFT_FRONT = 1; // 0x1

public static final int WHEEL_LEFT_REAR = 4; // 0x4

public static final int WHEEL_RIGHT_FRONT = 2; // 0x2

public static final int WHEEL_RIGHT_REAR = 8; // 0x8

public static final int WHEEL_UNKNOWN = 0; // 0x0
}

