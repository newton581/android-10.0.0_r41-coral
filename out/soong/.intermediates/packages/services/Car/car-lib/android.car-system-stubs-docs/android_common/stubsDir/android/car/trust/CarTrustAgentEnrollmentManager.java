/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.trust;

import android.car.Car;
import android.bluetooth.BluetoothDevice;
import android.os.Handler;

/**
 * APIs to help enroll a remote device as a trusted device that can be used to authenticate a user
 * in the head unit.
 * <p>
 * The call sequence to add a new trusted device from the client should be as follows:
 * <ol>
 * <li> setEnrollmentCallback()
 * <li> setBleCallback(bleCallback)
 * <li> startEnrollmentAdvertising()
 * <li> wait for onEnrollmentAdvertisingStarted() or
 * <li> wait for onBleEnrollmentDeviceConnected() and check if the device connected is the right
 * one.
 * <li> initiateEnrollmentHandshake()
 * <li> wait for onAuthStringAvailable() to get the pairing code to display to the user
 * <li> enrollmentHandshakeAccepted() after user confirms the pairing code
 * <li> wait for onEscrowTokenAdded()
 * <li> Authenticate user's credentials by showing the lock screen
 * <li> activateToken()
 * <li> wait for onEscrowTokenActiveStateChanged() to add the device as a trusted device and show
 * in the list
 * </ol>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarTrustAgentEnrollmentManager {

/** @hide */

CarTrustAgentEnrollmentManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Starts broadcasting enrollment UUID on BLE.
 * Phones can scan and connect for the enrollment process to begin.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 */

public void startEnrollmentAdvertising() { throw new RuntimeException("Stub!"); }

/**
 * Stops Enrollment advertising.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 */

public void stopEnrollmentAdvertising() { throw new RuntimeException("Stub!"); }

/**
 * Confirms that the enrollment handshake has been accepted by the user. This should be called
 * after the user has confirmed the verification code displayed on the UI.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 * @param device the remote Bluetooth device that will receive the signal.
 */

public void enrollmentHandshakeAccepted(android.bluetooth.BluetoothDevice device) { throw new RuntimeException("Stub!"); }

/**
 * Provides an option to quit enrollment if the pairing code doesn't match for example.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 */

public void terminateEnrollmentHandshake() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the escrow token associated with the given handle is active.
 * <p>
 * When a new escrow token has been added as part of the Trusted device enrollment, the client
 * will receive {@link CarTrustAgentEnrollmentCallback#onEscrowTokenAdded(long)} and
 * {@link CarTrustAgentEnrollmentCallback#onEscrowTokenActiveStateChanged(long, boolean)}
 * callbacks.  This method provides a way to query for the token state at a later point of time.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 * @param handle the handle corresponding to the escrow token
 * @param uid    user id associated with the token
 * @return true if the token is active, false if not
 */

public boolean isEscrowTokenActive(long handle, int uid) { throw new RuntimeException("Stub!"); }

/**
 * Remove the escrow token that is associated with the given handle and uid.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 * @param handle the handle associated with the escrow token
 * @param uid    user id associated with the token
 */

public void removeEscrowToken(long handle, int uid) { throw new RuntimeException("Stub!"); }

/**
 * Remove all of the trusted devices associated with the given user.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 * @param uid User id to remove the devices for
 */

public void removeAllTrustedDevices(int uid) { throw new RuntimeException("Stub!"); }

/**
 * Enable or Disable Trusted device enrollment.  Once disabled, head unit will not broadcast
 * for enrollment until enabled back.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 * @param isEnabled {@code true} enables enrollment.
 */

public void setTrustedDeviceEnrollmentEnabled(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Enable or disable Unlocking with a trusted device. Once disabled, head unit will not
 * broadcast until enabled back.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 * @param isEnabled {@code true} enables unlock.
 */

public void setTrustedDeviceUnlockEnabled(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Register for enrollment event callbacks.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 * @param callback The callback methods to call, null to unregister
 */

public void setEnrollmentCallback(android.car.trust.CarTrustAgentEnrollmentManager.CarTrustAgentEnrollmentCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Register for general BLE callbacks
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 * @param callback The callback methods to call, null to unregister
 */

public void setBleCallback(android.car.trust.CarTrustAgentEnrollmentManager.CarTrustAgentBleCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Provides a list that contains information about the enrolled devices for the given user id.
 * <p>
 * Each enrollment handle corresponds to a trusted device for the given user.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_ENROLL_TRUST}
 * @param uid user id.
 * @return list of the Enrollment handles and user names for the user id.
 */

public java.util.List<android.car.trust.TrustedDeviceInfo> getEnrolledDeviceInfoForUser(int uid) { throw new RuntimeException("Stub!"); }

/**
 * Enrollment Handshake failed.
 */

public static final int ENROLLMENT_HANDSHAKE_FAILURE = 1; // 0x1

/**
 * Enrollment of a new device is not allowed.  This happens when either the whole feature is
 * disabled or just the enrollment is disabled.  Useful when feature needs to be disabled
 * in a lost/stolen phone scenario.
 */

public static final int ENROLLMENT_NOT_ALLOWED = 2; // 0x2
/**
 * Callback interface for Trusted device enrollment applications to implement.  The applications
 * get notified on various BLE state change events that happen during trusted device enrollment.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface CarTrustAgentBleCallback {

/**
 * Indicates a remote device connected on BLE.
 */

public void onBleEnrollmentDeviceConnected(android.bluetooth.BluetoothDevice device);

/**
 * Indicates a remote device disconnected on BLE.
 */

public void onBleEnrollmentDeviceDisconnected(android.bluetooth.BluetoothDevice device);

/**
 * Indicates that the device is broadcasting for trusted device enrollment on BLE.
 */

public void onEnrollmentAdvertisingStarted();

/**
 * Indicates a failure in BLE broadcasting for enrollment.
 */

public void onEnrollmentAdvertisingFailed();
}

/**
 * Callback interface for Trusted device enrollment applications to implement.  The applications
 * get notified on various enrollment state change events.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface CarTrustAgentEnrollmentCallback {

/**
 * Communicate about failure/timeouts in the handshake process.  BluetoothDevice will be
 * null when the returned error code is {@link #ENROLLMENT_NOT_ALLOWED}.
 *
 * @param device    the remote device trying to enroll
 * @param errorCode information on what failed.

 * Value is {@link android.car.trust.CarTrustAgentEnrollmentManager#ENROLLMENT_HANDSHAKE_FAILURE}, or {@link android.car.trust.CarTrustAgentEnrollmentManager#ENROLLMENT_NOT_ALLOWED}
 */

public void onEnrollmentHandshakeFailure(android.bluetooth.BluetoothDevice device, int errorCode);

/**
 * Present the pairing/authentication string to the user.
 *
 * @param device     the remote device trying to enroll
 * @param authString the authentication string to show to the user to confirm across
 *                   both devices
 */

public void onAuthStringAvailable(android.bluetooth.BluetoothDevice device, java.lang.String authString);

/**
 * Escrow token was received and the Trust Agent framework has generated a corresponding
 * handle.
 *
 * @param handle the handle associated with the escrow token.
 */

public void onEscrowTokenAdded(long handle);

/**
 * Escrow token was removed as a result of a call to {@link #removeEscrowToken(long handle,
 * int uid)}. The peer device associated with this token is not trusted for authentication
 * anymore.
 *
 * @param handle the handle associated with the escrow token.
 */

public void onEscrowTokenRemoved(long handle);

/**
 * Escrow token's active state changed.
 *
 * @param handle the handle associated with the escrow token
 * @param active True if token has been activated, false if not.
 */

public void onEscrowTokenActiveStateChanged(long handle, boolean active);
}

}

