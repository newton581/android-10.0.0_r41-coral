/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car;


/**
 * Units used for int or float type with no attached enum types.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VehicleUnit {

VehicleUnit() { throw new RuntimeException("Stub!"); }

public static final int AMPERE_HOURS = 100; // 0x64

public static final int BAR = 114; // 0x72

public static final int CELSIUS = 48; // 0x30

public static final int DEGREES = 128; // 0x80

public static final int FAHRENHEIT = 49; // 0x31

public static final int HERTZ = 3; // 0x3

public static final int IMPERIAL_GALLON = 67; // 0x43

public static final int KELVIN = 50; // 0x32

public static final int KILOMETER = 35; // 0x23

public static final int KILOPASCAL = 112; // 0x70

public static final int KILOWATT_HOUR = 101; // 0x65

public static final int LITER = 65; // 0x41

public static final int METER = 33; // 0x21

public static final int METER_PER_SEC = 1; // 0x1

public static final int MILE = 36; // 0x24

public static final int MILLIAMPERE = 97; // 0x61

public static final int MILLILITER = 64; // 0x40

public static final int MILLIMETER = 32; // 0x20

public static final int MILLIVOLT = 98; // 0x62

public static final int MILLIWATTS = 99; // 0x63

public static final int NANO_SECS = 80; // 0x50

public static final int PERCENTILE = 16; // 0x10

public static final int PSI = 113; // 0x71

public static final int RPM = 2; // 0x2

public static final int SECS = 83; // 0x53

/**
 * List of Unit Types from VHAL
 */

public static final int SHOULD_NOT_USE = 0; // 0x0

public static final int US_GALLON = 66; // 0x42

public static final int WATT_HOUR = 96; // 0x60

public static final int YEAR = 89; // 0x59
}

