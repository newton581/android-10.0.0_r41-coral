/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car;

import android.content.pm.PackageManager;
import android.content.Context;
import android.os.Handler;
import android.content.ContextWrapper;
import android.content.ServiceConnection;
import android.app.Service;
import android.car.hardware.CarSensorManager;
import android.car.hardware.property.CarPropertyManager;
import android.car.content.pm.CarPackageManager;
import android.car.media.CarAudioManager;
import android.car.navigation.CarNavigationStatusManager;
import android.car.cluster.CarInstrumentClusterManager;
import android.car.hardware.cabin.CarCabinManager;
import android.car.hardware.hvac.CarHvacManager;
import android.car.hardware.CarVendorExtensionManager;
import android.car.drivingstate.CarDrivingStateManager;
import android.car.drivingstate.CarUxRestrictionsManager;
import android.car.settings.CarConfigurationManager;
import android.car.trust.CarTrustAgentEnrollmentManager;

/**
 *   Top level car API for embedded Android Auto deployments.
 *   This API works only for devices with {@link PackageManager#FEATURE_AUTOMOTIVE}
 *   Calling this API on a device with no such feature will lead to an exception.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class Car {

Car() { throw new RuntimeException("Stub!"); }

/**
 * A factory method that creates Car instance for all Car API access.
 * @param context App's Context. This should not be null. If you are passing
 *                {@link ContextWrapper}, make sure that its base Context is non-null as well.
 *                Otherwise it will throw {@link java.lang.NullPointerException}.
 * @param serviceConnectionListener listener for monitoring service connection.
 * @param handler the handler on which the callback should execute, or null to execute on the
 * service's main thread. Note: the service connection listener will be always on the main
 * thread regardless of the handler given.
 * @return Car instance if system is in car environment and returns {@code null} otherwise.
 *
 * @deprecated use {@link #createCar(Context, Handler)} instead.
 */

@Deprecated
public static android.car.Car createCar(android.content.Context context, android.content.ServiceConnection serviceConnectionListener, android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * A factory method that creates Car instance for all Car API access using main thread {@code
 * Looper}.
 *
 * @see #createCar(Context, ServiceConnection, Handler)
 *
 * @deprecated use {@link #createCar(Context, Handler)} instead.
 */

@Deprecated
public static android.car.Car createCar(android.content.Context context, android.content.ServiceConnection serviceConnectionListener) { throw new RuntimeException("Stub!"); }

/**
 * Creates new {@link Car} object which connected synchronously to Car Service and ready to use.
 *
 * @param context application's context
 *
 * @return Car object if operation succeeded, otherwise null.
 */

public static android.car.Car createCar(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Creates new {@link Car} object which connected synchronously to Car Service and ready to use.
 *
 * @param context App's Context. This should not be null. If you are passing
 *                {@link ContextWrapper}, make sure that its base Context is non-null as well.
 *                Otherwise it will throw {@link java.lang.NullPointerException}.
 * @param handler the handler on which the manager's callbacks will be executed, or null to
 * execute on the application's main thread.
 *
 * @return Car object if operation succeeded, otherwise null.
 */

public static android.car.Car createCar(android.content.Context context, android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * Connect to car service. This can be called while it is disconnected.
 * @throws IllegalStateException If connection is still on-going from previous
 *         connect call or it is already connected
 *
 * @deprecated this method is not need if this object is created via
 * {@link #createCar(Context, Handler)}.
 */

@Deprecated
public void connect() throws java.lang.IllegalStateException { throw new RuntimeException("Stub!"); }

/**
 * Disconnect from car service. This can be called while disconnected. Once disconnect is
 * called, all Car*Managers from this instance becomes invalid, and
 * {@link Car#getCarManager(String)} will return different instance if it is connected again.
 */

public void disconnect() { throw new RuntimeException("Stub!"); }

/**
 * Tells if it is connected to the service or not. This will return false if it is still
 * connecting.
 * @return
 */

public boolean isConnected() { throw new RuntimeException("Stub!"); }

/**
 * Tells if this instance is already connecting to car service or not.
 * @return
 */

public boolean isConnecting() { throw new RuntimeException("Stub!"); }

/**
 * Get car specific service as in {@link Context#getSystemService(String)}. Returned
 * {@link Object} should be type-casted to the desired service.
 * For example, to get sensor service,
 * SensorManagerService sensorManagerService = car.getCarManager(Car.SENSOR_SERVICE);
 * @param serviceName Name of service that should be created like {@link #SENSOR_SERVICE}.
 * @return Matching service manager or null if there is no such service.
 */

public java.lang.Object getCarManager(java.lang.String serviceName) { throw new RuntimeException("Stub!"); }

/**
 * Return the type of currently connected car.
 * @return

 * Value is {@link android.car.Car#CONNECTION_TYPE_EMBEDDED}
 */

public int getCarConnectionType() { throw new RuntimeException("Stub!"); }

/** Service name for {@link CarAppFocusManager}. */

public static final java.lang.String APP_FOCUS_SERVICE = "app_focus";

/** Service name for {@link CarAudioManager} */

public static final java.lang.String AUDIO_SERVICE = "audio";

/**
 * Service name for {@link CarCabinManager}.
 *
 * @deprecated {@link CarCabinManager} is deprecated. Use {@link CarPropertyManager} instead.
 * @hide
 */

@Deprecated public static final java.lang.String CABIN_SERVICE = "cabin";

/**
 * Service name for {@link android.car.settings.CarConfigurationManager}
 */

public static final java.lang.String CAR_CONFIGURATION_SERVICE = "configuration";

/**
 * Service name for {@link CarDrivingStateManager}
 * @hide
 */

public static final java.lang.String CAR_DRIVING_STATE_SERVICE = "drivingstate";

/**
 * When an activity is launched in the cluster, it will receive {@link ClusterActivityState} in
 * the intent's extra under this key, containing instrument cluster information such as
 * unobscured area, visibility, etc.
 *
 * @hide
 */

public static final java.lang.String CAR_EXTRA_CLUSTER_ACTIVITY_STATE = "android.car.cluster.ClusterActivityState";

/**
 * Used as a string extra field with {@link #CAR_INTENT_ACTION_MEDIA_TEMPLATE} to specify the
 * media app that user wants to start the media on. Note: this is not the templated media app.
 *
 * This is being deprecated. Use {@link #CAR_EXTRA_MEDIA_COMPONENT} instead.
 */

public static final java.lang.String CAR_EXTRA_MEDIA_PACKAGE = "android.car.intent.extra.MEDIA_PACKAGE";

/**
 * Activity Action: Provide media playing through a media template app.
 * <p>Input: String extra mapped by {@link android.app.SearchManager#QUERY} is the query
 * used to start the media. String extra mapped by {@link #CAR_EXTRA_MEDIA_COMPONENT} is the
 * component name of the media app which user wants to play media on.
 * <p>Output: nothing.
 */

public static final java.lang.String CAR_INTENT_ACTION_MEDIA_TEMPLATE = "android.car.intent.action.MEDIA_TEMPLATE";

/** Service name for {@link CarNavigationStatusManager} */

public static final java.lang.String CAR_NAVIGATION_SERVICE = "car_navigation_service";

/**
 * Service name for {@link android.car.trust.CarTrustAgentEnrollmentManager}
 * @hide
 */

public static final java.lang.String CAR_TRUST_AGENT_ENROLLMENT_SERVICE = "trust_enroll";

/**
 * Service name for {@link CarUxRestrictionsManager}
 */

public static final java.lang.String CAR_UX_RESTRICTION_SERVICE = "uxrestriction";

/** Type of car connection: platform runs directly in car. */

public static final int CONNECTION_TYPE_EMBEDDED = 5; // 0x5

/**
 * @hide
 */

public static final java.lang.String DIAGNOSTIC_SERVICE = "diagnostic";

/**
 * Service name for {@link CarHvacManager}
 * @deprecated {@link CarHvacManager} is deprecated. Use {@link CarPropertyManager} instead.
 * @hide
 */

@Deprecated public static final java.lang.String HVAC_SERVICE = "hvac";

/** Service name for {@link CarInfoManager}, to be used in {@link #getCarManager(String)}. */

public static final java.lang.String INFO_SERVICE = "info";

/** Service name for {@link CarPackageManager} */

public static final java.lang.String PACKAGE_SERVICE = "package";

/**
 * Permission necessary to change car audio settings through {@link CarAudioManager}.
 */

public static final java.lang.String PERMISSION_CAR_CONTROL_AUDIO_SETTINGS = "android.car.permission.CAR_CONTROL_AUDIO_SETTINGS";

/**
 * Permission necessary to change car audio volume through {@link CarAudioManager}.
 */

public static final java.lang.String PERMISSION_CAR_CONTROL_AUDIO_VOLUME = "android.car.permission.CAR_CONTROL_AUDIO_VOLUME";

/**
 * Permissions necessary to clear diagnostic information.
 *
 * @hide
 */

public static final java.lang.String PERMISSION_CAR_DIAGNOSTIC_CLEAR = "android.car.permission.CLEAR_CAR_DIAGNOSTICS";

/**
 * Permissions necessary to read diagnostic information, including vendor-specific bits.
 *
 * @hide
 */

public static final java.lang.String PERMISSION_CAR_DIAGNOSTIC_READ_ALL = "android.car.permission.CAR_DIAGNOSTICS";

/**
 * Permission necessary to access CarDrivingStateService to get a Car's driving state.
 * @hide
 */

public static final java.lang.String PERMISSION_CAR_DRIVING_STATE = "android.car.permission.CAR_DRIVING_STATE";

/** Permission necessary to access car's dynamics state.
 *  @hide
 */

public static final java.lang.String PERMISSION_CAR_DYNAMICS_STATE = "android.car.permission.CAR_DYNAMICS_STATE";

/**
 * Permission necessary to access car's engine information.
 * @hide
 */

public static final java.lang.String PERMISSION_CAR_ENGINE_DETAILED = "android.car.permission.CAR_ENGINE_DETAILED";

/**
 * Permission necessary to enroll a device as a trusted authenticator device.
 *
 * @hide
 */

public static final java.lang.String PERMISSION_CAR_ENROLL_TRUST = "android.car.permission.CAR_ENROLL_TRUST";

/** Permission necessary to use {@link CarInfoManager}. */

public static final java.lang.String PERMISSION_CAR_INFO = "android.car.permission.CAR_INFO";

/**
 * Permission necessary to start activities in the instrument cluster through
 * {@link CarInstrumentClusterManager}
 *
 * @hide
 */

public static final java.lang.String PERMISSION_CAR_INSTRUMENT_CLUSTER_CONTROL = "android.car.permission.CAR_INSTRUMENT_CLUSTER_CONTROL";

/**
 * Permission necessary to use {@link CarNavigationStatusManager}.
 */

public static final java.lang.String PERMISSION_CAR_NAVIGATION_MANAGER = "android.car.permission.CAR_NAVIGATION_MANAGER";

/**
 * Permission necessary to access Car POWER APIs.
 * @hide
 */

public static final java.lang.String PERMISSION_CAR_POWER = "android.car.permission.CAR_POWER";

/**
 * Permission necessary to access Car PROJECTION system APIs.
 * @hide
 */

public static final java.lang.String PERMISSION_CAR_PROJECTION = "android.car.permission.CAR_PROJECTION";

/**
 * Permission necessary to access projection status.
 * @hide
 */

public static final java.lang.String PERMISSION_CAR_PROJECTION_STATUS = "android.car.permission.ACCESS_CAR_PROJECTION_STATUS";

/**
 * Permission necessary to access CarTestService.
 * @hide
 */

public static final java.lang.String PERMISSION_CAR_TEST_SERVICE = "android.car.permission.CAR_TEST_SERVICE";

/**
 * @hide
 */

public static final java.lang.String PERMISSION_CONTROL_APP_BLOCKING = "android.car.permission.CONTROL_APP_BLOCKING";

/**
 * Permission necessary to access Car HVAC APIs.
 * @hide
 */

public static final java.lang.String PERMISSION_CONTROL_CAR_CLIMATE = "android.car.permission.CONTROL_CAR_CLIMATE";

/**
 * Permission necessary to control car's door.
 * @hide
 */

public static final java.lang.String PERMISSION_CONTROL_CAR_DOORS = "android.car.permission.CONTROL_CAR_DOORS";

/**
 * Permission necessary to control car's mirrors.
 * @hide
 */

public static final java.lang.String PERMISSION_CONTROL_CAR_MIRRORS = "android.car.permission.CONTROL_CAR_MIRRORS";

/**
 * Permission necessary to control car's seats.
 * @hide
 */

public static final java.lang.String PERMISSION_CONTROL_CAR_SEATS = "android.car.permission.CONTROL_CAR_SEATS";

/**
 * Permission necessary to control car's windows.
 * @hide
 */

public static final java.lang.String PERMISSION_CONTROL_CAR_WINDOWS = "android.car.permission.CONTROL_CAR_WINDOWS";

/**
 * Permission necessary to control display units for distance, fuel volume, tire pressure
 * and ev battery.
 */

public static final java.lang.String PERMISSION_CONTROL_DISPLAY_UNITS = "android.car.permission.CONTROL_CAR_DISPLAY_UNITS";

/** Permission necessary to control car's exterior lights.
 *  @hide
 */

public static final java.lang.String PERMISSION_CONTROL_EXTERIOR_LIGHTS = "android.car.permission.CONTROL_CAR_EXTERIOR_LIGHTS";

/**
 * Permission necessary to control car's interior lights.
 */

public static final java.lang.String PERMISSION_CONTROL_INTERIOR_LIGHTS = "android.car.permission.CONTROL_CAR_INTERIOR_LIGHTS";

/** Permission necessary to access car's energy information. */

public static final java.lang.String PERMISSION_ENERGY = "android.car.permission.CAR_ENERGY";

/** Permission necessary to access car's fuel door and ev charge port. */

public static final java.lang.String PERMISSION_ENERGY_PORTS = "android.car.permission.CAR_ENERGY_PORTS";

/** Permission necessary to read temperature of car's exterior environment. */

public static final java.lang.String PERMISSION_EXTERIOR_ENVIRONMENT = "android.car.permission.CAR_EXTERIOR_ENVIRONMENT";

/** Permission necessary to read car's exterior lights information.
 *  @hide
 */

public static final java.lang.String PERMISSION_EXTERIOR_LIGHTS = "android.car.permission.CAR_EXTERIOR_LIGHTS";

/** Permission necessary to access car's VIN information */

public static final java.lang.String PERMISSION_IDENTIFICATION = "android.car.permission.CAR_IDENTIFICATION";

/** Permission necessary to access car's mileage information.
 *  @hide
 */

public static final java.lang.String PERMISSION_MILEAGE = "android.car.permission.CAR_MILEAGE";

/**
 * Permission necessary to mock vehicle hal for testing.
 * @hide
 * @deprecated mocking vehicle HAL in car service is no longer supported.
 */

@Deprecated public static final java.lang.String PERMISSION_MOCK_VEHICLE_HAL = "android.car.permission.CAR_MOCK_VEHICLE_HAL";

/** Permission necessary to access car's powertrain information.*/

public static final java.lang.String PERMISSION_POWERTRAIN = "android.car.permission.CAR_POWERTRAIN";

/**
 * Permission necessary to read and write display units for distance, fuel volume, tire pressure
 * and ev battery.
 */

public static final java.lang.String PERMISSION_READ_DISPLAY_UNITS = "android.car.permission.READ_CAR_DISPLAY_UNITS";

/**
 * Permission necessary to read car's interior lights information.
 */

public static final java.lang.String PERMISSION_READ_INTERIOR_LIGHTS = "android.car.permission.READ_CAR_INTERIOR_LIGHTS";

/**
 * Permission necessary to access car's steering angle information.
 */

public static final java.lang.String PERMISSION_READ_STEERING_STATE = "android.car.permission.READ_CAR_STEERING";

/**
 * Permission necessary to receive full audio ducking events from car audio focus handler.
 *
 * @hide
 */

public static final java.lang.String PERMISSION_RECEIVE_CAR_AUDIO_DUCKING_EVENTS = "android.car.permission.RECEIVE_CAR_AUDIO_DUCKING_EVENTS";

/** Permission necessary to access car's speed. */

public static final java.lang.String PERMISSION_SPEED = "android.car.permission.CAR_SPEED";

/**
 * Permissions necessary to clear diagnostic information.
 *
 * @hide
 */

public static final java.lang.String PERMISSION_STORAGE_MONITORING = "android.car.permission.STORAGE_MONITORING";

/**
 * Permission necessary to access car's tire pressure information.
 * @hide
 */

public static final java.lang.String PERMISSION_TIRES = "android.car.permission.CAR_TIRES";

/**
 * Permission necessary to access car specific communication channel.
 * @hide
 */

public static final java.lang.String PERMISSION_VENDOR_EXTENSION = "android.car.permission.CAR_VENDOR_EXTENSION";

/**
 * Permissions necessary to access VMS publisher APIs.
 *
 * @hide
 */

public static final java.lang.String PERMISSION_VMS_PUBLISHER = "android.car.permission.VMS_PUBLISHER";

/**
 * Permissions necessary to access VMS subscriber APIs.
 *
 * @hide
 */

public static final java.lang.String PERMISSION_VMS_SUBSCRIBER = "android.car.permission.VMS_SUBSCRIBER";

/**
 * @hide
 */

public static final java.lang.String POWER_SERVICE = "power";

/**
 * @hide
 */

public static final java.lang.String PROJECTION_SERVICE = "projection";

/**
 * Service name for {@link CarPropertyManager}
 */

public static final java.lang.String PROPERTY_SERVICE = "property";

/**
 * Service name for {@link CarSensorManager}, to be used in {@link #getCarManager(String)}.
 *
 * @deprecated  {@link CarSensorManager} is deprecated. Use {@link CarPropertyManager} instead.
 */

@Deprecated public static final java.lang.String SENSOR_SERVICE = "sensor";

/**
 * @hide
 */

public static final java.lang.String STORAGE_MONITORING_SERVICE = "storage_monitoring";

/**
 * Service for testing. This is system app only feature.
 * Service name for {@link CarTestManager}, to be used in {@link #getCarManager(String)}.
 * @hide
 */

public static final java.lang.String TEST_SERVICE = "car-service-test";

/**
 * Service name for {@link CarVendorExtensionManager}
 *
 * @deprecated {@link CarVendorExtensionManager} is deprecated.
 * Use {@link CarPropertyManager} instead.
 * @hide
 */

@Deprecated public static final java.lang.String VENDOR_EXTENSION_SERVICE = "vendor_extension";

/**
 * @hide
 */

public static final java.lang.String VMS_SUBSCRIBER_SERVICE = "vehicle_map_subscriber_service";
}

