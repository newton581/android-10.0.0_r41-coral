/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.storagemonitoring;


/**
 * Change in wear-out information.
 *
 * Contains information about the first cycle during which the system detected a change
 * in wear-out information of the flash storage.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class WearEstimateChange implements android.os.Parcelable {

public WearEstimateChange(android.car.storagemonitoring.WearEstimate oldEstimate, android.car.storagemonitoring.WearEstimate newEstimate, long uptimeAtChange, java.time.Instant dateAtChange, boolean isAcceptableDegradation) { throw new RuntimeException("Stub!"); }

public WearEstimateChange(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object other) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.storagemonitoring.WearEstimateChange> CREATOR;
static { CREATOR = null; }

/**
 * Wall-clock time when this change was detected.
 */

public final java.time.Instant dateAtChange;
{ dateAtChange = null; }

/**
 * Whether this change was within the vendor range for acceptable flash degradation.
 */

public final boolean isAcceptableDegradation;
{ isAcceptableDegradation = false; }

/**
 * The new wear estimate.
 */

public final android.car.storagemonitoring.WearEstimate newEstimate;
{ newEstimate = null; }

/**
 * The previous wear estimate.
 */

public final android.car.storagemonitoring.WearEstimate oldEstimate;
{ oldEstimate = null; }

/**
 * Total CarService uptime when this change was detected.
 */

public final long uptimeAtChange;
{ uptimeAtChange = 0; }
}

