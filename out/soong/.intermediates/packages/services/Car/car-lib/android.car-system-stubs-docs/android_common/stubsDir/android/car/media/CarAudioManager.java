/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.media;

import android.car.Car;
import android.os.Bundle;

/**
 * APIs for handling audio in a car.
 *
 * In a car environment, we introduced the support to turn audio dynamic routing on /off by
 * setting the "audioUseDynamicRouting" attribute in config.xml
 *
 * When audio dynamic routing is enabled:
 * - Audio devices are grouped into zones
 * - There is at least one primary zone, and extra secondary zones such as RSE
 *   (Reat Seat Entertainment)
 * - Within each zone, audio devices are grouped into volume groups for volume control
 * - Audio is assigned to an audio device based on its AudioAttributes usage
 *
 * When audio dynamic routing is disabled:
 * - There is exactly one audio zone, which is the primary zone
 * - Each volume group represents a controllable STREAM_TYPE, same as AudioManager
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarAudioManager {

/** @hide */

CarAudioManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Sets the volume index for a volume group in primary zone.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @see {@link #setGroupVolume(int, int, int, int)}
 * @hide
 */

public void setGroupVolume(int groupId, int index, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Sets the volume index for a volume group.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @param zoneId The zone id whose volume group is affected.
 * @param groupId The volume group id whose volume index should be set.
 * @param index The volume index to set. See
 *            {@link #getGroupMaxVolume(int, int)} for the largest valid value.
 * @param flags One or more flags (e.g., {@link android.media.AudioManager#FLAG_SHOW_UI},
 *              {@link android.media.AudioManager#FLAG_PLAY_SOUND})
 * @hide
 */

public void setGroupVolume(int zoneId, int groupId, int index, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Returns the maximum volume index for a volume group in primary zone.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @see {@link #getGroupMaxVolume(int, int)}
 * @hide
 */

public int getGroupMaxVolume(int groupId) { throw new RuntimeException("Stub!"); }

/**
 * Returns the maximum volume index for a volume group.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @param zoneId The zone id whose volume group is queried.
 * @param groupId The volume group id whose maximum volume index is returned.
 * @return The maximum valid volume index for the given group.
 * @hide
 */

public int getGroupMaxVolume(int zoneId, int groupId) { throw new RuntimeException("Stub!"); }

/**
 * Returns the minimum volume index for a volume group in primary zone.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @see {@link #getGroupMinVolume(int, int)}
 * @hide
 */

public int getGroupMinVolume(int groupId) { throw new RuntimeException("Stub!"); }

/**
 * Returns the minimum volume index for a volume group.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @param zoneId The zone id whose volume group is queried.
 * @param groupId The volume group id whose minimum volume index is returned.
 * @return The minimum valid volume index for the given group, non-negative
 * @hide
 */

public int getGroupMinVolume(int zoneId, int groupId) { throw new RuntimeException("Stub!"); }

/**
 * Returns the current volume index for a volume group in primary zone.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @see {@link #getGroupVolume(int, int)}
 * @hide
 */

public int getGroupVolume(int groupId) { throw new RuntimeException("Stub!"); }

/**
 * Returns the current volume index for a volume group.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @param zoneId The zone id whose volume groups is queried.
 * @param groupId The volume group id whose volume index is returned.
 * @return The current volume index for the given group.
 *
 * @see #getGroupMaxVolume(int, int)
 * @see #setGroupVolume(int, int, int, int)
 * @hide
 */

public int getGroupVolume(int zoneId, int groupId) { throw new RuntimeException("Stub!"); }

/**
 * Adjust the relative volume in the front vs back of the vehicle cabin.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @param value in the range -1.0 to 1.0 for fully toward the back through
 *              fully toward the front.  0.0 means evenly balanced.
 *
 * @see #setBalanceTowardRight(float)
 * @hide
 */

public void setFadeTowardFront(float value) { throw new RuntimeException("Stub!"); }

/**
 * Adjust the relative volume on the left vs right side of the vehicle cabin.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @param value in the range -1.0 to 1.0 for fully toward the left through
 *              fully toward the right.  0.0 means evenly balanced.
 *
 * @see #setFadeTowardFront(float)
 * @hide
 */

public void setBalanceTowardRight(float value) { throw new RuntimeException("Stub!"); }

/**
 * Queries the system configuration in order to report the available, non-microphone audio
 * input devices.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_SETTINGS}
 * @return An array of strings representing the available input ports.
 * Each port is identified by it's "address" tag in the audioPolicyConfiguration xml file.
 * Empty array if we find nothing.
 *
 * @see #createAudioPatch(String, int, int)
 * @see #releaseAudioPatch(CarAudioPatchHandle)
 * @hide
 */

public java.lang.String[] getExternalSources() { throw new RuntimeException("Stub!"); }

/**
 * Given an input port identified by getExternalSources(), request that it's audio signal
 * be routed below the HAL to the output port associated with the given usage.  For example,
 * The output of a tuner might be routed directly to the output buss associated with
 * AudioAttributes.USAGE_MEDIA while the tuner is playing.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_SETTINGS}
 * @param sourceAddress the input port name obtained from getExternalSources().
 * @param usage the type of audio represented by this source (usually USAGE_MEDIA).
 * @param gainInMillibels How many steps above the minimum value defined for the source port to
 *                       set the gain when creating the patch.
 *                       This may be used for source balancing without affecting the user
 *                       controlled volumes applied to the destination ports.  A value of
 *                       0 indicates no gain change is requested.
 * @return A handle for the created patch which can be used to later remove it.
 *
 * @see #getExternalSources()
 * @see #releaseAudioPatch(CarAudioPatchHandle)
 * @hide
 */

public android.car.media.CarAudioPatchHandle createAudioPatch(java.lang.String sourceAddress, int usage, int gainInMillibels) { throw new RuntimeException("Stub!"); }

/**
 * Removes the association between an input port and an output port identified by the provided
 * handle.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_SETTINGS}
 * @param patch CarAudioPatchHandle returned from createAudioPatch().
 *
 * @see #getExternalSources()
 * @see #createAudioPatch(String, int, int)
 * @hide
 */

public void releaseAudioPatch(android.car.media.CarAudioPatchHandle patch) { throw new RuntimeException("Stub!"); }

/**
 * Gets the count of available volume groups in primary zone.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @see {@link #getVolumeGroupCount(int)}
 * @hide
 */

public int getVolumeGroupCount() { throw new RuntimeException("Stub!"); }

/**
 * Gets the count of available volume groups in the system.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @param zoneId The zone id whois count of volume groups is queried.
 * @return Count of volume groups
 * @hide
 */

public int getVolumeGroupCount(int zoneId) { throw new RuntimeException("Stub!"); }

/**
 * Gets the volume group id for a given {@link AudioAttributes} usage in primary zone.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @see {@link #getVolumeGroupIdForUsage(int, int)}
 * @hide
 */

public int getVolumeGroupIdForUsage(int usage) { throw new RuntimeException("Stub!"); }

/**
 * Gets the volume group id for a given {@link AudioAttributes} usage.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @param zoneId The zone id whose volume group is queried.
 * @param usage The {@link AudioAttributes} usage to get a volume group from.
 * @return The volume group id where the usage belongs to
 * @hide
 */

public int getVolumeGroupIdForUsage(int zoneId, int usage) { throw new RuntimeException("Stub!"); }

/**
 * Gets array of {@link AudioAttributes} usages for a volume group in primary zone.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @see {@link #getUsagesForVolumeGroupId(int, int)}
 * @hide
 */

public int[] getUsagesForVolumeGroupId(int groupId) { throw new RuntimeException("Stub!"); }

/**
 * Gets array of {@link AudioAttributes} usages for a volume group in a zone.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_CONTROL_AUDIO_VOLUME}
 * @param zoneId The zone id whose volume group is queried.
 * @param groupId The volume group id whose associated audio usages is returned.
 * @return Array of {@link AudioAttributes} usages for a given volume group id
 * @hide
 */

public int[] getUsagesForVolumeGroupId(int zoneId, int groupId) { throw new RuntimeException("Stub!"); }

/**
 * Registers a {@link CarVolumeCallback} to receive volume change callbacks
 * @param callback {@link CarVolumeCallback} instance, can not be null
 */

public void registerCarVolumeCallback(android.car.media.CarAudioManager.CarVolumeCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Unregisters a {@link CarVolumeCallback} from receiving volume change callbacks
 * @param callback {@link CarVolumeCallback} instance previously registered, can not be null
 */

public void unregisterCarVolumeCallback(android.car.media.CarAudioManager.CarVolumeCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Extra for {@link android.media.AudioAttributes.Builder#addBundle(Bundle)}: when used in an
 * {@link android.media.AudioFocusRequest}, the requester should receive all audio focus events,
 * including {@link android.media.AudioManager#AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK}.
 * The requester must hold {@link Car#PERMISSION_RECEIVE_CAR_AUDIO_DUCKING_EVENTS}; otherwise,
 * this extra is ignored.
 *
 * @hide
 */

public static final java.lang.String AUDIOFOCUS_EXTRA_RECEIVE_DUCKING_EVENTS = "android.car.media.AUDIOFOCUS_EXTRA_RECEIVE_DUCKING_EVENTS";

/**
 * Zone id of the primary audio zone.
 * @hide
 */

public static final int PRIMARY_AUDIO_ZONE = 0; // 0x0
/**
 * Callback interface to receive volume change events in a car.
 * Extend this class and register it with {@link #registerCarVolumeCallback(CarVolumeCallback)}
 * and unregister it via {@link #unregisterCarVolumeCallback(CarVolumeCallback)}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class CarVolumeCallback {

public CarVolumeCallback() { throw new RuntimeException("Stub!"); }

/**
 * This is called whenever a group volume is changed.
 * The changed-to volume index is not included, the caller is encouraged to
 * get the current group volume index via CarAudioManager.
 *
 * @param zoneId Id of the audio zone that volume change happens
 * @param groupId Id of the volume group that volume is changed
 * @param flags see {@link android.media.AudioManager} for flag definitions
 */

public void onGroupVolumeChanged(int zoneId, int groupId, int flags) { throw new RuntimeException("Stub!"); }

/**
 * This is called whenever the master mute state is changed.
 * The changed-to master mute state is not included, the caller is encouraged to
 * get the current master mute state via AudioManager.
 *
 * @param zoneId Id of the audio zone that master mute state change happens
 * @param flags see {@link android.media.AudioManager} for flag definitions
 */

public void onMasterMuteChanged(int zoneId, int flags) { throw new RuntimeException("Stub!"); }
}

}

