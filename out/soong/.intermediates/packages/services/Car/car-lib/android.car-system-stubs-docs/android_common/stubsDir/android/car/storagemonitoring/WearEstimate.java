/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.storagemonitoring;


/**
 * Wear-out information for flash storage.
 *
 * Contains a lower-bound estimate of the wear of "type A" (SLC) and "type B" (MLC) storage.
 *
 * Current technology offers wear data in increments of 10% (i.e. from 0 == new device up to
 * 100 == worn-out device). It is possible for a storage device to only support one type of memory
 * cell, in which case it is expected that the storage type not supported will have UNKNOWN wear.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class WearEstimate implements android.os.Parcelable {

public WearEstimate(int typeA, int typeB) { throw new RuntimeException("Stub!"); }

public WearEstimate(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object other) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.storagemonitoring.WearEstimate> CREATOR;
static { CREATOR = null; }

public static final int UNKNOWN = -1; // 0xffffffff

/**
 * Wear estimate data for "type A" storage.

 * <br>
 * Value is between -1 and 100 inclusive
 */

public final int typeA;
{ typeA = 0; }

/**
 * Wear estimate data for "type B" storage.

 * <br>
 * Value is between -1 and 100 inclusive
 */

public final int typeB;
{ typeB = 0; }
}

