/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.drivingstate;


/**
 * Driving State related events.  Driving State of a car conveys if the car is currently parked,
 * idling or moving.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarDrivingStateEvent implements android.os.Parcelable {

public CarDrivingStateEvent(int value, long time) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.drivingstate.CarDrivingStateEvent> CREATOR;
static { CREATOR = null; }

/**
 * Car is idling.  Gear is not in Parked mode and Speed of the vehicle is zero.
 */

public static final int DRIVING_STATE_IDLING = 1; // 0x1

/**
 * Car is moving.  Gear is not in parked mode and speed of the vehicle is non zero.
 */

public static final int DRIVING_STATE_MOVING = 2; // 0x2

/**
 * Car is parked - Gear is in Parked mode.
 */

public static final int DRIVING_STATE_PARKED = 0; // 0x0

/**
 * This is when we don't have enough information to infer the car's driving state.
 */

public static final int DRIVING_STATE_UNKNOWN = -1; // 0xffffffff

/**
 * The Car's driving state.

 * <br>
 * Value is {@link android.car.drivingstate.CarDrivingStateEvent#DRIVING_STATE_UNKNOWN}, {@link android.car.drivingstate.CarDrivingStateEvent#DRIVING_STATE_PARKED}, {@link android.car.drivingstate.CarDrivingStateEvent#DRIVING_STATE_IDLING}, or {@link android.car.drivingstate.CarDrivingStateEvent#DRIVING_STATE_MOVING}
 */

public final int eventValue;
{ eventValue = 0; }

/**
 * Time at which this driving state was inferred based on the car's sensors.
 * It is the elapsed time in nanoseconds since system boot.
 */

public final long timeStamp;
{ timeStamp = 0; }
}

