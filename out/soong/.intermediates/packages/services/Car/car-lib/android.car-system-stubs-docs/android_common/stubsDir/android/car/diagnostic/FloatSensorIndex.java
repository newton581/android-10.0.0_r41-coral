/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/


package android.car.diagnostic;


/**
 * This class is a container for the indices of diagnostic sensors. The values are extracted by
 * running packages/services/Car/tools/update-obd2-sensors.py against types.hal.
 *
 * DO NOT EDIT MANUALLY
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class FloatSensorIndex {

FloatSensorIndex() { throw new RuntimeException("Stub!"); }

public static final int ABSOLUTE_EVAPORATION_SYSTEM_VAPOR_PRESSURE = 58; // 0x3a

public static final int ABSOLUTE_LOAD_VALUE = 48; // 0x30

public static final int ABSOLUTE_THROTTLE_POSITION_B = 51; // 0x33

public static final int ABSOLUTE_THROTTLE_POSITION_C = 52; // 0x34

public static final int ACCELERATOR_PEDAL_POSITION_D = 53; // 0x35

public static final int ACCELERATOR_PEDAL_POSITION_E = 54; // 0x36

public static final int ACCELERATOR_PEDAL_POSITION_F = 55; // 0x37

public static final int CALCULATED_ENGINE_LOAD = 0; // 0x0

public static final int CATALYST_TEMPERATURE_BANK1_SENSOR1 = 44; // 0x2c

public static final int CATALYST_TEMPERATURE_BANK1_SENSOR2 = 46; // 0x2e

public static final int CATALYST_TEMPERATURE_BANK2_SENSOR1 = 45; // 0x2d

public static final int CATALYST_TEMPERATURE_BANK2_SENSOR2 = 47; // 0x2f

public static final int COMMANDED_EVAPORATIVE_PURGE = 41; // 0x29

public static final int COMMANDED_EXHAUST_GAS_RECIRCULATION = 39; // 0x27

public static final int COMMANDED_THROTTLE_ACTUATOR = 56; // 0x38

public static final int ENGINE_COOLANT_TEMPERATURE = 1; // 0x1

public static final int ENGINE_FUEL_RATE = 70; // 0x46

public static final int ENGINE_RPM = 8; // 0x8

public static final int ETHANOL_FUEL_PERCENTAGE = 57; // 0x39

public static final int EVAPORATION_SYSTEM_VAPOR_PRESSURE = 43; // 0x2b

public static final int EXHAUST_GAS_RECIRCULATION_ERROR = 40; // 0x28

public static final int FUEL_AIR_COMMANDED_EQUIVALENCE_RATIO = 49; // 0x31

public static final int FUEL_INJECTION_TIMING = 69; // 0x45

public static final int FUEL_PRESSURE = 6; // 0x6

public static final int FUEL_RAIL_GAUGE_PRESSURE = 38; // 0x26

public static final int FUEL_RAIL_PRESSURE = 37; // 0x25

public static final int FUEL_TANK_LEVEL_INPUT = 42; // 0x2a

public static final int HYBRID_BATTERY_PACK_REMAINING_LIFE = 68; // 0x44

public static final int INTAKE_MANIFOLD_ABSOLUTE_PRESSURE = 7; // 0x7

public static final int LAST_SYSTEM = 70; // 0x46

public static final int LONG_TERM_FUEL_TRIM_BANK1 = 3; // 0x3

public static final int LONG_TERM_FUEL_TRIM_BANK2 = 5; // 0x5

public static final int LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK1 = 63; // 0x3f

public static final int LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK2 = 64; // 0x40

public static final int LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK3 = 65; // 0x41

public static final int LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK4 = 66; // 0x42

public static final int MAF_AIR_FLOW_RATE = 11; // 0xb

public static final int OXYGEN_SENSOR1_FUEL_AIR_EQUIVALENCE_RATIO = 15; // 0xf

public static final int OXYGEN_SENSOR1_SHORT_TERM_FUEL_TRIM = 14; // 0xe

public static final int OXYGEN_SENSOR1_VOLTAGE = 13; // 0xd

public static final int OXYGEN_SENSOR2_FUEL_AIR_EQUIVALENCE_RATIO = 18; // 0x12

public static final int OXYGEN_SENSOR2_SHORT_TERM_FUEL_TRIM = 17; // 0x11

public static final int OXYGEN_SENSOR2_VOLTAGE = 16; // 0x10

public static final int OXYGEN_SENSOR3_FUEL_AIR_EQUIVALENCE_RATIO = 21; // 0x15

public static final int OXYGEN_SENSOR3_SHORT_TERM_FUEL_TRIM = 20; // 0x14

public static final int OXYGEN_SENSOR3_VOLTAGE = 19; // 0x13

public static final int OXYGEN_SENSOR4_FUEL_AIR_EQUIVALENCE_RATIO = 24; // 0x18

public static final int OXYGEN_SENSOR4_SHORT_TERM_FUEL_TRIM = 23; // 0x17

public static final int OXYGEN_SENSOR4_VOLTAGE = 22; // 0x16

public static final int OXYGEN_SENSOR5_FUEL_AIR_EQUIVALENCE_RATIO = 27; // 0x1b

public static final int OXYGEN_SENSOR5_SHORT_TERM_FUEL_TRIM = 26; // 0x1a

public static final int OXYGEN_SENSOR5_VOLTAGE = 25; // 0x19

public static final int OXYGEN_SENSOR6_FUEL_AIR_EQUIVALENCE_RATIO = 30; // 0x1e

public static final int OXYGEN_SENSOR6_SHORT_TERM_FUEL_TRIM = 29; // 0x1d

public static final int OXYGEN_SENSOR6_VOLTAGE = 28; // 0x1c

public static final int OXYGEN_SENSOR7_FUEL_AIR_EQUIVALENCE_RATIO = 33; // 0x21

public static final int OXYGEN_SENSOR7_SHORT_TERM_FUEL_TRIM = 32; // 0x20

public static final int OXYGEN_SENSOR7_VOLTAGE = 31; // 0x1f

public static final int OXYGEN_SENSOR8_FUEL_AIR_EQUIVALENCE_RATIO = 36; // 0x24

public static final int OXYGEN_SENSOR8_SHORT_TERM_FUEL_TRIM = 35; // 0x23

public static final int OXYGEN_SENSOR8_VOLTAGE = 34; // 0x22

public static final int RELATIVE_ACCELERATOR_PEDAL_POSITION = 67; // 0x43

public static final int RELATIVE_THROTTLE_POSITION = 50; // 0x32

public static final int SHORT_TERM_FUEL_TRIM_BANK1 = 2; // 0x2

public static final int SHORT_TERM_FUEL_TRIM_BANK2 = 4; // 0x4

public static final int SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK1 = 59; // 0x3b

public static final int SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK2 = 60; // 0x3c

public static final int SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK3 = 61; // 0x3d

public static final int SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK4 = 62; // 0x3e

public static final int THROTTLE_POSITION = 12; // 0xc

public static final int TIMING_ADVANCE = 10; // 0xa

public static final int VEHICLE_SPEED = 9; // 0x9

public static final int VENDOR_START = 71; // 0x47
}

