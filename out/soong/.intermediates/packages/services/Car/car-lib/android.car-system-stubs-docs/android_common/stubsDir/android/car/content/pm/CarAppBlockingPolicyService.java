/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.content.pm;

import android.car.Car;
import android.app.Service;

/**
 * Service to be implemented by Service which wants to control app blocking policy.
 * App should require android.car.permission.CONTROL_APP_BLOCKING to launch Service
 * implementation. Additionally the APK should have the permission to be launched by Car Service.
 * The implementing service should declare {@link #SERVICE_INTERFACE} in its intent filter as
 * action.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class CarAppBlockingPolicyService extends android.app.Service {

public CarAppBlockingPolicyService() { throw new RuntimeException("Stub!"); }

/**
 * Return the app blocking policy. This is called from binder thread.
 * @return
 */

protected abstract android.car.content.pm.CarAppBlockingPolicy getAppBlockingPolicy();

public int onStartCommand(android.content.Intent intent, int flags, int startId) { throw new RuntimeException("Stub!"); }

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

public boolean onUnbind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

public static final java.lang.String SERVICE_INTERFACE = "android.car.content.pm.CarAppBlockingPolicyService";
}

