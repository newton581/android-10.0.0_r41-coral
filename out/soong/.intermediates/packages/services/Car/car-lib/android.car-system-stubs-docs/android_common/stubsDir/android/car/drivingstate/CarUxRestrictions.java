/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.drivingstate;


/**
 * Car UX Restrictions event.  This contains information on the set of UX restrictions that is in
 * place due to the car's driving state.
 * <p>
 * The restriction information is organized as follows:
 * <ul>
 * <li> When there are no restrictions in place, for example when the car is parked,
 * <ul>
 * <li> {@link #isRequiresDistractionOptimization()} returns false.  Apps can display activities
 * that are not distraction optimized.
 * <li> When {@link #isRequiresDistractionOptimization()} returns false, apps don't have to call
 * {@link #getActiveRestrictions()}, since there is no distraction optimization required.
 * </ul>
 * <li> When the driving state changes, causing the UX restrictions to come in effect,
 * <ul>
 * <li> {@link #isRequiresDistractionOptimization()} returns true.  Apps can only display activities
 * that are distraction optimized.  Distraction optimized activities must follow the base design
 * guidelines to ensure a distraction free driving experience for the user.
 * <li> When {@link #isRequiresDistractionOptimization()} returns true, apps must call
 * {@link #getActiveRestrictions()}, to get the currently active UX restrictions to adhere to.
 * {@link #getActiveRestrictions()} provides additional information on the set of UX
 * restrictions that are in place for the current driving state.
 * <p>
 * The UX restrictions returned by {@link #getActiveRestrictions()}, for the same driving state of
 * the vehicle, could vary depending on the OEM and the market.  For example, when the car is
 * idling, the set of active UX restrictions will depend on the car maker and the safety standards
 * of the market that the vehicle is deployed in.
 * </ul>
 * </ul>
 * <p>
 * Apps that intend to be run when the car is being driven need to
 * <ul>
 * <li> Comply with the general distraction optimization guidelines.
 * <li> Listen and react to the UX restrictions changes as detailed above.  Since the restrictions
 * could vary depending on the market, apps are expected to react to the restriction information
 * and not to the absolute driving state.
 * </ul>
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarUxRestrictions implements android.os.Parcelable {

public CarUxRestrictions(android.car.drivingstate.CarUxRestrictions uxRestrictions) { throw new RuntimeException("Stub!"); }

/**
 * Conveys if the foreground activity needs to be distraction optimized.
 * Activities that can handle distraction optimization need to be tagged as a distraction
 * optimized in the app's manifest.
 * <p>
 * If the app has a foreground activity that has not been distraction optimized, the app has
 * to switch to another activity that is distraction optimized.  Failing that, the system will
 * stop the foreground activity.
 *
 * @return true if distraction optimization is required, false if not
 */

public boolean isRequiresDistractionOptimization() { throw new RuntimeException("Stub!"); }

/**
 * A combination of the Car UX Restrictions that is active for the current state of driving.
 *
 * @return A combination of the above {@code @CarUxRestrictionsInfo}

 * Value is either <code>0</code> or a combination of {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_BASELINE}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_DIALPAD}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_FILTERING}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_LIMIT_STRING_LENGTH}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_KEYBOARD}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_VIDEO}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_LIMIT_CONTENT}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_SETUP}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_TEXT_MESSAGE}, and {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_VOICE_TRANSCRIPTION}
 */

public int getActiveRestrictions() { throw new RuntimeException("Stub!"); }

/**
 * Get the maximum length of general purpose strings that can be displayed when
 * {@link CarUxRestrictions#UX_RESTRICTIONS_LIMIT_STRING_LENGTH} is imposed.
 *
 * @return the maximum length of string that can be displayed
 */

public int getMaxRestrictedStringLength() { throw new RuntimeException("Stub!"); }

/**
 * Get the maximum allowable number of content items that can be displayed to a user during
 * traversal through any one path in a single task, when
 * {@link CarUxRestrictions#UX_RESTRICTIONS_LIMIT_CONTENT} is imposed.
 * <p>
 * For example, if a task involving only one view, this represents the maximum allowable number
 * of content items in this single view.
 * <p>
 * However, if the task involves selection of a content item in an originating view that then
 * surfaces a secondary view to the user, then this value represents the maximum allowable
 * number of content items between the originating and secondary views combined.
 * <p>
 * Specifically, if the maximum allowable value was 60 and a task involved browsing a list of
 * countries and then viewing the top songs within a country, it would be acceptable to do
 * either of the following:
 * <ul>
 * <li> list 10 countries, and then display the top 50 songs after country selection, or
 * <li> list 20 countries, and then display the top 40 songs after country selection.
 * </ul>
 * <p>
 * Please refer to this and {@link #getMaxContentDepth()} to know the upper bounds on the
 * content display when the restriction is in place.
 *
 * @return maximum number of cumulative items that can be displayed
 */

public int getMaxCumulativeContentItems() { throw new RuntimeException("Stub!"); }

/**
 * Get the maximum allowable number of content depth levels or view traversals through any one
 * path in a single task.  This is applicable when
 * {@link CarUxRestrictions#UX_RESTRICTIONS_LIMIT_CONTENT} is imposed.
 * <p>
 * For example, if a task involves only selecting an item from a single list on one view,
 * the task's content depth would be considered 1.
 * <p>
 * However, if the task involves selection of a content item in an originating view that then
 * surfaces a secondary view to the user, the task's content depth would be considered 2.
 * <p>
 * Specifically, if a task involved browsing a list of countries, selecting a genre within the
 * country, and then viewing the top songs within a country, the task's content depth would be
 * considered 3.
 * <p>
 * Please refer to this and {@link #getMaxCumulativeContentItems()} to know the upper bounds on
 * the content display when the restriction is in place.
 *
 * @return maximum number of cumulative items that can be displayed
 */

public int getMaxContentDepth() { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Compares if the restrictions are the same.  Doesn't compare the timestamps.
 *
 * @param other the other CarUxRestrictions object
 * @return true if the restrictions are same, false otherwise
 */

public boolean isSameRestrictions(android.car.drivingstate.CarUxRestrictions other) { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.drivingstate.CarUxRestrictions> CREATOR;
static { CREATOR = null; }

/**
 * No specific restrictions in place, but baseline distraction optimization guidelines need to
 * be adhered to when {@link #isRequiresDistractionOptimization()} is true.
 */

public static final int UX_RESTRICTIONS_BASELINE = 0; // 0x0

/**
 * All restrictions are in effect.
 */

public static final int UX_RESTRICTIONS_FULLY_RESTRICTED = 511; // 0x1ff

/**
 * Limit the number of items user can browse through in total in a single task.
 *
 * <p>Refer to {@link #getMaxCumulativeContentItems()} and
 * {@link #getMaxContentDepth()} for the upper bounds on content
 * serving.
 */

public static final int UX_RESTRICTIONS_LIMIT_CONTENT = 32; // 0x20

/**
 * General purpose strings length cannot exceed the character limit provided by
 * {@link #getMaxRestrictedStringLength()}
 */

public static final int UX_RESTRICTIONS_LIMIT_STRING_LENGTH = 4; // 0x4

/**
 * No dialpad for the purpose of initiating a phone call.
 */

public static final int UX_RESTRICTIONS_NO_DIALPAD = 1; // 0x1

/**
 * No filtering a list with alpha-numeric character via the use of a character entry method.
 *
 * For example, do not allow entering a letter to filter the content of a list down to
 * items only containing that letter.
 */

public static final int UX_RESTRICTIONS_NO_FILTERING = 2; // 0x2

/**
 * No text entry for the purpose of searching or other manual text string entry actvities.
 */

public static final int UX_RESTRICTIONS_NO_KEYBOARD = 8; // 0x8

/**
 * No setup that requires form entry or interaction with external devices.
 */

public static final int UX_RESTRICTIONS_NO_SETUP = 64; // 0x40

/**
 * No Text Message (SMS, email, conversational, etc.)
 */

public static final int UX_RESTRICTIONS_NO_TEXT_MESSAGE = 128; // 0x80

/**
 * No video - no animated frames > 1fps.
 */

public static final int UX_RESTRICTIONS_NO_VIDEO = 16; // 0x10

/**
 * No text transcription (live or leave behind) of voice can be shown.
 */

public static final int UX_RESTRICTIONS_NO_VOICE_TRANSCRIPTION = 256; // 0x100
/**
 * Builder class for {@link CarUxRestrictions}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

/**
 * @param restrictions Value is either <code>0</code> or a combination of {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_BASELINE}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_DIALPAD}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_FILTERING}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_LIMIT_STRING_LENGTH}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_KEYBOARD}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_VIDEO}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_LIMIT_CONTENT}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_SETUP}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_TEXT_MESSAGE}, and {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_VOICE_TRANSCRIPTION}
 */

public Builder(boolean reqOpt, int restrictions, long time) { throw new RuntimeException("Stub!"); }

/**
 * Set the maximum length of general purpose strings that can be displayed when
 * {@link CarUxRestrictions#UX_RESTRICTIONS_LIMIT_STRING_LENGTH} is imposed.
 */

public android.car.drivingstate.CarUxRestrictions.Builder setMaxStringLength(int length) { throw new RuntimeException("Stub!"); }

/**
 *  Set the maximum number of cumulative content items that can be displayed when
 * {@link CarUxRestrictions#UX_RESTRICTIONS_LIMIT_CONTENT} is imposed.
 */

public android.car.drivingstate.CarUxRestrictions.Builder setMaxCumulativeContentItems(int number) { throw new RuntimeException("Stub!"); }

/**
 * Set the maximum number of levels that the user can navigate to when
 * {@link CarUxRestrictions#UX_RESTRICTIONS_LIMIT_CONTENT} is imposed.
 */

public android.car.drivingstate.CarUxRestrictions.Builder setMaxContentDepth(int depth) { throw new RuntimeException("Stub!"); }

/**
 * Build and return the {@link CarUxRestrictions} object
 */

public android.car.drivingstate.CarUxRestrictions build() { throw new RuntimeException("Stub!"); }
}

/**
 * Value is either <code>0</code> or a combination of {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_BASELINE}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_DIALPAD}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_FILTERING}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_LIMIT_STRING_LENGTH}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_KEYBOARD}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_VIDEO}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_LIMIT_CONTENT}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_SETUP}, {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_TEXT_MESSAGE}, and {@link android.car.drivingstate.CarUxRestrictions#UX_RESTRICTIONS_NO_VOICE_TRANSCRIPTION}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface CarUxRestrictionsInfo {
}

}

