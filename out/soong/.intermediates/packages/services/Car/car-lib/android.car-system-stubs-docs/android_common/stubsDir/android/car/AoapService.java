/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car;

import android.hardware.usb.UsbDevice;

/**
 * The service that must be implemented by USB AOAP handler system apps. The app must hold the
 * following permission: {@code android.car.permission.CAR_HANDLE_USB_AOAP_DEVICE}.
 *
 * <p>This service gets bound by the framework and the service needs to be protected by
 * {@code android.permission.MANAGE_USB} permission to ensure nobody else can
 * bind to the service. At most only one client should be bound at a time.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class AoapService extends android.app.Service {

public AoapService() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the given USB device is supported by this service.
 *
 * <p>The device is not expected to be in AOAP mode when this method is called. The purpose of
 * this method is just to give the service a chance to tell whether based on the information
 * provided in {@link UsbDevice} class (e.g. PID/VID) this service supports or doesn't support
 * given device.
 *
 * <p>The method must return one of the following status: {@link #RESULT_OK} or
 * {@link #RESULT_DEVICE_NOT_SUPPORTED}

 * @return Value is {@link android.car.AoapService#RESULT_OK}, {@link android.car.AoapService#RESULT_DEVICE_NOT_SUPPORTED}, or {@link android.car.AoapService#RESULT_DO_NOT_SWITCH_TO_AOAP}
 */

public abstract int isDeviceSupported(android.hardware.usb.UsbDevice device);

/**
 * This method will be called at least once per connection session before switching device into
 * AOAP mode.
 *
 * <p>The device is connected, but not in AOAP mode yet. Implementors of this method may ask
 * the framework to ignore this device for now and do not switch to AOAP. This may make sense if
 * a connection to the device has been established through other means, and switching the device
 * to AOAP would break that connection.
 *
 * <p>Note: the method may be called only if this device was claimed to be supported in
 * {@link #isDeviceSupported(UsbDevice)} method, and this app has been chosen to handle the
 * device.
 *
 * <p>The method must return one of the following status: {@link #RESULT_OK},
 * {@link #RESULT_DEVICE_NOT_SUPPORTED} or {@link #RESULT_DO_NOT_SWITCH_TO_AOAP}

 * @return Value is {@link android.car.AoapService#RESULT_OK}, {@link android.car.AoapService#RESULT_DEVICE_NOT_SUPPORTED}, or {@link android.car.AoapService#RESULT_DO_NOT_SWITCH_TO_AOAP}
 */

public int canSwitchToAoap(android.hardware.usb.UsbDevice device) { throw new RuntimeException("Stub!"); }

public void onCreate() { throw new RuntimeException("Stub!"); }

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

public boolean onUnbind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

protected void dump(java.io.FileDescriptor fd, java.io.PrintWriter writer, java.lang.String[] args) { throw new RuntimeException("Stub!"); }

/**
 * Indicates that the device is not supported by this service and system shouldn't associate
 * given device with this service.
 */

public static final int RESULT_DEVICE_NOT_SUPPORTED = 1; // 0x1

/**
 * Indicates that device shouldn't be switch to AOAP mode at this time.
 */

public static final int RESULT_DO_NOT_SWITCH_TO_AOAP = 2; // 0x2

/** Indicates success or confirmation. */

public static final int RESULT_OK = 0; // 0x0
}

