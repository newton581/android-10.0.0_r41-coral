/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.storagemonitoring;


/**
 * Delta of uid_io stats taken at a sample point.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class IoStats implements android.os.Parcelable {

public IoStats(java.util.List<android.car.storagemonitoring.IoStatsEntry> stats, long timestamp) { throw new RuntimeException("Stub!"); }

public IoStats(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public long getTimestamp() { throw new RuntimeException("Stub!"); }

public java.util.List<android.car.storagemonitoring.IoStatsEntry> getStats() { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public android.car.storagemonitoring.IoStatsEntry getUserIdStats(int uid) { throw new RuntimeException("Stub!"); }

public android.car.storagemonitoring.IoStatsEntry.Metrics getForegroundTotals() { throw new RuntimeException("Stub!"); }

public android.car.storagemonitoring.IoStatsEntry.Metrics getBackgroundTotals() { throw new RuntimeException("Stub!"); }

public android.car.storagemonitoring.IoStatsEntry.Metrics getTotals() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object other) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.storagemonitoring.IoStats> CREATOR;
static { CREATOR = null; }
}

