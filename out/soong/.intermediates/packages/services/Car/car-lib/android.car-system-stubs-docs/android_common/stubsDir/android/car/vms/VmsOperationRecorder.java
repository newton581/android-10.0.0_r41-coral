
package android.car.vms;


/**
 * Records VMS operations using the Android Log.
 *
 * This class records VMS operations. The recorded messages include the VMS operations and its
 * arguments encoded as JSON text so that the string can be both read as a log message and easily
 * parsed. VmsOperationRecorder is intended to be called after successful state change.
 *
 * Access the VmsOperationRecorder using the {@link #get()} method, which returns a singleton
 * instance. Each VMS operation has a corresponding VmsOperationRecorder method. For instance:
 * <pre>{@code
 *   VmsOperationRecorder.get().subscribe(layer);
 * }</pre>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class VmsOperationRecorder {

VmsOperationRecorder() { throw new RuntimeException("Stub!"); }

/** Return the singleton instance. */

public static android.car.vms.VmsOperationRecorder get() { throw new RuntimeException("Stub!"); }

public void subscribe(android.car.vms.VmsLayer layer) { throw new RuntimeException("Stub!"); }

public void unsubscribe(android.car.vms.VmsLayer layer) { throw new RuntimeException("Stub!"); }

public void subscribe(android.car.vms.VmsLayer layer, int publisherId) { throw new RuntimeException("Stub!"); }

public void unsubscribe(android.car.vms.VmsLayer layer, int publisherId) { throw new RuntimeException("Stub!"); }

public void startMonitoring() { throw new RuntimeException("Stub!"); }

public void stopMonitoring() { throw new RuntimeException("Stub!"); }

public void setLayersOffering(android.car.vms.VmsLayersOffering layersOffering) { throw new RuntimeException("Stub!"); }

public void getPublisherId(int publisherId) { throw new RuntimeException("Stub!"); }

public void addSubscription(int sequenceNumber, android.car.vms.VmsLayer layer) { throw new RuntimeException("Stub!"); }

public void removeSubscription(int sequenceNumber, android.car.vms.VmsLayer layer) { throw new RuntimeException("Stub!"); }

public void addPromiscuousSubscription(int sequenceNumber) { throw new RuntimeException("Stub!"); }

public void removePromiscuousSubscription(int sequenceNumber) { throw new RuntimeException("Stub!"); }

public void addHalSubscription(int sequenceNumber, android.car.vms.VmsLayer layer) { throw new RuntimeException("Stub!"); }

public void removeHalSubscription(int sequenceNumber, android.car.vms.VmsLayer layer) { throw new RuntimeException("Stub!"); }

public void setPublisherLayersOffering(android.car.vms.VmsLayersOffering layersOffering) { throw new RuntimeException("Stub!"); }

public void setHalPublisherLayersOffering(android.car.vms.VmsLayersOffering layersOffering) { throw new RuntimeException("Stub!"); }
}

