/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.hardware.hvac;

import android.car.hardware.property.CarPropertyManager;
import android.car.Car;
import android.car.hardware.CarPropertyConfig;
import java.util.List;

/**
 * @deprecated Use {@link CarPropertyManager} instead.
 *
 * API for controlling HVAC system in cars
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class CarHvacManager {

/**
 * Get an instance of the CarHvacManager.
 *
 * Should not be obtained directly by clients, use {@link Car#getCarManager(String)} instead.
 * @param service
 *
 * @hide
 */

CarHvacManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Implement wrappers for contained CarPropertyManager object
 * @param callback
 */

@Deprecated
public synchronized void registerCallback(android.car.hardware.hvac.CarHvacManager.CarHvacEventCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Stop getting property updates for the given callback. If there are multiple registrations for
 * this listener, all listening will be stopped.
 * @param callback
 */

@Deprecated
public synchronized void unregisterCallback(android.car.hardware.hvac.CarHvacManager.CarHvacEventCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Get list of properties represented by Car Hvac Manager for this car.
 * @return List of CarPropertyConfig objects available via Car Hvac Manager.
 */

@Deprecated
public java.util.List<android.car.hardware.CarPropertyConfig> getPropertyList() { throw new RuntimeException("Stub!"); }

/**
 * Check whether a given property is available or disabled based on the cars current state.
 * @param propertyId Value is {@link android.car.hardware.hvac.CarHvacManager#ID_MIRROR_DEFROSTER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_STEERING_WHEEL_HEAT}, {@link android.car.hardware.hvac.CarHvacManager#ID_OUTSIDE_AIR_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_TEMPERATURE_DISPLAY_UNITS}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_ACTUAL}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_RPM}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION_AVAILABLE}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_SEAT_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AUTOMATIC_MODE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AIR_RECIRCULATION_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_DUAL_ZONE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_DEFROST_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_POWER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_AUTO_RECIRC_ON}, or {@link android.car.hardware.hvac.CarHvacManager#ID_WINDOW_DEFROSTER_ON}
 * @return true if the property is AVAILABLE, false otherwise
 */

@Deprecated
public boolean isPropertyAvailable(int propertyId, int area) { throw new RuntimeException("Stub!"); }

/**
 * Get value of boolean property
 * @param propertyId
 * Value is {@link android.car.hardware.hvac.CarHvacManager#ID_MIRROR_DEFROSTER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_STEERING_WHEEL_HEAT}, {@link android.car.hardware.hvac.CarHvacManager#ID_OUTSIDE_AIR_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_TEMPERATURE_DISPLAY_UNITS}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_ACTUAL}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_RPM}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION_AVAILABLE}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_SEAT_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AUTOMATIC_MODE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AIR_RECIRCULATION_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_DUAL_ZONE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_DEFROST_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_POWER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_AUTO_RECIRC_ON}, or {@link android.car.hardware.hvac.CarHvacManager#ID_WINDOW_DEFROSTER_ON}
 * @param area
 * @return value of requested boolean property
 */

@Deprecated
public boolean getBooleanProperty(int propertyId, int area) { throw new RuntimeException("Stub!"); }

/**
 * Get value of float property
 * @param propertyId
 * Value is {@link android.car.hardware.hvac.CarHvacManager#ID_MIRROR_DEFROSTER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_STEERING_WHEEL_HEAT}, {@link android.car.hardware.hvac.CarHvacManager#ID_OUTSIDE_AIR_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_TEMPERATURE_DISPLAY_UNITS}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_ACTUAL}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_RPM}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION_AVAILABLE}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_SEAT_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AUTOMATIC_MODE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AIR_RECIRCULATION_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_DUAL_ZONE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_DEFROST_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_POWER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_AUTO_RECIRC_ON}, or {@link android.car.hardware.hvac.CarHvacManager#ID_WINDOW_DEFROSTER_ON}
 * @param area
 * @return value of requested float property
 */

@Deprecated
public float getFloatProperty(int propertyId, int area) { throw new RuntimeException("Stub!"); }

/**
 * Get value of integer property
 * @param propertyId
 * Value is {@link android.car.hardware.hvac.CarHvacManager#ID_MIRROR_DEFROSTER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_STEERING_WHEEL_HEAT}, {@link android.car.hardware.hvac.CarHvacManager#ID_OUTSIDE_AIR_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_TEMPERATURE_DISPLAY_UNITS}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_ACTUAL}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_RPM}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION_AVAILABLE}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_SEAT_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AUTOMATIC_MODE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AIR_RECIRCULATION_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_DUAL_ZONE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_DEFROST_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_POWER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_AUTO_RECIRC_ON}, or {@link android.car.hardware.hvac.CarHvacManager#ID_WINDOW_DEFROSTER_ON}
 * @param area
 * @return value of requested integer property
 */

@Deprecated
public int getIntProperty(int propertyId, int area) { throw new RuntimeException("Stub!"); }

/**
 * Set the value of a boolean property
 * @param propertyId
 * Value is {@link android.car.hardware.hvac.CarHvacManager#ID_MIRROR_DEFROSTER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_STEERING_WHEEL_HEAT}, {@link android.car.hardware.hvac.CarHvacManager#ID_OUTSIDE_AIR_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_TEMPERATURE_DISPLAY_UNITS}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_ACTUAL}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_RPM}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION_AVAILABLE}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_SEAT_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AUTOMATIC_MODE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AIR_RECIRCULATION_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_DUAL_ZONE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_DEFROST_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_POWER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_AUTO_RECIRC_ON}, or {@link android.car.hardware.hvac.CarHvacManager#ID_WINDOW_DEFROSTER_ON}
 * @param area
 * @param val
 */

@Deprecated
public void setBooleanProperty(int propertyId, int area, boolean val) { throw new RuntimeException("Stub!"); }

/**
 * Set the value of a float property
 * @param propertyId
 * Value is {@link android.car.hardware.hvac.CarHvacManager#ID_MIRROR_DEFROSTER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_STEERING_WHEEL_HEAT}, {@link android.car.hardware.hvac.CarHvacManager#ID_OUTSIDE_AIR_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_TEMPERATURE_DISPLAY_UNITS}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_ACTUAL}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_RPM}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION_AVAILABLE}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_SEAT_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AUTOMATIC_MODE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AIR_RECIRCULATION_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_DUAL_ZONE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_DEFROST_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_POWER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_AUTO_RECIRC_ON}, or {@link android.car.hardware.hvac.CarHvacManager#ID_WINDOW_DEFROSTER_ON}
 * @param area
 * @param val
 */

@Deprecated
public void setFloatProperty(int propertyId, int area, float val) { throw new RuntimeException("Stub!"); }

/**
 * Set the value of an integer property
 * @param propertyId
 * Value is {@link android.car.hardware.hvac.CarHvacManager#ID_MIRROR_DEFROSTER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_STEERING_WHEEL_HEAT}, {@link android.car.hardware.hvac.CarHvacManager#ID_OUTSIDE_AIR_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_TEMPERATURE_DISPLAY_UNITS}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_ACTUAL}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_RPM}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION_AVAILABLE}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_SEAT_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AUTOMATIC_MODE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AIR_RECIRCULATION_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_DUAL_ZONE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_DEFROST_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_POWER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_AUTO_RECIRC_ON}, or {@link android.car.hardware.hvac.CarHvacManager#ID_WINDOW_DEFROSTER_ON}
 * @param area
 * @param val
 */

@Deprecated
public void setIntProperty(int propertyId, int area, int val) { throw new RuntimeException("Stub!"); }

/**
 * Represents fan direction when air flows through defrost vents.
 * This constant must be used with {@link #ID_ZONED_FAN_DIRECTION} property.
 */

@Deprecated public static final int FAN_DIRECTION_DEFROST = 4; // 0x4

/**
 * Represents fan direction when air flows through face directed vents.
 * This constant must be used with {@link #ID_ZONED_FAN_DIRECTION} property.
 */

@Deprecated public static final int FAN_DIRECTION_FACE = 1; // 0x1

/**
 * Represents fan direction when air flows through floor directed vents.
 * This constant must be used with {@link #ID_ZONED_FAN_DIRECTION} property.
 */

@Deprecated public static final int FAN_DIRECTION_FLOOR = 2; // 0x2

/**
 * Mirror defrosters state, bool type
 * true indicates mirror defroster is on
 */

@Deprecated public static final int ID_MIRROR_DEFROSTER_ON = 339739916; // 0x1440050c

/**
 * Outside air temperature, float type
 * Value is in degrees Celsius
 */

@Deprecated public static final int ID_OUTSIDE_AIR_TEMP = 291505923; // 0x11600703

/**
 * Steering wheel temp, int type
 * Positive values indicate heating.
 * Negative values indicate cooling
 */

@Deprecated public static final int ID_STEERING_WHEEL_HEAT = 289408269; // 0x1140050d

/**
 * Temperature units being used, int type
 *  0x30 = Celsius
 *  0x31 = Fahrenheit
 */

@Deprecated public static final int ID_TEMPERATURE_DISPLAY_UNITS = 289408270; // 0x1140050e

/**
 * Defroster ON, bool type
 * Defroster controls are based on window position.
 * True indicates the defroster is ON.
 */

@Deprecated public static final int ID_WINDOW_DEFROSTER_ON = 320865540; // 0x13200504

/**
 * Air ON, bool type
 * true indicates AC is ON.
 */

@Deprecated public static final int ID_ZONED_AC_ON = 354419973; // 0x15200505

/**
 * Air recirculation ON, bool type
 * true indicates recirculation is active.
 */

@Deprecated public static final int ID_ZONED_AIR_RECIRCULATION_ON = 354419976; // 0x15200508

/**
 * Automatic Mode ON, bool type
 * true indicates HVAC is in automatic mode
 */

@Deprecated public static final int ID_ZONED_AUTOMATIC_MODE_ON = 354419978; // 0x1520050a

/** Dual zone ON, bool type
 * true indicates dual zone mode is ON
 */

@Deprecated public static final int ID_ZONED_DUAL_ZONE_ON = 354419977; // 0x15200509

/**
 * Current fan direction setting, int type. The value must be one of the FAN_DIRECTION_AVAILABLE
 * values declared above.
 */

@Deprecated public static final int ID_ZONED_FAN_DIRECTION = 356517121; // 0x15400501

/**
 *  Fan direction available, int vector type
 *  Fan direction is a bitmask of directions available for each zone.
 */

@Deprecated public static final int ID_ZONED_FAN_DIRECTION_AVAILABLE = 356582673; // 0x15410511

/**
 * Actual fan speed, int type
 * Actual fan speed is a read-only value, expressed in RPM.
 */

@Deprecated public static final int ID_ZONED_FAN_SPEED_RPM = 356517135; // 0x1540050f

/**
 * Fan speed setpoint, int type
 * Fan speed is an integer from 0-n, depending on number of fan speeds available.
 */

@Deprecated public static final int ID_ZONED_FAN_SPEED_SETPOINT = 356517120; // 0x15400500

/**
 * Automatic recirculation mode ON
 * true indicates recirculation is in automatic mode
 */

@Deprecated public static final int ID_ZONED_HVAC_AUTO_RECIRC_ON = 354419986; // 0x15200512

/**
 * HVAC system powered on / off, bool type
 * In many vehicles, if the HVAC system is powered off, the SET and GET command will
 * throw an IllegalStateException.  To correct this, need to turn on the HVAC module first
 * before manipulating a parameter.
 */

@Deprecated public static final int ID_ZONED_HVAC_POWER_ON = 354419984; // 0x15200510

/**
 * Max AC ON, bool type
 * true indicates MAX AC is ON
 */

@Deprecated public static final int ID_ZONED_MAX_AC_ON = 354419974; // 0x15200506

/**
 * Max Defrost ON, bool type
 * true indicates max defrost is active.
 */

@Deprecated public static final int ID_ZONED_MAX_DEFROST_ON = 354419975; // 0x15200507

/**
 * Seat temperature, int type
 * Seat temperature is negative for cooling, positive for heating.  Temperature is a
 * setting, i.e. -3 to 3 for 3 levels of cooling and 3 levels of heating.
 */

@Deprecated public static final int ID_ZONED_SEAT_TEMP = 356517131; // 0x1540050b

/**
 * Actual temperature, float type
 * Actual zone temperature is read only value, in terms of F or C.
 */

@Deprecated public static final int ID_ZONED_TEMP_ACTUAL = 358614274; // 0x15600502

/**
 * Temperature setpoint, float type
 * Temperature set by the user, units are in degrees Celsius.
 */

@Deprecated public static final int ID_ZONED_TEMP_SETPOINT = 358614275; // 0x15600503
/**
 * Application registers {@link CarHvacEventCallback} object to receive updates and changes to
 * subscribed Car HVAC properties.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static interface CarHvacEventCallback {

/**
 * Called when a property is updated
 * @param value Property that has been updated.
 */

@Deprecated
public void onChangeEvent(android.car.hardware.CarPropertyValue value);

/**
 * Called when an error is detected with a property
 * @param propertyId
 * Value is {@link android.car.hardware.hvac.CarHvacManager#ID_MIRROR_DEFROSTER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_STEERING_WHEEL_HEAT}, {@link android.car.hardware.hvac.CarHvacManager#ID_OUTSIDE_AIR_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_TEMPERATURE_DISPLAY_UNITS}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_TEMP_ACTUAL}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_SETPOINT}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_SPEED_RPM}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION_AVAILABLE}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_FAN_DIRECTION}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_SEAT_TEMP}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AUTOMATIC_MODE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_AIR_RECIRCULATION_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_AC_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_DUAL_ZONE_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_MAX_DEFROST_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_POWER_ON}, {@link android.car.hardware.hvac.CarHvacManager#ID_ZONED_HVAC_AUTO_RECIRC_ON}, or {@link android.car.hardware.hvac.CarHvacManager#ID_WINDOW_DEFROSTER_ON}
 * @param zone
 */

@Deprecated
public void onErrorEvent(int propertyId, int zone);
}

}

