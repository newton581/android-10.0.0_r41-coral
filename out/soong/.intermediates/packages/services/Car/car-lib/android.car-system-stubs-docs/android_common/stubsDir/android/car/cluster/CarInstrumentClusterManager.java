/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.cluster;

import android.car.Car;

/**
 * API to work with instrument cluster.
 *
 * @deprecated use {@link android.car.CarAppFocusManager} with focus type
 * {@link android.car.CarAppFocusManager#APP_FOCUS_TYPE_NAVIGATION} instead.
 * InstrumentClusterService will automatically launch a "android.car.cluster.NAVIGATION" activity
 * from the package holding navigation focus.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class CarInstrumentClusterManager {

/** @hide */

CarInstrumentClusterManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Starts activity in the instrument cluster.
 *
 * @deprecated see {@link CarInstrumentClusterManager} deprecation message
 *
 * @hide
 */

@Deprecated
public void startActivity(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Caller of this method will receive immediate callback with the most recent state if state
 * exists for given category.
 *
 * @param category category of the activity in the cluster,
 *                         see {@link #CATEGORY_NAVIGATION}
 * @param callback instance of {@link Callback} class to receive events.
 *
 * @deprecated see {@link CarInstrumentClusterManager} deprecation message
 *
 * @hide
 */

@Deprecated
public void registerCallback(java.lang.String category, android.car.cluster.CarInstrumentClusterManager.Callback callback) { throw new RuntimeException("Stub!"); }

/**
 * Unregisters given callback for all activity categories.
 *
 * @param callback previously registered callback
 *
 * @deprecated see {@link CarInstrumentClusterManager} deprecation message
 *
 * @hide
 */

@Deprecated
public void unregisterCallback(android.car.cluster.CarInstrumentClusterManager.Callback callback) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated use {@link android.car.Car#CATEGORY_NAVIGATION} instead
 *
 * @hide
 */

@Deprecated public static final java.lang.String CATEGORY_NAVIGATION = "android.car.cluster.NAVIGATION";

/**
 * When activity in the cluster is launched it will receive {@link ClusterActivityState} in the
 * intent's extra thus activity will know information about unobscured area, etc. upon activity
 * creation.
 *
 * @deprecated use {@link android.car.Car#CATEGORY_NAVIGATION} instead
 *
 * @hide
 */

@Deprecated public static final java.lang.String KEY_EXTRA_ACTIVITY_STATE = "android.car.cluster.ClusterActivityState";
/**
 * @deprecated activity state is not longer being reported. See
 * {@link CarInstrumentClusterManager} deprecation message for more details.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static interface Callback {

/**
 * Notify client that activity state was changed.
 *
 * @param category cluster activity category, see {@link #CATEGORY_NAVIGATION}
 * @param clusterActivityState see {@link ClusterActivityState} how to read this bundle.
 */

@Deprecated
public void onClusterActivityStateChanged(java.lang.String category, android.os.Bundle clusterActivityState);
}

}

