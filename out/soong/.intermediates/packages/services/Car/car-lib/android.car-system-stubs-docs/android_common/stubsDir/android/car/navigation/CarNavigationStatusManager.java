/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.navigation;

import android.car.Car;
import android.os.Bundle;

/**
 * API for providing navigation status for instrument cluster.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarNavigationStatusManager {

/**
 * Only for CarServiceLoader
 * @hide
 */

CarNavigationStatusManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Sends events from navigation app to instrument cluster.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_NAVIGATION_MANAGER}
 * @deprecated use {@link #sendEvent(Bundle)} instead.
 */

@Deprecated
public void sendEvent(int eventType, android.os.Bundle bundle) { throw new RuntimeException("Stub!"); }

/**
 * Sends events from navigation app to instrument cluster.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_NAVIGATION_MANAGER}
 * @param bundle object holding data about the navigation event. This information is
 *               generated using <a href="https://developer.android.com/reference/androidx/car/cluster/navigation/NavigationState.html#toParcelable()">
 *               androidx.car.cluster.navigation.NavigationState#toParcelable()</a>
 */

public void sendNavigationStateChange(android.os.Bundle bundle) { throw new RuntimeException("Stub!"); }

/**
 * Returns navigation features of instrument cluster
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_NAVIGATION_MANAGER}
 */

public android.car.navigation.CarNavigationInstrumentCluster getInstrumentClusterInfo() { throw new RuntimeException("Stub!"); }
}

