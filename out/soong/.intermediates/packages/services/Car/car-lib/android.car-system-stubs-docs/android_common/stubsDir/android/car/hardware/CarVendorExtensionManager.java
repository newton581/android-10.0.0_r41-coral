/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.hardware;

import android.car.Car;
import android.car.hardware.property.CarPropertyManager;

/**
 * @deprecated consider using {@link CarPropertyManager} instead.
 *
 * API to access custom vehicle properties defined by OEMs.
 * <p>
 * System permission {@link Car#PERMISSION_VENDOR_EXTENSION} is required to get this manager.
 * </p>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class CarVendorExtensionManager {

/**
 * Creates an instance of the {@link CarVendorExtensionManager}.
 *
 * <p>Should not be obtained directly by clients, use {@link Car#getCarManager(String)} instead.
 * @hide
 */

CarVendorExtensionManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Registers listener. The methods of the listener will be called when new events arrived in
 * the main thread.
 */

@Deprecated
public void registerCallback(android.car.hardware.CarVendorExtensionManager.CarVendorExtensionCallback callback) { throw new RuntimeException("Stub!"); }

/** Unregisters listener that was previously registered. */

@Deprecated
public void unregisterCallback(android.car.hardware.CarVendorExtensionManager.CarVendorExtensionCallback callback) { throw new RuntimeException("Stub!"); }

/** Get list of properties represented by CarVendorExtensionManager for this car. */

@Deprecated
public java.util.List<android.car.hardware.CarPropertyConfig> getProperties() { throw new RuntimeException("Stub!"); }

/**
 * Check whether a given property is available or disabled based on the cars current state.
 * @return true if the property is AVAILABLE, false otherwise
 */

@Deprecated
public boolean isPropertyAvailable(int propertyId, int area) { throw new RuntimeException("Stub!"); }

/**
 * Returns property value. Use this function for global vehicle properties.
 *
 * @param propertyClass - data type of the given property, for example property that was
 *        defined as {@code VEHICLE_VALUE_TYPE_INT32} in vehicle HAL could be accessed using
 *        {@code Integer.class}.
 * @param propId - property id which is matched with the one defined in vehicle HAL
 */

@Deprecated
public <E> E getGlobalProperty(java.lang.Class<E> propertyClass, int propId) { throw new RuntimeException("Stub!"); }

/**
 * Returns property value. Use this function for "zoned" vehicle properties.
 *
 * @param propertyClass - data type of the given property, for example property that was
 *        defined as {@code VEHICLE_VALUE_TYPE_INT32} in vehicle HAL could be accessed using
 *        {@code Integer.class}.
 * @param propId - property id which is matched with the one defined in vehicle HAL
 * @param area - vehicle area (e.g. {@code VehicleAreaSeat.ROW_1_LEFT}
 *        or {@code VEHICLE_MIRROR_DRIVER_LEFT}
 */

@Deprecated
public <E> E getProperty(java.lang.Class<E> propertyClass, int propId, int area) { throw new RuntimeException("Stub!"); }

/**
 * Call this function to set a value to global vehicle property.
 *
 * @param propertyClass - data type of the given property, for example property that was
 *        defined as {@code VEHICLE_VALUE_TYPE_INT32} in vehicle HAL could be accessed using
 *        {@code Integer.class}.
 * @param propId - property id which is matched with the one defined in vehicle HAL
 * @param value - new value, this object should match a class provided in {@code propertyClass}
 *        argument.
 */

@Deprecated
public <E> void setGlobalProperty(java.lang.Class<E> propertyClass, int propId, E value) { throw new RuntimeException("Stub!"); }

/**
 * Call this function to set a value to "zoned" vehicle property.
 *
 * @param propertyClass - data type of the given property, for example property that was
 *        defined as {@code VEHICLE_VALUE_TYPE_INT32} in vehicle HAL could be accessed using
 *        {@code Integer.class}.
 * @param propId - property id which is matched with the one defined in vehicle HAL
 * @param area - vehicle area (e.g. {@code VehicleAreaSeat.ROW_1_LEFT}
 *        or {@code VEHICLE_MIRROR_DRIVER_LEFT}
 * @param value - new value, this object should match a class provided in {@code propertyClass}
 *        argument.
 */

@Deprecated
public <E> void setProperty(java.lang.Class<E> propertyClass, int propId, int area, E value) { throw new RuntimeException("Stub!"); }
/**
 * Contains callback functions that will be called when some event happens with vehicle
 * property.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static interface CarVendorExtensionCallback {

/** Called when a property is updated */

@Deprecated
public void onChangeEvent(android.car.hardware.CarPropertyValue value);

/** Called when an error is detected with a property */

@Deprecated
public void onErrorEvent(int propertyId, int zone);
}

}

