/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.car.storagemonitoring;


/**
 * uid_io stats about one user ID.
 *
 * Contains information about I/O activity that can be attributed to processes running on
 * behalf of one user of the system, as collected by the kernel.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class IoStatsEntry implements android.os.Parcelable {

public IoStatsEntry(int uid, long runtimeMillis, android.car.storagemonitoring.IoStatsEntry.Metrics foreground, android.car.storagemonitoring.IoStatsEntry.Metrics background) { throw new RuntimeException("Stub!"); }

public IoStatsEntry(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

public IoStatsEntry(android.car.storagemonitoring.UidIoRecord record, long runtimeMillis) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object other) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.storagemonitoring.IoStatsEntry> CREATOR;
static { CREATOR = null; }

/**
 * Statistics for apps running in background.
 */

public final android.car.storagemonitoring.IoStatsEntry.Metrics background;
{ background = null; }

/**
 * Statistics for apps running in foreground.
 */

public final android.car.storagemonitoring.IoStatsEntry.Metrics foreground;
{ foreground = null; }

/**
 * How long any process running on behalf of this user id running for, in milliseconds.
 *
 * This field is allowed to be an approximation and it does not provide any way to
 * relate uptime to specific processes.
 */

public final long runtimeMillis;
{ runtimeMillis = 0; }

/**
 * The user id that this object contains metrics for.
 *
 * In many cases this can be converted to a list of Java app packages installed on the device.
 * In other cases, the user id can refer to either the kernel itself (uid 0), or low-level
 * system services that are running entirely natively.
 */

public final int uid;
{ uid = 0; }
/**
 * I/O activity metrics that pertain to either the foreground or the background state.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Metrics implements android.os.Parcelable {

public Metrics(long bytesRead, long bytesWritten, long bytesReadFromStorage, long bytesWrittenToStorage, long fsyncCalls) { throw new RuntimeException("Stub!"); }

public Metrics(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object other) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.storagemonitoring.IoStatsEntry.Metrics> CREATOR;
static { CREATOR = null; }

/**
 * Total bytes that processes running on behalf of this user obtained
 * via read() system calls.
 */

public final long bytesRead;
{ bytesRead = 0; }

/**
 * Total bytes that processes running on behalf of this user obtained
 * via read() system calls that actually were served by physical storage.
 */

public final long bytesReadFromStorage;
{ bytesReadFromStorage = 0; }

/**
 * Total bytes that processes running on behalf of this user transferred
 * via write() system calls.
 */

public final long bytesWritten;
{ bytesWritten = 0; }

/**
 * Total bytes that processes running on behalf of this user transferred
 * via write() system calls that were actually sent to physical storage.
 */

public final long bytesWrittenToStorage;
{ bytesWrittenToStorage = 0; }

/**
 * Total number of fsync() system calls that processes running on behalf of this user made.
 */

public final long fsyncCalls;
{ fsyncCalls = 0; }
}

}

