/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.projection;

import android.os.Bundle;
import android.app.ActivityOptions;

/**
 * This class holds OEM customization for projection receiver app.  It is created by Car Service.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ProjectionOptions {

/**
 * Creates new instance for given {@code Bundle}
 *
 * @param bundle contains OEM specific information
 */

public ProjectionOptions(android.os.Bundle bundle) { throw new RuntimeException("Stub!"); }

/**
 * Returns combination of flags from View.SYSTEM_UI_FLAG_* which will be used by projection
 * receiver app during rendering.

 * @return Value is {@link android.car.projection.ProjectionOptions#UI_MODE_FULL_SCREEN}, or {@link android.car.projection.ProjectionOptions#UI_MODE_BLENDED}
 */

public int getUiMode() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@link ActivityOptions} that needs to be applied when launching projection activity
 */

public android.app.ActivityOptions getActivityOptions() { throw new RuntimeException("Stub!"); }

/**
 * Returns package/activity name of the consent activity provided by OEM which needs to be shown
 * for all mobile devices unless user accepted the consent.
 *
 * <p>If the method returns null then consent dialog should not be shown.
 */

public android.content.ComponentName getConsentActivity() { throw new RuntimeException("Stub!"); }

/** Converts current object to {@link Bundle} */

public android.os.Bundle toBundle() { throw new RuntimeException("Stub!"); }

/** @hide */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** Show status and navigation bars. */

public static final int UI_MODE_BLENDED = 1; // 0x1

/** Immersive full screen mode (all system bars are hidden) */

public static final int UI_MODE_FULL_SCREEN = 0; // 0x0
}

