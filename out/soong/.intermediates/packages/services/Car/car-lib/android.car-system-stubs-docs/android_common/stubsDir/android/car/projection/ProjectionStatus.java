/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.projection;

import android.os.Parcelable;

/**
 * This class encapsulates information about projection status and connected mobile devices.
 *
 * <p>Since the number of connected devices expected to be small we include information about
 * connected devices in every status update.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ProjectionStatus implements android.os.Parcelable {

ProjectionStatus(android.car.projection.ProjectionStatus.Builder builder) { throw new RuntimeException("Stub!"); }

/** Parcelable implementation */

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** Returns projection state which could be one of the constants starting with
 * {@code #PROJECTION_STATE_}.

 * @return Value is {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_INACTIVE}, {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_READY_TO_PROJECT}, {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_ACTIVE_FOREGROUND}, or {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_ACTIVE_BACKGROUND}
 */

public int getState() { throw new RuntimeException("Stub!"); }

/** Returns package name of the projection receiver app. */

public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/** Returns extra information provided by projection receiver app */

public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/** Returns true if currently projecting either in the foreground or in the background. */

public boolean isActive() { throw new RuntimeException("Stub!"); }

/** Returns transport which is used for active projection or
 * {@link #PROJECTION_TRANSPORT_NONE} if projection is not running.

 * @return Value is {@link android.car.projection.ProjectionStatus#PROJECTION_TRANSPORT_NONE}, {@link android.car.projection.ProjectionStatus#PROJECTION_TRANSPORT_USB}, or {@link android.car.projection.ProjectionStatus#PROJECTION_TRANSPORT_WIFI}
 */

public int getTransport() { throw new RuntimeException("Stub!"); }

/** Returns a list of currently connected mobile devices. */

public java.util.List<android.car.projection.ProjectionStatus.MobileDevice> getConnectedMobileDevices() { throw new RuntimeException("Stub!"); }

/**
 * Returns new {@link Builder} instance.
 *
 * @param packageName package name that will be associated with this status
 * @param state current projection state, must be one of the {@code PROJECTION_STATE_*}

 * Value is {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_INACTIVE}, {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_READY_TO_PROJECT}, {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_ACTIVE_FOREGROUND}, or {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_ACTIVE_BACKGROUND}
 */

public static android.car.projection.ProjectionStatus.Builder builder(java.lang.String packageName, int state) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** Creator for this class. Required to have in parcelable implementations. */

public static final android.os.Parcelable.Creator<android.car.projection.ProjectionStatus> CREATOR;
static { CREATOR = null; }

/** Projection is running in the background */

public static final int PROJECTION_STATE_ACTIVE_BACKGROUND = 3; // 0x3

/** Projecting in the foreground */

public static final int PROJECTION_STATE_ACTIVE_FOREGROUND = 2; // 0x2

/** This state indicates that projection is not actively running and no compatible mobile
 * devices available. */

public static final int PROJECTION_STATE_INACTIVE = 0; // 0x0

/** At least one phone connected and ready to project. */

public static final int PROJECTION_STATE_READY_TO_PROJECT = 1; // 0x1

/** This status is used when projection is not actively running */

public static final int PROJECTION_TRANSPORT_NONE = 0; // 0x0

/** This status is used when projection is not actively running */

public static final int PROJECTION_TRANSPORT_USB = 1; // 0x1

/** This status is used when projection is not actively running */

public static final int PROJECTION_TRANSPORT_WIFI = 2; // 0x2
/** Builder class for {@link ProjectionStatus} */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

Builder(java.lang.String packageName, int state) { throw new RuntimeException("Stub!"); }

/**
 * Sets the transport which is used for currently projecting phone if any.
 *
 * @param transport transport of current projection, must be one of the
 * {@code PROJECTION_TRANSPORT_*}

 * Value is {@link android.car.projection.ProjectionStatus#PROJECTION_TRANSPORT_NONE}, {@link android.car.projection.ProjectionStatus#PROJECTION_TRANSPORT_USB}, or {@link android.car.projection.ProjectionStatus#PROJECTION_TRANSPORT_WIFI}
 */

public android.car.projection.ProjectionStatus.Builder setProjectionTransport(int transport) { throw new RuntimeException("Stub!"); }

/**
 * Add connected mobile device
 *
 * @param mobileDevice connected mobile device
 * @return this builder
 */

public android.car.projection.ProjectionStatus.Builder addMobileDevice(android.car.projection.ProjectionStatus.MobileDevice mobileDevice) { throw new RuntimeException("Stub!"); }

/**
 * Add extra information.
 *
 * @param extras may contain an extra information that can be passed from the projection
 * app to the projection status listeners
 * @return this builder
 */

public android.car.projection.ProjectionStatus.Builder setExtras(android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/** Creates {@link ProjectionStatus} object. */

public android.car.projection.ProjectionStatus build() { throw new RuntimeException("Stub!"); }
}

/** Class that represents information about connected mobile device. */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class MobileDevice implements android.os.Parcelable {

MobileDevice(android.car.projection.ProjectionStatus.MobileDevice.Builder builder) { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** Returns the device id which uniquely identifies the mobile device within projection  */

public int getId() { throw new RuntimeException("Stub!"); }

/** Returns the name of the device */

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

/** Returns a list of available projection transports. See {@code PROJECTION_TRANSPORT_*}
 * for possible values. */

public java.util.List<java.lang.Integer> getAvailableTransports() { throw new RuntimeException("Stub!"); }

/** Indicates whether this mobile device is currently projecting */

public boolean isProjecting() { throw new RuntimeException("Stub!"); }

/** Returns extra information for mobile device */

public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/** Parcelable implementation */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Creates new instance of {@link Builder}
 *
 * @param id uniquely identifies the device
 * @param name name of the connected device
 * @return the instance of {@link Builder}
 */

public static android.car.projection.ProjectionStatus.MobileDevice.Builder builder(int id, java.lang.String name) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** Creator for this class. Required to have in parcelable implementations. */

public static final android.os.Parcelable.Creator<android.car.projection.ProjectionStatus.MobileDevice> CREATOR;
static { CREATOR = null; }
/**
 * Builder class for {@link MobileDevice}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

Builder(int id, java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Add supported transport
 *
 * @param transport supported transport by given device, must be one of the
 * {@code PROJECTION_TRANSPORT_*}
 * Value is {@link android.car.projection.ProjectionStatus#PROJECTION_TRANSPORT_NONE}, {@link android.car.projection.ProjectionStatus#PROJECTION_TRANSPORT_USB}, or {@link android.car.projection.ProjectionStatus#PROJECTION_TRANSPORT_WIFI}
 * @return this builder
 */

public android.car.projection.ProjectionStatus.MobileDevice.Builder addTransport(int transport) { throw new RuntimeException("Stub!"); }

/**
 * Indicate whether the mobile device currently projecting or not.
 *
 * @param projecting {@code True} if this mobile device currently projecting
 * @return this builder
 */

public android.car.projection.ProjectionStatus.MobileDevice.Builder setProjecting(boolean projecting) { throw new RuntimeException("Stub!"); }

/**
 * Add extra information for mobile device
 *
 * @param extras provides an arbitrary extra information about this mobile device
 * @return this builder
 */

public android.car.projection.ProjectionStatus.MobileDevice.Builder setExtras(android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/** Creates new instance of {@link MobileDevice} */

public android.car.projection.ProjectionStatus.MobileDevice build() { throw new RuntimeException("Stub!"); }
}

}

}

