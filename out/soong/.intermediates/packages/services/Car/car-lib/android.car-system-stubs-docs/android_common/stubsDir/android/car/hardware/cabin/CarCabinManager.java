/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.hardware.cabin;

import android.car.Car;
import android.car.hardware.property.CarPropertyManager;
import android.car.hardware.CarPropertyConfig;
import java.util.List;

/**
 * @deprecated Use {@link CarPropertyManager} instead.
 *
 * API for controlling Cabin system in cars.
 * Most Car Cabin properties have both a MOVE and POSITION parameter associated with them.
 *
 * The MOVE parameter will start moving the device in the indicated direction.  Magnitude
 * indicates relative speed.  For instance, setting the WINDOW_MOVE parameter to +1 rolls
 * the window down.  Setting it to +2 (if available) will roll it down faster.
 *
 * POSITION parameter will move the device to the desired position.  For instance, if the
 * WINDOW_POS has a range of 0-100, setting this parameter to 50 will open the window
 * halfway.  Depending upon the initial position, the window may move up or down to the
 * 50% value.
 *
 * One or both of the MOVE/POSITION parameters may be implemented depending upon the
 * capability of the hardware.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class CarCabinManager {

/**
 * Get an instance of CarCabinManager
 *
 * Should not be obtained directly by clients, use {@link Car#getCarManager(String)} instead.
 * @param service
 * @param context
 * @param handler
 * @hide
 */

CarCabinManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * All properties in CarCabinManager are zoned.
 * @param propertyId
 * Value is {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_FOLD}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SELECT}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SET}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_BUCKLED}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_MOVE}, or {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_LOCK}
 * @return true if property is a zoned type
 */

@Deprecated
public static boolean isZonedProperty(int propertyId) { throw new RuntimeException("Stub!"); }

/**
 * Implement wrappers for contained CarPropertyManagerBase object
 * @param callback
 */

@Deprecated
public synchronized void registerCallback(android.car.hardware.cabin.CarCabinManager.CarCabinEventCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Stop getting property updates for the given callback. If there are multiple registrations for
 * this listener, all listening will be stopped.
 * @param callback
 */

@Deprecated
public synchronized void unregisterCallback(android.car.hardware.cabin.CarCabinManager.CarCabinEventCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Get list of properties represented by CarCabinManager for this car.
 * @return List of CarPropertyConfig objects available via Car Cabin Manager.
 */

@Deprecated
public java.util.List<android.car.hardware.CarPropertyConfig> getPropertyList() { throw new RuntimeException("Stub!"); }

/**
 * Get value of boolean property
 * @param propertyId
 * Value is {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_FOLD}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SELECT}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SET}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_BUCKLED}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_MOVE}, or {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_LOCK}
 * @param area
 * @return value of requested boolean property
 */

@Deprecated
public boolean getBooleanProperty(int propertyId, int area) { throw new RuntimeException("Stub!"); }

/**
 * Get value of float property
 * @param propertyId
 * Value is {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_FOLD}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SELECT}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SET}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_BUCKLED}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_MOVE}, or {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_LOCK}
 * @param area
 * @return value of requested float property
 */

@Deprecated
public float getFloatProperty(int propertyId, int area) { throw new RuntimeException("Stub!"); }

/**
 * Get value of integer property
 * @param propertyId
 * Value is {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_FOLD}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SELECT}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SET}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_BUCKLED}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_MOVE}, or {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_LOCK}
 * @param area
 * @return value of requested integer property
 */

@Deprecated
public int getIntProperty(int propertyId, int area) { throw new RuntimeException("Stub!"); }

/**
 * Set the value of a boolean property
 * @param propertyId
 * Value is {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_FOLD}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SELECT}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SET}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_BUCKLED}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_MOVE}, or {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_LOCK}
 * @param area
 * @param val
 */

@Deprecated
public void setBooleanProperty(int propertyId, int area, boolean val) { throw new RuntimeException("Stub!"); }

/**
 * Set the value of a float property
 * @param propertyId
 * Value is {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_FOLD}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SELECT}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SET}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_BUCKLED}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_MOVE}, or {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_LOCK}
 * @param area
 * @param val
 */

@Deprecated
public void setFloatProperty(int propertyId, int area, float val) { throw new RuntimeException("Stub!"); }

/**
 * Set the value of an integer property
 * @param propertyId
 * Value is {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_FOLD}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SELECT}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SET}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_BUCKLED}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_MOVE}, or {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_LOCK}
 * @param area
 * @param val
 */

@Deprecated
public void setIntProperty(int propertyId, int area, int val) { throw new RuntimeException("Stub!"); }

/** door lock, bool type
 * 'true' indicates door is locked.
 */

@Deprecated public static final int ID_DOOR_LOCK = 371198722; // 0x16200b02

/** door move, int type
 * Positive values open the door, negative values close it.
 */

@Deprecated public static final int ID_DOOR_MOVE = 373295873; // 0x16400b01

/**
 * door position, int type
 * Max value indicates fully open, min value (0) indicates fully closed.
 *
 * Some vehicles (minivans) can open the door electronically.  Hence, the ability
 * to write this property.
 */

@Deprecated public static final int ID_DOOR_POS = 373295872; // 0x16400b00

/**
 * mirror fold, bool type
 * True indicates mirrors are folded.
 */

@Deprecated public static final int ID_MIRROR_FOLD = 287312709; // 0x11200b45

/**
 * mirror lock, bool type
 * True indicates mirror positions are locked and not changeable.
 */

@Deprecated public static final int ID_MIRROR_LOCK = 287312708; // 0x11200b44

/** mirror y move, int type
 * Positive value tilts the mirror right, negative value tilts left.
 */

@Deprecated public static final int ID_MIRROR_Y_MOVE = 339741507; // 0x14400b43

/**
 * mirror y position, int type
 * Positive value indicates tilt right, negative value tilt left
 */

@Deprecated public static final int ID_MIRROR_Y_POS = 339741506; // 0x14400b42

/** mirror z move, int type
 * Positive value tilts the mirror upwards, negative value tilts downwards.
 */

@Deprecated public static final int ID_MIRROR_Z_MOVE = 339741505; // 0x14400b41

/**
 * mirror z position, int type
 * Positive value indicates tilt upwards, negative value tilt downwards.
 */

@Deprecated public static final int ID_MIRROR_Z_POS = 339741504; // 0x14400b40

/** seat backrest angle #1 move, int type
 * Backrest angle 1 is the actuator closest to the bottom of the seat.
 * Positive value angles seat towards the steering wheel.
 * Negatie value angles away from steering wheel.
 */

@Deprecated public static final int ID_SEAT_BACKREST_ANGLE_1_MOVE = 356518792; // 0x15400b88

/**
 * seat backrest angle #1 position, int type
 * Backrest angle 1 is the actuator closest to the bottom of the seat.
 * Max value indicates angling forward towards the steering wheel.
 * Min value indicates full recline.
 */

@Deprecated public static final int ID_SEAT_BACKREST_ANGLE_1_POS = 356518791; // 0x15400b87

/** seat backrest angle #2 move, int type
 * Backrest angle 2 is the next actuator up from the bottom of the seat.
 * Positive value tilts forward towards the steering wheel.
 * Negative value tilts backwards.
 */

@Deprecated public static final int ID_SEAT_BACKREST_ANGLE_2_MOVE = 356518794; // 0x15400b8a

/**
 * seat backrest angle #2 position, int type
 * Backrest angle 2 is the next actuator up from the bottom of the seat.
 * Max value indicates angling forward towards the steering wheel.
 * Min value indicates full recline.
 */

@Deprecated public static final int ID_SEAT_BACKREST_ANGLE_2_POS = 356518793; // 0x15400b89

/**
 * seat belt buckled, bool type
 * True indicates belt is buckled.
 */

@Deprecated public static final int ID_SEAT_BELT_BUCKLED = 354421634; // 0x15200b82

/** seat belt height move, int type
 * Adjusts the shoulder belt anchor point.
 * Positive value moves towards highest point.
 * Negative value moves towards lowest point.
 */

@Deprecated public static final int ID_SEAT_BELT_HEIGHT_MOVE = 356518788; // 0x15400b84

/**
 * seat belt height position, int type
 * Adjusts the shoulder belt anchor point.
 * Max value indicates highest position.
 * Min value indicates lowest position.
 */

@Deprecated public static final int ID_SEAT_BELT_HEIGHT_POS = 356518787; // 0x15400b83

/** seat depth move, int type
 * Adjusts the seat depth, distance from back rest to front edge of seat.
 * Positive value increases the distance from back rest to front edge of seat.
 * Negative value decreases this distance.
 */

@Deprecated public static final int ID_SEAT_DEPTH_MOVE = 356518798; // 0x15400b8e

/**
 * seat depth position, int type
 * Sets the seat depth, distance from back rest to front edge of seat.
 * Max value indicates longest depth position.
 * Min value indicates shortest position.
 */

@Deprecated public static final int ID_SEAT_DEPTH_POS = 356518797; // 0x15400b8d

/**
 * seat fore/aft move, int type
 * Positive value moves seat forward (closer to steering wheel).
 * Negative value moves seat rearward.
 */

@Deprecated public static final int ID_SEAT_FORE_AFT_MOVE = 356518790; // 0x15400b86

/**
 * seat fore/aft position, int type
 * Sets the seat position forward (closer to steering wheel) and backwards.
 * Max value indicates closest to wheel, min value indicates most rearward position.
 */

@Deprecated public static final int ID_SEAT_FORE_AFT_POS = 356518789; // 0x15400b85

/** seat headrest angle move, int type
 * Adjusts the angle of the headrest.
 * Positive value angles headrest towards most upright angle.
 * Negative value angles headrest towards shallowest headrest angle.
 */

@Deprecated public static final int ID_SEAT_HEADREST_ANGLE_MOVE = 356518808; // 0x15400b98

/**
 * seat headrest angle position, int type
 * Sets the angle of the headrest.
 * Max value indicates most upright angle.
 * Min value indicates shallowest headrest angle.
 */

@Deprecated public static final int ID_SEAT_HEADREST_ANGLE_POS = 356518807; // 0x15400b97

/** seat headrest fore/aft move, int type
 * Adjsuts the headrest forwards and backwards.
 * Positive value moves the headrest closer to front of car.
 * Negative value moves the headrest closer to rear of car.
 */

@Deprecated public static final int ID_SEAT_HEADREST_FORE_AFT_MOVE = 356518810; // 0x15400b9a

/**
 * seat headrest fore/aft position, int type
 * Sets the headrest forwards and backwards.
 * Max value indicates position closest to front of car.
 * Min value indicates position closest to rear of car.
 */

@Deprecated public static final int ID_SEAT_HEADREST_FORE_AFT_POS = 356518809; // 0x15400b99

/** seat headrest height move, int type
 * Postive value moves the headrest higher.
 * Negative value moves the headrest lower.
 */

@Deprecated public static final int ID_SEAT_HEADREST_HEIGHT_MOVE = 356518806; // 0x15400b96

/**
 * seat headrest height position, int type
 * Sets the headrest height.
 * Max value indicates tallest setting.
 * Min value indicates shortest setting.
 */

@Deprecated public static final int ID_SEAT_HEADREST_HEIGHT_POS = 356518805; // 0x15400b95

/** seat height move, int type
 * Sets the seat height.
 * Positive value raises the seat.
 * Negative value lowers the seat.
 * */

@Deprecated public static final int ID_SEAT_HEIGHT_MOVE = 356518796; // 0x15400b8c

/**
 * seat height position, int type
 * Sets the seat height.
 * Max value indicates highest position.
 * Min value indicates lowest position.
 */

@Deprecated public static final int ID_SEAT_HEIGHT_POS = 356518795; // 0x15400b8b

/** seat lumbar fore/aft move, int type
 * Adjusts the lumbar support forwards and backwards.
 * Positive value moves lumbar support forward.
 * Negative value moves lumbar support rearward.
 */

@Deprecated public static final int ID_SEAT_LUMBAR_FORE_AFT_MOVE = 356518802; // 0x15400b92

/**
 * seat lumbar fore/aft position, int type
 * Pushes the lumbar support forward and backwards.
 * Max value indicates most forward position.
 * Min value indicates most rearward position.
 */

@Deprecated public static final int ID_SEAT_LUMBAR_FORE_AFT_POS = 356518801; // 0x15400b91

/** seat lumbar side support move, int type
 * Adjusts the amount of lateral lumbar support.
 * Positive value widens the lumbar area.
 * Negative value makes the lumbar area thinner.
 */

@Deprecated public static final int ID_SEAT_LUMBAR_SIDE_SUPPORT_MOVE = 356518804; // 0x15400b94

/**
 * seat lumbar side support position, int type
 * Sets the amount of lateral lumbar support.
 * Max value indicates widest lumbar setting (i.e. least support)
 * Min value indicates thinnest lumbar setting.
 */

@Deprecated public static final int ID_SEAT_LUMBAR_SIDE_SUPPORT_POS = 356518803; // 0x15400b93

/**
 * seat memory select, int type
 * This parameter selects the memory preset to use to select the seat position.
 * The minValue is always 1, and the maxValue determines the number of seat
 * positions available.
 *
 * For instance, if the driver's seat has 3 memory presets, the maxValue will be 3.
 * When the user wants to select a preset, the desired preset number (1, 2, or 3)
 * is set.
 */

@Deprecated public static final int ID_SEAT_MEMORY_SELECT = 356518784; // 0x15400b80

/**
 * seat memory set, int type
 * This setting allows the user to save the current seat position settings into
 * the selected preset slot.  The maxValue for each seat position shall match
 * the maxValue for VEHICLE_PROPERTY_SEAT_MEMORY_SELECT.
 */

@Deprecated public static final int ID_SEAT_MEMORY_SET = 356518785; // 0x15400b81

/** seat tilt move, int type
 * Adjusts the seat tilt.
 * Positive value lifts front edge of seat higher than back edge.
 * Negative value lowers front edge of seat in relation to back edge.
 */

@Deprecated public static final int ID_SEAT_TILT_MOVE = 356518800; // 0x15400b90

/**
 * seat tilt position, int type
 * Sets the seat tilt.
 * Max value indicates front edge of seat higher than back edge.
 * Min value indicates front edge of seat lower than back edge.
 */

@Deprecated public static final int ID_SEAT_TILT_POS = 356518799; // 0x15400b8f

/**
 * window lock, bool type
 * True indicates windows are locked and can't be moved.
 */

@Deprecated public static final int ID_WINDOW_LOCK = 322964420; // 0x13400bc4

/** window move, int type
 * Positive value moves window down / opens window.
 * Negative value moves window up / closes window.
 */

@Deprecated public static final int ID_WINDOW_MOVE = 322964417; // 0x13400bc1

/**
 * window position, int type
 * Max = window down / open.
 * Min = window up / closed.
 */

@Deprecated public static final int ID_WINDOW_POS = 322964416; // 0x13400bc0
/**
 * Application registers CarCabinEventCallback object to receive updates and changes to
 * subscribed Car Cabin properties.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static interface CarCabinEventCallback {

/**
 * Called when a property is updated
 * @param value Property that has been updated.
 */

@Deprecated
public void onChangeEvent(android.car.hardware.CarPropertyValue value);

/**
 * Called when an error is detected with a property
 * @param propertyId
 * Value is {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_DOOR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Z_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_Y_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_LOCK}, {@link android.car.hardware.cabin.CarCabinManager#ID_MIRROR_FOLD}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SELECT}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_MEMORY_SET}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_BUCKLED}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BELT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_1_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_BACKREST_ANGLE_2_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_DEPTH_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_TILT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_LUMBAR_SIDE_SUPPORT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_HEIGHT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_ANGLE_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_SEAT_HEADREST_FORE_AFT_MOVE}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_POS}, {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_MOVE}, or {@link android.car.hardware.cabin.CarCabinManager#ID_WINDOW_LOCK}
 * @param zone
 */

@Deprecated
public void onErrorEvent(int propertyId, int zone);
}

}

