/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car;

import android.os.Handler;
import android.view.KeyEvent;
import java.util.concurrent.Executor;
import android.os.Bundle;
import android.car.projection.ProjectionOptions;
import java.util.Set;
import android.car.projection.ProjectionStatus;

/**
 * CarProjectionManager allows applications implementing projection to register/unregister itself
 * with projection manager, listen for voice notification.
 *
 * A client must have {@link Car#PERMISSION_CAR_PROJECTION} permission in order to access this
 * manager.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarProjectionManager {

/**
 * @hide
 */

CarProjectionManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Register listener to monitor projection. Only one listener can be registered and
 * registering multiple times will lead into only the last listener to be active.
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param listener
 * @param voiceSearchFilter Flags of voice search requests to get notification.
 */

public void registerProjectionListener(android.car.CarProjectionManager.CarProjectionListener listener, int voiceSearchFilter) { throw new RuntimeException("Stub!"); }

/**
 * Unregister listener and stop listening projection events.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 */

public void unregisterProjectionListener() { throw new RuntimeException("Stub!"); }

/**
 * Adds a {@link ProjectionKeyEventHandler} to be called for the given set of key events.
 *
 * If the given event handler is already registered, the event set and {@link Executor} for that
 * event handler will be replaced with those provided.
 *
 * For any event with a defined event handler, the system will suppress its default behavior for
 * that event, and call the event handler instead. (For instance, if an event handler is defined
 * for {@link #KEY_EVENT_CALL_SHORT_PRESS_KEY_UP}, the system will not open the dialer when the
 * {@link KeyEvent#KEYCODE_CALL CALL} key is short-pressed.)
 *
 * Callbacks on the event handler will be run on the {@link Handler} designated to run callbacks
 * from {@link Car}.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param events        The set of key events to which to subscribe.
 * @param eventHandler  The {@link ProjectionKeyEventHandler} to call when those events occur.
 */

public void addKeyEventHandler(java.util.Set<java.lang.Integer> events, android.car.CarProjectionManager.ProjectionKeyEventHandler eventHandler) { throw new RuntimeException("Stub!"); }

/**
 * Adds a {@link ProjectionKeyEventHandler} to be called for the given set of key events.
 *
 * If the given event handler is already registered, the event set and {@link Executor} for that
 * event handler will be replaced with those provided.
 *
 * For any event with a defined event handler, the system will suppress its default behavior for
 * that event, and call the event handler instead. (For instance, if an event handler is defined
 * for {@link #KEY_EVENT_CALL_SHORT_PRESS_KEY_UP}, the system will not open the dialer when the
 * {@link KeyEvent#KEYCODE_CALL CALL} key is short-pressed.)
 *
 * Callbacks on the event handler will be run on the given {@link Executor}, or, if it is null,
 * the {@link Handler} designated to run callbacks for {@link Car}.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param events        The set of key events to which to subscribe.
 * @param executor      An {@link Executor} on which to run callbacks.
 * @param eventHandler  The {@link ProjectionKeyEventHandler} to call when those events occur.
 */

public void addKeyEventHandler(java.util.Set<java.lang.Integer> events, java.util.concurrent.Executor executor, android.car.CarProjectionManager.ProjectionKeyEventHandler eventHandler) { throw new RuntimeException("Stub!"); }

/**
 * Removes a previously registered {@link ProjectionKeyEventHandler}.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param eventHandler The listener to remove.
 */

public void removeKeyEventHandler(android.car.CarProjectionManager.ProjectionKeyEventHandler eventHandler) { throw new RuntimeException("Stub!"); }

/**
 * Registers projection runner on projection start with projection service
 * to create reverse binding.
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param serviceIntent
 */

public void registerProjectionRunner(android.content.Intent serviceIntent) { throw new RuntimeException("Stub!"); }

/**
 * Unregisters projection runner on projection stop with projection service to create
 * reverse binding.
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param serviceIntent
 */

public void unregisterProjectionRunner(android.content.Intent serviceIntent) { throw new RuntimeException("Stub!"); }

/**
 * Request to start Wi-Fi access point if it hasn't been started yet for wireless projection
 * receiver app.
 *
 * <p>A process can have only one request to start an access point, subsequent call of this
 * method will invalidate previous calls.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param callback to receive notifications when access point status changed for the request
 */

public void startProjectionAccessPoint(android.car.CarProjectionManager.ProjectionAccessPointCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Returns a list of available Wi-Fi channels. A channel is specified as frequency in MHz,
 * e.g. channel 1 will be represented as 2412 in the list.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param band one of the values from {@code android.net.wifi.WifiScanner#WIFI_BAND_*}
 */

public java.util.List<java.lang.Integer> getAvailableWifiChannels(int band) { throw new RuntimeException("Stub!"); }

/**
 * Stop Wi-Fi Access Point for wireless projection receiver app.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 */

public void stopProjectionAccessPoint() { throw new RuntimeException("Stub!"); }

/**
 * Request to disconnect the given profile on the given device, and prevent it from reconnecting
 * until either the request is released, or the process owning the given token dies.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param device  The device on which to inhibit a profile.
 * @param profile The {@link android.bluetooth.BluetoothProfile} to inhibit.
 * @return True if the profile was successfully inhibited, false if an error occurred.
 */

public boolean requestBluetoothProfileInhibit(android.bluetooth.BluetoothDevice device, int profile) { throw new RuntimeException("Stub!"); }

/**
 * Release an inhibit request made by {@link #requestBluetoothProfileInhibit}, and reconnect the
 * profile if no other inhibit requests are active.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param device  The device on which to release the inhibit request.
 * @param profile The profile on which to release the inhibit request.
 * @return True if the request was released, false if an error occurred.
 */

public boolean releaseBluetoothProfileInhibit(android.bluetooth.BluetoothDevice device, int profile) { throw new RuntimeException("Stub!"); }

/**
 * Call this method to report projection status of your app. The aggregated status (from other
 * projection apps if available) will be broadcasted to interested parties.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 * @param status the reported status that will be distributed to the interested listeners
 *
 * @see #registerProjectionStatusListener(ProjectionStatusListener)
 */

public void updateProjectionStatus(android.car.projection.ProjectionStatus status) { throw new RuntimeException("Stub!"); }

/**
 * Register projection status listener. See {@link ProjectionStatusListener} for details. It is
 * allowed to register multiple listeners.
 *
 * <p>Note: provided listener will be called immediately with the most recent status.
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION_STATUS}
 * @param listener the listener to receive notification for any projection status changes
 */

public void registerProjectionStatusListener(android.car.CarProjectionManager.ProjectionStatusListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Unregister provided listener from projection status notifications
 *
 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION_STATUS}
 * @param listener the listener for projection status notifications that was previously
 * registered with {@link #unregisterProjectionStatusListener(ProjectionStatusListener)}
 */

public void unregisterProjectionStatusListener(android.car.CarProjectionManager.ProjectionStatusListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Returns {@link Bundle} object that contains customization for projection app. This bundle
 * can be parsed using {@link ProjectionOptions}.

 * <br>
 * Requires {@link android.car.Car#PERMISSION_CAR_PROJECTION}
 */

public android.os.Bundle getProjectionOptions() { throw new RuntimeException("Stub!"); }

/**
 * Event for {@link #addKeyEventHandler}: fired when the {@link KeyEvent#KEYCODE_CALL} key is
 * pressed down.
 *
 * If the key is released before the long-press timeout,
 * {@link #KEY_EVENT_CALL_SHORT_PRESS_KEY_UP} will be fired. If the key is held past the
 * long-press timeout, {@link #KEY_EVENT_CALL_LONG_PRESS_KEY_DOWN} will be fired, followed by
 * {@link #KEY_EVENT_CALL_LONG_PRESS_KEY_UP}.
 */

public static final int KEY_EVENT_CALL_KEY_DOWN = 4; // 0x4

/**
 * Event for {@link #addKeyEventHandler}: fired when the {@link KeyEvent#KEYCODE_CALL} key is
 * held down past the long-press timeout.
 */

public static final int KEY_EVENT_CALL_LONG_PRESS_KEY_DOWN = 6; // 0x6

/**
 * Event for {@link #addKeyEventHandler}: fired when the {@link KeyEvent#KEYCODE_CALL} key is
 * released after a long-press.
 */

public static final int KEY_EVENT_CALL_LONG_PRESS_KEY_UP = 7; // 0x7

/**
 * Event for {@link #addKeyEventHandler}: fired when the {@link KeyEvent#KEYCODE_CALL} key is
 * released after a short-press.
 */

public static final int KEY_EVENT_CALL_SHORT_PRESS_KEY_UP = 5; // 0x5

/**
 * Event for {@link #addKeyEventHandler}: fired when the {@link KeyEvent#KEYCODE_VOICE_ASSIST}
 * key is pressed down.
 *
 * If the key is released before the long-press timeout,
 * {@link #KEY_EVENT_VOICE_SEARCH_SHORT_PRESS_KEY_UP} will be fired. If the key is held past the
 * long-press timeout, {@link #KEY_EVENT_VOICE_SEARCH_LONG_PRESS_KEY_DOWN} will be fired,
 * followed by {@link #KEY_EVENT_VOICE_SEARCH_LONG_PRESS_KEY_UP}.
 */

public static final int KEY_EVENT_VOICE_SEARCH_KEY_DOWN = 0; // 0x0

/**
 * Event for {@link #addKeyEventHandler}: fired when the {@link KeyEvent#KEYCODE_VOICE_ASSIST}
 * key is held down past the long-press timeout.
 */

public static final int KEY_EVENT_VOICE_SEARCH_LONG_PRESS_KEY_DOWN = 2; // 0x2

/**
 * Event for {@link #addKeyEventHandler}: fired when the {@link KeyEvent#KEYCODE_VOICE_ASSIST}
 * key is released after a long-press.
 */

public static final int KEY_EVENT_VOICE_SEARCH_LONG_PRESS_KEY_UP = 3; // 0x3

/**
 * Event for {@link #addKeyEventHandler}: fired when the {@link KeyEvent#KEYCODE_VOICE_ASSIST}
 * key is released after a short-press.
 */

public static final int KEY_EVENT_VOICE_SEARCH_SHORT_PRESS_KEY_UP = 1; // 0x1

/**
 * Flag for {@link #registerProjectionListener(CarProjectionListener, int)}: subscribe to
 * voice-search long-press requests.
 *
 * @deprecated Use {@link #addKeyEventHandler(Set, ProjectionKeyEventHandler)} with the
 * {@link #KEY_EVENT_VOICE_SEARCH_LONG_PRESS_KEY_DOWN} event instead.
 */

@Deprecated public static final int PROJECTION_LONG_PRESS_VOICE_SEARCH = 2; // 0x2

/**
 * Flag for {@link #registerProjectionListener(CarProjectionListener, int)}: subscribe to
 * voice-search short-press requests.
 *
 * @deprecated Use {@link #addKeyEventHandler(Set, ProjectionKeyEventHandler)} with the
 * {@link #KEY_EVENT_VOICE_SEARCH_SHORT_PRESS_KEY_UP} event instead.
 */

@Deprecated public static final int PROJECTION_VOICE_SEARCH = 1; // 0x1
/**
 * Listener to get projected notifications.
 *
 * Currently only voice search request is supported.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface CarProjectionListener {

/**
 * Voice search was requested by the user.
 */

public void onVoiceAssistantRequest(boolean fromLongPress);
}

/**
 * Callback class for applications to receive updates about the LocalOnlyHotspot status.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class ProjectionAccessPointCallback {

public ProjectionAccessPointCallback() { throw new RuntimeException("Stub!"); }

/** Called when access point started successfully. */

public void onStarted(android.net.wifi.WifiConfiguration wifiConfiguration) { throw new RuntimeException("Stub!"); }

/** Called when access point is stopped. No events will be sent after that. */

public void onStopped() { throw new RuntimeException("Stub!"); }

/** Called when access point failed to start. No events will be sent after that. */

public void onFailed(int reason) { throw new RuntimeException("Stub!"); }

public static final int ERROR_GENERIC = 2; // 0x2

public static final int ERROR_INCOMPATIBLE_MODE = 3; // 0x3

public static final int ERROR_NO_CHANNEL = 1; // 0x1

public static final int ERROR_TETHERING_DISALLOWED = 4; // 0x4
}

/**
 * Interface for projection apps to receive and handle key events from the system.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface ProjectionKeyEventHandler {

/**
 * Called when a projection key event occurs.
 *
 * @param event The projection key event that occurred.

 * Value is {@link android.car.CarProjectionManager#KEY_EVENT_VOICE_SEARCH_KEY_DOWN}, {@link android.car.CarProjectionManager#KEY_EVENT_VOICE_SEARCH_SHORT_PRESS_KEY_UP}, {@link android.car.CarProjectionManager#KEY_EVENT_VOICE_SEARCH_LONG_PRESS_KEY_DOWN}, {@link android.car.CarProjectionManager#KEY_EVENT_VOICE_SEARCH_LONG_PRESS_KEY_UP}, {@link android.car.CarProjectionManager#KEY_EVENT_CALL_KEY_DOWN}, {@link android.car.CarProjectionManager#KEY_EVENT_CALL_SHORT_PRESS_KEY_UP}, {@link android.car.CarProjectionManager#KEY_EVENT_CALL_LONG_PRESS_KEY_DOWN}, or {@link android.car.CarProjectionManager#KEY_EVENT_CALL_LONG_PRESS_KEY_UP}
 */

public void onKeyEvent(int event);
}

/**
 * Interface to receive for projection status updates.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface ProjectionStatusListener {

/**
 * This method gets invoked if projection status has been changed.
 *
 * @param state - current projection state
 * Value is {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_INACTIVE}, {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_READY_TO_PROJECT}, {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_ACTIVE_FOREGROUND}, or {@link android.car.projection.ProjectionStatus#PROJECTION_STATE_ACTIVE_BACKGROUND}
 * @param packageName - if projection is currently running either in the foreground or
 *                      in the background this argument will contain its package name
 * @param details - contains detailed information about all currently registered projection
 *                  receivers.
 */

public void onProjectionStatusChanged(int state, java.lang.String packageName, java.util.List<android.car.projection.ProjectionStatus> details);
}

}

