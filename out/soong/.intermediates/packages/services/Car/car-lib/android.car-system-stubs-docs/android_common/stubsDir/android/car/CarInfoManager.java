/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car;

import android.os.Bundle;

/**
 * Utility to retrieve various static information from car. Each data are grouped as {@link Bundle}
 * and relevant data can be checked from {@link Bundle} using pre-specified keys.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarInfoManager {

/** @hide */

CarInfoManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * @return Manufacturer of the car.  Empty if not available.
 */

public java.lang.String getManufacturer() { throw new RuntimeException("Stub!"); }

/**
 * @return Model name of the car, empty if not available.  This information
 * may not necessarily allow distinguishing different car models as the same
 * name may be used for different cars depending on manufacturers.
 */

public java.lang.String getModel() { throw new RuntimeException("Stub!"); }

/**
 * @return Model year of the car in AD.  Empty if not available.
 * @deprecated Use {@link #getModelYearInInteger()} instead.
 */

@Deprecated
public java.lang.String getModelYear() { throw new RuntimeException("Stub!"); }

/**
 * @return Model year of the car in AD.  0 if not available.
 */

public int getModelYearInInteger() { throw new RuntimeException("Stub!"); }

/**
 * @return always return empty string.
 * @deprecated no support for car's identifier
 */

@Deprecated
public java.lang.String getVehicleId() { throw new RuntimeException("Stub!"); }

/**
 * @return Fuel capacity of the car in milliliters.  0 if car doesn't run on
 *         fuel.
 */

public float getFuelCapacity() { throw new RuntimeException("Stub!"); }

/**
 * @return Array of FUEL_TYPEs available in the car.  Empty array if no fuel
 * types available.

 * Value is {@link android.car.FuelType#UNKNOWN}, {@link android.car.FuelType#UNLEADED}, {@link android.car.FuelType#LEADED}, {@link android.car.FuelType#DIESEL_1}, {@link android.car.FuelType#DIESEL_2}, {@link android.car.FuelType#BIODIESEL}, {@link android.car.FuelType#E85}, {@link android.car.FuelType#LPG}, {@link android.car.FuelType#CNG}, {@link android.car.FuelType#LNG}, {@link android.car.FuelType#ELECTRIC}, {@link android.car.FuelType#HYDROGEN}, or {@link android.car.FuelType#OTHER}
 */

public int[] getFuelTypes() { throw new RuntimeException("Stub!"); }

/**
 *
 * @return Battery capacity of the car in Watt-Hour(Wh). Return 0 if car doesn't run on battery.
 */

public float getEvBatteryCapacity() { throw new RuntimeException("Stub!"); }

/**
 * @return Array of EV_CONNECTOR_TYPEs available in the car.  Empty array if
 *         no connector types available.

 * Value is {@link android.car.EvConnectorType#UNKNOWN}, {@link android.car.EvConnectorType#J1772}, {@link android.car.EvConnectorType#MENNEKES}, {@link android.car.EvConnectorType#CHADEMO}, {@link android.car.EvConnectorType#COMBO_1}, {@link android.car.EvConnectorType#COMBO_2}, {@link android.car.EvConnectorType#TESLA_ROADSTER}, {@link android.car.EvConnectorType#TESLA_HPWC}, {@link android.car.EvConnectorType#TESLA_SUPERCHARGER}, {@link android.car.EvConnectorType#GBT}, or {@link android.car.EvConnectorType#OTHER}
 */

public int[] getEvConnectorTypes() { throw new RuntimeException("Stub!"); }

/**
 * @return Driver seat's location.

 * Value is {@link android.car.VehicleAreaSeat#SEAT_UNKNOWN}, {@link android.car.VehicleAreaSeat#SEAT_ROW_1_LEFT}, {@link android.car.VehicleAreaSeat#SEAT_ROW_1_CENTER}, {@link android.car.VehicleAreaSeat#SEAT_ROW_1_RIGHT}, {@link android.car.VehicleAreaSeat#SEAT_ROW_2_LEFT}, {@link android.car.VehicleAreaSeat#SEAT_ROW_2_CENTER}, {@link android.car.VehicleAreaSeat#SEAT_ROW_2_RIGHT}, {@link android.car.VehicleAreaSeat#SEAT_ROW_3_LEFT}, {@link android.car.VehicleAreaSeat#SEAT_ROW_3_CENTER}, or {@link android.car.VehicleAreaSeat#SEAT_ROW_3_RIGHT}
 */

public int getDriverSeat() { throw new RuntimeException("Stub!"); }

/**
 * @return EV port location of the car.

 * Value is {@link android.car.PortLocationType#UNKNOWN}, {@link android.car.PortLocationType#FRONT_LEFT}, {@link android.car.PortLocationType#FRONT_RIGHT}, {@link android.car.PortLocationType#REAR_LEFT}, {@link android.car.PortLocationType#REAR_RIGHT}, {@link android.car.PortLocationType#FRONT}, or {@link android.car.PortLocationType#REAR}
 */

public int getEvPortLocation() { throw new RuntimeException("Stub!"); }

/**
 * @return Fuel door location of the car.

 * Value is {@link android.car.PortLocationType#UNKNOWN}, {@link android.car.PortLocationType#FRONT_LEFT}, {@link android.car.PortLocationType#FRONT_RIGHT}, {@link android.car.PortLocationType#REAR_LEFT}, {@link android.car.PortLocationType#REAR_RIGHT}, {@link android.car.PortLocationType#FRONT}, or {@link android.car.PortLocationType#REAR}
 */

public int getFuelDoorLocation() { throw new RuntimeException("Stub!"); }
}

