/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.settings;


/**
 * Manager that exposes car configuration values that are stored on the system.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class CarConfigurationManager {

/** @hide */

CarConfigurationManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Returns a configuration for Speed Bump that will determine when it kicks in.
 *
 * @return A {@link SpeedBumpConfiguration} that contains the configuration values.
 */

public android.car.settings.SpeedBumpConfiguration getSpeedBumpConfiguration() { throw new RuntimeException("Stub!"); }
}

