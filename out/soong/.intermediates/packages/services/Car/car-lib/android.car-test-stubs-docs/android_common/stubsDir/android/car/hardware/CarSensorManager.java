/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.hardware;

import android.car.hardware.property.CarPropertyManager;
import android.car.Car;
import java.util.List;

/**
 *  @deprecated Use {@link CarPropertyManager} instead.
 *  API for monitoring car sensor data.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class CarSensorManager {

/** @hide */

CarSensorManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Give the list of CarSensors available in the connected car.
 * @return array of all sensor types supported. Sensor types is the same as
 * property id.
 */

@Deprecated
public int[] getSupportedSensors() { throw new RuntimeException("Stub!"); }

/**
 * Get list of properties represented by CarSensorManager for this car.
 * @return List of CarPropertyConfig objects available via Car Sensor Manager.
 */

@Deprecated
public java.util.List<android.car.hardware.CarPropertyConfig> getPropertyList() { throw new RuntimeException("Stub!"); }

/**
 * Tells if given sensor is supported or not.
 * @param sensorType
 * Value is {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_CAR_SPEED}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_RPM}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ODOMETER}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_FUEL_LEVEL}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_PARKING_BRAKE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_GEAR}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_NIGHT}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ENV_OUTSIDE_TEMPERATURE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_IGNITION_STATE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_WHEEL_TICK_DISTANCE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ABS_ACTIVE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_TRACTION_CONTROL_ACTIVE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_FUEL_DOOR_OPEN}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_BATTERY_LEVEL}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_CHARGE_PORT_OPEN}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_CHARGE_PORT_CONNECTED}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_BATTERY_CHARGE_RATE}, or {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ENGINE_OIL_LEVEL}
 * @return true if the sensor is supported.
 */

@Deprecated
public boolean isSensorSupported(int sensorType) { throw new RuntimeException("Stub!"); }

/**
 * Register {@link OnSensorChangedListener} to get repeated sensor updates. Multiple listeners
 * can be registered for a single sensor or the same listener can be used for different sensors.
 * If the same listener is registered again for the same sensor, it will be either ignored or
 * updated depending on the rate.
 * <p>
 *
 * @param listener
 * @param sensorType sensor type to subscribe.
 * Value is {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_CAR_SPEED}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_RPM}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ODOMETER}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_FUEL_LEVEL}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_PARKING_BRAKE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_GEAR}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_NIGHT}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ENV_OUTSIDE_TEMPERATURE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_IGNITION_STATE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_WHEEL_TICK_DISTANCE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ABS_ACTIVE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_TRACTION_CONTROL_ACTIVE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_FUEL_DOOR_OPEN}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_BATTERY_LEVEL}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_CHARGE_PORT_OPEN}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_CHARGE_PORT_CONNECTED}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_BATTERY_CHARGE_RATE}, or {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ENGINE_OIL_LEVEL}
 * @param rate how fast the sensor events are delivered. It should be one of
 *        {@link #SENSOR_RATE_FASTEST}, {@link #SENSOR_RATE_FAST}, {@link #SENSOR_RATE_UI},
 *        {@link #SENSOR_RATE_NORMAL}. Rate may not be respected especially when the same sensor
 *        is registered with different listener with different rates. Also, rate might be
 *        ignored when vehicle property raises events only when the value is actually changed,
 *        for example {@link #SENSOR_TYPE_PARKING_BRAKE} will raise an event only when parking
 *        brake was engaged or disengaged.
 * Value is {@link android.car.hardware.CarSensorManager#SENSOR_RATE_ONCHANGE}, {@link android.car.hardware.CarSensorManager#SENSOR_RATE_NORMAL}, {@link android.car.hardware.CarSensorManager#SENSOR_RATE_UI}, {@link android.car.hardware.CarSensorManager#SENSOR_RATE_FAST}, or {@link android.car.hardware.CarSensorManager#SENSOR_RATE_FASTEST}
 * @return if the sensor was successfully enabled.
 * @throws IllegalArgumentException for wrong argument like wrong rate
 * @throws SecurityException if missing the appropriate permission
 */

@Deprecated
public boolean registerListener(android.car.hardware.CarSensorManager.OnSensorChangedListener listener, int sensorType, int rate) { throw new RuntimeException("Stub!"); }

/**
 * Stop getting sensor update for the given listener.
 * If there are multiple registrations for this listener, all listening will be stopped.
 * @param listener Listener for car sensor data change.
 */

@Deprecated
public void unregisterListener(android.car.hardware.CarSensorManager.OnSensorChangedListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Stop getting sensor update for the given listener and sensor.
 * If the same listener is used for other sensors, those subscriptions will not be affected.
 * @param listener Listener for car sensor data change.
 * @param sensorType Property Id

 * Value is {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_CAR_SPEED}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_RPM}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ODOMETER}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_FUEL_LEVEL}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_PARKING_BRAKE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_GEAR}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_NIGHT}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ENV_OUTSIDE_TEMPERATURE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_IGNITION_STATE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_WHEEL_TICK_DISTANCE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ABS_ACTIVE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_TRACTION_CONTROL_ACTIVE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_FUEL_DOOR_OPEN}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_BATTERY_LEVEL}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_CHARGE_PORT_OPEN}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_CHARGE_PORT_CONNECTED}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_BATTERY_CHARGE_RATE}, or {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ENGINE_OIL_LEVEL}
 */

@Deprecated
public void unregisterListener(android.car.hardware.CarSensorManager.OnSensorChangedListener listener, int sensorType) { throw new RuntimeException("Stub!"); }

/**
 * Get the most recent CarSensorEvent for the given type. Note that latest sensor data from car
 * will not be available if it was never subscribed before. This call will return immediately
 * with null if there is no data available.
 * @param type A sensor to request
 * Value is {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_CAR_SPEED}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_RPM}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ODOMETER}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_FUEL_LEVEL}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_PARKING_BRAKE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_GEAR}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_NIGHT}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ENV_OUTSIDE_TEMPERATURE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_IGNITION_STATE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_WHEEL_TICK_DISTANCE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ABS_ACTIVE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_TRACTION_CONTROL_ACTIVE}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_FUEL_DOOR_OPEN}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_BATTERY_LEVEL}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_CHARGE_PORT_OPEN}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_CHARGE_PORT_CONNECTED}, {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_EV_BATTERY_CHARGE_RATE}, or {@link android.car.hardware.CarSensorManager#SENSOR_TYPE_ENGINE_OIL_LEVEL}
 * @return null if there was no sensor update since connected to the car.
 */

@Deprecated
public android.car.hardware.CarSensorEvent getLatestSensorEvent(int type) { throw new RuntimeException("Stub!"); }

@Deprecated public static final int SENSOR_RATE_FAST = 10; // 0xa

/** Read sensor at the maximum rate. Actual rate will be different depending on the sensor. */

@Deprecated public static final int SENSOR_RATE_FASTEST = 100; // 0x64

/** Read sensor in default normal rate set for each sensors. This is default rate. */

@Deprecated public static final int SENSOR_RATE_NORMAL = 1; // 0x1

/** Read on_change type sensors */

@Deprecated public static final int SENSOR_RATE_ONCHANGE = 0; // 0x0

@Deprecated public static final int SENSOR_RATE_UI = 5; // 0x5

/**
 * Set to true when ABS is active.  This sensor is event driven.
 * This requires {@link Car#PERMISSION_CAR_DYNAMICS_STATE} permission.
 */

@Deprecated public static final int SENSOR_TYPE_ABS_ACTIVE = 287310858; // 0x1120040a

/**
 * This sensor represents vehicle speed in m/s.
 * Sensor data in {@link CarSensorEvent} is a float which will be >= 0.
 * This requires {@link Car#PERMISSION_SPEED} permission.
 */

@Deprecated public static final int SENSOR_TYPE_CAR_SPEED = 291504647; // 0x11600207

/**
 * Oil level sensor.
 * This requires {@link Car#PERMISSION_CAR_ENGINE_DETAILED} permission
 */

@Deprecated public static final int SENSOR_TYPE_ENGINE_OIL_LEVEL = 289407747; // 0x11400303

/**
 * Outside Environment like temperature.
 * This requires {@link Car#PERMISSION_EXTERIOR_ENVIRONMENT} permission.
 */

@Deprecated public static final int SENSOR_TYPE_ENV_OUTSIDE_TEMPERATURE = 291505923; // 0x11600703

/**
 *  Indicates the instantaneous battery charging rate in mW.
 *  This requires {@link Car#PERMISSION_ENERGY} permission.
 */

@Deprecated public static final int SENSOR_TYPE_EV_BATTERY_CHARGE_RATE = 291504908; // 0x1160030c

/**
 * Indicates battery level of the car.
 * In {@link CarSensorEvent}, represents battery level in WH.  floatValues[{@link
 * CarSensorEvent#INDEX_EV_BATTERY_CAPACITY_ACTUAL}] represents the actual battery capacity in
 * WH.  The battery degrades over time, so this value is expected to drop slowly over the life
 * of the vehicle.
 * This requires {@link Car#PERMISSION_ENERGY} permission.
 */

@Deprecated public static final int SENSOR_TYPE_EV_BATTERY_LEVEL = 291504905; // 0x11600309

/**
 * Set to true if EV charging port is connected.
 * This requires {@link Car#PERMISSION_ENERGY_PORTS} permission.
 */

@Deprecated public static final int SENSOR_TYPE_EV_CHARGE_PORT_CONNECTED = 287310603; // 0x1120030b

/**
 * Set to true if EV charging port is open.
 * This requires {@link Car#PERMISSION_ENERGY_PORTS} permission.
 */

@Deprecated public static final int SENSOR_TYPE_EV_CHARGE_PORT_OPEN = 287310602; // 0x1120030a

/**
 * Set to true if the fuel door is open.
 * This requires {@link Car#PERMISSION_ENERGY_PORTS} permission.
 */

@Deprecated public static final int SENSOR_TYPE_FUEL_DOOR_OPEN = 287310600; // 0x11200308

/**
 * Indicates fuel level of the car.
 * In {@link CarSensorEvent}, represents fuel level in milliliters.
 * This requires {@link Car#PERMISSION_ENERGY} permission.
 */

@Deprecated public static final int SENSOR_TYPE_FUEL_LEVEL = 291504903; // 0x11600307

/**
 * This represents the current position of transmission gear. Sensor data in
 * {@link CarSensorEvent} is an intValues[0]. For the meaning of the value, check
 * {@link CarSensorEvent#GEAR_NEUTRAL} and other GEAR_*.
 * This requires {@link Car#PERMISSION_POWERTRAIN} permission.
 */

@Deprecated public static final int SENSOR_TYPE_GEAR = 289408000; // 0x11400400

/**
 * Represents ignition state. The value should be one of the constants that starts with
 * IGNITION_STATE_* in {@link CarSensorEvent}.
 * This requires {@link Car#PERMISSION_POWERTRAIN} permission.
 */

@Deprecated public static final int SENSOR_TYPE_IGNITION_STATE = 289408009; // 0x11400409

/**
 * Day/night sensor. Sensor data is intValues[0].
 * This requires {@link Car#PERMISSION_EXTERIOR_ENVIRONMENT} permission.
 */

@Deprecated public static final int SENSOR_TYPE_NIGHT = 287310855; // 0x11200407

/**
 * Total travel distance of the car in Kilometer. Sensor data is a float.
 * This requires {@link Car#PERMISSION_MILEAGE} permission.
 */

@Deprecated public static final int SENSOR_TYPE_ODOMETER = 291504644; // 0x11600204

/**
 * Represents the current status of parking brake. Sensor data in {@link CarSensorEvent} is an
 * intValues[0]. Value of 1 represents parking brake applied while 0 means the other way
 * around. For this sensor, rate in {@link #registerListener(OnSensorChangedListener, int, int)}
 * will be ignored and all changes will be notified.
 * This requires {@link Car#PERMISSION_POWERTRAIN} permission.
 */

@Deprecated public static final int SENSOR_TYPE_PARKING_BRAKE = 287310850; // 0x11200402

/**
 * Represents engine RPM of the car. Sensor data in {@link CarSensorEvent} is a float.
 * This requires {@link Car#PERMISSION_CAR_ENGINE_DETAILED} permission.
 */

@Deprecated public static final int SENSOR_TYPE_RPM = 291504901; // 0x11600305

/**
 * Set to true when traction control is active.  This sensor is event driven.
 * This requires {@link Car#PERMISSION_CAR_DYNAMICS_STATE} permission.
 */

@Deprecated public static final int SENSOR_TYPE_TRACTION_CONTROL_ACTIVE = 287310859; // 0x1120040b

/**
 * Represents wheel distance in millimeters.  Some cars may not have individual sensors on each
 * wheel.  If a value is not available, Long.MAX_VALUE will be reported.  The wheel distance
 * accumulates over time.  It increments on forward movement, and decrements on reverse.  Wheel
 * distance shall be reset to zero each time a vehicle is started by the user.
 * This requires {@link Car#PERMISSION_SPEED} permission.
 */

@Deprecated public static final int SENSOR_TYPE_WHEEL_TICK_DISTANCE = 290521862; // 0x11510306
/**
 * Listener for car sensor data change.
 * Callbacks are called in the Looper context.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static interface OnSensorChangedListener {

/**
 * Called when there is a new sensor data from car.
 * @param event Incoming sensor event for the given sensor type.
 */

@Deprecated
public void onSensorChanged(android.car.hardware.CarSensorEvent event);
}

}

