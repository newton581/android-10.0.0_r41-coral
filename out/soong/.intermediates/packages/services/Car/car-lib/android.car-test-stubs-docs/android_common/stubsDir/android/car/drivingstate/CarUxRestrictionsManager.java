/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.drivingstate;

import java.util.List;
import android.os.Handler;

/**
 * API to register and get the User Experience restrictions imposed based on the car's driving
 * state.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarUxRestrictionsManager {

/** @hide */

CarUxRestrictionsManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Registers a {@link OnUxRestrictionsChangedListener} for listening to changes in the
 * UX Restrictions to adhere to.
 * <p>
 * If a listener has already been registered, it has to be unregistered before registering
 * the new one.
 *
 * @param listener {@link OnUxRestrictionsChangedListener}
 */

public void registerListener(android.car.drivingstate.CarUxRestrictionsManager.OnUxRestrictionsChangedListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Unregisters the registered {@link OnUxRestrictionsChangedListener}
 */

public void unregisterListener() { throw new RuntimeException("Stub!"); }

/**
 * Gets the current UX restrictions ({@link CarUxRestrictions}) in place.
 *
 * @return current UX restrictions that is in effect.
 */

public android.car.drivingstate.CarUxRestrictions getCurrentCarUxRestrictions() { throw new RuntimeException("Stub!"); }
/**
 * Listener Interface for clients to implement to get updated on driving state related
 * changes.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnUxRestrictionsChangedListener {

/**
 * Called when the UX restrictions due to a car's driving state changes.
 *
 * @param restrictionInfo The new UX restriction information
 */

public void onUxRestrictionsChanged(android.car.drivingstate.CarUxRestrictions restrictionInfo);
}

}

