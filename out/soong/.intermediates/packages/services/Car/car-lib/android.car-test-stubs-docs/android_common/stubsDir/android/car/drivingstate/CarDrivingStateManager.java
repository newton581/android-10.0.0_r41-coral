/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.drivingstate;

import android.car.Car;
import android.os.Handler;

/**
 * API to register and get driving state related information in a car.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarDrivingStateManager {

/** @hide */

CarDrivingStateManager(android.car.Car car, android.os.IBinder service) { throw new RuntimeException("Stub!"); }

/**
 * Notify registered driving state change listener about injected event.
 *
 * @param drivingState Value in {@link CarDrivingStateEvent.CarDrivingState}.
 *
 * Requires Permission:
 * {@link Car#PERMISSION_CONTROL_APP_BLOCKING}
 *
 * @hide
 */

public void injectDrivingState(int drivingState) { throw new RuntimeException("Stub!"); }
}

