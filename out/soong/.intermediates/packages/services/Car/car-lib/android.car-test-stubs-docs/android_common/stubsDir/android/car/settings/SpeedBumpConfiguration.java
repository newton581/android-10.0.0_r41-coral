/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.settings;


/**
 * A configuration struct that holds information for tweaking SpeedBump settings.
 *
 * @see androidx.car.moderator.SpeedBumpView
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class SpeedBumpConfiguration implements android.os.Parcelable {

public SpeedBumpConfiguration(double permitsPerSecond, double maxPermitPool, long permitFillDelay) { throw new RuntimeException("Stub!"); }

/**
 * Returns the number of permitted actions that are acquired each second that the user has not
 * interacted with the {@code SpeedBumpView}.
 */

public double getAcquiredPermitsPerSecond() { throw new RuntimeException("Stub!"); }

/**
 * Returns the maximum number of permits that can be acquired when the user is idling.
 */

public double getMaxPermitPool() { throw new RuntimeException("Stub!"); }

/**
 * Returns the delay time before when the permit pool has been depleted and when it begins to
 * refill.
 */

public long getPermitFillDelay() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object object) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel desk, int flags) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.settings.SpeedBumpConfiguration> CREATOR;
static { CREATOR = null; }
}

