/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car;

import android.content.pm.PackageManager;
import android.content.Context;
import android.os.Handler;
import android.content.ContextWrapper;
import android.content.ServiceConnection;
import android.app.Service;
import android.car.hardware.CarSensorManager;
import android.car.hardware.property.CarPropertyManager;
import android.car.content.pm.CarPackageManager;
import android.car.media.CarAudioManager;
import android.car.drivingstate.CarDrivingStateManager;
import android.car.drivingstate.CarUxRestrictionsManager;
import android.car.settings.CarConfigurationManager;

/**
 *   Top level car API for embedded Android Auto deployments.
 *   This API works only for devices with {@link PackageManager#FEATURE_AUTOMOTIVE}
 *   Calling this API on a device with no such feature will lead to an exception.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class Car {

Car() { throw new RuntimeException("Stub!"); }

/**
 * A factory method that creates Car instance for all Car API access.
 * @param context App's Context. This should not be null. If you are passing
 *                {@link ContextWrapper}, make sure that its base Context is non-null as well.
 *                Otherwise it will throw {@link java.lang.NullPointerException}.
 * @param serviceConnectionListener listener for monitoring service connection.
 * @param handler the handler on which the callback should execute, or null to execute on the
 * service's main thread. Note: the service connection listener will be always on the main
 * thread regardless of the handler given.
 * @return Car instance if system is in car environment and returns {@code null} otherwise.
 *
 * @deprecated use {@link #createCar(Context, Handler)} instead.
 */

@Deprecated
public static android.car.Car createCar(android.content.Context context, android.content.ServiceConnection serviceConnectionListener, android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * A factory method that creates Car instance for all Car API access using main thread {@code
 * Looper}.
 *
 * @see #createCar(Context, ServiceConnection, Handler)
 *
 * @deprecated use {@link #createCar(Context, Handler)} instead.
 */

@Deprecated
public static android.car.Car createCar(android.content.Context context, android.content.ServiceConnection serviceConnectionListener) { throw new RuntimeException("Stub!"); }

/**
 * Creates new {@link Car} object which connected synchronously to Car Service and ready to use.
 *
 * @param context application's context
 *
 * @return Car object if operation succeeded, otherwise null.
 */

public static android.car.Car createCar(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Creates new {@link Car} object which connected synchronously to Car Service and ready to use.
 *
 * @param context App's Context. This should not be null. If you are passing
 *                {@link ContextWrapper}, make sure that its base Context is non-null as well.
 *                Otherwise it will throw {@link java.lang.NullPointerException}.
 * @param handler the handler on which the manager's callbacks will be executed, or null to
 * execute on the application's main thread.
 *
 * @return Car object if operation succeeded, otherwise null.
 */

public static android.car.Car createCar(android.content.Context context, android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * Connect to car service. This can be called while it is disconnected.
 * @throws IllegalStateException If connection is still on-going from previous
 *         connect call or it is already connected
 *
 * @deprecated this method is not need if this object is created via
 * {@link #createCar(Context, Handler)}.
 */

@Deprecated
public void connect() throws java.lang.IllegalStateException { throw new RuntimeException("Stub!"); }

/**
 * Disconnect from car service. This can be called while disconnected. Once disconnect is
 * called, all Car*Managers from this instance becomes invalid, and
 * {@link Car#getCarManager(String)} will return different instance if it is connected again.
 */

public void disconnect() { throw new RuntimeException("Stub!"); }

/**
 * Tells if it is connected to the service or not. This will return false if it is still
 * connecting.
 * @return
 */

public boolean isConnected() { throw new RuntimeException("Stub!"); }

/**
 * Tells if this instance is already connecting to car service or not.
 * @return
 */

public boolean isConnecting() { throw new RuntimeException("Stub!"); }

/**
 * Get car specific service as in {@link Context#getSystemService(String)}. Returned
 * {@link Object} should be type-casted to the desired service.
 * For example, to get sensor service,
 * SensorManagerService sensorManagerService = car.getCarManager(Car.SENSOR_SERVICE);
 * @param serviceName Name of service that should be created like {@link #SENSOR_SERVICE}.
 * @return Matching service manager or null if there is no such service.
 */

public java.lang.Object getCarManager(java.lang.String serviceName) { throw new RuntimeException("Stub!"); }

/**
 * Return the type of currently connected car.
 * @return

 * Value is {@link android.car.Car#CONNECTION_TYPE_EMBEDDED}
 */

public int getCarConnectionType() { throw new RuntimeException("Stub!"); }

/** Service name for {@link CarAppFocusManager}. */

public static final java.lang.String APP_FOCUS_SERVICE = "app_focus";

/** Service name for {@link CarAudioManager} */

public static final java.lang.String AUDIO_SERVICE = "audio";

/**
 * Service name for {@link android.car.settings.CarConfigurationManager}
 */

public static final java.lang.String CAR_CONFIGURATION_SERVICE = "configuration";

/**
 * Used as a string extra field with {@link #CAR_INTENT_ACTION_MEDIA_TEMPLATE} to specify the
 * media app that user wants to start the media on. Note: this is not the templated media app.
 *
 * This is being deprecated. Use {@link #CAR_EXTRA_MEDIA_COMPONENT} instead.
 */

public static final java.lang.String CAR_EXTRA_MEDIA_PACKAGE = "android.car.intent.extra.MEDIA_PACKAGE";

/**
 * Activity Action: Provide media playing through a media template app.
 * <p>Input: String extra mapped by {@link android.app.SearchManager#QUERY} is the query
 * used to start the media. String extra mapped by {@link #CAR_EXTRA_MEDIA_COMPONENT} is the
 * component name of the media app which user wants to play media on.
 * <p>Output: nothing.
 */

public static final java.lang.String CAR_INTENT_ACTION_MEDIA_TEMPLATE = "android.car.intent.action.MEDIA_TEMPLATE";

/** Service name for {@link CarNavigationStatusManager} */

public static final java.lang.String CAR_NAVIGATION_SERVICE = "car_navigation_service";

/**
 * Service name for {@link CarUxRestrictionsManager}
 */

public static final java.lang.String CAR_UX_RESTRICTION_SERVICE = "uxrestriction";

/** Type of car connection: platform runs directly in car. */

public static final int CONNECTION_TYPE_EMBEDDED = 5; // 0x5

/** Service name for {@link CarInfoManager}, to be used in {@link #getCarManager(String)}. */

public static final java.lang.String INFO_SERVICE = "info";

/** Service name for {@link CarPackageManager} */

public static final java.lang.String PACKAGE_SERVICE = "package";

/**
 * Permission necessary to change car audio settings through {@link CarAudioManager}.
 */

public static final java.lang.String PERMISSION_CAR_CONTROL_AUDIO_SETTINGS = "android.car.permission.CAR_CONTROL_AUDIO_SETTINGS";

/**
 * Permission necessary to change car audio volume through {@link CarAudioManager}.
 */

public static final java.lang.String PERMISSION_CAR_CONTROL_AUDIO_VOLUME = "android.car.permission.CAR_CONTROL_AUDIO_VOLUME";

/** Permission necessary to use {@link CarInfoManager}. */

public static final java.lang.String PERMISSION_CAR_INFO = "android.car.permission.CAR_INFO";

/**
 * Permission necessary to use {@link CarNavigationStatusManager}.
 */

public static final java.lang.String PERMISSION_CAR_NAVIGATION_MANAGER = "android.car.permission.CAR_NAVIGATION_MANAGER";

/**
 * Permission necessary to control display units for distance, fuel volume, tire pressure
 * and ev battery.
 */

public static final java.lang.String PERMISSION_CONTROL_DISPLAY_UNITS = "android.car.permission.CONTROL_CAR_DISPLAY_UNITS";

/**
 * Permission necessary to control car's interior lights.
 */

public static final java.lang.String PERMISSION_CONTROL_INTERIOR_LIGHTS = "android.car.permission.CONTROL_CAR_INTERIOR_LIGHTS";

/** Permission necessary to access car's energy information. */

public static final java.lang.String PERMISSION_ENERGY = "android.car.permission.CAR_ENERGY";

/** Permission necessary to access car's fuel door and ev charge port. */

public static final java.lang.String PERMISSION_ENERGY_PORTS = "android.car.permission.CAR_ENERGY_PORTS";

/** Permission necessary to read temperature of car's exterior environment. */

public static final java.lang.String PERMISSION_EXTERIOR_ENVIRONMENT = "android.car.permission.CAR_EXTERIOR_ENVIRONMENT";

/** Permission necessary to access car's VIN information */

public static final java.lang.String PERMISSION_IDENTIFICATION = "android.car.permission.CAR_IDENTIFICATION";

/** Permission necessary to access car's powertrain information.*/

public static final java.lang.String PERMISSION_POWERTRAIN = "android.car.permission.CAR_POWERTRAIN";

/**
 * Permission necessary to read and write display units for distance, fuel volume, tire pressure
 * and ev battery.
 */

public static final java.lang.String PERMISSION_READ_DISPLAY_UNITS = "android.car.permission.READ_CAR_DISPLAY_UNITS";

/**
 * Permission necessary to read car's interior lights information.
 */

public static final java.lang.String PERMISSION_READ_INTERIOR_LIGHTS = "android.car.permission.READ_CAR_INTERIOR_LIGHTS";

/**
 * Permission necessary to access car's steering angle information.
 */

public static final java.lang.String PERMISSION_READ_STEERING_STATE = "android.car.permission.READ_CAR_STEERING";

/** Permission necessary to access car's speed. */

public static final java.lang.String PERMISSION_SPEED = "android.car.permission.CAR_SPEED";

/**
 * Service name for {@link CarPropertyManager}
 */

public static final java.lang.String PROPERTY_SERVICE = "property";

/**
 * Service name for {@link CarSensorManager}, to be used in {@link #getCarManager(String)}.
 *
 * @deprecated  {@link CarSensorManager} is deprecated. Use {@link CarPropertyManager} instead.
 */

@Deprecated public static final java.lang.String SENSOR_SERVICE = "sensor";
}

