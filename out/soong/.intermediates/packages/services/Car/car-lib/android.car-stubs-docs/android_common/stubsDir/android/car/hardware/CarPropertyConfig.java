/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.hardware;

import android.os.Parcel;
import android.car.VehicleAreaType;

/**
 * Represents general information about car property such as data type and min/max ranges for car
 * areas (if applicable). This class supposed to be immutable, parcelable and could be passed over.
 *
 * <p>Use {@link CarPropertyConfig#newBuilder} to create an instance of this class.
 *
 * @param <T> refer to Parcel#writeValue(Object) to get a list of all supported types. The class
 * should be visible to framework as default class loader is being used here.
 *
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarPropertyConfig<T> implements android.os.Parcelable {

CarPropertyConfig(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Return the access type of the car property.
 * <p>The access type could be one of the following:
 * <ul>
 *   <li>{@link CarPropertyConfig#VEHICLE_PROPERTY_ACCESS_NONE}</li>
 *   <li>{@link CarPropertyConfig#VEHICLE_PROPERTY_ACCESS_READ}</li>
 *   <li>{@link CarPropertyConfig#VEHICLE_PROPERTY_ACCESS_WRITE}</li>
 *   <li>{@link CarPropertyConfig#VEHICLE_PROPERTY_ACCESS_READ_WRITE}</li>
 * </ul>
 *
 * @return the access type of the car property.

 * Value is {@link android.car.hardware.CarPropertyConfig#VEHICLE_PROPERTY_ACCESS_NONE}, {@link android.car.hardware.CarPropertyConfig#VEHICLE_PROPERTY_ACCESS_READ}, {@link android.car.hardware.CarPropertyConfig#VEHICLE_PROPERTY_ACCESS_WRITE}, or {@link android.car.hardware.CarPropertyConfig#VEHICLE_PROPERTY_ACCESS_READ_WRITE}
 */

public int getAccess() { throw new RuntimeException("Stub!"); }

/**
 * Return the area type of the car property.
 * <p>The area type could be one of the following:
 * <ul>
 *   <li>{@link VehicleAreaType#VEHICLE_AREA_TYPE_GLOBAL}</li>
 *   <li>{@link VehicleAreaType#VEHICLE_AREA_TYPE_WINDOW}</li>
 *   <li>{@link VehicleAreaType#VEHICLE_AREA_TYPE_SEAT}</li>
 *   <li>{@link VehicleAreaType#VEHICLE_AREA_TYPE_DOOR}</li>
 *   <li>{@link VehicleAreaType#VEHICLE_AREA_TYPE_MIRROR}</li>
 *   <li>{@link VehicleAreaType#VEHICLE_AREA_TYPE_WHEEL}</li>
 * </ul>
 *
 * @return the area type of the car property.

 * Value is {@link android.car.VehicleAreaType#VEHICLE_AREA_TYPE_GLOBAL}, {@link android.car.VehicleAreaType#VEHICLE_AREA_TYPE_WINDOW}, {@link android.car.VehicleAreaType#VEHICLE_AREA_TYPE_SEAT}, {@link android.car.VehicleAreaType#VEHICLE_AREA_TYPE_DOOR}, {@link android.car.VehicleAreaType#VEHICLE_AREA_TYPE_MIRROR}, or {@link android.car.VehicleAreaType#VEHICLE_AREA_TYPE_WHEEL}
 */

public int getAreaType() { throw new RuntimeException("Stub!"); }

/**
 * Return the change mode of the car property.
 *
 * <p>The change mode could be one of the following:
 * <ul>
 *   <li>{@link CarPropertyConfig#VEHICLE_PROPERTY_CHANGE_MODE_STATIC }</li>
 *   <li>{@link CarPropertyConfig#VEHICLE_PROPERTY_CHANGE_MODE_ONCHANGE}</li>
 *   <li>{@link CarPropertyConfig#VEHICLE_PROPERTY_CHANGE_MODE_CONTINUOUS}</li>
 * </ul>
 *
 * @return the change mode of properties.

 * Value is {@link android.car.hardware.CarPropertyConfig#VEHICLE_PROPERTY_CHANGE_MODE_STATIC}, {@link android.car.hardware.CarPropertyConfig#VEHICLE_PROPERTY_CHANGE_MODE_ONCHANGE}, or {@link android.car.hardware.CarPropertyConfig#VEHICLE_PROPERTY_CHANGE_MODE_CONTINUOUS}
 */

public int getChangeMode() { throw new RuntimeException("Stub!"); }

/**
 *
 * @return Additional configuration parameters. For different properties, configArrays have
 * different information.
 */

public java.util.List<java.lang.Integer> getConfigArray() { throw new RuntimeException("Stub!"); }

/**
 *
 * @return Max sample rate in Hz. Must be defined for VehiclePropertyChangeMode::CONTINUOUS
 * return 0 if change mode is not continuous.
 */

public float getMaxSampleRate() { throw new RuntimeException("Stub!"); }

/**
 *
 * @return Min sample rate in Hz.Must be defined for VehiclePropertyChangeMode::CONTINUOUS
 * return 0 if change mode is not continuous.
 */

public float getMinSampleRate() { throw new RuntimeException("Stub!"); }

/**
 * @return Property identifier
 */

public int getPropertyId() { throw new RuntimeException("Stub!"); }

/**
 *
 * @return true if this property doesn't hold car area-specific configuration.
 */

public boolean isGlobalProperty() { throw new RuntimeException("Stub!"); }

/**
 *
 * @return Array of areaIds. An AreaID is a combination of one or more areas,
 * and is represented using a bitmask of Area enums. Different AreaTypes may
 * not be mixed in a single AreaID. For instance, a window area cannot be
 * combined with a seat area in an AreaID.
 * Rules for mapping a zoned property to AreaIDs:
 *  - A property must be mapped to an array of AreaIDs that are impacted when
 *    the property value changes.
 *  - Each element in the array must represent an AreaID, in which, the
 *    property value can only be changed together in all the areas within
 *    an AreaID and never independently. That is, when the property value
 *    changes in one of the areas in an AreaID in the array, then it must
 *    automatically change in all other areas in the AreaID.
 *  - The property value must be independently controllable in any two
 *    different AreaIDs in the array.
 *  - An area must only appear once in the array of AreaIDs. That is, an
 *    area must only be part of a single AreaID in the array.
 */

public int[] getAreaIds() { throw new RuntimeException("Stub!"); }

/**
 *
 * @param areaId
 * @return Min value in given areaId. Null if not have min value in given area.
 */

public T getMinValue(int areaId) { throw new RuntimeException("Stub!"); }

/**
 *
 * @param areaId
 * @return Max value in given areaId. Null if not have max value in given area.
 */

public T getMaxValue(int areaId) { throw new RuntimeException("Stub!"); }

/**
 *
 * @return Min value in areaId 0. Null if not have min value.
 */

public T getMinValue() { throw new RuntimeException("Stub!"); }

/**
 *
 * @return Max value in areaId 0. Null if not have max value.
 */

public T getMaxValue() { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @hide */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.hardware.CarPropertyConfig> CREATOR;
static { CREATOR = null; }

/** Property Access Unknown */

public static final int VEHICLE_PROPERTY_ACCESS_NONE = 0; // 0x0

/** The property is readable */

public static final int VEHICLE_PROPERTY_ACCESS_READ = 1; // 0x1

/** The property is readable and writable */

public static final int VEHICLE_PROPERTY_ACCESS_READ_WRITE = 3; // 0x3

/** The property is writable */

public static final int VEHICLE_PROPERTY_ACCESS_WRITE = 2; // 0x2

/** Properties of this type change continuously. */

public static final int VEHICLE_PROPERTY_CHANGE_MODE_CONTINUOUS = 2; // 0x2

/** Properties of this type must report when there is a change. */

public static final int VEHICLE_PROPERTY_CHANGE_MODE_ONCHANGE = 1; // 0x1

/** Properties of this type must never be changed. */

public static final int VEHICLE_PROPERTY_CHANGE_MODE_STATIC = 0; // 0x0
}

