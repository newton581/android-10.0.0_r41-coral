/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.hardware;

import android.os.Parcel;

/**
 * Stores values broken down by area for a vehicle property.
 *
 * @param <T> refer to Parcel#writeValue(Object) to get a list of all supported types. The class
 * should be visible to framework as default class loader is being used here.
 *
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarPropertyValue<T> implements android.os.Parcelable {

/**
 * Get an instance of CarPropertyValue
 * @param in Parcel to read
 * @hide
 */

CarPropertyValue(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

public int describeContents() { throw new RuntimeException("Stub!"); }

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @return Property id of CarPropertyValue
 */

public int getPropertyId() { throw new RuntimeException("Stub!"); }

/**
 * @return Area id of CarPropertyValue
 */

public int getAreaId() { throw new RuntimeException("Stub!"); }

/**
 * @return Status of CarPropertyValue

 * Value is {@link android.car.hardware.CarPropertyValue#STATUS_AVAILABLE}, {@link android.car.hardware.CarPropertyValue#STATUS_UNAVAILABLE}, or {@link android.car.hardware.CarPropertyValue#STATUS_ERROR}
 */

public int getStatus() { throw new RuntimeException("Stub!"); }

/**
 * @return Timestamp of CarPropertyValue
 */

public long getTimestamp() { throw new RuntimeException("Stub!"); }

/**
 * @return Value of CarPropertyValue
 */

public T getValue() { throw new RuntimeException("Stub!"); }

/** @hide */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public static final android.os.Parcelable.Creator<android.car.hardware.CarPropertyValue> CREATOR;
static { CREATOR = null; }

/**
 * CarPropertyValue is available.
 */

public static final int STATUS_AVAILABLE = 0; // 0x0

/**
 * CarPropertyVale has an error.
 */

public static final int STATUS_ERROR = 2; // 0x2

/**
 * CarPropertyValue is unavailable.
 */

public static final int STATUS_UNAVAILABLE = 1; // 0x1
/**
 * Value is {@link android.car.hardware.CarPropertyValue#STATUS_AVAILABLE}, {@link android.car.hardware.CarPropertyValue#STATUS_UNAVAILABLE}, or {@link android.car.hardware.CarPropertyValue#STATUS_ERROR}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface PropertyStatus {
}

}

