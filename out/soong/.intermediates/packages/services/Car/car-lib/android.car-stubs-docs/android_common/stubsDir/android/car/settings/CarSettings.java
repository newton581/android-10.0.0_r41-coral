/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


package android.car.settings;


/**
 * System level car related settings.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class CarSettings {

public CarSettings() { throw new RuntimeException("Stub!"); }
/**
 * Global car settings, containing preferences that always apply identically
 * to all defined users.  Applications can read these but are not allowed to write;
 * like the "Secure" settings, these are for preferences that the user must
 * explicitly modify through the system UI or specialized APIs for those values.
 *
 * To read/write the global car settings, use {@link android.provider.Settings.Global}
 * with the keys defined here.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Global {

public Global() { throw new RuntimeException("Stub!"); }

/**
 * DEPRECATED. Will be removed in Q. Key for whether garage mode is enabled.
 * @deprecated not used by GarageMode anymore. Will be removed in Q.
 */

@Deprecated public static final java.lang.String KEY_GARAGE_MODE_ENABLED = "android.car.GARAGE_MODE_ENABLED";

/**
 * DEPRECATED. Will be removed in Q. Key for garage mode maintenance window.
 * @deprecated not used by GarageMode anymore. Will be removed in Q.
 */

@Deprecated public static final java.lang.String KEY_GARAGE_MODE_MAINTENANCE_WINDOW = "android.car.GARAGE_MODE_MAINTENANCE_WINDOW";

/**
 * DEPRECATED. Will be removed in Q. Key for when to wake up to run garage mode.
 * @deprecated not used by GarageMode anymore. Will be removed in Q.
 */

@Deprecated public static final java.lang.String KEY_GARAGE_MODE_WAKE_UP_TIME = "android.car.GARAGE_MODE_WAKE_UP_TIME";
}

}

