/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.car.hardware.property;

import android.car.Car;
import android.car.hardware.CarPropertyConfig;
import java.util.List;
import android.car.hardware.CarPropertyValue;

/**
 * Provides an application interface for interacting with the Vehicle specific properties.
 * For details about the individual properties, see the descriptions in
 * hardware/interfaces/automotive/vehicle/types.hal
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class CarPropertyManager {

CarPropertyManager() { throw new RuntimeException("Stub!"); }

/**
 * Register {@link CarPropertyEventCallback} to get property updates. Multiple listeners
 * can be registered for a single property or the same listener can be used for different
 * properties. If the same listener is registered again for the same property, it will be
 * updated to new rate.
 * <p>Rate could be one of the following:
 * <ul>
 *   <li>{@link CarPropertyManager#SENSOR_RATE_ONCHANGE}</li>
 *   <li>{@link CarPropertyManager#SENSOR_RATE_NORMAL}</li>
 *   <li>{@link CarPropertyManager#SENSOR_RATE_UI}</li>
 *   <li>{@link CarPropertyManager#SENSOR_RATE_FAST}</li>
 *   <li>{@link CarPropertyManager#SENSOR_RATE_FASTEST}</li>
 * </ul>
 * <p>
 * <b>Note:</b>Rate has no effect if the property has one of the following change modes:
 * <ul>
 *   <li>{@link CarPropertyConfig#VEHICLE_PROPERTY_CHANGE_MODE_STATIC}</li>
 *   <li>{@link CarPropertyConfig#VEHICLE_PROPERTY_CHANGE_MODE_ONCHANGE}</li>
 * </ul>
 * See {@link CarPropertyConfig#getChangeMode()} for details.
 * If rate is higher than {@link CarPropertyConfig#getMaxSampleRate()}, it will be registered
 * with max sample rate.
 * If rate is lower than {@link CarPropertyConfig#getMinSampleRate()}, it will be registered
 * with min sample rate.
 *
 * @param callback CarPropertyEventCallback to be registered.
 * @param propertyId PropertyId to subscribe
 * @param rate how fast the property events are delivered in Hz.
 * Value is between 0.0 and 100.0 inclusive
 * @return true if the listener is successfully registered.
 * @throws SecurityException if missing the appropriate permission.
 */

public boolean registerCallback(android.car.hardware.property.CarPropertyManager.CarPropertyEventCallback callback, int propertyId, float rate) { throw new RuntimeException("Stub!"); }

/**
 * Stop getting property update for the given callback. If there are multiple registrations for
 * this callback, all listening will be stopped.
 * @param callback CarPropertyEventCallback to be unregistered.
 */

public void unregisterCallback(android.car.hardware.property.CarPropertyManager.CarPropertyEventCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Stop getting property update for the given callback and property. If the same callback is
 * used for other properties, those subscriptions will not be affected.
 *
 * @param callback CarPropertyEventCallback to be unregistered.
 * @param propertyId PropertyId to be unregistered.
 */

public void unregisterCallback(android.car.hardware.property.CarPropertyManager.CarPropertyEventCallback callback, int propertyId) { throw new RuntimeException("Stub!"); }

/**
 * @return List of properties implemented by this car that the application may access.
 */

public java.util.List<android.car.hardware.CarPropertyConfig> getPropertyList() { throw new RuntimeException("Stub!"); }

/**
 * @param propertyIds property ID list
 * @return List of properties implemented by this car in given property ID list that application
 *          may access.
 */

public java.util.List<android.car.hardware.CarPropertyConfig> getPropertyList(android.util.ArraySet<java.lang.Integer> propertyIds) { throw new RuntimeException("Stub!"); }

/**
 * Check whether a given property is available or disabled based on the car's current state.
 * @param propId Property Id
 * @param area AreaId of property
 * @return true if STATUS_AVAILABLE, false otherwise (eg STATUS_UNAVAILABLE)
 */

public boolean isPropertyAvailable(int propId, int area) { throw new RuntimeException("Stub!"); }

/**
 * Returns value of a bool property
 *
 * @param prop Property ID to get
 * @param area Area of the property to get
 * @return value of a bool property.
 */

public boolean getBooleanProperty(int prop, int area) { throw new RuntimeException("Stub!"); }

/**
 * Returns value of a float property
 *
 * @param prop Property ID to get
 * @param area Area of the property to get
 */

public float getFloatProperty(int prop, int area) { throw new RuntimeException("Stub!"); }

/**
 * Returns value of a integer property
 *
 * @param prop Property ID to get
 * @param area Zone of the property to get
 */

public int getIntProperty(int prop, int area) { throw new RuntimeException("Stub!"); }

/**
 * Returns value of a integer array property
 *
 * @param prop Property ID to get
 * @param area Zone of the property to get
 */

public int[] getIntArrayProperty(int prop, int area) { throw new RuntimeException("Stub!"); }

/**
 * Return CarPropertyValue
 *
 * @param clazz The class object for the CarPropertyValue
 * @param propId Property ID to get
 * @param areaId Zone of the property to get
 * @throws IllegalArgumentException if there is invalid property type.
 * @return CarPropertyValue. Null if property's id is invalid.
 */

public <E> android.car.hardware.CarPropertyValue<E> getProperty(java.lang.Class<E> clazz, int propId, int areaId) { throw new RuntimeException("Stub!"); }

/**
 * Query CarPropertyValue with property id and areaId.
 * @param propId Property Id
 * @param areaId areaId
 * @param <E>
 * @return CarPropertyValue. Null if property's id is invalid.
 */

public <E> android.car.hardware.CarPropertyValue<E> getProperty(int propId, int areaId) { throw new RuntimeException("Stub!"); }

/**
 * Set value of car property by areaId.
 * @param clazz The class object for the CarPropertyValue
 * @param propId Property ID
 * @param areaId areaId
 * @param val Value of CarPropertyValue
 * @param <E> data type of the given property, for example property that was
 * defined as {@code VEHICLE_VALUE_TYPE_INT32} in vehicle HAL could be accessed using
 * {@code Integer.class}
 */

public <E> void setProperty(java.lang.Class<E> clazz, int propId, int areaId, E val) { throw new RuntimeException("Stub!"); }

/**
 * Modifies a property.  If the property modification doesn't occur, an error event shall be
 * generated and propagated back to the application.
 *
 * @param prop Property ID to modify
 * @param areaId AreaId to apply the modification.
 * @param val Value to set
 */

public void setBooleanProperty(int prop, int areaId, boolean val) { throw new RuntimeException("Stub!"); }

/**
 * Set float value of property
 *
 * @param prop Property ID to modify
 * @param areaId AreaId to apply the modification
 * @param val Value to set
 */

public void setFloatProperty(int prop, int areaId, float val) { throw new RuntimeException("Stub!"); }

/**
 * Set int value of property
 *
 * @param prop Property ID to modify
 * @param areaId AreaId to apply the modification
 * @param val Value to set
 */

public void setIntProperty(int prop, int areaId, int val) { throw new RuntimeException("Stub!"); }

/** Read sensors at the rate of 10 hertz */

public static final float SENSOR_RATE_FAST = 10.0f;

/** Read sensors at the rate of 100 hertz */

public static final float SENSOR_RATE_FASTEST = 100.0f;

/** Read sensors at the rate of  1 hertz */

public static final float SENSOR_RATE_NORMAL = 1.0f;

/** Read ON_CHANGE sensors */

public static final float SENSOR_RATE_ONCHANGE = 0.0f;

/** Read sensors at the rate of 5 hertz */

public static final float SENSOR_RATE_UI = 5.0f;
/**
 * Application registers {@link CarPropertyEventCallback} object to receive updates and changes
 * to subscribed Vehicle specific properties.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface CarPropertyEventCallback {

/**
 * Called when a property is updated
 * @param value Property that has been updated.
 */

public void onChangeEvent(android.car.hardware.CarPropertyValue value);

/**
 * Called when an error is detected with a property
 * @param propId Property ID which is detected an error.
 * @param zone Zone which is detected an error.
 */

public void onErrorEvent(int propId, int zone);
}

}

