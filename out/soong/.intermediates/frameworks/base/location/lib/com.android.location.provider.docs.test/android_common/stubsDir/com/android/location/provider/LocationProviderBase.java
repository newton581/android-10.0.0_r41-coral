/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.location.provider;

import android.location.Location;
import android.os.Bundle;

/**
 * Base class for location providers implemented as unbundled services.
 *
 * <p>The network location provider must export a service with action
 * "com.android.location.service.v2.NetworkLocationProvider"
 * and a valid minor version in a meta-data field on the service, and
 * then return the result of {@link #getBinder()} on service binding.
 *
 * <p>The fused location provider must export a service with action
 * "com.android.location.service.FusedLocationProvider"
 * and a valid minor version in a meta-data field on the service, and
 * then return the result of {@link #getBinder()} on service binding.
 *
 * <p>IMPORTANT: This class is effectively a public API for unbundled
 * applications, and must remain API stable. See README.txt in the root
 * of this package for more information.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class LocationProviderBase {

public LocationProviderBase(java.lang.String tag, com.android.location.provider.ProviderPropertiesUnbundled properties) { throw new RuntimeException("Stub!"); }

public android.os.IBinder getBinder() { throw new RuntimeException("Stub!"); }

/**
 * Sets whether this provider is currently enabled or not. Note that this is specific to the
 * provider only, and is not related to global location settings. This is a hint to the Location
 * Manager that this provider will generally be unable to fulfill incoming requests. This
 * provider may still receive callbacks to onSetRequest while not enabled, and must decide
 * whether to attempt to satisfy those requests or not.
 *
 * Some guidelines: providers should set their own enabled/disabled status based only on state
 * "owned" by that provider. For instance, providers should not take into account the state of
 * the location master setting when setting themselves enabled or disabled, as this state is not
 * owned by a particular provider. If a provider requires some additional user consent that is
 * particular to the provider, this should be use to set the enabled/disabled state. If the
 * provider proxies to another provider, the child provider's enabled/disabled state should be
 * taken into account in the parent's enabled/disabled state. For most providers, it is expected
 * that they will be always enabled.
 */

public void setEnabled(boolean enabled) { throw new RuntimeException("Stub!"); }

/**
 * Sets the provider properties that may be queried by clients. Generally speaking, providers
 * should try to avoid changing their properties after construction.
 */

public void setProperties(com.android.location.provider.ProviderPropertiesUnbundled properties) { throw new RuntimeException("Stub!"); }

/**
 * Sets a list of additional packages that should be considered as part of this location
 * provider for the purposes of generating locations. This should generally only be used when
 * another package may issue location requests on behalf of this package in the course of
 * providing location. This will inform location services to treat the other packages as
 * location providers as well.
 */

public void setAdditionalProviderPackages(java.util.List<java.lang.String> packageNames) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if this provider has been set as enabled. This will be true unless explicitly
 * set otherwise.
 */

public boolean isEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Reports a new location from this provider.
 */

public void reportLocation(android.location.Location location) { throw new RuntimeException("Stub!"); }

protected void onInit() { throw new RuntimeException("Stub!"); }

/**
 * @deprecated This callback will be invoked once when the provider is created to maintain
 * backwards compatibility with providers not designed for Android Q and above. This method
 * should only be implemented in location providers that need to support SDKs below Android Q.
 * Even in this case, it is usually unnecessary to implement this callback with the correct
 * design. This method may be removed in the future.
 */

@Deprecated
protected void onEnable() { throw new RuntimeException("Stub!"); }

/**
 * @deprecated This callback will be never be invoked on Android Q and above. This method should
 * only be implemented in location providers that need to support SDKs below Android Q. Even in
 * this case, it is usually unnecessary to implement this callback with the correct design. This
 * method may be removed in the future.
 */

@Deprecated
protected void onDisable() { throw new RuntimeException("Stub!"); }

/**
 * Set the {@link ProviderRequest} requirements for this provider. Each call to this method
 * overrides all previous requests. This method might trigger the provider to start returning
 * locations, or to stop returning locations, depending on the parameters in the request.
 */

protected abstract void onSetRequest(com.android.location.provider.ProviderRequestUnbundled request, android.os.WorkSource source);

/**
 * @deprecated This callback will never be invoked on Android Q and above. This method may be
 * removed in the future. Prefer to dump provider state via the containing service instead.
 */

@Deprecated
protected void onDump(java.io.FileDescriptor fd, java.io.PrintWriter pw, java.lang.String[] args) { throw new RuntimeException("Stub!"); }

/**
 * This method will no longer be invoked.
 *
 * Returns a information on the status of this provider.
 * <p>{@link android.location.LocationProvider#OUT_OF_SERVICE} is returned if the provider is
 * out of service, and this is not expected to change in the near
 * future; {@link android.location.LocationProvider#TEMPORARILY_UNAVAILABLE} is returned if
 * the provider is temporarily unavailable but is expected to be
 * available shortly; and {@link android.location.LocationProvider#AVAILABLE} is returned
 * if the provider is currently available.
 *
 * <p>If extras is non-null, additional status information may be
 * added to it in the form of provider-specific key/value pairs.
 *
 * @deprecated This callback will be never be invoked on Android Q and above. This method should
 * only be implemented in location providers that need to support SDKs below Android Q. This
 * method may be removed in the future.
 */

@Deprecated
protected int onGetStatus(android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * This method will no longer be invoked.
 *
 * Returns the time at which the status was last updated. It is the
 * responsibility of the provider to appropriately set this value using
 * {@link android.os.SystemClock#elapsedRealtime SystemClock.elapsedRealtime()}.
 * there is a status update that it wishes to broadcast to all its
 * listeners. The provider should be careful not to broadcast
 * the same status again.
 *
 * @return time of last status update in millis since last reboot
 *
 * @deprecated This callback will be never be invoked on Android Q and above. This method should
 * only be implemented in location providers that need to support SDKs below Android Q. This
 * method may be removed in the future.
 */

@Deprecated
protected long onGetStatusUpdateTime() { throw new RuntimeException("Stub!"); }

/**
 * Implements location provider specific custom commands. The return value will be ignored on
 * Android Q and above.
 */

protected boolean onSendExtraCommand(java.lang.String command, android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Bundle key for a version of the location containing no GPS data.
 * Allows location providers to flag locations as being safe to
 * feed to LocationFudger.
 */

public static final java.lang.String EXTRA_NO_GPS_LOCATION = "noGPSLocation";

/**
 * Name of the Fused location provider.
 *
 * <p>This provider combines inputs for all possible location sources
 * to provide the best possible Location fix.
 */

public static final java.lang.String FUSED_PROVIDER = "fused";
}

