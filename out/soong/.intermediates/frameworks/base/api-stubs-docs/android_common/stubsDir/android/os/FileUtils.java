/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;

import java.io.File;
import java.io.FileDescriptor;
import android.system.Os;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.zip.CRC32;
import java.security.MessageDigest;
import java.io.FileNotFoundException;

/**
 * Utility methods useful for working with files.
 * @apiSince 29
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class FileUtils {

FileUtils() { throw new RuntimeException("Stub!"); }

/**
 * Copy the contents of one stream to another.
 * <p>
 * Attempts to use several optimization strategies to copy the data in the
 * kernel before falling back to a userspace copy as a last resort.
 *
 * @param in This value must never be {@code null}.
 * @param out This value must never be {@code null}.
 * @return number of bytes copied.
 * @apiSince 29
 */

public static long copy(@android.annotation.NonNull java.io.InputStream in, @android.annotation.NonNull java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Copy the contents of one stream to another.
 * <p>
 * Attempts to use several optimization strategies to copy the data in the
 * kernel before falling back to a userspace copy as a last resort.
 *
 * @param signal to signal if the copy should be cancelled early.
 * This value may be {@code null}.
 * @param executor that listener events should be delivered via.
 * This value may be {@code null}.
 * @param listener to be periodically notified as the copy progresses.
 * This value may be {@code null}.
 * @param in This value must never be {@code null}.
 * @param out This value must never be {@code null}.
 * @return number of bytes copied.
 * @apiSince 29
 */

public static long copy(@android.annotation.NonNull java.io.InputStream in, @android.annotation.NonNull java.io.OutputStream out, @android.annotation.Nullable android.os.CancellationSignal signal, @android.annotation.Nullable java.util.concurrent.Executor executor, @android.annotation.Nullable android.os.FileUtils.ProgressListener listener) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Copy the contents of one FD to another.
 * <p>
 * Attempts to use several optimization strategies to copy the data in the
 * kernel before falling back to a userspace copy as a last resort.
 *
 * @param in This value must never be {@code null}.
 * @param out This value must never be {@code null}.
 * @return number of bytes copied.
 * @apiSince 29
 */

public static long copy(@android.annotation.NonNull java.io.FileDescriptor in, @android.annotation.NonNull java.io.FileDescriptor out) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Copy the contents of one FD to another.
 * <p>
 * Attempts to use several optimization strategies to copy the data in the
 * kernel before falling back to a userspace copy as a last resort.
 *
 * @param signal to signal if the copy should be cancelled early.
 * This value may be {@code null}.
 * @param executor that listener events should be delivered via.
 * This value may be {@code null}.
 * @param listener to be periodically notified as the copy progresses.
 * This value may be {@code null}.
 * @param in This value must never be {@code null}.
 * @param out This value must never be {@code null}.
 * @return number of bytes copied.
 * @apiSince 29
 */

public static long copy(@android.annotation.NonNull java.io.FileDescriptor in, @android.annotation.NonNull java.io.FileDescriptor out, @android.annotation.Nullable android.os.CancellationSignal signal, @android.annotation.Nullable java.util.concurrent.Executor executor, @android.annotation.Nullable android.os.FileUtils.ProgressListener listener) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Closes the given object quietly, ignoring any checked exceptions. Does
 * nothing if the given object is {@code null}.
 
 * @param closeable This value may be {@code null}.
 * @apiSince 29
 */

public static void closeQuietly(@android.annotation.Nullable java.lang.AutoCloseable closeable) { throw new RuntimeException("Stub!"); }

/**
 * Closes the given object quietly, ignoring any checked exceptions. Does
 * nothing if the given object is {@code null}.
 
 * @param fd This value may be {@code null}.
 * @apiSince 29
 */

public static void closeQuietly(@android.annotation.Nullable java.io.FileDescriptor fd) { throw new RuntimeException("Stub!"); }
/**
 * Listener that is called periodically as progress is made.
 * @apiSince 29
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface ProgressListener {

/** @apiSince 29 */

public void onProgress(long progress);
}

}

