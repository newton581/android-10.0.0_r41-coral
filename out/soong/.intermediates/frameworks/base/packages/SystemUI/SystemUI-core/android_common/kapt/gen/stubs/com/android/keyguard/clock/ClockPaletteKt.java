package com.android.keyguard.clock;

import android.graphics.Color;
import android.util.MathUtils;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000"}, d2 = {"PRIMARY_INDEX", "", "SECONDARY_DARK_INDEX", "SECONDARY_LIGHT_INDEX"})
public final class ClockPaletteKt {
    private static final int PRIMARY_INDEX = 5;
    private static final int SECONDARY_DARK_INDEX = 8;
    private static final int SECONDARY_LIGHT_INDEX = 2;
}