package com.android.systemui.statusbar.phone;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.biometrics.BiometricSourceType;
import android.provider.Settings;
import com.android.systemui.plugins.statusbar.StatusBarStateController;
import com.android.systemui.statusbar.NotificationLockscreenUserManager;
import com.android.systemui.statusbar.StatusBarState;
import com.android.systemui.tuner.TunerService;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.inject.Singleton;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u0000 22\u00020\u0001:\u00012B\'\b\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010(\u001a\u00020\fJ\u0006\u0010)\u001a\u00020\fJ\u000e\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020-J\u0006\u0010.\u001a\u00020+J\u000e\u0010/\u001a\u00020\f2\u0006\u00100\u001a\u00020\u001bJ\u0006\u00101\u001a\u00020+R\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R \u0010\u0012\u001a\u00020\f2\u0006\u0010\u0011\u001a\u00020\f8F@BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u000eR\u000e\u0010\u0014\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u000e\"\u0004\b\u0016\u0010\u0010R\u001a\u0010\u0017\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u000e\"\u0004\b\u0019\u0010\u0010R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\u001d\u001a\u00020\f2\u0006\u0010\u001c\u001a\u00020\f@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u000e\"\u0004\b\u001f\u0010\u0010R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010 \u001a\u00020!X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000"}, d2 = {"Lcom/android/systemui/statusbar/phone/KeyguardBypassController;", "", "context", "Landroid/content/Context;", "tunerService", "Lcom/android/systemui/tuner/TunerService;", "statusBarStateController", "Lcom/android/systemui/plugins/statusbar/StatusBarStateController;", "lockscreenUserManager", "Lcom/android/systemui/statusbar/NotificationLockscreenUserManager;", "(Landroid/content/Context;Lcom/android/systemui/tuner/TunerService;Lcom/android/systemui/plugins/statusbar/StatusBarStateController;Lcom/android/systemui/statusbar/NotificationLockscreenUserManager;)V", "bouncerShowing", "", "getBouncerShowing", "()Z", "setBouncerShowing", "(Z)V", "<set-?>", "bypassEnabled", "getBypassEnabled", "hasFaceFeature", "isPulseExpanding", "setPulseExpanding", "launchingAffordance", "getLaunchingAffordance", "setLaunchingAffordance", "pendingUnlockType", "Landroid/hardware/biometrics/BiometricSourceType;", "value", "qSExpanded", "getQSExpanded", "setQSExpanded", "unlockController", "Lcom/android/systemui/statusbar/phone/BiometricUnlockController;", "getUnlockController", "()Lcom/android/systemui/statusbar/phone/BiometricUnlockController;", "setUnlockController", "(Lcom/android/systemui/statusbar/phone/BiometricUnlockController;)V", "unlockMethodCache", "Lcom/android/systemui/statusbar/phone/UnlockMethodCache;", "canBypass", "canPlaySubtleWindowAnimations", "dump", "", "pw", "Ljava/io/PrintWriter;", "maybePerformPendingUnlock", "onBiometricAuthenticated", "biometricSourceType", "onStartedGoingToSleep", "Companion"})
@javax.inject.Singleton()
public final class KeyguardBypassController {
    private final com.android.systemui.statusbar.phone.UnlockMethodCache unlockMethodCache = null;
    private final com.android.systemui.plugins.statusbar.StatusBarStateController statusBarStateController = null;
    private boolean hasFaceFeature;
    
    /**
     * * The pending unlock type which is set if the bypass was blocked when it happened.
     */
    private android.hardware.biometrics.BiometricSourceType pendingUnlockType;
    @org.jetbrains.annotations.NotNull()
    public com.android.systemui.statusbar.phone.BiometricUnlockController unlockController;
    private boolean isPulseExpanding;
    
    /**
     * * If face unlock dismisses the lock screen or keeps user on keyguard for the current user.
     */
    private boolean bypassEnabled;
    private boolean bouncerShowing;
    private boolean launchingAffordance;
    private boolean qSExpanded;
    public static final int BYPASS_PANEL_FADE_DURATION = 67;
    public static final com.android.systemui.statusbar.phone.KeyguardBypassController.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.android.systemui.statusbar.phone.BiometricUnlockController getUnlockController() {
        return null;
    }
    
    public final void setUnlockController(@org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.phone.BiometricUnlockController p0) {
    }
    
    public final boolean isPulseExpanding() {
        return false;
    }
    
    public final void setPulseExpanding(boolean p0) {
    }
    
    public final boolean getBypassEnabled() {
        return false;
    }
    
    public final boolean getBouncerShowing() {
        return false;
    }
    
    public final void setBouncerShowing(boolean p0) {
    }
    
    public final boolean getLaunchingAffordance() {
        return false;
    }
    
    public final void setLaunchingAffordance(boolean p0) {
    }
    
    public final boolean getQSExpanded() {
        return false;
    }
    
    public final void setQSExpanded(boolean value) {
    }
    
    /**
     * * Notify that the biometric unlock has happened.
     *     *
     *     * @return false if we can not wake and unlock right now
     */
    public final boolean onBiometricAuthenticated(@org.jetbrains.annotations.NotNull()
    android.hardware.biometrics.BiometricSourceType biometricSourceType) {
        return false;
    }
    
    public final void maybePerformPendingUnlock() {
    }
    
    /**
     * * If keyguard can be dismissed because of bypass.
     */
    public final boolean canBypass() {
        return false;
    }
    
    /**
     * * If shorter animations should be played when unlocking.
     */
    public final boolean canPlaySubtleWindowAnimations() {
        return false;
    }
    
    public final void onStartedGoingToSleep() {
    }
    
    public final void dump(@org.jetbrains.annotations.NotNull()
    java.io.PrintWriter pw) {
    }
    
    @javax.inject.Inject()
    public KeyguardBypassController(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.android.systemui.tuner.TunerService tunerService, @org.jetbrains.annotations.NotNull()
    com.android.systemui.plugins.statusbar.StatusBarStateController statusBarStateController, @org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.NotificationLockscreenUserManager lockscreenUserManager) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000"}, d2 = {"Lcom/android/systemui/statusbar/phone/KeyguardBypassController$Companion;", "", "()V", "BYPASS_PANEL_FADE_DURATION", ""})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}