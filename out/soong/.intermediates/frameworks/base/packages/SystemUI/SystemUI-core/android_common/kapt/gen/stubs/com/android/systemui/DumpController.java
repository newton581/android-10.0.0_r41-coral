package com.android.systemui;

import android.util.Log;
import androidx.annotation.GuardedBy;
import com.android.internal.util.Preconditions;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * * Controller that allows any [Dumpable] to subscribe and be dumped along with other SystemUI
 * * dependencies.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0007\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0001J/\u0010\r\u001a\u00020\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013H\u0016\u00a2\u0006\u0002\u0010\u0015J\u000e\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0001R\u001c\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010\u00050\u00048\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u00078F\u00a2\u0006\u0006\u001a\u0004\b\b\u0010\t"}, d2 = {"Lcom/android/systemui/DumpController;", "Lcom/android/systemui/Dumpable;", "()V", "listeners", "", "Ljava/lang/ref/WeakReference;", "numListeners", "", "getNumListeners", "()I", "addListener", "", "listener", "dump", "fd", "Ljava/io/FileDescriptor;", "pw", "Ljava/io/PrintWriter;", "args", "", "", "(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V", "removeListener", "Companion"})
@javax.inject.Singleton()
public final class DumpController implements com.android.systemui.Dumpable {
    @androidx.annotation.GuardedBy(value = "listeners")
    private final java.util.List<java.lang.ref.WeakReference<com.android.systemui.Dumpable>> listeners = null;
    private static final java.lang.String TAG = "DumpController";
    private static final boolean DEBUG = false;
    public static final com.android.systemui.DumpController.Companion Companion = null;
    
    public final int getNumListeners() {
        return 0;
    }
    
    /**
     * * Adds a [Dumpable] listener to be dumped. It will only be added if it is not already tracked.
     *     *
     *     * @param listener the [Dumpable] to be added
     */
    public final void addListener(@org.jetbrains.annotations.NotNull()
    com.android.systemui.Dumpable listener) {
    }
    
    /**
     * * Removes a listener from the list of elements to be dumped.
     *     *
     *     * @param listener the [Dumpable] to be removed.
     */
    public final void removeListener(@org.jetbrains.annotations.NotNull()
    com.android.systemui.Dumpable listener) {
    }
    
    /**
     * * Dump all the [Dumpable] registered with the controller
     */
    @java.lang.Override()
    public void dump(@org.jetbrains.annotations.Nullable()
    java.io.FileDescriptor fd, @org.jetbrains.annotations.NotNull()
    java.io.PrintWriter pw, @org.jetbrains.annotations.Nullable()
    java.lang.String[] args) {
    }
    
    @javax.inject.Inject()
    public DumpController() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000"}, d2 = {"Lcom/android/systemui/DumpController$Companion;", "", "()V", "DEBUG", "", "TAG", ""})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}