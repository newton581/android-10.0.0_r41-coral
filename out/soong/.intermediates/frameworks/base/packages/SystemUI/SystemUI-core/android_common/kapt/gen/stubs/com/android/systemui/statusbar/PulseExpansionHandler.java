package com.android.systemui.statusbar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.os.PowerManager;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import com.android.systemui.Dependency;
import com.android.systemui.Gefingerpoken;
import com.android.systemui.Interpolators;
import com.android.systemui.R;
import com.android.systemui.plugins.FalsingManager;
import com.android.systemui.plugins.statusbar.StatusBarStateController;
import com.android.systemui.statusbar.notification.NotificationWakeUpCoordinator;
import com.android.systemui.statusbar.notification.row.ExpandableNotificationRow;
import com.android.systemui.statusbar.notification.row.ExpandableView;
import com.android.systemui.statusbar.notification.stack.NotificationRoundnessManager;
import com.android.systemui.statusbar.notification.stack.NotificationStackScrollLayout;
import com.android.systemui.statusbar.phone.HeadsUpManagerPhone;
import com.android.systemui.statusbar.phone.KeyguardBypassController;
import com.android.systemui.statusbar.phone.ShadeController;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * * A utility class to enable the downward swipe on when pulsing.
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0012\b\u0007\u0018\u0000 [2\u00020\u0001:\u0002[\\B7\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\b\u0010A\u001a\u00020BH\u0002J\u0018\u0010C\u001a\u00020B2\u0006\u0010D\u001a\u00020!2\u0006\u0010E\u001a\u00020!H\u0002J\u001a\u0010F\u001a\u0004\u0018\u00010-2\u0006\u0010D\u001a\u00020!2\u0006\u0010E\u001a\u00020!H\u0002J\b\u0010G\u001a\u00020BH\u0002J\u0010\u0010H\u001a\u00020\u00102\u0006\u0010I\u001a\u00020JH\u0002J\u0010\u0010K\u001a\u00020\u00102\u0006\u0010I\u001a\u00020JH\u0016J\u0006\u0010L\u001a\u00020BJ\u0010\u0010M\u001a\u00020\u00102\u0006\u0010I\u001a\u00020JH\u0016J\b\u0010N\u001a\u00020BH\u0002J\u0010\u0010O\u001a\u00020B2\u0006\u0010P\u001a\u00020-H\u0002J\b\u0010Q\u001a\u00020BH\u0002J\u0010\u0010R\u001a\u00020B2\u0006\u0010S\u001a\u00020!H\u0002J\u000e\u0010T\u001a\u00020B2\u0006\u0010U\u001a\u00020\u0010J\u001e\u0010V\u001a\u00020B2\u0006\u0010=\u001a\u00020>2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010;\u001a\u00020<J\u0018\u0010W\u001a\u00020B2\u0006\u0010P\u001a\u00020-2\u0006\u0010X\u001a\u00020\u0010H\u0002J\u0010\u0010Y\u001a\u00020B2\u0006\u0010Z\u001a\u00020!H\u0002R\u001a\u0010\u000f\u001a\u00020\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0018\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u0010@BX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0012\"\u0004\b\u0019\u0010\u0014R\u0014\u0010\u001a\u001a\u00020\u00108BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u0012R\u001e\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u0010@BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0012R\u001e\u0010\u001d\u001a\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u0010@BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0012R\u000e\u0010\u001f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010(\u001a\u0004\u0018\u00010)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010,\u001a\u0004\u0018\u00010-X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020/X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00101\u001a\u00020!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u00102\u001a\u0004\u0018\u000103X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\u001a\u00108\u001a\u00020\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b9\u0010\u0012\"\u0004\b:\u0010\u0014R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010;\u001a\u00020<X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020>X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010?\u001a\u0004\u0018\u00010@X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000"}, d2 = {"Lcom/android/systemui/statusbar/PulseExpansionHandler;", "Lcom/android/systemui/Gefingerpoken;", "context", "Landroid/content/Context;", "wakeUpCoordinator", "Lcom/android/systemui/statusbar/notification/NotificationWakeUpCoordinator;", "bypassController", "Lcom/android/systemui/statusbar/phone/KeyguardBypassController;", "headsUpManager", "Lcom/android/systemui/statusbar/phone/HeadsUpManagerPhone;", "roundnessManager", "Lcom/android/systemui/statusbar/notification/stack/NotificationRoundnessManager;", "statusBarStateController", "Lcom/android/systemui/plugins/statusbar/StatusBarStateController;", "(Landroid/content/Context;Lcom/android/systemui/statusbar/notification/NotificationWakeUpCoordinator;Lcom/android/systemui/statusbar/phone/KeyguardBypassController;Lcom/android/systemui/statusbar/phone/HeadsUpManagerPhone;Lcom/android/systemui/statusbar/notification/stack/NotificationRoundnessManager;Lcom/android/systemui/plugins/statusbar/StatusBarStateController;)V", "bouncerShowing", "", "getBouncerShowing", "()Z", "setBouncerShowing", "(Z)V", "expansionCallback", "Lcom/android/systemui/statusbar/PulseExpansionHandler$ExpansionCallback;", "value", "isExpanding", "setExpanding", "isFalseTouch", "<set-?>", "isWakingToShadeLocked", "leavingLockscreen", "getLeavingLockscreen", "mDraggedFarEnough", "mEmptyDragAmount", "", "mFalsingManager", "Lcom/android/systemui/plugins/FalsingManager;", "mInitialTouchX", "mInitialTouchY", "mMinDragDistance", "", "mPowerManager", "Landroid/os/PowerManager;", "mPulsing", "mReachedWakeUpHeight", "mStartingChild", "Lcom/android/systemui/statusbar/notification/row/ExpandableView;", "mTemp2", "", "mTouchSlop", "mWakeUpHeight", "pulseExpandAbortListener", "Ljava/lang/Runnable;", "getPulseExpandAbortListener", "()Ljava/lang/Runnable;", "setPulseExpandAbortListener", "(Ljava/lang/Runnable;)V", "qsExpanded", "getQsExpanded", "setQsExpanded", "shadeController", "Lcom/android/systemui/statusbar/phone/ShadeController;", "stackScroller", "Lcom/android/systemui/statusbar/notification/stack/NotificationStackScrollLayout;", "velocityTracker", "Landroid/view/VelocityTracker;", "cancelExpansion", "", "captureStartingChild", "x", "y", "findView", "finishExpansion", "maybeStartExpansion", "event", "Landroid/view/MotionEvent;", "onInterceptTouchEvent", "onStartedWakingUp", "onTouchEvent", "recycleVelocityTracker", "reset", "child", "resetClock", "setEmptyDragAmount", "amount", "setPulsing", "pulsing", "setUp", "setUserLocked", "userLocked", "updateExpansionHeight", "height", "Companion", "ExpansionCallback"})
@javax.inject.Singleton()
public final class PulseExpansionHandler implements com.android.systemui.Gefingerpoken {
    private final android.os.PowerManager mPowerManager = null;
    private com.android.systemui.statusbar.phone.ShadeController shadeController;
    private final int mMinDragDistance = 0;
    private float mInitialTouchX;
    private float mInitialTouchY;
    private boolean isExpanding;
    private boolean leavingLockscreen;
    private final float mTouchSlop = 0.0F;
    private com.android.systemui.statusbar.PulseExpansionHandler.ExpansionCallback expansionCallback;
    private com.android.systemui.statusbar.notification.stack.NotificationStackScrollLayout stackScroller;
    private final int[] mTemp2 = null;
    private boolean mDraggedFarEnough;
    private com.android.systemui.statusbar.notification.row.ExpandableView mStartingChild;
    private final com.android.systemui.plugins.FalsingManager mFalsingManager = null;
    private boolean mPulsing;
    private boolean isWakingToShadeLocked;
    private float mEmptyDragAmount;
    private float mWakeUpHeight;
    private boolean mReachedWakeUpHeight;
    private android.view.VelocityTracker velocityTracker;
    private boolean qsExpanded;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Runnable pulseExpandAbortListener;
    private boolean bouncerShowing;
    private final com.android.systemui.statusbar.notification.NotificationWakeUpCoordinator wakeUpCoordinator = null;
    private final com.android.systemui.statusbar.phone.KeyguardBypassController bypassController = null;
    private final com.android.systemui.statusbar.phone.HeadsUpManagerPhone headsUpManager = null;
    private final com.android.systemui.statusbar.notification.stack.NotificationRoundnessManager roundnessManager = null;
    private final com.android.systemui.plugins.statusbar.StatusBarStateController statusBarStateController = null;
    private static final float RUBBERBAND_FACTOR_STATIC = 0.25F;
    private static final int SPRING_BACK_ANIMATION_LENGTH_MS = 375;
    public static final com.android.systemui.statusbar.PulseExpansionHandler.Companion Companion = null;
    
    public final boolean isExpanding() {
        return false;
    }
    
    private final void setExpanding(boolean value) {
    }
    
    public final boolean getLeavingLockscreen() {
        return false;
    }
    
    public final boolean isWakingToShadeLocked() {
        return false;
    }
    
    private final boolean isFalseTouch() {
        return false;
    }
    
    public final boolean getQsExpanded() {
        return false;
    }
    
    public final void setQsExpanded(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Runnable getPulseExpandAbortListener() {
        return null;
    }
    
    public final void setPulseExpandAbortListener(@org.jetbrains.annotations.Nullable()
    java.lang.Runnable p0) {
    }
    
    public final boolean getBouncerShowing() {
        return false;
    }
    
    public final void setBouncerShowing(boolean p0) {
    }
    
    @java.lang.Override()
    public boolean onInterceptTouchEvent(@org.jetbrains.annotations.NotNull()
    android.view.MotionEvent event) {
        return false;
    }
    
    private final boolean maybeStartExpansion(android.view.MotionEvent event) {
        return false;
    }
    
    private final void recycleVelocityTracker() {
    }
    
    @java.lang.Override()
    public boolean onTouchEvent(@org.jetbrains.annotations.NotNull()
    android.view.MotionEvent event) {
        return false;
    }
    
    private final void finishExpansion() {
    }
    
    private final void updateExpansionHeight(float height) {
    }
    
    private final void captureStartingChild(float x, float y) {
    }
    
    private final void setEmptyDragAmount(float amount) {
    }
    
    private final void reset(com.android.systemui.statusbar.notification.row.ExpandableView child) {
    }
    
    private final void setUserLocked(com.android.systemui.statusbar.notification.row.ExpandableView child, boolean userLocked) {
    }
    
    private final void resetClock() {
    }
    
    private final void cancelExpansion() {
    }
    
    private final com.android.systemui.statusbar.notification.row.ExpandableView findView(float x, float y) {
        return null;
    }
    
    public final void setUp(@org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.notification.stack.NotificationStackScrollLayout stackScroller, @org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.PulseExpansionHandler.ExpansionCallback expansionCallback, @org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.phone.ShadeController shadeController) {
    }
    
    public final void setPulsing(boolean pulsing) {
    }
    
    public final void onStartedWakingUp() {
    }
    
    @javax.inject.Inject()
    public PulseExpansionHandler(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.notification.NotificationWakeUpCoordinator wakeUpCoordinator, @org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.phone.KeyguardBypassController bypassController, @org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.phone.HeadsUpManagerPhone headsUpManager, @org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.notification.stack.NotificationRoundnessManager roundnessManager, @org.jetbrains.annotations.NotNull()
    com.android.systemui.plugins.statusbar.StatusBarStateController statusBarStateController) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&"}, d2 = {"Lcom/android/systemui/statusbar/PulseExpansionHandler$ExpansionCallback;", "", "setEmptyDragAmount", "", "amount", ""})
    public static abstract interface ExpansionCallback {
        
        public abstract void setEmptyDragAmount(float amount);
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000"}, d2 = {"Lcom/android/systemui/statusbar/PulseExpansionHandler$Companion;", "", "()V", "RUBBERBAND_FACTOR_STATIC", "", "SPRING_BACK_ANIMATION_LENGTH_MS", ""})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}