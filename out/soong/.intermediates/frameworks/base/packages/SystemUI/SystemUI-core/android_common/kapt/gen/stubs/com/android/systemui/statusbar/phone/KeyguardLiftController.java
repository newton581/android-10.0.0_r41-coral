package com.android.systemui.statusbar.phone;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import com.android.keyguard.KeyguardUpdateMonitor;
import com.android.keyguard.KeyguardUpdateMonitorCallback;
import com.android.systemui.plugins.statusbar.StatusBarStateController;
import com.android.systemui.util.Assert;
import com.android.systemui.util.AsyncSensorManager;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u00012\u00020\u0002B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000bH\u0016J\u0010\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u000bH\u0016J\u0010\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u001a\u001a\u00020\u000bH\u0016J\b\u0010\u001b\u001a\u00020\u0015H\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\n \u000f*\u0004\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\n \u000f*\u0004\u0018\u00010\u00130\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000"}, d2 = {"Lcom/android/systemui/statusbar/phone/KeyguardLiftController;", "Lcom/android/systemui/plugins/statusbar/StatusBarStateController$StateListener;", "Lcom/android/keyguard/KeyguardUpdateMonitorCallback;", "context", "Landroid/content/Context;", "statusBarStateController", "Lcom/android/systemui/plugins/statusbar/StatusBarStateController;", "asyncSensorManager", "Lcom/android/systemui/util/AsyncSensorManager;", "(Landroid/content/Context;Lcom/android/systemui/plugins/statusbar/StatusBarStateController;Lcom/android/systemui/util/AsyncSensorManager;)V", "bouncerVisible", "", "isListening", "keyguardUpdateMonitor", "Lcom/android/keyguard/KeyguardUpdateMonitor;", "kotlin.jvm.PlatformType", "listener", "Landroid/hardware/TriggerEventListener;", "pickupSensor", "Landroid/hardware/Sensor;", "onDozingChanged", "", "isDozing", "onKeyguardBouncerChanged", "bouncer", "onKeyguardVisibilityChanged", "showing", "updateListeningState"})
public final class KeyguardLiftController extends com.android.keyguard.KeyguardUpdateMonitorCallback implements com.android.systemui.plugins.statusbar.StatusBarStateController.StateListener {
    private final com.android.keyguard.KeyguardUpdateMonitor keyguardUpdateMonitor = null;
    private final android.hardware.Sensor pickupSensor = null;
    private boolean isListening;
    private boolean bouncerVisible;
    private final android.hardware.TriggerEventListener listener = null;
    private final com.android.systemui.plugins.statusbar.StatusBarStateController statusBarStateController = null;
    private final com.android.systemui.util.AsyncSensorManager asyncSensorManager = null;
    
    @java.lang.Override()
    public void onDozingChanged(boolean isDozing) {
    }
    
    @java.lang.Override()
    public void onKeyguardBouncerChanged(boolean bouncer) {
    }
    
    @java.lang.Override()
    public void onKeyguardVisibilityChanged(boolean showing) {
    }
    
    private final void updateListeningState() {
    }
    
    public KeyguardLiftController(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.android.systemui.plugins.statusbar.StatusBarStateController statusBarStateController, @org.jetbrains.annotations.NotNull()
    com.android.systemui.util.AsyncSensorManager asyncSensorManager) {
        super();
    }
}