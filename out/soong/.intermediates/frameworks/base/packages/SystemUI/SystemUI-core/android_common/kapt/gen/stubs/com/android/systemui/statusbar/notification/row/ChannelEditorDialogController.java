package com.android.systemui.statusbar.notification.row;

import android.app.Dialog;
import android.app.INotificationManager;
import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.android.internal.annotations.VisibleForTesting;
import com.android.systemui.R;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * * ChannelEditorDialogController is the controller for the dialog half-shelf
 * * that allows users to quickly turn off channels. It is launched from the NotificationInfo
 * * guts view and displays controls for toggling app notifications as well as up to 4 channels
 * * from that app like so:
 * *
 * *   APP TOGGLE                                                 <on/off>
 * *   - Channel from which we launched                           <on/off>
 * *   -                                                          <on/off>
 * *   - the next 3 channels sorted alphabetically for that app   <on/off>
 * *   -                                                          <on/off>
 */
@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\t\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\b\u00108\u001a\u000209H\u0007J\u0010\u0010:\u001a\u0002092\u0006\u0010;\u001a\u00020\fH\u0002J\b\u0010<\u001a\u000209H\u0002J\b\u0010=\u001a\u00020\fH\u0002J\u0006\u0010>\u001a\u000209J\b\u0010?\u001a\u000209H\u0002J\u000e\u0010@\u001a\b\u0012\u0004\u0012\u00020\u00160AH\u0002J\u001c\u0010B\u001a\b\u0012\u0004\u0012\u00020\"0C2\f\u0010D\u001a\b\u0012\u0004\u0012\u00020\u00160CH\u0002J\u0010\u0010E\u001a\u00020%2\b\u0010F\u001a\u0004\u0018\u00010\nJ\b\u0010G\u001a\u000209H\u0002J\u0010\u0010H\u001a\u0002092\u0006\u0010I\u001a\u00020JH\u0007J\u0016\u0010K\u001a\u0002092\f\u0010L\u001a\b\u0012\u0004\u0012\u00020\"0MH\u0002J>\u0010N\u001a\u0002092\u0006\u0010\t\u001a\u00020\n2\u0006\u00102\u001a\u00020\n2\u0006\u0010O\u001a\u00020\u00122\f\u0010L\u001a\b\u0012\u0004\u0012\u00020\"0M2\u0006\u0010\u0007\u001a\u00020\b2\b\u00100\u001a\u0004\u0018\u000101J\u0016\u0010P\u001a\u0002092\u0006\u0010Q\u001a\u00020\"2\u0006\u0010R\u001a\u00020\u0012J\b\u0010S\u001a\u000209H\u0002J\u0018\u0010T\u001a\u0002092\u0006\u0010Q\u001a\u00020\"2\u0006\u0010U\u001a\u00020\u0012H\u0002J\u0006\u0010V\u001a\u000209R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0013R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0017\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u001a\u0010\u001a\u001a\u00020\u001bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001a\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020\u00120!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R(\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020%0$8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b&\u0010\'\u001a\u0004\b(\u0010)R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010*\u001a\u0004\u0018\u00010+X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R\u0010\u00100\u001a\u0004\u0018\u000101X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00102\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u00103\u001a\b\u0012\u0004\u0012\u00020\"0\u00158\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\b4\u0010\'\u001a\u0004\b5\u00106R\u000e\u00107\u001a\u00020\u0012X\u0082D\u00a2\u0006\u0002\n\u0000"}, d2 = {"Lcom/android/systemui/statusbar/notification/row/ChannelEditorDialogController;", "", "c", "Landroid/content/Context;", "noMan", "Landroid/app/INotificationManager;", "(Landroid/content/Context;Landroid/app/INotificationManager;)V", "appIcon", "Landroid/graphics/drawable/Drawable;", "appName", "", "appNotificationsEnabled", "", "getAppNotificationsEnabled", "()Z", "setAppNotificationsEnabled", "(Z)V", "appUid", "", "Ljava/lang/Integer;", "channelGroupList", "", "Landroid/app/NotificationChannelGroup;", "context", "getContext", "()Landroid/content/Context;", "dialog", "Landroid/app/Dialog;", "getDialog", "()Landroid/app/Dialog;", "setDialog", "(Landroid/app/Dialog;)V", "edits", "", "Landroid/app/NotificationChannel;", "groupNameLookup", "Ljava/util/HashMap;", "", "groupNameLookup$annotations", "()V", "getGroupNameLookup$main", "()Ljava/util/HashMap;", "onFinishListener", "Lcom/android/systemui/statusbar/notification/row/OnChannelEditorDialogFinishedListener;", "getOnFinishListener", "()Lcom/android/systemui/statusbar/notification/row/OnChannelEditorDialogFinishedListener;", "setOnFinishListener", "(Lcom/android/systemui/statusbar/notification/row/OnChannelEditorDialogFinishedListener;)V", "onSettingsClickListener", "Lcom/android/systemui/statusbar/notification/row/NotificationInfo$OnSettingsClickListener;", "packageName", "providedChannels", "providedChannels$annotations", "getProvidedChannels$main", "()Ljava/util/List;", "wmFlags", "apply", "", "applyAppNotificationsOn", "b", "buildGroupNameLookup", "checkAreAppNotificationsOn", "close", "done", "fetchNotificationChannelGroups", "", "getDisplayableChannels", "Lkotlin/sequences/Sequence;", "groupList", "groupNameForId", "groupId", "initDialog", "launchSettings", "sender", "Landroid/view/View;", "padToFourChannels", "channels", "", "prepareDialogForApp", "uid", "proposeEditForChannel", "channel", "edit", "resetState", "setChannelImportance", "importance", "show"})
@javax.inject.Singleton()
public final class ChannelEditorDialogController {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    public android.app.Dialog dialog;
    private android.graphics.drawable.Drawable appIcon;
    private java.lang.Integer appUid;
    private java.lang.String packageName;
    private java.lang.String appName;
    private com.android.systemui.statusbar.notification.row.NotificationInfo.OnSettingsClickListener onSettingsClickListener;
    @org.jetbrains.annotations.Nullable()
    private com.android.systemui.statusbar.notification.row.OnChannelEditorDialogFinishedListener onFinishListener;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<android.app.NotificationChannel> providedChannels = null;
    private final java.util.Map<android.app.NotificationChannel, java.lang.Integer> edits = null;
    private boolean appNotificationsEnabled;
    @org.jetbrains.annotations.NotNull()
    private final java.util.HashMap<java.lang.String, java.lang.CharSequence> groupNameLookup = null;
    private final java.util.List<android.app.NotificationChannelGroup> channelGroupList = null;
    private final int wmFlags = -2130444032;
    private final android.app.INotificationManager noMan = null;
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Dialog getDialog() {
        return null;
    }
    
    public final void setDialog(@org.jetbrains.annotations.NotNull()
    android.app.Dialog p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.android.systemui.statusbar.notification.row.OnChannelEditorDialogFinishedListener getOnFinishListener() {
        return null;
    }
    
    public final void setOnFinishListener(@org.jetbrains.annotations.Nullable()
    com.android.systemui.statusbar.notification.row.OnChannelEditorDialogFinishedListener p0) {
    }
    
    @com.android.internal.annotations.VisibleForTesting()
    public static void providedChannels$annotations() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<android.app.NotificationChannel> getProvidedChannels$main() {
        return null;
    }
    
    public final boolean getAppNotificationsEnabled() {
        return false;
    }
    
    public final void setAppNotificationsEnabled(boolean p0) {
    }
    
    @com.android.internal.annotations.VisibleForTesting()
    public static void groupNameLookup$annotations() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.HashMap<java.lang.String, java.lang.CharSequence> getGroupNameLookup$main() {
        return null;
    }
    
    /**
     * * Give the controller all of the information it needs to present the dialog
     *     * for a given app. Does a bunch of querying of NoMan, but won't present anything yet
     */
    public final void prepareDialogForApp(@org.jetbrains.annotations.NotNull()
    java.lang.String appName, @org.jetbrains.annotations.NotNull()
    java.lang.String packageName, int uid, @org.jetbrains.annotations.NotNull()
    java.util.Set<android.app.NotificationChannel> channels, @org.jetbrains.annotations.NotNull()
    android.graphics.drawable.Drawable appIcon, @org.jetbrains.annotations.Nullable()
    com.android.systemui.statusbar.notification.row.NotificationInfo.OnSettingsClickListener onSettingsClickListener) {
    }
    
    private final void buildGroupNameLookup() {
    }
    
    private final void padToFourChannels(java.util.Set<android.app.NotificationChannel> channels) {
    }
    
    private final kotlin.sequences.Sequence<android.app.NotificationChannel> getDisplayableChannels(kotlin.sequences.Sequence<android.app.NotificationChannelGroup> groupList) {
        return null;
    }
    
    public final void show() {
    }
    
    /**
     * * Close the dialog without saving. For external callers
     */
    public final void close() {
    }
    
    private final void done() {
    }
    
    private final void resetState() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.CharSequence groupNameForId(@org.jetbrains.annotations.Nullable()
    java.lang.String groupId) {
        return null;
    }
    
    public final void proposeEditForChannel(@org.jetbrains.annotations.NotNull()
    android.app.NotificationChannel channel, int edit) {
    }
    
    @kotlin.Suppress(names = {"unchecked_cast"})
    private final java.util.List<android.app.NotificationChannelGroup> fetchNotificationChannelGroups() {
        return null;
    }
    
    private final boolean checkAreAppNotificationsOn() {
        return false;
    }
    
    private final void applyAppNotificationsOn(boolean b) {
    }
    
    private final void setChannelImportance(android.app.NotificationChannel channel, int importance) {
    }
    
    @com.android.internal.annotations.VisibleForTesting()
    public final void apply() {
    }
    
    @com.android.internal.annotations.VisibleForTesting()
    public final void launchSettings(@org.jetbrains.annotations.NotNull()
    android.view.View sender) {
    }
    
    private final void initDialog() {
    }
    
    @javax.inject.Inject()
    public ChannelEditorDialogController(@org.jetbrains.annotations.NotNull()
    android.content.Context c, @org.jetbrains.annotations.NotNull()
    android.app.INotificationManager noMan) {
        super();
    }
}