package com.android.systemui.statusbar.notification;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.FloatProperty;
import com.android.systemui.Interpolators;
import com.android.systemui.plugins.statusbar.StatusBarStateController;
import com.android.systemui.statusbar.StatusBarState;
import com.android.systemui.statusbar.notification.collection.NotificationEntry;
import com.android.systemui.statusbar.notification.stack.NotificationStackScrollLayout;
import com.android.systemui.statusbar.notification.stack.StackStateAnimator;
import com.android.systemui.statusbar.phone.DozeParameters;
import com.android.systemui.statusbar.phone.HeadsUpManagerPhone;
import com.android.systemui.statusbar.phone.KeyguardBypassController;
import com.android.systemui.statusbar.phone.NotificationIconAreaController;
import com.android.systemui.statusbar.phone.PanelExpansionListener;
import com.android.systemui.statusbar.policy.OnHeadsUpChangedListener;
import javax.inject.Inject;
import javax.inject.Singleton;

@kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b&\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0001mB\'\b\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fJ\u000e\u0010F\u001a\u00020G2\u0006\u0010H\u001a\u00020?J\u0006\u0010I\u001a\u00020\u001eJ\b\u0010J\u001a\u00020GH\u0002J\u0006\u0010K\u001a\u00020\u000eJ\u0010\u0010L\u001a\u00020G2\u0006\u0010M\u001a\u00020\u000eH\u0002J\u0018\u0010N\u001a\u00020G2\u0006\u0010O\u001a\u00020\u001e2\u0006\u0010P\u001a\u00020\u001eH\u0016J\u0010\u0010Q\u001a\u00020G2\u0006\u0010R\u001a\u00020\u000eH\u0016J\u0018\u0010S\u001a\u00020G2\u0006\u0010T\u001a\u00020#2\u0006\u0010U\u001a\u00020\u000eH\u0016J\u0018\u0010V\u001a\u00020G2\u0006\u0010W\u001a\u00020\u001e2\u0006\u0010X\u001a\u00020\u000eH\u0016J\u0010\u0010Y\u001a\u00020G2\u0006\u0010Z\u001a\u00020<H\u0016J\u000e\u0010[\u001a\u00020G2\u0006\u0010H\u001a\u00020?J\u0016\u0010\\\u001a\u00020G2\u0006\u0010O\u001a\u00020\u001e2\u0006\u0010P\u001a\u00020\u001eJ \u0010]\u001a\u00020G2\u0006\u0010^\u001a\u00020\u000e2\u0006\u0010_\u001a\u00020\u000e2\u0006\u0010`\u001a\u00020\u000eH\u0002J\u001e\u0010a\u001a\u00020G2\u0006\u0010^\u001a\u00020\u000e2\u0006\u0010_\u001a\u00020\u000e2\u0006\u0010`\u001a\u00020\u000eJ\u000e\u0010b\u001a\u00020\u001e2\u0006\u0010c\u001a\u00020\u001eJ\u000e\u0010d\u001a\u00020G2\u0006\u0010e\u001a\u00020,J\u0010\u0010f\u001a\u00020G2\u0006\u0010g\u001a\u00020\u001eH\u0002J\b\u0010h\u001a\u00020\u000eH\u0002J\u0010\u0010i\u001a\u00020G2\u0006\u0010`\u001a\u00020\u000eH\u0002J\b\u0010j\u001a\u00020\u000eH\u0002J\b\u0010k\u001a\u00020GH\u0002J\u0018\u0010l\u001a\u00020G2\u0006\u0010_\u001a\u00020\u000e2\u0006\u0010`\u001a\u00020\u000eH\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u000f\u001a\u00020\u000e2\u0006\u0010\r\u001a\u00020\u000e8F@BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u000e\u0010\u0012\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0013\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0011\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010!\u001a\b\u0012\u0004\u0012\u00020#0\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00000\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020,X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u001eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010.\u001a\u0004\u0018\u00010/X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u00100\u001a\n 2*\u0004\u0018\u00010101X\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u00104\u001a\u00020\u000e2\u0006\u00103\u001a\u00020\u000e@BX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b5\u0010\u0011\"\u0004\b6\u0010\u0016R\u000e\u00107\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u00108\u001a\u00020\u000e2\u0006\u00103\u001a\u00020\u000e@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b9\u0010\u0011\"\u0004\b:\u0010\u0016R\u000e\u0010;\u001a\u00020<X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010=\u001a\b\u0012\u0004\u0012\u00020?0>X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010@\u001a\u00020\u000e2\u0006\u00103\u001a\u00020\u000e@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bA\u0010\u0011\"\u0004\bB\u0010\u0016R$\u0010C\u001a\u00020\u000e2\u0006\u00103\u001a\u00020\u000e@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010\u0011\"\u0004\bE\u0010\u0016\u0082\u0002\u0007\n\u0005\b\u0091(0\u0001"}, d2 = {"Lcom/android/systemui/statusbar/notification/NotificationWakeUpCoordinator;", "Lcom/android/systemui/statusbar/policy/OnHeadsUpChangedListener;", "Lcom/android/systemui/plugins/statusbar/StatusBarStateController$StateListener;", "Lcom/android/systemui/statusbar/phone/PanelExpansionListener;", "mContext", "Landroid/content/Context;", "mHeadsUpManagerPhone", "Lcom/android/systemui/statusbar/phone/HeadsUpManagerPhone;", "statusBarStateController", "Lcom/android/systemui/plugins/statusbar/StatusBarStateController;", "bypassController", "Lcom/android/systemui/statusbar/phone/KeyguardBypassController;", "(Landroid/content/Context;Lcom/android/systemui/statusbar/phone/HeadsUpManagerPhone;Lcom/android/systemui/plugins/statusbar/StatusBarStateController;Lcom/android/systemui/statusbar/phone/KeyguardBypassController;)V", "<set-?>", "", "canShowPulsingHuns", "getCanShowPulsingHuns", "()Z", "collapsedEnoughToHide", "fullyAwake", "getFullyAwake", "setFullyAwake", "(Z)V", "iconAreaController", "Lcom/android/systemui/statusbar/phone/NotificationIconAreaController;", "getIconAreaController", "()Lcom/android/systemui/statusbar/phone/NotificationIconAreaController;", "setIconAreaController", "(Lcom/android/systemui/statusbar/phone/NotificationIconAreaController;)V", "mDozeAmount", "", "mDozeParameters", "Lcom/android/systemui/statusbar/phone/DozeParameters;", "mEntrySetToClearWhenFinished", "", "Lcom/android/systemui/statusbar/notification/collection/NotificationEntry;", "mLinearDozeAmount", "mLinearVisibilityAmount", "mNotificationVisibility", "Landroid/util/FloatProperty;", "mNotificationVisibleAmount", "mNotificationsVisible", "mNotificationsVisibleForExpansion", "mStackScroller", "Lcom/android/systemui/statusbar/notification/stack/NotificationStackScrollLayout;", "mVisibilityAmount", "mVisibilityAnimator", "Landroid/animation/ObjectAnimator;", "mVisibilityInterpolator", "Landroid/view/animation/Interpolator;", "kotlin.jvm.PlatformType", "value", "notificationsFullyHidden", "getNotificationsFullyHidden", "setNotificationsFullyHidden", "pulseExpanding", "pulsing", "getPulsing", "setPulsing", "state", "", "wakeUpListeners", "Ljava/util/ArrayList;", "Lcom/android/systemui/statusbar/notification/NotificationWakeUpCoordinator$WakeUpListener;", "wakingUp", "getWakingUp", "setWakingUp", "willWakeUp", "getWillWakeUp", "setWillWakeUp", "addListener", "", "listener", "getWakeUpHeight", "handleAnimationFinished", "isPulseExpanding", "notifyAnimationStart", "awake", "onDozeAmountChanged", "linear", "eased", "onDozingChanged", "isDozing", "onHeadsUpStateChanged", "entry", "isHeadsUp", "onPanelExpansionChanged", "expansion", "tracking", "onStateChanged", "newState", "removeListener", "setDozeAmount", "setNotificationsVisible", "visible", "animate", "increaseSpeed", "setNotificationsVisibleForExpansion", "setPulseHeight", "height", "setStackScroller", "stackScroller", "setVisibilityAmount", "visibilityAmount", "shouldAnimateVisibility", "startVisibilityAnimation", "updateDozeAmountIfBypass", "updateHideAmount", "updateNotificationVisibility", "WakeUpListener"})
@javax.inject.Singleton()
public final class NotificationWakeUpCoordinator implements com.android.systemui.statusbar.policy.OnHeadsUpChangedListener, com.android.systemui.plugins.statusbar.StatusBarStateController.StateListener, com.android.systemui.statusbar.phone.PanelExpansionListener {
    private final android.util.FloatProperty<com.android.systemui.statusbar.notification.NotificationWakeUpCoordinator> mNotificationVisibility = null;
    private com.android.systemui.statusbar.notification.stack.NotificationStackScrollLayout mStackScroller;
    private android.view.animation.Interpolator mVisibilityInterpolator;
    private float mLinearDozeAmount;
    private float mDozeAmount;
    private float mNotificationVisibleAmount;
    private boolean mNotificationsVisible;
    private boolean mNotificationsVisibleForExpansion;
    private android.animation.ObjectAnimator mVisibilityAnimator;
    private float mVisibilityAmount;
    private float mLinearVisibilityAmount;
    private final java.util.Set<com.android.systemui.statusbar.notification.collection.NotificationEntry> mEntrySetToClearWhenFinished = null;
    private final com.android.systemui.statusbar.phone.DozeParameters mDozeParameters = null;
    private boolean pulseExpanding;
    private final java.util.ArrayList<com.android.systemui.statusbar.notification.NotificationWakeUpCoordinator.WakeUpListener> wakeUpListeners = null;
    private int state;
    private boolean fullyAwake;
    private boolean wakingUp;
    private boolean willWakeUp;
    private boolean collapsedEnoughToHide;
    @org.jetbrains.annotations.NotNull()
    public com.android.systemui.statusbar.phone.NotificationIconAreaController iconAreaController;
    private boolean pulsing;
    private boolean notificationsFullyHidden;
    
    /**
     * * True if we can show pulsing heads up notifications
     */
    private boolean canShowPulsingHuns;
    private final android.content.Context mContext = null;
    private final com.android.systemui.statusbar.phone.HeadsUpManagerPhone mHeadsUpManagerPhone = null;
    private final com.android.systemui.plugins.statusbar.StatusBarStateController statusBarStateController = null;
    private final com.android.systemui.statusbar.phone.KeyguardBypassController bypassController = null;
    
    public final boolean getFullyAwake() {
        return false;
    }
    
    public final void setFullyAwake(boolean p0) {
    }
    
    public final boolean getWakingUp() {
        return false;
    }
    
    public final void setWakingUp(boolean value) {
    }
    
    public final boolean getWillWakeUp() {
        return false;
    }
    
    public final void setWillWakeUp(boolean value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.android.systemui.statusbar.phone.NotificationIconAreaController getIconAreaController() {
        return null;
    }
    
    public final void setIconAreaController(@org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.phone.NotificationIconAreaController p0) {
    }
    
    public final boolean getPulsing() {
        return false;
    }
    
    public final void setPulsing(boolean value) {
    }
    
    public final boolean getNotificationsFullyHidden() {
        return false;
    }
    
    private final void setNotificationsFullyHidden(boolean value) {
    }
    
    public final boolean getCanShowPulsingHuns() {
        return false;
    }
    
    public final void setStackScroller(@org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.notification.stack.NotificationStackScrollLayout stackScroller) {
    }
    
    public final boolean isPulseExpanding() {
        return false;
    }
    
    /**
     * * @param visible should notifications be visible
     *     * @param animate should this change be animated
     *     * @param increaseSpeed should the speed be increased of the animation
     */
    public final void setNotificationsVisibleForExpansion(boolean visible, boolean animate, boolean increaseSpeed) {
    }
    
    public final void addListener(@org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.notification.NotificationWakeUpCoordinator.WakeUpListener listener) {
    }
    
    public final void removeListener(@org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.notification.NotificationWakeUpCoordinator.WakeUpListener listener) {
    }
    
    private final void updateNotificationVisibility(boolean animate, boolean increaseSpeed) {
    }
    
    private final void setNotificationsVisible(boolean visible, boolean animate, boolean increaseSpeed) {
    }
    
    @java.lang.Override()
    public void onDozeAmountChanged(float linear, float eased) {
    }
    
    public final void setDozeAmount(float linear, float eased) {
    }
    
    @java.lang.Override()
    public void onStateChanged(int newState) {
    }
    
    @java.lang.Override()
    public void onPanelExpansionChanged(float expansion, boolean tracking) {
    }
    
    private final boolean updateDozeAmountIfBypass() {
        return false;
    }
    
    private final void startVisibilityAnimation(boolean increaseSpeed) {
    }
    
    private final void setVisibilityAmount(float visibilityAmount) {
    }
    
    private final void handleAnimationFinished() {
    }
    
    public final float getWakeUpHeight() {
        return 0.0F;
    }
    
    private final void updateHideAmount() {
    }
    
    private final void notifyAnimationStart(boolean awake) {
    }
    
    @java.lang.Override()
    public void onDozingChanged(boolean isDozing) {
    }
    
    /**
     * * Set the height how tall notifications are pulsing. This is only set whenever we are expanding
     *     * from a pulse and determines how much the notifications are expanded.
     */
    public final float setPulseHeight(float height) {
        return 0.0F;
    }
    
    @java.lang.Override()
    public void onHeadsUpStateChanged(@org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.notification.collection.NotificationEntry entry, boolean isHeadsUp) {
    }
    
    private final boolean shouldAnimateVisibility() {
        return false;
    }
    
    @javax.inject.Inject()
    public NotificationWakeUpCoordinator(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.phone.HeadsUpManagerPhone mHeadsUpManagerPhone, @org.jetbrains.annotations.NotNull()
    com.android.systemui.plugins.statusbar.StatusBarStateController statusBarStateController, @org.jetbrains.annotations.NotNull()
    com.android.systemui.statusbar.phone.KeyguardBypassController bypassController) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 13}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0017J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0005H\u0017\u00f8\u0001\u0000\u0082\u0002\u0007\n\u0005\b\u0091(0\u0001"}, d2 = {"Lcom/android/systemui/statusbar/notification/NotificationWakeUpCoordinator$WakeUpListener;", "", "onFullyHiddenChanged", "", "isFullyHidden", "", "onPulseExpansionChanged", "expandingChanged"})
    public static abstract interface WakeUpListener {
        
        /**
         * * Called whenever the notifications are fully hidden or shown
         */
        public void onFullyHiddenChanged(boolean isFullyHidden) {
        }
        
        /**
         * * Called whenever the pulseExpansion changes
         *         * @param expandingChanged if the user has started or stopped expanding
         */
        public void onPulseExpansionChanged(boolean expandingChanged) {
        }
    }
}