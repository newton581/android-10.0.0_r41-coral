/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.android.carrierdefaultapp;

public final class R {
  public static final class dimen {
    /**
     * Default screen margins, per the Android Design guidelines.
     */
    public static final int activity_horizontal_margin=0x7f010000;
    public static final int activity_vertical_margin=0x7f010001;
    public static final int glif_icon_size=0x7f010002;
  }
  public static final class drawable {
    public static final int ic_sim_card=0x7f020000;
  }
  public static final class id {
    public static final int container=0x7f030000;
    public static final int progress_bar=0x7f030001;
    public static final int url_bar=0x7f030002;
    public static final int webview=0x7f030003;
  }
  public static final class layout {
    public static final int activity_captive_portal_login=0x7f040000;
  }
  public static final class mipmap {
    public static final int ic_launcher_android=0x7f050000;
  }
  public static final class string {
    public static final int action_bar_label=0x7f060000;
    public static final int android_system_label=0x7f060001;
    public static final int app_name=0x7f060002;
    public static final int mobile_data_status_notification_channel_name=0x7f060003;
    public static final int no_data_notification_detail=0x7f060004;
    public static final int no_data_notification_id=0x7f060005;
    public static final int no_mobile_data_connection=0x7f060006;
    public static final int no_mobile_data_connection_title=0x7f060007;
    public static final int portal_notification_detail=0x7f060008;
    public static final int portal_notification_id=0x7f060009;
    public static final int ssl_error_continue=0x7f06000a;
    public static final int ssl_error_example=0x7f06000b;
    public static final int ssl_error_warning=0x7f06000c;
  }
  public static final class style {
    public static final int AppBaseTheme=0x7f070000;
    /**
     * Application theme.
     */
    public static final int AppTheme=0x7f070001;
  }
}