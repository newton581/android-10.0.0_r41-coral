/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;

import java.io.FileDescriptor;

/**
 * Collection representing a set of open file descriptors and an opaque data stream.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class NativeHandle implements java.io.Closeable {

/**
 * Constructs a {@link NativeHandle} object containing
 * zero file descriptors and an empty data stream.
 */

public NativeHandle() { throw new RuntimeException("Stub!"); }

/**
 * Constructs a {@link NativeHandle} object containing the given
 * {@link FileDescriptor} object and an empty data stream.

 * @param descriptor This value must never be {@code null}.
 */

public NativeHandle(java.io.FileDescriptor descriptor, boolean own) { throw new RuntimeException("Stub!"); }

/**
 * Instantiate an opaque {@link NativeHandle} from fds and integers.
 *
 * @param own whether the fds are owned by this object and should be closed
 
 * @param fds This value must never be {@code null}.

 * @param ints This value must never be {@code null}.
 */

public NativeHandle(java.io.FileDescriptor[] fds, int[] ints, boolean own) { throw new RuntimeException("Stub!"); }

/**
 * Returns whether this {@link NativeHandle} object contains a single file
 * descriptor and nothing else.
 *
 * @return a boolean value
 */

public boolean hasSingleFileDescriptor() { throw new RuntimeException("Stub!"); }

/**
 * Explicitly duplicate NativeHandle (this dups all file descritptors).
 *
 * If this method is called, this must also be explicitly closed with
 * {@link #close()}.

 * @return This value will never be {@code null}.
 */

public android.os.NativeHandle dup() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Closes the file descriptors if they are owned by this object.
 *
 * This also invalidates the object.
 */

public void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Returns the underlying lone file descriptor.
 *
 * @return a {@link FileDescriptor} object
 * This value will never be {@code null}.
 * @throws IllegalStateException if this object contains either zero or
 *         more than one file descriptor, or a non-empty data stream.
 */

public java.io.FileDescriptor getFileDescriptor() { throw new RuntimeException("Stub!"); }

/**
 * Fetch file descriptors
 *
 * @return the fds.

 * This value will never be {@code null}.
 */

public java.io.FileDescriptor[] getFileDescriptors() { throw new RuntimeException("Stub!"); }

/**
 * Fetch opaque ints. Note: This object retains ownership of the data.
 *
 * @return the opaque data stream.

 * This value will never be {@code null}.
 */

public int[] getInts() { throw new RuntimeException("Stub!"); }
}

