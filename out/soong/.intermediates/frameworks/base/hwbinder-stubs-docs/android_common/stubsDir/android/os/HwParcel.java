/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;

import java.util.ArrayList;

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HwParcel {

/**
 * Creates an initialized and empty parcel.
 */

public HwParcel() { throw new RuntimeException("Stub!"); }

/**
 * Writes an interface token into the parcel used to verify that
 * a transaction has made it to the write type of interface.
 *
 * @param interfaceName fully qualified name of interface message
 *     is being sent to.
 */

public final native void writeInterfaceToken(java.lang.String interfaceName);

/**
 * Writes a boolean value to the end of the parcel.
 * @param val to write
 */

public final native void writeBool(boolean val);

/**
 * Writes a byte value to the end of the parcel.
 * @param val to write
 */

public final native void writeInt8(byte val);

/**
 * Writes a short value to the end of the parcel.
 * @param val to write
 */

public final native void writeInt16(short val);

/**
 * Writes a int value to the end of the parcel.
 * @param val to write
 */

public final native void writeInt32(int val);

/**
 * Writes a long value to the end of the parcel.
 * @param val to write
 */

public final native void writeInt64(long val);

/**
 * Writes a float value to the end of the parcel.
 * @param val to write
 */

public final native void writeFloat(float val);

/**
 * Writes a double value to the end of the parcel.
 * @param val to write
 */

public final native void writeDouble(double val);

/**
 * Writes a String value to the end of the parcel.
 *
 * Note, this will be converted to UTF-8 when it is written.
 *
 * @param val to write
 */

public final native void writeString(java.lang.String val);

/**
 * Writes a native handle (without duplicating the underlying
 * file descriptors) to the end of the parcel.
 *
 * @param val to write
 */

public final native void writeNativeHandle(android.os.NativeHandle val);

/**
 * Helper method to write a list of Booleans to val.
 * @param val list to write
 */

public final void writeBoolVector(java.util.ArrayList<java.lang.Boolean> val) { throw new RuntimeException("Stub!"); }

/**
 * Helper method to write a list of Booleans to the end of the parcel.
 * @param val list to write
 */

public final void writeInt8Vector(java.util.ArrayList<java.lang.Byte> val) { throw new RuntimeException("Stub!"); }

/**
 * Helper method to write a list of Shorts to the end of the parcel.
 * @param val list to write
 */

public final void writeInt16Vector(java.util.ArrayList<java.lang.Short> val) { throw new RuntimeException("Stub!"); }

/**
 * Helper method to write a list of Integers to the end of the parcel.
 * @param val list to write
 */

public final void writeInt32Vector(java.util.ArrayList<java.lang.Integer> val) { throw new RuntimeException("Stub!"); }

/**
 * Helper method to write a list of Longs to the end of the parcel.
 * @param val list to write
 */

public final void writeInt64Vector(java.util.ArrayList<java.lang.Long> val) { throw new RuntimeException("Stub!"); }

/**
 * Helper method to write a list of Floats to the end of the parcel.
 * @param val list to write
 */

public final void writeFloatVector(java.util.ArrayList<java.lang.Float> val) { throw new RuntimeException("Stub!"); }

/**
 * Helper method to write a list of Doubles to the end of the parcel.
 * @param val list to write
 */

public final void writeDoubleVector(java.util.ArrayList<java.lang.Double> val) { throw new RuntimeException("Stub!"); }

/**
 * Helper method to write a list of Strings to the end of the parcel.
 * @param val list to write
 */

public final void writeStringVector(java.util.ArrayList<java.lang.String> val) { throw new RuntimeException("Stub!"); }

/**
 * Helper method to write a list of native handles to the end of the parcel.
 * @param val list of {@link NativeHandle} objects to write

 * This value must never be {@code null}.
 */

public final void writeNativeHandleVector(java.util.ArrayList<android.os.NativeHandle> val) { throw new RuntimeException("Stub!"); }

/**
 * Write a hwbinder object to the end of the parcel.
 * @param binder value to write
 */

public final native void writeStrongBinder(android.os.IHwBinder binder);

/**
 * Checks to make sure that the interface name matches the name written by the parcel
 * sender by writeInterfaceToken
 *
 * @throws SecurityException interface doesn't match
 */

public final native void enforceInterface(java.lang.String interfaceName);

/**
 * Reads a boolean value from the current location in the parcel.
 * @return value parsed from the parcel
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native boolean readBool();

/**
 * Reads a byte value from the current location in the parcel.
 * @return value parsed from the parcel
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native byte readInt8();

/**
 * Reads a short value from the current location in the parcel.
 * @return value parsed from the parcel
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native short readInt16();

/**
 * Reads a int value from the current location in the parcel.
 * @return value parsed from the parcel
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native int readInt32();

/**
 * Reads a long value from the current location in the parcel.
 * @return value parsed from the parcel
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native long readInt64();

/**
 * Reads a float value from the current location in the parcel.
 * @return value parsed from the parcel
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native float readFloat();

/**
 * Reads a double value from the current location in the parcel.
 * @return value parsed from the parcel
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native double readDouble();

/**
 * Reads a String value from the current location in the parcel.
 * @return value parsed from the parcel
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native java.lang.String readString();

/**
 * Reads a native handle (without duplicating the underlying file
 * descriptors) from the parcel. These file descriptors will only
 * be open for the duration that the binder window is open. If they
 * are needed further, you must call {@link NativeHandle#dup()}.
 *
 * @return a {@link NativeHandle} instance parsed from the parcel
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native android.os.NativeHandle readNativeHandle();

/**
 * Reads an embedded native handle (without duplicating the underlying
 * file descriptors) from the parcel. These file descriptors will only
 * be open for the duration that the binder window is open. If they
 * are needed further, you must call {@link NativeHandle#dup()}. You
 * do not need to call close on the NativeHandle returned from this.
 *
 * @param parentHandle handle from which to read the embedded object
 * @param offset offset into parent
 * @return a {@link NativeHandle} instance parsed from the parcel
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native android.os.NativeHandle readEmbeddedNativeHandle(long parentHandle, long offset);

/**
 * Convenience method to read a Boolean vector as an ArrayList.
 * @return array of parsed values.
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final java.util.ArrayList<java.lang.Boolean> readBoolVector() { throw new RuntimeException("Stub!"); }

/**
 * Convenience method to read a Byte vector as an ArrayList.
 * @return array of parsed values.
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final java.util.ArrayList<java.lang.Byte> readInt8Vector() { throw new RuntimeException("Stub!"); }

/**
 * Convenience method to read a Short vector as an ArrayList.
 * @return array of parsed values.
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final java.util.ArrayList<java.lang.Short> readInt16Vector() { throw new RuntimeException("Stub!"); }

/**
 * Convenience method to read a Integer vector as an ArrayList.
 * @return array of parsed values.
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final java.util.ArrayList<java.lang.Integer> readInt32Vector() { throw new RuntimeException("Stub!"); }

/**
 * Convenience method to read a Long vector as an ArrayList.
 * @return array of parsed values.
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final java.util.ArrayList<java.lang.Long> readInt64Vector() { throw new RuntimeException("Stub!"); }

/**
 * Convenience method to read a Float vector as an ArrayList.
 * @return array of parsed values.
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final java.util.ArrayList<java.lang.Float> readFloatVector() { throw new RuntimeException("Stub!"); }

/**
 * Convenience method to read a Double vector as an ArrayList.
 * @return array of parsed values.
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final java.util.ArrayList<java.lang.Double> readDoubleVector() { throw new RuntimeException("Stub!"); }

/**
 * Convenience method to read a String vector as an ArrayList.
 * @return array of parsed values.
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final java.util.ArrayList<java.lang.String> readStringVector() { throw new RuntimeException("Stub!"); }

/**
 * Convenience method to read a vector of native handles as an ArrayList.
 * @return array of {@link NativeHandle} objects.
 * This value will never be {@code null}.
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final java.util.ArrayList<android.os.NativeHandle> readNativeHandleVector() { throw new RuntimeException("Stub!"); }

/**
 * Reads a strong binder value from the parcel.
 * @return binder object read from parcel or null if no binder can be read
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native android.os.IHwBinder readStrongBinder();

/**
 * Read opaque segment of data as a blob.
 * @return blob of size expectedSize
 * @throws IllegalArgumentException if the parcel has no more data
 */

public final native android.os.HwBlob readBuffer(long expectedSize);

/**
 * Read a buffer written using scatter gather.
 *
 * @param expectedSize size that buffer should be
 * @param parentHandle handle from which to read the embedded buffer
 * @param offset offset into parent
 * @param nullable whether or not to allow for a null return
 * @return blob of data with size expectedSize
 * @throws NoSuchElementException if an embedded buffer is not available to read
 * @throws IllegalArgumentException if expectedSize < 0
 * @throws NullPointerException if the transaction specified the blob to be null
 *    but nullable is false
 */

public final native android.os.HwBlob readEmbeddedBuffer(long expectedSize, long parentHandle, long offset, boolean nullable);

/**
 * Write a buffer into the transaction.
 * @param blob blob to write into the parcel.
 */

public final native void writeBuffer(android.os.HwBlob blob);

/**
 * Write a status value into the blob.
 * @param status value to write
 */

public final native void writeStatus(int status);

/**
 * @throws IllegalArgumentException if a success vaue cannot be read
 * @throws RemoteException if success value indicates a transaction error
 */

public final native void verifySuccess();

/**
 * Should be called to reduce memory pressure when this object no longer needs
 * to be written to.
 */

public final native void releaseTemporaryStorage();

/**
 * Should be called when object is no longer needed to reduce possible memory
 * pressure if the Java GC does not get to this object in time.
 */

public final native void release();

/**
 * Sends the parcel to the specified destination.
 */

public final native void send();

/**
 * Success return error for a transaction. Written to parcels
 * using writeStatus.
 */

public static final int STATUS_SUCCESS = 0; // 0x0
/**
 * Value is {@link android.os.HwParcel#STATUS_SUCCESS}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface Status {
}

}

