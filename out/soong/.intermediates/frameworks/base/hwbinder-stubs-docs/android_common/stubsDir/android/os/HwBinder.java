/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;

import java.util.NoSuchElementException;

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class HwBinder implements android.os.IHwBinder {

/**
 * Create and initialize a HwBinder object and the native objects
 * used to allow this to participate in hwbinder transactions.
 */

public HwBinder() { throw new RuntimeException("Stub!"); }

public final native void transact(int code, android.os.HwParcel request, android.os.HwParcel reply, int flags) throws android.os.RemoteException;

/**
 * Process a hwbinder transaction.
 *
 * @param code interface specific code for interface.
 * @param request parceled transaction
 * @param reply object to parcel reply into
 * @param flags transaction flags to be chosen by wire protocol
 */

public abstract void onTransact(int code, android.os.HwParcel request, android.os.HwParcel reply, int flags) throws android.os.RemoteException;

/**
 * Registers this service with the hwservicemanager.
 *
 * @param serviceName instance name of the service
 */

public final native void registerService(java.lang.String serviceName) throws android.os.RemoteException;

/**
 * Returns the specified service from the hwservicemanager. Does not retry.
 *
 * @param iface fully-qualified interface name for example foo.bar@1.3::IBaz
 * @param serviceName the instance name of the service for example default.
 * @throws NoSuchElementException when the service is unavailable
 */

public static final android.os.IHwBinder getService(java.lang.String iface, java.lang.String serviceName) throws java.util.NoSuchElementException, android.os.RemoteException { throw new RuntimeException("Stub!"); }

/**
 * Returns the specified service from the hwservicemanager.
 * @param iface fully-qualified interface name for example foo.bar@1.3::IBaz
 * @param serviceName the instance name of the service for example default.
 * @param retry whether to wait for the service to start if it's not already started
 * @throws NoSuchElementException when the service is unavailable
 */

public static final native android.os.IHwBinder getService(java.lang.String iface, java.lang.String serviceName, boolean retry) throws java.util.NoSuchElementException, android.os.RemoteException;

/**
 * Configures how many threads the process-wide hwbinder threadpool
 * has to process incoming requests.
 *
 * @param maxThreads total number of threads to create (includes this thread if
 *     callerWillJoin is true)
 * @param callerWillJoin whether joinRpcThreadpool will be called in advance
 */

public static final native void configureRpcThreadpool(long maxThreads, boolean callerWillJoin);

/**
 * Current thread will join hwbinder threadpool and process
 * commands in the pool. Should be called after configuring
 * a threadpool with callerWillJoin true and then registering
 * the provided service if this thread doesn't need to do
 * anything else.
 */

public static final native void joinRpcThreadpool();

/**
 * Enable instrumentation if available.
 *
 * On a non-user build, this method:
 * - tries to enable atracing (if enabled)
 * - tries to enable coverage dumps (if running in VTS)
 * - tries to enable record and replay (if running in VTS)
 */

public static void enableInstrumentation() { throw new RuntimeException("Stub!"); }
}

