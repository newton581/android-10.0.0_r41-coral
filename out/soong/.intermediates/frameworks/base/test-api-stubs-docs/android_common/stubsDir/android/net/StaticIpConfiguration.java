/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net;

import android.os.Parcelable;

/**
 * Class that describes static IP configuration.
 *
 * <p>This class is different from {@link LinkProperties} because it represents
 * configuration intent. The general contract is that if we can represent
 * a configuration here, then we should be able to configure it on a network.
 * The intent is that it closely match the UI we have for configuring networks.
 *
 * <p>In contrast, {@link LinkProperties} represents current state. It is much more
 * expressive. For example, it supports multiple IP addresses, multiple routes,
 * stacked interfaces, and so on. Because LinkProperties is so expressive,
 * using it to represent configuration intent as well as current state causes
 * problems. For example, we could unknowingly save a configuration that we are
 * not in fact capable of applying, or we could save a configuration that the
 * UI cannot display, which has the potential for malicious code to hide
 * hostile or unexpected configuration from the user.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class StaticIpConfiguration implements android.os.Parcelable {

/** @apiSince REL */

public StaticIpConfiguration() { throw new RuntimeException("Stub!"); }

/**
 * @param source This value may be {@code null}.
 * @apiSince REL
 */

public StaticIpConfiguration(@android.annotation.Nullable android.net.StaticIpConfiguration source) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void clear() { throw new RuntimeException("Stub!"); }

/**
 * Get the static IP address included in the configuration.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.net.LinkAddress getIpAddress() { throw new RuntimeException("Stub!"); }

/**
 * Get the gateway included in the configuration.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.net.InetAddress getGateway() { throw new RuntimeException("Stub!"); }

/**
 * Get the DNS servers included in the configuration.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<java.net.InetAddress> getDnsServers() { throw new RuntimeException("Stub!"); }

/**
 * Get a {@link String} listing in priority order of the comma separated domains to search when
 * resolving host names on the link.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getDomains() { throw new RuntimeException("Stub!"); }

/**
 * Add a DNS server to this configuration.
 
 * @param server This value must never be {@code null}.
 * @apiSince REL
 */

public void addDnsServer(@android.annotation.NonNull java.net.InetAddress server) { throw new RuntimeException("Stub!"); }

/**
 * Returns the network routes specified by this object. Will typically include a
 * directly-connected route for the IP address's local subnet and a default route.
 * @param iface Interface to include in the routes.
 
 * This value may be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.net.RouteInfo> getRoutes(@android.annotation.Nullable java.lang.String iface) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * Implement the Parcelable interface
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Implement the Parcelable interface
 * @apiSince REL
 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Implement the Parcelable interface
 * @apiSince REL
 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.net.StaticIpConfiguration> CREATOR;
static { CREATOR = null; }
/**
 * Helper class to build a new instance of {@link StaticIpConfiguration}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Set the IP address to be included in the configuration; null by default.
 * @param ipAddress This value may be {@code null}.
 * @return The {@link Builder} for chaining.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.StaticIpConfiguration.Builder setIpAddress(@android.annotation.Nullable android.net.LinkAddress ipAddress) { throw new RuntimeException("Stub!"); }

/**
 * Set the address of the gateway to be included in the configuration; null by default.
 * @param gateway This value may be {@code null}.
 * @return The {@link Builder} for chaining.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.StaticIpConfiguration.Builder setGateway(@android.annotation.Nullable java.net.InetAddress gateway) { throw new RuntimeException("Stub!"); }

/**
 * Set the addresses of the DNS servers included in the configuration; empty by default.
 * @param dnsServers This value must never be {@code null}.
 * @return The {@link Builder} for chaining.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.StaticIpConfiguration.Builder setDnsServers(@android.annotation.NonNull java.lang.Iterable<java.net.InetAddress> dnsServers) { throw new RuntimeException("Stub!"); }

/**
 * Sets the DNS domain search path to be used on the link; null by default.
 * @param newDomains A {@link String} containing the comma separated domains to search when
 *                   resolving host names on this link, in priority order.
 * This value may be {@code null}.
 * @return The {@link Builder} for chaining.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.StaticIpConfiguration.Builder setDomains(@android.annotation.Nullable java.lang.String newDomains) { throw new RuntimeException("Stub!"); }

/**
 * Create a {@link StaticIpConfiguration} from the parameters in this {@link Builder}.
 * @return The newly created StaticIpConfiguration.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.StaticIpConfiguration build() { throw new RuntimeException("Stub!"); }
}

}

