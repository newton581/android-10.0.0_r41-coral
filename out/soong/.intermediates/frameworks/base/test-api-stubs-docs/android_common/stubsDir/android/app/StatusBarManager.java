/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app;


/**
 * Allows an app to control the status bar.
 * @apiSince 29
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class StatusBarManager {

StatusBarManager(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Enable or disable status bar elements (notifications, clock) which are inappropriate during
 * device setup.
 *
 * <br>
 * Requires {@link android.Manifest.permission#STATUS_BAR}
 * @param disabled whether to apply or remove the disabled flags
 *
 * @hide
 */

public void setDisabledForSetup(boolean disabled) { throw new RuntimeException("Stub!"); }

/**
 * Get this app's currently requested disabled components
 *
 * <br>
 * Requires {@link android.Manifest.permission#STATUS_BAR}
 * @return a new DisableInfo
 *
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public android.app.StatusBarManager.DisableInfo getDisableInfo() { throw new RuntimeException("Stub!"); }
/**
 * DisableInfo describes this app's requested state of the StatusBar with regards to which
 * components are enabled/disabled
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class DisableInfo {

/** @hide */

DisableInfo() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if expanding the notification shade is disabled
 *
 * @hide
 */

public boolean isStatusBarExpansionDisabled() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if navigation home is disabled
 *
 * @hide
 */

public boolean isNavigateToHomeDisabled() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if notification peeking (heads-up notification) is disabled
 *
 * @hide
 */

public boolean isNotificationPeekingDisabled() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if mRecents/overview is disabled
 *
 * @hide
 */

public boolean isRecentsDisabled() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if mSearch is disabled
 *
 * @hide
 */

public boolean isSearchDisabled() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if no components are disabled (default state)
 *
 * @hide
 */

public boolean areAllComponentsEnabled() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

}

