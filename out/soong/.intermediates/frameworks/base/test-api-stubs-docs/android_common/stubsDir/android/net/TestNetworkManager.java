/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.net;


/**
 * Class that allows creation and management of per-app, test-only networks
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class TestNetworkManager {

TestNetworkManager() { throw new RuntimeException("Stub!"); }

/**
 * Teardown the capability-limited, testing-only network for a given interface
 *
 * @param network The test network that should be torn down
 * This value must never be {@code null}.
 * @hide
 */

public void teardownTestNetwork(@android.annotation.NonNull android.net.Network network) { throw new RuntimeException("Stub!"); }

/**
 * Sets up a capability-limited, testing-only network for a given interface
 *
 * @param iface the name of the interface to be used for the Network LinkProperties.
 * This value must never be {@code null}.
 * @param binder A binder object guarding the lifecycle of this test network.
 * This value must never be {@code null}.
 * @hide
 */

public void setupTestNetwork(@android.annotation.NonNull java.lang.String iface, @android.annotation.NonNull android.os.IBinder binder) { throw new RuntimeException("Stub!"); }

/**
 * Create a tun interface for testing purposes
 *
 * @param linkAddrs an array of LinkAddresses to assign to the TUN interface
 * This value must never be {@code null}.
 * @return A ParcelFileDescriptor of the underlying TUN interface. Close this to tear down the
 *     TUN interface.
 * @hide
 */

public android.net.TestNetworkInterface createTunInterface(@android.annotation.NonNull android.net.LinkAddress[] linkAddrs) { throw new RuntimeException("Stub!"); }

/**
 * Create a tap interface for testing purposes
 *
 * @return A ParcelFileDescriptor of the underlying TAP interface. Close this to tear down the
 *     TAP interface.
 * @hide
 */

public android.net.TestNetworkInterface createTapInterface() { throw new RuntimeException("Stub!"); }
}

