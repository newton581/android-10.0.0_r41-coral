/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;

import android.net.Uri;

/**
 * Class to take an incident report.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class IncidentManager {

/**
 * @hide
 */

IncidentManager(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Take an incident report.
 
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @apiSince REL
 */

public void reportIncident(android.os.IncidentReportArgs args) { throw new RuntimeException("Stub!"); }

/**
 * Request authorization of an incident report.
 
 * <br>
 * Requires android.Manifest.permission.REQUEST_INCIDENT_REPORT_APPROVAL
 * @apiSince REL
 */

public void requestAuthorization(int callingUid, java.lang.String callingPackage, int flags, android.os.IncidentManager.AuthListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Cancel a previous request for incident report authorization.
 
 * <br>
 * Requires android.Manifest.permission.REQUEST_INCIDENT_REPORT_APPROVAL
 * @apiSince REL
 */

public void cancelAuthorization(android.os.IncidentManager.AuthListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Get incident (and bug) reports that are pending approval to share.
 
 * <br>
 * Requires {@link android.Manifest.permission#APPROVE_INCIDENT_REPORTS}
 * @apiSince REL
 */

public java.util.List<android.os.IncidentManager.PendingReport> getPendingReports() { throw new RuntimeException("Stub!"); }

/**
 * Allow this report to be shared with the given app.
 
 * <br>
 * Requires {@link android.Manifest.permission#APPROVE_INCIDENT_REPORTS}
 * @apiSince REL
 */

public void approveReport(android.net.Uri uri) { throw new RuntimeException("Stub!"); }

/**
 * Do not allow this report to be shared with the given app.
 
 * <br>
 * Requires {@link android.Manifest.permission#APPROVE_INCIDENT_REPORTS}
 * @apiSince REL
 */

public void denyReport(android.net.Uri uri) { throw new RuntimeException("Stub!"); }

/**
 * Get the incident reports that are available for upload for the supplied
 * broadcast recevier.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param receiverClass Class name of broadcast receiver in this package that
 *   was registered to retrieve reports.
 *
 * @return A list of {@link Uri Uris} that are awaiting upload.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.net.Uri> getIncidentReportList(java.lang.String receiverClass) { throw new RuntimeException("Stub!"); }

/**
 * Get the incident report with the given URI id.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param uri Identifier of the incident report.
 *
 * @return an IncidentReport object, or null if the incident report has been
 *  expired from disk.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.os.IncidentManager.IncidentReport getIncidentReport(android.net.Uri uri) { throw new RuntimeException("Stub!"); }

/**
 * Delete the incident report with the given URI id.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param uri Identifier of the incident report. Pass null to delete all
 *              incident reports owned by this application.
 * @apiSince REL
 */

public void deleteIncidentReports(android.net.Uri uri) { throw new RuntimeException("Stub!"); }

/**
 * Do the confirmation with a dialog instead of the default, which is a notification.
 * It is possible for the dialog to be downgraded to a notification in some cases.
 * @apiSince REL
 */

public static final int FLAG_CONFIRMATION_DIALOG = 1; // 0x1

/**
 * Flag marking fields and incident reports than can be taken
 * off the device with prior consent.
 * @apiSince REL
 */

public static final int PRIVACY_POLICY_AUTO = 200; // 0xc8

/**
 * Flag marking fields and incident reports than can be taken
 * off the device with contemporary consent.
 * @apiSince REL
 */

public static final int PRIVACY_POLICY_EXPLICIT = 100; // 0x64

/**
 * Flag marking fields and incident reports than can be taken
 * off the device only via adb.
 * @apiSince REL
 */

public static final int PRIVACY_POLICY_LOCAL = 0; // 0x0
/**
 * Listener for the status of an incident report being authorized or denied.
 *
 * @see #requestAuthorization
 * @see #cancelAuthorization
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class AuthListener {

public AuthListener() { throw new RuntimeException("Stub!"); }

/**
 * Called when a report is approved.
 * @apiSince REL
 */

public void onReportApproved() { throw new RuntimeException("Stub!"); }

/**
 * Called when a report is denied.
 * @apiSince REL
 */

public void onReportDenied() { throw new RuntimeException("Stub!"); }
}

/**
 * Record of an incident report that has previously been taken.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class IncidentReport implements android.os.Parcelable, java.io.Closeable {

/** @apiSince REL */

public IncidentReport(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Close the input stream associated with this entry.
 * @apiSince REL
 */

public void close() { throw new RuntimeException("Stub!"); }

/**
 * Get the time at which this incident report was taken, in wall clock time
 * ({@link System#currenttimeMillis System.currenttimeMillis()} time base).
 * @apiSince REL
 */

public long getTimestamp() { throw new RuntimeException("Stub!"); }

/**
 * Get the privacy level to which this report has been filtered.
 *
 * @see #PRIVACY_POLICY_AUTO
 * @see #PRIVACY_POLICY_EXPLICIT
 * @see #PRIVACY_POLICY_LOCAL
 * @apiSince REL
 */

public long getPrivacyPolicy() { throw new RuntimeException("Stub!"); }

/**
 * Get the contents of this incident report.
 * @apiSince REL
 */

public java.io.InputStream getInputStream() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * @inheritDoc
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * @inheritDoc
 * @apiSince REL
 */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/**
 * {@link Parcelable.Creator Creator} for {@link IncidentReport}.
 * @apiSince REL
 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.IncidentManager.IncidentReport> CREATOR;
static { CREATOR = null; }
}

/**
 * Record for a report that has been taken and is pending user authorization
 * to share it.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class PendingReport {

/**
 * Constructor.
 
 * @param uri This value must never be {@code null}.
 * @apiSince REL
 */

public PendingReport(@android.annotation.NonNull android.net.Uri uri) { throw new RuntimeException("Stub!"); }

/**
 * Get the package with which this report will be shared.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getRequestingPackage() { throw new RuntimeException("Stub!"); }

/**
 * Get the flags requested for this pending report.
 *
 * @see #FLAG_CONFIRMATION_DIALOG
 * @apiSince REL
 */

public int getFlags() { throw new RuntimeException("Stub!"); }

/**
 * Get the time this pending report was posted.
 * @apiSince REL
 */

public long getTimestamp() { throw new RuntimeException("Stub!"); }

/**
 * Get the URI associated with this PendingReport.  It can be used to
 * re-retrieve it from {@link IncidentManager} or set as the data field of
 * an Intent.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.Uri getUri() { throw new RuntimeException("Stub!"); }

/**
 * String representation of this PendingReport.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * @inheritDoc
 * @apiSince REL
 */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }
}

}

