/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.view.contentcapture;

import android.content.Context;
import android.os.Bundle;
import android.app.TaskInfo;
import android.view.Display;
import android.view.View;

/**
 * Context associated with a {@link ContentCaptureSession} - see {@link ContentCaptureManager} for
 * more info.
 * @apiSince 29
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ContentCaptureContext implements android.os.Parcelable {

ContentCaptureContext(@android.annotation.NonNull android.view.contentcapture.ContentCaptureContext.Builder builder) { throw new RuntimeException("Stub!"); }

/**
 * Gets the (optional) extras set by the app (through {@link Builder#setExtras(Bundle)}).
 *
 * <p>It can be used to provide vendor-specific data that can be modified and examined.
 
 * @return This value may be {@code null}.
 * @apiSince 29
 */

@android.annotation.Nullable
public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/**
 * Gets the context id.
 
 * @return This value may be {@code null}.
 * @apiSince 29
 */

@android.annotation.Nullable
public android.content.LocusId getLocusId() { throw new RuntimeException("Stub!"); }

/**
 * Gets the id of the {@link TaskInfo task} associated with this context.
 *
 * @hide
 */

public int getTaskId() { throw new RuntimeException("Stub!"); }

/**
 * Gets the activity associated with this context, or {@code null} when it is a child session.
 *
 * @hide
 */

@android.annotation.Nullable
public android.content.ComponentName getActivityComponent() { throw new RuntimeException("Stub!"); }

/**
 * Gets the id of the session that originated this session (through
 * {@link ContentCaptureSession#createContentCaptureSession(ContentCaptureContext)}),
 * or {@code null} if this is the main session associated with the Activity's {@link Context}.
 *
 * @hide
 */

@android.annotation.Nullable
public android.view.contentcapture.ContentCaptureSessionId getParentSessionId() { throw new RuntimeException("Stub!"); }

/**
 * Gets the ID of the display associated with this context, as defined by
 * {G android.hardware.display.DisplayManager#getDisplay(int) DisplayManager.getDisplay()}.
 *
 * @hide
 */

public int getDisplayId() { throw new RuntimeException("Stub!"); }

/**
 * Gets the flags associated with this context.
 *
 * @return any combination of {@link #FLAG_DISABLED_BY_FLAG_SECURE},
 * {@link #FLAG_DISABLED_BY_APP} and {@link #FLAG_RECONNECTED}.
 *
 * Value is either <code>0</code> or a combination of {@link android.view.contentcapture.ContentCaptureContext#FLAG_DISABLED_BY_APP}, {@link android.view.contentcapture.ContentCaptureContext#FLAG_DISABLED_BY_FLAG_SECURE}, and {@link android.view.contentcapture.ContentCaptureContext#FLAG_RECONNECTED}
 * @hide
 */

public int getFlags() { throw new RuntimeException("Stub!"); }

/**
 * Helper that creates a {@link ContentCaptureContext} associated with the given {@code id}.
 
 * @param id This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince 29
 */

@android.annotation.NonNull
public static android.view.contentcapture.ContentCaptureContext forLocusId(@android.annotation.NonNull java.lang.String id) { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.view.contentcapture.ContentCaptureContext> CREATOR;
static { CREATOR = null; }

/**
 * Flag used to indicate that the app explicitly disabled content capture for the activity
 * (using {@link ContentCaptureManager#setContentCaptureEnabled(boolean)}),
 * in which case the service will just receive activity-level events.
 *
 * @hide
 */

public static final int FLAG_DISABLED_BY_APP = 1; // 0x1

/**
 * Flag used to indicate that the activity's window is tagged with
 * {@link android.view.Display#FLAG_SECURE}, in which case the service will just receive
 * activity-level events.
 *
 * @hide
 */

public static final int FLAG_DISABLED_BY_FLAG_SECURE = 2; // 0x2

/**
 * Flag used when the event is sent because the Android System reconnected to the service (for
 * example, after its process died).
 *
 * @hide
 */

public static final int FLAG_RECONNECTED = 4; // 0x4
/**
 * Builder for {@link ContentCaptureContext} objects.
 * @apiSince 29
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Creates a new builder.
 *
 * <p>The context must have an id, which is usually one of the following:
 *
 * <ul>
 *   <li>A URL representing a web page (or {@code IFRAME}) that's being rendered by the
 *   activity (See {@link View#setContentCaptureSession(ContentCaptureSession)} for an
 *   example).
 *   <li>A unique identifier of the application state (for example, a conversation between
 *   2 users in a chat app).
 * </ul>
 *
 * <p>See {@link ContentCaptureManager} for more info about the content capture context.
 *
 * @param id id associated with this context.
 
 * This value must never be {@code null}.
 * @apiSince 29
 */

public Builder(@android.annotation.NonNull android.content.LocusId id) { throw new RuntimeException("Stub!"); }

/**
 * Sets extra options associated with this context.
 *
 * <p>It can be used to provide vendor-specific data that can be modified and examined.
 *
 * @param extras extra options.
 * This value must never be {@code null}.
 * @return this builder.
 *
 * This value will never be {@code null}.
 * @throws IllegalStateException if {@link #build()} was already called.
 * @apiSince 29
 */

@android.annotation.NonNull
public android.view.contentcapture.ContentCaptureContext.Builder setExtras(@android.annotation.NonNull android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Builds the {@link ContentCaptureContext}.
 *
 * @throws IllegalStateException if {@link #build()} was already called.
 *
 * @return the built {@code ContentCaptureContext}
 
 * This value will never be {@code null}.
 * @apiSince 29
 */

@android.annotation.NonNull
public android.view.contentcapture.ContentCaptureContext build() { throw new RuntimeException("Stub!"); }
}

}

