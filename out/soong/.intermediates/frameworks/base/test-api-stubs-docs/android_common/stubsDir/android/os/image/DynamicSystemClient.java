/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.os.image;

import android.net.Uri;
import android.content.Context;
import java.util.concurrent.Executor;
import android.os.Message;
import android.content.Intent;

/**
 * <p>This class contains methods and constants used to start a {@code DynamicSystem} installation,
 * and a listener for status updates.</p>
 *
 * <p>{@code DynamicSystem} allows users to run certified system images in a non destructive manner
 * without needing to prior OEM unlock. It creates a temporary system partition to install the new
 * system image, and a temporary data partition for the newly installed system to run with.</p>
 *
 * After the installation is completed, the device will be running in the new system on next the
 * reboot. Then, when the user reboots the device again, it will leave {@code DynamicSystem} and go
 * back to the original system. While running in {@code DynamicSystem}, persitent storage for
 * factory reset protection (FRP) remains unchanged. Since the user is running the new system with
 * a temporarily created data partition, their original user data are kept unchanged.</p>
 *
 * <p>With {@link #setOnStatusChangedListener}, API users can register an
 * {@link #OnStatusChangedListener} to get status updates and their causes when the installation is
 * started, stopped, or cancelled. It also sends progress updates during the installation. With
 * {@link #start}, API users can start an installation with the {@link Uri} to a unsparsed and
 * gzipped system image. The {@link Uri} can be a web URL or a content Uri to a local path.</p>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DynamicSystemClient {

/**
 * Create a new {@code DynamicSystem} client.
 *
 * @param context a {@link Context} will be used to bind the installation service.
 *
 * This value must never be {@code null}.
 * @hide
 */

public DynamicSystemClient(@android.annotation.NonNull android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * This method register a listener for status change. The listener is called using
 * the executor.
 
 * @param executor This value must never be {@code null}.
 
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 
 * @param listener This value must never be {@code null}.
 * @apiSince REL
 */

public void setOnStatusChangedListener(@android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.os.image.DynamicSystemClient.OnStatusChangedListener listener) { throw new RuntimeException("Stub!"); }

/**
 * This method register a listener for status change. The listener is called in main
 * thread.
 
 * @param listener This value must never be {@code null}.
 * @apiSince REL
 */

public void setOnStatusChangedListener(@android.annotation.NonNull android.os.image.DynamicSystemClient.OnStatusChangedListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Bind to {@code DynamicSystem} installation service. Binding to the installation service
 * allows it to send status updates to {@link #OnStatusChangedListener}. It is recommanded
 * to bind before calling {@link #start} and get status updates.
 * <br>
 * Requires android.Manifest.permission.INSTALL_DYNAMIC_SYSTEM
 * @hide
 */

public void bind() { throw new RuntimeException("Stub!"); }

/**
 * Unbind from {@code DynamicSystem} installation service. Unbinding from the installation
 * service stops it from sending following status updates.
 * <br>
 * Requires android.Manifest.permission.INSTALL_DYNAMIC_SYSTEM
 * @hide
 */

public void unbind() { throw new RuntimeException("Stub!"); }

/**
 * Start installing {@code DynamicSystem} from URL with default userdata size.
 *
 * Calling this function will first start an Activity to confirm device credential, using
 * {@link KeyguardManager}. If it's confirmed, the installation service will be started.
 *
 * This function doesn't require prior calling {@link #bind}.
 *
 * <br>
 * Requires android.Manifest.permission.INSTALL_DYNAMIC_SYSTEM
 * @param systemUrl a network Uri, a file Uri or a content Uri pointing to a system image file.
 * This value must never be {@code null}.
 * @param systemSize size of system image.
 * Value is a non-negative number of bytes.
 * @hide
 */

public void start(@android.annotation.NonNull android.net.Uri systemUrl, long systemSize) { throw new RuntimeException("Stub!"); }

/**
 * Start installing {@code DynamicSystem} from URL.
 *
 * Calling this function will first start an Activity to confirm device credential, using
 * {@link KeyguardManager}. If it's confirmed, the installation service will be started.
 *
 * This function doesn't require prior calling {@link #bind}.
 *
 * <br>
 * Requires android.Manifest.permission.INSTALL_DYNAMIC_SYSTEM
 * @param systemUrl a network Uri, a file Uri or a content Uri pointing to a system image file.
 * This value must never be {@code null}.
 * @param systemSize size of system image.
 * Value is a non-negative number of bytes.
 * @param userdataSize bytes reserved for userdata.
 
 * Value is a non-negative number of bytes.
 * @apiSince REL
 */

public void start(@android.annotation.NonNull android.net.Uri systemUrl, long systemSize, long userdataSize) { throw new RuntimeException("Stub!"); }

/**
 * Installation failed due to unhandled exception.
 * @apiSince REL
 */

public static final int CAUSE_ERROR_EXCEPTION = 6; // 0x6

/**
 * Installation failed because the image URL source is not supported.
 * @apiSince REL
 */

public static final int CAUSE_ERROR_INVALID_URL = 4; // 0x4

/**
 * Installation failed due to {@code IOException}.
 * @apiSince REL
 */

public static final int CAUSE_ERROR_IO = 3; // 0x3

/**
 * Installation failed due to IPC error.
 * @apiSince REL
 */

public static final int CAUSE_ERROR_IPC = 5; // 0x5

/**
 * Status changed because installation is cancelled.
 * @apiSince REL
 */

public static final int CAUSE_INSTALL_CANCELLED = 2; // 0x2

/**
 * Status changed because installation is completed.
 * @apiSince REL
 */

public static final int CAUSE_INSTALL_COMPLETED = 1; // 0x1

/**
 * Cause is not specified. This means the status is not changed.
 * @apiSince REL
 */

public static final int CAUSE_NOT_SPECIFIED = 0; // 0x0

/**
 * Installation is in progress.
 * @apiSince REL
 */

public static final int STATUS_IN_PROGRESS = 2; // 0x2

/**
 * Device is running in {@code DynamicSystem}.
 * @apiSince REL
 */

public static final int STATUS_IN_USE = 4; // 0x4

/**
 * Installation is not started yet.
 * @apiSince REL
 */

public static final int STATUS_NOT_STARTED = 1; // 0x1

/**
 * Installation is finished but the user has not launched it.
 * @apiSince REL
 */

public static final int STATUS_READY = 3; // 0x3

/**
 * We are bound to installation service, but failed to get its status
 * @apiSince REL
 */

public static final int STATUS_UNKNOWN = 0; // 0x0
/**
 * Listener for installation status updates.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnStatusChangedListener {

/**
 * This callback is called when installation status is changed, and when the
 * client is {@link #bind} to {@code DynamicSystem} installation service.
 *
 * @param status status code, also defined in {@code DynamicSystemClient}.
 * Value is {@link android.os.image.DynamicSystemClient#STATUS_UNKNOWN}, {@link android.os.image.DynamicSystemClient#STATUS_NOT_STARTED}, {@link android.os.image.DynamicSystemClient#STATUS_IN_PROGRESS}, {@link android.os.image.DynamicSystemClient#STATUS_READY}, or {@link android.os.image.DynamicSystemClient#STATUS_IN_USE}
 * @param cause cause code, also defined in {@code DynamicSystemClient}.
 * Value is {@link android.os.image.DynamicSystemClient#CAUSE_NOT_SPECIFIED}, {@link android.os.image.DynamicSystemClient#CAUSE_INSTALL_COMPLETED}, {@link android.os.image.DynamicSystemClient#CAUSE_INSTALL_CANCELLED}, {@link android.os.image.DynamicSystemClient#CAUSE_ERROR_IO}, {@link android.os.image.DynamicSystemClient#CAUSE_ERROR_INVALID_URL}, {@link android.os.image.DynamicSystemClient#CAUSE_ERROR_IPC}, or {@link android.os.image.DynamicSystemClient#CAUSE_ERROR_EXCEPTION}
 * @param progress number of bytes installed.
 * Value is a non-negative number of bytes.
 * @param detail additional detail about the error if available, otherwise null.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void onStatusChanged(int status, int cause, long progress, @android.annotation.Nullable java.lang.Throwable detail);
}

}

