/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.view.inputmethod;

import android.os.Build;
import android.content.ComponentName;

/**
 * Various (pseudo) constants about IME behaviors.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class InputMethodSystemProperty {

public InputMethodSystemProperty() { throw new RuntimeException("Stub!"); }

/**
 * {@code true} when multi-client IME is enabled.
 *
 * <p>TODO: Move this back to MultiClientInputMethodManagerService once
 * {@link #PER_PROFILE_IME_ENABLED} always becomes {@code true}.</p>
 *
 * @hide
 */

public static final boolean MULTI_CLIENT_IME_ENABLED;
static { MULTI_CLIENT_IME_ENABLED = false; }
}

