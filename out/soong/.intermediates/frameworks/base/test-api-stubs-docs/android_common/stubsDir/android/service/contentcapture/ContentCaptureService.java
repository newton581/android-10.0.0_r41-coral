/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.contentcapture;

import android.content.ComponentName;
import android.view.contentcapture.ContentCaptureManager;
import android.view.contentcapture.ContentCaptureCondition;
import android.view.contentcapture.ContentCaptureEvent;
import android.content.Intent;
import android.os.Binder;

/**
 * A service used to capture the content of the screen to provide contextual data in other areas of
 * the system such as Autofill.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ContentCaptureService extends android.app.Service {

public ContentCaptureService() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * If you override this method you <em>must</em> call through to the
 * superclass implementation.
 * @apiSince REL
 */

public void onCreate() { throw new RuntimeException("Stub!"); }

/** @hide */

public final android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Explicitly limits content capture to the given packages and activities.
 *
 * <p>To reset the whitelist, call it passing {@code null} to both arguments.
 *
 * <p>Useful when the service wants to restrict content capture to a category of apps, like
 * chat apps. For example, if the service wants to support view captures on all activities of
 * app {@code ChatApp1} and just activities {@code act1} and {@code act2} of {@code ChatApp2},
 * it would call: {@code setContentCaptureWhitelist(Sets.newArraySet("ChatApp1"),
 * Sets.newArraySet(new ComponentName("ChatApp2", "act1"),
 * new ComponentName("ChatApp2", "act2")));}
 
 * @param packages This value may be {@code null}.
 
 * @param activities This value may be {@code null}.
 * @apiSince REL
 */

public final void setContentCaptureWhitelist(@android.annotation.Nullable java.util.Set<java.lang.String> packages, @android.annotation.Nullable java.util.Set<android.content.ComponentName> activities) { throw new RuntimeException("Stub!"); }

/**
 * Explicitly sets the conditions for which content capture should be available by an app.
 *
 * <p>Typically used to restrict content capture to a few websites on browser apps. Example:
 *
 * <code>
 *   ArraySet<ContentCaptureCondition> conditions = new ArraySet<>(1);
 *   conditions.add(new ContentCaptureCondition(new LocusId("^https://.*\\.example\\.com$"),
 *       ContentCaptureCondition.FLAG_IS_REGEX));
 *   service.setContentCaptureConditions("com.example.browser_app", conditions);
 *
 * </code>
 *
 * <p>NOTE: </p> this method doesn't automatically disable content capture for the given
 * conditions; it's up to the {@code packageName} implementation to call
 * {@link ContentCaptureManager#getContentCaptureConditions()} and disable it accordingly.
 *
 * @param packageName name of the packages where the restrictions are set.
 * This value must never be {@code null}.
 * @param conditions list of conditions, or {@code null} to reset the conditions for the
 * package.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public final void setContentCaptureConditions(@android.annotation.NonNull java.lang.String packageName, @android.annotation.Nullable java.util.Set<android.view.contentcapture.ContentCaptureCondition> conditions) { throw new RuntimeException("Stub!"); }

/**
 * Called when the Android system connects to service.
 *
 * <p>You should generally do initialization here rather than in {@link #onCreate}.
 * @apiSince REL
 */

public void onConnected() { throw new RuntimeException("Stub!"); }

/**
 * Creates a new content capture session.
 *
 * @param context content capture context
 * This value must never be {@code null}.
 * @param sessionId the session's Id
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onCreateContentCaptureSession(@android.annotation.NonNull android.view.contentcapture.ContentCaptureContext context, @android.annotation.NonNull android.view.contentcapture.ContentCaptureSessionId sessionId) { throw new RuntimeException("Stub!"); }

/**
 * Notifies the service of {@link ContentCaptureEvent events} associated with a content capture
 * session.
 *
 * @param sessionId the session's Id
 * This value must never be {@code null}.
 * @param event the event
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onContentCaptureEvent(@android.annotation.NonNull android.view.contentcapture.ContentCaptureSessionId sessionId, @android.annotation.NonNull android.view.contentcapture.ContentCaptureEvent event) { throw new RuntimeException("Stub!"); }

/**
 * Notifies the service that the app requested to remove content capture data.
 *
 * @param request the content capture data requested to be removed
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onDataRemovalRequest(@android.annotation.NonNull android.view.contentcapture.DataRemovalRequest request) { throw new RuntimeException("Stub!"); }

/**
 * Notifies the service of {@link SnapshotData snapshot data} associated with a session.
 *
 * @param sessionId the session's Id
 * This value must never be {@code null}.
 * @param snapshotData the data
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onActivitySnapshot(@android.annotation.NonNull android.view.contentcapture.ContentCaptureSessionId sessionId, @android.annotation.NonNull android.service.contentcapture.SnapshotData snapshotData) { throw new RuntimeException("Stub!"); }

/**
 * Notifies the service of an activity-level event that is not associated with a session.
 *
 * <p>This method can be used to track some high-level events for all activities, even those
 * that are not whitelisted for Content Capture.
 *
 * @param event high-level activity event
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onActivityEvent(@android.annotation.NonNull android.service.contentcapture.ActivityEvent event) { throw new RuntimeException("Stub!"); }

/**
 * Destroys the content capture session.
 *
 * @param sessionId the id of the session to destroy
 * This value must never be {@code null}.
 * * @apiSince REL
 */

public void onDestroyContentCaptureSession(@android.annotation.NonNull android.view.contentcapture.ContentCaptureSessionId sessionId) { throw new RuntimeException("Stub!"); }

/**
 * Disables the Content Capture service for the given user.
 * @apiSince REL
 */

public final void disableSelf() { throw new RuntimeException("Stub!"); }

/**
 * Called when the Android system disconnects from the service.
 *
 * <p> At this point this service may no longer be an active {@link ContentCaptureService}.
 * It should not make calls on {@link ContentCaptureManager} that requires the caller to be
 * the current service.
 * @apiSince REL
 */

public void onDisconnected() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * If you override this method you <em>must</em> call through to the
 * superclass implementation.
 * @apiSince REL
 */

protected void dump(java.io.FileDescriptor fd, java.io.PrintWriter pw, java.lang.String[] args) { throw new RuntimeException("Stub!"); }

/**
 * The {@link Intent} that must be declared as handled by the service.
 *
 * <p>To be supported, the service must also require the
 * {@link android.Manifest.permission#BIND_CONTENT_CAPTURE_SERVICE} permission so
 * that other applications can not abuse it.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.service.contentcapture.ContentCaptureService";

/**
 * Name under which a ContentCaptureService component publishes information about itself.
 *
 * <p>This meta-data should reference an XML resource containing a
 * <code>&lt;{@link
 * android.R.styleable#ContentCaptureService content-capture-service}&gt;</code> tag.
 *
 * <p>Here's an example of how to use it on {@code AndroidManifest.xml}:
 *
 * <pre>
 * &lt;service android:name=".MyContentCaptureService"
 *     android:permission="android.permission.BIND_CONTENT_CAPTURE_SERVICE"&gt;
 *   &lt;intent-filter&gt;
 *     &lt;action android:name="android.service.contentcapture.ContentCaptureService" /&gt;
 *   &lt;/intent-filter&gt;
 *
 *   &lt;meta-data
 *       android:name="android.content_capture"
 *       android:resource="@xml/my_content_capture_service"/&gt;
 * &lt;/service&gt;
 * </pre>
 *
 * <p>And then on {@code res/xml/my_content_capture_service.xml}:
 *
 * <pre>
 *   &lt;content-capture-service xmlns:android="http://schemas.android.com/apk/res/android"
 *       android:settingsActivity="my.package.MySettingsActivity"&gt;
 *   &lt;/content-capture-service&gt;
 * </pre>
 * @apiSince REL
 */

public static final java.lang.String SERVICE_META_DATA = "android.content_capture";
}

