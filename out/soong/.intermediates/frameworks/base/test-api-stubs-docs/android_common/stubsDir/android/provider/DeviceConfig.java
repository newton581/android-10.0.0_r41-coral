/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.provider;

import android.content.Context;
import java.util.concurrent.Executor;
import android.database.ContentObserver;
import java.util.List;

/**
 * Device level configuration parameters which can be tuned by a separate configuration service.
 * Namespaces that end in "_native" such as {@link #NAMESPACE_NETD_NATIVE} are intended to be used
 * by native code and should be pushed to system properties to make them accessible.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class DeviceConfig {

DeviceConfig() { throw new RuntimeException("Stub!"); }

/**
 * Look up the value of a property for a particular namespace.
 *
 * <br>
 * Requires android.Manifest.permission.READ_DEVICE_CONFIG
 * @param namespace The namespace containing the property to look up.
 * This value must never be {@code null}.
 * @param name      The name of the property to look up.
 * This value must never be {@code null}.
 * @return the corresponding value, or null if not present.
 * @hide
 */

public static java.lang.String getProperty(@android.annotation.NonNull java.lang.String namespace, @android.annotation.NonNull java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Look up the String value of a property for a particular namespace.
 *
 * <br>
 * Requires android.Manifest.permission.READ_DEVICE_CONFIG
 * @param namespace    The namespace containing the property to look up.
 * This value must never be {@code null}.
 * @param name         The name of the property to look up.
 * This value must never be {@code null}.
 * @param defaultValue The value to return if the property does not exist or has no non-null
 *                     value.
 * This value may be {@code null}.
 * @return the corresponding value, or defaultValue if none exists.
 * @hide
 */

public static java.lang.String getString(@android.annotation.NonNull java.lang.String namespace, @android.annotation.NonNull java.lang.String name, @android.annotation.Nullable java.lang.String defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Look up the boolean value of a property for a particular namespace.
 *
 * <br>
 * Requires android.Manifest.permission.READ_DEVICE_CONFIG
 * @param namespace The namespace containing the property to look up.
 * This value must never be {@code null}.
 * @param name      The name of the property to look up.
 * This value must never be {@code null}.
 * @param defaultValue The value to return if the property does not exist or has no non-null
 *                     value.
 * @return the corresponding value, or defaultValue if none exists.
 * @hide
 */

public static boolean getBoolean(@android.annotation.NonNull java.lang.String namespace, @android.annotation.NonNull java.lang.String name, boolean defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Look up the int value of a property for a particular namespace.
 *
 * <br>
 * Requires android.Manifest.permission.READ_DEVICE_CONFIG
 * @param namespace The namespace containing the property to look up.
 * This value must never be {@code null}.
 * @param name      The name of the property to look up.
 * This value must never be {@code null}.
 * @param defaultValue The value to return if the property does not exist, has no non-null
 *                     value, or fails to parse into an int.
 * @return the corresponding value, or defaultValue if either none exists or it does not parse.
 * @hide
 */

public static int getInt(@android.annotation.NonNull java.lang.String namespace, @android.annotation.NonNull java.lang.String name, int defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Look up the long value of a property for a particular namespace.
 *
 * <br>
 * Requires android.Manifest.permission.READ_DEVICE_CONFIG
 * @param namespace The namespace containing the property to look up.
 * This value must never be {@code null}.
 * @param name      The name of the property to look up.
 * This value must never be {@code null}.
 * @param defaultValue The value to return if the property does not exist, has no non-null
 *                     value, or fails to parse into a long.
 * @return the corresponding value, or defaultValue if either none exists or it does not parse.
 * @hide
 */

public static long getLong(@android.annotation.NonNull java.lang.String namespace, @android.annotation.NonNull java.lang.String name, long defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Look up the float value of a property for a particular namespace.
 *
 * <br>
 * Requires android.Manifest.permission.READ_DEVICE_CONFIG
 * @param namespace The namespace containing the property to look up.
 * This value must never be {@code null}.
 * @param name      The name of the property to look up.
 * This value must never be {@code null}.
 * @param defaultValue The value to return if the property does not exist, has no non-null
 *                     value, or fails to parse into a float.
 * @return the corresponding value, or defaultValue if either none exists or it does not parse.
 * @hide
 */

public static float getFloat(@android.annotation.NonNull java.lang.String namespace, @android.annotation.NonNull java.lang.String name, float defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Create a new property with the the provided name and value in the provided namespace, or
 * update the value of such a property if it already exists. The same name can exist in multiple
 * namespaces and might have different values in any or all namespaces.
 * <p>
 * The method takes an argument indicating whether to make the value the default for this
 * property.
 * <p>
 * All properties stored for a particular scope can be reverted to their default values
 * by passing the namespace to {@link #resetToDefaults(int, String)}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#WRITE_DEVICE_CONFIG}
 * @param namespace   The namespace containing the property to create or update.
 * This value must never be {@code null}.
 * @param name        The name of the property to create or update.
 * This value must never be {@code null}.
 * @param value       The value to store for the property.
 * This value may be {@code null}.
 * @param makeDefault Whether to make the new value the default one.
 * @return True if the value was set, false if the storage implementation throws errors.
 * @hide
 * @see #resetToDefaults(int, String).
 */

public static boolean setProperty(@android.annotation.NonNull java.lang.String namespace, @android.annotation.NonNull java.lang.String name, @android.annotation.Nullable java.lang.String value, boolean makeDefault) { throw new RuntimeException("Stub!"); }

/**
 * Reset properties to their default values.
 * <p>
 * The method accepts an optional namespace parameter. If provided, only properties set within
 * that namespace will be reset. Otherwise, all properties will be reset.
 *
 * <br>
 * Requires {@link android.Manifest.permission#WRITE_DEVICE_CONFIG}
 * @param resetMode The reset mode to use.
 * Value is {@link android.provider.Settings#RESET_MODE_PACKAGE_DEFAULTS}, android.provider.Settings.RESET_MODE_UNTRUSTED_DEFAULTS, android.provider.Settings.RESET_MODE_UNTRUSTED_CHANGES, or android.provider.Settings.RESET_MODE_TRUSTED_DEFAULTS
 * @param namespace Optionally, the specific namespace which resets will be limited to.
 * This value may be {@code null}.
 * @hide
 * @see #setProperty(String, String, String, boolean)
 */

public static void resetToDefaults(int resetMode, @android.annotation.Nullable java.lang.String namespace) { throw new RuntimeException("Stub!"); }

/**
 * Add a listener for property changes.
 * <p>
 * This listener will be called whenever properties in the specified namespace change. Callbacks
 * will be made on the specified executor. Future calls to this method with the same listener
 * will replace the old namespace and executor. Remove the listener entirely by calling
 * {@link #removeOnPropertiesChangedListener(OnPropertiesChangedListener)}.
 *
 * <br>
 * Requires android.Manifest.permission.READ_DEVICE_CONFIG
 * @param namespace                   The namespace containing properties to monitor.
 * This value must never be {@code null}.
 * @param executor                    The executor which will be used to run callbacks.
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param onPropertiesChangedListener The listener to add.
 * This value must never be {@code null}.
 * @hide
 * @see #removeOnPropertiesChangedListener(OnPropertiesChangedListener)
 */

public static void addOnPropertiesChangedListener(@android.annotation.NonNull java.lang.String namespace, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.provider.DeviceConfig.OnPropertiesChangedListener onPropertiesChangedListener) { throw new RuntimeException("Stub!"); }

/**
 * Remove a listener for property changes. The listener will receive no further notification of
 * property changes.
 *
 * @param onPropertiesChangedListener The listener to remove.
 * This value must never be {@code null}.
 * @hide
 * @see #addOnPropertiesChangedListener(String, Executor, OnPropertiesChangedListener)
 */

public static void removeOnPropertiesChangedListener(@android.annotation.NonNull android.provider.DeviceConfig.OnPropertiesChangedListener onPropertiesChangedListener) { throw new RuntimeException("Stub!"); }

/**
 * Namespace for autofill feature that provides suggestions across all apps when
 * the user interacts with input fields.
 *
 * @hide
 */

public static final java.lang.String NAMESPACE_AUTOFILL = "autofill";

/**
 * Namespace for content capture feature used by on-device machine intelligence
 * to provide suggestions in a privacy-safe manner.
 *
 * @hide
 */

public static final java.lang.String NAMESPACE_CONTENT_CAPTURE = "content_capture";

/**
 * Privacy related properties definitions.
 *
 * @hide
 */

public static final java.lang.String NAMESPACE_PRIVACY = "privacy";

/**
 * Namespace for Rollback flags that are applied immediately.
 *
 * @hide
 */

public static final java.lang.String NAMESPACE_ROLLBACK = "rollback";

/**
 * Namespace for Rollback flags that are applied after a reboot.
 *
 * @hide
 */

public static final java.lang.String NAMESPACE_ROLLBACK_BOOT = "rollback_boot";

/**
 * Namespace for window manager related features. The names to access the properties in this
 * namespace should be defined in {@link WindowManager}.
 *
 * @hide
 */

public static final java.lang.String NAMESPACE_WINDOW_MANAGER = "android:window_manager";
/**
 * Interface for monitoring changes to properties.
 * <p>
 * Override {@link #onPropertiesChanged(Properties)} to handle callbacks for changes.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnPropertiesChangedListener {

/**
 * Called when one or more properties have changed.
 *
 * @param properties Contains the complete collection of properties which have changed for a
 *                   single namespace.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onPropertiesChanged(@android.annotation.NonNull android.provider.DeviceConfig.Properties properties);
}

/**
 * A mapping of properties to values, as well as a single namespace which they all belong to.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Properties {

/**
 * Create a mapping of properties to values and the namespace they belong to.
 *
 * @param namespace The namespace these properties belong to.
 * @param keyValueMap A map between property names and property values.
 */

Properties(@android.annotation.NonNull java.lang.String namespace, @android.annotation.Nullable java.util.Map<java.lang.String,java.lang.String> keyValueMap) { throw new RuntimeException("Stub!"); }

/**
 * @return the namespace all properties within this instance belong to.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getNamespace() { throw new RuntimeException("Stub!"); }

/**
 * @return the non-null set of property names.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.Set<java.lang.String> getKeyset() { throw new RuntimeException("Stub!"); }

/**
 * Look up the String value of a property.
 *
 * @param name         The name of the property to look up.
 * This value must never be {@code null}.
 * @param defaultValue The value to return if the property has not been defined.
 * This value may be {@code null}.
 * @return the corresponding value, or defaultValue if none exists.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getString(@android.annotation.NonNull java.lang.String name, @android.annotation.Nullable java.lang.String defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Look up the boolean value of a property.
 *
 * @param name         The name of the property to look up.
 * This value must never be {@code null}.
 * @param defaultValue The value to return if the property has not been defined.
 * @return the corresponding value, or defaultValue if none exists.
 * @apiSince REL
 */

public boolean getBoolean(@android.annotation.NonNull java.lang.String name, boolean defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Look up the int value of a property.
 *
 * @param name         The name of the property to look up.
 * This value must never be {@code null}.
 * @param defaultValue The value to return if the property has not been defined or fails to
 *                     parse into an int.
 * @return the corresponding value, or defaultValue if no valid int is available.
 * @apiSince REL
 */

public int getInt(@android.annotation.NonNull java.lang.String name, int defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Look up the long value of a property.
 *
 * @param name         The name of the property to look up.
 * This value must never be {@code null}.
 * @param defaultValue The value to return if the property has not been defined. or fails to
 *                     parse into a long.
 * @return the corresponding value, or defaultValue if no valid long is available.
 * @apiSince REL
 */

public long getLong(@android.annotation.NonNull java.lang.String name, long defaultValue) { throw new RuntimeException("Stub!"); }

/**
 * Look up the int value of a property.
 *
 * @param name         The name of the property to look up.
 * This value must never be {@code null}.
 * @param defaultValue The value to return if the property has not been defined. or fails to
 *                     parse into a float.
 * @return the corresponding value, or defaultValue if no valid float is available.
 * @apiSince REL
 */

public float getFloat(@android.annotation.NonNull java.lang.String name, float defaultValue) { throw new RuntimeException("Stub!"); }
}

/**
 * Interface for accessing keys belonging to {@link #NAMESPACE_WINDOW_MANAGER}.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface WindowManager {

/**
 * Key for controlling whether system gestures are implicitly excluded by windows requesting
 * sticky immersive mode from apps that are targeting an SDK prior to Q.
 *
 * @see android.provider.DeviceConfig#NAMESPACE_WINDOW_MANAGER
 * @hide
 */

public static final java.lang.String KEY_SYSTEM_GESTURES_EXCLUDED_BY_PRE_Q_STICKY_IMMERSIVE = "system_gestures_excluded_by_pre_q_sticky_immersive";

/**
 * Key for accessing the system gesture exclusion limit (an integer in dp).
 *
 * @see android.provider.DeviceConfig#NAMESPACE_WINDOW_MANAGER
 * @hide
 */

public static final java.lang.String KEY_SYSTEM_GESTURE_EXCLUSION_LIMIT_DP = "system_gesture_exclusion_limit_dp";
}

}

