/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app.prediction;

import android.os.UserHandle;
import android.content.pm.ShortcutInfo;

/**
 * A representation of a launchable target.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AppTarget implements android.os.Parcelable {

AppTarget(android.os.Parcel parcel) { throw new RuntimeException("Stub!"); }

/**
 * Returns the target id.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppTargetId getId() { throw new RuntimeException("Stub!"); }

/**
 * Returns the class name for the app target.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getClassName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the package name for the app target.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the user for the app target.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.UserHandle getUser() { throw new RuntimeException("Stub!"); }

/**
 * Returns the shortcut info for the target.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.content.pm.ShortcutInfo getShortcutInfo() { throw new RuntimeException("Stub!"); }

/**
 * Returns the rank for the target. Rank of an AppTarget is a non-negative integer that
 * represents the importance of this target compared to other candidate targets. A smaller value
 * means higher importance in the list.
 
 * @return Value is 0 or greater
 * @apiSince REL
 */

public int getRank() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.prediction.AppTarget> CREATOR;
static { CREATOR = null; }
/**
 * A builder for app targets.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * @param id A unique id for this launchable target.
 * This value must never be {@code null}.
 * @param packageName PackageName of the target.
 * This value must never be {@code null}.
 * @param user The UserHandle of the user which this target belongs to.
 * This value must never be {@code null}.
 * @hide
 */

public Builder(@android.annotation.NonNull android.app.prediction.AppTargetId id, @android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * @param id A unique id for this launchable target.
 * This value must never be {@code null}.
 * @param info The ShortcutInfo that represents this launchable target.
 * This value must never be {@code null}.
 * @hide
 */

public Builder(@android.annotation.NonNull android.app.prediction.AppTargetId id, @android.annotation.NonNull android.content.pm.ShortcutInfo info) { throw new RuntimeException("Stub!"); }

/**
 * Sets the className for the target.
 
 * @param className This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppTarget.Builder setClassName(@android.annotation.NonNull java.lang.String className) { throw new RuntimeException("Stub!"); }

/**
 * Sets the rank of the target.
 
 * @param rank Value is 0 or greater
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppTarget.Builder setRank(int rank) { throw new RuntimeException("Stub!"); }

/**
 * Builds a new AppTarget instance.
 *
 * @throws IllegalStateException if no target is set
 * @see #setTarget(ShortcutInfo)
 * @see #setTarget(String, UserHandle)
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppTarget build() { throw new RuntimeException("Stub!"); }
}

}

