/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.content;


/**
 * Autofill options for a given package.
 *
 * <p>This object is created by the Autofill System Service and passed back to the app when the
 * application is created.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AutofillOptions implements android.os.Parcelable {

/** @apiSince REL */

public AutofillOptions(int loggingLevel, boolean compatModeEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Returns whether activity is whitelisted for augmented autofill.
 
 * @param context This value must never be {@code null}.
 * @apiSince REL
 */

public boolean isAugmentedAutofillEnabled(@android.annotation.NonNull android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public static android.content.AutofillOptions forWhitelistingItself() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.content.AutofillOptions> CREATOR;
static { CREATOR = null; }

/**
 * Whether package is whitelisted for augmented autofill.
 * @apiSince REL
 */

public boolean augmentedAutofillEnabled;

/**
 * Whether compatibility mode is enabled for the package.
 * @apiSince REL
 */

public final boolean compatModeEnabled;
{ compatModeEnabled = false; }

/**
 * Logging level for {@code logcat} statements.
 * @apiSince REL
 */

public final int loggingLevel;
{ loggingLevel = 0; }

/**
 * List of whitelisted activities.
 * @apiSince REL
 */

@android.annotation.Nullable public android.util.ArraySet<android.content.ComponentName> whitelistedActivitiesForAugmentedAutofill;
}

