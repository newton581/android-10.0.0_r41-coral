
package android.os;


/**
 * Describes the source of some work that may be done by someone else.
 * Currently the public representation of what a work source is is not
 * defined; this is an opaque container.
 * @apiSince 9
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class WorkSource implements android.os.Parcelable {

/**
 * Create an empty work source.
 * @apiSince 9
 */

public WorkSource() { throw new RuntimeException("Stub!"); }

/**
 * Create a new WorkSource that is a copy of an existing one.
 * If <var>orig</var> is null, an empty WorkSource is created.
 * @apiSince 9
 */

public WorkSource(android.os.WorkSource orig) { throw new RuntimeException("Stub!"); }

/** @hide */

public WorkSource(int uid) { throw new RuntimeException("Stub!"); }

/** @hide */

public int size() { throw new RuntimeException("Stub!"); }

/** @hide */

public int get(int index) { throw new RuntimeException("Stub!"); }

/** @hide */

public java.lang.String getName(int index) { throw new RuntimeException("Stub!"); }

/**
 * Clear this WorkSource to be empty.
 * @apiSince 9
 */

public void clear() { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * Compare this WorkSource with another.
 * @param other The WorkSource to compare against.
 * @return If there is a difference, true is returned.
 * @apiSince 9
 */

public boolean diff(android.os.WorkSource other) { throw new RuntimeException("Stub!"); }

/**
 * Replace the current contents of this work source with the given
 * work source.  If {@code other} is null, the current work source
 * will be made empty.
 * @apiSince 9
 */

public void set(android.os.WorkSource other) { throw new RuntimeException("Stub!"); }

/**
 * Legacy API, DO NOT USE: Only deals with flat UIDs and tags. No chains are transferred, and no
 * differences in chains are returned. This will be removed once its callers have been
 * rewritten.
 *
 * NOTE: This is currently only used in GnssLocationProvider.
 *
 * @hide
 * @deprecated for internal use only. WorkSources are opaque and no external callers should need
 *     to be aware of internal differences.
 */

@Deprecated
public android.os.WorkSource[] setReturningDiffs(android.os.WorkSource other) { throw new RuntimeException("Stub!"); }

/**
 * Merge the contents of <var>other</var> WorkSource in to this one.
 *
 * @param other The other WorkSource whose contents are to be merged.
 * @return Returns true if any new sources were added.
 * @apiSince 9
 */

public boolean add(android.os.WorkSource other) { throw new RuntimeException("Stub!"); }

/**
 * Legacy API: DO NOT USE. Only in use from unit tests.
 *
 * @hide
 * @deprecated meant for unit testing use only. Will be removed in a future API revision.
 */

@Deprecated
public android.os.WorkSource addReturningNewbs(android.os.WorkSource other) { throw new RuntimeException("Stub!"); }

/** @hide */

public boolean add(int uid) { throw new RuntimeException("Stub!"); }

/** @hide */

public boolean add(int uid, java.lang.String name) { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public boolean remove(android.os.WorkSource other) { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.WorkSource> CREATOR;
static { CREATOR = null; }
}

