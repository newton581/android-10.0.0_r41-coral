/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.net;


/**
 * This class is used to return the interface name and fd of the test interface
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class TestNetworkInterface implements android.os.Parcelable {

/** @apiSince REL */

public TestNetworkInterface(android.os.ParcelFileDescriptor pfd, java.lang.String intf) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.ParcelFileDescriptor getFileDescriptor() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getInterfaceName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final android.os.Parcelable.Creator<android.net.TestNetworkInterface> CREATOR;
static { CREATOR = null; }
}

