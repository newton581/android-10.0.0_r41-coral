/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.permission;

import android.Manifest;
import java.util.List;

/**
 * System level service for accessing the permission capabilities of the platform.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class PermissionManager {

PermissionManager() { throw new RuntimeException("Stub!"); }

/**
 * Gets the version of the runtime permission database.
 *
 * <br>
 * Requires android.Manifest.permission.ADJUST_RUNTIME_PERMISSIONS_POLICY
 * @return The database version, -1 when this is an upgrade from pre-Q, 0 when this is a fresh
 * install.
 *
 * Value is 0 or greater
 * @hide
 */

public int getRuntimePermissionsVersion() { throw new RuntimeException("Stub!"); }

/**
 * Sets the version of the runtime permission database.
 *
 * <br>
 * Requires android.Manifest.permission.ADJUST_RUNTIME_PERMISSIONS_POLICY
 * @param version The new version.
 *
 * Value is 0 or greater
 * @hide
 */

public void setRuntimePermissionsVersion(int version) { throw new RuntimeException("Stub!"); }

/**
 * Get set of permissions that have been split into more granular or dependent permissions.
 *
 * <p>E.g. before {@link android.os.Build.VERSION_CODES#Q} an app that was granted
 * {@link Manifest.permission#ACCESS_COARSE_LOCATION} could access he location while it was in
 * foreground and background. On platforms after {@link android.os.Build.VERSION_CODES#Q}
 * the location permission only grants location access while the app is in foreground. This
 * would break apps that target before {@link android.os.Build.VERSION_CODES#Q}. Hence whenever
 * such an old app asks for a location permission (i.e. the
 * {@link SplitPermissionInfo#getSplitPermission()}), then the
 * {@link Manifest.permission#ACCESS_BACKGROUND_LOCATION} permission (inside
 * {@link SplitPermissionInfo#getNewPermissions}) is added.
 *
 * <p>Note: Regular apps do not have to worry about this. The platform and permission controller
 * automatically add the new permissions where needed.
 *
 * @return All permissions that are split.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.permission.PermissionManager.SplitPermissionInfo> getSplitPermissions() { throw new RuntimeException("Stub!"); }
/**
 * A permission that was added in a previous API level might have split into several
 * permissions. This object describes one such split.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SplitPermissionInfo {

/**
 * Constructs a split permission.
 *
 * @param splitPerm old permission that will be split
 * @param newPerms list of new permissions that {@code rootPerm} will be split into
 * @param targetSdk apps targetting SDK versions below this will have {@code rootPerm}
 * split into {@code newPerms}
 * @hide
 */

SplitPermissionInfo(@android.annotation.NonNull java.lang.String splitPerm, @android.annotation.NonNull java.util.List<java.lang.String> newPerms, int targetSdk) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * Get the permission that is split.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getSplitPermission() { throw new RuntimeException("Stub!"); }

/**
 * Get the permissions that are added.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<java.lang.String> getNewPermissions() { throw new RuntimeException("Stub!"); }

/**
 * Get the target API level when the permission was split.
 * @apiSince REL
 */

public int getTargetSdk() { throw new RuntimeException("Stub!"); }
}

}

