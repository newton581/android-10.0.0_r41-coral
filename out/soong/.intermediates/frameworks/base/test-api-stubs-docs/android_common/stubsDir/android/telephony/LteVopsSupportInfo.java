/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;


/**
 * Class stores information related to LTE network VoPS support
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class LteVopsSupportInfo implements android.os.Parcelable {

/**
 * @param vops Value is {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_NOT_AVAILABLE}, {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_SUPPORTED}, or {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_NOT_SUPPORTED}
 
 * @param emergency Value is {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_NOT_AVAILABLE}, {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_SUPPORTED}, or {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_NOT_SUPPORTED}
 * @apiSince REL
 */

public LteVopsSupportInfo(int vops, int emergency) { throw new RuntimeException("Stub!"); }

/**
 * Provides the LTE VoPS support capability as described in:
 * 3GPP 24.301 EPS network feature support -> IMS VoPS
 
 * @return Value is {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_NOT_AVAILABLE}, {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_SUPPORTED}, or {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_NOT_SUPPORTED}
 * @apiSince REL
 */

public int getVopsSupport() { throw new RuntimeException("Stub!"); }

/**
 * Provides the LTE Emergency bearer support capability as described in:
 *    3GPP 24.301 EPS network feature support -> EMC BS
 *    25.331 LTE RRC SIB1 : ims-EmergencySupport-r9
 
 * @return Value is {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_NOT_AVAILABLE}, {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_SUPPORTED}, or {@link android.telephony.LteVopsSupportInfo#LTE_STATUS_NOT_SUPPORTED}
 * @apiSince REL
 */

public int getEmcBearerSupport() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * @return string representation.
 * @apiSince REL
 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.LteVopsSupportInfo> CREATOR;
static { CREATOR = null; }

/**
 * Indicates information not available from modem.
 * @apiSince REL
 */

public static final int LTE_STATUS_NOT_AVAILABLE = 1; // 0x1

/**
 * Indicates network does not support the feature.
 * @apiSince REL
 */

public static final int LTE_STATUS_NOT_SUPPORTED = 3; // 0x3

/**
 * Indicates network support the feature.
 * @apiSince REL
 */

public static final int LTE_STATUS_SUPPORTED = 2; // 0x2
}

