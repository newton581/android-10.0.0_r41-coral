/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.metrics;


/**
 * An event recorded by IpClient when IP provisioning completes for a network or
 * when a network disconnects.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class IpManagerEvent implements android.net.metrics.IpConnectivityLog.Event {

/**
 * @param eventType Value is {@link android.net.metrics.IpManagerEvent#PROVISIONING_OK}, {@link android.net.metrics.IpManagerEvent#PROVISIONING_FAIL}, {@link android.net.metrics.IpManagerEvent#COMPLETE_LIFECYCLE}, {@link android.net.metrics.IpManagerEvent#ERROR_STARTING_IPV4}, {@link android.net.metrics.IpManagerEvent#ERROR_STARTING_IPV6}, {@link android.net.metrics.IpManagerEvent#ERROR_STARTING_IPREACHABILITYMONITOR}, {@link android.net.metrics.IpManagerEvent#ERROR_INVALID_PROVISIONING}, or {@link android.net.metrics.IpManagerEvent#ERROR_INTERFACE_NOT_FOUND}
 * @apiSince REL
 */

public IpManagerEvent(int eventType, long duration) { throw new RuntimeException("Stub!"); }

/** @hide */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @hide */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int COMPLETE_LIFECYCLE = 3; // 0x3

/** @apiSince REL */

public static final int ERROR_INTERFACE_NOT_FOUND = 8; // 0x8

/** @apiSince REL */

public static final int ERROR_INVALID_PROVISIONING = 7; // 0x7

/** @apiSince REL */

public static final int ERROR_STARTING_IPREACHABILITYMONITOR = 6; // 0x6

/** @apiSince REL */

public static final int ERROR_STARTING_IPV4 = 4; // 0x4

/** @apiSince REL */

public static final int ERROR_STARTING_IPV6 = 5; // 0x5

/** @apiSince REL */

public static final int PROVISIONING_FAIL = 2; // 0x2

/** @apiSince REL */

public static final int PROVISIONING_OK = 1; // 0x1
}

