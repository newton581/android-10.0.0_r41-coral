/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.display;

import android.content.pm.ApplicationInfo;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class BrightnessConfiguration implements android.os.Parcelable {

BrightnessConfiguration(float[] lux, float[] nits, java.util.Map<java.lang.String,android.hardware.display.BrightnessCorrection> correctionsByPackageName, java.util.Map<java.lang.Integer,android.hardware.display.BrightnessCorrection> correctionsByCategory, java.lang.String description) { throw new RuntimeException("Stub!"); }

/**
 * Gets the base brightness as curve.
 *
 * The curve is returned as a pair of float arrays, the first representing all of the lux
 * points of the brightness curve and the second representing all of the nits values of the
 * brightness curve.
 *
 * @return the control points for the brightness curve.
 * @apiSince REL
 */

public android.util.Pair<float[],float[]> getCurve() { throw new RuntimeException("Stub!"); }

/**
 * Returns a brightness correction by app, or null.
 *
 * @param packageName
 *      The app's package name.
 *
 * This value must never be {@code null}.
 * @return The matching brightness correction, or null.
 *
 * @apiSince REL
 */

@android.annotation.Nullable
public android.hardware.display.BrightnessCorrection getCorrectionByPackageName(@android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Returns a brightness correction by app category, or null.
 *
 * @param category
 *      The app category.
 *
 * Value is {@link android.content.pm.ApplicationInfo#CATEGORY_UNDEFINED}, {@link android.content.pm.ApplicationInfo#CATEGORY_GAME}, {@link android.content.pm.ApplicationInfo#CATEGORY_AUDIO}, {@link android.content.pm.ApplicationInfo#CATEGORY_VIDEO}, {@link android.content.pm.ApplicationInfo#CATEGORY_IMAGE}, {@link android.content.pm.ApplicationInfo#CATEGORY_SOCIAL}, {@link android.content.pm.ApplicationInfo#CATEGORY_NEWS}, {@link android.content.pm.ApplicationInfo#CATEGORY_MAPS}, or {@link android.content.pm.ApplicationInfo#CATEGORY_PRODUCTIVITY}
 * @return The matching brightness correction, or null.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.hardware.display.BrightnessCorrection getCorrectionByCategory(int category) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.display.BrightnessConfiguration> CREATOR;
static { CREATOR = null; }
/**
 * A builder class for {@link BrightnessConfiguration}s.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

/**
 * Constructs the builder with the control points for the brightness curve.
 *
 * Brightness curves must have strictly increasing ambient brightness values in lux and
 * monotonically increasing display brightness values in nits. In addition, the initial
 * control point must be 0 lux.
 *
 * @throws IllegalArgumentException if the initial control point is not at 0 lux.
 * @throws IllegalArgumentException if the lux levels are not strictly increasing.
 * @throws IllegalArgumentException if the nit levels are not monotonically increasing.
 * @apiSince REL
 */

public Builder(float[] lux, float[] nits) { throw new RuntimeException("Stub!"); }

/**
 * Returns the maximum number of corrections by package name allowed.
 *
 * @return The maximum number of corrections by package name allowed.
 *
 * @apiSince REL
 */

public int getMaxCorrectionsByPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the maximum number of corrections by category allowed.
 *
 * @return The maximum number of corrections by category allowed.
 *
 * @apiSince REL
 */

public int getMaxCorrectionsByCategory() { throw new RuntimeException("Stub!"); }

/**
 * Add a brightness correction by app package name.
 * This correction is applied whenever an app with this package name has the top activity
 * of the focused stack.
 *
 * @param packageName
 *      The app's package name.
 * This value must never be {@code null}.
 * @param correction
 *      The brightness correction.
 *
 * This value must never be {@code null}.
 * @return The builder.
 *
 * This value will never be {@code null}.
 * @throws IllegalArgumentExceptions
 *      Maximum number of corrections by package name exceeded (see
 *      {@link #getMaxCorrectionsByPackageName}).
 *
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.display.BrightnessConfiguration.Builder addCorrectionByPackageName(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull android.hardware.display.BrightnessCorrection correction) { throw new RuntimeException("Stub!"); }

/**
 * Add a brightness correction by app category.
 * This correction is applied whenever an app with this category has the top activity of
 * the focused stack, and only if a correction by package name has not been applied.
 *
 * @param category
 *      The {@link android.content.pm.ApplicationInfo#category app category}.
 * Value is {@link android.content.pm.ApplicationInfo#CATEGORY_UNDEFINED}, {@link android.content.pm.ApplicationInfo#CATEGORY_GAME}, {@link android.content.pm.ApplicationInfo#CATEGORY_AUDIO}, {@link android.content.pm.ApplicationInfo#CATEGORY_VIDEO}, {@link android.content.pm.ApplicationInfo#CATEGORY_IMAGE}, {@link android.content.pm.ApplicationInfo#CATEGORY_SOCIAL}, {@link android.content.pm.ApplicationInfo#CATEGORY_NEWS}, {@link android.content.pm.ApplicationInfo#CATEGORY_MAPS}, or {@link android.content.pm.ApplicationInfo#CATEGORY_PRODUCTIVITY}
 * @param correction
 *      The brightness correction.
 *
 * This value must never be {@code null}.
 * @return The builder.
 *
 * This value will never be {@code null}.
 * @throws IllegalArgumentException
 *      Maximum number of corrections by category exceeded (see
 *      {@link #getMaxCorrectionsByCategory}).
 *
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.display.BrightnessConfiguration.Builder addCorrectionByCategory(int category, @android.annotation.NonNull android.hardware.display.BrightnessCorrection correction) { throw new RuntimeException("Stub!"); }

/**
 * Set description of the brightness curve.
 *
 * @param description brief text describing the curve pushed. It maybe truncated
 *                    and will not be displayed in the UI
 
 * This value may be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.display.BrightnessConfiguration.Builder setDescription(@android.annotation.Nullable java.lang.String description) { throw new RuntimeException("Stub!"); }

/**
 * Builds the {@link BrightnessConfiguration}.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.display.BrightnessConfiguration build() { throw new RuntimeException("Stub!"); }
}

}

