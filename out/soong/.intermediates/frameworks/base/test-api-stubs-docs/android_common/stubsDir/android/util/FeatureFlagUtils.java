/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.util;


/**
 * Util class to get feature flag information.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class FeatureFlagUtils {

public FeatureFlagUtils() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not a flag is enabled.
 *
 * @param feature the flag name
 * @return true if the flag is enabled (either by default in system, or override by user)
 * @apiSince REL
 */

public static boolean isEnabled(android.content.Context context, java.lang.String feature) { throw new RuntimeException("Stub!"); }

/**
 * Override feature flag to new state.
 * @apiSince REL
 */

public static void setEnabled(android.content.Context context, java.lang.String feature, boolean enabled) { throw new RuntimeException("Stub!"); }

/**
 * Returns all feature flags in their raw form.
 * @apiSince REL
 */

public static java.util.Map<java.lang.String,java.lang.String> getAllFeatureFlags() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final java.lang.String DYNAMIC_SYSTEM = "settings_dynamic_system";

/** @apiSince REL */

public static final java.lang.String FFLAG_OVERRIDE_PREFIX = "sys.fflag.override.";

/** @apiSince REL */

public static final java.lang.String FFLAG_PREFIX = "sys.fflag.";

/** @apiSince REL */

public static final java.lang.String HEARING_AID_SETTINGS = "settings_bluetooth_hearing_aid";

/** @apiSince REL */

public static final java.lang.String PERSIST_PREFIX = "persist.sys.fflag.override.";

/** @apiSince REL */

public static final java.lang.String PIXEL_WALLPAPER_CATEGORY_SWITCH = "settings_pixel_wallpaper_category_switch";

/** @apiSince REL */

public static final java.lang.String SCREENRECORD_LONG_PRESS = "settings_screenrecord_long_press";

/** @apiSince REL */

public static final java.lang.String SEAMLESS_TRANSFER = "settings_seamless_transfer";
}

