/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony.mbms.vendor;

import android.os.IBinder;
import android.app.Service;
import android.telephony.mbms.MbmsErrors;
import android.os.Binder;

/**
 * Base class for MBMS group-call services. The middleware should override this class to implement
 * its {@link Service} for group calls
 * Subclasses should call this class's {@link #onBind} method to obtain an {@link IBinder} if they
 * override {@link #onBind}.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MbmsGroupCallServiceBase extends android.app.Service {

public MbmsGroupCallServiceBase() { throw new RuntimeException("Stub!"); }

/**
 * Initialize the group call service for this app and subscription ID, registering the callback.
 *
 * May throw an {@link IllegalArgumentException} or a {@link SecurityException}, which
 * will be intercepted and passed to the app as
 * {@link MbmsErrors.InitializationErrtrors#ERROR_UNABLE_TO_INITIALIZE}
 *
 * May return any value from {@link MbmsErrors.InitializationErrors}
 * or {@link MbmsErrors#SUCCESS}. Non-successful error codes will be passed to the app via
 * {@link IMbmsGroupCallSessionCallback#onError(int, String)}.
 *
 * @param callback The callback to use to communicate with the app.
 * This value must never be {@code null}.
 * @param subscriptionId The subscription ID to use.
 * @apiSince REL
 */

public int initialize(@android.annotation.NonNull android.telephony.mbms.MbmsGroupCallSessionCallback callback, int subscriptionId) throws android.os.RemoteException { throw new RuntimeException("Stub!"); }

/**
 * Starts a particular group call. This method may perform asynchronous work. When
 * the call is ready for consumption, the middleware should inform the app via
 * {@link IGroupCallCallback#onGroupCallStateChanged(int, int)}.
 *
 * May throw an {@link IllegalArgumentException} or an {@link IllegalStateException}
 *
 * @param subscriptionId The subscription id to use.
 * @param tmgi The TMGI, an identifier for the group call.
 * @param saiList A list of SAIs for the group call.
 * This value must never be {@code null}.
 * @param frequencyList A list of frequencies for the group call.
 * This value must never be {@code null}.
 * @param callback The callback object on which the app wishes to receive updates.
 * This value must never be {@code null}.
 * @return Any error in {@link MbmsErrors.GeneralErrors}
 * @apiSince REL
 */

public int startGroupCall(int subscriptionId, long tmgi, @android.annotation.NonNull java.util.List<java.lang.Integer> saiList, @android.annotation.NonNull java.util.List<java.lang.Integer> frequencyList, @android.annotation.NonNull android.telephony.mbms.GroupCallCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Stop the group call identified by {@code tmgi}.
 *
 * The callback provided via {@link #startGroupCall} should no longer be
 * used after this method has called by the app.
 *
 * May throw an {@link IllegalStateException}
 *
 * @param subscriptionId The subscription id to use.
 * @param tmgi The TMGI for the call to stop.
 * @apiSince REL
 */

public void stopGroupCall(int subscriptionId, long tmgi) { throw new RuntimeException("Stub!"); }

/**
 * Called when the app receives new SAI and frequency information for the group call identified
 * by {@code tmgi}.
 * @param saiList New list of SAIs that the call is available on.
 * This value must never be {@code null}.
 * @param frequencyList New list of frequencies that the call is available on.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void updateGroupCall(int subscriptionId, long tmgi, @android.annotation.NonNull java.util.List<java.lang.Integer> saiList, @android.annotation.NonNull java.util.List<java.lang.Integer> frequencyList) { throw new RuntimeException("Stub!"); }

/**
 * Signals that the app wishes to dispose of the session identified by the
 * {@code subscriptionId} argument and the caller's uid. No notification back to the
 * app is required for this operation, and the corresponding callback provided via
 * {@link #initialize} should no longer be used
 * after this method has been called by the app.
 *
 * May throw an {@link IllegalStateException}
 *
 * @param subscriptionId The subscription id to use.
 * @apiSince REL
 */

public void dispose(int subscriptionId) throws android.os.RemoteException { throw new RuntimeException("Stub!"); }

/**
 * Indicates that the app identified by the given UID and subscription ID has died.
 * @param uid the UID of the app, as returned by {@link Binder#getCallingUid()}.
 * @param subscriptionId The subscription ID the app is using.
 * @apiSince REL
 */

public void onAppCallbackDied(int uid, int subscriptionId) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }
}

