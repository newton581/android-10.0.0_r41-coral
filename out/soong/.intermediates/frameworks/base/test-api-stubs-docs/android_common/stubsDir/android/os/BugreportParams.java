/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;


/**
 * Parameters that specify what kind of bugreport should be taken.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class BugreportParams {

/**
 * @param mode Value is {@link android.os.BugreportParams#BUGREPORT_MODE_FULL}, {@link android.os.BugreportParams#BUGREPORT_MODE_INTERACTIVE}, {@link android.os.BugreportParams#BUGREPORT_MODE_REMOTE}, {@link android.os.BugreportParams#BUGREPORT_MODE_WEAR}, {@link android.os.BugreportParams#BUGREPORT_MODE_TELEPHONY}, or {@link android.os.BugreportParams#BUGREPORT_MODE_WIFI}
 * @apiSince REL
 */

public BugreportParams(int mode) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getMode() { throw new RuntimeException("Stub!"); }

/**
 * Options for a bugreport without user interference (and hence causing less
 * interference to the system), but includes all sections.
 * @apiSince REL
 */

public static final int BUGREPORT_MODE_FULL = 0; // 0x0

/**
 * Options that allow user to monitor progress and enter additional data; might not
 * include all sections.
 * @apiSince REL
 */

public static final int BUGREPORT_MODE_INTERACTIVE = 1; // 0x1

/**
 * Options for a bugreport requested remotely by administrator of the Device Owner app,
 * not the device's user.
 * @apiSince REL
 */

public static final int BUGREPORT_MODE_REMOTE = 2; // 0x2

/**
 * Options for a lightweight version of bugreport that only includes a few, urgent
 * sections used to report telephony bugs.
 * @apiSince REL
 */

public static final int BUGREPORT_MODE_TELEPHONY = 4; // 0x4

/**
 * Options for a bugreport on a wearable device.
 * @apiSince REL
 */

public static final int BUGREPORT_MODE_WEAR = 3; // 0x3

/**
 * Options for a lightweight bugreport that only includes a few sections related to
 * Wifi.
 * @apiSince REL
 */

public static final int BUGREPORT_MODE_WIFI = 5; // 0x5
}

