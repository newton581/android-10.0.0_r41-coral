/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.appprediction;

import android.os.Looper;
import android.app.prediction.AppPredictionSessionId;
import java.util.List;
import android.content.Intent;

/**
 * A service used to predict app and shortcut usage.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class AppPredictionService extends android.app.Service {

public AppPredictionService() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * If you override this method you <em>must</em> call through to the
 * superclass implementation.
 * @apiSince REL
 */

public void onCreate() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param intent This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public final android.os.IBinder onBind(@android.annotation.NonNull android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Called by a client app to indicate a target launch
 
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 
 * @param sessionId This value must never be {@code null}.
 
 * @param event This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onAppTargetEvent(@android.annotation.NonNull android.app.prediction.AppPredictionSessionId sessionId, @android.annotation.NonNull android.app.prediction.AppTargetEvent event);

/**
 * Called by a client app to indication a particular location has been shown to the user.
 
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 
 * @param sessionId This value must never be {@code null}.
 
 * @param launchLocation This value must never be {@code null}.
 
 * @param targetIds This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onLaunchLocationShown(@android.annotation.NonNull android.app.prediction.AppPredictionSessionId sessionId, @android.annotation.NonNull java.lang.String launchLocation, @android.annotation.NonNull java.util.List<android.app.prediction.AppTargetId> targetIds);

/**
 * Creates a new interaction session.
 *
 * @param context interaction context
 * This value must never be {@code null}.
 * @param sessionId the session's Id
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onCreatePredictionSession(@android.annotation.NonNull android.app.prediction.AppPredictionContext context, @android.annotation.NonNull android.app.prediction.AppPredictionSessionId sessionId) { throw new RuntimeException("Stub!"); }

/**
 * Called by the client app to request sorting of targets based on prediction rank.
 
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 
 * @param sessionId This value must never be {@code null}.
 
 * @param targets This value must never be {@code null}.
 
 * @param cancellationSignal This value must never be {@code null}.
 
 * @param callback This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onSortAppTargets(@android.annotation.NonNull android.app.prediction.AppPredictionSessionId sessionId, @android.annotation.NonNull java.util.List<android.app.prediction.AppTarget> targets, @android.annotation.NonNull android.os.CancellationSignal cancellationSignal, @android.annotation.NonNull java.util.function.Consumer<java.util.List<android.app.prediction.AppTarget>> callback);

/**
 * Called when any continuous prediction callback is registered.
 
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @apiSince REL
 */

public void onStartPredictionUpdates() { throw new RuntimeException("Stub!"); }

/**
 * Called when there are no longer any continuous prediction callbacks registered.
 
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @apiSince REL
 */

public void onStopPredictionUpdates() { throw new RuntimeException("Stub!"); }

/**
 * Called by the client app to request target predictions. This method is only called if there
 * are one or more prediction callbacks registered.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @see #updatePredictions(AppPredictionSessionId, List)
 
 * @param sessionId This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onRequestPredictionUpdate(@android.annotation.NonNull android.app.prediction.AppPredictionSessionId sessionId);

/**
 * Destroys the interaction session.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param sessionId the id of the session to destroy
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onDestroyPredictionSession(@android.annotation.NonNull android.app.prediction.AppPredictionSessionId sessionId) { throw new RuntimeException("Stub!"); }

/**
 * Used by the prediction factory to send back results the client app. The can be called
 * in response to {@link #onRequestPredictionUpdate(AppPredictionSessionId)} or proactively as
 * a result of changes in predictions.
 
 * @param sessionId This value must never be {@code null}.
 
 * @param targets This value must never be {@code null}.
 * @apiSince REL
 */

public final void updatePredictions(@android.annotation.NonNull android.app.prediction.AppPredictionSessionId sessionId, @android.annotation.NonNull java.util.List<android.app.prediction.AppTarget> targets) { throw new RuntimeException("Stub!"); }
}

