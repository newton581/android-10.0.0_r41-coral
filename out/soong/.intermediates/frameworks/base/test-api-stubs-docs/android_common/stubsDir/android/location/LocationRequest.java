/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.location;

import android.Manifest;
import android.os.SystemClock;
import android.os.WorkSource;

/**
 * A data object that contains quality of service parameters for requests
 * to the {@link LocationManager}.
 *
 * <p>LocationRequest objects are used to request a quality of service
 * for location updates from the Location Manager.
 *
 * <p>For example, if your application wants high accuracy location
 * it should create a location request with {@link #setQuality} set to
 * {@link #ACCURACY_FINE} or {@link #POWER_HIGH}, and it should set
 * {@link #setInterval} to less than one second. This would be
 * appropriate for mapping applications that are showing your location
 * in real-time.
 *
 * <p>At the other extreme, if you want negligible power
 * impact, but to still receive location updates when available, then use
 * {@link #setQuality} with {@link #POWER_NONE}. With this request your
 * application will not trigger (and therefore will not receive any
 * power blame) any location updates, but will receive locations
 * triggered by other applications. This would be appropriate for
 * applications that have no firm requirement for location, but can
 * take advantage when available.
 *
 * <p>In between these two extremes is a very common use-case, where
 * applications definitely want to receive
 * updates at a specified interval, and can receive them faster when
 * available, but still want a low power impact. These applications
 * should consider {@link #POWER_LOW} combined with a faster
 * {@link #setFastestInterval} (such as 1 minute) and a slower
 * {@link #setInterval} (such as 60 minutes). They will only be assigned
 * power blame for the interval set by {@link #setInterval}, but can
 * still receive locations triggered by other applications at a rate up
 * to {@link #setFastestInterval}. This style of request is appropriate for
 * many location aware applications, including background usage. Do be
 * careful to also throttle {@link #setFastestInterval} if you perform
 * heavy-weight work after receiving an update - such as using the network.
 *
 * <p>Activities should strongly consider removing all location
 * request when entering the background
 * (for example at {@link android.app.Activity#onPause}), or
 * at least swap the request to a larger interval and lower quality.
 * Future version of the location manager may automatically perform background
 * throttling on behalf of applications.
 *
 * <p>Applications cannot specify the exact location sources that are
 * used by Android's <em>Fusion Engine</em>. In fact, the system
 * may have multiple location sources (providers) running and may
 * fuse the results from several sources into a single Location object.
 *
 * <p>Location requests from applications with
 * {@link android.Manifest.permission#ACCESS_COARSE_LOCATION} and not
 * {@link android.Manifest.permission#ACCESS_FINE_LOCATION} will
 * be automatically throttled to a slower interval, and the location
 * object will be obfuscated to only show a coarse level of accuracy.
 *
 * <p>All location requests are considered hints, and you may receive
 * locations that are more accurate, less accurate, and slower
 * than requested.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class LocationRequest implements android.os.Parcelable {

/** @hide */

LocationRequest() { throw new RuntimeException("Stub!"); }

/**
 * Create a location request with default parameters.
 *
 * <p>Default parameters are for a low power, slowly updated location.
 * It can then be adjusted as required by the applications before passing
 * to the {@link LocationManager}
 *
 * @return a new location request
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static android.location.LocationRequest create() { throw new RuntimeException("Stub!"); }

/**
 * Set the quality of the request.
 *
 * <p>Use with a accuracy constant such as {@link #ACCURACY_FINE}, or a power
 * constant such as {@link #POWER_LOW}. You cannot request both accuracy and
 * power, only one or the other can be specified. The system will then
 * maximize accuracy or minimize power as appropriate.
 *
 * <p>The quality of the request is a strong hint to the system for which
 * location sources to use. For example, {@link #ACCURACY_FINE} is more likely
 * to use GPS, and {@link #POWER_LOW} is more likely to use WIFI & Cell tower
 * positioning, but it also depends on many other factors (such as which sources
 * are available) and is implementation dependent.
 *
 * <p>{@link #setQuality} and {@link #setInterval} are the most important parameters
 * on a location request.
 *
 * @param quality an accuracy or power constant
 * @return the same object, so that setters can be chained
 * This value will never be {@code null}.
 * @throws IllegalArgumentException if the quality constant is not valid
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.LocationRequest setQuality(int quality) { throw new RuntimeException("Stub!"); }

/**
 * Get the quality of the request.
 *
 * @return an accuracy or power constant
 * @apiSince REL
 */

public int getQuality() { throw new RuntimeException("Stub!"); }

/**
 * Set the desired interval for active location updates, in milliseconds.
 *
 * <p>The location manager will actively try to obtain location updates
 * for your application at this interval, so it has a
 * direct influence on the amount of power used by your application.
 * Choose your interval wisely.
 *
 * <p>This interval is inexact. You may not receive updates at all (if
 * no location sources are available), or you may receive them
 * slower than requested. You may also receive them faster than
 * requested (if other applications are requesting location at a
 * faster interval). The fastest rate that you will receive
 * updates can be controlled with {@link #setFastestInterval}.
 *
 * <p>Applications with only the coarse location permission may have their
 * interval silently throttled.
 *
 * <p>An interval of 0 is allowed, but not recommended, since
 * location updates may be extremely fast on future implementations.
 *
 * <p>{@link #setQuality} and {@link #setInterval} are the most important parameters
 * on a location request.
 *
 * @param millis desired interval in millisecond, inexact
 * @return the same object, so that setters can be chained
 * This value will never be {@code null}.
 * @throws IllegalArgumentException if the interval is less than zero
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.LocationRequest setInterval(long millis) { throw new RuntimeException("Stub!"); }

/**
 * Get the desired interval of this request, in milliseconds.
 *
 * @return desired interval in milliseconds, inexact
 * @apiSince REL
 */

public long getInterval() { throw new RuntimeException("Stub!"); }

/**
 * Requests that user location settings be ignored in order to satisfy this request. This API
 * is only for use in extremely rare scenarios where it is appropriate to ignore user location
 * settings, such as a user initiated emergency (dialing 911 for instance).
 *
 * <br>
 * Requires {@link android.Manifest.permission#WRITE_SECURE_SETTINGS}
 * @param locationSettingsIgnored Whether to ignore location settings
 * @return the same object, so that setters can be chained
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.LocationRequest setLocationSettingsIgnored(boolean locationSettingsIgnored) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if location settings will be ignored in order to satisfy this request.
 * @apiSince REL
 */

public boolean isLocationSettingsIgnored() { throw new RuntimeException("Stub!"); }

/**
 * Explicitly set the fastest interval for location updates, in
 * milliseconds.
 *
 * <p>This controls the fastest rate at which your application will
 * receive location updates, which might be faster than
 * {@link #setInterval} in some situations (for example, if other
 * applications are triggering location updates).
 *
 * <p>This allows your application to passively acquire locations
 * at a rate faster than it actively acquires locations, saving power.
 *
 * <p>Unlike {@link #setInterval}, this parameter is exact. Your
 * application will never receive updates faster than this value.
 *
 * <p>If you don't call this method, a fastest interval
 * will be selected for you. It will be a value faster than your
 * active interval ({@link #setInterval}).
 *
 * <p>An interval of 0 is allowed, but not recommended, since
 * location updates may be extremely fast on future implementations.
 *
 * <p>If {@link #setFastestInterval} is set slower than {@link #setInterval},
 * then your effective fastest interval is {@link #setInterval}.
 *
 * @param millis fastest interval for updates in milliseconds, exact
 * @return the same object, so that setters can be chained
 * This value will never be {@code null}.
 * @throws IllegalArgumentException if the interval is less than zero
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.LocationRequest setFastestInterval(long millis) { throw new RuntimeException("Stub!"); }

/**
 * Get the fastest interval of this request, in milliseconds.
 *
 * <p>The system will never provide location updates faster
 * than the minimum of {@link #getFastestInterval} and
 * {@link #getInterval}.
 *
 * @return fastest interval in milliseconds, exact
 * @apiSince REL
 */

public long getFastestInterval() { throw new RuntimeException("Stub!"); }

/**
 * Set the duration of this request, in milliseconds.
 *
 * <p>The duration begins immediately (and not when the request
 * is passed to the location manager), so call this method again
 * if the request is re-used at a later time.
 *
 * <p>The location manager will automatically stop updates after
 * the request expires.
 *
 * <p>The duration includes suspend time. Values less than 0
 * are allowed, but indicate that the request has already expired.
 *
 * @param millis duration of request in milliseconds
 * @return the same object, so that setters can be chained
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.LocationRequest setExpireIn(long millis) { throw new RuntimeException("Stub!"); }

/**
 * Set the request expiration time, in millisecond since boot.
 *
 * <p>This expiration time uses the same time base as {@link SystemClock#elapsedRealtime}.
 *
 * <p>The location manager will automatically stop updates after
 * the request expires.
 *
 * <p>The duration includes suspend time. Values before {@link SystemClock#elapsedRealtime}
 * are allowed,  but indicate that the request has already expired.
 *
 * @param millis expiration time of request, in milliseconds since boot including suspend
 * @return the same object, so that setters can be chained
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.LocationRequest setExpireAt(long millis) { throw new RuntimeException("Stub!"); }

/**
 * Get the request expiration time, in milliseconds since boot.
 *
 * <p>This value can be compared to {@link SystemClock#elapsedRealtime} to determine
 * the time until expiration.
 *
 * @return expiration time of request, in milliseconds since boot including suspend
 * @apiSince REL
 */

public long getExpireAt() { throw new RuntimeException("Stub!"); }

/**
 * Set the number of location updates.
 *
 * <p>By default locations are continuously updated until the request is explicitly
 * removed, however you can optionally request a set number of updates.
 * For example, if your application only needs a single fresh location,
 * then call this method with a value of 1 before passing the request
 * to the location manager.
 *
 * @param numUpdates the number of location updates requested
 * @return the same object, so that setters can be chained
 * This value will never be {@code null}.
 * @throws IllegalArgumentException if numUpdates is 0 or less
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.LocationRequest setNumUpdates(int numUpdates) { throw new RuntimeException("Stub!"); }

/**
 * Get the number of updates requested.
 *
 * <p>By default this is {@link Integer#MAX_VALUE}, which indicates that
 * locations are updated until the request is explicitly removed.
 *
 * @return number of updates
 * @apiSince REL
 */

public int getNumUpdates() { throw new RuntimeException("Stub!"); }

/**
 * Sets the provider to use for this location request.
 * @param provider This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.LocationRequest setProvider(@android.annotation.NonNull java.lang.String provider) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Used with {@link #setQuality} to request "block" level accuracy.
 *
 * <p>Block level accuracy is considered to be about 100 meter accuracy,
 * although this is implementation dependent. Using a coarse accuracy
 * such as this often consumes less power.
 * @apiSince REL
 */

public static final int ACCURACY_BLOCK = 102; // 0x66

/**
 * Used with {@link #setQuality} to request "city" level accuracy.
 *
 * <p>City level accuracy is considered to be about 10km accuracy,
 * although this is implementation dependent. Using a coarse accuracy
 * such as this often consumes less power.
 * @apiSince REL
 */

public static final int ACCURACY_CITY = 104; // 0x68

/**
 * Used with {@link #setQuality} to request the most accurate locations available.
 *
 * <p>This may be up to 1 meter accuracy, although this is implementation dependent.
 * @apiSince REL
 */

public static final int ACCURACY_FINE = 100; // 0x64

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.location.LocationRequest> CREATOR;
static { CREATOR = null; }

/**
 * Used with {@link #setQuality} to allow high power consumption for location.
 *
 * <p>This location request will allow high power location work.
 * @apiSince REL
 */

public static final int POWER_HIGH = 203; // 0xcb

/**
 * Used with {@link #setQuality} to request low power impact.
 *
 * <p>This location request will avoid high power location work where
 * possible.
 * @apiSince REL
 */

public static final int POWER_LOW = 201; // 0xc9

/**
 * Used with {@link #setQuality} to require no direct power impact (passive locations).
 *
 * <p>This location request will not trigger any active location requests,
 * but will receive locations triggered by other applications. Your application
 * will not receive any direct power blame for location work.
 * @apiSince REL
 */

public static final int POWER_NONE = 200; // 0xc8
}

