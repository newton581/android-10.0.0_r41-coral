/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.view.contentcapture;

import android.view.autofill.AutofillId;
import android.view.View;

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ViewNode extends android.app.assist.AssistStructure.ViewNode {

/** @hide */

ViewNode() { throw new RuntimeException("Stub!"); }

/**
 * Returns the {@link AutofillId} of this view's parent, if the parent is also part of the
 * screen observation tree.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.view.autofill.AutofillId getParentAutofillId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.view.autofill.AutofillId getAutofillId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.CharSequence getText() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getClassName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getIdPackage() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getIdType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getIdEntry() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getLeft() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTop() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getScrollX() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getScrollY() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getWidth() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getHeight() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isAssistBlocked() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isEnabled() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isClickable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isLongClickable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isContextClickable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isFocusable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isFocused() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isAccessibilityFocused() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isCheckable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isChecked() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isSelected() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isActivated() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isOpaque() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.CharSequence getContentDescription() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getHint() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextSelectionStart() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextSelectionEnd() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextColor() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextBackgroundColor() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public float getTextSize() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextStyle() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int[] getTextLineCharOffsets() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int[] getTextLineBaselines() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getVisibility() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getInputType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getMinTextEms() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getMaxTextEms() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getMaxTextLength() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getTextIdEntry() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return Value is {@link android.view.View#AUTOFILL_TYPE_NONE}, {@link android.view.View#AUTOFILL_TYPE_TEXT}, {@link android.view.View#AUTOFILL_TYPE_TOGGLE}, {@link android.view.View#AUTOFILL_TYPE_LIST}, or {@link android.view.View#AUTOFILL_TYPE_DATE}
 * @apiSince REL
 */

public int getAutofillType() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String[] getAutofillHints() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.view.autofill.AutofillValue getAutofillValue() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.CharSequence[] getAutofillOptions() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.LocaleList getLocaleList() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * @param parcel This value must never be {@code null}.

 * @param node This value may be {@code null}.
 */

public static void writeToParcel(@android.annotation.NonNull android.os.Parcel parcel, @android.annotation.Nullable android.view.contentcapture.ViewNode node, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * @param parcel This value must never be {@code null}.
 * @return This value may be {@code null}.
 */

@android.annotation.Nullable
public static android.view.contentcapture.ViewNode readFromParcel(@android.annotation.NonNull android.os.Parcel parcel) { throw new RuntimeException("Stub!"); }
/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class ViewStructureImpl extends android.view.ViewStructure {

/**
 * @hide
 * @param view This value must never be {@code null}.
 */

public ViewStructureImpl(@android.annotation.NonNull android.view.View view) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * @param parentId This value must never be {@code null}.
 */

public ViewStructureImpl(@android.annotation.NonNull android.view.autofill.AutofillId parentId, long virtualId, int sessionId) { throw new RuntimeException("Stub!"); }

/** @hide */

public android.view.contentcapture.ViewNode getNode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setId(int id, java.lang.String packageName, java.lang.String typeName, java.lang.String entryName) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setDimens(int left, int top, int scrollX, int scrollY, int width, int height) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setTransformation(android.graphics.Matrix matrix) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setElevation(float elevation) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setAlpha(float alpha) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setVisibility(int visibility) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setAssistBlocked(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setEnabled(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setClickable(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setLongClickable(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setContextClickable(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setFocusable(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setFocused(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setAccessibilityFocused(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setCheckable(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setChecked(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setSelected(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setActivated(boolean state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setOpaque(boolean opaque) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setClassName(java.lang.String className) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setContentDescription(java.lang.CharSequence contentDescription) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setText(java.lang.CharSequence text) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setText(java.lang.CharSequence text, int selectionStart, int selectionEnd) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setTextStyle(float size, int fgColor, int bgColor, int style) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setTextLines(int[] charOffsets, int[] baselines) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setTextIdEntry(java.lang.String entryName) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setHint(java.lang.CharSequence hint) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.CharSequence getText() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextSelectionStart() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextSelectionEnd() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.CharSequence getHint() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean hasExtras() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setChildCount(int num) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int addChildCount(int num) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getChildCount() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.view.ViewStructure newChild(int index) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.view.ViewStructure asyncNewChild(int index) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.view.autofill.AutofillId getAutofillId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setAutofillId(android.view.autofill.AutofillId id) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setAutofillId(android.view.autofill.AutofillId parentId, int virtualId) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param type Value is {@link android.view.View#AUTOFILL_TYPE_NONE}, {@link android.view.View#AUTOFILL_TYPE_TEXT}, {@link android.view.View#AUTOFILL_TYPE_TOGGLE}, {@link android.view.View#AUTOFILL_TYPE_LIST}, or {@link android.view.View#AUTOFILL_TYPE_DATE}
 * @apiSince REL
 */

public void setAutofillType(int type) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setAutofillHints(java.lang.String[] hints) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setAutofillValue(android.view.autofill.AutofillValue value) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setAutofillOptions(java.lang.CharSequence[] options) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setInputType(int inputType) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setMinTextEms(int minEms) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setMaxTextEms(int maxEms) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setMaxTextLength(int maxLength) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setDataIsSensitive(boolean sensitive) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void asyncCommit() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.graphics.Rect getTempRect() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setWebDomain(java.lang.String domain) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setLocaleList(android.os.LocaleList localeList) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.view.ViewStructure.HtmlInfo.Builder newHtmlInfoBuilder(java.lang.String tagName) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setHtmlInfo(android.view.ViewStructure.HtmlInfo htmlInfo) { throw new RuntimeException("Stub!"); }
}

}

