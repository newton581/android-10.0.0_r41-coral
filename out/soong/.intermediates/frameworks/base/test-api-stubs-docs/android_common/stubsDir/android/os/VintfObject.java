/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;


/**
 * Java API for libvintf.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class VintfObject {

VintfObject() { throw new RuntimeException("Stub!"); }

/**
 * Slurps all device information (both manifests and both matrices)
 * and report them.
 * If any error in getting one of the manifests, it is not included in
 * the list.
 *
 * @hide
 */

public static native java.lang.String[] report();

/**
 * @return a list of HAL names and versions that is supported by this
 * device as stated in device and framework manifests. For example,
 * ["android.hidl.manager@1.0", "android.hardware.camera.device@1.0",
 *  "android.hardware.camera.device@3.2"]. There are no duplicates.
 *
 * @hide
 */

public static native java.lang.String[] getHalNamesAndVersions();

/**
 * @return the BOARD_SEPOLICY_VERS build flag available in device manifest.
 *
 * @hide
 */

public static native java.lang.String getSepolicyVersion();

/**
 * @return a list of VNDK snapshots supported by the framework, as
 * specified in framework manifest. For example,
 * [("27", ["libjpeg.so", "libbase.so"]),
 *  ("28", ["libjpeg.so", "libbase.so"])]
 *
 * @hide
 */

public static native java.util.Map<java.lang.String,java.lang.String[]> getVndkSnapshots();

/**
 * @return Target Framework Compatibility Matrix (FCM) version, a number
 * specified in the device manifest indicating the FCM version that the
 * device manifest implements. Null if device manifest doesn't specify this
 * number (for legacy devices).
 *
 * @hide
 */

public static native java.lang.Long getTargetFrameworkCompatibilityMatrixVersion();
}

