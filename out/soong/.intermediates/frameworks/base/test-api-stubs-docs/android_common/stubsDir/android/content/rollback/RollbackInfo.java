/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.rollback;


/**
 * Information about a set of packages that can be, or already have been
 * rolled back together.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class RollbackInfo implements android.os.Parcelable {

RollbackInfo(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Returns a unique identifier for this rollback.
 * @apiSince REL
 */

public int getRollbackId() { throw new RuntimeException("Stub!"); }

/**
 * Returns the list of package that are rolled back.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.content.rollback.PackageRollbackInfo> getPackages() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if this rollback requires reboot to take effect after
 * being committed.
 * @apiSince REL
 */

public boolean isStaged() { throw new RuntimeException("Stub!"); }

/**
 * Returns the session ID for the committed rollback for staged rollbacks.
 * Only applicable for rollbacks that have been committed.
 * @apiSince REL
 */

public int getCommittedSessionId() { throw new RuntimeException("Stub!"); }

/**
 * Gets the list of package versions that motivated this rollback.
 * As provided to {@link #commitRollback} when the rollback was committed.
 * This is only applicable for rollbacks that have been committed.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.content.pm.VersionedPackage> getCausePackages() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.content.rollback.RollbackInfo> CREATOR;
static { CREATOR = null; }
}

