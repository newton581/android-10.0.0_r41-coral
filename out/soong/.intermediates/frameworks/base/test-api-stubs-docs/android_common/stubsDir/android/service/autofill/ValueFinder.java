/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill;


/**
 * Helper object used to obtain the value of a field in the screen being autofilled.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface ValueFinder {

/**
 * Gets the value of a field as String, or {@code null} when not found.
 
 * @param id This value must never be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public default java.lang.String findByAutofillId(@android.annotation.NonNull android.view.autofill.AutofillId id) { throw new RuntimeException("Stub!"); }

/**
 * Gets the value of a field, or {@code null} when not found.
 
 * @param id This value must never be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.view.autofill.AutofillValue findRawValueByAutofillId(@android.annotation.NonNull android.view.autofill.AutofillId id);
}

