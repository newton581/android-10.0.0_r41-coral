/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill;

import android.view.autofill.AutofillValue;

/**
 * Superclass of all sanitizers the system understands. As this is not public all public subclasses
 * have to implement {@link Sanitizer} again.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class InternalSanitizer implements android.service.autofill.Sanitizer, android.os.Parcelable {

public InternalSanitizer() { throw new RuntimeException("Stub!"); }

/**
 * Sanitizes an {@link AutofillValue}.
 *
 * @param value This value must never be {@code null}.
 * @return sanitized value or {@code null} if value could not be sanitized (for example: didn't
 * match regex, it's an invalid type, regex failed, etc).
 * @apiSince REL
 */

@android.annotation.Nullable
public abstract android.view.autofill.AutofillValue sanitize(@android.annotation.NonNull android.view.autofill.AutofillValue value);
}

