/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony.mbms;


/**
 * Describes a single MBMS streaming service.
 * @apiSince 28
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class StreamingServiceInfo extends android.telephony.mbms.ServiceInfo implements android.os.Parcelable {

/**
 * @param names User displayable names listed by language.
 * @param className The class name for this service - used by frontend apps to categorize and
 *                  filter.
 * @param locales The languages available for this service content.
 * @param serviceId The carrier's identifier for the service.
 * @param start The start time indicating when this service will be available.
 * @param end The end time indicating when this session stops being available.
 * @hide
 */

public StreamingServiceInfo(java.util.Map<java.util.Locale,java.lang.String> names, java.lang.String className, java.util.List<java.util.Locale> locales, java.lang.String serviceId, java.util.Date start, java.util.Date end) { super(null); throw new RuntimeException("Stub!"); }

/** @apiSince 28 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince 28 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince 28 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.mbms.StreamingServiceInfo> CREATOR;
static { CREATOR = null; }
}

