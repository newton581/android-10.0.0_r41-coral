/**
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.notification;

import android.app.RemoteInput;

/**
 * Information about how the user has interacted with a given notification.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class NotificationStats implements android.os.Parcelable {

/** @apiSince REL */

public NotificationStats() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the user has seen this notification at least once.
 * @apiSince REL
 */

public boolean hasSeen() { throw new RuntimeException("Stub!"); }

/**
 * Records that the user as seen this notification at least once.
 * @apiSince REL
 */

public void setSeen() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the user has expanded this notification at least once.
 * @apiSince REL
 */

public boolean hasExpanded() { throw new RuntimeException("Stub!"); }

/**
 * Records that the user has expanded this notification at least once.
 * @apiSince REL
 */

public void setExpanded() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the user has replied to a notification that has a
 * {@link android.app.Notification.Action.Builder#addRemoteInput(RemoteInput) direct reply} at
 * least once.
 * @apiSince REL
 */

public boolean hasDirectReplied() { throw new RuntimeException("Stub!"); }

/**
 * Records that the user has replied to a notification that has a
 * {@link android.app.Notification.Action.Builder#addRemoteInput(RemoteInput) direct reply}
 * at least once.
 * @apiSince REL
 */

public void setDirectReplied() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the user has snoozed this notification at least once.
 * @apiSince REL
 */

public boolean hasSnoozed() { throw new RuntimeException("Stub!"); }

/**
 * Records that the user has snoozed this notification at least once.
 * @apiSince REL
 */

public void setSnoozed() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the user has viewed the in-shade settings for this notification at least
 * once.
 * @apiSince REL
 */

public boolean hasViewedSettings() { throw new RuntimeException("Stub!"); }

/**
 * Records that the user has viewed the in-shade settings for this notification at least once.
 * @apiSince REL
 */

public void setViewedSettings() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the user has interacted with this notification beyond having viewed it.
 * @apiSince REL
 */

public boolean hasInteracted() { throw new RuntimeException("Stub!"); }

/**
 * Returns from which surface the notification was dismissed.
 
 * @return Value is {@link android.service.notification.NotificationStats#DISMISSAL_NOT_DISMISSED}, {@link android.service.notification.NotificationStats#DISMISSAL_OTHER}, {@link android.service.notification.NotificationStats#DISMISSAL_PEEK}, {@link android.service.notification.NotificationStats#DISMISSAL_AOD}, or {@link android.service.notification.NotificationStats#DISMISSAL_SHADE}
 * @apiSince REL
 */

public int getDismissalSurface() { throw new RuntimeException("Stub!"); }

/**
 * Returns from which surface the notification was dismissed.
 
 * @param dismissalSurface Value is {@link android.service.notification.NotificationStats#DISMISSAL_NOT_DISMISSED}, {@link android.service.notification.NotificationStats#DISMISSAL_OTHER}, {@link android.service.notification.NotificationStats#DISMISSAL_PEEK}, {@link android.service.notification.NotificationStats#DISMISSAL_AOD}, or {@link android.service.notification.NotificationStats#DISMISSAL_SHADE}
 * @apiSince REL
 */

public void setDismissalSurface(int dismissalSurface) { throw new RuntimeException("Stub!"); }

/**
 * Records whether the user indicated how they felt about a notification before or
 * during dismissal.
 
 * @param dismissalSentiment Value is {@link android.service.notification.NotificationStats#DISMISS_SENTIMENT_UNKNOWN}, {@link android.service.notification.NotificationStats#DISMISS_SENTIMENT_NEGATIVE}, {@link android.service.notification.NotificationStats#DISMISS_SENTIMENT_NEUTRAL}, or {@link android.service.notification.NotificationStats#DISMISS_SENTIMENT_POSITIVE}
 * @apiSince REL
 */

public void setDismissalSentiment(int dismissalSentiment) { throw new RuntimeException("Stub!"); }

/**
 * Returns how the user indicated they felt about a notification before or during dismissal.
 
 * @return Value is {@link android.service.notification.NotificationStats#DISMISS_SENTIMENT_UNKNOWN}, {@link android.service.notification.NotificationStats#DISMISS_SENTIMENT_NEGATIVE}, {@link android.service.notification.NotificationStats#DISMISS_SENTIMENT_NEUTRAL}, or {@link android.service.notification.NotificationStats#DISMISS_SENTIMENT_POSITIVE}
 * @apiSince REL
 */

public int getDismissalSentiment() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.notification.NotificationStats> CREATOR;
static { CREATOR = null; }

/**
 * Notification has been dismissed from always on display.
 * @apiSince REL
 */

public static final int DISMISSAL_AOD = 2; // 0x2

/**
 * Notification has not been dismissed yet.
 * @apiSince REL
 */

public static final int DISMISSAL_NOT_DISMISSED = -1; // 0xffffffff

/**
 * Notification has been dismissed from a {@link NotificationListenerService} or the app
 * itself.
 * @apiSince REL
 */

public static final int DISMISSAL_OTHER = 0; // 0x0

/**
 * Notification has been dismissed while peeking.
 * @apiSince REL
 */

public static final int DISMISSAL_PEEK = 1; // 0x1

/**
 * Notification has been dismissed from the notification shade.
 * @apiSince REL
 */

public static final int DISMISSAL_SHADE = 3; // 0x3

/**
 * The user indicated while dismissing that they did not like the notification.
 * @apiSince REL
 */

public static final int DISMISS_SENTIMENT_NEGATIVE = 0; // 0x0

/**
 * The user didn't indicate one way or another how they felt about the notification while
 * dismissing it.
 * @apiSince REL
 */

public static final int DISMISS_SENTIMENT_NEUTRAL = 1; // 0x1

/**
 * The user indicated while dismissing that they did like the notification.
 * @apiSince REL
 */

public static final int DISMISS_SENTIMENT_POSITIVE = 2; // 0x2

/**
 * No information is available about why this notification was dismissed, or the notification
 * isn't dismissed yet.
 * @apiSince REL
 */

public static final int DISMISS_SENTIMENT_UNKNOWN = -1000; // 0xfffffc18
}

