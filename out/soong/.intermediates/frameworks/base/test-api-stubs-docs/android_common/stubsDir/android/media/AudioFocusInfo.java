/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media;


/**
 * @hide
 * A class to encapsulate information about an audio focus owner or request.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AudioFocusInfo implements android.os.Parcelable {

/**
 * Class constructor
 * @param aa
 * @param clientId
 * @param packageName
 * @param gainRequest
 * @param lossReceived
 * @param flags
 * @hide
 */

AudioFocusInfo(android.media.AudioAttributes aa, int clientUid, java.lang.String clientId, java.lang.String packageName, int gainRequest, int lossReceived, int flags, int sdk) { throw new RuntimeException("Stub!"); }

/**
 * The audio attributes for the audio focus request.
 * @return non-null {@link AudioAttributes}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.media.AudioAttributes getAttributes() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getClientUid() { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getClientId() { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * The type of audio focus gain request.
 * @return one of {@link AudioManager#AUDIOFOCUS_GAIN},
 *     {@link AudioManager#AUDIOFOCUS_GAIN_TRANSIENT},
 *     {@link AudioManager#AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK},
 *     {@link AudioManager#AUDIOFOCUS_GAIN_TRANSIENT_EXCLUSIVE}.
 * @apiSince REL
 */

public int getGainRequest() { throw new RuntimeException("Stub!"); }

/**
 * The type of audio focus loss that was received by the
 * {@link AudioManager.OnAudioFocusChangeListener} if one was set.
 * @return 0 if focus wasn't lost, or one of {@link AudioManager#AUDIOFOCUS_LOSS},
 *   {@link AudioManager#AUDIOFOCUS_LOSS_TRANSIENT} or
 *   {@link AudioManager#AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK}.
 * @apiSince REL
 */

public int getLossReceived() { throw new RuntimeException("Stub!"); }

/**
 * The flags set in the audio focus request.
 * @return 0 or a combination of {link AudioManager#AUDIOFOCUS_FLAG_DELAY_OK},
 *     {@link AudioManager#AUDIOFOCUS_FLAG_PAUSES_ON_DUCKABLE_LOSS}, and
 *     {@link AudioManager#AUDIOFOCUS_FLAG_LOCK}.
 * @apiSince REL
 */

public int getFlags() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.media.AudioFocusInfo> CREATOR;
static { CREATOR = null; }
}

