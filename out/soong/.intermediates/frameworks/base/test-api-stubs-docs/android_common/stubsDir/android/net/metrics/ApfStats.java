/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.metrics;


/**
 * An event logged for an interface with APF capabilities when its IpClient state machine exits.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ApfStats implements android.net.metrics.IpConnectivityLog.Event {

ApfStats(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @hide */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @hide */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }
/**
 * Utility to create an instance of {@link ApfStats}.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Set the time interval in milliseconds these statistics covers.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats.Builder setDurationMs(long durationMs) { throw new RuntimeException("Stub!"); }

/**
 * Set the number of received RAs.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats.Builder setReceivedRas(int receivedRas) { throw new RuntimeException("Stub!"); }

/**
 * Set the number of received RAs matching a known RA.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats.Builder setMatchingRas(int matchingRas) { throw new RuntimeException("Stub!"); }

/**
 * Set the number of received RAs ignored due to the MAX_RAS limit.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats.Builder setDroppedRas(int droppedRas) { throw new RuntimeException("Stub!"); }

/**
 * Set the number of received RAs with a minimum lifetime of 0.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats.Builder setZeroLifetimeRas(int zeroLifetimeRas) { throw new RuntimeException("Stub!"); }

/**
 * Set the number of received RAs that could not be parsed.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats.Builder setParseErrors(int parseErrors) { throw new RuntimeException("Stub!"); }

/**
 * Set the number of APF program updates from receiving RAs.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats.Builder setProgramUpdates(int programUpdates) { throw new RuntimeException("Stub!"); }

/**
 * Set the total number of APF program updates.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats.Builder setProgramUpdatesAll(int programUpdatesAll) { throw new RuntimeException("Stub!"); }

/**
 * Set the number of APF program updates from allowing multicast traffic.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats.Builder setProgramUpdatesAllowingMulticast(int programUpdatesAllowingMulticast) { throw new RuntimeException("Stub!"); }

/**
 * Set the maximum APF program size advertised by hardware.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats.Builder setMaxProgramSize(int maxProgramSize) { throw new RuntimeException("Stub!"); }

/**
 * Create a new {@link ApfStats}.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfStats build() { throw new RuntimeException("Stub!"); }
}

}

