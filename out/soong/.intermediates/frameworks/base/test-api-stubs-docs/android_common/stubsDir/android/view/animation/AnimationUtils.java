/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.view.animation;

import android.os.SystemClock;
import android.content.res.Resources.NotFoundException;
import android.content.Context;

/**
 * Defines common utilities for working with animations.
 *
 * @apiSince 1
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AnimationUtils {

public AnimationUtils() { throw new RuntimeException("Stub!"); }

/**
 * Locks AnimationUtils{@link #currentAnimationTimeMillis()} to a fixed value for the current
 * thread. This is used by {@link android.view.Choreographer} to ensure that all accesses
 * during a vsync update are synchronized to the timestamp of the vsync.
 *
 * It is also exposed to tests to allow for rapid, flake-free headless testing.
 *
 * Must be followed by a call to {@link #unlockAnimationClock()} to allow time to
 * progress. Failing to do this will result in stuck animations, scrolls, and flings.
 *
 * Note that time is not allowed to "rewind" and must perpetually flow forward. So the
 * lock may fail if the time is in the past from a previously returned value, however
 * time will be frozen for the duration of the lock. The clock is a thread-local, so
 * ensure that {@link #lockAnimationClock(long)}, {@link #unlockAnimationClock()}, and
 * {@link #currentAnimationTimeMillis()} are all called on the same thread.
 *
 * This is also not reference counted in any way. Any call to {@link #unlockAnimationClock()}
 * will unlock the clock for everyone on the same thread. It is therefore recommended
 * for tests to use their own thread to ensure that there is no collision with any existing
 * {@link android.view.Choreographer} instance.
 *
 * @hide
 * */

public static void lockAnimationClock(long vsyncMillis) { throw new RuntimeException("Stub!"); }

/**
 * Frees the time lock set in place by {@link #lockAnimationClock(long)}. Must be called
 * to allow the animation clock to self-update.
 *
 * @hide
 */

public static void unlockAnimationClock() { throw new RuntimeException("Stub!"); }

/**
 * Returns the current animation time in milliseconds. This time should be used when invoking
 * {@link Animation#setStartTime(long)}. Refer to {@link android.os.SystemClock} for more
 * information about the different available clocks. The clock used by this method is
 * <em>not</em> the "wall" clock (it is not {@link System#currentTimeMillis}).
 *
 * @return the current animation time in milliseconds
 *
 * @see android.os.SystemClock
 * @apiSince 1
 */

public static long currentAnimationTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * Loads an {@link Animation} object from a resource
 *
 * @param context Application context used to access resources
 * @param id The resource id of the animation to load
 * @return The animation object referenced by the specified id
 * @throws NotFoundException when the animation cannot be loaded
 * @apiSince 1
 */

public static android.view.animation.Animation loadAnimation(android.content.Context context, int id) throws android.content.res.Resources.NotFoundException { throw new RuntimeException("Stub!"); }

/**
 * Loads a {@link LayoutAnimationController} object from a resource
 *
 * @param context Application context used to access resources
 * @param id The resource id of the animation to load
 * @return The animation controller object referenced by the specified id
 * @throws NotFoundException when the layout animation controller cannot be loaded
 * @apiSince 1
 */

public static android.view.animation.LayoutAnimationController loadLayoutAnimation(android.content.Context context, int id) throws android.content.res.Resources.NotFoundException { throw new RuntimeException("Stub!"); }

/**
 * Make an animation for objects becoming visible. Uses a slide and fade
 * effect.
 *
 * @param c Context for loading resources
 * @param fromLeft is the object to be animated coming from the left
 * @return The new animation
 * @apiSince 1
 */

public static android.view.animation.Animation makeInAnimation(android.content.Context c, boolean fromLeft) { throw new RuntimeException("Stub!"); }

/**
 * Make an animation for objects becoming invisible. Uses a slide and fade
 * effect.
 *
 * @param c Context for loading resources
 * @param toRight is the object to be animated exiting to the right
 * @return The new animation
 * @apiSince 1
 */

public static android.view.animation.Animation makeOutAnimation(android.content.Context c, boolean toRight) { throw new RuntimeException("Stub!"); }

/**
 * Make an animation for objects becoming visible. Uses a slide up and fade
 * effect.
 *
 * @param c Context for loading resources
 * @return The new animation
 * @apiSince 1
 */

public static android.view.animation.Animation makeInChildBottomAnimation(android.content.Context c) { throw new RuntimeException("Stub!"); }

/**
 * Loads an {@link Interpolator} object from a resource
 *
 * @param context Application context used to access resources
 * @param id The resource id of the animation to load
 * @return The interpolator object referenced by the specified id
 * @throws NotFoundException
 * @apiSince 1
 */

public static android.view.animation.Interpolator loadInterpolator(android.content.Context context, int id) throws android.content.res.Resources.NotFoundException { throw new RuntimeException("Stub!"); }
}

