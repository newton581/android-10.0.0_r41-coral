/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app;

import android.graphics.Rect;
import android.content.res.Configuration;

/**
 * Class that contains windowing configuration/state for other objects that contain windows directly
 * or indirectly. E.g. Activities, Task, Displays, ...
 * The test class is {@link com.android.server.wm.WindowConfigurationTests} which must be kept
 * up-to-date and ran anytime changes are made to this class.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class WindowConfiguration implements android.os.Parcelable, java.lang.Comparable<android.app.WindowConfiguration> {

/** @apiSince REL */

public WindowConfiguration() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Sets the bounds to the provided {@link Rect}.
 * @param rect the new bounds value.
 * @apiSince REL
 */

public void setBounds(android.graphics.Rect rect) { throw new RuntimeException("Stub!"); }

/**
 * Set {@link #mAppBounds} to the input Rect.
 * @param rect The rect value to set {@link #mAppBounds} to.
 * @see #getAppBounds()
 * @apiSince REL
 */

public void setAppBounds(android.graphics.Rect rect) { throw new RuntimeException("Stub!"); }

/**
 * @see #setAppBounds(Rect)
 * @apiSince REL
 */

public android.graphics.Rect getAppBounds() { throw new RuntimeException("Stub!"); }

/**
 * @see #setBounds(Rect)
 * @apiSince REL
 */

public android.graphics.Rect getBounds() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getRotation() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setRotation(int rotation) { throw new RuntimeException("Stub!"); }

/**
 * @param windowingMode Value is {@link android.app.WindowConfiguration#WINDOWING_MODE_UNDEFINED}, {@link android.app.WindowConfiguration#WINDOWING_MODE_FULLSCREEN}, {@link android.app.WindowConfiguration#WINDOWING_MODE_PINNED}, {@link android.app.WindowConfiguration#WINDOWING_MODE_SPLIT_SCREEN_PRIMARY}, {@link android.app.WindowConfiguration#WINDOWING_MODE_SPLIT_SCREEN_SECONDARY}, {@link android.app.WindowConfiguration#WINDOWING_MODE_FULLSCREEN_OR_SPLIT_SCREEN_SECONDARY}, or {@link android.app.WindowConfiguration#WINDOWING_MODE_FREEFORM}
 * @apiSince REL
 */

public void setWindowingMode(int windowingMode) { throw new RuntimeException("Stub!"); }

/**
 * @return Value is {@link android.app.WindowConfiguration#WINDOWING_MODE_UNDEFINED}, {@link android.app.WindowConfiguration#WINDOWING_MODE_FULLSCREEN}, {@link android.app.WindowConfiguration#WINDOWING_MODE_PINNED}, {@link android.app.WindowConfiguration#WINDOWING_MODE_SPLIT_SCREEN_PRIMARY}, {@link android.app.WindowConfiguration#WINDOWING_MODE_SPLIT_SCREEN_SECONDARY}, {@link android.app.WindowConfiguration#WINDOWING_MODE_FULLSCREEN_OR_SPLIT_SCREEN_SECONDARY}, or {@link android.app.WindowConfiguration#WINDOWING_MODE_FREEFORM}
 * @apiSince REL
 */

public int getWindowingMode() { throw new RuntimeException("Stub!"); }

/**
 * @param activityType Value is {@link android.app.WindowConfiguration#ACTIVITY_TYPE_UNDEFINED}, {@link android.app.WindowConfiguration#ACTIVITY_TYPE_STANDARD}, {@link android.app.WindowConfiguration#ACTIVITY_TYPE_HOME}, {@link android.app.WindowConfiguration#ACTIVITY_TYPE_RECENTS}, or {@link android.app.WindowConfiguration#ACTIVITY_TYPE_ASSISTANT}
 * @apiSince REL
 */

public void setActivityType(int activityType) { throw new RuntimeException("Stub!"); }

/**
 * @return Value is {@link android.app.WindowConfiguration#ACTIVITY_TYPE_UNDEFINED}, {@link android.app.WindowConfiguration#ACTIVITY_TYPE_STANDARD}, {@link android.app.WindowConfiguration#ACTIVITY_TYPE_HOME}, {@link android.app.WindowConfiguration#ACTIVITY_TYPE_RECENTS}, or {@link android.app.WindowConfiguration#ACTIVITY_TYPE_ASSISTANT}
 * @apiSince REL
 */

public int getActivityType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setTo(android.app.WindowConfiguration other) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int compareTo(android.app.WindowConfiguration that) { throw new RuntimeException("Stub!"); }

/** @hide */

public boolean equals(java.lang.Object that) { throw new RuntimeException("Stub!"); }

/** @hide */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @hide */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Assistant activity type.
 * @apiSince REL
 */

public static final int ACTIVITY_TYPE_ASSISTANT = 4; // 0x4

/**
 * Home/Launcher activity type.
 * @apiSince REL
 */

public static final int ACTIVITY_TYPE_HOME = 2; // 0x2

/**
 * Recents/Overview activity type. There is only one activity with this type in the system.
 * @apiSince REL
 */

public static final int ACTIVITY_TYPE_RECENTS = 3; // 0x3

/**
 * Standard activity type. Nothing special about the activity...
 * @apiSince REL
 */

public static final int ACTIVITY_TYPE_STANDARD = 1; // 0x1

/**
 * Activity type is currently not defined.
 * @apiSince REL
 */

public static final int ACTIVITY_TYPE_UNDEFINED = 0; // 0x0

/**
 * Rotation is not defined, use the parent containers rotation.
 * @apiSince REL
 */

public static final int ROTATION_UNDEFINED = -1; // 0xffffffff

/**
 * Can be freely resized within its parent container.
 * @apiSince REL
 */

public static final int WINDOWING_MODE_FREEFORM = 5; // 0x5

/**
 * Occupies the full area of the screen or the parent container.
 * @apiSince REL
 */

public static final int WINDOWING_MODE_FULLSCREEN = 1; // 0x1

/**
 * Alias for {@link #WINDOWING_MODE_SPLIT_SCREEN_SECONDARY} that makes it clear that the usage
 * points for APIs like {@link ActivityOptions#setLaunchWindowingMode(int)} that the container
 * will launch into fullscreen or split-screen secondary depending on if the device is currently
 * in fullscreen mode or split-screen mode.
 * @apiSince REL
 */

public static final int WINDOWING_MODE_FULLSCREEN_OR_SPLIT_SCREEN_SECONDARY = 4; // 0x4

/**
 * Always on-top (always visible). of other siblings in its parent container.
 * @apiSince REL
 */

public static final int WINDOWING_MODE_PINNED = 2; // 0x2

/**
 * The primary container driving the screen to be in split-screen mode.
 * @apiSince REL
 */

public static final int WINDOWING_MODE_SPLIT_SCREEN_PRIMARY = 3; // 0x3

/**
 * The containers adjacent to the {@link #WINDOWING_MODE_SPLIT_SCREEN_PRIMARY} container in
 * split-screen mode.
 * NOTE: Containers launched with the windowing mode with APIs like
 * {@link ActivityOptions#setLaunchWindowingMode(int)} will be launched in
 * {@link #WINDOWING_MODE_FULLSCREEN} if the display isn't currently in split-screen windowing
 * mode
 * @see #WINDOWING_MODE_FULLSCREEN_OR_SPLIT_SCREEN_SECONDARY
 * @apiSince REL
 */

public static final int WINDOWING_MODE_SPLIT_SCREEN_SECONDARY = 4; // 0x4

/**
 * Windowing mode is currently not defined.
 * @apiSince REL
 */

public static final int WINDOWING_MODE_UNDEFINED = 0; // 0x0
}

