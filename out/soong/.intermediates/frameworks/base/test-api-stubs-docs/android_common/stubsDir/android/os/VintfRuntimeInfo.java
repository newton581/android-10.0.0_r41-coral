/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;


/**
 * Java API for ::android::vintf::RuntimeInfo. Methods return null / 0 on any error.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class VintfRuntimeInfo {

VintfRuntimeInfo() { throw new RuntimeException("Stub!"); }

/**
 * @return content of /proc/cpuinfo
 *
 * @hide
 */

public static native java.lang.String getCpuInfo();

/**
 * @return os name extracted from uname() native call
 *
 * @hide
 */

public static native java.lang.String getOsName();

/**
 * @return node name extracted from uname() native call
 *
 * @hide
 */

public static native java.lang.String getNodeName();

/**
 * @return os release extracted from uname() native call
 *
 * @hide
 */

public static native java.lang.String getOsRelease();

/**
 * @return os version extracted from uname() native call
 *
 * @hide
 */

public static native java.lang.String getOsVersion();

/**
 * @return hardware id extracted from uname() native call
 *
 * @hide
 */

public static native java.lang.String getHardwareId();

/**
 * @return kernel version extracted from uname() native call. Format is
 * {@code x.y.z}.
 *
 * @hide
 */

public static native java.lang.String getKernelVersion();
}

