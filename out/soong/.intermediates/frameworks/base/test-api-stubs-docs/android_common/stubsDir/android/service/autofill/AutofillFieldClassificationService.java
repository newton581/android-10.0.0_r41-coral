/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill;

import android.view.autofill.AutofillValue;
import android.os.Bundle;
import java.util.Arrays;
import android.content.Intent;
import android.app.Service;
import android.os.Parcelable;

/**
 * A service that calculates field classification scores.
 *
 * <p>A field classification score is a {@code float} representing how well an
 * {@link AutofillValue} filled matches a expected value predicted by an autofill service
 * &mdash;a full match is {@code 1.0} (representing 100%), while a full mismatch is {@code 0.0}.
 *
 * <p>The exact score depends on the algorithm used to calculate it&mdash;the service must provide
 * at least one default algorithm (which is used when the algorithm is not specified or is invalid),
 * but it could provide more (in which case the algorithm name should be specified by the caller
 * when calculating the scores).
 *
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class AutofillFieldClassificationService extends android.app.Service {

/** @hide */

AutofillFieldClassificationService() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onCreate() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Field classification algorithm that computes the edit distance between two Strings.
 *
 * <p>Service implementation must provide this algorithm.</p>
 * @apiSince REL
 */

public static final java.lang.String REQUIRED_ALGORITHM_EDIT_DISTANCE = "EDIT_DISTANCE";

/**
 * Field classification algorithm that computes whether the last four digits between two
 * Strings match exactly.
 *
 * <p>Service implementation must provide this algorithm.</p>
 * @apiSince REL
 */

public static final java.lang.String REQUIRED_ALGORITHM_EXACT_MATCH = "EXACT_MATCH";

/**
 * The {@link Intent} action that must be declared as handled by a service
 * in its manifest for the system to recognize it as a quota providing service.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.service.autofill.AutofillFieldClassificationService";

/**
 * Manifest metadata key for the resource string array containing the names of all field
 * classification algorithms provided by the service.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_META_DATA_KEY_AVAILABLE_ALGORITHMS = "android.autofill.field_classification.available_algorithms";

/**
 * Manifest metadata key for the resource string containing the name of the default field
 * classification algorithm.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_META_DATA_KEY_DEFAULT_ALGORITHM = "android.autofill.field_classification.default_algorithm";
}

