/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill;

import android.widget.RemoteViews;

/**
 * Superclass of all transformation the system understands. As this is not public all
 * subclasses have to implement {@link Transformation} again.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class InternalTransformation implements android.service.autofill.Transformation, android.os.Parcelable {

public InternalTransformation() { throw new RuntimeException("Stub!"); }

/**
 * Applies multiple transformations to the children views of a
 * {@link android.widget.RemoteViews presentation template}.
 *
 * @param finder object used to find the value of a field in the screen.
 * This value must never be {@code null}.
 * @param template the {@link RemoteViews presentation template}.
 * This value must never be {@code null}.
 * @param transformations map of resource id of the child view inside the template to
 * transformation.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public static boolean batchApply(@android.annotation.NonNull android.service.autofill.ValueFinder finder, @android.annotation.NonNull android.widget.RemoteViews template, @android.annotation.NonNull java.util.ArrayList<android.util.Pair<java.lang.Integer,android.service.autofill.InternalTransformation>> transformations) { throw new RuntimeException("Stub!"); }
}

