/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.database.sqlite;


/**
 * Provides debugging info about all SQLite databases running in the current process.
 *
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class SQLiteDebug {

SQLiteDebug() { throw new RuntimeException("Stub!"); }

/**
 * return all pager and database stats for the current process.
 * @return {@link PagerStats}
 * @apiSince REL
 */

public static android.database.sqlite.SQLiteDebug.PagerStats getDatabaseInfo() { throw new RuntimeException("Stub!"); }

/**
 * Dumps detailed information about all databases used by the process.
 * @param printer The printer for dumping database state.
 * @param args Command-line arguments supplied to dumpsys dbinfo
 * @apiSince REL
 */

public static void dump(android.util.Printer printer, java.lang.String[] args) { throw new RuntimeException("Stub!"); }
/**
 * contains statistics about a database
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class DbStats {

/** @apiSince REL */

public DbStats(java.lang.String dbName, long pageCount, long pageSize, int lookaside, int hits, int misses, int cachesize) { throw new RuntimeException("Stub!"); }

/**
 * statement cache stats: hits/misses/cachesize
 * @apiSince REL
 */

public java.lang.String cache;

/**
 * name of the database
 * @apiSince REL
 */

public java.lang.String dbName;

/**
 * the database size
 * @apiSince REL
 */

public long dbSize;

/**
 * Number of lookaside slots: http://www.sqlite.org/c3ref/c_dbstatus_lookaside_used.html         * @apiSince REL
 */

public int lookaside;

/**
 * the page size for the database
 * @apiSince REL
 */

public long pageSize;
}

/**
 * Contains statistics about the active pagers in the current process.
 *
 * @see #nativeGetPagerStats(PagerStats)
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class PagerStats {

public PagerStats() { throw new RuntimeException("Stub!"); }

/** a list of {@link DbStats} - one for each main database opened by the applications
 * running on the android device
 * @apiSince REL
 */

public java.util.ArrayList<android.database.sqlite.SQLiteDebug.DbStats> dbStats;

/** records the largest memory allocation request handed to sqlite3.
 * documented at http://www.sqlite.org/c3ref/c_status_malloc_size.html
 * @apiSince REL
 */

public int largestMemAlloc;

/** the current amount of memory checked out by sqlite using sqlite3_malloc().
 * documented at http://www.sqlite.org/c3ref/c_status_malloc_size.html
 * @apiSince REL
 */

public int memoryUsed;

/** the number of bytes of page cache allocation which could not be sattisfied by the
 * SQLITE_CONFIG_PAGECACHE buffer and where forced to overflow to sqlite3_malloc().
 * The returned value includes allocations that overflowed because they where too large
 * (they were larger than the "sz" parameter to SQLITE_CONFIG_PAGECACHE) and allocations
 * that overflowed because no space was left in the page cache.
 * documented at http://www.sqlite.org/c3ref/c_status_malloc_size.html
 * @apiSince REL
 */

public int pageCacheOverflow;
}

}

