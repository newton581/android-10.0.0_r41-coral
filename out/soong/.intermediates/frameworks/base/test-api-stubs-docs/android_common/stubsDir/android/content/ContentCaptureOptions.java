/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.content;

import android.view.contentcapture.ContentCaptureManager;

/**
 * Content capture options for a given package.
 *
 * <p>This object is created by the Content Capture System Service and passed back to the app when
 * the application is created.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ContentCaptureOptions implements android.os.Parcelable {

/**
 * Constructor for "lite" objects that are just used to enable a {@link ContentCaptureManager}
 * for contexts belonging to the content capture service app.
 * @apiSince REL
 */

public ContentCaptureOptions(int loggingLevel) { throw new RuntimeException("Stub!"); }

/**
 * Default constructor.
 
 * @param whitelistedComponents This value may be {@code null}.
 * @apiSince REL
 */

public ContentCaptureOptions(int loggingLevel, int maxBufferSize, int idleFlushingFrequencyMs, int textChangeFlushingFrequencyMs, int logHistorySize, @android.annotation.Nullable android.util.ArraySet<android.content.ComponentName> whitelistedComponents) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static android.content.ContentCaptureOptions forWhitelistingItself() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.content.ContentCaptureOptions> CREATOR;
static { CREATOR = null; }

/**
 * Frequency the buffer is flushed if idle.
 * @apiSince REL
 */

public final int idleFlushingFrequencyMs;
{ idleFlushingFrequencyMs = 0; }

/**
 * Used to enable just a small set of APIs so it can used by activities belonging to the
 * content capture service APK.
 * @apiSince REL
 */

public final boolean lite;
{ lite = false; }

/**
 * Size of events that are logging on {@code dump}.
 * @apiSince REL
 */

public final int logHistorySize;
{ logHistorySize = 0; }

/**
 * Logging level for {@code logcat} statements.
 * @apiSince REL
 */

public final int loggingLevel;
{ loggingLevel = 0; }

/**
 * Maximum number of events that are buffered before sent to the app.
 * @apiSince REL
 */

public final int maxBufferSize;
{ maxBufferSize = 0; }

/**
 * Frequency the buffer is flushed if last event is a text change.
 * @apiSince REL
 */

public final int textChangeFlushingFrequencyMs;
{ textChangeFlushingFrequencyMs = 0; }

/**
 * List of activities explicitly whitelisted for content capture (or {@code null} if whitelisted
 * for all acitivites in the package).
 * @apiSince REL
 */

@android.annotation.Nullable public final android.util.ArraySet<android.content.ComponentName> whitelistedComponents;
{ whitelistedComponents = null; }
}

