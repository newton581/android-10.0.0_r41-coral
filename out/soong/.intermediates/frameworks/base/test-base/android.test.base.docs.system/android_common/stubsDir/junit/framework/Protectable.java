
package junit.framework;


/**
 * A <em>Protectable</em> can be run and can throw a Throwable.
 *
 * @see TestResult
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface Protectable {

/**
 * Run the the following method protected.
 */

public void protect() throws java.lang.Throwable;
}

