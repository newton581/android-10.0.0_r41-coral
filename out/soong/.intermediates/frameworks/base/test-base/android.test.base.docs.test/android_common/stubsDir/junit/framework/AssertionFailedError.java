
package junit.framework;


/**
 * Thrown when an assertion failed.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AssertionFailedError extends java.lang.AssertionError {

public AssertionFailedError() { throw new RuntimeException("Stub!"); }

public AssertionFailedError(java.lang.String message) { throw new RuntimeException("Stub!"); }
}

