
package junit.framework;


/**
 * A <code>TestFailure</code> collects a failed test together with
 * the caught exception.
 * @see TestResult
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class TestFailure {

/**
 * Constructs a TestFailure with the given test and exception.
 */

public TestFailure(junit.framework.Test failedTest, java.lang.Throwable thrownException) { throw new RuntimeException("Stub!"); }

/**
 * Gets the failed test.
 */

public junit.framework.Test failedTest() { throw new RuntimeException("Stub!"); }

/**
 * Gets the thrown exception.
 */

public java.lang.Throwable thrownException() { throw new RuntimeException("Stub!"); }

/**
 * Returns a short description of the failure.
 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public java.lang.String trace() { throw new RuntimeException("Stub!"); }

public java.lang.String exceptionMessage() { throw new RuntimeException("Stub!"); }

public boolean isFailure() { throw new RuntimeException("Stub!"); }

protected junit.framework.Test fFailedTest;

protected java.lang.Throwable fThrownException;
}

