/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.nfc_extras;

import java.io.IOException;

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class NfcExecutionEnvironment {

NfcExecutionEnvironment(com.android.nfc_extras.NfcAdapterExtras extras) { throw new RuntimeException("Stub!"); }

/**
 * Open the NFC Execution Environment on its contact interface.
 *
 * <p>Opening a channel to the the secure element may fail
 * for a number of reasons:
 * <ul>
 * <li>NFC must be enabled for the connection to the SE to be opened.
 * If it is disabled at the time of this call, an {@link EeNfcDisabledException}
 * is thrown.
 *
 * <li>Only one process may open the secure element at a time. Additionally,
 * this method is not reentrant. If the secure element is already opened,
 * either by this process or by a different process, an {@link EeAlreadyOpenException}
 * is thrown.
 *
 * <li>If the connection to the secure element could not be initialized,
 * an {@link EeInitializationException} is thrown.
 *
 * <li>If the secure element or the NFC controller is activated in listen
 * mode - that is, it is talking over the contactless interface - an
 * {@link EeListenModeException} is thrown.
 *
 * <li>If the NFC controller is in a field powered by a remote device,
 * such as a payment terminal, an {@link EeExternalFieldException} is
 * thrown.
 * </ul>
 * <p>All other NFC functionality is disabled while the NFC-EE is open
 * on its contact interface, so make sure to call {@link #close} once complete.
 *
 * <p class="note">
 * Requires the {@link android.Manifest.permission#WRITE_SECURE_SETTINGS} permission.
 *
 * @throws EeAlreadyOpenException if the NFC-EE is already open
 * @throws EeNfcDisabledException if NFC is disabled
 * @throws EeInitializationException if the Secure Element could not be initialized
 * @throws EeListenModeException if the NFCC or Secure Element is activated in listen mode
 * @throws EeExternalFieldException if the NFCC is in the presence of a remote-powered field
 * @throws EeIoException if an unknown error occurs
 */

public void open() throws com.android.nfc_extras.EeIOException { throw new RuntimeException("Stub!"); }

/**
 * Close the NFC Execution Environment on its contact interface.
 *
 * <p class="note">
 * Requires the {@link android.Manifest.permission#WRITE_SECURE_SETTINGS} permission.
 *
 * @throws IOException if the NFC-EE is already open, or some other error occurs
 */

public void close() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Send raw commands to the NFC-EE and receive the response.
 *
 * <p class="note">
 * Requires the {@link android.Manifest.permission#WRITE_SECURE_SETTINGS} permission.
 *
 * @throws IOException if the NFC-EE is not open, or some other error occurs
 */

public byte[] transceive(byte[] in) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Broadcast Action: An ISO-DEP AID was selected.
 *
 * <p>This happens as the result of a 'SELECT AID' command from an
 * external NFC reader/writer.
 *
 * <p>Always contains the extra field {@link #EXTRA_AID}
 *
 * <p class="note">
 * Requires the {@link android.Manifest.permission#WRITE_SECURE_SETTINGS} permission
 * to receive.
 */

public static final java.lang.String ACTION_AID_SELECTED = "com.android.nfc_extras.action.AID_SELECTED";
}

