/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.nfc_extras;

import android.nfc.NfcAdapter;

/**
 * Provides additional methods on an {@link NfcAdapter} for Card Emulation
 * and management of {@link NfcExecutionEnvironment}'s.
 *
 * There is a 1-1 relationship between an {@link NfcAdapterExtras} object and
 * a {@link NfcAdapter} object.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class NfcAdapterExtras {

NfcAdapterExtras(android.nfc.NfcAdapter adapter) { throw new RuntimeException("Stub!"); }

/**
 * Get the {@link NfcAdapterExtras} for the given {@link NfcAdapter}.
 *
 * <p class="note">
 * Requires the {@link android.Manifest.permission#WRITE_SECURE_SETTINGS} permission.
 *
 * @param adapter a {@link NfcAdapter}, must not be null
 * @return the {@link NfcAdapterExtras} object for the given {@link NfcAdapter}
 */

public static com.android.nfc_extras.NfcAdapterExtras get(android.nfc.NfcAdapter adapter) { throw new RuntimeException("Stub!"); }

/**
 * Get the routing state of this NFC EE.
 *
 * <p class="note">
 * Requires the {@link android.Manifest.permission#WRITE_SECURE_SETTINGS} permission.
 */

public com.android.nfc_extras.NfcAdapterExtras.CardEmulationRoute getCardEmulationRoute() { throw new RuntimeException("Stub!"); }

/**
 * Set the routing state of this NFC EE.
 *
 * <p>This routing state is not persisted across reboot.
 *
 * <p class="note">
 * Requires the {@link android.Manifest.permission#WRITE_SECURE_SETTINGS} permission.
 *
 * @param route a {@link CardEmulationRoute}
 */

public void setCardEmulationRoute(com.android.nfc_extras.NfcAdapterExtras.CardEmulationRoute route) { throw new RuntimeException("Stub!"); }

/**
 * Get the {@link NfcExecutionEnvironment} that is embedded with the
 * {@link NfcAdapter}.
 *
 * <p class="note">
 * Requires the {@link android.Manifest.permission#WRITE_SECURE_SETTINGS} permission.
 *
 * @return a {@link NfcExecutionEnvironment}, or null if there is no embedded NFC-EE
 */

public com.android.nfc_extras.NfcExecutionEnvironment getEmbeddedExecutionEnvironment() { throw new RuntimeException("Stub!"); }

/**
 * Authenticate the client application.
 *
 * Some implementations of NFC Adapter Extras may require applications
 * to authenticate with a token, before using other methods.
 *
 * @param token a implementation specific token
 * @throws java.lang.SecurityException if authentication failed
 */

public void authenticate(byte[] token) { throw new RuntimeException("Stub!"); }

/**
 * Returns the name of this adapter's driver.
 *
 * <p>Different NFC adapters may use different drivers.  This value is
 * informational and should not be parsed.
 *
 * @return the driver name, or empty string if unknown
 */

public java.lang.String getDriverName() { throw new RuntimeException("Stub!"); }

/**
 * Broadcast Action: an RF field OFF has been detected.
 *
 * <p class="note">This is an unreliable signal, and will be removed.
 * <p class="note">
 * Requires the {@link android.Manifest.permission#WRITE_SECURE_SETTINGS} permission
 * to receive.
 */

public static final java.lang.String ACTION_RF_FIELD_OFF_DETECTED = "com.android.nfc_extras.action.RF_FIELD_OFF_DETECTED";

/**
 * Broadcast Action: an RF field ON has been detected.
 *
 * <p class="note">This is an unreliable signal, and will be removed.
 * <p class="note">
 * Requires the {@link android.Manifest.permission#WRITE_SECURE_SETTINGS} permission
 * to receive.
 */

public static final java.lang.String ACTION_RF_FIELD_ON_DETECTED = "com.android.nfc_extras.action.RF_FIELD_ON_DETECTED";
/**
 * Immutable data class that describes a card emulation route.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class CardEmulationRoute {

public CardEmulationRoute(int route, com.android.nfc_extras.NfcExecutionEnvironment nfcEe) { throw new RuntimeException("Stub!"); }

/**
 * Card Emulation is turned off on this NfcAdapter.
 * <p>This is the default routing state after boot.
 */

public static final int ROUTE_OFF = 1; // 0x1

/**
 * Card Emulation is routed to {@link #nfcEe} only when the screen is on,
 * otherwise it is turned off.
 */

public static final int ROUTE_ON_WHEN_SCREEN_ON = 2; // 0x2

/**
 * The {@link NFcExecutionEnvironment} that is Card Emulation is routed to.
 * <p>null if {@link #route} is {@link #ROUTE_OFF}, otherwise not null.
 */

public final com.android.nfc_extras.NfcExecutionEnvironment nfcEe;
{ nfcEe = null; }

/**
 * A route such as {@link #ROUTE_OFF} or {@link #ROUTE_ON_WHEN_SCREEN_ON}.
 */

public final int route;
{ route = 0; }
}

}

