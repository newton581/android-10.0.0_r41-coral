/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.contentsuggestions;


/**
 * The request object used to request content selections from {@link ContentSuggestionsManager}.
 *
 * <p>Selections are requested for a given taskId as specified by
 * {@link android.app.ActivityManager} and optionally take an interest point that specifies the
 * point on the screen that should be considered as the most important.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class SelectionsRequest implements android.os.Parcelable {

SelectionsRequest(int taskId, @android.annotation.Nullable android.graphics.Point interestPoint, @android.annotation.Nullable android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Return the request task id.
 * @apiSince REL
 */

public int getTaskId() { throw new RuntimeException("Stub!"); }

/**
 * Return the request point of interest or {@code null} if there is no point of interest for
 * this request.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.graphics.Point getInterestPoint() { throw new RuntimeException("Stub!"); }

/**
 * Return the request extras, may be an empty bundle if there aren't any.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.contentsuggestions.SelectionsRequest> CREATOR;
static { CREATOR = null; }
/**
 * A builder for selections requests events.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Default constructor.
 *
 * @param taskId of the type used by {@link android.app.ActivityManager}
 * @apiSince REL
 */

public Builder(int taskId) { throw new RuntimeException("Stub!"); }

/**
 * Sets the request extras.
 
 * @param extras This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.contentsuggestions.SelectionsRequest.Builder setExtras(@android.annotation.NonNull android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Sets the request interest point.
 
 * @param interestPoint This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.contentsuggestions.SelectionsRequest.Builder setInterestPoint(@android.annotation.NonNull android.graphics.Point interestPoint) { throw new RuntimeException("Stub!"); }

/**
 * Builds a new request instance.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.contentsuggestions.SelectionsRequest build() { throw new RuntimeException("Stub!"); }
}

}

