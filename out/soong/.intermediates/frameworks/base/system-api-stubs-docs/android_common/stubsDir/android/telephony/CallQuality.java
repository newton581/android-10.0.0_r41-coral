/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;

import android.os.Parcelable;

/**
 * Parcelable object to handle call quality.
 * <p>
 * Currently this supports IMS calls.
 * <p>
 * It provides the call quality level, duration, and additional information related to RTP packets,
 * jitter and delay.
 * <p>
 * If there are multiple active calls, the CallQuality will pertain to the call in the foreground.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CallQuality implements android.os.Parcelable {

/**
 * Constructor.
 *
 * @param callQualityLevel the call quality level (see #CallQualityLevel)
 * @param callDuration the call duration in milliseconds
 * @param numRtpPacketsTransmitted RTP packets sent to network
 * @param numRtpPacketsReceived RTP packets received from network
 * @param numRtpPacketsTransmittedLost RTP packets which were lost in network and never
 * transmitted
 * @param numRtpPacketsNotReceived RTP packets which were lost in network and never recieved
 * @param averageRelativeJitter average relative jitter in milliseconds
 * @param maxRelativeJitter maximum relative jitter in milliseconds
 * @param averageRoundTripTime average round trip delay in milliseconds
 * @param codecType the codec type
 
 * @param downlinkCallQualityLevel Value is {@link android.telephony.CallQuality#CALL_QUALITY_EXCELLENT}, {@link android.telephony.CallQuality#CALL_QUALITY_GOOD}, {@link android.telephony.CallQuality#CALL_QUALITY_FAIR}, {@link android.telephony.CallQuality#CALL_QUALITY_POOR}, {@link android.telephony.CallQuality#CALL_QUALITY_BAD}, or {@link android.telephony.CallQuality#CALL_QUALITY_NOT_AVAILABLE}
 
 * @param uplinkCallQualityLevel Value is {@link android.telephony.CallQuality#CALL_QUALITY_EXCELLENT}, {@link android.telephony.CallQuality#CALL_QUALITY_GOOD}, {@link android.telephony.CallQuality#CALL_QUALITY_FAIR}, {@link android.telephony.CallQuality#CALL_QUALITY_POOR}, {@link android.telephony.CallQuality#CALL_QUALITY_BAD}, or {@link android.telephony.CallQuality#CALL_QUALITY_NOT_AVAILABLE}
 * @apiSince REL
 */

public CallQuality(int downlinkCallQualityLevel, int uplinkCallQualityLevel, int callDuration, int numRtpPacketsTransmitted, int numRtpPacketsReceived, int numRtpPacketsTransmittedLost, int numRtpPacketsNotReceived, int averageRelativeJitter, int maxRelativeJitter, int averageRoundTripTime, int codecType) { throw new RuntimeException("Stub!"); }

/**
 * Returns the downlink CallQualityLevel for a given ongoing call.
 
 * @return Value is {@link android.telephony.CallQuality#CALL_QUALITY_EXCELLENT}, {@link android.telephony.CallQuality#CALL_QUALITY_GOOD}, {@link android.telephony.CallQuality#CALL_QUALITY_FAIR}, {@link android.telephony.CallQuality#CALL_QUALITY_POOR}, {@link android.telephony.CallQuality#CALL_QUALITY_BAD}, or {@link android.telephony.CallQuality#CALL_QUALITY_NOT_AVAILABLE}
 * @apiSince REL
 */

public int getDownlinkCallQualityLevel() { throw new RuntimeException("Stub!"); }

/**
 * Returns the uplink CallQualityLevel for a given ongoing call.
 
 * @return Value is {@link android.telephony.CallQuality#CALL_QUALITY_EXCELLENT}, {@link android.telephony.CallQuality#CALL_QUALITY_GOOD}, {@link android.telephony.CallQuality#CALL_QUALITY_FAIR}, {@link android.telephony.CallQuality#CALL_QUALITY_POOR}, {@link android.telephony.CallQuality#CALL_QUALITY_BAD}, or {@link android.telephony.CallQuality#CALL_QUALITY_NOT_AVAILABLE}
 * @apiSince REL
 */

public int getUplinkCallQualityLevel() { throw new RuntimeException("Stub!"); }

/**
 * Returns the duration of the call, in milliseconds.
 * @apiSince REL
 */

public int getCallDuration() { throw new RuntimeException("Stub!"); }

/**
 * Returns the total number of RTP packets transmitted by this device for a given ongoing call.
 * @apiSince REL
 */

public int getNumRtpPacketsTransmitted() { throw new RuntimeException("Stub!"); }

/**
 * Returns the total number of RTP packets received by this device for a given ongoing call.
 * @apiSince REL
 */

public int getNumRtpPacketsReceived() { throw new RuntimeException("Stub!"); }

/**
 * Returns the number of RTP packets which were sent by this device but were lost in the
 * network before reaching the other party.
 * @apiSince REL
 */

public int getNumRtpPacketsTransmittedLost() { throw new RuntimeException("Stub!"); }

/**
 * Returns the number of RTP packets which were sent by the other party but were lost in the
 * network before reaching this device.
 * @apiSince REL
 */

public int getNumRtpPacketsNotReceived() { throw new RuntimeException("Stub!"); }

/**
 * Returns the average relative jitter in milliseconds. Jitter represents the amount of variance
 * in interarrival time of packets, for example, if two packets are sent 2 milliseconds apart
 * but received 3 milliseconds apart, the relative jitter between those packets is 1
 * millisecond.
 *
 * <p>See RFC 3550 for more information on jitter calculations.
 * @apiSince REL
 */

public int getAverageRelativeJitter() { throw new RuntimeException("Stub!"); }

/**
 * Returns the maximum relative jitter for a given ongoing call. Jitter represents the amount of
 * variance in interarrival time of packets, for example, if two packets are sent 2 milliseconds
 * apart but received 3 milliseconds apart, the relative jitter between those packets is 1
 * millisecond.
 *
 * <p>See RFC 3550 for more information on jitter calculations.
 * @apiSince REL
 */

public int getMaxRelativeJitter() { throw new RuntimeException("Stub!"); }

/**
 * Returns the average round trip time in milliseconds.
 * @apiSince REL
 */

public int getAverageRoundTripTime() { throw new RuntimeException("Stub!"); }

/**
 * Returns the codec type. This value corresponds to the AUDIO_QUALITY_* constants in
 * {@link ImsStreamMediaProfile}.
 *
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_NONE
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_AMR
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_AMR_WB
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_QCELP13K
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_EVRC
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_EVRC_B
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_EVRC_WB
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_EVRC_NW
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_GSM_EFR
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_GSM_FR
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_GSM_HR
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_G711U
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_G723
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_G711A
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_G722
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_G711AB
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_G729
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_EVS_NB
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_EVS_WB
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_EVS_SWB
 * @see ImsStreamMediaProfile#AUDIO_QUALITY_EVS_FB
 * @apiSince REL
 */

public int getCodecType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * {@link Parcelable#describeContents}
 
 * @return Value is either <code>0</code> or {@link android.os.Parcelable#CONTENTS_FILE_DESCRIPTOR}
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * {@link Parcelable#writeToParcel}
 
 * @param flags Value is either <code>0</code> or a combination of {@link android.os.Parcelable#PARCELABLE_WRITE_RETURN_VALUE}, and android.os.Parcelable.PARCELABLE_ELIDE_DUPLICATES
 * @apiSince REL
 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int CALL_QUALITY_BAD = 4; // 0x4

/** @apiSince REL */

public static final int CALL_QUALITY_EXCELLENT = 0; // 0x0

/** @apiSince REL */

public static final int CALL_QUALITY_FAIR = 2; // 0x2

/** @apiSince REL */

public static final int CALL_QUALITY_GOOD = 1; // 0x1

/** @apiSince REL */

public static final int CALL_QUALITY_NOT_AVAILABLE = 5; // 0x5

/** @apiSince REL */

public static final int CALL_QUALITY_POOR = 3; // 0x3

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.CallQuality> CREATOR;
static { CREATOR = null; }
}

