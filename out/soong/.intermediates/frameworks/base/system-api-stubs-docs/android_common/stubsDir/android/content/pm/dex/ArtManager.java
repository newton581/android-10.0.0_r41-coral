/**
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.pm.dex;

import android.content.Context;
import java.util.concurrent.Executor;
import android.os.ParcelFileDescriptor;

/**
 * Class for retrieving various kinds of information related to the runtime artifacts of
 * packages that are currently installed on the device.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ArtManager {

ArtManager() { throw new RuntimeException("Stub!"); }

/**
 * Snapshots a runtime profile according to the {@code profileType} parameter.
 *
 * If {@code profileType} is {@link ArtManager#PROFILE_APPS} the method will snapshot
 * the profile for for an apk belonging to the package {@code packageName}.
 * The apk is identified by {@code codePath}.
 *
 * If {@code profileType} is {@code ArtManager.PROFILE_BOOT_IMAGE} the method will snapshot
 * the profile for the boot image. In this case {@code codePath can be null}. The parameters
 * {@code packageName} and {@code codePath} are ignored.
 *u
 * The calling process must have {@code android.permission.READ_RUNTIME_PROFILE} permission.
 *
 * The result will be posted on the {@code executor} using the given {@code callback}.
 * The profile will be available as a read-only {@link android.os.ParcelFileDescriptor}.
 *
 * This method will throw {@link IllegalStateException} if
 * {@link ArtManager#isRuntimeProfilingEnabled(int)} does not return true for the given
 * {@code profileType}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_RUNTIME_PROFILES} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param profileType the type of profile that should be snapshot (boot image or app)
 * Value is either <code>0</code> or a combination of {@link android.content.pm.dex.ArtManager#PROFILE_APPS}, and {@link android.content.pm.dex.ArtManager#PROFILE_BOOT_IMAGE}
 * @param packageName the target package name or null if the target is the boot image
 * This value may be {@code null}.
 * @param codePath the code path for which the profile should be retrieved or null if
 *                 the target is the boot image
 * This value may be {@code null}.
 * @param callback the callback which should be used for the result
 * This value must never be {@code null}.
 * @param executor the executor which should be used to post the result
 
 * This value must never be {@code null}.
 
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @apiSince REL
 */

public void snapshotRuntimeProfile(int profileType, @android.annotation.Nullable java.lang.String packageName, @android.annotation.Nullable java.lang.String codePath, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.content.pm.dex.ArtManager.SnapshotRuntimeProfileCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if runtime profiles are enabled for the given type, false otherwise.
 *
 * The calling process must have {@code android.permission.READ_RUNTIME_PROFILE} permission.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_RUNTIME_PROFILES} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param profileType can be either {@link ArtManager#PROFILE_APPS}
 *                    or {@link ArtManager#PROFILE_BOOT_IMAGE}
 
 * Value is either <code>0</code> or a combination of {@link android.content.pm.dex.ArtManager#PROFILE_APPS}, and {@link android.content.pm.dex.ArtManager#PROFILE_BOOT_IMAGE}
 * @apiSince REL
 */

public boolean isRuntimeProfilingEnabled(int profileType) { throw new RuntimeException("Stub!"); }

/**
 * Constant used for applications profiles.
 * @apiSince REL
 */

public static final int PROFILE_APPS = 0; // 0x0

/**
 * Constant used for the boot image profile.
 * @apiSince REL
 */

public static final int PROFILE_BOOT_IMAGE = 1; // 0x1

/**
 * The snapshot failed because the package code path does not exist.
 * @apiSince REL
 */

public static final int SNAPSHOT_FAILED_CODE_PATH_NOT_FOUND = 1; // 0x1

/**
 * The snapshot failed because of an internal error (e.g. error during opening profiles).
 * @apiSince REL
 */

public static final int SNAPSHOT_FAILED_INTERNAL_ERROR = 2; // 0x2

/**
 * The snapshot failed because the package was not found.
 * @apiSince REL
 */

public static final int SNAPSHOT_FAILED_PACKAGE_NOT_FOUND = 0; // 0x0
/**
 * Callback used for retrieving runtime profiles.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class SnapshotRuntimeProfileCallback {

public SnapshotRuntimeProfileCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called when the profile snapshot finished with success.
 *
 * @param profileReadFd the file descriptor that can be used to read the profile. Note that
 *                      the file might be empty (which is valid profile).
 * @apiSince REL
 */

public abstract void onSuccess(android.os.ParcelFileDescriptor profileReadFd);

/**
 * Called when the profile snapshot finished with an error.
 *
 * @param errCode the error code {@see SNAPSHOT_FAILED_PACKAGE_NOT_FOUND,
 *      SNAPSHOT_FAILED_CODE_PATH_NOT_FOUND, SNAPSHOT_FAILED_INTERNAL_ERROR}.
 * @apiSince REL
 */

public abstract void onError(int errCode);
}

}

