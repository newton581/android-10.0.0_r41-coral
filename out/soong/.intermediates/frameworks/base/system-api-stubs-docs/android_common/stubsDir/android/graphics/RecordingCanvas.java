/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.graphics;


/**
 * A Canvas implementation that records view system drawing operations for deferred rendering.
 * This is used in combination with RenderNode. This class keeps a list of all the Paint and
 * Bitmap objects that it draws, preventing the backing memory of Bitmaps from being released while
 * the RecordingCanvas is still holding a native reference to the memory.
 *
 * This is obtained by calling {@link RenderNode#beginRecording()} and is valid until the matching
 * {@link RenderNode#endRecording()} is called. It must not be retained beyond that as it is
 * internally reused.
 * @apiSince 29
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class RecordingCanvas extends android.graphics.Canvas {

/** @hide */

RecordingCanvas(@android.annotation.NonNull android.graphics.RenderNode node, int width, int height) { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public void setDensity(int density) { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public boolean isHardwareAccelerated() { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public void setBitmap(android.graphics.Bitmap bitmap) { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public boolean isOpaque() { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public int getWidth() { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public int getHeight() { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public int getMaximumBitmapWidth() { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public int getMaximumBitmapHeight() { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public void enableZ() { throw new RuntimeException("Stub!"); }

/** @apiSince 29 */

public void disableZ() { throw new RuntimeException("Stub!"); }

/**
 * Draws the specified display list onto this canvas.
 *
 * @param renderNode The RenderNode to draw.
 
 * This value must never be {@code null}.
 * @apiSince 29
 */

public void drawRenderNode(@android.annotation.NonNull android.graphics.RenderNode renderNode) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param text This value must never be {@code null}.
 
 * @param path This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawTextOnPath(@android.annotation.NonNull char[] text, int index, int count, @android.annotation.NonNull android.graphics.Path path, float hOffset, float vOffset, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param text This value must never be {@code null}.
 
 * @param path This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawTextOnPath(@android.annotation.NonNull java.lang.String text, @android.annotation.NonNull android.graphics.Path path, float hOffset, float vOffset, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated checkstyle
 * @param text This value must never be {@code null}.
 
 * @param pos This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

@Deprecated
public final void drawPosText(@android.annotation.NonNull char[] text, int index, int count, @android.annotation.NonNull float[] pos, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated checkstyle
 * @param text This value must never be {@code null}.
 
 * @param pos This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

@Deprecated
public final void drawPosText(@android.annotation.NonNull java.lang.String text, @android.annotation.NonNull float[] pos, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param paint This value must never be {@code null}.
 */

public final void drawOval(float left, float top, float right, float bottom, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param oval This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawOval(@android.annotation.NonNull android.graphics.RectF oval, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param outer This value must never be {@code null}.
 
 * @param inner This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawDoubleRoundRect(@android.annotation.NonNull android.graphics.RectF outer, float outerRx, float outerRy, @android.annotation.NonNull android.graphics.RectF inner, float innerRx, float innerRy, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param outer This value must never be {@code null}.
 
 * @param inner This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawDoubleRoundRect(@android.annotation.NonNull android.graphics.RectF outer, float[] outerRadii, @android.annotation.NonNull android.graphics.RectF inner, float[] innerRadii, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param paint This value must never be {@code null}.
 */

public final void drawPoint(float x, float y, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param patch This value must never be {@code null}.
 
 * @param dst This value must never be {@code null}.

 * @param paint This value may be {@code null}.
 */

public final void drawPatch(@android.annotation.NonNull android.graphics.NinePatch patch, @android.annotation.NonNull android.graphics.Rect dst, @android.annotation.Nullable android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param patch This value must never be {@code null}.
 
 * @param dst This value must never be {@code null}.

 * @param paint This value may be {@code null}.
 */

public final void drawPatch(@android.annotation.NonNull android.graphics.NinePatch patch, @android.annotation.NonNull android.graphics.RectF dst, @android.annotation.Nullable android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param bitmap This value must never be {@code null}.

 * @param paint This value may be {@code null}.
 */

public final void drawBitmap(@android.annotation.NonNull android.graphics.Bitmap bitmap, float left, float top, @android.annotation.Nullable android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param bitmap This value must never be {@code null}.
 
 * @param matrix This value must never be {@code null}.

 * @param paint This value may be {@code null}.
 */

public final void drawBitmap(@android.annotation.NonNull android.graphics.Bitmap bitmap, @android.annotation.NonNull android.graphics.Matrix matrix, @android.annotation.Nullable android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param bitmap This value must never be {@code null}.

 * @param src This value may be {@code null}.
 
 * @param dst This value must never be {@code null}.

 * @param paint This value may be {@code null}.
 */

public final void drawBitmap(@android.annotation.NonNull android.graphics.Bitmap bitmap, @android.annotation.Nullable android.graphics.Rect src, @android.annotation.NonNull android.graphics.Rect dst, @android.annotation.Nullable android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param bitmap This value must never be {@code null}.

 * @param src This value may be {@code null}.
 
 * @param dst This value must never be {@code null}.

 * @param paint This value may be {@code null}.
 */

public final void drawBitmap(@android.annotation.NonNull android.graphics.Bitmap bitmap, @android.annotation.Nullable android.graphics.Rect src, @android.annotation.NonNull android.graphics.RectF dst, @android.annotation.Nullable android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated checkstyle
 * @param colors This value must never be {@code null}.

 * @param paint This value may be {@code null}.
 */

@Deprecated
public final void drawBitmap(@android.annotation.NonNull int[] colors, int offset, int stride, float x, float y, int width, int height, boolean hasAlpha, @android.annotation.Nullable android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated checkstyle
 * @param colors This value must never be {@code null}.

 * @param paint This value may be {@code null}.
 */

@Deprecated
public final void drawBitmap(@android.annotation.NonNull int[] colors, int offset, int stride, int x, int y, int width, int height, boolean hasAlpha, @android.annotation.Nullable android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

public final void drawColor(int color) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param mode This value must never be {@code null}.
 */

public final void drawColor(int color, @android.annotation.NonNull android.graphics.PorterDuff.Mode mode) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param mode This value must never be {@code null}.
 */

public final void drawColor(int color, @android.annotation.NonNull android.graphics.BlendMode mode) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param mode This value must never be {@code null}.
 */

public final void drawColor(long color, @android.annotation.NonNull android.graphics.BlendMode mode) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param paint This value must never be {@code null}.
 */

public final void drawPoints(float[] pts, int offset, int count, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param pts This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawPoints(@android.annotation.NonNull float[] pts, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param paint This value must never be {@code null}.
 */

public final void drawRoundRect(float left, float top, float right, float bottom, float rx, float ry, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param rect This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawRoundRect(@android.annotation.NonNull android.graphics.RectF rect, float rx, float ry, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param paint This value must never be {@code null}.
 */

public final void drawPaint(@android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param bitmap This value must never be {@code null}.

 * @param verts This value must never be {@code null}.
 
 * @param colors This value may be {@code null}.

 * @param paint This value may be {@code null}.
 */

public final void drawBitmapMesh(@android.annotation.NonNull android.graphics.Bitmap bitmap, int meshWidth, int meshHeight, @android.annotation.NonNull float[] verts, int vertOffset, @android.annotation.Nullable int[] colors, int colorOffset, @android.annotation.Nullable android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param text This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawTextRun(@android.annotation.NonNull char[] text, int index, int count, int contextIndex, int contextCount, float x, float y, boolean isRtl, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param text This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawTextRun(@android.annotation.NonNull java.lang.CharSequence text, int start, int end, int contextStart, int contextEnd, float x, float y, boolean isRtl, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param measuredText This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public void drawTextRun(@android.annotation.NonNull android.graphics.text.MeasuredText measuredText, int start, int end, int contextStart, int contextEnd, float x, float y, boolean isRtl, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param paint This value must never be {@code null}.
 */

public final void drawCircle(float cx, float cy, float radius, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param text This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawText(@android.annotation.NonNull char[] text, int index, int count, float x, float y, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param text This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawText(@android.annotation.NonNull java.lang.CharSequence text, int start, int end, float x, float y, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param text This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawText(@android.annotation.NonNull java.lang.String text, float x, float y, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param text This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawText(@android.annotation.NonNull java.lang.String text, int start, int end, float x, float y, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param paint This value must never be {@code null}.
 */

public final void drawArc(float left, float top, float right, float bottom, float startAngle, float sweepAngle, boolean useCenter, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param oval This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawArc(@android.annotation.NonNull android.graphics.RectF oval, float startAngle, float sweepAngle, boolean useCenter, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param paint This value must never be {@code null}.
 */

public final void drawLine(float startX, float startY, float stopX, float stopY, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param mode This value must never be {@code null}.

 * @param verts This value must never be {@code null}.
 
 * @param texs This value may be {@code null}.

 * @param colors This value may be {@code null}.
 
 * @param indices This value may be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawVertices(@android.annotation.NonNull android.graphics.Canvas.VertexMode mode, int vertexCount, @android.annotation.NonNull float[] verts, int vertOffset, @android.annotation.Nullable float[] texs, int texOffset, @android.annotation.Nullable int[] colors, int colorOffset, @android.annotation.Nullable short[] indices, int indexOffset, int indexCount, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

public final void drawRGB(int r, int g, int b) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param pts This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawLines(@android.annotation.NonNull float[] pts, int offset, int count, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param pts This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawLines(@android.annotation.NonNull float[] pts, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param picture This value must never be {@code null}.
 */

public final void drawPicture(@android.annotation.NonNull android.graphics.Picture picture) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param picture This value must never be {@code null}.

 * @param dst This value must never be {@code null}.
 */

public final void drawPicture(@android.annotation.NonNull android.graphics.Picture picture, @android.annotation.NonNull android.graphics.Rect dst) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param picture This value must never be {@code null}.

 * @param dst This value must never be {@code null}.
 */

public final void drawPicture(@android.annotation.NonNull android.graphics.Picture picture, @android.annotation.NonNull android.graphics.RectF dst) { throw new RuntimeException("Stub!"); }

public final void drawARGB(int a, int r, int g, int b) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param path This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawPath(@android.annotation.NonNull android.graphics.Path path, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param paint This value must never be {@code null}.
 */

public final void drawRect(float left, float top, float right, float bottom, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param r This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawRect(@android.annotation.NonNull android.graphics.Rect r, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param rect This value must never be {@code null}.

 * @param paint This value must never be {@code null}.
 */

public final void drawRect(@android.annotation.NonNull android.graphics.RectF rect, @android.annotation.NonNull android.graphics.Paint paint) { throw new RuntimeException("Stub!"); }
}

