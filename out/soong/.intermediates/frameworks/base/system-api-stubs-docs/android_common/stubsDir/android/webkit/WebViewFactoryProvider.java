/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.webkit;

import android.content.Context;
import android.content.Intent;
import java.util.List;

/**
 * This is the main entry-point into the WebView back end implementations, which the WebView
 * proxy class uses to instantiate all the other objects as needed. The backend must provide an
 * implementation of this interface, and make it available to the WebView via mechanism TBD.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface WebViewFactoryProvider {

/** @apiSince REL */

public android.webkit.WebViewFactoryProvider.Statics getStatics();

/**
 * Construct a new WebViewProvider.
 * @param webView the WebView instance bound to this implementation instance. Note it will not
 * necessarily be fully constructed at the point of this call: defer real initialization to
 * WebViewProvider.init().
 * @param privateAccess provides access into WebView internal methods.
 * @apiSince REL
 */

public android.webkit.WebViewProvider createWebView(android.webkit.WebView webView, android.webkit.WebView.PrivateAccess privateAccess);

/**
 * Gets the singleton GeolocationPermissions instance for this WebView implementation. The
 * implementation must return the same instance on subsequent calls.
 * @return the single GeolocationPermissions instance.
 * @apiSince REL
 */

public android.webkit.GeolocationPermissions getGeolocationPermissions();

/**
 * Gets the singleton CookieManager instance for this WebView implementation. The
 * implementation must return the same instance on subsequent calls.
 *
 * @return the singleton CookieManager instance
 * @apiSince REL
 */

public android.webkit.CookieManager getCookieManager();

/**
 * Gets the TokenBindingService instance for this WebView implementation. The
 * implementation must return the same instance on subsequent calls.
 *
 * @deprecated this method only returns {@code null}
 * @return the TokenBindingService instance (which is always {@code null})
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public android.webkit.TokenBindingService getTokenBindingService();

/**
 * Gets the TracingController instance for this WebView implementation. The
 * implementation must return the same instance on subsequent calls.
 *
 * @return the TracingController instance
 * @apiSince REL
 */

public android.webkit.TracingController getTracingController();

/**
 * Gets the ServiceWorkerController instance for this WebView implementation. The
 * implementation must return the same instance on subsequent calls.
 *
 * @return the ServiceWorkerController instance
 * @apiSince REL
 */

public android.webkit.ServiceWorkerController getServiceWorkerController();

/**
 * Gets the singleton WebIconDatabase instance for this WebView implementation. The
 * implementation must return the same instance on subsequent calls.
 *
 * @return the singleton WebIconDatabase instance
 * @apiSince REL
 */

public android.webkit.WebIconDatabase getWebIconDatabase();

/**
 * Gets the singleton WebStorage instance for this WebView implementation. The
 * implementation must return the same instance on subsequent calls.
 *
 * @return the singleton WebStorage instance
 * @apiSince REL
 */

public android.webkit.WebStorage getWebStorage();

/**
 * Gets the singleton WebViewDatabase instance for this WebView implementation. The
 * implementation must return the same instance on subsequent calls.
 *
 * @return the singleton WebViewDatabase instance
 * @apiSince REL
 */

public android.webkit.WebViewDatabase getWebViewDatabase(android.content.Context context);

/**
 * Gets the classloader used to load internal WebView implementation classes. This interface
 * should only be used by the WebView Support Library.
 * @apiSince REL
 */

public java.lang.ClassLoader getWebViewClassLoader();
/**
 * This Interface provides glue for implementing the backend of WebView static methods which
 * cannot be implemented in-situ in the proxy class.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface Statics {

/**
 * Implements the API method:
 * {@link android.webkit.WebView#findAddress(String)}
 * @apiSince REL
 */

public java.lang.String findAddress(java.lang.String addr);

/**
 * Implements the API method:
 * {@link android.webkit.WebSettings#getDefaultUserAgent(Context) }
 * @apiSince REL
 */

public java.lang.String getDefaultUserAgent(android.content.Context context);

/**
 * Used for tests only.
 * @apiSince REL
 */

public void freeMemoryForTests();

/**
 * Implements the API method:
 * {@link android.webkit.WebView#setWebContentsDebuggingEnabled(boolean) }
 * @apiSince REL
 */

public void setWebContentsDebuggingEnabled(boolean enable);

/**
 * Implements the API method:
 * {@link android.webkit.WebView#clearClientCertPreferences(Runnable) }
 * @apiSince REL
 */

public void clearClientCertPreferences(java.lang.Runnable onCleared);

/**
 * Implements the API method:
 * {@link android.webkit.WebView#setSlowWholeDocumentDrawEnabled(boolean) }
 * @apiSince REL
 */

public void enableSlowWholeDocumentDraw();

/**
 * Implement the API method
 * {@link android.webkit.WebChromeClient.FileChooserParams#parseResult(int, Intent)}
 * @apiSince REL
 */

public android.net.Uri[] parseFileChooserResult(int resultCode, android.content.Intent intent);

/**
 * Implement the API method
 * {@link android.webkit.WebView#startSafeBrowsing(Context , ValueCallback<Boolean>)}
 * @apiSince REL
 */

public void initSafeBrowsing(android.content.Context context, android.webkit.ValueCallback<java.lang.Boolean> callback);

/**
 * Implement the API method
 * {@link android.webkit.WebView#setSafeBrowsingWhitelist(List<String>,
 * ValueCallback<Boolean>)}
 * @apiSince REL
 */

public void setSafeBrowsingWhitelist(java.util.List<java.lang.String> hosts, android.webkit.ValueCallback<java.lang.Boolean> callback);

/**
 * Implement the API method
 * {@link android.webkit.WebView#getSafeBrowsingPrivacyPolicyUrl()}
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.Uri getSafeBrowsingPrivacyPolicyUrl();
}

}

