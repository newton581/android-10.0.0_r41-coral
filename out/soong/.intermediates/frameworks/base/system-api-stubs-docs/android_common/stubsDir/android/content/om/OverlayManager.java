/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.om;

import android.os.UserHandle;

/**
 * Updates OverlayManager state; gets information about installed overlay packages.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class OverlayManager {

/** @hide */

OverlayManager(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Request that an overlay package is enabled and any other overlay packages with the same
 * target package and category are disabled.
 *
 * If a set of overlay packages share the same category, single call to this method is
 * equivalent to multiple calls to {@link #setEnabled(String, boolean, UserHandle)}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS} or {@link android.Manifest.permission#INTERACT_ACROSS_USERS_FULL}
 * @param packageName the name of the overlay package to enable.
 * This value must never be {@code null}.
 * @param user The user for which to change the overlay.
 *
 * This value must never be {@code null}.
 * @hide
 */

public void setEnabledExclusiveInCategory(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Request that an overlay package is enabled or disabled.
 *
 * While {@link #setEnabledExclusiveInCategory(String, UserHandle)} doesn't support disabling
 * every overlay in a category, this method allows you to disable everything.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS} or {@link android.Manifest.permission#INTERACT_ACROSS_USERS_FULL}
 * @param packageName the name of the overlay package to enable.
 * This value must never be {@code null}.
 * @param enable {@code false} if the overlay should be turned off.
 * @param user The user for which to change the overlay.
 *
 * This value must never be {@code null}.
 * @hide
 */

public void setEnabled(@android.annotation.NonNull java.lang.String packageName, boolean enable, @android.annotation.NonNull android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Returns information about the overlay with the given package name for
 * the specified user.
 *
 * @param packageName The name of the package.
 * This value must never be {@code null}.
 * @param userHandle The user to get the OverlayInfos for.
 * This value must never be {@code null}.
 * @return An OverlayInfo object; if no overlays exist with the
 *         requested package name, null is returned.
 *
 * @hide
 */

@android.annotation.Nullable
public android.content.om.OverlayInfo getOverlayInfo(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull android.os.UserHandle userHandle) { throw new RuntimeException("Stub!"); }

/**
 * Returns information about all overlays for the given target package for
 * the specified user. The returned list is ordered according to the
 * overlay priority with the highest priority at the end of the list.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS} or {@link android.Manifest.permission#INTERACT_ACROSS_USERS_FULL}
 * @param targetPackageName The name of the target package.
 * This value must never be {@code null}.
 * @param user The user to get the OverlayInfos for.
 * This value must never be {@code null}.
 * @return A list of OverlayInfo objects; if no overlays exist for the
 *         requested package, an empty list is returned.
 *
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public java.util.List<android.content.om.OverlayInfo> getOverlayInfosForTarget(@android.annotation.NonNull java.lang.String targetPackageName, @android.annotation.NonNull android.os.UserHandle user) { throw new RuntimeException("Stub!"); }
}

