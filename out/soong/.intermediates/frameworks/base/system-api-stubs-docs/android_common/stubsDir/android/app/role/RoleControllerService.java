/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.role;

import java.util.concurrent.Executor;
import android.os.UserHandle;
import android.os.RemoteCallback;
import android.content.Intent;

/**
 * Abstract base class for the role controller service.
 * <p>
 * Subclass should implement the business logic for role management, including enforcing role
 * requirements and granting or revoking relevant privileges of roles. This class can only be
 * implemented by the permission controller app which is registered in {@code PackageManager}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class RoleControllerService extends android.app.Service {

public RoleControllerService() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onCreate() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onDestroy() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param intent This value may be {@code null}.
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public final android.os.IBinder onBind(@android.annotation.Nullable android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Called by system to grant default permissions and roles.
 * <p>
 * This is typically when creating a new user or upgrading either system or
 * permission controller package
 *
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * @return whether this call was successful
 * @apiSince REL
 */

public abstract boolean onGrantDefaultRoles();

/**
 * Add a specific application to the holders of a role. If the role is exclusive, the previous
 * holder will be replaced.
 * <p>
 * Implementation should enforce the role requirements and grant or revoke the relevant
 * privileges of roles.
 *
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * @param roleName the name of the role to add the role holder for
 * This value must never be {@code null}.
 * @param packageName the package name of the application to add to the role holders
 * This value must never be {@code null}.
 * @param flags optional behavior flags
 *
 * Value is either <code>0</code> or {@link android.app.role.RoleManager#MANAGE_HOLDERS_FLAG_DONT_KILL_APP}
 * @return whether this call was successful
 *
 * @see RoleManager#addRoleHolderAsUser(String, String, int, UserHandle, Executor,
 *      RemoteCallback)
 * @apiSince REL
 */

public abstract boolean onAddRoleHolder(@android.annotation.NonNull java.lang.String roleName, @android.annotation.NonNull java.lang.String packageName, int flags);

/**
 * Remove a specific application from the holders of a role.
 *
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * @param roleName the name of the role to remove the role holder for
 * This value must never be {@code null}.
 * @param packageName the package name of the application to remove from the role holders
 * This value must never be {@code null}.
 * @param flags optional behavior flags
 *
 * Value is either <code>0</code> or {@link android.app.role.RoleManager#MANAGE_HOLDERS_FLAG_DONT_KILL_APP}
 * @return whether this call was successful
 *
 * @see RoleManager#removeRoleHolderAsUser(String, String, int, UserHandle, Executor,
 *      RemoteCallback)
 * @apiSince REL
 */

public abstract boolean onRemoveRoleHolder(@android.annotation.NonNull java.lang.String roleName, @android.annotation.NonNull java.lang.String packageName, int flags);

/**
 * Remove all holders of a role.
 *
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * @param roleName the name of the role to remove role holders for
 * This value must never be {@code null}.
 * @param flags optional behavior flags
 *
 * Value is either <code>0</code> or {@link android.app.role.RoleManager#MANAGE_HOLDERS_FLAG_DONT_KILL_APP}
 * @return whether this call was successful
 *
 * @see RoleManager#clearRoleHoldersAsUser(String, int, UserHandle, Executor, RemoteCallback)
 * @apiSince REL
 */

public abstract boolean onClearRoleHolders(@android.annotation.NonNull java.lang.String roleName, int flags);

/**
 * Check whether an application is qualified for a role.
 *
 * @param roleName name of the role to check for
 * This value must never be {@code null}.
 * @param packageName package name of the application to check for
 *
 * This value must never be {@code null}.
 * @return whether the application is qualified for the role
 * @apiSince REL
 */

public abstract boolean onIsApplicationQualifiedForRole(@android.annotation.NonNull java.lang.String roleName, @android.annotation.NonNull java.lang.String packageName);

/**
 * Check whether a role should be visible to user.
 *
 * @param roleName name of the role to check for
 *
 * This value must never be {@code null}.
 * @return whether the role should be visible to user
 * @apiSince REL
 */

public abstract boolean onIsRoleVisible(@android.annotation.NonNull java.lang.String roleName);

/**
 * The {@link Intent} that must be declared as handled by the service.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.app.role.RoleControllerService";
}

