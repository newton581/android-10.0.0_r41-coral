/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;


/**
 * Gives access to the system properties store.  The system properties
 * store contains a list of string key-value pairs.
 *
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class SystemProperties {

SystemProperties() { throw new RuntimeException("Stub!"); }

/**
 * Get the String value for the given {@code key}.
 *
 * @param key the key to lookup
 * This value must never be {@code null}.
 * @return an empty string if the {@code key} isn't found
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public static java.lang.String get(@android.annotation.NonNull java.lang.String key) { throw new RuntimeException("Stub!"); }

/**
 * Get the String value for the given {@code key}.
 *
 * @param key the key to lookup
 * This value must never be {@code null}.
 * @param def the default value in case the property is not set or empty
 * This value may be {@code null}.
 * @return if the {@code key} isn't found, return {@code def} if it isn't null, or an empty
 * string otherwise
 * @hide
 */

@android.annotation.NonNull
public static java.lang.String get(@android.annotation.NonNull java.lang.String key, @android.annotation.Nullable java.lang.String def) { throw new RuntimeException("Stub!"); }

/**
 * Get the value for the given {@code key}, and return as an integer.
 *
 * @param key the key to lookup
 * This value must never be {@code null}.
 * @param def a default value to return
 * @return the key parsed as an integer, or def if the key isn't found or
 *         cannot be parsed
 * @hide
 */

public static int getInt(@android.annotation.NonNull java.lang.String key, int def) { throw new RuntimeException("Stub!"); }

/**
 * Get the value for the given {@code key}, and return as a long.
 *
 * @param key the key to lookup
 * This value must never be {@code null}.
 * @param def a default value to return
 * @return the key parsed as a long, or def if the key isn't found or
 *         cannot be parsed
 * @hide
 */

public static long getLong(@android.annotation.NonNull java.lang.String key, long def) { throw new RuntimeException("Stub!"); }

/**
 * Get the value for the given {@code key}, returned as a boolean.
 * Values 'n', 'no', '0', 'false' or 'off' are considered false.
 * Values 'y', 'yes', '1', 'true' or 'on' are considered true.
 * (case sensitive).
 * If the key does not exist, or has any other value, then the default
 * result is returned.
 *
 * @param key the key to lookup
 * This value must never be {@code null}.
 * @param def a default value to return
 * @return the key parsed as a boolean, or def if the key isn't found or is
 *         not able to be parsed as a boolean.
 * @hide
 */

public static boolean getBoolean(@android.annotation.NonNull java.lang.String key, boolean def) { throw new RuntimeException("Stub!"); }
}

