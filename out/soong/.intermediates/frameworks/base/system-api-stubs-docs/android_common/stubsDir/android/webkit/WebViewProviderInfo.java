/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.webkit;


/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class WebViewProviderInfo implements android.os.Parcelable {

/** @apiSince REL */

public WebViewProviderInfo(java.lang.String packageName, java.lang.String description, boolean availableByDefault, boolean isFallback, java.lang.String[] signatures) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.webkit.WebViewProviderInfo> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public final boolean availableByDefault;
{ availableByDefault = false; }

/** @apiSince REL */

public final java.lang.String description;
{ description = null; }

/** @apiSince REL */

public final boolean isFallback;
{ isFallback = false; }

/** @apiSince REL */

public final java.lang.String packageName;
{ packageName = null; }

/** @apiSince REL */

public final android.content.pm.Signature[] signatures;
{ signatures = new android.content.pm.Signature[0]; }
}

