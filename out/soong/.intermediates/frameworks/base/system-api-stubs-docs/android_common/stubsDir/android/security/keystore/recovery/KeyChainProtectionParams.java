/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.security.keystore.recovery;


/**
 * A {@link KeyChainSnapshot} is protected with a key derived from the user's lock screen. This
 * class wraps all the data necessary to derive the same key on a recovering device:
 *
 * <ul>
 *     <li>UI parameters for the user's lock screen - so that if e.g., the user was using a pattern,
 *         the recovering device can display the pattern UI to the user when asking them to enter
 *         the lock screen from their previous device.
 *     <li>The algorithm used to derive a key from the user's lock screen, e.g. SHA-256 with a salt.
 * </ul>
 *
 * <p>As such, this data is sent along with the {@link KeyChainSnapshot} when syncing the current
 * version of the keychain.
 *
 * <p>For now, the recoverable keychain only supports a single layer of protection, which is the
 * user's lock screen. In the future, the keychain will support multiple layers of protection
 * (e.g. an additional keychain password, along with the lock screen).
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class KeyChainProtectionParams implements android.os.Parcelable {

KeyChainProtectionParams() { throw new RuntimeException("Stub!"); }

/**
 * @see TYPE_LOCKSCREEN
 
 * @return Value is {@link android.security.keystore.recovery.KeyChainProtectionParams#TYPE_LOCKSCREEN}
 * @apiSince REL
 */

public int getUserSecretType() { throw new RuntimeException("Stub!"); }

/**
 * Specifies UX shown to user during recovery.
 * Default value is {@code UI_FORMAT_LOCKSCREEN}
 *
 * @see UI_FORMAT_PIN
 * @see UI_FORMAT_PASSWORD
 * @see UI_FORMAT_PATTERN
 
 * @return Value is {@link android.security.keystore.recovery.KeyChainProtectionParams#UI_FORMAT_PIN}, {@link android.security.keystore.recovery.KeyChainProtectionParams#UI_FORMAT_PASSWORD}, or {@link android.security.keystore.recovery.KeyChainProtectionParams#UI_FORMAT_PATTERN}
 * @apiSince REL
 */

public int getLockScreenUiFormat() { throw new RuntimeException("Stub!"); }

/**
 * Specifies function used to derive symmetric key from user input
 * Format is defined in separate util class.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.security.keystore.recovery.KeyDerivationParams getKeyDerivationParams() { throw new RuntimeException("Stub!"); }

/**
 * Secret derived from user input.
 * Default value is empty array
 *
 * @return secret or empty array
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public byte[] getSecret() { throw new RuntimeException("Stub!"); }

/**
 * Fills secret with zeroes.
 * @apiSince REL
 */

public void clearSecret() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.security.keystore.recovery.KeyChainProtectionParams> CREATOR;
static { CREATOR = null; }

/**
 * Lockscreen secret is required to recover KeyStore.
 * @apiSince REL
 */

public static final int TYPE_LOCKSCREEN = 100; // 0x64

/**
 * Password. String with latin-1 characters only.
 * @apiSince REL
 */

public static final int UI_FORMAT_PASSWORD = 2; // 0x2

/**
 * Pattern with 3 by 3 grid.
 * @apiSince REL
 */

public static final int UI_FORMAT_PATTERN = 3; // 0x3

/**
 * Pin with digits only.
 * @apiSince REL
 */

public static final int UI_FORMAT_PIN = 1; // 0x1
/**
 * Builder for creating {@link KeyChainProtectionParams}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Sets user secret type.
 * Default value is {@link TYPE_LOCKSCREEN}.
 *
 * @see TYPE_LOCKSCREEN
 * @param userSecretType The secret type
 * Value is {@link android.security.keystore.recovery.KeyChainProtectionParams#TYPE_LOCKSCREEN}
 * @return This builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.security.keystore.recovery.KeyChainProtectionParams.Builder setUserSecretType(int userSecretType) { throw new RuntimeException("Stub!"); }

/**
 * Sets UI format.
 *
 * @see UI_FORMAT_PIN
 * @see UI_FORMAT_PASSWORD
 * @see UI_FORMAT_PATTERN
 * @param lockScreenUiFormat The UI format
 * Value is {@link android.security.keystore.recovery.KeyChainProtectionParams#UI_FORMAT_PIN}, {@link android.security.keystore.recovery.KeyChainProtectionParams#UI_FORMAT_PASSWORD}, or {@link android.security.keystore.recovery.KeyChainProtectionParams#UI_FORMAT_PATTERN}
 * @return This builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.security.keystore.recovery.KeyChainProtectionParams.Builder setLockScreenUiFormat(int lockScreenUiFormat) { throw new RuntimeException("Stub!"); }

/**
 * Sets parameters of the key derivation function.
 *
 * @param keyDerivationParams Key derivation parameters
 * This value must never be {@code null}.
 * @return This builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.security.keystore.recovery.KeyChainProtectionParams.Builder setKeyDerivationParams(@android.annotation.NonNull android.security.keystore.recovery.KeyDerivationParams keyDerivationParams) { throw new RuntimeException("Stub!"); }

/**
 * Secret derived from user input, or empty array.
 *
 * @param secret The secret.
 * This value must never be {@code null}.
 * @return This builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.security.keystore.recovery.KeyChainProtectionParams.Builder setSecret(@android.annotation.NonNull byte[] secret) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new {@link KeyChainProtectionParams} instance.
 * The instance will include default values, if {@link #setSecret}
 * or {@link #setUserSecretType} were not called.
 *
 * @return new instance
 * This value will never be {@code null}.
 * @throws NullPointerException if some required fields were not set.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.security.keystore.recovery.KeyChainProtectionParams build() { throw new RuntimeException("Stub!"); }
}

}

