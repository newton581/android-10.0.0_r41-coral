/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app;


/**
 * Display properties to be used by VR mode when creating a virtual display.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class Vr2dDisplayProperties implements android.os.Parcelable {

/** @apiSince REL */

public Vr2dDisplayProperties(int width, int height, int dpi) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Prints out dump info.
 
 * @param pw This value must never be {@code null}.
 
 * @param prefix This value must never be {@code null}.
 * @apiSince REL
 */

public void dump(@android.annotation.NonNull java.io.PrintWriter pw, @android.annotation.NonNull java.lang.String prefix) { throw new RuntimeException("Stub!"); }

/**
 * Returns the width of VR 2d display.
 * @apiSince REL
 */

public int getWidth() { throw new RuntimeException("Stub!"); }

/**
 * Returns the height of VR 2d display.
 * @apiSince REL
 */

public int getHeight() { throw new RuntimeException("Stub!"); }

/**
 * Returns the dpi of VR 2d display.
 * @apiSince REL
 */

public int getDpi() { throw new RuntimeException("Stub!"); }

/**
 * Returns the added flags of VR 2d display. Flags are combined by logic or.
 
 * @return Value is {@link android.app.Vr2dDisplayProperties#FLAG_VIRTUAL_DISPLAY_ENABLED}
 * @apiSince REL
 */

public int getAddedFlags() { throw new RuntimeException("Stub!"); }

/**
 * Returns the removed flags of VR 2d display. Flags are combined by logic or.
 
 * @return Value is {@link android.app.Vr2dDisplayProperties#FLAG_VIRTUAL_DISPLAY_ENABLED}
 * @apiSince REL
 */

public int getRemovedFlags() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.Vr2dDisplayProperties> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public static final int FLAG_VIRTUAL_DISPLAY_ENABLED = 1; // 0x1
/**
 * Convenience class for creating Vr2dDisplayProperties.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/** @apiSince REL */

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Sets the dimensions to use for the virtual display.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.Vr2dDisplayProperties.Builder setDimensions(int width, int height, int dpi) { throw new RuntimeException("Stub!"); }

/**
 * Toggles the virtual display functionality for 2D activities in VR.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.Vr2dDisplayProperties.Builder setEnabled(boolean enabled) { throw new RuntimeException("Stub!"); }

/**
 * Adds property flags.
 
 * @param flags Value is {@link android.app.Vr2dDisplayProperties#FLAG_VIRTUAL_DISPLAY_ENABLED}
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.Vr2dDisplayProperties.Builder addFlags(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Removes property flags.
 
 * @param flags Value is {@link android.app.Vr2dDisplayProperties#FLAG_VIRTUAL_DISPLAY_ENABLED}
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.Vr2dDisplayProperties.Builder removeFlags(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Builds the Vr2dDisplayProperty instance.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.Vr2dDisplayProperties build() { throw new RuntimeException("Stub!"); }
}

}

