/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.metrics;


/**
 * Helper class to assemble more complex logs.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class LogMaker {

/**
 * @param category for the new LogMaker.
 * @apiSince REL
 */

public LogMaker(int category) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public LogMaker(java.lang.Object[] items) { throw new RuntimeException("Stub!"); }

/**
 * @param category to replace the existing setting.
 * @apiSince REL
 */

public android.metrics.LogMaker setCategory(int category) { throw new RuntimeException("Stub!"); }

/**
 * Set the category to unknown.
 * @apiSince REL
 */

public android.metrics.LogMaker clearCategory() { throw new RuntimeException("Stub!"); }

/**
 * @param type to replace the existing setting.
 * @apiSince REL
 */

public android.metrics.LogMaker setType(int type) { throw new RuntimeException("Stub!"); }

/**
 * Set the type to unknown.
 * @apiSince REL
 */

public android.metrics.LogMaker clearType() { throw new RuntimeException("Stub!"); }

/**
 * @param subtype to replace the existing setting.
 * @apiSince REL
 */

public android.metrics.LogMaker setSubtype(int subtype) { throw new RuntimeException("Stub!"); }

/**
 * Set the subtype to 0.
 * @apiSince REL
 */

public android.metrics.LogMaker clearSubtype() { throw new RuntimeException("Stub!"); }

/**
 * @param packageName to replace the existing setting.
 * @apiSince REL
 */

public android.metrics.LogMaker setPackageName(java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Remove the package name property.
 * @apiSince REL
 */

public android.metrics.LogMaker clearPackageName() { throw new RuntimeException("Stub!"); }

/**
 * @param tag From your MetricsEvent enum.
 * @param value One of Integer, Long, Float, or String; or null to clear the tag.
 * @return modified LogMaker
 * @apiSince REL
 */

public android.metrics.LogMaker addTaggedData(int tag, java.lang.Object value) { throw new RuntimeException("Stub!"); }

/**
 * Remove a value from the LogMaker.
 *
 * @param tag From your MetricsEvent enum.
 * @return modified LogMaker
 * @apiSince REL
 */

public android.metrics.LogMaker clearTaggedData(int tag) { throw new RuntimeException("Stub!"); }

/**
 * @return true if this object may be added to a LogMaker as a value.
 * @apiSince REL
 */

public boolean isValidValue(java.lang.Object value) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.Object getTaggedData(int tag) { throw new RuntimeException("Stub!"); }

/**
 * @return the category of the log, or unknown.
 * @apiSince REL
 */

public int getCategory() { throw new RuntimeException("Stub!"); }

/**
 * @return the type of the log, or unknwon.
 * @apiSince REL
 */

public int getType() { throw new RuntimeException("Stub!"); }

/**
 * @return the subtype of the log, or 0.
 * @apiSince REL
 */

public int getSubtype() { throw new RuntimeException("Stub!"); }

/**
 * @return the timestamp of the log.or 0
 * @apiSince REL
 */

public long getTimestamp() { throw new RuntimeException("Stub!"); }

/**
 * @return the package name of the log, or null.
 * @apiSince REL
 */

public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * @return the process ID of the log, or -1.
 * @apiSince REL
 */

public int getProcessId() { throw new RuntimeException("Stub!"); }

/**
 * @return the UID of the log, or -1.
 * @apiSince REL
 */

public int getUid() { throw new RuntimeException("Stub!"); }

/**
 * @return the name of the counter, or null.
 * @apiSince REL
 */

public java.lang.String getCounterName() { throw new RuntimeException("Stub!"); }

/**
 * @return the bucket label of the histogram\, or 0.
 * @apiSince REL
 */

public long getCounterBucket() { throw new RuntimeException("Stub!"); }

/**
 * @return true if the bucket label was specified as a long integer.
 * @apiSince REL
 */

public boolean isLongCounterBucket() { throw new RuntimeException("Stub!"); }

/**
 * @return the increment value of the counter, or 0.
 * @apiSince REL
 */

public int getCounterValue() { throw new RuntimeException("Stub!"); }

/**
 * @return a representation of the log suitable for EventLog.
 * @apiSince REL
 */

public java.lang.Object[] serialize() { throw new RuntimeException("Stub!"); }

/**
 * Reconstitute an object from the output of {@link #serialize()}.
 * @apiSince REL
 */

public void deserialize(java.lang.Object[] items) { throw new RuntimeException("Stub!"); }

/**
 * @param that the object to compare to.
 * @return true if values in that equal values in this, for tags that exist in this.
 * @apiSince REL
 */

public boolean isSubsetOf(android.metrics.LogMaker that) { throw new RuntimeException("Stub!"); }
}

