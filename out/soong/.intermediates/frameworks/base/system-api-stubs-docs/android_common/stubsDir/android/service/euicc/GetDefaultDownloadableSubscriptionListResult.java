/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.euicc;

import android.telephony.euicc.DownloadableSubscription;

/**
 * Result of a {@link EuiccService#onGetDefaultDownloadableSubscriptionList} operation.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class GetDefaultDownloadableSubscriptionListResult implements android.os.Parcelable {

/**
 * Construct a new {@link GetDefaultDownloadableSubscriptionListResult}.
 *
 * @param result Result of the operation. May be one of the predefined {@code RESULT_} constants
 *     in EuiccService or any implementation-specific code starting with
 *     {@link EuiccService#RESULT_FIRST_USER}.
 * @param subscriptions The available subscriptions. Should only be provided if the result is
 *     {@link EuiccService#RESULT_OK}.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public GetDefaultDownloadableSubscriptionListResult(int result, @android.annotation.Nullable android.telephony.euicc.DownloadableSubscription[] subscriptions) { throw new RuntimeException("Stub!"); }

/**
 * Gets the result of the operation.
 *
 * <p>May be one of the predefined {@code RESULT_} constants in EuiccService or any
 * implementation-specific code starting with {@link EuiccService#RESULT_FIRST_USER}.
 * @apiSince REL
 */

public int getResult() { throw new RuntimeException("Stub!"); }

/**
 * Gets the available {@link DownloadableSubscription}s (with filled-in metadata).
 *
 * <p>Only non-null if {@link #result} is {@link EuiccService#RESULT_OK}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.util.List<android.telephony.euicc.DownloadableSubscription> getDownloadableSubscriptions() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.euicc.GetDefaultDownloadableSubscriptionListResult> CREATOR;
static { CREATOR = null; }
}

