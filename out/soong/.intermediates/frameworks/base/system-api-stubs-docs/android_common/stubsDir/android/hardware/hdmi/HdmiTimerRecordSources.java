/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.hdmi;

import android.hardware.hdmi.HdmiRecordSources.DigitalServiceSource;

/**
 * Container for timer record source used for timer recording. Timer source consists of two parts,
 * timer info and record source.
 * <p>
 * Timer info contains all timing information used for recording. It consists of the following
 * values.
 * <ul>
 * <li>[Day of Month]
 * <li>[Month of Year]
 * <li>[Start Time]
 * <li>[Duration]
 * <li>[Recording Sequence]
 * </ul>
 * <p>
 * Record source containers all program information used for recording.
 * For more details, look at {@link HdmiRecordSources}.
 * <p>
 * Usage
 * <pre>
 * TimeOrDuration startTime = HdmiTimerRecordSources.ofTime(18, 00);  // 6PM.
 * TimeOrDuration duration = HdmiTimerRecordSource.ofDuration(1, 00);  // 1 hour duration.
 * // For 1 hour from 6PM, August 10th every SaturDay and Sunday.
 * TimerInfo timerInfo = HdmiTimerRecordSource.timerInfoOf(10, 8, starTime, duration,
 *            HdmiTimerRecordSource.RECORDING_SEQUENCE_REPEAT_SATURDAY |
 *            HdmiTimerRecordSource.RECORDING_SEQUENCE_REPEAT_SUNDAY);
 * // create digital source.
 * DigitalServiceSource recordSource = HdmiRecordSource.ofDvb(...);
 * TimerRecordSource source = ofDigitalSource(timerInfo, recordSource);
 * </pre>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HdmiTimerRecordSources {

HdmiTimerRecordSources() { throw new RuntimeException("Stub!"); }

/**
 * Creates {@link TimerRecordSource} for digital source which is used for &lt;Set Digital
 * Timer&gt;.
 *
 * @param timerInfo timer info used for timer recording
 * @param source digital source used for timer recording
 * @return {@link TimerRecordSource}
 * @throws IllegalArgumentException if {@code timerInfo} or {@code source} is null
 * @apiSince REL
 */

public static android.hardware.hdmi.HdmiTimerRecordSources.TimerRecordSource ofDigitalSource(android.hardware.hdmi.HdmiTimerRecordSources.TimerInfo timerInfo, android.hardware.hdmi.HdmiRecordSources.DigitalServiceSource source) { throw new RuntimeException("Stub!"); }

/**
 * Creates {@link TimerRecordSource} for analogue source which is used for &lt;Set Analogue
 * Timer&gt;.
 *
 * @param timerInfo timer info used for timer recording
 * @param source digital source used for timer recording
 * @return {@link TimerRecordSource}
 * @throws IllegalArgumentException if {@code timerInfo} or {@code source} is null
 * @apiSince REL
 */

public static android.hardware.hdmi.HdmiTimerRecordSources.TimerRecordSource ofAnalogueSource(android.hardware.hdmi.HdmiTimerRecordSources.TimerInfo timerInfo, android.hardware.hdmi.HdmiRecordSources.AnalogueServiceSource source) { throw new RuntimeException("Stub!"); }

/**
 * Creates {@link TimerRecordSource} for external plug which is used for &lt;Set External
 * Timer&gt;.
 *
 * @param timerInfo timer info used for timer recording
 * @param source digital source used for timer recording
 * @return {@link TimerRecordSource}
 * @throws IllegalArgumentException if {@code timerInfo} or {@code source} is null
 * @apiSince REL
 */

public static android.hardware.hdmi.HdmiTimerRecordSources.TimerRecordSource ofExternalPlug(android.hardware.hdmi.HdmiTimerRecordSources.TimerInfo timerInfo, android.hardware.hdmi.HdmiRecordSources.ExternalPlugData source) { throw new RuntimeException("Stub!"); }

/**
 * Creates {@link TimerRecordSource} for external physical address which is used for &lt;Set
 * External Timer&gt;.
 *
 * @param timerInfo timer info used for timer recording
 * @param source digital source used for timer recording
 * @return {@link TimerRecordSource}
 * @throws IllegalArgumentException if {@code timerInfo} or {@code source} is null
 * @apiSince REL
 */

public static android.hardware.hdmi.HdmiTimerRecordSources.TimerRecordSource ofExternalPhysicalAddress(android.hardware.hdmi.HdmiTimerRecordSources.TimerInfo timerInfo, android.hardware.hdmi.HdmiRecordSources.ExternalPhysicalAddress source) { throw new RuntimeException("Stub!"); }

/**
 * Creates {@link Duration} for time value.
 *
 * @param hour hour in range of [0, 23]
 * @param minute minute in range of [0, 60]
 * @return {@link Duration}
 * @throws IllegalArgumentException if hour or minute is out of range
 * @apiSince REL
 */

public static android.hardware.hdmi.HdmiTimerRecordSources.Time timeOf(int hour, int minute) { throw new RuntimeException("Stub!"); }

/**
 * Creates {@link Duration} for duration value.
 *
 * @param hour hour in range of [0, 99]
 * @param minute minute in range of [0, 59]
 * @return {@link Duration}
 * @throws IllegalArgumentException if hour or minute is out of range
 * @apiSince REL
 */

public static android.hardware.hdmi.HdmiTimerRecordSources.Duration durationOf(int hour, int minute) { throw new RuntimeException("Stub!"); }

/**
 * Creates {@link TimerInfo} with the given information.
 *
 * @param dayOfMonth day of month
 * @param monthOfYear month of year
 * @param startTime start time in {@link Time}
 * @param duration duration in {@link Duration}
 * @param recordingSequence recording sequence. Use RECORDING_SEQUENCE_REPEAT_ONCE_ONLY for no
 *            repeat. Otherwise use combination of {@link #RECORDING_SEQUENCE_REPEAT_SUNDAY},
 *            {@link #RECORDING_SEQUENCE_REPEAT_MONDAY},
 *            {@link #RECORDING_SEQUENCE_REPEAT_TUESDAY},
 *            {@link #RECORDING_SEQUENCE_REPEAT_WEDNESDAY},
 *            {@link #RECORDING_SEQUENCE_REPEAT_THURSDAY},
 *            {@link #RECORDING_SEQUENCE_REPEAT_FRIDAY},
 *            {@link #RECORDING_SEQUENCE_REPEAT_SATUREDAY}.
 * @return {@link TimerInfo}.
 * @throws IllegalArgumentException if input value is invalid
 * @apiSince REL
 */

public static android.hardware.hdmi.HdmiTimerRecordSources.TimerInfo timerInfoOf(int dayOfMonth, int monthOfYear, android.hardware.hdmi.HdmiTimerRecordSources.Time startTime, android.hardware.hdmi.HdmiTimerRecordSources.Duration duration, int recordingSequence) { throw new RuntimeException("Stub!"); }

/**
 * Checks the byte array of timer record source.
 * @param sourcetype
 * @param recordSource
 * @hide
 */

public static boolean checkTimerRecordSource(int sourcetype, byte[] recordSource) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int RECORDING_SEQUENCE_REPEAT_FRIDAY = 32; // 0x20

/** @apiSince REL */

public static final int RECORDING_SEQUENCE_REPEAT_MONDAY = 2; // 0x2

/**
 * Fields for recording sequence.
 * The following can be merged by OR(|) operation.
 * @apiSince REL
 */

public static final int RECORDING_SEQUENCE_REPEAT_ONCE_ONLY = 0; // 0x0

/** @apiSince REL */

public static final int RECORDING_SEQUENCE_REPEAT_SATUREDAY = 64; // 0x40

/** @apiSince REL */

public static final int RECORDING_SEQUENCE_REPEAT_SUNDAY = 1; // 0x1

/** @apiSince REL */

public static final int RECORDING_SEQUENCE_REPEAT_THURSDAY = 16; // 0x10

/** @apiSince REL */

public static final int RECORDING_SEQUENCE_REPEAT_TUESDAY = 4; // 0x4

/** @apiSince REL */

public static final int RECORDING_SEQUENCE_REPEAT_WEDNESDAY = 8; // 0x8
/**
 * Place holder for duration value.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Duration {

Duration(int hour, int minute) { throw new RuntimeException("Stub!"); }
}

/**
 * Place holder for time value.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Time {

Time(int hour, int minute) { throw new RuntimeException("Stub!"); }
}

/**
 * Container basic timer information. It consists of the following fields.
 * <ul>
 * <li>[Day of Month]
 * <li>[Month of Year]
 * <li>[Start Time]
 * <li>[Duration]
 * <li>[Recording Sequence]
 * </ul>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class TimerInfo {

TimerInfo(int dayOfMonth, int monthOfYear, android.hardware.hdmi.HdmiTimerRecordSources.Time startTime, android.hardware.hdmi.HdmiTimerRecordSources.Duration duration, int recordingSequence) { throw new RuntimeException("Stub!"); }
}

/**
 * Record source container for timer record. This is used to set parameter for &lt;Set Digital
 * Timer&gt;, &lt;Set Analogue Timer&gt;, and &lt;Set External Timer&gt; message.
 * <p>
 * In order to create this from each source type, use one of helper method.
 * <ul>
 * <li>{@link #ofDigitalSource} for digital source
 * <li>{@link #ofAnalogueSource} for analogue source
 * <li>{@link #ofExternalPlug} for external plug type
 * <li>{@link #ofExternalPhysicalAddress} for external physical address type.
 * </ul>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class TimerRecordSource {

TimerRecordSource(android.hardware.hdmi.HdmiTimerRecordSources.TimerInfo timerInfo, android.hardware.hdmi.HdmiRecordSources.RecordSource recordSource) { throw new RuntimeException("Stub!"); }
}

}

