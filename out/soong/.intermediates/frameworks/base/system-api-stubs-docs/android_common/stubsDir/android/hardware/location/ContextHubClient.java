/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.location;

import android.app.PendingIntent;

/**
 * A class describing a client of the Context Hub Service.
 *
 * Clients can send messages to nanoapps at a Context Hub through this object. The APIs supported
 * by this object are thread-safe and can be used without external synchronization.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ContextHubClient implements java.io.Closeable {

ContextHubClient(android.hardware.location.ContextHubInfo hubInfo, boolean persistent) { throw new RuntimeException("Stub!"); }

/**
 * Returns the hub that this client is attached to.
 *
 * @return the ContextHubInfo of the attached hub
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.ContextHubInfo getAttachedHub() { throw new RuntimeException("Stub!"); }

/**
 * Closes the connection for this client and the Context Hub Service.
 *
 * When this function is invoked, the messaging associated with this client is invalidated.
 * All futures messages targeted for this client are dropped at the service, and the
 * ContextHubClient is unregistered from the service.
 *
 * If this object has a PendingIntent, i.e. the object was generated via
 * {@link ContextHubManager.createClient(PendingIntent, ContextHubInfo, long)}, then the
 * Intent events corresponding to the PendingIntent will no longer be triggered.
 * @apiSince REL
 */

public void close() { throw new RuntimeException("Stub!"); }

/**
 * Sends a message to a nanoapp through the Context Hub Service.
 *
 * This function returns RESULT_SUCCESS if the message has reached the HAL, but
 * does not guarantee delivery of the message to the target nanoapp.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param message the message object to send
 *
 * This value must never be {@code null}.
 * @return the result of sending the message defined as in ContextHubTransaction.Result
 *
 * Value is {@link android.hardware.location.ContextHubTransaction#RESULT_SUCCESS}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_UNKNOWN}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_BAD_PARAMS}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_UNINITIALIZED}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_BUSY}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_AT_HUB}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_TIMEOUT}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_SERVICE_INTERNAL_FAILURE}, or {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_HAL_UNAVAILABLE}
 * @throws NullPointerException if NanoAppMessage is null
 *
 * @see NanoAppMessage
 * @see ContextHubTransaction.Result
 * @apiSince REL
 */

public int sendMessageToNanoApp(@android.annotation.NonNull android.hardware.location.NanoAppMessage message) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

protected void finalize() throws java.lang.Throwable { throw new RuntimeException("Stub!"); }
}

