/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.location;


/**
 * Holds the characteristics of the reflecting plane that a satellite signal has bounced from.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class GnssReflectingPlane implements android.os.Parcelable {

GnssReflectingPlane(android.location.GnssReflectingPlane.Builder builder) { throw new RuntimeException("Stub!"); }

/**
 * Gets the latitude in degrees of the reflecting plane.
 * @return Value is between -90.0f and 90.0f inclusive
 * @apiSince REL
 */

public double getLatitudeDegrees() { throw new RuntimeException("Stub!"); }

/**
 * Gets the longitude in degrees of the reflecting plane.
 * @return Value is between -180.0f and 180.0f inclusive
 * @apiSince REL
 */

public double getLongitudeDegrees() { throw new RuntimeException("Stub!"); }

/**
 * Gets the altitude in meters above the WGS 84 reference ellipsoid of the reflecting point
 * within the plane
 
 * @return Value is between -1000.0f and 10000.0f inclusive
 * @apiSince REL
 */

public double getAltitudeMeters() { throw new RuntimeException("Stub!"); }

/**
 * Gets the azimuth clockwise from north of the reflecting plane in degrees.
 * @return Value is between 0.0f and 360.0f inclusive
 * @apiSince REL
 */

public double getAzimuthDegrees() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param parcel This value must never be {@code null}.
 * @apiSince REL
 */

public void writeToParcel(@android.annotation.NonNull android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final android.os.Parcelable.Creator<android.location.GnssReflectingPlane> CREATOR;
static { CREATOR = null; }
/**
 * Builder for {@link GnssReflectingPlane}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Sets the latitude in degrees of the reflecting plane.
 * @param latitudeDegrees Value is between -90.0f and 90.0f inclusive
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssReflectingPlane.Builder setLatitudeDegrees(double latitudeDegrees) { throw new RuntimeException("Stub!"); }

/**
 * Sets the longitude in degrees of the reflecting plane.
 * @param longitudeDegrees Value is between -180.0f and 180.0f inclusive
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssReflectingPlane.Builder setLongitudeDegrees(double longitudeDegrees) { throw new RuntimeException("Stub!"); }

/**
 * Sets the altitude in meters above the WGS 84 reference ellipsoid of the reflecting point
 * within the plane
 
 * @param altitudeMeters Value is between -1000.0f and 10000.0f inclusive
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssReflectingPlane.Builder setAltitudeMeters(double altitudeMeters) { throw new RuntimeException("Stub!"); }

/**
 * Sets the azimuth clockwise from north of the reflecting plane in degrees.
 * @param azimuthDegrees Value is between 0.0f and 360.0f inclusive
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssReflectingPlane.Builder setAzimuthDegrees(double azimuthDegrees) { throw new RuntimeException("Stub!"); }

/**
 * Builds a {@link GnssReflectingPlane} object as specified by this builder.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssReflectingPlane build() { throw new RuntimeException("Stub!"); }
}

}

