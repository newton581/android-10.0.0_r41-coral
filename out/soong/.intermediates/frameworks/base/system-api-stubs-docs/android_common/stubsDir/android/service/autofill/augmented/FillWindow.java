/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill.augmented;

import android.service.autofill.augmented.PresentationParams.Area;
import android.view.View;

/**
 * Handle to a window used to display the augmented autofill UI.
 *
 * <p>The steps to create an augmented autofill UI are:
 *
 * <ol>
 *   <li>Gets the {@link PresentationParams} from the {@link FillRequest}.
 *   <li>Gets the {@link Area} to display the UI (for example, through
 *   {@link PresentationParams#getSuggestionArea()}.
 *   <li>Creates a {@link View} that must fit in the {@link Area#getBounds() area boundaries}.
 *   <li>Set the proper listeners to the view (for example, a click listener that
 *   triggers {@link FillController#autofill(java.util.List)}
 *   <li>Call {@link #update(Area, View, long)} with these arguments.
 *   <li>Create a {@link FillResponse} with the {@link FillWindow}.
 *   <li>Pass such {@link FillResponse} to {@link FillCallback#onSuccess(FillResponse)}.
 * </ol>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class FillWindow implements java.lang.AutoCloseable {

public FillWindow() { throw new RuntimeException("Stub!"); }

/**
 * Updates the content of the window.
 *
 * @param rootView new root view
 * This value must never be {@code null}.
 * @param area coordinates to render the view.
 * This value must never be {@code null}.
 * @param flags currently not used.
 *
 * @return boolean whether the window was updated or not.
 *
 * @throws IllegalArgumentException if the area is not compatible with this window
 * @apiSince REL
 */

public boolean update(@android.annotation.NonNull android.service.autofill.augmented.PresentationParams.Area area, @android.annotation.NonNull android.view.View rootView, long flags) { throw new RuntimeException("Stub!"); }

/**
 * Destroys the window.
 *
 * <p>Once destroyed, this window cannot be used anymore
 * @apiSince REL
 */

public void destroy() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

protected void finalize() throws java.lang.Throwable { throw new RuntimeException("Stub!"); }

/** @hide */

public void close() throws java.lang.Exception { throw new RuntimeException("Stub!"); }
}

