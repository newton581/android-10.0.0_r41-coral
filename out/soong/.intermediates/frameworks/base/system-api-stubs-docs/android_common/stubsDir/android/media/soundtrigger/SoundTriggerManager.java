/**
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.soundtrigger;

import java.util.UUID;
import android.os.Handler;
import android.hardware.soundtrigger.SoundTrigger;

/**
 * This class provides management of non-voice (general sound trigger) based sound recognition
 * models. Usage of this class is restricted to system or signature applications only. This allows
 * OEMs to write apps that can manage non-voice based sound trigger models.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class SoundTriggerManager {

SoundTriggerManager() { throw new RuntimeException("Stub!"); }

/**
 * Updates the given sound trigger model.
 
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_SOUND_TRIGGER}
 * @apiSince REL
 */

public void updateModel(android.media.soundtrigger.SoundTriggerManager.Model model) { throw new RuntimeException("Stub!"); }

/**
 * Returns the sound trigger model represented by the given UUID. An instance of {@link Model}
 * is returned.
 
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_SOUND_TRIGGER}
 * @apiSince REL
 */

public android.media.soundtrigger.SoundTriggerManager.Model getModel(java.util.UUID soundModelId) { throw new RuntimeException("Stub!"); }

/**
 * Deletes the sound model represented by the provided UUID.
 
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_SOUND_TRIGGER}
 * @apiSince REL
 */

public void deleteModel(java.util.UUID soundModelId) { throw new RuntimeException("Stub!"); }

/**
 * Creates an instance of {@link SoundTriggerDetector} which can be used to start/stop
 * recognition on the model and register for triggers from the model. Note that this call
 * invalidates any previously returned instances for the same sound model Uuid.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_SOUND_TRIGGER}
 * @param soundModelId UUID of the sound model to create the receiver object for.
 * @param callback Instance of the {@link SoundTriggerDetector#Callback} object for the
 * callbacks for the given sound model.
 * This value must never be {@code null}.
 * @param handler The Handler to use for the callback operations. A null value will use the
 * current thread's Looper.
 * This value may be {@code null}.
 * @return Instance of {@link SoundTriggerDetector} or null on error.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.media.soundtrigger.SoundTriggerDetector createSoundTriggerDetector(java.util.UUID soundModelId, @android.annotation.NonNull android.media.soundtrigger.SoundTriggerDetector.Callback callback, @android.annotation.Nullable android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * Get the amount of time (in milliseconds) an operation of the
 * {@link ISoundTriggerDetectionService} is allowed to ask.
 *
 * @return The amount of time an sound trigger detection service operation is allowed to last
 * @apiSince REL
 */

public int getDetectionServiceOperationsTimeout() { throw new RuntimeException("Stub!"); }
/**
 * Class captures the data and fields that represent a non-keyphrase sound model. Use the
 * factory constructor {@link Model#create()} to create an instance.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Model {

Model() { throw new RuntimeException("Stub!"); }

/**
 * Factory constructor to create a SoundModel instance for use with methods in this
 * class.
 * @apiSince REL
 */

public static android.media.soundtrigger.SoundTriggerManager.Model create(java.util.UUID modelUuid, java.util.UUID vendorUuid, byte[] data) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.util.UUID getModelUuid() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.util.UUID getVendorUuid() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public byte[] getModelData() { throw new RuntimeException("Stub!"); }
}

}

