/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.settings.suggestions;


/**
 * This is the base class for implementing suggestion service. A suggestion service is responsible
 * to provide a collection of {@link Suggestion}s for the current user when queried.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class SuggestionService extends android.app.Service {

public SuggestionService() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Return all available suggestions.
 * @apiSince REL
 */

public abstract java.util.List<android.service.settings.suggestions.Suggestion> onGetSuggestions();

/**
 * Dismiss a suggestion. The suggestion will not be included in future
 * {@link #onGetSuggestions()} calls.
 * @apiSince REL
 */

public abstract void onSuggestionDismissed(android.service.settings.suggestions.Suggestion suggestion);

/**
 * This is the opposite signal to {@link #onSuggestionDismissed}, indicating a suggestion has
 * been launched.
 * @apiSince REL
 */

public abstract void onSuggestionLaunched(android.service.settings.suggestions.Suggestion suggestion);
}

