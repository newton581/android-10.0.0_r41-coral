/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.tv;


/**
 * Simple container for information about TV input hardware.
 * Not for third-party developers.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class TvInputHardwareInfo implements android.os.Parcelable {

TvInputHardwareInfo() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getDeviceId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getAudioType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getAudioAddress() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getHdmiPortId() { throw new RuntimeException("Stub!"); }

/**
 * Gets the cable connection status of the hardware.
 *
 * @return {@code CABLE_CONNECTION_STATUS_CONNECTED} if cable is connected.
 *         {@code CABLE_CONNECTION_STATUS_DISCONNECTED} if cable is disconnected.
 *         {@code CABLE_CONNECTION_STATUS_UNKNOWN} if the hardware is unsure about the
 *         connection status or does not support cable detection.
 
 * Value is {@link android.media.tv.TvInputHardwareInfo#CABLE_CONNECTION_STATUS_UNKNOWN}, {@link android.media.tv.TvInputHardwareInfo#CABLE_CONNECTION_STATUS_CONNECTED}, or {@link android.media.tv.TvInputHardwareInfo#CABLE_CONNECTION_STATUS_DISCONNECTED}
 * @apiSince REL
 */

public int getCableConnectionStatus() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void readFromParcel(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/**
 * Cable is connected to the hardware.
 * @apiSince REL
 */

public static final int CABLE_CONNECTION_STATUS_CONNECTED = 1; // 0x1

/**
 * Cable is disconnected to the hardware.
 * @apiSince REL
 */

public static final int CABLE_CONNECTION_STATUS_DISCONNECTED = 2; // 0x2

/**
 * The hardware is unsure about the connection status or does not support cable detection.
 * @apiSince REL
 */

public static final int CABLE_CONNECTION_STATUS_UNKNOWN = 0; // 0x0

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.media.tv.TvInputHardwareInfo> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public static final int TV_INPUT_TYPE_COMPONENT = 6; // 0x6

/** @apiSince REL */

public static final int TV_INPUT_TYPE_COMPOSITE = 3; // 0x3

/** @apiSince REL */

public static final int TV_INPUT_TYPE_DISPLAY_PORT = 10; // 0xa

/** @apiSince REL */

public static final int TV_INPUT_TYPE_DVI = 8; // 0x8

/** @apiSince REL */

public static final int TV_INPUT_TYPE_HDMI = 9; // 0x9

/** @apiSince REL */

public static final int TV_INPUT_TYPE_OTHER_HARDWARE = 1; // 0x1

/** @apiSince REL */

public static final int TV_INPUT_TYPE_SCART = 5; // 0x5

/** @apiSince REL */

public static final int TV_INPUT_TYPE_SVIDEO = 4; // 0x4

/** @apiSince REL */

public static final int TV_INPUT_TYPE_TUNER = 2; // 0x2

/** @apiSince REL */

public static final int TV_INPUT_TYPE_VGA = 7; // 0x7
/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/** @apiSince REL */

public Builder() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvInputHardwareInfo.Builder deviceId(int deviceId) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvInputHardwareInfo.Builder type(int type) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvInputHardwareInfo.Builder audioType(int audioType) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvInputHardwareInfo.Builder audioAddress(java.lang.String audioAddress) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvInputHardwareInfo.Builder hdmiPortId(int hdmiPortId) { throw new RuntimeException("Stub!"); }

/**
 * Sets cable connection status.
 
 * @param cableConnectionStatus Value is {@link android.media.tv.TvInputHardwareInfo#CABLE_CONNECTION_STATUS_UNKNOWN}, {@link android.media.tv.TvInputHardwareInfo#CABLE_CONNECTION_STATUS_CONNECTED}, or {@link android.media.tv.TvInputHardwareInfo#CABLE_CONNECTION_STATUS_DISCONNECTED}
 * @apiSince REL
 */

public android.media.tv.TvInputHardwareInfo.Builder cableConnectionStatus(int cableConnectionStatus) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvInputHardwareInfo build() { throw new RuntimeException("Stub!"); }
}

}

