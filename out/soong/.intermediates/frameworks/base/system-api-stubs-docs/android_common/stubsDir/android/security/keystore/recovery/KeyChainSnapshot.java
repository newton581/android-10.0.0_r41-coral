/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.security.keystore.recovery;

import java.security.cert.CertPath;
import java.util.List;
import java.security.cert.CertificateException;

/**
 * A snapshot of a version of the keystore. Two events can trigger the generation of a new snapshot:
 *
 * <ul>
 *     <li>The user's lock screen changes. (A key derived from the user's lock screen is used to
 *         protected the keychain, which is why this forces a new snapshot.)
 *     <li>A key is added to or removed from the recoverable keychain.
 * </ul>
 *
 * <p>The snapshot data is also encrypted with the remote trusted hardware's public key, so even
 * the recovery agent itself should not be able to decipher the data. The recovery agent sends an
 * instance of this to the remote trusted hardware whenever a new snapshot is generated. During a
 * recovery flow, the recovery agent retrieves a snapshot from the remote trusted hardware. It then
 * sends it to the framework, where it is decrypted using the user's lock screen from their previous
 * device.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class KeyChainSnapshot implements android.os.Parcelable {

/**
 * Use builder to create an instance of the class.
 */

KeyChainSnapshot() { throw new RuntimeException("Stub!"); }

/**
 * Snapshot version for given recovery agent. It is incremented when user secret or list of
 * application keys changes.
 * @apiSince REL
 */

public int getSnapshotVersion() { throw new RuntimeException("Stub!"); }

/**
 * Number of user secret guesses allowed during KeyChain recovery.
 * @apiSince REL
 */

public int getMaxAttempts() { throw new RuntimeException("Stub!"); }

/**
 * CounterId which is rotated together with user secret.
 * @apiSince REL
 */

public long getCounterId() { throw new RuntimeException("Stub!"); }

/**
 * Server parameters.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public byte[] getServerParams() { throw new RuntimeException("Stub!"); }

/**
 * CertPath containing the public key used to encrypt {@code encryptedRecoveryKeyBlob}.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.security.cert.CertPath getTrustedHardwareCertPath() { throw new RuntimeException("Stub!"); }

/**
 * UI and key derivation parameters. Note that combination of secrets may be used.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.security.keystore.recovery.KeyChainProtectionParams> getKeyChainProtectionParams() { throw new RuntimeException("Stub!"); }

/**
 * List of application keys, with key material encrypted by
 * the recovery key ({@link #getEncryptedRecoveryKeyBlob}).
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.security.keystore.recovery.WrappedApplicationKey> getWrappedApplicationKeys() { throw new RuntimeException("Stub!"); }

/**
 * Recovery key blob, encrypted by user secret and recovery service public key.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public byte[] getEncryptedRecoveryKeyBlob() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.security.keystore.recovery.KeyChainSnapshot> CREATOR;
static { CREATOR = null; }
}

