/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telecom;

import java.util.List;
import android.content.Intent;

/**
 * Base class for service that allows system apps to suggest phone accounts for outgoing calls.
 *
 * Phone account suggestions allow OEMs to intelligently select phone accounts based on knowledge
 * about the user's past behavior, carrier billing patterns, or other factors unknown to the AOSP
 * Telecom system.
 * OEMs who wish to provide a phone account suggestion service on their device should implement this
 * service in an app that resides in the /system/priv-app/ directory on their device. For security
 * reasons, the service's entry {@code AndroidManifest.xml} file must declare the
 * {@link android.Manifest.permission.BIND_PHONE_ACCOUNT_SUGGESTION_SERVICE} permission:
 * <pre>
 * {@code
 * <service android:name="your.package.YourServiceName"
 *          android:permission="android.permission.BIND_PHONE_ACCOUNT_SUGGESTION_SERVICE">
 *      <intent-filter>
 *          <action android:name="android.telecom.PhoneAccountSuggestionService"/>
 *      </intent-filter>
 * </service>
 * }
 * </pre>
 * Only one system app on each device may implement this service. If multiple system apps implement
 * this service, none of them will be queried for suggestions.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class PhoneAccountSuggestionService extends android.app.Service {

public PhoneAccountSuggestionService() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * The system calls this method during the outgoing call flow if it needs account suggestions.
 *
 * The implementer of this service must override this method to implement its account suggestion
 * logic. After preparing the suggestions, the implementation of the service must call
 * {@link #suggestPhoneAccounts(String, List)} to deliver the suggestions back to the system.
 *
 * Note that the system will suspend the outgoing call process after it calls this method until
 * this service calls {@link #suggestPhoneAccounts}.
 *
 * @param number The phone number to provide suggestions for.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onAccountSuggestionRequest(@android.annotation.NonNull java.lang.String number) { throw new RuntimeException("Stub!"); }

/**
 * The implementation of this service calls this method to deliver suggestions to the system.
 *
 * The implementation of this service must call this method after receiving a call to
 * {@link #onAccountSuggestionRequest(String)}. If no suggestions are available, pass an empty
 * list as the {@code suggestions} argument.
 *
 * @param number The phone number to provide suggestions for.
 * This value must never be {@code null}.
 * @param suggestions The list of suggestions.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public final void suggestPhoneAccounts(@android.annotation.NonNull java.lang.String number, @android.annotation.NonNull java.util.List<android.telecom.PhoneAccountSuggestion> suggestions) { throw new RuntimeException("Stub!"); }

/**
 * The {@link Intent} that must be declared in the {@code intent-filter} element of the
 * service's manifest entry.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.telecom.PhoneAccountSuggestionService";
}

