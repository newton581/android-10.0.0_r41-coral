/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.location;


/**
 * A class representing a GNSS measurement corrections for all used GNSS satellites at the location
 * and time specified
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class GnssMeasurementCorrections implements android.os.Parcelable {

GnssMeasurementCorrections(android.location.GnssMeasurementCorrections.Builder builder) { throw new RuntimeException("Stub!"); }

/**
 * Gets the latitude in degrees at which the corrections are computed.
 * @return Value is between -90.0f and 90.0f inclusive
 * @apiSince REL
 */

public double getLatitudeDegrees() { throw new RuntimeException("Stub!"); }

/**
 * Gets the longitude in degrees at which the corrections are computed.
 * @return Value is between -180.0f and 180.0f inclusive
 * @apiSince REL
 */

public double getLongitudeDegrees() { throw new RuntimeException("Stub!"); }

/**
 * Gets the altitude in meters above the WGS 84 reference ellipsoid at which the corrections are
 * computed.
 
 * @return Value is between -1000.0f and 10000.0f inclusive
 * @apiSince REL
 */

public double getAltitudeMeters() { throw new RuntimeException("Stub!"); }

/**
 * Gets the horizontal uncertainty (68% confidence) in meters on the device position at
 * which the corrections are provided.
 
 * @return Value is 0.0f or greater
 * @apiSince REL
 */

public double getHorizontalPositionUncertaintyMeters() { throw new RuntimeException("Stub!"); }

/**
 * Gets the vertical uncertainty (68% confidence) in meters on the device position at
 * which the corrections are provided.
 
 * @return Value is 0.0f or greater
 * @apiSince REL
 */

public double getVerticalPositionUncertaintyMeters() { throw new RuntimeException("Stub!"); }

/**
 * Gets the time of applicability, GPS time of week in nanoseconds.
 * @return Value is 0 or greater
 * @apiSince REL
 */

public long getToaGpsNanosecondsOfWeek() { throw new RuntimeException("Stub!"); }

/**
 * Gets a set of {@link GnssSingleSatCorrection} each containing measurement corrections for a
 * satellite in view
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.location.GnssSingleSatCorrection> getSingleSatelliteCorrectionList() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param parcel This value must never be {@code null}.
 * @apiSince REL
 */

public void writeToParcel(@android.annotation.NonNull android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final android.os.Parcelable.Creator<android.location.GnssMeasurementCorrections> CREATOR;
static { CREATOR = null; }
/**
 * Builder for {@link GnssMeasurementCorrections}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Sets the latitude in degrees at which the corrections are computed.
 * @param latitudeDegrees Value is between -90.0f and 90.0f inclusive
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssMeasurementCorrections.Builder setLatitudeDegrees(double latitudeDegrees) { throw new RuntimeException("Stub!"); }

/**
 * Sets the longitude in degrees at which the corrections are computed.
 * @param longitudeDegrees Value is between -180.0f and 180.0f inclusive
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssMeasurementCorrections.Builder setLongitudeDegrees(double longitudeDegrees) { throw new RuntimeException("Stub!"); }

/**
 * Sets the altitude in meters above the WGS 84 reference ellipsoid at which the corrections
 * are computed.
 
 * @param altitudeMeters Value is between -1000.0f and 10000.0f inclusive
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssMeasurementCorrections.Builder setAltitudeMeters(double altitudeMeters) { throw new RuntimeException("Stub!"); }

/**
 * Sets the horizontal uncertainty (68% confidence) in meters on the device position at
 * which the corrections are provided.
 
 * @param horizontalPositionUncertaintyMeters Value is 0.0f or greater
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssMeasurementCorrections.Builder setHorizontalPositionUncertaintyMeters(double horizontalPositionUncertaintyMeters) { throw new RuntimeException("Stub!"); }

/**
 * Sets the vertical uncertainty (68% confidence) in meters on the device position at which
 * the corrections are provided.
 
 * @param verticalPositionUncertaintyMeters Value is 0.0f or greater
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssMeasurementCorrections.Builder setVerticalPositionUncertaintyMeters(double verticalPositionUncertaintyMeters) { throw new RuntimeException("Stub!"); }

/**
 * Sets the time of applicability, GPS time of week in nanoseconds.
 * @param toaGpsNanosecondsOfWeek Value is 0 or greater
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssMeasurementCorrections.Builder setToaGpsNanosecondsOfWeek(long toaGpsNanosecondsOfWeek) { throw new RuntimeException("Stub!"); }

/**
 * Sets a the list of {@link GnssSingleSatCorrection} containing measurement corrections for
 * a satellite in view
 
 * @param singleSatCorrectionList This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssMeasurementCorrections.Builder setSingleSatelliteCorrectionList(@android.annotation.NonNull java.util.List<android.location.GnssSingleSatCorrection> singleSatCorrectionList) { throw new RuntimeException("Stub!"); }

/**
 * Builds a {@link GnssMeasurementCorrections} instance as specified by this builder.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssMeasurementCorrections build() { throw new RuntimeException("Stub!"); }
}

}

