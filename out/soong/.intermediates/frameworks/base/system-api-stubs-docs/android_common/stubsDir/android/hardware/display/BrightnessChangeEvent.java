/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.display;


/**
 * Data about a brightness settings change.
 *
 * {@see DisplayManager.getBrightnessEvents()}
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class BrightnessChangeEvent implements android.os.Parcelable {

BrightnessChangeEvent(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.display.BrightnessChangeEvent> CREATOR;
static { CREATOR = null; }

/**
 * Most recent battery level when brightness was changed or Float.NaN
 * @apiSince REL
 */

public final float batteryLevel;
{ batteryLevel = 0; }

/**
 * Brightness in nits
 * @apiSince REL
 */

public final float brightness;
{ brightness = 0; }

/**
 * How many milliseconds of data are contained in the colorValueBuckets, if the device does
 * not support color sampling the value will be 0L.
 *
 * {@see #colorValueBuckets}
 * @apiSince REL
 */

public final long colorSampleDuration;
{ colorSampleDuration = 0; }

/**
 * If night mode color filter is active this will be the temperature in kelvin
 * @apiSince REL
 */

public final int colorTemperature;
{ colorTemperature = 0; }

/**
 * Histogram counting how many times a pixel of a given value was displayed onscreen for the
 * Value component of HSV if the device supports color sampling, if the device does not support
 * color sampling the value will be null.
 *
 * The buckets of the histogram are evenly weighted, the number of buckets is device specific.
 * The units are in pixels * milliseconds, with 1 pixel millisecond being 1 pixel displayed
 * for 1 millisecond.
 * For example if we had {100, 50, 30, 20}, value component was onscreen for 100 pixel
 * milliseconds in range 0x00->0x3F, 30 pixel milliseconds in range 0x40->0x7F, etc.
 *
 * {@see #colorSampleDuration}
 * @apiSince REL
 */

@android.annotation.Nullable public final long[] colorValueBuckets;
{ colorValueBuckets = new long[0]; }

/**
 * Whether brightness configuration is default version
 * @apiSince REL
 */

public final boolean isDefaultBrightnessConfig;
{ isDefaultBrightnessConfig = false; }

/**
 * Whether brightness curve includes a user brightness point
 * @apiSince REL
 */

public final boolean isUserSetBrightness;
{ isUserSetBrightness = false; }

/**
 * Brightness level before slider adjustment
 * @apiSince REL
 */

public final float lastBrightness;
{ lastBrightness = 0; }

/**
 * Timestamps of the lux sensor readings {@see System.currentTimeMillis()}
 * @apiSince REL
 */

public final long[] luxTimestamps;
{ luxTimestamps = new long[0]; }

/**
 * Lux values of recent sensor data
 * @apiSince REL
 */

public final float[] luxValues;
{ luxValues = new float[0]; }

/**
 * Color filter active to provide night mode
 * @apiSince REL
 */

public final boolean nightMode;
{ nightMode = false; }

/** Package name of focused activity when brightness was changed.
 *  This will be null if the caller of {@see DisplayManager.getBrightnessEvents()}
 *  does not have access to usage stats {@see UsageStatsManager}     * @apiSince REL
 */

public final java.lang.String packageName;
{ packageName = null; }

/**
 * Factor applied to brightness due to battery level, 0.0-1.0
 * @apiSince REL
 */

public final float powerBrightnessFactor;
{ powerBrightnessFactor = 0; }

/**
 * Timestamp of the change {@see System.currentTimeMillis()}
 * @apiSince REL
 */

public final long timeStamp;
{ timeStamp = 0; }
}

