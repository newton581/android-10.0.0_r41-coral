/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.permission;

import java.util.concurrent.Executor;
import java.util.List;
import android.content.Context;
import android.os.AsyncTask;
import android.Manifest;
import java.util.Map;
import android.os.Handler;
import java.util.function.Consumer;
import android.os.UserHandle;

/**
 * Interface for communicating with the permission controller.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class PermissionControllerManager {

/**
 * Create a new {@link PermissionControllerManager}.
 *
 * @param context to create the manager for
 * @param handler handler to schedule work
 *
 * @hide
 */

PermissionControllerManager(@android.annotation.NonNull android.content.Context context, @android.annotation.NonNull android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * Revoke a set of runtime permissions for various apps.
 *
 * <br>
 * Requires {@link android.Manifest.permission#REVOKE_RUNTIME_PERMISSIONS}
 * @param request The permissions to revoke as {@code Map<packageName, List<permission>>}
 * This value must never be {@code null}.
 * @param doDryRun Compute the permissions that would be revoked, but not actually revoke them
 * @param reason Why the permission should be revoked
 * Value is {@link android.permission.PermissionControllerManager#REASON_MALWARE}, or {@link android.permission.PermissionControllerManager#REASON_INSTALLER_POLICY_VIOLATION}
 * @param executor Executor on which to invoke the callback
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback Callback to receive the result
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void revokeRuntimePermissions(@android.annotation.NonNull java.util.Map<java.lang.String,java.util.List<java.lang.String>> request, boolean doDryRun, int reason, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.permission.PermissionControllerManager.OnRevokeRuntimePermissionsCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Count an app only if the permission is granted to the app.
 * @apiSince REL
 */

public static final int COUNT_ONLY_WHEN_GRANTED = 1; // 0x1

/**
 * Count and app even if it is a system app.
 * @apiSince REL
 */

public static final int COUNT_WHEN_SYSTEM = 2; // 0x2

/**
 * The permissions are revoked because the apps holding the permissions violate a policy of the
 * app that installed it.
 *
 * <p>If this reason is used only permissions of apps that are installed by the caller of the
 * API can be revoked.
 * @apiSince REL
 */

public static final int REASON_INSTALLER_POLICY_VIOLATION = 2; // 0x2

/**
 * The permissions are revoked because the apps holding the permissions are malware
 * @apiSince REL
 */

public static final int REASON_MALWARE = 1; // 0x1
/**
 * Callback for delivering the result of {@link #revokeRuntimePermissions}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class OnRevokeRuntimePermissionsCallback {

public OnRevokeRuntimePermissionsCallback() { throw new RuntimeException("Stub!"); }

/**
 * The result for {@link #revokeRuntimePermissions}.
 *
 * @param revoked The actually revoked permissions as
 *                {@code Map<packageName, List<permission>>}
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onRevokeRuntimePermissions(@android.annotation.NonNull java.util.Map<java.lang.String,java.util.List<java.lang.String>> revoked);
}

}

