/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.metrics;

import android.util.Log;
import android.os.Parcelable;

/**
 * Class for logging IpConnectvity events with IpConnectivityMetrics
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class IpConnectivityLog {

/** @hide */

public IpConnectivityLog() { throw new RuntimeException("Stub!"); }

/**
 * Log an IpConnectivity event.
 * @param timestamp is the epoch timestamp of the event in ms.
 * If the timestamp is 0, the timestamp is set to the current time in milliseconds.
 * @param data is a Parcelable instance representing the event.
 * This value must never be {@code null}.
 * @return true if the event was successfully logged.
 * @apiSince REL
 */

public boolean log(long timestamp, @android.annotation.NonNull android.net.metrics.IpConnectivityLog.Event data) { throw new RuntimeException("Stub!"); }

/**
 * Log an IpConnectivity event.
 * @param ifname the network interface associated with the event.
 * This value must never be {@code null}.
 * @param data is a Parcelable instance representing the event.
 * This value must never be {@code null}.
 * @return true if the event was successfully logged.
 * @apiSince REL
 */

public boolean log(@android.annotation.NonNull java.lang.String ifname, @android.annotation.NonNull android.net.metrics.IpConnectivityLog.Event data) { throw new RuntimeException("Stub!"); }

/**
 * Log an IpConnectivity event.
 * @param network the network associated with the event.
 * This value must never be {@code null}.
 * @param transports the current transports of the network associated with the event, as defined
 * in NetworkCapabilities.
 * This value must never be {@code null}.
 * @param data is a Parcelable instance representing the event.
 * This value must never be {@code null}.
 * @return true if the event was successfully logged.
 * @apiSince REL
 */

public boolean log(@android.annotation.NonNull android.net.Network network, @android.annotation.NonNull int[] transports, @android.annotation.NonNull android.net.metrics.IpConnectivityLog.Event data) { throw new RuntimeException("Stub!"); }

/**
 * Log an IpConnectivity event.
 * @param netid the id of the network associated with the event.
 * @param transports the current transports of the network associated with the event, as defined
 * in NetworkCapabilities.
 * This value must never be {@code null}.
 * @param data is a Parcelable instance representing the event.
 * This value must never be {@code null}.
 * @return true if the event was successfully logged.
 * @apiSince REL
 */

public boolean log(int netid, @android.annotation.NonNull int[] transports, @android.annotation.NonNull android.net.metrics.IpConnectivityLog.Event data) { throw new RuntimeException("Stub!"); }

/**
 * Log an IpConnectivity event.
 * @param data is a Parcelable instance representing the event.
 * This value must never be {@code null}.
 * @return true if the event was successfully logged.
 * @apiSince REL
 */

public boolean log(@android.annotation.NonNull android.net.metrics.IpConnectivityLog.Event data) { throw new RuntimeException("Stub!"); }
/**
 * An event to be logged.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface Event extends android.os.Parcelable {
}

}

