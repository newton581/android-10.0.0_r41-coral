/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.usage;


/**
 * CacheQuotaHint represents a triplet of a uid, the volume UUID it is stored upon, and
 * its usage stats. When processed, it obtains a cache quota as defined by the system which
 * allows apps to understand how much cache to use.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CacheQuotaHint implements android.os.Parcelable {

/**
 * Create a new request.
 * @param builder A builder for this object.
 * @apiSince REL
 */

public CacheQuotaHint(android.app.usage.CacheQuotaHint.Builder builder) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getVolumeUuid() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getUid() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long getQuota() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.app.usage.UsageStats getUsageStats() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.usage.CacheQuotaHint> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public static final long QUOTA_NOT_SET = -1L; // 0xffffffffffffffffL
/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/** @apiSince REL */

public Builder() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public Builder(android.app.usage.CacheQuotaHint hint) { throw new RuntimeException("Stub!"); }

/**
 * @param uuid This value may be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.usage.CacheQuotaHint.Builder setVolumeUuid(@android.annotation.Nullable java.lang.String uuid) { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.usage.CacheQuotaHint.Builder setUid(int uid) { throw new RuntimeException("Stub!"); }

/**
 * @param stats This value may be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.usage.CacheQuotaHint.Builder setUsageStats(@android.annotation.Nullable android.app.usage.UsageStats stats) { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.usage.CacheQuotaHint.Builder setQuota(long quota) { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.usage.CacheQuotaHint build() { throw new RuntimeException("Stub!"); }
}

}

