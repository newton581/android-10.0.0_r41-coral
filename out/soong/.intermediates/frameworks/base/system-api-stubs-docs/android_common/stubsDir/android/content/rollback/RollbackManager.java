/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.rollback;

import android.content.pm.PackageInstaller;

/**
 * Offers the ability to rollback packages after upgrade.
 * <p>
 * For packages installed with rollbacks enabled, the RollbackManager can be
 * used to initiate rollback of those packages for a limited time period after
 * upgrade.
 *
 * @see PackageInstaller.SessionParams#setEnableRollback(boolean)
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class RollbackManager {

RollbackManager() { throw new RuntimeException("Stub!"); }

/**
 * Returns a list of all currently available rollbacks.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ROLLBACKS} or android.Manifest.permission.TEST_MANAGE_ROLLBACKS
 * @throws SecurityException if the caller does not have appropriate permissions.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.content.rollback.RollbackInfo> getAvailableRollbacks() { throw new RuntimeException("Stub!"); }

/**
 * Gets the list of all recently committed rollbacks.
 * This is for the purposes of preventing re-install of a bad version of a
 * package and monitoring the status of a staged rollback.
 * <p>
 * Returns an empty list if there are no recently committed rollbacks.
 * <p>
 * To avoid having to keep around complete rollback history forever on a
 * device, the returned list of rollbacks is only guaranteed to include
 * rollbacks that are still relevant. A rollback is no longer considered
 * relevant if the package is subsequently uninstalled or upgraded
 * (without the possibility of rollback) to a higher version code than was
 * rolled back from.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ROLLBACKS} or android.Manifest.permission.TEST_MANAGE_ROLLBACKS
 * @return the recently committed rollbacks
 * This value will never be {@code null}.
 * @throws SecurityException if the caller does not have appropriate permissions.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.content.rollback.RollbackInfo> getRecentlyCommittedRollbacks() { throw new RuntimeException("Stub!"); }

/**
 * Commit the rollback with given id, rolling back all versions of the
 * packages to the last good versions previously installed on the device
 * as specified in the corresponding RollbackInfo object. The
 * rollback will fail if any of the installed packages or available
 * rollbacks are inconsistent with the versions specified in the given
 * rollback object, which can happen if a package has been updated or a
 * rollback expired since the rollback object was retrieved from
 * {@link #getAvailableRollbacks()}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ROLLBACKS} or android.Manifest.permission.TEST_MANAGE_ROLLBACKS
 * @param rollbackId ID of the rollback to commit
 * @param causePackages package versions to record as the motivation for this
 *                      rollback.
 * This value must never be {@code null}.
 * @param statusReceiver where to deliver the results. Intents sent to
 *                       this receiver contain {@link #EXTRA_STATUS}
 *                       and {@link #EXTRA_STATUS_MESSAGE}.
 * This value must never be {@code null}.
 * @throws SecurityException if the caller does not have appropriate permissions.
 * @apiSince REL
 */

public void commitRollback(int rollbackId, @android.annotation.NonNull java.util.List<android.content.pm.VersionedPackage> causePackages, @android.annotation.NonNull android.content.IntentSender statusReceiver) { throw new RuntimeException("Stub!"); }

/**
 * Status of a rollback commit. Will be one of
 * {@link #STATUS_SUCCESS}, {@link #STATUS_FAILURE},
 * {@link #STATUS_FAILURE_ROLLBACK_UNAVAILABLE}, {@link #STATUS_FAILURE_INSTALL}
 *
 * @see Intent#getIntExtra(String, int)
 * @apiSince REL
 */

public static final java.lang.String EXTRA_STATUS = "android.content.rollback.extra.STATUS";

/**
 * Detailed string representation of the status, including raw details that
 * are useful for debugging.
 *
 * @see Intent#getStringExtra(String)
 * @apiSince REL
 */

public static final java.lang.String EXTRA_STATUS_MESSAGE = "android.content.rollback.extra.STATUS_MESSAGE";

/**
 * The rollback could not be committed due to some generic failure.
 *
 * @see #EXTRA_STATUS_MESSAGE
 * @apiSince REL
 */

public static final int STATUS_FAILURE = 1; // 0x1

/**
 * The rollback failed to install successfully.
 *
 * @see #EXTRA_STATUS_MESSAGE
 * @apiSince REL
 */

public static final int STATUS_FAILURE_INSTALL = 3; // 0x3

/**
 * The rollback could not be committed because it was no longer available.
 *
 * @see #EXTRA_STATUS_MESSAGE
 * @apiSince REL
 */

public static final int STATUS_FAILURE_ROLLBACK_UNAVAILABLE = 2; // 0x2

/**
 * The rollback was successfully committed.
 * @apiSince REL
 */

public static final int STATUS_SUCCESS = 0; // 0x0
}

