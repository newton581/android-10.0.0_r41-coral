/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.hdmi;


/**
 * Listener for hdmi record feature including one touch record and timer recording.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class HdmiRecordListener {

/** @apiSince REL */

public HdmiRecordListener() { throw new RuntimeException("Stub!"); }

/**
 * Called when TV received one touch record request from record device. The client of this
 * should use {@link HdmiRecordSources} to return it.
 *
 * @param recorderAddress
 * @return record source to be used for recording. Null if no device is available.
 * @apiSince REL
 */

public abstract android.hardware.hdmi.HdmiRecordSources.RecordSource onOneTouchRecordSourceRequested(int recorderAddress);

/**
 * Called when one touch record is started or failed during initialization.
 *
 * @param recorderAddress An address of recorder that reports result of one touch record
 *            request
 * @param result result code. For more details, please look at all constants starting with
 *            "ONE_TOUCH_RECORD_". Only
 *            {@link HdmiControlManager#ONE_TOUCH_RECORD_RECORDING_CURRENTLY_SELECTED_SOURCE},
 *            {@link HdmiControlManager#ONE_TOUCH_RECORD_RECORDING_DIGITAL_SERVICE},
 *            {@link HdmiControlManager#ONE_TOUCH_RECORD_RECORDING_ANALOGUE_SERVICE}, and
 *            {@link HdmiControlManager#ONE_TOUCH_RECORD_RECORDING_EXTERNAL_INPUT} mean normal
 *            start of recording; otherwise, describes failure.
 * @apiSince REL
 */

public void onOneTouchRecordResult(int recorderAddress, int result) { throw new RuntimeException("Stub!"); }

/**
 * Called when timer recording is started or failed during initialization.
 *
 * @param recorderAddress An address of recorder that reports result of timer recording
 *            request
 * @param data timer status data. For more details, look at {@link TimerStatusData}.
 * @apiSince REL
 */

public void onTimerRecordingResult(int recorderAddress, android.hardware.hdmi.HdmiRecordListener.TimerStatusData data) { throw new RuntimeException("Stub!"); }

/**
 * Called when receiving result for clear timer recording request.
 *
 * @param recorderAddress An address of recorder that reports result of clear timer recording
 *            request
 * @param result result of clear timer. It should be one of
 *            {@link HdmiControlManager#CLEAR_TIMER_STATUS_TIMER_NOT_CLEARED_RECORDING}
 *            {@link HdmiControlManager#CLEAR_TIMER_STATUS_TIMER_NOT_CLEARED_NO_MATCHING},
 *            {@link HdmiControlManager#CLEAR_TIMER_STATUS_TIMER_NOT_CLEARED_NO_INFO_AVAILABLE},
 *            {@link HdmiControlManager#CLEAR_TIMER_STATUS_TIMER_CLEARED},
 *            {@link HdmiControlManager#CLEAR_TIMER_STATUS_CHECK_RECORDER_CONNECTION},
 *            {@link HdmiControlManager#CLEAR_TIMER_STATUS_FAIL_TO_CLEAR_SELECTED_SOURCE},
 *            {@link HdmiControlManager#CLEAR_TIMER_STATUS_CEC_DISABLE}.
 * @apiSince REL
 */

public void onClearTimerRecordingResult(int recorderAddress, int result) { throw new RuntimeException("Stub!"); }
/**
 * [Timer overlap warning] [Media Info] [Timer Programmed Info]
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class TimerStatusData {

TimerStatusData() { throw new RuntimeException("Stub!"); }

/**
 * Indicates if there is another timer block already set which overlaps with this new
 * recording request.
 * @apiSince REL
 */

public boolean isOverlapped() { throw new RuntimeException("Stub!"); }

/**
 * Indicates if removable media is present and its write protect state.
 * It should be one of the following values.
 * <ul>
 *   <li>{@link HdmiControlManager#TIMER_STATUS_MEDIA_INFO_PRESENT_NOT_PROTECTED}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_MEDIA_INFO_PRESENT_PROTECTED}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_MEDIA_INFO_NOT_PRESENT}
 * </ul>
 * @apiSince REL
 */

public int getMediaInfo() { throw new RuntimeException("Stub!"); }

/**
 * Selector for [Timer Programmed Info].
 * If it is {@code true}, {@link #getProgrammedInfo()} would have meaningful value and
 * ignore result of {@link #getNotProgammedError()}.
 * @apiSince REL
 */

public boolean isProgrammed() { throw new RuntimeException("Stub!"); }

/**
 * Information indicating any non-fatal issues with the programming request.
 * It's set only if {@link #isProgrammed()} returns true.
 * It should be one of the following values.
 * <ul>
 *   <li>{@link HdmiControlManager#TIMER_STATUS_PROGRAMMED_INFO_ENOUGH_SPACE}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_PROGRAMMED_INFO_NOT_ENOUGH_SPACE}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_PROGRAMMED_INFO_MIGHT_NOT_ENOUGH_SPACE}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_PROGRAMMED_INFO_NO_MEDIA_INFO}
 * </ul>
 *
 * @throws IllegalStateException if it's called when {@link #isProgrammed()}
 *                               returns false
 * @apiSince REL
 */

public int getProgrammedInfo() { throw new RuntimeException("Stub!"); }

/**
 * Information indicating any fatal issues with the programming request.
 * It's set only if {@link #isProgrammed()} returns false.
 * it should be one of the following values.
 * <ul>
 *   <li>{@link HdmiControlManager#TIMER_STATUS_NOT_PROGRAMMED_NO_FREE_TIME}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_NOT_PROGRAMMED_DATE_OUT_OF_RANGE}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_NOT_PROGRAMMED_INVALID_SEQUENCE}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_NOT_PROGRAMMED_INVALID_EXTERNAL_PHYSICAL_NUMBER}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_NOT_PROGRAMMED_CA_NOT_SUPPORTED}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_NOT_PROGRAMMED_NO_CA_ENTITLEMENTS}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_NOT_PROGRAMMED_UNSUPPORTED_RESOLUTION}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_NOT_PROGRAMMED_PARENTAL_LOCK_ON}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_NOT_PROGRAMMED_CLOCK_FAILURE}
 *   <li>{@link HdmiControlManager#TIMER_STATUS_NOT_PROGRAMMED_DUPLICATED}
 * </ul>
 *
 * @throws IllegalStateException if it's called when {@link #isProgrammed()}
 *                               returns true
 * @apiSince REL
 */

public int getNotProgammedError() { throw new RuntimeException("Stub!"); }

/**
 * Duration hours.
 * Optional parameter: Contains an estimate of the space left on the media, expressed as a
 * time. This parameter may be returned when:
 *  - [Programmed Info] is “Not enough space available”; or
 *  - [Not Programmed Info] is “Duplicate: already programmed”
 * @apiSince REL
 */

public int getDurationHour() { throw new RuntimeException("Stub!"); }

/**
 * Duration minutes.
 * Optional parameter: Contains an estimate of the space left on the media, expressed as a
 * time. This parameter may be returned when:
 *  - [Programmed Info] is “Not enough space available”; or
 *  - [Not Programmed Info] is “Duplicate: already programmed”
 * @apiSince REL
 */

public int getDurationMinute() { throw new RuntimeException("Stub!"); }

/**
 * Extra error code.
 * <ul>
 * <li>{@link HdmiControlManager#TIMER_RECORDING_RESULT_EXTRA_NO_ERROR}
 *     No extra errors. Other values of this class might be available.
 * <li>{@link HdmiControlManager#TIMER_RECORDING_RESULT_EXTRA_CHECK_RECORDER_CONNECTION}
 *     Check record connection. Other values of this class should be ignored.
 * <li>{@link HdmiControlManager#TIMER_RECORDING_RESULT_EXTRA_FAIL_TO_RECORD_SELECTED_SOURCE}
 *     Fail to record selected source. Other values of this class should be ignored.
 * <li>{@link HdmiControlManager#TIMER_RECORDING_RESULT_EXTRA_CEC_DISABLED}
 *     Cec disabled. Other values of this class should be ignored.
 * </ul>
 * @apiSince REL
 */

public int getExtraError() { throw new RuntimeException("Stub!"); }
}

}

