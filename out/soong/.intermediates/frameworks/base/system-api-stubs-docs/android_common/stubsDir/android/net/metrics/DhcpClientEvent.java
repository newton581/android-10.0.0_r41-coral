/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.metrics;


/**
 * An event recorded when a DhcpClient state machine transitions to a new state.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class DhcpClientEvent implements android.net.metrics.IpConnectivityLog.Event {

DhcpClientEvent(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @hide */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @hide */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }
/**
 * Utility to create an instance of {@link ApfProgramEvent}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Set the message of the event.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.DhcpClientEvent.Builder setMsg(java.lang.String msg) { throw new RuntimeException("Stub!"); }

/**
 * Set the duration of the event in milliseconds.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.DhcpClientEvent.Builder setDurationMs(int durationMs) { throw new RuntimeException("Stub!"); }

/**
 * Create a new {@link DhcpClientEvent}.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.DhcpClientEvent build() { throw new RuntimeException("Stub!"); }
}

}

