/**
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.radio;

import android.content.pm.PackageManager;
import android.Manifest;
import java.util.List;
import android.os.Handler;
import android.content.Context;
import java.util.concurrent.Executor;
import java.util.Set;

/**
 * The RadioManager class allows to control a broadcast radio tuner present on the device.
 * It provides data structures and methods to query for available radio modules, list their
 * properties and open an interface to control tuning operations and receive callbacks when
 * asynchronous operations complete or events occur.
 * <br>
 * Requires the {@link android.content.pm.PackageManager#FEATURE_BROADCAST_RADIO PackageManager#FEATURE_BROADCAST_RADIO} feature which can be detected using {@link android.content.pm.PackageManager#hasSystemFeature(String) PackageManager.hasSystemFeature(String)}.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class RadioManager {

RadioManager() { throw new RuntimeException("Stub!"); }

/**
 * Returns a list of descriptors for all broadcast radio modules present on the device.
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_BROADCAST_RADIO}
 * @param modules An List of {@link ModuleProperties} where the list will be returned.
 * @return
 * <ul>
 *  <li>{@link #STATUS_OK} in case of success, </li>
 *  <li>{@link #STATUS_ERROR} in case of unspecified error, </li>
 *  <li>{@link #STATUS_NO_INIT} if the native service cannot be reached, </li>
 *  <li>{@link #STATUS_BAD_VALUE} if modules is null, </li>
 *  <li>{@link #STATUS_DEAD_OBJECT} if the binder transaction to the native service fails, </li>
 * </ul>
 * @apiSince REL
 */

public int listModules(java.util.List<android.hardware.radio.RadioManager.ModuleProperties> modules) { throw new RuntimeException("Stub!"); }

/**
 * Open an interface to control a tuner on a given broadcast radio module.
 * Optionally selects and applies the configuration passed as "config" argument.
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_BROADCAST_RADIO}
 * @param moduleId radio module identifier {@link ModuleProperties#getId()}. Mandatory.
 * @param config desired band and configuration to apply when enabling the hardware module.
 * optional, can be null.
 * @param withAudio {@code true} to request a tuner with an audio source.
 * This tuner is intended for live listening or recording or a radio program.
 * If {@code false}, the tuner can only be used to retrieve program informations.
 * @param callback {@link RadioTuner.Callback} interface. Mandatory.
 * @param handler the Handler on which the callbacks will be received.
 * Can be null if default handler is OK.
 * @return a valid {@link RadioTuner} interface in case of success or null in case of error.
 * @apiSince REL
 */

public android.hardware.radio.RadioTuner openTuner(int moduleId, android.hardware.radio.RadioManager.BandConfig config, boolean withAudio, android.hardware.radio.RadioTuner.Callback callback, android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * Adds new announcement listener.
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_BROADCAST_RADIO}
 * @param enabledAnnouncementTypes a set of announcement types to listen to
 * This value must never be {@code null}.
 * @param listener announcement listener
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void addAnnouncementListener(@android.annotation.NonNull java.util.Set<java.lang.Integer> enabledAnnouncementTypes, @android.annotation.NonNull android.hardware.radio.Announcement.OnListUpdatedListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Adds new announcement listener with executor.
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_BROADCAST_RADIO}
 * @param executor the executor
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param enabledAnnouncementTypes a set of announcement types to listen to
 * This value must never be {@code null}.
 * @param listener announcement listener
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void addAnnouncementListener(@android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull java.util.Set<java.lang.Integer> enabledAnnouncementTypes, @android.annotation.NonNull android.hardware.radio.Announcement.OnListUpdatedListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Removes previously registered announcement listener.
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_BROADCAST_RADIO}
 * @param listener announcement listener, previously registered with
 *        {@link addAnnouncementListener}
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void removeAnnouncementListener(@android.annotation.NonNull android.hardware.radio.Announcement.OnListUpdatedListener listener) { throw new RuntimeException("Stub!"); }

/** AM radio band (LW/MW/SW).
 * @see BandDescriptor     * @apiSince REL
 */

public static final int BAND_AM = 0; // 0x0

/** AM HD radio or DRM band.
 * @see BandDescriptor     * @apiSince REL
 */

public static final int BAND_AM_HD = 3; // 0x3

/** FM radio band.
 * @see BandDescriptor     * @apiSince REL
 */

public static final int BAND_FM = 1; // 0x1

/** FM HD radio or DRM  band.
 * @see BandDescriptor     * @apiSince REL
 */

public static final int BAND_FM_HD = 2; // 0x2

/** @apiSince REL */

public static final int BAND_INVALID = -1; // 0xffffffff

/**
 * Radio module class supporting FM (including HD radio) and AM
 * @apiSince REL
 */

public static final int CLASS_AM_FM = 0; // 0x0

/**
 * Radio module class supporting Digital terrestrial radio
 * @apiSince REL
 */

public static final int CLASS_DT = 2; // 0x2

/**
 * Radio module class supporting satellite radio
 * @apiSince REL
 */

public static final int CLASS_SAT = 1; // 0x1

/**
 * Enables DAB-DAB hard- and implicit-linking (the same content).
 * @apiSince REL
 */

public static final int CONFIG_DAB_DAB_LINKING = 6; // 0x6

/**
 * Enables DAB-DAB soft-linking (related content).
 * @apiSince REL
 */

public static final int CONFIG_DAB_DAB_SOFT_LINKING = 8; // 0x8

/**
 * Enables DAB-FM hard- and implicit-linking (the same content).
 * @apiSince REL
 */

public static final int CONFIG_DAB_FM_LINKING = 7; // 0x7

/**
 * Enables DAB-FM soft-linking (related content).
 * @apiSince REL
 */

public static final int CONFIG_DAB_FM_SOFT_LINKING = 9; // 0x9

/**
 * Forces the analog playback for the supporting radio technology.
 *
 * User may disable digital playback for FM HD Radio or hybrid FM/DAB with
 * this option. This is purely user choice, ie. does not reflect digital-
 * analog handover state managed from the HAL implementation side.
 *
 * Some radio technologies may not support this, ie. DAB.
 * @apiSince REL
 */

public static final int CONFIG_FORCE_ANALOG = 2; // 0x2

/**
 * Forces the digital playback for the supporting radio technology.
 *
 * User may disable digital-analog handover that happens with poor
 * reception conditions. With digital forced, the radio will remain silent
 * instead of switching to analog channel if it's available. This is purely
 * user choice, it does not reflect the actual state of handover.
 * @apiSince REL
 */

public static final int CONFIG_FORCE_DIGITAL = 3; // 0x3

/**
 * Forces mono audio stream reception.
 *
 * Analog broadcasts can recover poor reception conditions by jointing
 * stereo channels into one. Mainly for, but not limited to AM/FM.
 * @apiSince REL
 */

public static final int CONFIG_FORCE_MONO = 1; // 0x1

/**
 * RDS Alternative Frequencies.
 *
 * If set and the currently tuned RDS station broadcasts on multiple
 * channels, radio tuner automatically switches to the best available
 * alternative.
 * @apiSince REL
 */

public static final int CONFIG_RDS_AF = 4; // 0x4

/**
 * RDS region-specific program lock-down.
 *
 * Allows user to lock to the current region as they move into the
 * other region.
 * @apiSince REL
 */

public static final int CONFIG_RDS_REG = 5; // 0x5

/** Africa, Europe.
 * @see BandDescriptor     * @apiSince REL
 */

public static final int REGION_ITU_1 = 0; // 0x0

/** Americas.
 * @see BandDescriptor     * @apiSince REL
 */

public static final int REGION_ITU_2 = 1; // 0x1

/** Japan.
 * @see BandDescriptor     * @apiSince REL
 */

public static final int REGION_JAPAN = 3; // 0x3

/** Korea.
 * @see BandDescriptor     * @apiSince REL
 */

public static final int REGION_KOREA = 4; // 0x4

/** Russia.
 * @see BandDescriptor     * @apiSince REL
 */

public static final int REGION_OIRT = 2; // 0x2

/**
 * Method return status: invalid argument provided
 * @apiSince REL
 */

public static final int STATUS_BAD_VALUE = -22; // 0xffffffea

/**
 * Method return status: cannot reach service
 * @apiSince REL
 */

public static final int STATUS_DEAD_OBJECT = -32; // 0xffffffe0

/**
 * Method return status: unspecified error
 * @apiSince REL
 */

public static final int STATUS_ERROR = -2147483648; // 0x80000000

/**
 * Method return status: invalid or out of sequence operation
 * @apiSince REL
 */

public static final int STATUS_INVALID_OPERATION = -38; // 0xffffffda

/**
 * Method return status: initialization failure
 * @apiSince REL
 */

public static final int STATUS_NO_INIT = -19; // 0xffffffed

/**
 * Method return status: successful operation
 * @apiSince REL
 */

public static final int STATUS_OK = 0; // 0x0

/**
 * Method return status: permission denied
 * @apiSince REL
 */

public static final int STATUS_PERMISSION_DENIED = -1; // 0xffffffff

/**
 * Method return status: time out before operation completion
 * @apiSince REL
 */

public static final int STATUS_TIMED_OUT = -110; // 0xffffff92
/** AM band configuration.
 * @see #BAND_AM     * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class AmBandConfig extends android.hardware.radio.RadioManager.BandConfig {

/** @hide */

AmBandConfig(android.hardware.radio.RadioManager.AmBandDescriptor descriptor) { super(null); throw new RuntimeException("Stub!"); }

/** Get stereo enable state
 * @return the enable state.
 * @apiSince REL
 */

public boolean getStereo() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.RadioManager.AmBandConfig> CREATOR;
static { CREATOR = null; }
/**
 * Builder class for {@link AmBandConfig} objects.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

/**
 * Constructs a new Builder with the defaults from an {@link AmBandDescriptor} .
 * @param descriptor the FmBandDescriptor defaults are read from .
 * @apiSince REL
 */

public Builder(android.hardware.radio.RadioManager.AmBandDescriptor descriptor) { throw new RuntimeException("Stub!"); }

/**
 * Constructs a new Builder from a given {@link AmBandConfig}
 * @param config the FmBandConfig object whose data will be reused in the new Builder.
 * @apiSince REL
 */

public Builder(android.hardware.radio.RadioManager.AmBandConfig config) { throw new RuntimeException("Stub!"); }

/**
 * Combines all of the parameters that have been set and return a new
 * {@link AmBandConfig} object.
 * @return a new {@link AmBandConfig} object
 * @apiSince REL
 */

public android.hardware.radio.RadioManager.AmBandConfig build() { throw new RuntimeException("Stub!"); }

/** Set stereo enable state
 * @param state The new enable state.
 * @return the same Builder instance.
 * @apiSince REL
 */

public android.hardware.radio.RadioManager.AmBandConfig.Builder setStereo(boolean state) { throw new RuntimeException("Stub!"); }
}

}

/** AM band descriptor.
 * @see #BAND_AM     * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class AmBandDescriptor extends android.hardware.radio.RadioManager.BandDescriptor {

AmBandDescriptor(android.os.Parcel in) { super(null); throw new RuntimeException("Stub!"); }

/** Stereo is supported
 *  @return {@code true} if stereo is supported, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isStereoSupported() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.RadioManager.AmBandDescriptor> CREATOR;
static { CREATOR = null; }
}

/**
 * Value is {@link android.hardware.radio.RadioManager#BAND_INVALID}, {@link android.hardware.radio.RadioManager#BAND_AM}, {@link android.hardware.radio.RadioManager#BAND_FM}, {@link android.hardware.radio.RadioManager#BAND_AM_HD}, or {@link android.hardware.radio.RadioManager#BAND_FM_HD}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface Band {
}

/**
 * Radio band configuration.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class BandConfig implements android.os.Parcelable {

BandConfig(android.hardware.radio.RadioManager.BandDescriptor descriptor) { throw new RuntimeException("Stub!"); }

/** Region this band applies to. E.g. {@link #REGION_ITU_1}
 *  @return the region associated with this band.
 * @apiSince REL
 */

public int getRegion() { throw new RuntimeException("Stub!"); }

/** Band type, e.g {@link #BAND_FM}. Defines the subclass this descriptor can be cast to:
 * <ul>
 *  <li>{@link #BAND_FM} or {@link #BAND_FM_HD} cast to {@link FmBandDescriptor}, </li>
 *  <li>{@link #BAND_AM} cast to {@link AmBandDescriptor}, </li>
 * </ul>
 *  @return the band type.
 * @apiSince REL
 */

public int getType() { throw new RuntimeException("Stub!"); }

/** Lower band limit expressed in units according to band type.
 * Currently all defined band types express channels as frequency in kHz
 *  @return the lower band limit.
 * @apiSince REL
 */

public int getLowerLimit() { throw new RuntimeException("Stub!"); }

/** Upper band limit expressed in units according to band type.
 * Currently all defined band types express channels as frequency in kHz
 *  @return the upper band limit.
 * @apiSince REL
 */

public int getUpperLimit() { throw new RuntimeException("Stub!"); }

/** Channel spacing in units according to band type.
 * Currently all defined band types express channels as frequency in kHz
 *  @return the channel spacing.
 * @apiSince REL
 */

public int getSpacing() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.RadioManager.BandConfig> CREATOR;
static { CREATOR = null; }
}

/** Radio band descriptor: an element in ModuleProperties bands array.
 * It is either an instance of {@link FmBandDescriptor} or {@link AmBandDescriptor}     * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class BandDescriptor implements android.os.Parcelable {

BandDescriptor(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** Region this band applies to. E.g. {@link #REGION_ITU_1}
 * @return the region this band is associated to.
 * @apiSince REL
 */

public int getRegion() { throw new RuntimeException("Stub!"); }

/** Band type, e.g {@link #BAND_FM}. Defines the subclass this descriptor can be cast to:
 * <ul>
 *  <li>{@link #BAND_FM} or {@link #BAND_FM_HD} cast to {@link FmBandDescriptor}, </li>
 *  <li>{@link #BAND_AM} cast to {@link AmBandDescriptor}, </li>
 * </ul>
 * @return the band type.
 * @apiSince REL
 */

public int getType() { throw new RuntimeException("Stub!"); }

/**
 * Checks if the band is either AM or AM_HD.
 *
 * @return {@code true}, if band is AM or AM_HD.
 * @apiSince REL
 */

public boolean isAmBand() { throw new RuntimeException("Stub!"); }

/**
 * Checks if the band is either FM or FM_HD.
 *
 * @return {@code true}, if band is FM or FM_HD.
 * @apiSince REL
 */

public boolean isFmBand() { throw new RuntimeException("Stub!"); }

/** Lower band limit expressed in units according to band type.
 * Currently all defined band types express channels as frequency in kHz
 * @return the lower band limit.
 * @apiSince REL
 */

public int getLowerLimit() { throw new RuntimeException("Stub!"); }

/** Upper band limit expressed in units according to band type.
 * Currently all defined band types express channels as frequency in kHz
 * @return the upper band limit.
 * @apiSince REL
 */

public int getUpperLimit() { throw new RuntimeException("Stub!"); }

/** Channel spacing in units according to band type.
 * Currently all defined band types express channels as frequency in kHz
 * @return the channel spacing.
 * @apiSince REL
 */

public int getSpacing() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.RadioManager.BandDescriptor> CREATOR;
static { CREATOR = null; }
}

/** FM band configuration.
 * @see #BAND_FM
 * @see #BAND_FM_HD     * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class FmBandConfig extends android.hardware.radio.RadioManager.BandConfig {

/** @hide */

FmBandConfig(android.hardware.radio.RadioManager.FmBandDescriptor descriptor) { super(null); throw new RuntimeException("Stub!"); }

/** Get stereo enable state
 * @return the enable state.
 * @apiSince REL
 */

public boolean getStereo() { throw new RuntimeException("Stub!"); }

/** Get RDS or RBDS(if region is ITU2) enable state
 * @return the enable state.
 * @apiSince REL
 */

public boolean getRds() { throw new RuntimeException("Stub!"); }

/** Get Traffic announcement enable state
 * @return the enable state.
 * @apiSince REL
 */

public boolean getTa() { throw new RuntimeException("Stub!"); }

/** Get Alternate Frequency Switching enable state
 * @return the enable state.
 * @apiSince REL
 */

public boolean getAf() { throw new RuntimeException("Stub!"); }

/**
 * Get Emergency announcement enable state
 * @return the enable state.
 * @apiSince REL
 */

public boolean getEa() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.RadioManager.FmBandConfig> CREATOR;
static { CREATOR = null; }
/**
 * Builder class for {@link FmBandConfig} objects.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

/**
 * Constructs a new Builder with the defaults from an {@link FmBandDescriptor} .
 * @param descriptor the FmBandDescriptor defaults are read from .
 * @apiSince REL
 */

public Builder(android.hardware.radio.RadioManager.FmBandDescriptor descriptor) { throw new RuntimeException("Stub!"); }

/**
 * Constructs a new Builder from a given {@link FmBandConfig}
 * @param config the FmBandConfig object whose data will be reused in the new Builder.
 * @apiSince REL
 */

public Builder(android.hardware.radio.RadioManager.FmBandConfig config) { throw new RuntimeException("Stub!"); }

/**
 * Combines all of the parameters that have been set and return a new
 * {@link FmBandConfig} object.
 * @return a new {@link FmBandConfig} object
 * @apiSince REL
 */

public android.hardware.radio.RadioManager.FmBandConfig build() { throw new RuntimeException("Stub!"); }

/** Set stereo enable state
 * @param state The new enable state.
 * @return the same Builder instance.
 * @apiSince REL
 */

public android.hardware.radio.RadioManager.FmBandConfig.Builder setStereo(boolean state) { throw new RuntimeException("Stub!"); }

/** Set RDS or RBDS(if region is ITU2) enable state
 * @param state The new enable state.
 * @return the same Builder instance.
 * @apiSince REL
 */

public android.hardware.radio.RadioManager.FmBandConfig.Builder setRds(boolean state) { throw new RuntimeException("Stub!"); }

/** Set Traffic announcement enable state
 * @param state The new enable state.
 * @return the same Builder instance.
 * @apiSince REL
 */

public android.hardware.radio.RadioManager.FmBandConfig.Builder setTa(boolean state) { throw new RuntimeException("Stub!"); }

/** Set Alternate Frequency Switching enable state
 * @param state The new enable state.
 * @return the same Builder instance.
 * @apiSince REL
 */

public android.hardware.radio.RadioManager.FmBandConfig.Builder setAf(boolean state) { throw new RuntimeException("Stub!"); }

/** Set Emergency Announcement enable state
 * @param state The new enable state.
 * @return the same Builder instance.
 * @apiSince REL
 */

public android.hardware.radio.RadioManager.FmBandConfig.Builder setEa(boolean state) { throw new RuntimeException("Stub!"); }
}

}

/** FM band descriptor
 * @see #BAND_FM
 * @see #BAND_FM_HD     * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class FmBandDescriptor extends android.hardware.radio.RadioManager.BandDescriptor {

FmBandDescriptor(android.os.Parcel in) { super(null); throw new RuntimeException("Stub!"); }

/** Stereo is supported
 * @return {@code true} if stereo is supported, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isStereoSupported() { throw new RuntimeException("Stub!"); }

/** RDS or RBDS(if region is ITU2) is supported
 * @return {@code true} if RDS or RBDS is supported, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isRdsSupported() { throw new RuntimeException("Stub!"); }

/** Traffic announcement is supported
 * @return {@code true} if TA is supported, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isTaSupported() { throw new RuntimeException("Stub!"); }

/** Alternate Frequency Switching is supported
 * @return {@code true} if AF switching is supported, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isAfSupported() { throw new RuntimeException("Stub!"); }

/** Emergency Announcement is supported
 * @return {@code true} if Emergency annoucement is supported, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isEaSupported() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.RadioManager.FmBandDescriptor> CREATOR;
static { CREATOR = null; }
}

/*****************************************************************************
 * Lists properties, options and radio bands supported by a given broadcast radio module.
 * Each module has a unique ID used to address it when calling RadioManager APIs.
 * Module properties are returned by {@link #listModules(List <ModuleProperties>)} method.
 ***************************************************************************     * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ModuleProperties implements android.os.Parcelable {

ModuleProperties(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** Unique module identifier provided by the native service.
 * For use with {@link #openTuner(int, BandConfig, boolean, Callback, Handler)}.
 * @return the radio module unique identifier.
 * @apiSince REL
 */

public int getId() { throw new RuntimeException("Stub!"); }

/**
 * Module service (driver) name as registered with HIDL.
 * @return the module service name.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getServiceName() { throw new RuntimeException("Stub!"); }

/** Module class identifier: {@link #CLASS_AM_FM}, {@link #CLASS_SAT}, {@link #CLASS_DT}
 * @return the radio module class identifier.
 * @apiSince REL
 */

public int getClassId() { throw new RuntimeException("Stub!"); }

/** Human readable broadcast radio module implementor
 * @return the name of the radio module implementator.
 * @apiSince REL
 */

public java.lang.String getImplementor() { throw new RuntimeException("Stub!"); }

/** Human readable broadcast radio module product name
 * @return the radio module product name.
 * @apiSince REL
 */

public java.lang.String getProduct() { throw new RuntimeException("Stub!"); }

/** Human readable broadcast radio module version number
 * @return the radio module version.
 * @apiSince REL
 */

public java.lang.String getVersion() { throw new RuntimeException("Stub!"); }

/** Radio module serial number.
 * Can be used for subscription services.
 * @return the radio module serial number.
 * @apiSince REL
 */

public java.lang.String getSerial() { throw new RuntimeException("Stub!"); }

/** Number of tuners available.
 * This is the number of tuners that can be open simultaneously.
 * @return the number of tuners supported.
 * @apiSince REL
 */

public int getNumTuners() { throw new RuntimeException("Stub!"); }

/** Number tuner audio sources available. Must be less or equal to getNumTuners().
 * When more than one tuner is supported, one is usually for playback and has one
 * associated audio source and the other is for pre scanning and building a
 * program list.
 * @return the number of audio sources available.
 * @apiSince REL
 */

public int getNumAudioSources() { throw new RuntimeException("Stub!"); }

/**
 * Checks, if BandConfig initialization (after {@link RadioManager#openTuner})
 * is required to be done before other operations or not.
 *
 * If it is, the client has to wait for {@link RadioTuner.Callback#onConfigurationChanged}
 * callback before executing any other operations. Otherwise, such operation will fail
 * returning {@link RadioManager#STATUS_INVALID_OPERATION} error code.
 * @apiSince REL
 */

public boolean isInitializationRequired() { throw new RuntimeException("Stub!"); }

/** {@code true} if audio capture is possible from radio tuner output.
 * This indicates if routing to audio devices not connected to the same HAL as the FM radio
 * is possible (e.g. to USB) or DAR (Digital Audio Recorder) feature can be implemented.
 * @return {@code true} if audio capture is possible, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isCaptureSupported() { throw new RuntimeException("Stub!"); }

/**
 * {@code true} if the module supports background scanning. At the given time it may not
 * be available though, see {@link RadioTuner#startBackgroundScan()}.
 *
 * @return {@code true} if background scanning is supported (not necessary available
 * at a given time), {@code false} otherwise.
 * @apiSince REL
 */

public boolean isBackgroundScanningSupported() { throw new RuntimeException("Stub!"); }

/**
 * Checks, if a given program type is supported by this tuner.
 *
 * If a program type is supported by radio module, it means it can tune
 * to ProgramSelector of a given type.
 *
 * @param type Value is {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_INVALID}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_AM}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_FM}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_AM_HD}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_FM_HD}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_DAB}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_DRMO}, or {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_SXM}
 * Value is between PROGRAM_TYPE_VENDOR_START and PROGRAM_TYPE_VENDOR_END inclusive
 * @return {@code true} if a given program type is supported.
 * @apiSince REL
 */

public boolean isProgramTypeSupported(int type) { throw new RuntimeException("Stub!"); }

/**
 * Checks, if a given program identifier is supported by this tuner.
 *
 * If an identifier is supported by radio module, it means it can use it for
 * tuning to ProgramSelector with either primary or secondary Identifier of
 * a given type.
 *
 * @param type Value is {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_INVALID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_AMFM_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_RDS_PI}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_ID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_SUBCHANNEL}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_NAME}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SIDECC}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_ENSEMBLE}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SCID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_SERVICE_ID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_MODULATION}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_SERVICE_ID}, or {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_CHANNEL}
 * Value is between IDENTIFIER_TYPE_VENDOR_START and IDENTIFIER_TYPE_VENDOR_END inclusive
 * @return {@code true} if a given program type is supported.
 * @apiSince REL
 */

public boolean isProgramIdentifierSupported(int type) { throw new RuntimeException("Stub!"); }

/**
 * A frequency table for Digital Audio Broadcasting (DAB).
 *
 * The key is a channel name, i.e. 5A, 7B.
 *
 * The value is a frequency, in kHz.
 *
 * @return a frequency table, or {@code null} if the module doesn't support DAB
 * @apiSince REL
 */

@android.annotation.Nullable
public java.util.Map<java.lang.String,java.lang.Integer> getDabFrequencyTable() { throw new RuntimeException("Stub!"); }

/**
 * A map of vendor-specific opaque strings, passed from HAL without changes.
 * Format of these strings can vary across vendors.
 *
 * It may be used for extra features, that's not supported by a platform,
 * for example: preset-slots=6; ultra-hd-capable=false.
 *
 * Keys must be prefixed with unique vendor Java-style namespace,
 * eg. 'com.somecompany.parameter1'.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.Map<java.lang.String,java.lang.String> getVendorInfo() { throw new RuntimeException("Stub!"); }

/** List of descriptors for all bands supported by this module.
 * @return an array of {@link BandDescriptor}.
 * @apiSince REL
 */

public android.hardware.radio.RadioManager.BandDescriptor[] getBands() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.RadioManager.ModuleProperties> CREATOR;
static { CREATOR = null; }
}

/**
 * Radio program information.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ProgramInfo implements android.os.Parcelable {

ProgramInfo(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Program selector, necessary for tuning to a program.
 *
 * @return the program selector.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.radio.ProgramSelector getSelector() { throw new RuntimeException("Stub!"); }

/**
 * Identifier currently used for program selection.
 *
 * This identifier can be used to determine which technology is
 * currently being used for reception.
 *
 * Some program selectors contain tuning information for different radio
 * technologies (i.e. FM RDS and DAB). For example, user may tune using
 * a ProgramSelector with RDS_PI primary identifier, but the tuner hardware
 * may choose to use DAB technology to make actual tuning. This identifier
 * must reflect that.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.hardware.radio.ProgramSelector.Identifier getLogicallyTunedTo() { throw new RuntimeException("Stub!"); }

/**
 * Identifier currently used by hardware to physically tune to a channel.
 *
 * Some radio technologies broadcast the same program on multiple channels,
 * i.e. with RDS AF the same program may be broadcasted on multiple
 * alternative frequencies; the same DAB program may be broadcast on
 * multiple ensembles. This identifier points to the channel to which the
 * radio hardware is physically tuned to.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.hardware.radio.ProgramSelector.Identifier getPhysicallyTunedTo() { throw new RuntimeException("Stub!"); }

/**
 * Primary identifiers of related contents.
 *
 * Some radio technologies provide pointers to other programs that carry
 * related content (i.e. DAB soft-links). This field is a list of pointers
 * to other programs on the program list.
 *
 * Please note, that these identifiers does not have to exist on the program
 * list - i.e. DAB tuner may provide information on FM RDS alternatives
 * despite not supporting FM RDS. If the system has multiple tuners, another
 * one may have it on its list.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.util.Collection<android.hardware.radio.ProgramSelector.Identifier> getRelatedContent() { throw new RuntimeException("Stub!"); }

/** Main channel expressed in units according to band type.
 * Currently all defined band types express channels as frequency in kHz
 * @return the program channel
 * @deprecated Use {@link getSelector()} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getChannel() { throw new RuntimeException("Stub!"); }

/** Sub channel ID. E.g 1 for HD radio HD1
 * @return the program sub channel
 * @deprecated Use {@link getSelector()} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getSubChannel() { throw new RuntimeException("Stub!"); }

/** {@code true} if the tuner is currently tuned on a valid station
 * @return {@code true} if currently tuned, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isTuned() { throw new RuntimeException("Stub!"); }

/** {@code true} if the received program is stereo
 * @return {@code true} if stereo, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isStereo() { throw new RuntimeException("Stub!"); }

/** {@code true} if the received program is digital (e.g HD radio)
 * @return {@code true} if digital, {@code false} otherwise.
 * @deprecated Use {@link getLogicallyTunedTo()} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean isDigital() { throw new RuntimeException("Stub!"); }

/**
 * {@code true} if the program is currently playing live stream.
 * This may result in a slightly altered reception parameters,
 * usually targetted at reduced latency.
 * @apiSince REL
 */

public boolean isLive() { throw new RuntimeException("Stub!"); }

/**
 * {@code true} if radio stream is not playing, ie. due to bad reception
 * conditions or buffering. In this state volume knob MAY be disabled to
 * prevent user increasing volume too much.
 * It does NOT mean the user has muted audio.
 * @apiSince REL
 */

public boolean isMuted() { throw new RuntimeException("Stub!"); }

/**
 * {@code true} if radio station transmits traffic information
 * regularily.
 * @apiSince REL
 */

public boolean isTrafficProgram() { throw new RuntimeException("Stub!"); }

/**
 * {@code true} if radio station transmits traffic information
 * at the very moment.
 * @apiSince REL
 */

public boolean isTrafficAnnouncementActive() { throw new RuntimeException("Stub!"); }

/**
 * Signal quality (as opposed to the name) indication from 0 (no signal)
 * to 100 (excellent)
 * @return the signal quality indication.
 * @apiSince REL
 */

public int getSignalStrength() { throw new RuntimeException("Stub!"); }

/** Metadata currently received from this station.
 * null if no metadata have been received
 * @return current meta data received from this program.
 * @apiSince REL
 */

public android.hardware.radio.RadioMetadata getMetadata() { throw new RuntimeException("Stub!"); }

/**
 * A map of vendor-specific opaque strings, passed from HAL without changes.
 * Format of these strings can vary across vendors.
 *
 * It may be used for extra features, that's not supported by a platform,
 * for example: paid-service=true; bitrate=320kbps.
 *
 * Keys must be prefixed with unique vendor Java-style namespace,
 * eg. 'com.somecompany.parameter1'.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.Map<java.lang.String,java.lang.String> getVendorInfo() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.RadioManager.ProgramInfo> CREATOR;
static { CREATOR = null; }
}

}

