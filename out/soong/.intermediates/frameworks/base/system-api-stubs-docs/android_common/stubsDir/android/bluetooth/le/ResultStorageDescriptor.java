/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.bluetooth.le;


/**
 * Describes the way to store scan result.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ResultStorageDescriptor implements android.os.Parcelable {

/**
 * Constructor of {@link ResultStorageDescriptor}
 *
 * @param type Type of the data.
 * @param offset Offset from start of the advertise packet payload.
 * @param length Byte length of the data
 * @apiSince REL
 */

public ResultStorageDescriptor(int type, int offset, int length) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getOffset() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getLength() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.bluetooth.le.ResultStorageDescriptor> CREATOR;
static { CREATOR = null; }
}

