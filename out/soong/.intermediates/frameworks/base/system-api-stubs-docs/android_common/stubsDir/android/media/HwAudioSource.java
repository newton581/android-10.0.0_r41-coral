/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media;


/**
 * The HwAudioSource represents the audio playback directly from a source audio device.
 * It currently supports {@link HwAudioSource#start()} and {@link HwAudioSource#stop()} only
 * corresponding to {@link AudioSystem#startAudioSource(AudioPortConfig, AudioAttributes)}
 * and {@link AudioSystem#stopAudioSource(int)}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HwAudioSource {

/**
 * Class constructor for a hardware audio source based player.
 *
 * Use the {@link Builder} class to construct a {@link HwAudioSource} instance.
 *
 * @param device {@link AudioDeviceInfo} instance of the source audio device.
 * @param attributes {@link AudioAttributes} instance for this player.
 */

HwAudioSource(@android.annotation.NonNull android.media.AudioDeviceInfo device, @android.annotation.NonNull android.media.AudioAttributes attributes) { throw new RuntimeException("Stub!"); }

/**
 * Starts the playback from {@link AudioDeviceInfo}.
 * @apiSince REL
 */

public void start() { throw new RuntimeException("Stub!"); }

/**
 * Checks whether the HwAudioSource player is playing.
 * @return true if currently playing, false otherwise
 * @apiSince REL
 */

public boolean isPlaying() { throw new RuntimeException("Stub!"); }

/**
 * Stops the playback from {@link AudioDeviceInfo}.
 * @apiSince REL
 */

public void stop() { throw new RuntimeException("Stub!"); }
/**
 * Builder class for {@link HwAudioSource} objects.
 * Use this class to configure and create a <code>HwAudioSource</code> instance.
 * <p>Here is an example where <code>Builder</code> is used to specify an audio
 * playback directly from a source device as media usage, to be used by a new
 * <code>HwAudioSource</code> instance:
 *
 * <pre class="prettyprint">
 * HwAudioSource player = new HwAudioSource.Builder()
 *              .setAudioAttributes(new AudioAttributes.Builder()
 *                       .setUsage(AudioAttributes.USAGE_MEDIA)
 *                       .build())
 *              .setAudioDeviceInfo(device)
 *              .build()
 * </pre>
 * <p>
 * If the audio attributes are not set with {@link #setAudioAttributes(AudioAttributes)},
 * attributes comprising {@link AudioAttributes#USAGE_MEDIA} will be used.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Constructs a new Builder with default values.
 * @apiSince REL
 */

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Sets the {@link AudioAttributes}.
 * @param attributes a non-null {@link AudioAttributes} instance that describes the audio
 *     data to be played.
 * This value must never be {@code null}.
 * @return the same Builder instance.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.media.HwAudioSource.Builder setAudioAttributes(@android.annotation.NonNull android.media.AudioAttributes attributes) { throw new RuntimeException("Stub!"); }

/**
 * Sets the {@link AudioDeviceInfo}.
 * @param info a non-null {@link AudioDeviceInfo} instance that describes the audio
 *     data come from.
 * This value must never be {@code null}.
 * @return the same Builder instance.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.media.HwAudioSource.Builder setAudioDeviceInfo(@android.annotation.NonNull android.media.AudioDeviceInfo info) { throw new RuntimeException("Stub!"); }

/**
 * Builds an {@link HwAudioSource} instance initialized with all the parameters set
 * on this <code>Builder</code>.
 * @return a new successfully initialized {@link HwAudioSource} instance.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.media.HwAudioSource build() { throw new RuntimeException("Stub!"); }
}

}

