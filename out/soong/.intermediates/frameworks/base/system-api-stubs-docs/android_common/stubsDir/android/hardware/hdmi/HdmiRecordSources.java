/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.hdmi;


/**
 * Container for record source used for one touch record.
 * Use one of helper method by source type.
 * <ul>
 * <li>Own source: {@link #ofOwnSource()}
 * <li>Digital service(channel id): {@link #ofDigitalChannelId(int, DigitalChannelData)}
 * <li>Digital service(ARIB): {@link #ofArib(int, AribData)}
 * <li>Digital service(ATSC): {@link #ofAtsc(int, AtscData)}
 * <li>Digital service(DVB): {@link #ofDvb(int, DvbData)}
 * <li>Analogue: {@link #ofAnalogue(int, int, int)}
 * <li>External plug: {@link #ofExternalPlug(int)}
 * <li>External physical address: {@link #ofExternalPhysicalAddress(int)}.
 * <ul>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class HdmiRecordSources {

HdmiRecordSources() { throw new RuntimeException("Stub!"); }

/**
 * Creates {@link OwnSource} of own source.
 * @apiSince REL
 */

public static android.hardware.hdmi.HdmiRecordSources.OwnSource ofOwnSource() { throw new RuntimeException("Stub!"); }

/**
 * Checks the byte array of record source.
 * @hide
 */

public static boolean checkRecordSource(byte[] recordSource) { throw new RuntimeException("Stub!"); }
/**
 * Record source for analogue service data. It consists of
 * <ul>
 * <li>[Record Source Type] - 1 byte
 * <li>[Analogue Broadcast Type] - 1 byte
 * <li>[Analogue Frequency] - 2 bytes
 * <li>[Broadcast System] - 1 byte
 * </ul>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class AnalogueServiceSource extends android.hardware.hdmi.HdmiRecordSources.RecordSource {

AnalogueServiceSource(int broadcastType, int frequency, int broadcastSystem) { super(0, 0); throw new RuntimeException("Stub!"); }
}

/**
 * Record source container for "Digital Service".
 * <ul>
 * <li>[Record Source Type] - 1 byte
 * <li>[Digital Identification] - 7 bytes
 * </ul>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class DigitalServiceSource extends android.hardware.hdmi.HdmiRecordSources.RecordSource {

DigitalServiceSource() { super(0, 0); throw new RuntimeException("Stub!"); }
}

/**
 * Record source for external physical address.
 * <ul>
 * <li>[Record Source Type] - 1 byte
 * <li>[Physical address] - 2 byte
 * </ul>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class ExternalPhysicalAddress extends android.hardware.hdmi.HdmiRecordSources.RecordSource {

ExternalPhysicalAddress(int physicalAddress) { super(0, 0); throw new RuntimeException("Stub!"); }
}

/**
 * Record source for external plug (external non-HDMI device connect) type.
 * <ul>
 * <li>[Record Source Type] - 1 byte
 * <li>[External Plug] - 1 byte
 * </ul>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class ExternalPlugData extends android.hardware.hdmi.HdmiRecordSources.RecordSource {

ExternalPlugData(int plugNumber) { super(0, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class OwnSource extends android.hardware.hdmi.HdmiRecordSources.RecordSource {

OwnSource() { super(0, 0); throw new RuntimeException("Stub!"); }
}

/**
 * Base class for each record source.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class RecordSource {

RecordSource(int sourceType, int extraDataSize) { throw new RuntimeException("Stub!"); }
}

}

