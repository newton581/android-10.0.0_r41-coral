/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.metrics;


/**
 * An event logged when there is a change or event that requires updating the
 * the APF program in place with a new APF program.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ApfProgramEvent implements android.net.metrics.IpConnectivityLog.Event {

ApfProgramEvent(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @hide */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @hide */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }
/**
 * Utility to create an instance of {@link ApfProgramEvent}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Set the maximum computed lifetime of the program in seconds.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfProgramEvent.Builder setLifetime(long lifetime) { throw new RuntimeException("Stub!"); }

/**
 * Set the effective program lifetime in seconds.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfProgramEvent.Builder setActualLifetime(long lifetime) { throw new RuntimeException("Stub!"); }

/**
 * Set the number of RAs filtered by the APF program.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfProgramEvent.Builder setFilteredRas(int filteredRas) { throw new RuntimeException("Stub!"); }

/**
 * Set the total number of current RAs at generation time.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfProgramEvent.Builder setCurrentRas(int currentRas) { throw new RuntimeException("Stub!"); }

/**
 * Set the length of the APF program in bytes.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfProgramEvent.Builder setProgramLength(int programLength) { throw new RuntimeException("Stub!"); }

/**
 * Set the flags describing what an Apf program filters.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfProgramEvent.Builder setFlags(boolean hasIPv4, boolean multicastFilterOn) { throw new RuntimeException("Stub!"); }

/**
 * Build a new {@link ApfProgramEvent}.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ApfProgramEvent build() { throw new RuntimeException("Stub!"); }
}

}

