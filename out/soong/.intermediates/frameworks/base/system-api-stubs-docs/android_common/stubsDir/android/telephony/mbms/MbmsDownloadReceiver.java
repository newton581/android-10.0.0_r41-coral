/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.mbms;

import android.content.BroadcastReceiver;
import android.telephony.mbms.vendor.VendorUtils;
import android.content.Intent;

/**
 * The {@link BroadcastReceiver} responsible for handling intents sent from the middleware. Apps
 * that wish to download using MBMS APIs should declare this class in their AndroidManifest.xml as
 * follows:
 <pre>{@code
 <receiver
    android:name="android.telephony.mbms.MbmsDownloadReceiver"
    android:permission="android.permission.SEND_EMBMS_INTENTS"
    android:enabled="true"
    android:exported="true">
 </receiver>}</pre>
 * @apiSince 28
  */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MbmsDownloadReceiver extends android.content.BroadcastReceiver {

public MbmsDownloadReceiver() { throw new RuntimeException("Stub!"); }

/** @hide */

public void onReceive(android.content.Context context, android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Indicates that the manager was unable to notify the app of the completed download.
 * This is a fatal result code and no result extras should be expected.
 * @hide
 */

public static final int RESULT_APP_NOTIFICATION_ERROR = 6; // 0x6

/**
 * Indicates that the supplied value for {@link VendorUtils#EXTRA_TEMP_FILE_ROOT}
 * does not match what the app has stored.
 * This is a fatal result code and no result extras should be expected.
 * @hide
 */

public static final int RESULT_BAD_TEMP_FILE_ROOT = 3; // 0x3

/**
 * Indicates that the manager was unable to move the completed download to its final location.
 * This is a fatal result code and no result extras should be expected.
 * @hide
 */

public static final int RESULT_DOWNLOAD_FINALIZATION_ERROR = 4; // 0x4

/**
 * Indicates that the intent sent had an invalid action. This will be the result if
 * {@link Intent#getAction()} returns anything other than
 * {@link VendorUtils#ACTION_DOWNLOAD_RESULT_INTERNAL},
 * {@link VendorUtils#ACTION_FILE_DESCRIPTOR_REQUEST}, or
 * {@link VendorUtils#ACTION_CLEANUP}.
 * This is a fatal result code and no result extras should be expected.
 * @hide
 */

public static final int RESULT_INVALID_ACTION = 1; // 0x1

/**
 * Indicates that the intent was missing some required extras.
 * This is a fatal result code and no result extras should be expected.
 * @hide
 */

public static final int RESULT_MALFORMED_INTENT = 2; // 0x2

/**
 * Indicates that the requested operation completed without error.
 * @hide
 */

public static final int RESULT_OK = 0; // 0x0

/**
 * Indicates that the manager was unable to generate one or more of the requested file
 * descriptors.
 * This is a non-fatal result code -- some file descriptors may still be generated, but there
 * is no guarantee that they will be the same number as requested.
 * @hide
 */

public static final int RESULT_TEMP_FILE_GENERATION_ERROR = 5; // 0x5
}

