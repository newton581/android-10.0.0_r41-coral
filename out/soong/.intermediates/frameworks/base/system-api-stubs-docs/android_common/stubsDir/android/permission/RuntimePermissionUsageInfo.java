/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.permission;


/**
 * This class contains information about how a runtime permission
 * is used. A single runtime permission presented to the user may
 * correspond to multiple platform defined permissions, e.g. the
 * location permission may control both the coarse and fine platform
 * permissions.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class RuntimePermissionUsageInfo implements android.os.Parcelable {

/**
 * Creates a new instance.
 *
 * @param name The permission group name.
 * This value must never be {@code null}.
 * @param numUsers The number of apps that have used this permission.
 * @apiSince REL
 */

public RuntimePermissionUsageInfo(@android.annotation.NonNull java.lang.String name, int numUsers) { throw new RuntimeException("Stub!"); }

/**
 * @return The number of apps that accessed this permission
 * @apiSince REL
 */

public int getAppAccessCount() { throw new RuntimeException("Stub!"); }

/**
 * Gets the permission group name.
 *
 * @return The name.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.permission.RuntimePermissionUsageInfo> CREATOR;
static { CREATOR = null; }
}

