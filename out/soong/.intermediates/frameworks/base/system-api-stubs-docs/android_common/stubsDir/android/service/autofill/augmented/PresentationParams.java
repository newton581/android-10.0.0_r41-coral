/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill.augmented;

import android.view.View;

/**
 * Abstraction of a "Smart Suggestion" component responsible to embed the autofill UI provided by
 * the augmented autofill service.
 *
 * <p>The Smart Suggestion is represented by a {@link Area} object that contains the
 * dimensions the smart suggestion window, so the service can use it to calculate the size of the
 * view that will be passed to {@link FillWindow#update(Area, View, long)}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class PresentationParams {

PresentationParams() { throw new RuntimeException("Stub!"); }

/**
 * Gets the area of the suggestion strip for the given {@code metadata}
 *
 * @return strip dimensions, or {@code null} if the Smart Suggestion provider does not support
 * suggestions strip.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.service.autofill.augmented.PresentationParams.Area getSuggestionArea() { throw new RuntimeException("Stub!"); }
/**
 * Area associated with a {@link PresentationParams Smart Suggestions} provider.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class Area {

Area() { throw new RuntimeException("Stub!"); }

/**
 * Gets the area boundaries.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.graphics.Rect getBounds() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

}

