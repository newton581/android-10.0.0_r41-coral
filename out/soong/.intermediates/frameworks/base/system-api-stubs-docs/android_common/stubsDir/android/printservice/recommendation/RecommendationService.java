/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.printservice.recommendation;

import android.content.Intent;

/**
 * Base class for the print service recommendation services.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class RecommendationService extends android.app.Service {

public RecommendationService() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

protected void attachBaseContext(android.content.Context base) { throw new RuntimeException("Stub!"); }

/**
 * Update the print service recommendations.
 *
 * @param recommendations The new set of recommendations
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public final void updateRecommendations(@android.annotation.Nullable java.util.List<android.printservice.recommendation.RecommendationInfo> recommendations) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public final android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Called when the client connects to the recommendation service.
 * @apiSince REL
 */

public abstract void onConnected();

/**
 * Called when the client disconnects from the recommendation service.
 * @apiSince REL
 */

public abstract void onDisconnected();

/**
 * The {@link Intent} action that must be declared as handled by a service in its manifest for
 * the system to recognize it as a print service recommendation service.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.printservice.recommendation.RecommendationService";
}

