/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.pm;


/**
 * This class represents the state of an instant app. Instant apps can
 * be installed or uninstalled. If the app is installed you can call
 * {@link #getApplicationInfo()} to get the app info, otherwise this
 * class provides APIs to get basic app info for showing it in the UI,
 * such as permissions, label, package name.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class InstantAppInfo implements android.os.Parcelable {

/** @apiSince REL */

public InstantAppInfo(android.content.pm.ApplicationInfo appInfo, java.lang.String[] requestedPermissions, java.lang.String[] grantedPermissions) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public InstantAppInfo(java.lang.String packageName, java.lang.CharSequence label, java.lang.String[] requestedPermissions, java.lang.String[] grantedPermissions) { throw new RuntimeException("Stub!"); }

/**
 * @return The application info if the app is installed,
 *     <code>null</code> otherwise,
 * @apiSince REL
 */

@android.annotation.Nullable
public android.content.pm.ApplicationInfo getApplicationInfo() { throw new RuntimeException("Stub!"); }

/**
 * @return The package name.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * @param packageManager Package manager for loading resources.
 * This value must never be {@code null}.
 * @return Loads the label if the app is installed or returns the cached one otherwise.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.CharSequence loadLabel(@android.annotation.NonNull android.content.pm.PackageManager packageManager) { throw new RuntimeException("Stub!"); }

/**
 * @param packageManager Package manager for loading resources.
 * This value must never be {@code null}.
 * @return Loads the icon if the app is installed or returns the cached one otherwise.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.graphics.drawable.Drawable loadIcon(@android.annotation.NonNull android.content.pm.PackageManager packageManager) { throw new RuntimeException("Stub!"); }

/**
 * @return The requested permissions.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String[] getRequestedPermissions() { throw new RuntimeException("Stub!"); }

/**
 * @return The granted permissions.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String[] getGrantedPermissions() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.content.pm.InstantAppInfo> CREATOR;
static { CREATOR = null; }
}

