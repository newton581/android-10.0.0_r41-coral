/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.view.contentcapture;

import android.view.autofill.AutofillId;
import android.view.View;

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ViewNode extends android.app.assist.AssistStructure.ViewNode {

/** @hide */

ViewNode() { throw new RuntimeException("Stub!"); }

/**
 * Returns the {@link AutofillId} of this view's parent, if the parent is also part of the
 * screen observation tree.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.view.autofill.AutofillId getParentAutofillId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.view.autofill.AutofillId getAutofillId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.CharSequence getText() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getClassName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getIdPackage() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getIdType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getIdEntry() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getLeft() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTop() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getScrollX() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getScrollY() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getWidth() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getHeight() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isAssistBlocked() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isEnabled() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isClickable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isLongClickable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isContextClickable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isFocusable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isFocused() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isAccessibilityFocused() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isCheckable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isChecked() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isSelected() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isActivated() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isOpaque() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.CharSequence getContentDescription() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getHint() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextSelectionStart() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextSelectionEnd() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextColor() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextBackgroundColor() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public float getTextSize() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTextStyle() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int[] getTextLineCharOffsets() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int[] getTextLineBaselines() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getVisibility() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getInputType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getMinTextEms() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getMaxTextEms() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getMaxTextLength() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getTextIdEntry() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return Value is {@link android.view.View#AUTOFILL_TYPE_NONE}, {@link android.view.View#AUTOFILL_TYPE_TEXT}, {@link android.view.View#AUTOFILL_TYPE_TOGGLE}, {@link android.view.View#AUTOFILL_TYPE_LIST}, or {@link android.view.View#AUTOFILL_TYPE_DATE}
 * @apiSince REL
 */

public int getAutofillType() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String[] getAutofillHints() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.view.autofill.AutofillValue getAutofillValue() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.CharSequence[] getAutofillOptions() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.LocaleList getLocaleList() { throw new RuntimeException("Stub!"); }
}

