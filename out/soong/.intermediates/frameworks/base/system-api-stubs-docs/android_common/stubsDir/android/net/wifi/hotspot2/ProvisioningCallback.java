/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.wifi.hotspot2;

import android.net.wifi.WifiManager;
import android.os.Handler;

/**
 * Base class for provisioning callbacks. Should be extended by applications and set when calling
 * {@link WifiManager#startSubscriptionProvisioning(OsuProvider, ProvisioningCallback, Handler)}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ProvisioningCallback {

public ProvisioningCallback() { throw new RuntimeException("Stub!"); }

/**
 * Provisioning status for OSU failure
 *
 * @param status indicates error condition
 * @apiSince REL
 */

public abstract void onProvisioningFailure(int status);

/**
 * Provisioning status when OSU is in progress
 *
 * @param status indicates status of OSU flow
 * @apiSince REL
 */

public abstract void onProvisioningStatus(int status);

/**
 * Provisioning complete when provisioning/remediation flow completes
 * @apiSince REL
 */

public abstract void onProvisioningComplete();

/**
 * The reason code for provisioning failure when a {@link PasspointConfiguration} is failed to
 * install.
 * @apiSince REL
 */

public static final int OSU_FAILURE_ADD_PASSPOINT_CONFIGURATION = 22; // 0x16

/**
 * The reason code for Provisioning Failure due to connection failure to OSU AP.
 * @apiSince REL
 */

public static final int OSU_FAILURE_AP_CONNECTION = 1; // 0x1

/**
 * The reason code for provisioning failure due to invalid web url format for an OSU web page.
 * @apiSince REL
 */

public static final int OSU_FAILURE_INVALID_URL_FORMAT_FOR_OSU = 8; // 0x8

/**
 * The reason code for provisioning failure when there is no AAAServerTrustRoot node in a PPS
 * MO.
 * @apiSince REL
 */

public static final int OSU_FAILURE_NO_AAA_SERVER_TRUST_ROOT_NODE = 17; // 0x11

/**
 * The reason code for provisioning failure when there is no trust root certificate for AAA
 * server.
 * @apiSince REL
 */

public static final int OSU_FAILURE_NO_AAA_TRUST_ROOT_CERTIFICATE = 21; // 0x15

/**
 * The reason code for provisioning failure when there is no OSU activity to listen to
 * {@link WifiManager#ACTION_PASSPOINT_LAUNCH_OSU_VIEW} intent.
 * @apiSince REL
 */

public static final int OSU_FAILURE_NO_OSU_ACTIVITY_FOUND = 14; // 0xe

/**
 * The reason code for provisioning failure when there is no TrustRoot node for policy server in
 * a PPS MO.
 * @apiSince REL
 */

public static final int OSU_FAILURE_NO_POLICY_SERVER_TRUST_ROOT_NODE = 19; // 0x13

/**
 * The reason code for provisioning failure when there is no PPS MO.
 * MO.
 * @apiSince REL
 */

public static final int OSU_FAILURE_NO_PPS_MO = 16; // 0x10

/**
 * The reason code for provisioning failure when there is no TrustRoot node for remediation
 * server in a PPS MO.
 * @apiSince REL
 */

public static final int OSU_FAILURE_NO_REMEDIATION_SERVER_TRUST_ROOT_NODE = 18; // 0x12

/**
 * The reason code for provisioning failure when an {@link OsuProvider} is not found for
 * provisioning.
 * @apiSince REL
 */

public static final int OSU_FAILURE_OSU_PROVIDER_NOT_FOUND = 23; // 0x17

/**
 * The reason code for provisioning failure when a provisioning flow is aborted.
 * @apiSince REL
 */

public static final int OSU_FAILURE_PROVISIONING_ABORTED = 6; // 0x6

/**
 * The reason code for provisioning failure when a provisioning flow is not possible.
 * @apiSince REL
 */

public static final int OSU_FAILURE_PROVISIONING_NOT_AVAILABLE = 7; // 0x7

/**
 * The reason code for provisioning failure when failing to retrieve trust root certificates
 * used for validating server certificate for AAA, Remediation and Policy server.
 * @apiSince REL
 */

public static final int OSU_FAILURE_RETRIEVE_TRUST_ROOT_CERTIFICATES = 20; // 0x14

/**
 * The reason code for provisioning failure due to connection failure to the server.
 * @apiSince REL
 */

public static final int OSU_FAILURE_SERVER_CONNECTION = 3; // 0x3

/**
 * The reason code for invalid server URL address.
 * @apiSince REL
 */

public static final int OSU_FAILURE_SERVER_URL_INVALID = 2; // 0x2

/**
 * The reason code for provisioning failure due to invalid server certificate.
 * @apiSince REL
 */

public static final int OSU_FAILURE_SERVER_VALIDATION = 4; // 0x4

/**
 * The reason code for provisioning failure due to invalid service provider.
 * @apiSince REL
 */

public static final int OSU_FAILURE_SERVICE_PROVIDER_VERIFICATION = 5; // 0x5

/**
 * The reason code for provisioning failure when a SOAP message exchange fails.
 * @apiSince REL
 */

public static final int OSU_FAILURE_SOAP_MESSAGE_EXCHANGE = 11; // 0xb

/**
 * The reason code for provisioning failure when a redirect listener fails to start.
 * @apiSince REL
 */

public static final int OSU_FAILURE_START_REDIRECT_LISTENER = 12; // 0xc

/**
 * The reason code for provisioning failure when a redirect listener timed out to receive a HTTP
 * redirect response.
 * @apiSince REL
 */

public static final int OSU_FAILURE_TIMED_OUT_REDIRECT_LISTENER = 13; // 0xd

/**
 * The reason code for provisioning failure when a command received is not the expected command
 * type.
 * @apiSince REL
 */

public static final int OSU_FAILURE_UNEXPECTED_COMMAND_TYPE = 9; // 0x9

/**
 * The reason code for provisioning failure when the status of a SOAP message is not the
 * expected message status.
 * @apiSince REL
 */

public static final int OSU_FAILURE_UNEXPECTED_SOAP_MESSAGE_STATUS = 15; // 0xf

/**
 * The reason code for provisioning failure when a SOAP message is not the expected message
 * type.
 * @apiSince REL
 */

public static final int OSU_FAILURE_UNEXPECTED_SOAP_MESSAGE_TYPE = 10; // 0xa

/**
 * The status code for provisioning flow to indicate the OSU AP is connected.
 * @apiSince REL
 */

public static final int OSU_STATUS_AP_CONNECTED = 2; // 0x2

/**
 * The status code for provisioning flow to indicate connecting to OSU AP
 * @apiSince REL
 */

public static final int OSU_STATUS_AP_CONNECTING = 1; // 0x1

/**
 * The status code for provisioning flow to indicate starting the first SOAP exchange.
 * @apiSince REL
 */

public static final int OSU_STATUS_INIT_SOAP_EXCHANGE = 6; // 0x6

/**
 * The status code for provisioning flow to indicate a HTTP redirect response is received.
 * @apiSince REL
 */

public static final int OSU_STATUS_REDIRECT_RESPONSE_RECEIVED = 8; // 0x8

/**
 * The status code for provisioning flow to indicate starting a step retrieving trust root
 * certs.
 * @apiSince REL
 */

public static final int OSU_STATUS_RETRIEVING_TRUST_ROOT_CERTS = 11; // 0xb

/**
 * The status code for provisioning flow to indicate starting the second SOAP exchange.
 * @apiSince REL
 */

public static final int OSU_STATUS_SECOND_SOAP_EXCHANGE = 9; // 0x9

/**
 * The status code for provisioning flow to indicate the server is connected
 * @apiSince REL
 */

public static final int OSU_STATUS_SERVER_CONNECTED = 5; // 0x5

/**
 * The status code for provisioning flow to indicate connecting to the server.
 * @apiSince REL
 */

public static final int OSU_STATUS_SERVER_CONNECTING = 3; // 0x3

/**
 * The status code for provisioning flow to indicate the server certificate is validated.
 * @apiSince REL
 */

public static final int OSU_STATUS_SERVER_VALIDATED = 4; // 0x4

/**
 * The status code for provisioning flow to indicate starting the third SOAP exchange.
 * @apiSince REL
 */

public static final int OSU_STATUS_THIRD_SOAP_EXCHANGE = 10; // 0xa

/**
 * The status code for provisioning flow to indicate waiting for a HTTP redirect response.
 * @apiSince REL
 */

public static final int OSU_STATUS_WAITING_FOR_REDIRECT_RESPONSE = 7; // 0x7
}

