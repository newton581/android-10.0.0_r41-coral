/**
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.radio;

import java.util.concurrent.Executor;
import java.util.List;

/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ProgramList implements java.lang.AutoCloseable {

ProgramList() { throw new RuntimeException("Stub!"); }

/**
 * Registers list change callback with executor.
 
 * @param executor This value must never be {@code null}.
 
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 
 * @param callback This value must never be {@code null}.
 * @apiSince REL
 */

public void registerListCallback(@android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.hardware.radio.ProgramList.ListCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Registers list change callback.
 
 * @param callback This value must never be {@code null}.
 * @apiSince REL
 */

public void registerListCallback(@android.annotation.NonNull android.hardware.radio.ProgramList.ListCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Unregisters list change callback.
 
 * @param callback This value must never be {@code null}.
 * @apiSince REL
 */

public void unregisterListCallback(@android.annotation.NonNull android.hardware.radio.ProgramList.ListCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Adds list complete event listener with executor.
 
 * @param executor This value must never be {@code null}.
 
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 
 * @param listener This value must never be {@code null}.
 * @apiSince REL
 */

public void addOnCompleteListener(@android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.hardware.radio.ProgramList.OnCompleteListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Adds list complete event listener.
 
 * @param listener This value must never be {@code null}.
 * @apiSince REL
 */

public void addOnCompleteListener(@android.annotation.NonNull android.hardware.radio.ProgramList.OnCompleteListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Removes list complete event listener.
 
 * @param listener This value must never be {@code null}.
 * @apiSince REL
 */

public void removeOnCompleteListener(@android.annotation.NonNull android.hardware.radio.ProgramList.OnCompleteListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Disables list updates and releases all resources.
 * @apiSince REL
 */

public void close() { throw new RuntimeException("Stub!"); }

/**
 * Converts the program list in its current shape to the static List<>.
 *
 * @return the new List<> object; it won't receive any further updates
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.hardware.radio.RadioManager.ProgramInfo> toList() { throw new RuntimeException("Stub!"); }

/**
 * Returns the program with a specified primary identifier.
 *
 * @param id primary identifier of a program to fetch
 * This value must never be {@code null}.
 * @return the program info, or null if there is no such program on the list
 * @apiSince REL
 */

@android.annotation.Nullable
public android.hardware.radio.RadioManager.ProgramInfo get(@android.annotation.NonNull android.hardware.radio.ProgramSelector.Identifier id) { throw new RuntimeException("Stub!"); }
/**
 * Filter for the program list.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Filter implements android.os.Parcelable {

/**
 * Constructor of program list filter.
 *
 * Arrays passed to this constructor become owned by this object, do not modify them later.
 *
 * @param identifierTypes see getIdentifierTypes()
 * This value must never be {@code null}.
 * @param identifiers see getIdentifiers()
 * This value must never be {@code null}.
 * @param includeCategories see areCategoriesIncluded()
 * @param excludeModifications see areModificationsExcluded()
 * @apiSince REL
 */

public Filter(@android.annotation.NonNull java.util.Set<java.lang.Integer> identifierTypes, @android.annotation.NonNull java.util.Set<android.hardware.radio.ProgramSelector.Identifier> identifiers, boolean includeCategories, boolean excludeModifications) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Returns the list of identifier types that satisfy the filter.
 *
 * If the program list entry contains at least one identifier of the type
 * listed, it satisfies this condition.
 *
 * Empty list means no filtering on identifier type.
 *
 * @return the list of accepted identifier types, must not be modified
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.Set<java.lang.Integer> getIdentifierTypes() { throw new RuntimeException("Stub!"); }

/**
 * Returns the list of identifiers that satisfy the filter.
 *
 * If the program list entry contains at least one listed identifier,
 * it satisfies this condition.
 *
 * Empty list means no filtering on identifier.
 *
 * @return the list of accepted identifiers, must not be modified
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.Set<android.hardware.radio.ProgramSelector.Identifier> getIdentifiers() { throw new RuntimeException("Stub!"); }

/**
 * Checks, if non-tunable entries that define tree structure on the
 * program list (i.e. DAB ensembles) should be included.
 * @apiSince REL
 */

public boolean areCategoriesIncluded() { throw new RuntimeException("Stub!"); }

/**
 * Checks, if updates on entry modifications should be disabled.
 *
 * If true, 'modified' vector of ProgramListChunk must contain list
 * additions only. Once the program is added to the list, it's not
 * updated anymore.
 * @apiSince REL
 */

public boolean areModificationsExcluded() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.ProgramList.Filter> CREATOR;
static { CREATOR = null; }
}

/**
 * Callback for list change operations.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class ListCallback {

public ListCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called when item was modified or added to the list.
 
 * @param id This value must never be {@code null}.
 * @apiSince REL
 */

public void onItemChanged(@android.annotation.NonNull android.hardware.radio.ProgramSelector.Identifier id) { throw new RuntimeException("Stub!"); }

/**
 * Called when item was removed from the list.
 
 * @param id This value must never be {@code null}.
 * @apiSince REL
 */

public void onItemRemoved(@android.annotation.NonNull android.hardware.radio.ProgramSelector.Identifier id) { throw new RuntimeException("Stub!"); }
}

/**
 * Listener of list complete event.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnCompleteListener {

/**
 * Called when the list turned complete (i.e. when the scan process
 * came to an end).
 * @apiSince REL
 */

public void onComplete();
}

}

