/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.watchdog;

import android.os.Bundle;
import java.util.List;
import android.content.Intent;

/**
 * A service to provide packages supporting explicit health checks and route checks to these
 * packages on behalf of the package watchdog.
 *
 * <p>To extend this class, you must declare the service in your manifest file with the
 * {@link android.Manifest.permission#BIND_EXPLICIT_HEALTH_CHECK_SERVICE} permission,
 * and include an intent filter with the {@link #SERVICE_INTERFACE} action. In adddition,
 * your implementation must live in {@link PackageManger#SYSTEM_SHARED_LIBRARY_SERVICES}.
 * For example:</p>
 * <pre>
 *     &lt;service android:name=".FooExplicitHealthCheckService"
 *             android:exported="true"
 *             android:priority="100"
 *             android:permission="android.permission.BIND_EXPLICIT_HEALTH_CHECK_SERVICE"&gt;
 *         &lt;intent-filter&gt;
 *             &lt;action android:name="android.service.watchdog.ExplicitHealthCheckService" /&gt;
 *         &lt;/intent-filter&gt;
 *     &lt;/service&gt;
 * </pre>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ExplicitHealthCheckService extends android.app.Service {

public ExplicitHealthCheckService() { throw new RuntimeException("Stub!"); }

/**
 * Called when the system requests an explicit health check for {@code packageName}.
 *
 * <p> When {@code packageName} passes the check, implementors should call
 * {@link #notifyHealthCheckPassed} to inform the system.
 *
 * <p> It could take many hours before a {@code packageName} passes a check and implementors
 * should never drop requests unless {@link onCancel} is called or the service dies.
 *
 * <p> Requests should not be queued and additional calls while expecting a result for
 * {@code packageName} should have no effect.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onRequestHealthCheck(@android.annotation.NonNull java.lang.String packageName);

/**
 * Called when the system cancels the explicit health check request for {@code packageName}.
 * Should do nothing if there are is no active request for {@code packageName}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onCancelHealthCheck(@android.annotation.NonNull java.lang.String packageName);

/**
 * Called when the system requests for all the packages supporting explicit health checks. The
 * system may request an explicit health check for any of these packages with
 * {@link #onRequestHealthCheck}.
 *
 * @return all packages supporting explicit health checks
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public abstract java.util.List<android.service.watchdog.ExplicitHealthCheckService.PackageConfig> onGetSupportedPackages();

/**
 * Called when the system requests for all the packages that it has currently requested
 * an explicit health check for.
 *
 * @return all packages expecting an explicit health check result
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public abstract java.util.List<java.lang.String> onGetRequestedPackages();

/**
 * {@inheritDoc}
 
 * @param intent This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public final android.os.IBinder onBind(@android.annotation.NonNull android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Implementors should call this to notify the system when explicit health check passes
 * for {@code packageName};
 
 * @param packageName This value must never be {@code null}.
 * @apiSince REL
 */

public final void notifyHealthCheckPassed(@android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * The permission that a service must require to ensure that only Android system can bind to it.
 * If this permission is not enforced in the AndroidManifest of the service, the system will
 * skip that service.
 * @apiSince REL
 */

public static final java.lang.String BIND_PERMISSION = "android.permission.BIND_EXPLICIT_HEALTH_CHECK_SERVICE";

/**
 * The Intent action that a service must respond to. Add it to the intent filter of the service
 * in its manifest.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.service.watchdog.ExplicitHealthCheckService";
/**
 * A PackageConfig contains a package supporting explicit health checks and the
 * timeout in {@link System#uptimeMillis} across reboots after which health
 * check requests from clients are failed.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class PackageConfig implements android.os.Parcelable {

/**
 * Creates a new instance.
 *
 * @param packageName the package name
 * This value must never be {@code null}.
 * @param durationMillis the duration in milliseconds, must be greater than or
 * equal to 0. If it is 0, it will use a system default value.
 * @apiSince REL
 */

public PackageConfig(@android.annotation.NonNull java.lang.String packageName, long healthCheckTimeoutMillis) { throw new RuntimeException("Stub!"); }

/**
 * Gets the package name.
 *
 * @return the package name
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Gets the timeout in milliseconds to evaluate an explicit health check result after a
 * request.
 *
 * @return the duration in {@link System#uptimeMillis} across reboots
 * @apiSince REL
 */

public long getHealthCheckTimeoutMillis() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object other) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.watchdog.ExplicitHealthCheckService.PackageConfig> CREATOR;
static { CREATOR = null; }
}

}

