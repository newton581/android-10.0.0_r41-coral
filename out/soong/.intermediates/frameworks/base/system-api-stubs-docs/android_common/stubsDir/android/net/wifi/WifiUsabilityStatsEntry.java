/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.wifi;

import android.os.Parcelable;

/**
 * This class makes a subset of
 * com.android.server.wifi.nano.WifiMetricsProto.WifiUsabilityStatsEntry parcelable.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class WifiUsabilityStatsEntry implements android.os.Parcelable {

/** Constructor function {@hide} */

WifiUsabilityStatsEntry(long timeStampMillis, int rssi, int linkSpeedMbps, long totalTxSuccess, long totalTxRetries, long totalTxBad, long totalRxSuccess, long totalRadioOnTimeMillis, long totalRadioTxTimeMillis, long totalRadioRxTimeMillis, long totalScanTimeMillis, long totalNanScanTimeMillis, long totalBackgroundScanTimeMillis, long totalRoamScanTimeMillis, long totalPnoScanTimeMillis, long totalHotspot2ScanTimeMillis, long totalCcaBusyFreqTimeMillis, long totalRadioOnFreqTimeMillis, long totalBeaconRx, int probeStatusSinceLastUpdate, int probeElapsedTimeSinceLastUpdateMillis, int probeMcsRateSinceLastUpdate, int rxLinkSpeedMbps, int cellularDataNetworkType, int cellularSignalStrengthDbm, int cellularSignalStrengthDb, boolean isSameRegisteredCell) { throw new RuntimeException("Stub!"); }

/**
 * Implement the Parcelable interface
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Implement the Parcelable interface
 * @apiSince REL
 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Absolute milliseconds from device boot when these stats were sampled
 * @apiSince REL
 */

public long getTimeStampMillis() { throw new RuntimeException("Stub!"); }

/**
 * The RSSI (in dBm) at the sample time
 * @apiSince REL
 */

public int getRssi() { throw new RuntimeException("Stub!"); }

/**
 * Link speed at the sample time in Mbps
 * @apiSince REL
 */

public int getLinkSpeedMbps() { throw new RuntimeException("Stub!"); }

/**
 * The total number of tx success counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalTxSuccess() { throw new RuntimeException("Stub!"); }

/**
 * The total number of MPDU data packet retries counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalTxRetries() { throw new RuntimeException("Stub!"); }

/**
 * The total number of tx bad counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalTxBad() { throw new RuntimeException("Stub!"); }

/**
 * The total number of rx success counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalRxSuccess() { throw new RuntimeException("Stub!"); }

/**
 * The total time the wifi radio is on in ms counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalRadioOnTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * The total time the wifi radio is doing tx in ms counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalRadioTxTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * The total time the wifi radio is doing rx in ms counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalRadioRxTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * The total time spent on all types of scans in ms counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalScanTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * The total time spent on nan scans in ms counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalNanScanTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * The total time spent on background scans in ms counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalBackgroundScanTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * The total time spent on roam scans in ms counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalRoamScanTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * The total time spent on pno scans in ms counted from the last radio chip reset
 * @apiSince REL
 */

public long getTotalPnoScanTimeMillis() { throw new RuntimeException("Stub!"); }

/** The total time spent on hotspot2.0 scans and GAS exchange in ms counted from the last radio
 * chip reset     * @apiSince REL
 */

public long getTotalHotspot2ScanTimeMillis() { throw new RuntimeException("Stub!"); }

/** The total time CCA is on busy status on the current frequency in ms counted from the last
 * radio chip reset     * @apiSince REL
 */

public long getTotalCcaBusyFreqTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * The total radio on time on the current frequency from the last radio chip reset
 * @apiSince REL
 */

public long getTotalRadioOnFreqTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * The total number of beacons received from the last radio chip reset
 * @apiSince REL
 */

public long getTotalBeaconRx() { throw new RuntimeException("Stub!"); }

/**
 * The status of link probe since last stats update
 * @return Value is {@link android.net.wifi.WifiUsabilityStatsEntry#PROBE_STATUS_UNKNOWN}, {@link android.net.wifi.WifiUsabilityStatsEntry#PROBE_STATUS_NO_PROBE}, {@link android.net.wifi.WifiUsabilityStatsEntry#PROBE_STATUS_SUCCESS}, or {@link android.net.wifi.WifiUsabilityStatsEntry#PROBE_STATUS_FAILURE}
 * @apiSince REL
 */

public int getProbeStatusSinceLastUpdate() { throw new RuntimeException("Stub!"); }

/**
 * The elapsed time of the most recent link probe since last stats update
 * @apiSince REL
 */

public int getProbeElapsedTimeSinceLastUpdateMillis() { throw new RuntimeException("Stub!"); }

/**
 * The MCS rate of the most recent link probe since last stats update
 * @apiSince REL
 */

public int getProbeMcsRateSinceLastUpdate() { throw new RuntimeException("Stub!"); }

/**
 * Rx link speed at the sample time in Mbps
 * @apiSince REL
 */

public int getRxLinkSpeedMbps() { throw new RuntimeException("Stub!"); }

/**
 * Cellular data network type currently in use on the device for data transmission
 * @return Value is {@link android.telephony.TelephonyManager#NETWORK_TYPE_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IDEN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IWLAN}, android.telephony.TelephonyManager.NETWORK_TYPE_LTE_CA, or {@link android.telephony.TelephonyManager#NETWORK_TYPE_NR}
 * @apiSince REL
 */

public int getCellularDataNetworkType() { throw new RuntimeException("Stub!"); }

/**
 * Cellular signal strength in dBm, NR: CsiRsrp, LTE: Rsrp, WCDMA/TDSCDMA: Rscp,
 * CDMA: Rssi, EVDO: Rssi, GSM: Rssi
 * @apiSince REL
 */

public int getCellularSignalStrengthDbm() { throw new RuntimeException("Stub!"); }

/**
 * Cellular signal strength in dB, NR: CsiSinr, LTE: Rsrq, WCDMA: EcNo, TDSCDMA: invalid,
 * CDMA: Ecio, EVDO: SNR, GSM: invalid
 * @apiSince REL
 */

public int getCellularSignalStrengthDb() { throw new RuntimeException("Stub!"); }

/**
 * Whether the primary registered cell of current entry is same as that of previous entry
 * @apiSince REL
 */

public boolean isSameRegisteredCell() { throw new RuntimeException("Stub!"); }

/**
 * Implement the Parcelable interface
 * @apiSince REL
 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.net.wifi.WifiUsabilityStatsEntry> CREATOR;
static { CREATOR = null; }

/**
 * Link probe is triggered and the result is failure
 * @apiSince REL
 */

public static final int PROBE_STATUS_FAILURE = 3; // 0x3

/**
 * Link probe is not triggered
 * @apiSince REL
 */

public static final int PROBE_STATUS_NO_PROBE = 1; // 0x1

/**
 * Link probe is triggered and the result is success
 * @apiSince REL
 */

public static final int PROBE_STATUS_SUCCESS = 2; // 0x2

/**
 * Link probe status is unknown
 * @apiSince REL
 */

public static final int PROBE_STATUS_UNKNOWN = 0; // 0x0
}

