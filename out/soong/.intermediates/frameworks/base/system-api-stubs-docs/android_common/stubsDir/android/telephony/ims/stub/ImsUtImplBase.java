/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims.stub;


/**
 * Base implementation of IMS UT interface, which implements stubs. Override these methods to
 * implement functionality.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ImsUtImplBase {

public ImsUtImplBase() { throw new RuntimeException("Stub!"); }

/**
 * Called when the framework no longer needs to interact with the IMS UT implementation any
 * longer.
 * @apiSince REL
 */

public void close() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the call barring configuration.
 * @param cbType
 * @apiSince REL
 */

public int queryCallBarring(int cbType) { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the configuration of the call barring for specified service class.
 * @apiSince REL
 */

public int queryCallBarringForServiceClass(int cbType, int serviceClass) { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the configuration of the call forward.
 * @apiSince REL
 */

public int queryCallForward(int condition, java.lang.String number) { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the configuration of the call waiting.
 * @apiSince REL
 */

public int queryCallWaiting() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the default CLIR setting.
 * @apiSince REL
 */

public int queryClir() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the CLIP call setting.
 * @apiSince REL
 */

public int queryClip() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the COLR call setting.
 * @apiSince REL
 */

public int queryColr() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the COLP call setting.
 * @apiSince REL
 */

public int queryColp() { throw new RuntimeException("Stub!"); }

/**
 * Updates or retrieves the supplementary service configuration.
 * @apiSince REL
 */

public int transact(android.os.Bundle ssInfo) { throw new RuntimeException("Stub!"); }

/**
 * Updates the configuration of the call barring.
 * @apiSince REL
 */

public int updateCallBarring(int cbType, int action, java.lang.String[] barrList) { throw new RuntimeException("Stub!"); }

/**
 * Updates the configuration of the call barring for specified service class.
 * @apiSince REL
 */

public int updateCallBarringForServiceClass(int cbType, int action, java.lang.String[] barrList, int serviceClass) { throw new RuntimeException("Stub!"); }

/**
 * Updates the configuration of the call forward.
 * @apiSince REL
 */

public int updateCallForward(int action, int condition, java.lang.String number, int serviceClass, int timeSeconds) { throw new RuntimeException("Stub!"); }

/**
 * Updates the configuration of the call waiting.
 * @apiSince REL
 */

public int updateCallWaiting(boolean enable, int serviceClass) { throw new RuntimeException("Stub!"); }

/**
 * Updates the configuration of the CLIR supplementary service.
 * @apiSince REL
 */

public int updateClir(int clirMode) { throw new RuntimeException("Stub!"); }

/**
 * Updates the configuration of the CLIP supplementary service.
 * @apiSince REL
 */

public int updateClip(boolean enable) { throw new RuntimeException("Stub!"); }

/**
 * Updates the configuration of the COLR supplementary service.
 * @apiSince REL
 */

public int updateColr(int presentation) { throw new RuntimeException("Stub!"); }

/**
 * Updates the configuration of the COLP supplementary service.
 * @apiSince REL
 */

public int updateColp(boolean enable) { throw new RuntimeException("Stub!"); }

/**
 * Sets the listener.
 * @apiSince REL
 */

public void setListener(android.telephony.ims.ImsUtListener listener) { throw new RuntimeException("Stub!"); }
}

