/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package android.telephony.ims;


/**
 * Provides STK Call Control Supplementary Service information.
 *
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ImsSsData implements android.os.Parcelable {

/**
 * Generate IMS Supplementary Service information.
 * @param serviceType The Supplementary Service type.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsSsData#SS_CFU}, {@link android.telephony.ims.ImsSsData#SS_CF_BUSY}, {@link android.telephony.ims.ImsSsData#SS_CF_NO_REPLY}, {@link android.telephony.ims.ImsSsData#SS_CF_NOT_REACHABLE}, {@link android.telephony.ims.ImsSsData#SS_CF_ALL}, {@link android.telephony.ims.ImsSsData#SS_CF_ALL_CONDITIONAL}, {@link android.telephony.ims.ImsSsData#SS_CFUT}, {@link android.telephony.ims.ImsSsData#SS_CLIP}, {@link android.telephony.ims.ImsSsData#SS_CLIR}, {@link android.telephony.ims.ImsSsData#SS_COLP}, {@link android.telephony.ims.ImsSsData#SS_COLR}, {@link android.telephony.ims.ImsSsData#SS_CNAP}, {@link android.telephony.ims.ImsSsData#SS_WAIT}, {@link android.telephony.ims.ImsSsData#SS_BAOC}, {@link android.telephony.ims.ImsSsData#SS_BAOIC}, {@link android.telephony.ims.ImsSsData#SS_BAOIC_EXC_HOME}, {@link android.telephony.ims.ImsSsData#SS_BAIC}, {@link android.telephony.ims.ImsSsData#SS_BAIC_ROAMING}, {@link android.telephony.ims.ImsSsData#SS_ALL_BARRING}, {@link android.telephony.ims.ImsSsData#SS_OUTGOING_BARRING}, {@link android.telephony.ims.ImsSsData#SS_INCOMING_BARRING}, {@link android.telephony.ims.ImsSsData#SS_INCOMING_BARRING_DN}, and {@link android.telephony.ims.ImsSsData#SS_INCOMING_BARRING_ANONYMOUS}
 * @param requestType Supplementary Service request Type. Valid values are:
 *     {@link #SS_ACTIVATION},
 *     {@link #SS_DEACTIVATION},
 *     {@link #SS_INTERROGATION},
 *     {@link #SS_REGISTRATION},
 *     {@link #SS_ERASURE}
 * @param teleserviceType Supplementary Service teleservice type:
 *     {@link #SS_ALL_TELE_AND_BEARER_SERVICES},
 *     {@link #SS_ALL_TELESEVICES},
 *     {@link #SS_TELEPHONY},
 *     {@link #SS_ALL_DATA_TELESERVICES},
 *     {@link #SS_SMS_SERVICES},
 *     {@link #SS_ALL_TELESERVICES_EXCEPT_SMS}
 * @param serviceClass Supplementary Service service class. See See 27.007 +CCFC or +CLCK.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_NONE}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_VOICE}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_FAX}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_SMS}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_CIRCUIT_SYNC}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_CIRCUIT_ASYNC}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_PACKET_ACCESS}, and {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_PAD}
 * @param result Result of Supplementary Service operation. Valid values are 0 if the result is
 *               success, or ImsReasonInfo code if the result is a failure.
 * @apiSince REL
 */

public ImsSsData(int serviceType, int requestType, int teleserviceType, int serviceClass, int result) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isTypeCf() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isTypeUnConditional() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isTypeCw() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isTypeClip() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isTypeColr() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isTypeColp() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isTypeClir() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isTypeIcb() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isTypeBarring() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isTypeInterrogation() { throw new RuntimeException("Stub!"); }

/**
 * Supplementary Service request Type.
 
 * @return Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsSsData#SS_ACTIVATION}, {@link android.telephony.ims.ImsSsData#SS_DEACTIVATION}, {@link android.telephony.ims.ImsSsData#SS_INTERROGATION}, {@link android.telephony.ims.ImsSsData#SS_REGISTRATION}, and {@link android.telephony.ims.ImsSsData#SS_ERASURE}
 * @apiSince REL
 */

public int getRequestType() { throw new RuntimeException("Stub!"); }

/**
 * The Service type of this Supplementary service.
 
 * @return Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsSsData#SS_CFU}, {@link android.telephony.ims.ImsSsData#SS_CF_BUSY}, {@link android.telephony.ims.ImsSsData#SS_CF_NO_REPLY}, {@link android.telephony.ims.ImsSsData#SS_CF_NOT_REACHABLE}, {@link android.telephony.ims.ImsSsData#SS_CF_ALL}, {@link android.telephony.ims.ImsSsData#SS_CF_ALL_CONDITIONAL}, {@link android.telephony.ims.ImsSsData#SS_CFUT}, {@link android.telephony.ims.ImsSsData#SS_CLIP}, {@link android.telephony.ims.ImsSsData#SS_CLIR}, {@link android.telephony.ims.ImsSsData#SS_COLP}, {@link android.telephony.ims.ImsSsData#SS_COLR}, {@link android.telephony.ims.ImsSsData#SS_CNAP}, {@link android.telephony.ims.ImsSsData#SS_WAIT}, {@link android.telephony.ims.ImsSsData#SS_BAOC}, {@link android.telephony.ims.ImsSsData#SS_BAOIC}, {@link android.telephony.ims.ImsSsData#SS_BAOIC_EXC_HOME}, {@link android.telephony.ims.ImsSsData#SS_BAIC}, {@link android.telephony.ims.ImsSsData#SS_BAIC_ROAMING}, {@link android.telephony.ims.ImsSsData#SS_ALL_BARRING}, {@link android.telephony.ims.ImsSsData#SS_OUTGOING_BARRING}, {@link android.telephony.ims.ImsSsData#SS_INCOMING_BARRING}, {@link android.telephony.ims.ImsSsData#SS_INCOMING_BARRING_DN}, and {@link android.telephony.ims.ImsSsData#SS_INCOMING_BARRING_ANONYMOUS}
 * @apiSince REL
 */

public int getServiceType() { throw new RuntimeException("Stub!"); }

/**
 * Supplementary Service teleservice type.
 
 * @return Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsSsData#SS_ALL_TELE_AND_BEARER_SERVICES}, {@link android.telephony.ims.ImsSsData#SS_ALL_TELESEVICES}, {@link android.telephony.ims.ImsSsData#SS_TELEPHONY}, {@link android.telephony.ims.ImsSsData#SS_ALL_DATA_TELESERVICES}, {@link android.telephony.ims.ImsSsData#SS_SMS_SERVICES}, and {@link android.telephony.ims.ImsSsData#SS_ALL_TELESERVICES_EXCEPT_SMS}
 * @apiSince REL
 */

public int getTeleserviceType() { throw new RuntimeException("Stub!"); }

/**
 * Supplementary Service service class.
 
 * @return Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_NONE}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_VOICE}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_FAX}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_SMS}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_CIRCUIT_SYNC}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_CIRCUIT_ASYNC}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_PACKET_ACCESS}, and {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_PAD}
 * @apiSince REL
 */

public int getServiceClass() { throw new RuntimeException("Stub!"); }

/**
 * Result of Supplementary Service operation. Valid values are:
 *     {@link #RESULT_SUCCESS} if the result is success, or
 *     {@link ImsReasonInfo.UtReason} code if the result is a failure.
 
 * @return Value is {@link android.telephony.ims.ImsReasonInfo#CODE_UT_NOT_SUPPORTED}, {@link android.telephony.ims.ImsReasonInfo#CODE_UT_SERVICE_UNAVAILABLE}, {@link android.telephony.ims.ImsReasonInfo#CODE_UT_OPERATION_NOT_ALLOWED}, {@link android.telephony.ims.ImsReasonInfo#CODE_UT_NETWORK_ERROR}, {@link android.telephony.ims.ImsReasonInfo#CODE_UT_CB_PASSWORD_MISMATCH}, {@link android.telephony.ims.ImsReasonInfo#CODE_UT_SS_MODIFIED_TO_DIAL}, {@link android.telephony.ims.ImsReasonInfo#CODE_UT_SS_MODIFIED_TO_USSD}, {@link android.telephony.ims.ImsReasonInfo#CODE_UT_SS_MODIFIED_TO_SS}, or {@link android.telephony.ims.ImsReasonInfo#CODE_UT_SS_MODIFIED_TO_DIAL_VIDEO}
 * @apiSince REL
 */

public int getResult() { throw new RuntimeException("Stub!"); }

/**
 * @return an array of {@link ImsSsInfo}s associated with this supplementary service data.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.telephony.ims.ImsSsInfo> getSuppServiceInfo() { throw new RuntimeException("Stub!"); }

/**
 * @return an array of {@link ImsCallForwardInfo}s associated with this supplementary service
 * data.
 *
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.util.List<android.telephony.ims.ImsCallForwardInfo> getCallForwardInfo() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.ims.ImsSsData> CREATOR;
static { CREATOR = null; }

/**
 * Result code used if the operation was successful. See {@link #getResult()}.
 * @apiSince REL
 */

public static final int RESULT_SUCCESS = 0; // 0x0

/**
 * Service class flag for all data bearers (including
 * {@link #SERVICE_CLASS_DATA_CIRCUIT_SYNC,
 * {@link #SERVICE_CLASS_DATA_CIRCUIT_ASYNC}, {@link #SERVICE_CLASS_PACKET_ACCESS},
 * {@link #SERVICE_CLASS_PAD}}) if supported by the carrier.
 *
 * See TS 27.007 7.11 (+CCFC) and 7.4 (CLCK)
 * @apiSince REL
 */

public static final int SERVICE_CLASS_DATA = 2; // 0x2

/**
 * Service class flag for the asynchronous bearer service.
 *
 * See TS 27.007 7.11 (+CCFC) and 7.4 (CLCK)
 * @apiSince REL
 */

public static final int SERVICE_CLASS_DATA_CIRCUIT_ASYNC = 32; // 0x20

/**
 * Service class flag for the synchronous bearer service.
 *
 * See TS 27.007 7.11 (+CCFC) and 7.4 (CLCK)
 * @apiSince REL
 */

public static final int SERVICE_CLASS_DATA_CIRCUIT_SYNC = 16; // 0x10

/**
 * Service class flag for the packet access bearer service.
 *
 * See TS 27.007 7.11 (+CCFC) and 7.4 (CLCK)
 * @apiSince REL
 */

public static final int SERVICE_CLASS_DATA_PACKET_ACCESS = 64; // 0x40

/**
 * Service class flag for the Packet Assembly/Disassembly bearer service.
 *
 * See TS 27.007 7.11 (+CCFC) and 7.4 (CLCK)
 * @apiSince REL
 */

public static final int SERVICE_CLASS_DATA_PAD = 128; // 0x80

/**
 * Service class flag for fax services.
 *
 * See TS 27.007 7.11 (+CCFC) and 7.4 (CLCK)
 * @apiSince REL
 */

public static final int SERVICE_CLASS_FAX = 4; // 0x4

/**
 * No call forwarding service class defined.
 *
 * See TS 27.007 7.11 (+CCFC) and 7.4 (CLCK)
 * @apiSince REL
 */

public static final int SERVICE_CLASS_NONE = 0; // 0x0

/**
 * Service class flag for SMS services.
 *
 * See TS 27.007 7.11 (+CCFC) and 7.4 (CLCK)
 * @apiSince REL
 */

public static final int SERVICE_CLASS_SMS = 8; // 0x8

/**
 * Service class flag for voice telephony.
 *
 * See TS 27.007 7.11 (+CCFC) and 7.4 (CLCK)
 * @apiSince REL
 */

public static final int SERVICE_CLASS_VOICE = 1; // 0x1

/** @apiSince REL */

public static final int SS_ACTIVATION = 0; // 0x0

/** @apiSince REL */

public static final int SS_ALL_BARRING = 18; // 0x12

/** @apiSince REL */

public static final int SS_ALL_DATA_TELESERVICES = 3; // 0x3

/** @apiSince REL */

public static final int SS_ALL_TELESERVICES_EXCEPT_SMS = 5; // 0x5

/** @apiSince REL */

public static final int SS_ALL_TELESEVICES = 1; // 0x1

/** @apiSince REL */

public static final int SS_ALL_TELE_AND_BEARER_SERVICES = 0; // 0x0

/** @apiSince REL */

public static final int SS_BAIC = 16; // 0x10

/** @apiSince REL */

public static final int SS_BAIC_ROAMING = 17; // 0x11

/** @apiSince REL */

public static final int SS_BAOC = 13; // 0xd

/** @apiSince REL */

public static final int SS_BAOIC = 14; // 0xe

/** @apiSince REL */

public static final int SS_BAOIC_EXC_HOME = 15; // 0xf

/** @apiSince REL */

public static final int SS_CFU = 0; // 0x0

/** @apiSince REL */

public static final int SS_CFUT = 6; // 0x6

/** @apiSince REL */

public static final int SS_CF_ALL = 4; // 0x4

/** @apiSince REL */

public static final int SS_CF_ALL_CONDITIONAL = 5; // 0x5

/** @apiSince REL */

public static final int SS_CF_BUSY = 1; // 0x1

/** @apiSince REL */

public static final int SS_CF_NOT_REACHABLE = 3; // 0x3

/** @apiSince REL */

public static final int SS_CF_NO_REPLY = 2; // 0x2

/** @apiSince REL */

public static final int SS_CLIP = 7; // 0x7

/** @apiSince REL */

public static final int SS_CLIR = 8; // 0x8

/** @apiSince REL */

public static final int SS_CNAP = 11; // 0xb

/** @apiSince REL */

public static final int SS_COLP = 9; // 0x9

/** @apiSince REL */

public static final int SS_COLR = 10; // 0xa

/** @apiSince REL */

public static final int SS_DEACTIVATION = 1; // 0x1

/** @apiSince REL */

public static final int SS_ERASURE = 4; // 0x4

/** @apiSince REL */

public static final int SS_INCOMING_BARRING = 20; // 0x14

/** @apiSince REL */

public static final int SS_INCOMING_BARRING_ANONYMOUS = 22; // 0x16

/** @apiSince REL */

public static final int SS_INCOMING_BARRING_DN = 21; // 0x15

/** @apiSince REL */

public static final int SS_INTERROGATION = 2; // 0x2

/** @apiSince REL */

public static final int SS_OUTGOING_BARRING = 19; // 0x13

/** @apiSince REL */

public static final int SS_REGISTRATION = 3; // 0x3

/** @apiSince REL */

public static final int SS_SMS_SERVICES = 4; // 0x4

/** @apiSince REL */

public static final int SS_TELEPHONY = 2; // 0x2

/** @apiSince REL */

public static final int SS_WAIT = 12; // 0xc
/**
 * Builder for optional ImsSsData parameters.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Generate IMS Supplementary Service information.
 * @param serviceType The Supplementary Service type.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsSsData#SS_CFU}, {@link android.telephony.ims.ImsSsData#SS_CF_BUSY}, {@link android.telephony.ims.ImsSsData#SS_CF_NO_REPLY}, {@link android.telephony.ims.ImsSsData#SS_CF_NOT_REACHABLE}, {@link android.telephony.ims.ImsSsData#SS_CF_ALL}, {@link android.telephony.ims.ImsSsData#SS_CF_ALL_CONDITIONAL}, {@link android.telephony.ims.ImsSsData#SS_CFUT}, {@link android.telephony.ims.ImsSsData#SS_CLIP}, {@link android.telephony.ims.ImsSsData#SS_CLIR}, {@link android.telephony.ims.ImsSsData#SS_COLP}, {@link android.telephony.ims.ImsSsData#SS_COLR}, {@link android.telephony.ims.ImsSsData#SS_CNAP}, {@link android.telephony.ims.ImsSsData#SS_WAIT}, {@link android.telephony.ims.ImsSsData#SS_BAOC}, {@link android.telephony.ims.ImsSsData#SS_BAOIC}, {@link android.telephony.ims.ImsSsData#SS_BAOIC_EXC_HOME}, {@link android.telephony.ims.ImsSsData#SS_BAIC}, {@link android.telephony.ims.ImsSsData#SS_BAIC_ROAMING}, {@link android.telephony.ims.ImsSsData#SS_ALL_BARRING}, {@link android.telephony.ims.ImsSsData#SS_OUTGOING_BARRING}, {@link android.telephony.ims.ImsSsData#SS_INCOMING_BARRING}, {@link android.telephony.ims.ImsSsData#SS_INCOMING_BARRING_DN}, and {@link android.telephony.ims.ImsSsData#SS_INCOMING_BARRING_ANONYMOUS}
 * @param requestType Supplementary Service request Type:
 *     {@link #SS_ACTIVATION},
 *     {@link #SS_DEACTIVATION},
 *     {@link #SS_INTERROGATION},
 *     {@link #SS_REGISTRATION},
 *     {@link #SS_ERASURE}
 * @param teleserviceType Supplementary Service teleservice type:
 *     {@link #SS_ALL_TELE_AND_BEARER_SERVICES},
 *     {@link #SS_ALL_TELESEVICES},
 *     {@link #SS_TELEPHONY},
 *     {@link #SS_ALL_DATA_TELESERVICES},
 *     {@link #SS_SMS_SERVICES},
 *     {@link #SS_ALL_TELESERVICES_EXCEPT_SMS}
 * @param serviceClass Supplementary Service service class. See See 27.007 +CCFC or +CLCK.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_NONE}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_VOICE}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_FAX}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_SMS}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_CIRCUIT_SYNC}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_CIRCUIT_ASYNC}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_PACKET_ACCESS}, and {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_PAD}
 * @param result Result of Supplementary Service operation. Valid values are 0 if the result
 *               is success, or {@link ImsReasonInfo} code if the result is a failure.
 * @return this Builder instance for further constructing.
 * @see #build()
 * @apiSince REL
 */

public Builder(int serviceType, int requestType, int teleserviceType, int serviceClass, int result) { throw new RuntimeException("Stub!"); }

/**
 * Set the array of {@link ImsSsInfo}s that are associated with this supplementary service
 * data.
 
 * @param imsSsInfos This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.ImsSsData.Builder setSuppServiceInfo(@android.annotation.NonNull java.util.List<android.telephony.ims.ImsSsInfo> imsSsInfos) { throw new RuntimeException("Stub!"); }

/**
 * Set the array of {@link ImsCallForwardInfo}s that are associated with this supplementary
 * service data.
 
 * @param imsCallForwardInfos This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.ImsSsData.Builder setCallForwardingInfo(@android.annotation.NonNull java.util.List<android.telephony.ims.ImsCallForwardInfo> imsCallForwardInfos) { throw new RuntimeException("Stub!"); }

/**
 * @return an {@link ImsSsData} containing optional parameters.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.ImsSsData build() { throw new RuntimeException("Stub!"); }
}

}

