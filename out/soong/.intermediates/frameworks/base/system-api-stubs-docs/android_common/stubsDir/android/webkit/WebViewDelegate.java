/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.webkit;

import android.graphics.Canvas;
import android.graphics.RecordingCanvas;

/**
 * Delegate used by the WebView provider implementation to access
 * the required framework functionality needed to implement a {@link WebView}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class WebViewDelegate {

WebViewDelegate() { throw new RuntimeException("Stub!"); }

/**
 * Register a callback to be invoked when tracing for the WebView component has been
 * enabled/disabled.
 * @apiSince REL
 */

public void setOnTraceEnabledChangeListener(android.webkit.WebViewDelegate.OnTraceEnabledChangeListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the WebView trace tag is enabled and {@code false} otherwise.
 * @apiSince REL
 */

public boolean isTraceTagEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the draw GL functor can be invoked (see {@link #invokeDrawGlFunctor})
 * and {@code false} otherwise.
 *
 * @deprecated Use {@link #drawWebViewFunctor(Canvas, int)}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean canInvokeDrawGlFunctor(android.view.View containerView) { throw new RuntimeException("Stub!"); }

/**
 * Invokes the draw GL functor. If waitForCompletion is {@code false} the functor
 * may be invoked asynchronously.
 *
 * @param nativeDrawGLFunctor the pointer to the native functor that implements
 *        system/core/include/utils/Functor.h
 * @deprecated Use {@link #drawWebViewFunctor(Canvas, int)}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void invokeDrawGlFunctor(android.view.View containerView, long nativeDrawGLFunctor, boolean waitForCompletion) { throw new RuntimeException("Stub!"); }

/**
 * Calls the function specified with the nativeDrawGLFunctor functor pointer. This
 * functionality is used by the WebView for calling into their renderer from the
 * framework display lists.
 *
 * @param canvas a hardware accelerated canvas (see {@link Canvas#isHardwareAccelerated()})
 * @param nativeDrawGLFunctor the pointer to the native functor that implements
 *        system/core/include/utils/Functor.h
 * @throws IllegalArgumentException if the canvas is not hardware accelerated
 * @deprecated Use {@link #drawWebViewFunctor(Canvas, int)}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void callDrawGlFunction(android.graphics.Canvas canvas, long nativeDrawGLFunctor) { throw new RuntimeException("Stub!"); }

/**
 * Calls the function specified with the nativeDrawGLFunctor functor pointer. This
 * functionality is used by the WebView for calling into their renderer from the
 * framework display lists.
 *
 * @param canvas a hardware accelerated canvas (see {@link Canvas#isHardwareAccelerated()})
 * This value must never be {@code null}.
 * @param nativeDrawGLFunctor the pointer to the native functor that implements
 *        system/core/include/utils/Functor.h
 * @param releasedRunnable Called when this nativeDrawGLFunctor is no longer referenced by this
 *        canvas, so is safe to be destroyed.
 * This value may be {@code null}.
 * @throws IllegalArgumentException if the canvas is not hardware accelerated
 * @deprecated Use {@link #drawWebViewFunctor(Canvas, int)}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void callDrawGlFunction(@android.annotation.NonNull android.graphics.Canvas canvas, long nativeDrawGLFunctor, @android.annotation.Nullable java.lang.Runnable releasedRunnable) { throw new RuntimeException("Stub!"); }

/**
 * Call webview draw functor. See API in draw_fn.h.
 * @param canvas a {@link RecordingCanvas}.
 * This value must never be {@code null}.
 * @param functor created by AwDrawFn_CreateFunctor in draw_fn.h.
 * @apiSince REL
 */

public void drawWebViewFunctor(@android.annotation.NonNull android.graphics.Canvas canvas, int functor) { throw new RuntimeException("Stub!"); }

/**
 * Detaches the draw GL functor.
 *
 * @param nativeDrawGLFunctor the pointer to the native functor that implements
 *        system/core/include/utils/Functor.h
 * @deprecated Use {@link #drawWebViewFunctor(Canvas, int)}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void detachDrawGlFunctor(android.view.View containerView, long nativeDrawGLFunctor) { throw new RuntimeException("Stub!"); }

/**
 * Returns the package id of the given {@code packageName}.
 * @apiSince REL
 */

public int getPackageId(android.content.res.Resources resources, java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Returns the application which is embedding the WebView.
 * @apiSince REL
 */

public android.app.Application getApplication() { throw new RuntimeException("Stub!"); }

/**
 * Returns the error string for the given {@code errorCode}.
 * @apiSince REL
 */

public java.lang.String getErrorString(android.content.Context context, int errorCode) { throw new RuntimeException("Stub!"); }

/**
 * Adds the WebView asset path to {@link android.content.res.AssetManager}.
 * @apiSince REL
 */

public void addWebViewAssetPath(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Returns whether WebView should run in multiprocess mode.
 * @apiSince REL
 */

public boolean isMultiProcessEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Returns the data directory suffix to use, or null for none.
 * @apiSince REL
 */

public java.lang.String getDataDirectorySuffix() { throw new RuntimeException("Stub!"); }
/**
 * Listener that gets notified whenever tracing has been enabled/disabled.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnTraceEnabledChangeListener {

/** @apiSince REL */

public void onTraceEnabledChange(boolean enabled);
}

}

