/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.pm;

import android.os.Bundle;
import android.content.Intent;

/**
 * Describes an externally resolvable instant application. There are three states that this class
 * can represent: <p/>
 * <ul>
 *     <li>
 *         The first, usable only for non http/s intents, implies that the resolver cannot
 *         immediately resolve this intent and would prefer that resolution be deferred to the
 *         instant app installer. Represent this state with {@link #InstantAppResolveInfo(Bundle)}.
 *         If the {@link android.content.Intent} has the scheme set to http/s and a set of digest
 *         prefixes were passed into one of the resolve methods in
 *         {@link android.app.InstantAppResolverService}, this state cannot be used.
 *     </li>
 *     <li>
 *         The second represents a partial match and is constructed with any of the other
 *         constructors. By setting one or more of the {@link Nullable}arguments to null, you
 *         communicate to the resolver in response to
 *         {@link android.app.InstantAppResolverService#onGetInstantAppResolveInfo(Intent, int[],
 *                String, InstantAppResolverService.InstantAppResolutionCallback)}
 *         that you need a 2nd round of resolution to complete the request.
 *     </li>
 *     <li>
 *         The third represents a complete match and is constructed with all @Nullable parameters
 *         populated.
 *     </li>
 * </ul>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class InstantAppResolveInfo implements android.os.Parcelable {

/**
 * Constructor for intent-based InstantApp resolution results.
 * @param digest This value must never be {@code null}.
 
 * @param packageName This value may be {@code null}.
 
 * @param filters This value may be {@code null}.
 * @apiSince REL
 */

public InstantAppResolveInfo(@android.annotation.NonNull android.content.pm.InstantAppResolveInfo.InstantAppDigest digest, @android.annotation.Nullable java.lang.String packageName, @android.annotation.Nullable java.util.List<android.content.pm.InstantAppIntentFilter> filters, int versionCode) { throw new RuntimeException("Stub!"); }

/**
 * Constructor for intent-based InstantApp resolution results with extras.
 * @param digest This value must never be {@code null}.
 
 * @param packageName This value may be {@code null}.
 
 * @param filters This value may be {@code null}.
 
 * @param extras This value may be {@code null}.
 * @apiSince REL
 */

public InstantAppResolveInfo(@android.annotation.NonNull android.content.pm.InstantAppResolveInfo.InstantAppDigest digest, @android.annotation.Nullable java.lang.String packageName, @android.annotation.Nullable java.util.List<android.content.pm.InstantAppIntentFilter> filters, long versionCode, @android.annotation.Nullable android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Constructor for intent-based InstantApp resolution results by hostname.
 * @param hostName This value must never be {@code null}.
 
 * @param packageName This value may be {@code null}.
 
 * @param filters This value may be {@code null}.
 * @apiSince REL
 */

public InstantAppResolveInfo(@android.annotation.NonNull java.lang.String hostName, @android.annotation.Nullable java.lang.String packageName, @android.annotation.Nullable java.util.List<android.content.pm.InstantAppIntentFilter> filters) { throw new RuntimeException("Stub!"); }

/**
 * Constructor that indicates that resolution could be delegated to the installer when the
 * sanitized intent contains enough information to resolve completely.
 
 * @param extras This value may be {@code null}.
 * @apiSince REL
 */

public InstantAppResolveInfo(@android.annotation.Nullable android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if the resolver is aware that an app may match, but would prefer
 * that the installer get the sanitized intent to decide. This should not be true for
 * resolutions that include a host and will be ignored in such cases.
 * @apiSince REL
 */

public boolean shouldLetInstallerDecide() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public byte[] getDigestBytes() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getDigestPrefix() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.util.List<android.content.pm.InstantAppIntentFilter> getIntentFilters() { throw new RuntimeException("Stub!"); }

/**
 * @deprecated Use {@link #getLongVersionCode} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getVersionCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long getLongVersionCode() { throw new RuntimeException("Stub!"); }

/**
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.content.pm.InstantAppResolveInfo> CREATOR;
static { CREATOR = null; }
/**
 * Helper class to generate and store each of the digests and prefixes
 * sent to the Instant App Resolver.
 * <p>
 * Since intent filters may want to handle multiple hosts within a
 * domain [eg “*.google.com”], the resolver is presented with multiple
 * hash prefixes. For example, "a.b.c.d.e" generates digests for
 * "d.e", "c.d.e", "b.c.d.e" and "a.b.c.d.e".
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class InstantAppDigest implements android.os.Parcelable {

/**
 * @param hostName This value must never be {@code null}.
 * @apiSince REL
 */

public InstantAppDigest(@android.annotation.NonNull java.lang.String hostName) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public byte[][] getDigestBytes() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int[] getDigestPrefix() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.content.pm.InstantAppResolveInfo.InstantAppDigest> CREATOR;
static { CREATOR = null; }

/**
 * A special instance that represents and undefined digest used for cases that a host was
 * not provided or is irrelevant to the response.
 * @apiSince REL
 */

public static final android.content.pm.InstantAppResolveInfo.InstantAppDigest UNDEFINED;
static { UNDEFINED = null; }
}

}

