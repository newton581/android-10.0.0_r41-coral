/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims.stub;

import android.telephony.ims.ImsReasonInfo;
import android.net.Uri;

/**
 * Controls IMS registration for this ImsService and notifies the framework when the IMS
 * registration for this ImsService has changed status.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ImsRegistrationImplBase {

public ImsRegistrationImplBase() { throw new RuntimeException("Stub!"); }

/**
 * Notify the framework that the device is connected to the IMS network.
 *
 * @param imsRadioTech the radio access technology. Valid values are defined as
 * {@link #REGISTRATION_TECH_LTE} and {@link #REGISTRATION_TECH_IWLAN}.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @apiSince REL
 */

public final void onRegistered(int imsRadioTech) { throw new RuntimeException("Stub!"); }

/**
 * Notify the framework that the device is trying to connect the IMS network.
 *
 * @param imsRadioTech the radio access technology. Valid values are defined as
 * {@link #REGISTRATION_TECH_LTE} and {@link #REGISTRATION_TECH_IWLAN}.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @apiSince REL
 */

public final void onRegistering(int imsRadioTech) { throw new RuntimeException("Stub!"); }

/**
 * Notify the framework that the device is disconnected from the IMS network.
 * <p>
 * Note: Prior to calling {@link #onDeregistered(ImsReasonInfo)}, you should ensure that any
 * changes to {@link android.telephony.ims.feature.ImsFeature} capability availability is sent
 * to the framework.  For example,
 * {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}
 * and
 * {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}
 * may be set to unavailable to ensure the framework knows these services are no longer
 * available due to de-registration.  If you do not report capability changes impacted by
 * de-registration, the framework will not know which features are no longer available as a
 * result.
 *
 * @param info the {@link ImsReasonInfo} associated with why registration was disconnected.
 * @apiSince REL
 */

public final void onDeregistered(android.telephony.ims.ImsReasonInfo info) { throw new RuntimeException("Stub!"); }

/**
 * Notify the framework that the handover from the current radio technology to the technology
 * defined in {@code imsRadioTech} has failed.
 * @param imsRadioTech The technology that has failed to be changed. Valid values are
 * {@link #REGISTRATION_TECH_LTE} and {@link #REGISTRATION_TECH_IWLAN}.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @param info The {@link ImsReasonInfo} for the failure to change technology.
 * @apiSince REL
 */

public final void onTechnologyChangeFailed(int imsRadioTech, android.telephony.ims.ImsReasonInfo info) { throw new RuntimeException("Stub!"); }

/**
 * The this device's subscriber associated {@link Uri}s have changed, which are used to filter
 * out this device's {@link Uri}s during conference calling.
 * @param uris
 * @apiSince REL
 */

public final void onSubscriberAssociatedUriChanged(android.net.Uri[] uris) { throw new RuntimeException("Stub!"); }

/**
 * IMS is registered to IMS via IWLAN.
 * @apiSince REL
 */

public static final int REGISTRATION_TECH_IWLAN = 1; // 0x1

/**
 * IMS is registered to IMS via LTE.
 * @apiSince REL
 */

public static final int REGISTRATION_TECH_LTE = 0; // 0x0

/**
 * No registration technology specified, used when we are not registered.
 * @apiSince REL
 */

public static final int REGISTRATION_TECH_NONE = -1; // 0xffffffff
}

