/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.wifi;

import android.os.Parcelable;

/**
 * Connection Statistics For a WiFi Network.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class WifiNetworkConnectionStatistics implements android.os.Parcelable {

/** @apiSince REL */

public WifiNetworkConnectionStatistics(int connection, int usage) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public WifiNetworkConnectionStatistics() { throw new RuntimeException("Stub!"); }

/**
 * copy constructor
 * @apiSince REL
 */

public WifiNetworkConnectionStatistics(android.net.wifi.WifiNetworkConnectionStatistics source) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Implement the Parcelable interface
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Implement the Parcelable interface
 * @apiSince REL
 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Implement the Parcelable interface
 * @apiSince REL
 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.net.wifi.WifiNetworkConnectionStatistics> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public int numConnection;

/** @apiSince REL */

public int numUsage;
}

