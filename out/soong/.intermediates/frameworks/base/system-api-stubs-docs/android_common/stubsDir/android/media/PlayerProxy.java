/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media;


/**
 * Class to remotely control a player.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class PlayerProxy {

/**
 * @hide
 * Constructor. Proxy for this player associated with this AudioPlaybackConfiguration
 * @param conf the configuration being proxied.
 */

PlayerProxy(@android.annotation.NonNull android.media.AudioPlaybackConfiguration apc) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public void start() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public void pause() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public void stop() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * @param vol
 */

public void setVolume(float vol) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * @param pan
 */

public void setPan(float pan) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * @param delayMs
 */

public void setStartDelayMs(int delayMs) { throw new RuntimeException("Stub!"); }
}

