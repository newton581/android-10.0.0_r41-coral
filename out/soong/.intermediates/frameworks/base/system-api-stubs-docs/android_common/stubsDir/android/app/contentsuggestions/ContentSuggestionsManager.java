/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.contentsuggestions;

import java.util.concurrent.Executor;
import android.os.Bundle;

/**
 * When provided with content from an app, can suggest selections and classifications of that
 * content.
 *
 * <p>The content is mainly a snapshot of a running task, the selections will be text and image
 * selections with that image content. These mSelections can then be classified to find actions and
 * entities on those selections.
 *
 * <p>Only accessible to blessed components such as Overview.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ContentSuggestionsManager {

ContentSuggestionsManager() { throw new RuntimeException("Stub!"); }

/**
 * Hints to the system that a new context image for the provided task should be sent to the
 * system content suggestions service.
 *
 * @param taskId of the task to snapshot.
 * @param imageContextRequestExtras sent with request to provide implementation specific
 *                                  extra information.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void provideContextImage(int taskId, @android.annotation.NonNull android.os.Bundle imageContextRequestExtras) { throw new RuntimeException("Stub!"); }

/**
 * Suggest content selections, based on the provided task id and optional
 * location on screen provided in the request. Called after provideContextImage().
 * The result can be passed to
 * {@link #classifyContentSelections(ClassificationsRequest, Executor, ClassificationsCallback)}
 *  to classify actions and entities on these selections.
 *
 * @param request containing the task and point location.
 * This value must never be {@code null}.
 * @param callbackExecutor to execute the provided callback on.
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback to receive the selections.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void suggestContentSelections(@android.annotation.NonNull android.app.contentsuggestions.SelectionsRequest request, @android.annotation.NonNull java.util.concurrent.Executor callbackExecutor, @android.annotation.NonNull android.app.contentsuggestions.ContentSuggestionsManager.SelectionsCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Classify actions and entities in content selections, as returned from
 * suggestContentSelections. Note these selections may be modified by the
 * caller before being passed here.
 *
 * @param request containing the selections to classify.
 * This value must never be {@code null}.
 * @param callbackExecutor to execute the provided callback on.
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback to receive the classifications.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void classifyContentSelections(@android.annotation.NonNull android.app.contentsuggestions.ClassificationsRequest request, @android.annotation.NonNull java.util.concurrent.Executor callbackExecutor, @android.annotation.NonNull android.app.contentsuggestions.ContentSuggestionsManager.ClassificationsCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Report telemetry for interaction with suggestions / classifications.
 *
 * @param requestId the id for the associated interaction
 * This value must never be {@code null}.
 * @param interaction to report back to the system content suggestions service.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void notifyInteraction(@android.annotation.NonNull java.lang.String requestId, @android.annotation.NonNull android.os.Bundle interaction) { throw new RuntimeException("Stub!"); }

/**
 * Indicates that Content Suggestions is available and enabled for the provided user. That is,
 * has an implementation and not disabled through device management.
 *
 * @return {@code true} if Content Suggestions is enabled and available for the provided user.
 * @apiSince REL
 */

public boolean isEnabled() { throw new RuntimeException("Stub!"); }
/**
 * Callback to receive classifications from
 * {@link #classifyContentSelections(ClassificationsRequest, Executor, ClassificationsCallback)}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface ClassificationsCallback {

/**
 * Async callback called when the content suggestions service has classified selections. The
 * contents of the classification is implementation dependent.
 *
 * @param statusCode as defined by the implementation of content suggestions service.
 * @param classifications not {@code null}, but can be size {@code 0}.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onContentClassificationsAvailable(int statusCode, @android.annotation.NonNull java.util.List<android.app.contentsuggestions.ContentClassification> classifications);
}

/**
 * Callback to receive content selections from
 *  {@link #suggestContentSelections(SelectionsRequest, Executor, SelectionsCallback)}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface SelectionsCallback {

/**
 * Async callback called when the content suggestions service has selections available.
 * These can be modified and sent back to the manager for classification. The contents of
 * the selection is implementation dependent.
 *
 * @param statusCode as defined by the implementation of content suggestions service.
 * @param selections not {@code null}, but can be size {@code 0}.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onContentSelectionsAvailable(int statusCode, @android.annotation.NonNull java.util.List<android.app.contentsuggestions.ContentSelection> selections);
}

}

