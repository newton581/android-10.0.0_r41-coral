/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.sms;

import android.database.CursorWindow;
import android.content.Intent;

/**
 * A service to support sms messages read for financial apps.
 *
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class FinancialSmsService extends android.app.Service {

/** @hide */

FinancialSmsService() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onCreate() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Get sms messages for financial apps.
 *
 * @param params parameters passed in by the calling app.
 * This value must never be {@code null}.
 * @return the {@code CursorWindow} with all sms messages for the app to read.
 *
 * {@hide}
 
 * This value may be {@code null}.
 */

@android.annotation.Nullable
public abstract android.database.CursorWindow onGetSmsMessages(@android.annotation.NonNull android.os.Bundle params);

/**
 * The {@link Intent} action that must be declared as handled by a service
 * in its manifest for the system to recognize it as a quota providing
 * service.
 * @apiSince REL
 */

public static final java.lang.String ACTION_FINANCIAL_SERVICE_INTENT = "android.service.sms.action.FINANCIAL_SERVICE_INTENT";
}

