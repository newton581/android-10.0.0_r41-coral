/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;

import android.Manifest;
import java.util.List;
import android.telecom.PhoneAccountHandle;
import java.util.Map;
import android.telecom.TelecomManager;
import android.telephony.VisualVoicemailService.VisualVoicemailTask;
import android.app.PendingIntent;
import android.os.Binder;
import android.os.Build;
import java.util.concurrent.Executor;
import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.SettingNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.net.ConnectivityManager;
import android.os.RemoteException;
import android.telephony.ims.stub.ImsRegistrationImplBase;
import java.util.Locale;
import android.telecom.PhoneAccount;
import android.service.carrier.CarrierIdentifier;
import android.telephony.emergency.EmergencyNumber;
import android.util.Pair;
import android.os.ResultReceiver;
import android.content.Intent;
import android.telecom.InCallService;
import android.telecom.CallScreeningService;
import java.util.regex.Pattern;

/**
 * Provides access to information about the telephony services on
 * the device. Applications can use the methods in this class to
 * determine telephony services and states, as well as to access some
 * types of subscriber information. Applications can also register
 * a listener to receive notification of telephony state changes.
 * <p>
 * The returned TelephonyManager will use the default subscription for all calls.
 * To call an API for a specific subscription, use {@link #createForSubscriptionId(int)}. e.g.
 * <code>
 *   telephonyManager = defaultSubTelephonyManager.createForSubscriptionId(subId);
 * </code>
 * <p>
 * Note that access to some telephony information is
 * permission-protected. Your application cannot access the protected
 * information unless it has the appropriate permissions declared in
 * its manifest file. Where permissions apply, they are noted in the
 * the methods through which you access the protected information.
 * @apiSince 1
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class TelephonyManager {

/** @hide */

TelephonyManager() { throw new RuntimeException("Stub!"); }

/**
 * Returns the number of phones available.
 * Returns 0 if none of voice, sms, data is not supported
 * Returns 1 for Single standby mode (Single SIM functionality)
 * Returns 2 for Dual standby mode.(Dual SIM functionality)
 * Returns 3 for Tri standby mode.(Tri SIM functionality)
 * @apiSince 23
 */

public int getPhoneCount() { throw new RuntimeException("Stub!"); }

/**
 * Create a new TelephonyManager object pinned to the given subscription ID.
 *
 * @return a TelephonyManager that uses the given subId for all calls.
 * @apiSince 24
 */

public android.telephony.TelephonyManager createForSubscriptionId(int subId) { throw new RuntimeException("Stub!"); }

/**
 * Create a new TelephonyManager object pinned to the subscription ID associated with the given
 * phone account.
 *
 * @return a TelephonyManager that uses the given phone account for all calls, or {@code null}
 * if the phone account does not correspond to a valid subscription ID.
 * @apiSince 26
 */

@android.annotation.Nullable
public android.telephony.TelephonyManager createForPhoneAccountHandle(android.telecom.PhoneAccountHandle phoneAccountHandle) { throw new RuntimeException("Stub!"); }

/**
 * Returns the software version number for the device, for example,
 * the IMEI/SV for GSM phones. Return null if the software version is
 * not available.
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @apiSince 1
 */

public java.lang.String getDeviceSoftwareVersion() { throw new RuntimeException("Stub!"); }

/**
 * Returns the unique device ID, for example, the IMEI for GSM and the MEID
 * or ESN for CDMA phones. Return null if device ID is not available.
 *
 * <p>Requires Permission: READ_PRIVILEGED_PHONE_STATE, for the calling app to be the device or
 * profile owner and have the READ_PHONE_STATE permission, or that the calling app has carrier
 * privileges (see {@link #hasCarrierPrivileges}) on any active subscription. The profile owner
 * is an app that owns a managed profile on the device; for more details see <a
 * href="https://developer.android.com/work/managed-profiles">Work profiles</a>. Profile owner
 * access is deprecated and will be removed in a future release.
 *
 * <p>If the calling app does not meet one of these requirements then this method will behave
 * as follows:
 *
 * <ul>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app has the
 *     READ_PHONE_STATE permission then null is returned.</li>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app does not have
 *     the READ_PHONE_STATE permission, or if the calling app is targeting API level 29 or
 *     higher, then a SecurityException is thrown.</li>
 * </ul>
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @deprecated Use {@link #getImei} which returns IMEI for GSM or {@link #getMeid} which returns
 * MEID for CDMA.
 * @apiSince 1
 * @deprecatedSince 26
 */

@Deprecated
public java.lang.String getDeviceId() { throw new RuntimeException("Stub!"); }

/**
 * Returns the unique device ID of a subscription, for example, the IMEI for
 * GSM and the MEID for CDMA phones. Return null if device ID is not available.
 *
 * <p>Requires Permission: READ_PRIVILEGED_PHONE_STATE, for the calling app to be the device or
 * profile owner and have the READ_PHONE_STATE permission, or that the calling app has carrier
 * privileges (see {@link #hasCarrierPrivileges}) on any active subscription. The profile owner
 * is an app that owns a managed profile on the device; for more details see <a
 * href="https://developer.android.com/work/managed-profiles">Work profiles</a>. Profile owner
 * access is deprecated and will be removed in a future release.
 *
 * <p>If the calling app does not meet one of these requirements then this method will behave
 * as follows:
 *
 * <ul>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app has the
 *     READ_PHONE_STATE permission then null is returned.</li>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app does not have
 *     the READ_PHONE_STATE permission, or if the calling app is targeting API level 29 or
 *     higher, then a SecurityException is thrown.</li>
 * </ul>
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param slotIndex of which deviceID is returned
 *
 * @deprecated Use {@link #getImei} which returns IMEI for GSM or {@link #getMeid} which returns
 * MEID for CDMA.
 * @apiSince 23
 * @deprecatedSince 26
 */

@Deprecated
public java.lang.String getDeviceId(int slotIndex) { throw new RuntimeException("Stub!"); }

/**
 * Returns the IMEI (International Mobile Equipment Identity). Return null if IMEI is not
 * available.
 *
 * See {@link #getImei(int)} for details on the required permissions and behavior
 * when the caller does not hold sufficient permissions.
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @apiSince 26
 */

public java.lang.String getImei() { throw new RuntimeException("Stub!"); }

/**
 * Returns the IMEI (International Mobile Equipment Identity). Return null if IMEI is not
 * available.
 *
 * <p>This API requires one of the following:
 * <ul>
 *     <li>The caller holds the READ_PRIVILEGED_PHONE_STATE permission.</li>
 *     <li>If the caller is the device or profile owner, the caller holds the
 *     {@link Manifest.permission#READ_PHONE_STATE} permission.</li>
 *     <li>The caller has carrier privileges (see {@link #hasCarrierPrivileges()} on any
 *     active subscription.</li>
 *     <li>The caller is the default SMS app for the device.</li>
 * </ul>
 * <p>The profile owner is an app that owns a managed profile on the device; for more details
 * see <a href="https://developer.android.com/work/managed-profiles">Work profiles</a>.
 * Access by profile owners is deprecated and will be removed in a future release.
 *
 * <p>If the calling app does not meet one of these requirements then this method will behave
 * as follows:
 *
 * <ul>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app has the
 *     READ_PHONE_STATE permission then null is returned.</li>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app does not have
 *     the READ_PHONE_STATE permission, or if the calling app is targeting API level 29 or
 *     higher, then a SecurityException is thrown.</li>
 * </ul>
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param slotIndex of which IMEI is returned
 * @apiSince 26
 */

public java.lang.String getImei(int slotIndex) { throw new RuntimeException("Stub!"); }

/**
 * Returns the Type Allocation Code from the IMEI. Return null if Type Allocation Code is not
 * available.
 * @apiSince 29
 */

@android.annotation.Nullable
public java.lang.String getTypeAllocationCode() { throw new RuntimeException("Stub!"); }

/**
 * Returns the Type Allocation Code from the IMEI. Return null if Type Allocation Code is not
 * available.
 *
 * @param slotIndex of which Type Allocation Code is returned
 * @apiSince 29
 */

@android.annotation.Nullable
public java.lang.String getTypeAllocationCode(int slotIndex) { throw new RuntimeException("Stub!"); }

/**
 * Returns the MEID (Mobile Equipment Identifier). Return null if MEID is not available.
 *
 * <p>Requires Permission: READ_PRIVILEGED_PHONE_STATE, for the calling app to be the device or
 * profile owner and have the READ_PHONE_STATE permission, or that the calling app has carrier
 * privileges (see {@link #hasCarrierPrivileges}) on any active subscription. The profile owner
 * is an app that owns a managed profile on the device; for more details see <a
 * href="https://developer.android.com/work/managed-profiles">Work profiles</a>. Profile owner
 * access is deprecated and will be removed in a future release.
 *
 * <p>If the calling app does not meet one of these requirements then this method will behave
 * as follows:
 *
 * <ul>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app has the
 *     READ_PHONE_STATE permission then null is returned.</li>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app does not have
 *     the READ_PHONE_STATE permission, or if the calling app is targeting API level 29 or
 *     higher, then a SecurityException is thrown.</li>
 * </ul>
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @apiSince 26
 */

public java.lang.String getMeid() { throw new RuntimeException("Stub!"); }

/**
 * Returns the MEID (Mobile Equipment Identifier). Return null if MEID is not available.
 *
 * <p>Requires Permission: READ_PRIVILEGED_PHONE_STATE, for the calling app to be the device or
 * profile owner and have the READ_PHONE_STATE permission, or that the calling app has carrier
 * privileges (see {@link #hasCarrierPrivileges}) on any active subscription. The profile owner
 * is an app that owns a managed profile on the device; for more details see <a
 * href="https://developer.android.com/work/managed-profiles">Work profiles</a>. Profile owner
 * access is deprecated and will be removed in a future release.
 *
 * <p>If the calling app does not meet one of these requirements then this method will behave
 * as follows:
 *
 * <ul>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app has the
 *     READ_PHONE_STATE permission then null is returned.</li>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app does not have
 *     the READ_PHONE_STATE permission, or if the calling app is targeting API level 29 or
 *     higher, then a SecurityException is thrown.</li>
 * </ul>
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param slotIndex of which MEID is returned
 * @apiSince 26
 */

public java.lang.String getMeid(int slotIndex) { throw new RuntimeException("Stub!"); }

/**
 * Returns the Manufacturer Code from the MEID. Return null if Manufacturer Code is not
 * available.
 * @apiSince 29
 */

@android.annotation.Nullable
public java.lang.String getManufacturerCode() { throw new RuntimeException("Stub!"); }

/**
 * Returns the Manufacturer Code from the MEID. Return null if Manufacturer Code is not
 * available.
 *
 * @param slotIndex of which Type Allocation Code is returned
 * @apiSince 29
 */

@android.annotation.Nullable
public java.lang.String getManufacturerCode(int slotIndex) { throw new RuntimeException("Stub!"); }

/**
 * Returns the Network Access Identifier (NAI). Return null if NAI is not available.
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @apiSince 28
 */

public java.lang.String getNai() { throw new RuntimeException("Stub!"); }

/**
 * Returns the current location of the device.
 *<p>
 * If there is only one radio in the device and that radio has an LTE connection,
 * this method will return null. The implementation must not to try add LTE
 * identifiers into the existing cdma/gsm classes.
 *<p>
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_FINE_LOCATION}
 * @return Current location of the device or null if not available.
 *
 * @deprecated use {@link #getAllCellInfo} instead, which returns a superset of this API.
 * @apiSince 1
 * @deprecatedSince 26
 */

@Deprecated
public android.telephony.CellLocation getCellLocation() { throw new RuntimeException("Stub!"); }

/**
 * Returns the current phone type.
 * TODO: This is a last minute change and hence hidden.
 *
 * @see #PHONE_TYPE_NONE
 * @see #PHONE_TYPE_GSM
 * @see #PHONE_TYPE_CDMA
 * @see #PHONE_TYPE_SIP
 *
 * {@hide}
 */

public int getCurrentPhoneType() { throw new RuntimeException("Stub!"); }

/**
 * Returns a constant indicating the device phone type for a subscription.
 *
 * @see #PHONE_TYPE_NONE
 * @see #PHONE_TYPE_GSM
 * @see #PHONE_TYPE_CDMA
 *
 * @param subId for which phone type is returned
 * @hide
 */

public int getCurrentPhoneType(int subId) { throw new RuntimeException("Stub!"); }

/**
 * Returns a constant indicating the device phone type.  This
 * indicates the type of radio used to transmit voice calls.
 *
 * @see #PHONE_TYPE_NONE
 * @see #PHONE_TYPE_GSM
 * @see #PHONE_TYPE_CDMA
 * @see #PHONE_TYPE_SIP
 * @apiSince 1
 */

public int getPhoneType() { throw new RuntimeException("Stub!"); }

/**
 * @return The max value for the timeout passed in {@link #requestNumberVerification}.
 * @hide
 */

public static long getMaxNumberVerificationTimeoutMillis() { throw new RuntimeException("Stub!"); }

/**
 * Returns the alphabetic name of current registered operator.
 * <p>
 * Availability: Only when user is registered to a network. Result may be
 * unreliable on CDMA networks (use {@link #getPhoneType()} to determine if
 * on a CDMA network).
 * @apiSince 1
 */

public java.lang.String getNetworkOperatorName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the numeric name (MCC+MNC) of current registered operator.
 * <p>
 * Availability: Only when user is registered to a network. Result may be
 * unreliable on CDMA networks (use {@link #getPhoneType()} to determine if
 * on a CDMA network).
 * @apiSince 1
 */

public java.lang.String getNetworkOperator() { throw new RuntimeException("Stub!"); }

/**
 * Returns the network specifier of the subscription ID pinned to the TelephonyManager. The
 * network specifier is used by {@link
 * android.net.NetworkRequest.Builder#setNetworkSpecifier(String)} to create a {@link
 * android.net.NetworkRequest} that connects through the subscription.
 *
 * @see android.net.NetworkRequest.Builder#setNetworkSpecifier(String)
 * @see #createForSubscriptionId(int)
 * @see #createForPhoneAccountHandle(PhoneAccountHandle)
 * @apiSince 26
 */

public java.lang.String getNetworkSpecifier() { throw new RuntimeException("Stub!"); }

/**
 * Returns the carrier config of the subscription ID pinned to the TelephonyManager. If an
 * invalid subscription ID is pinned to the TelephonyManager, the returned config will contain
 * default values.
 *
 * <p>This method may take several seconds to complete, so it should only be called from a
 * worker thread.
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @see CarrierConfigManager#getConfigForSubId(int)
 * @see #createForSubscriptionId(int)
 * @see #createForPhoneAccountHandle(PhoneAccountHandle)
 * @apiSince 26
 */

public android.os.PersistableBundle getCarrierConfig() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if the device is considered roaming on the current
 * network, for GSM purposes.
 * <p>
 * Availability: Only when user registered to a network.
 * @apiSince 1
 */

public boolean isNetworkRoaming() { throw new RuntimeException("Stub!"); }

/**
 * Returns the ISO country code equivalent of the MCC (Mobile Country Code) of the current
 * registered operator or the cell nearby, if available.
 * .
 * <p>
 * Note: In multi-sim, this returns a shared emergency network country iso from other
 * subscription if the subscription used to create the TelephonyManager doesn't camp on
 * a network due to some reason (e.g. pin/puk locked), or sim is absent in the corresponding
 * slot.
 * Note: Result may be unreliable on CDMA networks (use {@link #getPhoneType()} to determine
 * if on a CDMA network).
 * @apiSince 1
 */

public java.lang.String getNetworkCountryIso() { throw new RuntimeException("Stub!"); }

/**
 * @return the NETWORK_TYPE_xxxx for current data connection.
 
 * Value is {@link android.telephony.TelephonyManager#NETWORK_TYPE_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IDEN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IWLAN}, android.telephony.TelephonyManager.NETWORK_TYPE_LTE_CA, or {@link android.telephony.TelephonyManager#NETWORK_TYPE_NR}
 * @apiSince 1
 */

public int getNetworkType() { throw new RuntimeException("Stub!"); }

/**
 * Returns a constant indicating the radio technology (network type)
 * currently in use on the device for data transmission.
 *
 * If this object has been created with {@link #createForSubscriptionId}, applies to the given
 * subId. Otherwise, applies to {@link SubscriptionManager#getDefaultDataSubscriptionId()}
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @return the network type
 *
 * Value is {@link android.telephony.TelephonyManager#NETWORK_TYPE_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IDEN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IWLAN}, android.telephony.TelephonyManager.NETWORK_TYPE_LTE_CA, or {@link android.telephony.TelephonyManager#NETWORK_TYPE_NR}
 * @see #NETWORK_TYPE_UNKNOWN
 * @see #NETWORK_TYPE_GPRS
 * @see #NETWORK_TYPE_EDGE
 * @see #NETWORK_TYPE_UMTS
 * @see #NETWORK_TYPE_HSDPA
 * @see #NETWORK_TYPE_HSUPA
 * @see #NETWORK_TYPE_HSPA
 * @see #NETWORK_TYPE_CDMA
 * @see #NETWORK_TYPE_EVDO_0
 * @see #NETWORK_TYPE_EVDO_A
 * @see #NETWORK_TYPE_EVDO_B
 * @see #NETWORK_TYPE_1xRTT
 * @see #NETWORK_TYPE_IDEN
 * @see #NETWORK_TYPE_LTE
 * @see #NETWORK_TYPE_EHRPD
 * @see #NETWORK_TYPE_HSPAP
 * @see #NETWORK_TYPE_NR
 * @apiSince 24
 */

public int getDataNetworkType() { throw new RuntimeException("Stub!"); }

/**
 * Returns the NETWORK_TYPE_xxxx for voice
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 
 * @return Value is {@link android.telephony.TelephonyManager#NETWORK_TYPE_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IDEN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IWLAN}, android.telephony.TelephonyManager.NETWORK_TYPE_LTE_CA, or {@link android.telephony.TelephonyManager#NETWORK_TYPE_NR}
 * @apiSince 24
 */

public int getVoiceNetworkType() { throw new RuntimeException("Stub!"); }

/**
 * @return true if a ICC card is present
 * @apiSince 5
 */

public boolean hasIccCard() { throw new RuntimeException("Stub!"); }

/**
 * Returns a constant indicating the state of the default SIM card.
 *
 * @see #SIM_STATE_UNKNOWN
 * @see #SIM_STATE_ABSENT
 * @see #SIM_STATE_PIN_REQUIRED
 * @see #SIM_STATE_PUK_REQUIRED
 * @see #SIM_STATE_NETWORK_LOCKED
 * @see #SIM_STATE_READY
 * @see #SIM_STATE_NOT_READY
 * @see #SIM_STATE_PERM_DISABLED
 * @see #SIM_STATE_CARD_IO_ERROR
 * @see #SIM_STATE_CARD_RESTRICTED
 * @apiSince 1
 */

public int getSimState() { throw new RuntimeException("Stub!"); }

/**
 * Returns a constant indicating the state of the default SIM card.
 *
 * @see #SIM_STATE_UNKNOWN
 * @see #SIM_STATE_ABSENT
 * @see #SIM_STATE_CARD_IO_ERROR
 * @see #SIM_STATE_CARD_RESTRICTED
 * @see #SIM_STATE_PRESENT
 *
 * @hide
 */

public int getSimCardState() { throw new RuntimeException("Stub!"); }

/**
 * Returns a constant indicating the state of the card applications on the default SIM card.
 *
 * @see #SIM_STATE_UNKNOWN
 * @see #SIM_STATE_PIN_REQUIRED
 * @see #SIM_STATE_PUK_REQUIRED
 * @see #SIM_STATE_NETWORK_LOCKED
 * @see #SIM_STATE_NOT_READY
 * @see #SIM_STATE_PERM_DISABLED
 * @see #SIM_STATE_LOADED
 *
 * @hide
 */

public int getSimApplicationState() { throw new RuntimeException("Stub!"); }

/**
 * Returns a constant indicating the state of the device SIM card in a slot.
 *
 * @param slotIndex
 *
 * @see #SIM_STATE_UNKNOWN
 * @see #SIM_STATE_ABSENT
 * @see #SIM_STATE_PIN_REQUIRED
 * @see #SIM_STATE_PUK_REQUIRED
 * @see #SIM_STATE_NETWORK_LOCKED
 * @see #SIM_STATE_READY
 * @see #SIM_STATE_NOT_READY
 * @see #SIM_STATE_PERM_DISABLED
 * @see #SIM_STATE_CARD_IO_ERROR
 * @see #SIM_STATE_CARD_RESTRICTED
 * @apiSince 26
 */

public int getSimState(int slotIndex) { throw new RuntimeException("Stub!"); }

/**
 * Returns the MCC+MNC (mobile country code + mobile network code) of the
 * provider of the SIM. 5 or 6 decimal digits.
 * <p>
 * Availability: SIM state must be {@link #SIM_STATE_READY}
 *
 * @see #getSimState
 * @apiSince 1
 */

public java.lang.String getSimOperator() { throw new RuntimeException("Stub!"); }

/**
 * Returns the Service Provider Name (SPN).
 * <p>
 * Availability: SIM state must be {@link #SIM_STATE_READY}
 *
 * @see #getSimState
 * @apiSince 1
 */

public java.lang.String getSimOperatorName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the ISO country code equivalent for the SIM provider's country code.
 * @apiSince 1
 */

public java.lang.String getSimCountryIso() { throw new RuntimeException("Stub!"); }

/**
 * Returns the serial number of the SIM, if applicable. Return null if it is
 * unavailable.
 *
 * <p>Requires Permission: READ_PRIVILEGED_PHONE_STATE, for the calling app to be the device or
 * profile owner and have the READ_PHONE_STATE permission, or that the calling app has carrier
 * privileges (see {@link #hasCarrierPrivileges}). The profile owner is an app that owns a
 * managed profile on the device; for more details see <a
 * href="https://developer.android.com/work/managed-profiles">Work profiles</a>. Profile owner
 * access is deprecated and will be removed in a future release.
 *
 * <p>If the calling app does not meet one of these requirements then this method will behave
 * as follows:
 *
 * <ul>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app has the
 *     READ_PHONE_STATE permission then null is returned.</li>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app does not have
 *     the READ_PHONE_STATE permission, or if the calling app is targeting API level 29 or
 *     higher, then a SecurityException is thrown.</li>
 * </ul>
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @apiSince 1
 */

public java.lang.String getSimSerialNumber() { throw new RuntimeException("Stub!"); }

/**
 * Get the card ID of the default eUICC card. If the eUICCs have not yet been loaded, returns
 * {@link #UNINITIALIZED_CARD_ID}. If there is no eUICC or the device does not support card IDs
 * for eUICCs, returns {@link #UNSUPPORTED_CARD_ID}.
 *
 * <p>The card ID is a unique identifier associated with a UICC or eUICC card. Card IDs are
 * unique to a device, and always refer to the same UICC or eUICC card unless the device goes
 * through a factory reset.
 *
 * @return card ID of the default eUICC card, if loaded.
 * @apiSince 29
 */

public int getCardIdForDefaultEuicc() { throw new RuntimeException("Stub!"); }

/**
 * Gets information about currently inserted UICCs and eUICCs.
 * <p>
 * Requires that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 * <p>
 * If the caller has carrier priviliges on any active subscription, then they have permission to
 * get simple information like the card ID ({@link UiccCardInfo#getCardId()}), whether the card
 * is an eUICC ({@link UiccCardInfo#isEuicc()}), and the slot index where the card is inserted
 * ({@link UiccCardInfo#getSlotIndex()}).
 * <p>
 * To get private information such as the EID ({@link UiccCardInfo#getEid()}) or ICCID
 * ({@link UiccCardInfo#getIccId()}), the caller must have carrier priviliges on that specific
 * UICC or eUICC card.
 * <p>
 * See {@link UiccCardInfo} for more details on the kind of information available.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return a list of UiccCardInfo objects, representing information on the currently inserted
 * UICCs and eUICCs. Each UiccCardInfo in the list will have private information filtered out if
 * the caller does not have adequate permissions for that card.
 
 * This value will never be {@code null}.
 * @apiSince 29
 */

@android.annotation.NonNull
public java.util.List<android.telephony.UiccCardInfo> getUiccCardsInfo() { throw new RuntimeException("Stub!"); }

/**
 * Gets all the UICC slots. The objects in the array can be null if the slot info is not
 * available, which is possible between phone process starting and getting slot info from modem.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return UiccSlotInfo array.
 *
 * @hide
 */

public android.telephony.UiccSlotInfo[] getUiccSlotsInfo() { throw new RuntimeException("Stub!"); }

/**
 * Map logicalSlot to physicalSlot, and activate the physicalSlot if it is inactive. For
 * example, passing the physicalSlots array [1, 0] means mapping the first item 1, which is
 * physical slot index 1, to the logical slot 0; and mapping the second item 0, which is
 * physical slot index 0, to the logical slot 1. The index of the array means the index of the
 * logical slots.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param physicalSlots The content of the array represents the physical slot index. The array
 *        size should be same as {@link #getUiccSlotsInfo()}.
 * @return boolean Return true if the switch succeeds, false if the switch fails.
 * @hide
 */

public boolean switchSlots(int[] physicalSlots) { throw new RuntimeException("Stub!"); }

/**
 * Get the mapping from logical slots to physical slots. The key of the map is the logical slot
 * id and the value is the physical slots id mapped to this logical slot id.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return a map indicates the mapping from logical slots to physical slots. The size of the map
 * should be {@link #getPhoneCount()} if success, otherwise return an empty map.
 *
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public java.util.Map<java.lang.Integer,java.lang.Integer> getLogicalToPhysicalSlotMapping() { throw new RuntimeException("Stub!"); }

/**
 * Returns the unique subscriber ID, for example, the IMSI for a GSM phone.
 * Return null if it is unavailable.
 *
 * <p>Requires Permission: READ_PRIVILEGED_PHONE_STATE, for the calling app to be the device or
 * profile owner and have the READ_PHONE_STATE permission, or that the calling app has carrier
 * privileges (see {@link #hasCarrierPrivileges}). The profile owner is an app that owns a
 * managed profile on the device; for more details see <a
 * href="https://developer.android.com/work/managed-profiles">Work profiles</a>. Profile owner
 * access is deprecated and will be removed in a future release.
 *
 * <p>If the calling app does not meet one of these requirements then this method will behave
 * as follows:
 *
 * <ul>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app has the
 *     READ_PHONE_STATE permission then null is returned.</li>
 *     <li>If the calling app's target SDK is API level 28 or lower and the app does not have
 *     the READ_PHONE_STATE permission, or if the calling app is targeting API level 29 or
 *     higher, then a SecurityException is thrown.</li>
 * </ul>
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @apiSince 1
 */

public java.lang.String getSubscriberId() { throw new RuntimeException("Stub!"); }

/**
 * Returns the Group Identifier Level1 for a GSM phone.
 * Return null if it is unavailable.
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @apiSince 18
 */

public java.lang.String getGroupIdLevel1() { throw new RuntimeException("Stub!"); }

/**
 * Returns the phone number string for line 1, for example, the MSISDN
 * for a GSM phone. Return null if it is unavailable.
 *
 * <p>Requires Permission:
 *     {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE},
 *     {@link android.Manifest.permission#READ_SMS READ_SMS},
 *     {@link android.Manifest.permission#READ_PHONE_NUMBERS READ_PHONE_NUMBERS},
 *     that the caller is the default SMS app,
 *     or that the caller has carrier privileges (see {@link #hasCarrierPrivileges}).
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE} or {@link android.Manifest.permission#READ_SMS} or {@link android.Manifest.permission#READ_PHONE_NUMBERS}
 * @apiSince 1
 */

public java.lang.String getLine1Number() { throw new RuntimeException("Stub!"); }

/**
 * Set the line 1 phone number string and its alphatag for the current ICCID
 * for display purpose only, for example, displayed in Phone Status. It won't
 * change the actual MSISDN/MDN. To unset alphatag or number, pass in a null
 * value.
 *
 * <p>Requires that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param alphaTag alpha-tagging of the dailing nubmer
 * @param number The dialing number
 * @return true if the operation was executed correctly.
 * @apiSince 22
 */

public boolean setLine1NumberForDisplay(java.lang.String alphaTag, java.lang.String number) { throw new RuntimeException("Stub!"); }

/**
 * Returns the voice mail number. Return null if it is unavailable.
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @apiSince 1
 */

public java.lang.String getVoiceMailNumber() { throw new RuntimeException("Stub!"); }

/**
 * Sets the voice mail number.
 *
 * <p>Requires that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param alphaTag The alpha tag to display.
 * @param number The voicemail number.
 * @apiSince 22
 */

public boolean setVoiceMailNumber(java.lang.String alphaTag, java.lang.String number) { throw new RuntimeException("Stub!"); }

/**
 * Enables or disables the visual voicemail client for a phone account.
 *
 * <p>Requires that the calling app is the default dialer, or has carrier privileges (see
 * {@link #hasCarrierPrivileges}), or has permission
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 *
 * @param phoneAccountHandle the phone account to change the client state
 * @param enabled the new state of the client
 * @hide
 * @deprecated Visual voicemail no longer in telephony. {@link VisualVoicemailService} should
 * be implemented instead.
 */

@Deprecated
public void setVisualVoicemailEnabled(android.telecom.PhoneAccountHandle phoneAccountHandle, boolean enabled) { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the visual voicemail client is enabled.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @param phoneAccountHandle the phone account to check for.
 * @return {@code true} when the visual voicemail client is enabled for this client
 * @hide
 * @deprecated Visual voicemail no longer in telephony. {@link VisualVoicemailService} should
 * be implemented instead.
 */

@Deprecated
public boolean isVisualVoicemailEnabled(android.telecom.PhoneAccountHandle phoneAccountHandle) { throw new RuntimeException("Stub!"); }

/**
 * Returns an opaque bundle of settings formerly used by the visual voicemail client for the
 * subscription ID pinned to the TelephonyManager, or {@code null} if the subscription ID is
 * invalid. This method allows the system dialer to migrate settings out of the pre-O visual
 * voicemail client in telephony.
 *
 * <p>Requires the caller to be the system dialer.
 *
 * @see #KEY_VISUAL_VOICEMAIL_ENABLED_BY_USER_BOOL
 * @see #KEY_VOICEMAIL_SCRAMBLED_PIN_STRING
 *
 * @hide
 */

@android.annotation.Nullable
public android.os.Bundle getVisualVoicemailSettings() { throw new RuntimeException("Stub!"); }

/**
 * Returns the package responsible of processing visual voicemail for the subscription ID pinned
 * to the TelephonyManager. Returns {@code null} when there is no package responsible for
 * processing visual voicemail for the subscription.
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @see #createForSubscriptionId(int)
 * @see #createForPhoneAccountHandle(PhoneAccountHandle)
 * @see VisualVoicemailService
 * @apiSince 26
 */

@android.annotation.Nullable
public java.lang.String getVisualVoicemailPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Set the visual voicemail SMS filter settings for the subscription ID pinned
 * to the TelephonyManager.
 * When the filter is enabled, {@link
 * VisualVoicemailService#onSmsReceived(VisualVoicemailTask, VisualVoicemailSms)} will be
 * called when a SMS matching the settings is received. Caller must be the default dialer,
 * system dialer, or carrier visual voicemail app.
 *
 * @param settings The settings for the filter, or {@code null} to disable the filter.
 *
 * @see TelecomManager#getDefaultDialerPackage()
 * @see CarrierConfigManager#KEY_CARRIER_VVM_PACKAGE_NAME_STRING_ARRAY
 * @apiSince 26
 */

public void setVisualVoicemailSmsFilterSettings(android.telephony.VisualVoicemailSmsFilterSettings settings) { throw new RuntimeException("Stub!"); }

/**
 * Send a visual voicemail SMS. The caller must be the current default dialer.
 * A {@link VisualVoicemailService} uses this method to send a command via SMS to the carrier's
 * visual voicemail server.  Some examples for carriers using the OMTP standard include
 * activating and deactivating visual voicemail, or requesting the current visual voicemail
 * provisioning status.  See the OMTP Visual Voicemail specification for more information on the
 * format of these SMS messages.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#SEND_SMS SEND_SMS}
 *
 * @param number The destination number.
 * @param port The destination port for data SMS, or 0 for text SMS.
 * @param text The message content. For data sms, it will be encoded as a UTF-8 byte stream.
 * @param sentIntent The sent intent passed to the {@link SmsManager}
 *
 * @throws SecurityException if the caller is not the current default dialer
 *
 * @see SmsManager#sendDataMessage(String, String, short, byte[], PendingIntent, PendingIntent)
 * @see SmsManager#sendTextMessage(String, String, String, PendingIntent, PendingIntent)
 * @apiSince 26
 */

public void sendVisualVoicemailSms(java.lang.String number, int port, java.lang.String text, android.app.PendingIntent sentIntent) { throw new RuntimeException("Stub!"); }

/**
 * Sets the voice activation state
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the
 * calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param activationState The voice activation state
 * Value is {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_UNKNOWN}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_ACTIVATING}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_ACTIVATED}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_DEACTIVATED}, or {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_RESTRICTED}
 * @see #SIM_ACTIVATION_STATE_UNKNOWN
 * @see #SIM_ACTIVATION_STATE_ACTIVATING
 * @see #SIM_ACTIVATION_STATE_ACTIVATED
 * @see #SIM_ACTIVATION_STATE_DEACTIVATED
 * @hide
 */

public void setVoiceActivationState(int activationState) { throw new RuntimeException("Stub!"); }

/**
 * Sets the data activation state
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the
 * calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param activationState The data activation state
 * Value is {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_UNKNOWN}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_ACTIVATING}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_ACTIVATED}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_DEACTIVATED}, or {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_RESTRICTED}
 * @see #SIM_ACTIVATION_STATE_UNKNOWN
 * @see #SIM_ACTIVATION_STATE_ACTIVATING
 * @see #SIM_ACTIVATION_STATE_ACTIVATED
 * @see #SIM_ACTIVATION_STATE_DEACTIVATED
 * @see #SIM_ACTIVATION_STATE_RESTRICTED
 * @hide
 */

public void setDataActivationState(int activationState) { throw new RuntimeException("Stub!"); }

/**
 * Returns the voice activation state
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE READ_PRIVILEGED_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return voiceActivationState
 * Value is {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_UNKNOWN}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_ACTIVATING}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_ACTIVATED}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_DEACTIVATED}, or {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_RESTRICTED}
 * @see #SIM_ACTIVATION_STATE_UNKNOWN
 * @see #SIM_ACTIVATION_STATE_ACTIVATING
 * @see #SIM_ACTIVATION_STATE_ACTIVATED
 * @see #SIM_ACTIVATION_STATE_DEACTIVATED
 * @hide
 */

public int getVoiceActivationState() { throw new RuntimeException("Stub!"); }

/**
 * Returns the data activation state
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE READ_PRIVILEGED_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return dataActivationState for the given subscriber
 * Value is {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_UNKNOWN}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_ACTIVATING}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_ACTIVATED}, {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_DEACTIVATED}, or {@link android.telephony.TelephonyManager#SIM_ACTIVATION_STATE_RESTRICTED}
 * @see #SIM_ACTIVATION_STATE_UNKNOWN
 * @see #SIM_ACTIVATION_STATE_ACTIVATING
 * @see #SIM_ACTIVATION_STATE_ACTIVATED
 * @see #SIM_ACTIVATION_STATE_DEACTIVATED
 * @see #SIM_ACTIVATION_STATE_RESTRICTED
 * @hide
 */

public int getDataActivationState() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the alphabetic identifier associated with the voice
 * mail number.
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @apiSince 1
 */

public java.lang.String getVoiceMailAlphaTag() { throw new RuntimeException("Stub!"); }

/**
 * Send the special dialer code. The IPC caller must be the current default dialer or have
 * carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param inputCode The special dialer code to send
 *
 * @throws SecurityException if the caller does not have carrier privileges or is not the
 *         current default dialer
 * @apiSince 26
 */

public void sendDialerSpecialCode(java.lang.String inputCode) { throw new RuntimeException("Stub!"); }

/**
 * Returns the IMS home network domain name that was loaded from the ISIM {@see #APPTYPE_ISIM}.
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return the IMS domain name. Returns {@code null} if ISIM hasn't been loaded or IMS domain
 * hasn't been loaded or isn't present on the ISIM.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE READ_PRIVILEGED_PHONE_STATE}
 * @hide
 */

@android.annotation.Nullable
public java.lang.String getIsimDomain() { throw new RuntimeException("Stub!"); }

/**
 * Returns the state of all calls on the device.
 * <p>
 * This method considers not only calls in the Telephony stack, but also calls via other
 * {@link android.telecom.ConnectionService} implementations.
 * <p>
 * Note: The call state returned via this method may differ from what is reported by
 * {@link PhoneStateListener#onCallStateChanged(int, String)}, as that callback only considers
 * Telephony (mobile) calls.
 *
 * @return the current call state.
 
 * Value is {@link android.telephony.TelephonyManager#CALL_STATE_IDLE}, {@link android.telephony.TelephonyManager#CALL_STATE_RINGING}, or {@link android.telephony.TelephonyManager#CALL_STATE_OFFHOOK}
 * @apiSince 1
 */

public int getCallState() { throw new RuntimeException("Stub!"); }

/**
 * Returns a constant indicating the type of activity on a data connection
 * (cellular).
 *
 * @see #DATA_ACTIVITY_NONE
 * @see #DATA_ACTIVITY_IN
 * @see #DATA_ACTIVITY_OUT
 * @see #DATA_ACTIVITY_INOUT
 * @see #DATA_ACTIVITY_DORMANT
 * @apiSince 1
 */

public int getDataActivity() { throw new RuntimeException("Stub!"); }

/**
 * Returns a constant indicating the current data connection state
 * (cellular).
 *
 * @see #DATA_DISCONNECTED
 * @see #DATA_CONNECTING
 * @see #DATA_CONNECTED
 * @see #DATA_SUSPENDED
 * @apiSince 1
 */

public int getDataState() { throw new RuntimeException("Stub!"); }

/**
 * Registers a listener object to receive notification of changes
 * in specified telephony states.
 * <p>
 * To register a listener, pass a {@link PhoneStateListener} and specify at least one telephony
 * state of interest in the events argument.
 *
 * At registration, and when a specified telephony state changes, the telephony manager invokes
 * the appropriate callback method on the listener object and passes the current (updated)
 * values.
 * <p>
 * To un-register a listener, pass the listener object and set the events argument to
 * {@link PhoneStateListener#LISTEN_NONE LISTEN_NONE} (0).
 *
 * If this TelephonyManager object has been created with {@link #createForSubscriptionId},
 * applies to the given subId. Otherwise, applies to
 * {@link SubscriptionManager#getDefaultSubscriptionId()}. To listen events for multiple subIds,
 * pass a separate listener object to each TelephonyManager object created with
 * {@link #createForSubscriptionId}.
 *
 * Note: if you call this method while in the middle of a binder transaction, you <b>must</b>
 * call {@link android.os.Binder#clearCallingIdentity()} before calling this method. A
 * {@link SecurityException} will be thrown otherwise.
 *
 * @param listener The {@link PhoneStateListener} object to register
 *                 (or unregister)
 * @param events The telephony state(s) of interest to the listener,
 *               as a bitwise-OR combination of {@link PhoneStateListener}
 *               LISTEN_ flags.
 * @apiSince 1
 */

public void listen(android.telephony.PhoneStateListener listener, int events) { throw new RuntimeException("Stub!"); }

/**
 * @return true if the current device is "voice capable".
 * <p>
 * "Voice capable" means that this device supports circuit-switched
 * (i.e. voice) phone calls over the telephony network, and is allowed
 * to display the in-call UI while a cellular voice call is active.
 * This will be false on "data only" devices which can't make voice
 * calls and don't support any in-call UI.
 * <p>
 * Note: the meaning of this flag is subtly different from the
 * PackageManager.FEATURE_TELEPHONY system feature, which is available
 * on any device with a telephony radio, even if the device is
 * data-only.
 * @apiSince 22
 */

public boolean isVoiceCapable() { throw new RuntimeException("Stub!"); }

/**
 * @return true if the current device supports sms service.
 * <p>
 * If true, this means that the device supports both sending and
 * receiving sms via the telephony network.
 * <p>
 * Note: Voicemail waiting sms, cell broadcasting sms, and MMS are
 *       disabled when device doesn't support sms.
 * @apiSince 21
 */

public boolean isSmsCapable() { throw new RuntimeException("Stub!"); }

/**
 * Requests all available cell information from all radios on the device including the
 * camped/registered, serving, and neighboring cells.
 *
 * <p>The response can include one or more {@link android.telephony.CellInfoGsm CellInfoGsm},
 * {@link android.telephony.CellInfoCdma CellInfoCdma},
 * {@link android.telephony.CellInfoTdscdma CellInfoTdscdma},
 * {@link android.telephony.CellInfoLte CellInfoLte}, and
 * {@link android.telephony.CellInfoWcdma CellInfoWcdma} objects, in any combination.
 * It is typical to see instances of one or more of any these in the list. In addition, zero
 * or more of the returned objects may be considered registered; that is, their
 * {@link android.telephony.CellInfo#isRegistered CellInfo.isRegistered()}
 * methods may return true, indicating that the cell is being used or would be used for
 * signaling communication if necessary.
 *
 * <p>Beginning with {@link android.os.Build.VERSION_CODES#Q Android Q},
 * if this API results in a change of the cached CellInfo, that change will be reported via
 * {@link android.telephony.PhoneStateListener#onCellInfoChanged onCellInfoChanged()}.
 *
 * <p>Apps targeting {@link android.os.Build.VERSION_CODES#Q Android Q} or higher will no
 * longer trigger a refresh of the cached CellInfo by invoking this API. Instead, those apps
 * will receive the latest cached results, which may not be current. Apps targeting
 * {@link android.os.Build.VERSION_CODES#Q Android Q} or higher that wish to request updated
 * CellInfo should call
 * {@link android.telephony.TelephonyManager#requestCellInfoUpdate requestCellInfoUpdate()};
 * however, in all cases, updates will be rate-limited and are not guaranteed. To determine the
 * recency of CellInfo data, callers should check
 * {@link android.telephony.CellInfo#getTimeStamp CellInfo#getTimeStamp()}.
 *
 * <p>This method returns valid data for devices with
 * {@link android.content.pm.PackageManager#FEATURE_TELEPHONY FEATURE_TELEPHONY}. In cases
 * where only partial information is available for a particular CellInfo entry, unavailable
 * fields will be reported as {@link android.telephony.CellInfo#UNAVAILABLE}. All reported
 * cells will include at least a valid set of technology-specific identification info and a
 * power level measurement.
 *
 * <p>This method is preferred over using {@link
 * android.telephony.TelephonyManager#getCellLocation getCellLocation()}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_FINE_LOCATION}
 * @return List of {@link android.telephony.CellInfo}; null if cell
 * information is unavailable.
 * @apiSince 17
 */

public java.util.List<android.telephony.CellInfo> getAllCellInfo() { throw new RuntimeException("Stub!"); }

/**
 * Requests all available cell information from the current subscription for observed
 * camped/registered, serving, and neighboring cells.
 *
 * <p>Any available results from this request will be provided by calls to
 * {@link android.telephony.PhoneStateListener#onCellInfoChanged onCellInfoChanged()}
 * for each active subscription.
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_FINE_LOCATION}
 * @param executor the executor on which callback will be invoked.
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback a callback to receive CellInfo.
 
 * This value must never be {@code null}.
 * @apiSince 29
 */

public void requestCellInfoUpdate(@android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.telephony.TelephonyManager.CellInfoCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Requests all available cell information from the current subscription for observed
 * camped/registered, serving, and neighboring cells.
 *
 * <p>Any available results from this request will be provided by calls to
 * {@link android.telephony.PhoneStateListener#onCellInfoChanged onCellInfoChanged()}
 * for each active subscription.
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_FINE_LOCATION} and {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param workSource the requestor to whom the power consumption for this should be attributed.
 * This value must never be {@code null}.
 * @param executor the executor on which callback will be invoked.
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback a callback to receive CellInfo.
 * This value must never be {@code null}.
 * @hide
 */

public void requestCellInfoUpdate(@android.annotation.NonNull android.os.WorkSource workSource, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.telephony.TelephonyManager.CellInfoCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Returns the MMS user agent.
 * @apiSince 19
 */

public java.lang.String getMmsUserAgent() { throw new RuntimeException("Stub!"); }

/**
 * Returns the MMS user agent profile URL.
 * @apiSince 19
 */

public java.lang.String getMmsUAProfUrl() { throw new RuntimeException("Stub!"); }

/**
 * Opens a logical channel to the ICC card.
 *
 * Input parameters equivalent to TS 27.007 AT+CCHO command.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param AID Application id. See ETSI 102.221 and 101.220.
 * @return an IccOpenLogicalChannelResponse object.
 * @deprecated Replaced by {@link #iccOpenLogicalChannel(String, int)}
 * @apiSince 21
 * @deprecatedSince 26
 */

@Deprecated
public android.telephony.IccOpenLogicalChannelResponse iccOpenLogicalChannel(java.lang.String AID) { throw new RuntimeException("Stub!"); }

/**
 * Opens a logical channel to the ICC card using the physical slot index.
 *
 * Use this method when no subscriptions are available on the SIM and the operation must be
 * performed using the physical slot index.
 *
 * Input parameters equivalent to TS 27.007 AT+CCHO command.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param slotIndex the physical slot index of the ICC card
 * @param aid Application id. See ETSI 102.221 and 101.220.
 * This value may be {@code null}.
 * @param p2 P2 parameter (described in ISO 7816-4).
 * @return an IccOpenLogicalChannelResponse object.
 * This value may be {@code null}.
 * @hide
 */

@android.annotation.Nullable
public android.telephony.IccOpenLogicalChannelResponse iccOpenLogicalChannelBySlot(int slotIndex, @android.annotation.Nullable java.lang.String aid, int p2) { throw new RuntimeException("Stub!"); }

/**
 * Opens a logical channel to the ICC card.
 *
 * Input parameters equivalent to TS 27.007 AT+CCHO command.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param AID Application id. See ETSI 102.221 and 101.220.
 * @param p2 P2 parameter (described in ISO 7816-4).
 * @return an IccOpenLogicalChannelResponse object.
 * @apiSince 26
 */

public android.telephony.IccOpenLogicalChannelResponse iccOpenLogicalChannel(java.lang.String AID, int p2) { throw new RuntimeException("Stub!"); }

/**
 * Closes a previously opened logical channel to the ICC card using the physical slot index.
 *
 * Use this method when no subscriptions are available on the SIM and the operation must be
 * performed using the physical slot index.
 *
 * Input parameters equivalent to TS 27.007 AT+CCHC command.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param slotIndex the physical slot index of the ICC card
 * @param channel is the channel id to be closed as returned by a successful
 *            iccOpenLogicalChannel.
 * @return true if the channel was closed successfully.
 * @hide
 */

public boolean iccCloseLogicalChannelBySlot(int slotIndex, int channel) { throw new RuntimeException("Stub!"); }

/**
 * Closes a previously opened logical channel to the ICC card.
 *
 * Input parameters equivalent to TS 27.007 AT+CCHC command.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param channel is the channel id to be closed as returned by a successful
 *            iccOpenLogicalChannel.
 * @return true if the channel was closed successfully.
 * @apiSince 21
 */

public boolean iccCloseLogicalChannel(int channel) { throw new RuntimeException("Stub!"); }

/**
 * Transmit an APDU to the ICC card over a logical channel using the physical slot index.
 *
 * Use this method when no subscriptions are available on the SIM and the operation must be
 * performed using the physical slot index.
 *
 * Input parameters equivalent to TS 27.007 AT+CGLA command.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param slotIndex the physical slot index of the ICC card
 * @param channel is the channel id to be closed as returned by a successful
 *            iccOpenLogicalChannel.
 * @param cla Class of the APDU command.
 * @param instruction Instruction of the APDU command.
 * @param p1 P1 value of the APDU command.
 * @param p2 P2 value of the APDU command.
 * @param p3 P3 value of the APDU command. If p3 is negative a 4 byte APDU
 *            is sent to the SIM.
 * @param data Data to be sent with the APDU.
 * This value may be {@code null}.
 * @return The APDU response from the ICC card with the status appended at the end, or null if
 * there is an issue connecting to the Telephony service.
 * @hide
 */

@android.annotation.Nullable
public java.lang.String iccTransmitApduLogicalChannelBySlot(int slotIndex, int channel, int cla, int instruction, int p1, int p2, int p3, @android.annotation.Nullable java.lang.String data) { throw new RuntimeException("Stub!"); }

/**
 * Transmit an APDU to the ICC card over a logical channel.
 *
 * Input parameters equivalent to TS 27.007 AT+CGLA command.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param channel is the channel id to be closed as returned by a successful
 *            iccOpenLogicalChannel.
 * @param cla Class of the APDU command.
 * @param instruction Instruction of the APDU command.
 * @param p1 P1 value of the APDU command.
 * @param p2 P2 value of the APDU command.
 * @param p3 P3 value of the APDU command. If p3 is negative a 4 byte APDU
 *            is sent to the SIM.
 * @param data Data to be sent with the APDU.
 * @return The APDU response from the ICC card with the status appended at
 *            the end.
 * @apiSince 21
 */

public java.lang.String iccTransmitApduLogicalChannel(int channel, int cla, int instruction, int p1, int p2, int p3, java.lang.String data) { throw new RuntimeException("Stub!"); }

/**
 * Transmit an APDU to the ICC card over the basic channel using the physical slot index.
 *
 * Use this method when no subscriptions are available on the SIM and the operation must be
 * performed using the physical slot index.
 *
 * Input parameters equivalent to TS 27.007 AT+CSIM command.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param slotIndex the physical slot index of the ICC card to target
 * @param cla Class of the APDU command.
 * @param instruction Instruction of the APDU command.
 * @param p1 P1 value of the APDU command.
 * @param p2 P2 value of the APDU command.
 * @param p3 P3 value of the APDU command. If p3 is negative a 4 byte APDU
 *            is sent to the SIM.
 * @param data Data to be sent with the APDU.
 * This value may be {@code null}.
 * @return The APDU response from the ICC card with the status appended at
 *            the end.
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public java.lang.String iccTransmitApduBasicChannelBySlot(int slotIndex, int cla, int instruction, int p1, int p2, int p3, @android.annotation.Nullable java.lang.String data) { throw new RuntimeException("Stub!"); }

/**
 * Transmit an APDU to the ICC card over the basic channel.
 *
 * Input parameters equivalent to TS 27.007 AT+CSIM command.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param cla Class of the APDU command.
 * @param instruction Instruction of the APDU command.
 * @param p1 P1 value of the APDU command.
 * @param p2 P2 value of the APDU command.
 * @param p3 P3 value of the APDU command. If p3 is negative a 4 byte APDU
 *            is sent to the SIM.
 * @param data Data to be sent with the APDU.
 * @return The APDU response from the ICC card with the status appended at
 *            the end.
 * @apiSince 21
 */

public java.lang.String iccTransmitApduBasicChannel(int cla, int instruction, int p1, int p2, int p3, java.lang.String data) { throw new RuntimeException("Stub!"); }

/**
 * Returns the response APDU for a command APDU sent through SIM_IO.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param fileID
 * @param command
 * @param p1 P1 value of the APDU command.
 * @param p2 P2 value of the APDU command.
 * @param p3 P3 value of the APDU command.
 * @param filePath
 * @return The APDU response.
 * @apiSince 21
 */

public byte[] iccExchangeSimIO(int fileID, int command, int p1, int p2, int p3, java.lang.String filePath) { throw new RuntimeException("Stub!"); }

/**
 * Send ENVELOPE to the SIM and return the response.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param content String containing SAT/USAT response in hexadecimal
 *                format starting with command tag. See TS 102 223 for
 *                details.
 * @return The APDU response from the ICC card in hexadecimal format
 *         with the last 4 bytes being the status word. If the command fails,
 *         returns an empty string.
 * @apiSince 21
 */

public java.lang.String sendEnvelopeWithStatus(java.lang.String content) { throw new RuntimeException("Stub!"); }

/**
 * Rollback modem configurations to factory default except some config which are in whitelist.
 * Used for device configuration by some carriers.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @return {@code true} on success; {@code false} on any failure.
 *
 * @hide
 */

public boolean resetRadioConfig() { throw new RuntimeException("Stub!"); }

/**
 * Generate a radio modem reset. Used for device configuration by some carriers.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @return {@code true} on success; {@code false} on any failure.
 *
 * @hide
 */

public boolean rebootRadio() { throw new RuntimeException("Stub!"); }

/**
 * Request that the next incoming call from a number matching {@code range} be intercepted.
 *
 * This API is intended for OEMs to provide a service for apps to verify the device's phone
 * number. When called, the Telephony stack will store the provided {@link PhoneNumberRange} and
 * intercept the next incoming call from a number that lies within the range, within a timeout
 * specified by {@code timeoutMillis}.
 *
 * If such a phone call is received, the caller will be notified via
 * {@link NumberVerificationCallback#onCallReceived(String)} on the provided {@link Executor}.
 * If verification fails for any reason, the caller will be notified via
 * {@link NumberVerificationCallback#onVerificationFailed(int)}
 * on the provided {@link Executor}.
 *
 * In addition to the {@link Manifest.permission#MODIFY_PHONE_STATE} permission, callers of this
 * API must also be listed in the device configuration as an authorized app in
 * {@code packages/services/Telephony/res/values/config.xml} under the
 * {@code config_number_verification_package_name} key.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide
 * @param range The range of phone numbers the caller expects a phone call from.
 * This value must never be {@code null}.
 * @param timeoutMillis The amount of time to wait for such a call, or the value of
 *                      {@link #getMaxNumberVerificationTimeoutMillis()}, whichever is lesser.
 * @param executor The {@link Executor} that callbacks should be executed on.
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to use for delivering results.

 * This value must never be {@code null}.
 */

public void requestNumberVerification(@android.annotation.NonNull android.telephony.PhoneNumberRange range, long timeoutMillis, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.telephony.NumberVerificationCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Returns the IMS Service Table (IST) that was loaded from the ISIM.
 *
 * See 3GPP TS 31.103 (Section 4.2.7) for the definition and more information on this table.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return IMS Service Table or null if not present or not loaded
 * @hide
 */

@android.annotation.Nullable
public java.lang.String getIsimIst() { throw new RuntimeException("Stub!"); }

/**
 * Returns the response of authentication for the default subscription.
 * Returns null if the authentication hasn't been successful
 *
 * <p>Requires Permission: READ_PRIVILEGED_PHONE_STATE or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param appType the icc application type, like {@link #APPTYPE_USIM}
 * @param authType the authentication type, {@link #AUTHTYPE_EAP_AKA} or
 * {@link #AUTHTYPE_EAP_SIM}
 * @param data authentication challenge data, base64 encoded.
 * See 3GPP TS 31.102 7.1.2 for more details.
 * @return the response of authentication. This value will be null in the following cases:
 *   Authentication error, incorrect MAC
 *   Authentication error, security context not supported
 *   Key freshness failure
 *   Authentication error, no memory space available
 *   Authentication error, no memory space available in EFMUK
 * @apiSince 24
 */

public java.lang.String getIccAuthentication(int appType, int authType, java.lang.String data) { throw new RuntimeException("Stub!"); }

/**
 * Returns an array of Forbidden PLMNs from the USIM App
 * Returns null if the query fails.
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @return an array of forbidden PLMNs or null if not available
 * @apiSince 26
 */

public java.lang.String[] getForbiddenPlmns() { throw new RuntimeException("Stub!"); }

/**
 * Get the preferred network type bitmask.
 *
 * <p>If this object has been created with {@link #createForSubscriptionId}, applies to the
 * given subId. Otherwise, applies to {@link SubscriptionManager#getDefaultSubscriptionId()}
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE READ_PRIVILEGED_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return The bitmask of preferred network types.
 *
 * Value is either <code>0</code> or a combination of {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_LTE_CA}, and {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_NR}
 * @hide
 */

public long getPreferredNetworkTypeBitmask() { throw new RuntimeException("Stub!"); }

/**
 * Sets the network selection mode to automatic.
 *
 * <p>If this object has been created with {@link #createForSubscriptionId}, applies to the
 * given subId. Otherwise, applies to {@link SubscriptionManager#getDefaultSubscriptionId()}
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @apiSince 28
 */

public void setNetworkSelectionModeAutomatic() { throw new RuntimeException("Stub!"); }

/**
 * Request a network scan.
 *
 * This method is asynchronous, so the network scan results will be returned by callback.
 * The returned NetworkScan will contain a callback method which can be used to stop the scan.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges})
 * and {@link android.Manifest.permission#ACCESS_FINE_LOCATION}.
 *
 * If the system-wide location switch is off, apps may still call this API, with the
 * following constraints:
 * <ol>
 *     <li>The app must hold the {@code android.permission.NETWORK_SCAN} permission.</li>
 *     <li>The app must not supply any specific bands or channels to scan.</li>
 *     <li>The app must only specify MCC/MNC pairs that are
 *     associated to a SIM in the device.</li>
 *     <li>Returned results will have no meaningful info other than signal strength
 *     and MCC/MNC info.</li>
 * </ol>
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE} and {@link android.Manifest.permission#ACCESS_FINE_LOCATION}
 * @param request Contains all the RAT with bands/channels that need to be scanned.
 * @param executor The executor through which the callback should be invoked. Since the scan
 *        request may trigger multiple callbacks and they must be invoked in the same order as
 *        they are received by the platform, the user should provide an executor which executes
 *        tasks one at a time in serial order. For example AsyncTask.SERIAL_EXECUTOR.
 * @param callback Returns network scan results or errors.
 * @return A NetworkScan obj which contains a callback which can be used to stop the scan.
 * @apiSince 28
 */

public android.telephony.NetworkScan requestNetworkScan(android.telephony.NetworkScanRequest request, java.util.concurrent.Executor executor, android.telephony.TelephonyScanManager.NetworkScanCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Ask the radio to connect to the input network and change selection mode to manual.
 *
 * <p>If this object has been created with {@link #createForSubscriptionId}, applies to the
 * given subId. Otherwise, applies to {@link SubscriptionManager#getDefaultSubscriptionId()}
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param operatorNumeric the PLMN ID of the network to select.
 * @param persistSelection whether the selection will persist until reboot. If true, only allows
 * attaching to the selected PLMN until reboot; otherwise, attach to the chosen PLMN and resume
 * normal network selection next time.
 * @return {@code true} on success; {@code false} on any failure.
 * @apiSince 28
 */

public boolean setNetworkSelectionModeManual(java.lang.String operatorNumeric, boolean persistSelection) { throw new RuntimeException("Stub!"); }

/**
 * Set the preferred network type bitmask.
 *
 * <p>If this object has been created with {@link #createForSubscriptionId}, applies to the
 * given subId. Otherwise, applies to {@link SubscriptionManager#getDefaultSubscriptionId()}
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param networkTypeBitmask The bitmask of preferred network types.
 * Value is either <code>0</code> or a combination of {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_LTE_CA}, and {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_NR}
 * @return true on success; false on any failure.
 * @hide
 */

public boolean setPreferredNetworkTypeBitmask(long networkTypeBitmask) { throw new RuntimeException("Stub!"); }

/**
 * Set the preferred network type to global mode which includes LTE, CDMA, EvDo and GSM/WCDMA.
 *
 * <p>Requires that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @return true on success; false on any failure.
 * @apiSince 22
 */

public boolean setPreferredNetworkTypeToGlobal() { throw new RuntimeException("Stub!"); }

/**
 * Has the calling application been granted carrier privileges by the carrier.
 *
 * If any of the packages in the calling UID has carrier privileges, the
 * call will return true. This access is granted by the owner of the UICC
 * card and does not depend on the registered carrier.
 *
 * @return true if the app has carrier privileges.
 * @apiSince 22
 */

public boolean hasCarrierPrivileges() { throw new RuntimeException("Stub!"); }

/**
 * Override the branding for the current ICCID.
 *
 * Once set, whenever the SIM is present in the device, the service
 * provider name (SPN) and the operator name will both be replaced by the
 * brand value input. To unset the value, the same function should be
 * called with a null brand value.
 *
 * <p>Requires that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * @param brand The brand name to display/set.
 * @return true if the operation was executed correctly.
 * @apiSince 22
 */

public boolean setOperatorBrandOverride(java.lang.String brand) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public java.lang.String getCdmaMdn() { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public java.lang.String getCdmaMdn(int subId) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public java.lang.String getCdmaMin() { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public java.lang.String getCdmaMin(int subId) { throw new RuntimeException("Stub!"); }

/** @hide */

public int checkCarrierPrivilegesForPackage(java.lang.String pkgName) { throw new RuntimeException("Stub!"); }

/** @hide */

public int checkCarrierPrivilegesForPackageAnyPhone(java.lang.String pkgName) { throw new RuntimeException("Stub!"); }

/** @hide */

public java.util.List<java.lang.String> getCarrierPackageNamesForIntent(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/** @hide */

public java.util.List<java.lang.String> getCarrierPackageNamesForIntentAndPhone(android.content.Intent intent, int phoneId) { throw new RuntimeException("Stub!"); }

/** @hide */

public void dial(java.lang.String number) { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#CALL_PHONE}
 * @deprecated Use  {@link android.telecom.TelecomManager#placeCall(Uri address,
 * Bundle extras)} instead.
 * @hide
 */

@Deprecated
public void call(java.lang.String callingPackage, java.lang.String number) { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE} or {@link android.Manifest.permission#READ_PHONE_STATE}
 * @deprecated Use {@link android.telecom.TelecomManager#isInCall} instead
 * @hide
 */

@Deprecated
public boolean isOffhook() { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE} or {@link android.Manifest.permission#READ_PHONE_STATE}
 * @deprecated Use {@link android.telecom.TelecomManager#isRinging} instead
 * @hide
 */

@Deprecated
public boolean isRinging() { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE} or {@link android.Manifest.permission#READ_PHONE_STATE}
 * @deprecated Use {@link android.telecom.TelecomManager#isInCall} instead
 * @hide
 */

@Deprecated
public boolean isIdle() { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE} or {@link android.Manifest.permission#READ_PHONE_STATE}
 * @deprecated Use {@link android.telephony.TelephonyManager#getServiceState} instead
 * @hide
 */

@Deprecated
public boolean isRadioOn() { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public boolean supplyPin(java.lang.String pin) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public boolean supplyPuk(java.lang.String puk, java.lang.String pin) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public int[] supplyPinReportResult(java.lang.String pin) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public int[] supplyPukReportResult(java.lang.String puk, java.lang.String pin) { throw new RuntimeException("Stub!"); }

/**
 * Sends an Unstructured Supplementary Service Data (USSD) request to the mobile network and
 * informs the caller of the response via the supplied {@code callback}.
 * <p>Carriers define USSD codes which can be sent by the user to request information such as
 * the user's current data balance or minutes balance.
 * <p>Requires permission:
 * {@link android.Manifest.permission#CALL_PHONE}
 * <br>
 * Requires {@link android.Manifest.permission#CALL_PHONE}
 * @param ussdRequest the USSD command to be executed.
 * @param callback called by the framework to inform the caller of the result of executing the
 *                 USSD request (see {@link UssdResponseCallback}).
 * @param handler the {@link Handler} to run the request on.
 * @apiSince 26
 */

public void sendUssdRequest(java.lang.String ussdRequest, android.telephony.TelephonyManager.UssdResponseCallback callback, android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * Whether the device is currently on a technology (e.g. UMTS or LTE) which can support
 * voice and data simultaneously. This can change based on location or network condition.
 *
 * @return {@code true} if simultaneous voice and data supported, and {@code false} otherwise.
 * @apiSince 26
 */

public boolean isConcurrentVoiceAndDataSupported() { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public boolean handlePinMmi(java.lang.String dialString) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public boolean handlePinMmiForSubscriber(int subId, java.lang.String dialString) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public void toggleRadioOnOff() { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public boolean setRadio(boolean turnOn) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public boolean setRadioPower(boolean turnOn) { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE} or {@link android.Manifest.permission#READ_PHONE_STATE}
 * @return current modem radio state.
 *
 * <p>Requires permission: {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE} or
 * {@link android.Manifest.permission#READ_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * Value is {@link android.telephony.TelephonyManager#RADIO_POWER_OFF}, {@link android.telephony.TelephonyManager#RADIO_POWER_ON}, or {@link android.telephony.TelephonyManager#RADIO_POWER_UNAVAILABLE}
 * @hide
 */

public int getRadioPowerState() { throw new RuntimeException("Stub!"); }

/** @hide */

public void updateServiceLocation() { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public boolean enableDataConnectivity() { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public boolean disableDataConnectivity() { throw new RuntimeException("Stub!"); }

/** @hide */

public boolean isDataConnectivityPossible() { throw new RuntimeException("Stub!"); }

/** @hide */

public boolean needsOtaServiceProvisioning() { throw new RuntimeException("Stub!"); }

/**
 * Turns mobile data on or off.
 * If this object has been created with {@link #createForSubscriptionId}, applies to the given
 * subId. Otherwise, applies to {@link SubscriptionManager#getDefaultDataSubscriptionId()}
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param enable Whether to enable mobile data.
 *
 * @apiSince 26
 */

public void setDataEnabled(boolean enable) { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide
 * @deprecated use {@link #setDataEnabled(boolean)} instead.
 */

@Deprecated
public void setDataEnabled(int subId, boolean enable) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated use {@link #isDataEnabled()} instead.
 * @hide
 */

@Deprecated
public boolean getDataEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether mobile data is enabled or not per user setting. There are other factors
 * that could disable mobile data, but they are not considered here.
 *
 * If this object has been created with {@link #createForSubscriptionId}, applies to the given
 * subId. Otherwise, applies to {@link SubscriptionManager#getDefaultDataSubscriptionId()}
 *
 * <p>Requires one of the following permissions:
 * {@link android.Manifest.permission#ACCESS_NETWORK_STATE},
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE}, or that the calling app has carrier
 * privileges (see {@link #hasCarrierPrivileges}).
 *
 * <p>Note that this does not take into account any data restrictions that may be present on the
 * calling app. Such restrictions may be inspected with
 * {@link ConnectivityManager#getRestrictBackgroundStatus}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_NETWORK_STATE} or {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @return true if mobile data is enabled.
 * @apiSince 26
 */

public boolean isDataEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether mobile data roaming is enabled on the subscription.
 *
 * <p>If this object has been created with {@link #createForSubscriptionId}, applies to the
 * given subId. Otherwise, applies to {@link SubscriptionManager#getDefaultDataSubscriptionId()}
 *
 * <p>Requires one of the following permissions:
 * {@link android.Manifest.permission#ACCESS_NETWORK_STATE},
 * {@link android.Manifest.permission#READ_PHONE_STATE} or that the calling app
 * has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_NETWORK_STATE} or {@link android.Manifest.permission#READ_PHONE_STATE}
 * @return {@code true} if the data roaming is enabled on the subscription, otherwise return
 * {@code false}.
 * @apiSince 29
 */

public boolean isDataRoamingEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Enables/Disables the data roaming on the subscription.
 *
 * <p>If this object has been created with {@link #createForSubscriptionId}, applies to the
 * given subId. Otherwise, applies to {@link SubscriptionManager#getDefaultDataSubscriptionId()}
 *
 * <p> Requires permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE} or that the calling app has carrier
 * privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param isEnabled {@code true} to enable mobile data roaming, otherwise disable it.
 *
 * @hide
 */

public void setDataRoamingEnabled(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated use {@link #isDataEnabled()} instead.
 * @hide
 */

@Deprecated
public boolean getDataEnabled(int subId) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @hide */

public void enableVideoCalling(boolean enable) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE} or {@link android.Manifest.permission#READ_PHONE_STATE}
 * @hide */

public boolean isVideoCallingEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Whether the device supports configuring the DTMF tone length.
 *
 * @return {@code true} if the DTMF tone length can be changed, and {@code false} otherwise.
 * @apiSince 23
 */

public boolean canChangeDtmfToneLength() { throw new RuntimeException("Stub!"); }

/**
 * Whether the device is a world phone.
 *
 * @return {@code true} if the device is a world phone, and {@code false} otherwise.
 * @apiSince 23
 */

public boolean isWorldPhone() { throw new RuntimeException("Stub!"); }

/**
 * @deprecated Use {@link TelecomManager#isTtySupported()} instead
 * Whether the phone supports TTY mode.
 *
 * @return {@code true} if the device supports TTY mode, and {@code false} otherwise.
 *
 * @apiSince 23
 * @deprecatedSince 28
 */

@Deprecated
public boolean isTtyModeSupported() { throw new RuntimeException("Stub!"); }

/**
 * Determines whether the device currently supports RTT (Real-time text). Based both on carrier
 * support for the feature and device firmware support.
 *
 * @return {@code true} if the device and carrier both support RTT, {@code false} otherwise.
 * @apiSince 29
 */

public boolean isRttSupported() { throw new RuntimeException("Stub!"); }

/**
 * Whether the phone supports hearing aid compatibility.
 *
 * @return {@code true} if the device supports hearing aid compatibility, and {@code false}
 * otherwise.
 * @apiSince 23
 */

public boolean isHearingAidCompatibilitySupported() { throw new RuntimeException("Stub!"); }

/**
 * Set SIM card power state.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param state  State of SIM (power down, power up, pass through)
 * @see #CARD_POWER_DOWN
 * @see #CARD_POWER_UP
 * @see #CARD_POWER_UP_PASS_THROUGH
 * Callers should monitor for {@link TelephonyIntents#ACTION_SIM_STATE_CHANGED}
 * broadcasts to determine success or failure and timeout if needed.
 *
 * <p>Requires Permission:
 *   {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}
 *
 * {@hide}
 **/

public void setSimPowerState(int state) { throw new RuntimeException("Stub!"); }

/**
 * Set SIM card power state.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param slotIndex SIM slot id
 * @param state  State of SIM (power down, power up, pass through)
 * @see #CARD_POWER_DOWN
 * @see #CARD_POWER_UP
 * @see #CARD_POWER_UP_PASS_THROUGH
 * Callers should monitor for {@link TelephonyIntents#ACTION_SIM_STATE_CHANGED}
 * broadcasts to determine success or failure and timeout if needed.
 *
 * <p>Requires Permission:
 *   {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}
 *
 * {@hide}
 **/

public void setSimPowerStateForSlot(int slotIndex, int state) { throw new RuntimeException("Stub!"); }

/**
 * Returns a locale based on the country and language from the SIM. Returns {@code null} if
 * no locale could be derived from subscriptions.
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE READ_PRIVILEGED_PHONE_STATE}
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @see Locale#toLanguageTag()
 *
 * @hide
 */

@android.annotation.Nullable
public java.util.Locale getSimLocale() { throw new RuntimeException("Stub!"); }

/**
 * Returns the current {@link ServiceState} information.
 *
 * <p>If this object has been created with {@link #createForSubscriptionId}, applies to the
 * given subId. Otherwise, applies to {@link SubscriptionManager#getDefaultSubscriptionId()}
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges})
 * and {@link android.Manifest.permission#ACCESS_COARSE_LOCATION}.
 
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE} and {@link android.Manifest.permission#ACCESS_COARSE_LOCATION}
 * @apiSince 26
 */

public android.telephony.ServiceState getServiceState() { throw new RuntimeException("Stub!"); }

/**
 * Returns the URI for the per-account voicemail ringtone set in Phone settings.
 *
 * @param accountHandle The handle for the {@link PhoneAccount} for which to retrieve the
 * voicemail ringtone.
 * @return The URI for the ringtone to play when receiving a voicemail from a specific
 * PhoneAccount.
 * @apiSince 24
 */

public android.net.Uri getVoicemailRingtoneUri(android.telecom.PhoneAccountHandle accountHandle) { throw new RuntimeException("Stub!"); }

/**
 * Sets the per-account voicemail ringtone.
 *
 * <p>Requires that the calling app is the default dialer, or has carrier privileges (see
 * {@link #hasCarrierPrivileges}, or has permission
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 *
 * @param phoneAccountHandle The handle for the {@link PhoneAccount} for which to set the
 * voicemail ringtone.
 * @param uri The URI for the ringtone to play when receiving a voicemail from a specific
 * PhoneAccount.
 *
 * @deprecated Use {@link android.provider.Settings#ACTION_CHANNEL_NOTIFICATION_SETTINGS}
 * instead.
 * @apiSince 26
 * @deprecatedSince 28
 */

@Deprecated
public void setVoicemailRingtoneUri(android.telecom.PhoneAccountHandle phoneAccountHandle, android.net.Uri uri) { throw new RuntimeException("Stub!"); }

/**
 * Returns whether vibration is set for voicemail notification in Phone settings.
 *
 * @param accountHandle The handle for the {@link PhoneAccount} for which to retrieve the
 * voicemail vibration setting.
 * @return {@code true} if the vibration is set for this PhoneAccount, {@code false} otherwise.
 * @apiSince 24
 */

public boolean isVoicemailVibrationEnabled(android.telecom.PhoneAccountHandle accountHandle) { throw new RuntimeException("Stub!"); }

/**
 * Sets the per-account preference whether vibration is enabled for voicemail notifications.
 *
 * <p>Requires that the calling app is the default dialer, or has carrier privileges (see
 * {@link #hasCarrierPrivileges}, or has permission
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 *
 * @param phoneAccountHandle The handle for the {@link PhoneAccount} for which to set the
 * voicemail vibration setting.
 * @param enabled Whether to enable or disable vibration for voicemail notifications from a
 * specific PhoneAccount.
 *
 * @deprecated Use {@link android.provider.Settings#ACTION_CHANNEL_NOTIFICATION_SETTINGS}
 * instead.
 * @apiSince 26
 * @deprecatedSince 28
 */

@Deprecated
public void setVoicemailVibrationEnabled(android.telecom.PhoneAccountHandle phoneAccountHandle, boolean enabled) { throw new RuntimeException("Stub!"); }

/**
 * Returns carrier id of the current subscription.
 * <p>To recognize a carrier (including MVNO) as a first-class identity, Android assigns each
 * carrier with a canonical integer a.k.a. carrier id. The carrier ID is an Android
 * platform-wide identifier for a carrier. AOSP maintains carrier ID assignments in
 * <a href="https://android.googlesource.com/platform/packages/providers/TelephonyProvider/+/master/assets/carrier_list.textpb">here</a>
 *
 * <p>Apps which have carrier-specific configurations or business logic can use the carrier id
 * as an Android platform-wide identifier for carriers.
 *
 * @return Carrier id of the current subscription. Return {@link #UNKNOWN_CARRIER_ID} if the
 * subscription is unavailable or the carrier cannot be identified.
 * @apiSince 28
 */

public int getSimCarrierId() { throw new RuntimeException("Stub!"); }

/**
 * Returns carrier id name of the current subscription.
 * <p>Carrier id name is a user-facing name of carrier id returned by
 * {@link #getSimCarrierId()}, usually the brand name of the subsidiary
 * (e.g. T-Mobile). Each carrier could configure multiple {@link #getSimOperatorName() SPN} but
 * should have a single carrier name. Carrier name is not a canonical identity,
 * use {@link #getSimCarrierId()} instead.
 * <p>The returned carrier name is unlocalized.
 *
 * @return Carrier name of the current subscription. Return {@code null} if the subscription is
 * unavailable or the carrier cannot be identified.
 * @apiSince 28
 */

@android.annotation.Nullable
public java.lang.CharSequence getSimCarrierIdName() { throw new RuntimeException("Stub!"); }

/**
 * Returns fine-grained carrier ID of the current subscription.
 *
 * A specific carrier ID can represent the fact that a carrier may be in effect an aggregation
 * of other carriers (ie in an MVNO type scenario) where each of these specific carriers which
 * are used to make up the actual carrier service may have different carrier configurations.
 * A specific carrier ID could also be used, for example, in a scenario where a carrier requires
 * different carrier configuration for different service offering such as a prepaid plan.
 *
 * the specific carrier ID would be used for configuration purposes, but apps wishing to know
 * about the carrier itself should use the regular carrier ID returned by
 * {@link #getSimCarrierId()}.
 *
 * e.g, Tracfone SIMs could return different specific carrier ID based on IMSI from current
 * subscription while carrier ID remains the same.
 *
 * <p>For carriers without fine-grained specific carrier ids, return {@link #getSimCarrierId()}
 * <p>Specific carrier ids are defined in the same way as carrier id
 * <a href="https://android.googlesource.com/platform/packages/providers/TelephonyProvider/+/master/assets/carrier_list.textpb">here</a>
 * except each with a "parent" id linking to its top-level carrier id.
 *
 * @return Returns fine-grained carrier id of the current subscription.
 * Return {@link #UNKNOWN_CARRIER_ID} if the subscription is unavailable or the carrier cannot
 * be identified.
 * @apiSince 29
 */

public int getSimSpecificCarrierId() { throw new RuntimeException("Stub!"); }

/**
 * Similar like {@link #getSimCarrierIdName()}, returns user-facing name of the
 * specific carrier id returned by {@link #getSimSpecificCarrierId()}.
 *
 * The specific carrier ID would be used for configuration purposes, but apps wishing to know
 * about the carrier itself should use the regular carrier ID returned by
 * {@link #getSimCarrierIdName()}.
 *
 * <p>The returned name is unlocalized.
 *
 * @return user-facing name of the subscription specific carrier id. Return {@code null} if the
 * subscription is unavailable or the carrier cannot be identified.
 * @apiSince 29
 */

@android.annotation.Nullable
public java.lang.CharSequence getSimSpecificCarrierIdName() { throw new RuntimeException("Stub!"); }

/**
 * Returns carrier id based on sim MCCMNC (returned by {@link #getSimOperator()}) only.
 * This is used for fallback when configurations/logic for exact carrier id
 * {@link #getSimCarrierId()} are not found.
 *
 * Android carrier id table <a href="https://android.googlesource.com/platform/packages/providers/TelephonyProvider/+/master/assets/carrier_list.textpb">here</a>
 * can be updated out-of-band, its possible a MVNO (Mobile Virtual Network Operator) carrier
 * was not fully recognized and assigned to its MNO (Mobile Network Operator) carrier id
 * by default. After carrier id table update, a new carrier id was assigned. If apps don't
 * take the update with the new id, it might be helpful to always fallback by using carrier
 * id based on MCCMNC if there is no match.
 *
 * @return matching carrier id from sim MCCMNC. Return {@link #UNKNOWN_CARRIER_ID} if the
 * subscription is unavailable or the carrier cannot be identified.
 * @apiSince 29
 */

public int getCarrierIdFromSimMccMnc() { throw new RuntimeException("Stub!"); }

/**
 * Return the application ID for the uicc application type like {@link #APPTYPE_CSIM}.
 * All uicc applications are uniquely identified by application ID, represented by the hex
 * string. e.g, A00000015141434C00. See ETSI 102.221 and 101.220
 * <p>Requires Permission:
 *   {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param appType the uicc app type.
 * Value is {@link android.telephony.TelephonyManager#APPTYPE_SIM}, {@link android.telephony.TelephonyManager#APPTYPE_USIM}, {@link android.telephony.TelephonyManager#APPTYPE_RUIM}, {@link android.telephony.TelephonyManager#APPTYPE_CSIM}, or {@link android.telephony.TelephonyManager#APPTYPE_ISIM}
 * @return Application ID for specified app type or {@code null} if no uicc or error.
 * @hide
 */

@android.annotation.Nullable
public java.lang.String getAidForAppType(int appType) { throw new RuntimeException("Stub!"); }

/**
 * Return the Preferred Roaming List Version
 *
 * Requires that the calling app has READ_PRIVILEGED_PHONE_STATE permission
 *
 * @return PRLVersion or null if error.
 * @hide
 */

public java.lang.String getCdmaPrlVersion() { throw new RuntimeException("Stub!"); }

/**
 * Get snapshot of Telephony histograms
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @return List of Telephony histograms
 * Requires Permission:
 *   {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}
 * Or the calling app has carrier privileges.
 * @hide
 */

public java.util.List<android.telephony.TelephonyHistogram> getTelephonyHistograms() { throw new RuntimeException("Stub!"); }

/**
 * Set the allowed carrier list for slotIndex
 * Require system privileges. In the future we may add this to carrier APIs.
 *
 * <p>Requires Permission:
 *   {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 *
 * <p>This method works only on devices with {@link
 * android.content.pm.PackageManager#FEATURE_TELEPHONY_CARRIERLOCK} enabled.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @deprecated use setCarrierRestrictionRules instead
 *
 * @return The number of carriers set successfully. Should be length of
 * carrierList on success; -1 if carrierList null or on error.
 * @hide
 */

@Deprecated
public int setAllowedCarriers(int slotIndex, java.util.List<android.service.carrier.CarrierIdentifier> carriers) { throw new RuntimeException("Stub!"); }

/**
 * Set the allowed carrier list and the excluded carrier list indicating the priority between
 * the two lists.
 * Requires system privileges.
 *
 * <p>Requires Permission:
 *   {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 *
 * <p>This method works only on devices with {@link
 * android.content.pm.PackageManager#FEATURE_TELEPHONY_CARRIERLOCK} enabled.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param rules This value must never be {@code null}.
 * @return {@link #SET_CARRIER_RESTRICTION_SUCCESS} in case of success.
 * {@link #SET_CARRIER_RESTRICTION_NOT_SUPPORTED} if the modem does not support the
 * configuration. {@link #SET_CARRIER_RESTRICTION_ERROR} in all other error cases.
 * Value is {@link android.telephony.TelephonyManager#SET_CARRIER_RESTRICTION_SUCCESS}, {@link android.telephony.TelephonyManager#SET_CARRIER_RESTRICTION_NOT_SUPPORTED}, or {@link android.telephony.TelephonyManager#SET_CARRIER_RESTRICTION_ERROR}
 * @hide
 */

public int setCarrierRestrictionRules(@android.annotation.NonNull android.telephony.CarrierRestrictionRules rules) { throw new RuntimeException("Stub!"); }

/**
 * Get the allowed carrier list for slotIndex.
 * Requires system privileges.
 *
 * <p>This method returns valid data on devices with {@link
 * android.content.pm.PackageManager#FEATURE_TELEPHONY_CARRIERLOCK} enabled.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @deprecated Apps should use {@link getCarriersRestrictionRules} to retrieve the list of
 * allowed and excliuded carriers, as the result of this API is valid only when the excluded
 * list is empty. This API could return an empty list, even if some restrictions are present.
 *
 * @return List of {@link android.telephony.CarrierIdentifier}; empty list
 * means all carriers are allowed.
 * @hide
 */

@Deprecated
public java.util.List<android.service.carrier.CarrierIdentifier> getAllowedCarriers(int slotIndex) { throw new RuntimeException("Stub!"); }

/**
 * Get the allowed carrier list and the excluded carrier list indicating the priority between
 * the two lists.
 * Require system privileges. In the future we may add this to carrier APIs.
 *
 * <p>This method returns valid data on devices with {@link
 * android.content.pm.PackageManager#FEATURE_TELEPHONY_CARRIERLOCK} enabled.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return {@link CarrierRestrictionRules} which contains the allowed carrier list and the
 * excluded carrier list with the priority between the two lists. Returns {@code null}
 * in case of error.
 * @hide
 */

@android.annotation.Nullable
public android.telephony.CarrierRestrictionRules getCarrierRestrictionRules() { throw new RuntimeException("Stub!"); }

/**
 * Used to enable or disable carrier data by the system based on carrier signalling or
 * carrier privileged apps. Different from {@link #setDataEnabled(boolean)} which is linked to
 * user settings, carrier data on/off won't affect user settings but will bypass the
 * settings and turns off data internally if set to {@code false}.
 *
 * <p>If this object has been created with {@link #createForSubscriptionId}, applies to the
 * given subId. Otherwise, applies to {@link SubscriptionManager#getDefaultDataSubscriptionId()}
 *
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param enabled control enable or disable carrier data.
 * @hide
 */

public void setCarrierDataEnabled(boolean enabled) { throw new RuntimeException("Stub!"); }

/**
 * Checks if phone is in emergency callback mode.
 *
 * <p>If this object has been created with {@link #createForSubscriptionId}, applies to the
 * given subId. Otherwise, applies to {@link SubscriptionManager#getDefaultSubscriptionId()}
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return true if phone is in emergency callback mode.
 * @hide
 */

public boolean getEmergencyCallbackMode() { throw new RuntimeException("Stub!"); }

/**
 * Get the most recently available signal strength information.
 *
 * Get the most recent SignalStrength information reported by the modem. Due
 * to power saving this information may not always be current.
 * @return the most recent cached signal strength info from the modem
 
 * This value may be {@code null}.
 * @apiSince 28
 */

@android.annotation.Nullable
public android.telephony.SignalStrength getSignalStrength() { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return Modem supported radio access family bitmask
 *
 * <p>Requires permission: {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE} or
 * that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 * Value is either <code>0</code> or a combination of {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_LTE_CA}, and {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_NR}
 * @hide
 */

public long getSupportedRadioAccessFamily() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether {@link TelephonyManager#ACTION_EMERGENCY_ASSISTANCE emergency assistance} is
 * available on the device.
 * <p>
 * Requires permission: {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return {@code true} if emergency assistance is available, {@code false} otherwise
 *
 * @hide
 */

public boolean isEmergencyAssistanceEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Get the emergency number list based on current locale, sim, default, modem and network.
 *
 * <p>In each returned list, the emergency number {@link EmergencyNumber} coming from higher
 * priority sources will be located at the smaller index; the priority order of sources are:
 * {@link EmergencyNumber#EMERGENCY_NUMBER_SOURCE_NETWORK_SIGNALING} >
 * {@link EmergencyNumber#EMERGENCY_NUMBER_SOURCE_SIM} >
 * {@link EmergencyNumber#EMERGENCY_NUMBER_SOURCE_DATABASE} >
 * {@link EmergencyNumber#EMERGENCY_NUMBER_SOURCE_DEFAULT} >
 * {@link EmergencyNumber#EMERGENCY_NUMBER_SOURCE_MODEM_CONFIG}
 *
 * <p>The subscriptions which the returned list would be based on, are all the active
 * subscriptions, no matter which subscription could be used to create TelephonyManager.
 *
 * <p>Requires permission {@link android.Manifest.permission#READ_PHONE_STATE} or the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @return Map including the keys as the active subscription IDs (Note: if there is no active
 * subscription, the key is {@link SubscriptionManager#getDefaultSubscriptionId}) and the value
 * as the list of {@link EmergencyNumber}; empty Map if this information is not available;
 * or throw a SecurityException if the caller does not have the permission.
 
 * This value will never be {@code null}.
 * @apiSince 29
 */

@android.annotation.NonNull
public java.util.Map<java.lang.Integer,java.util.List<android.telephony.emergency.EmergencyNumber>> getEmergencyNumberList() { throw new RuntimeException("Stub!"); }

/**
 * Get the per-category emergency number list based on current locale, sim, default, modem
 * and network.
 *
 * <p>In each returned list, the emergency number {@link EmergencyNumber} coming from higher
 * priority sources will be located at the smaller index; the priority order of sources are:
 * {@link EmergencyNumber#EMERGENCY_NUMBER_SOURCE_NETWORK_SIGNALING} >
 * {@link EmergencyNumber#EMERGENCY_NUMBER_SOURCE_SIM} >
 * {@link EmergencyNumber#EMERGENCY_NUMBER_SOURCE_DATABASE} >
 * {@link EmergencyNumber#EMERGENCY_NUMBER_SOURCE_DEFAULT} >
 * {@link EmergencyNumber#EMERGENCY_NUMBER_SOURCE_MODEM_CONFIG}
 *
 * <p>The subscriptions which the returned list would be based on, are all the active
 * subscriptions, no matter which subscription could be used to create TelephonyManager.
 *
 * <p>Requires permission {@link android.Manifest.permission#READ_PHONE_STATE} or the calling
 * app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @param categories the emergency service categories which are the bitwise-OR combination of
 * the following constants:
 * <ol>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_UNSPECIFIED} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_POLICE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AMBULANCE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_FIRE_BRIGADE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MARINE_GUARD} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MOUNTAIN_RESCUE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MIEC} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AIEC} </li>
 * </ol>
 * Value is either <code>0</code> or a combination of {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_UNSPECIFIED}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_POLICE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AMBULANCE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_FIRE_BRIGADE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MARINE_GUARD}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MOUNTAIN_RESCUE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MIEC}, and {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AIEC}
 * @return Map including the keys as the active subscription IDs (Note: if there is no active
 * subscription, the key is {@link SubscriptionManager#getDefaultSubscriptionId}) and the value
 * as the list of {@link EmergencyNumber}; empty Map if this information is not available;
 * or throw a SecurityException if the caller does not have the permission.
 
 * This value will never be {@code null}.
 * @apiSince 29
 */

@android.annotation.NonNull
public java.util.Map<java.lang.Integer,java.util.List<android.telephony.emergency.EmergencyNumber>> getEmergencyNumberList(int categories) { throw new RuntimeException("Stub!"); }

/**
 * Identifies if the supplied phone number is an emergency number that matches a known
 * emergency number based on current locale, SIM card(s), Android database, modem, network,
 * or defaults.
 *
 * <p>This method assumes that only dialable phone numbers are passed in; non-dialable
 * numbers are not considered emergency numbers. A dialable phone number consists only
 * of characters/digits identified by {@link PhoneNumberUtils#isDialable(char)}.
 *
 * <p>The subscriptions which the identification would be based on, are all the active
 * subscriptions, no matter which subscription could be used to create TelephonyManager.
 *
 * @param number - the number to look up
 * This value must never be {@code null}.
 * @return {@code true} if the given number is an emergency number based on current locale,
 * SIM card(s), Android database, modem, network or defaults; {@code false} otherwise.
 * @apiSince 29
 */

public boolean isEmergencyNumber(@android.annotation.NonNull java.lang.String number) { throw new RuntimeException("Stub!"); }

/**
 * Checks if the supplied number is an emergency number based on current locale, sim, default,
 * modem and network.
 *
 * <p> Specifically, this method will return {@code true} if the specified number is an
 * emergency number, *or* if the number simply starts with the same digits as any current
 * emergency number.
 *
 * <p>The subscriptions which the identification would be based on, are all the active
 * subscriptions, no matter which subscription could be used to create TelephonyManager.
 *
 * <p>Requires permission: {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE} or
 * that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param number - the number to look up
 * This value must never be {@code null}.
 * @return {@code true} if the given number is an emergency number or it simply starts with
 * the same digits of any current emergency number based on current locale, sim, modem and
 * network; {@code false} if it is not; or throw an SecurityException if the caller does not
 * have the required permission/privileges
 *
 * @hide
 */

public boolean isPotentialEmergencyNumber(@android.annotation.NonNull java.lang.String number) { throw new RuntimeException("Stub!"); }

/**
 * Set preferred opportunistic data subscription id.
 *
 * Switch internet data to preferred opportunistic data subscription id. This api
 * can result in lose of internet connectivity for short period of time while internet data
 * is handed over.
 * <p>Requires that the calling app has carrier privileges on both primary and
 * secondary subscriptions (see
 * {@link #hasCarrierPrivileges}), or has permission
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 *
 * @param subId which opportunistic subscription
 * {@link SubscriptionManager#getOpportunisticSubscriptions} is preferred for cellular data.
 * Pass {@link SubscriptionManager#DEFAULT_SUBSCRIPTION_ID} to unset the preference
 * @param needValidation whether validation is needed before switch happens.
 * @param executor The executor of where the callback will execute.
 * This value may be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback Callback will be triggered once it succeeds or failed.
 *                 See {@link TelephonyManager.SetOpportunisticSubscriptionResult}
 *                 for more details. Pass null if don't care about the result.
 
 * This value may be {@code null}.
 * @apiSince 29
 */

public void setPreferredOpportunisticDataSubscription(int subId, boolean needValidation, @android.annotation.Nullable java.util.concurrent.Executor executor, @android.annotation.Nullable java.util.function.Consumer<java.lang.Integer> callback) { throw new RuntimeException("Stub!"); }

/**
 * Get preferred opportunistic data subscription Id
 *
 * <p>Requires that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}),
 * or has either READ_PRIVILEGED_PHONE_STATE
 * or {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE} permission.
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE} or {@link android.Manifest.permission#READ_PHONE_STATE}
 * @return subId preferred opportunistic subscription id or
 * {@link SubscriptionManager#DEFAULT_SUBSCRIPTION_ID} if there are no preferred
 * subscription id
 *
 * @apiSince 29
 */

public int getPreferredOpportunisticDataSubscription() { throw new RuntimeException("Stub!"); }

/**
 * Update availability of a list of networks in the current location.
 *
 * This api should be called to inform OpportunisticNetwork Service about the availability
 * of a network at the current location. This information will be used by OpportunisticNetwork
 * service to enable modem stack and to attach to the network. If an empty list is passed,
 * it is assumed that no network is available and will result in disabling the modem stack
 * to save power. This api do not switch internet data once network attach is completed.
 * Use {@link TelephonyManager#setPreferredOpportunisticDataSubscription}
 * to switch internet data after network attach is complete.
 * Requires that the calling app has carrier privileges on both primary and
 * secondary subscriptions (see {@link #hasCarrierPrivileges}), or has permission
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 * @param availableNetworks is a list of available network information.
 * This value must never be {@code null}.
 * @param executor The executor of where the callback will execute.
 * This value may be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback Callback will be triggered once it succeeds or failed.
 *
 
 * Value is {@link android.telephony.TelephonyManager#UPDATE_AVAILABLE_NETWORKS_SUCCESS}, {@link android.telephony.TelephonyManager#UPDATE_AVAILABLE_NETWORKS_UNKNOWN_FAILURE}, {@link android.telephony.TelephonyManager#UPDATE_AVAILABLE_NETWORKS_ABORTED}, {@link android.telephony.TelephonyManager#UPDATE_AVAILABLE_NETWORKS_INVALID_ARGUMENTS}, or {@link android.telephony.TelephonyManager#UPDATE_AVAILABLE_NETWORKS_NO_CARRIER_PRIVILEGE}
 
 * This value may be {@code null}.
 * @apiSince 29
 */

public void updateAvailableNetworks(@android.annotation.NonNull java.util.List<android.telephony.AvailableNetworkInfo> availableNetworks, @android.annotation.Nullable java.util.concurrent.Executor executor, @android.annotation.Nullable java.util.function.Consumer<java.lang.Integer> callback) { throw new RuntimeException("Stub!"); }

/**
 * Enable or disable a logical modem stack. When a logical modem is disabled, the corresponding
 * SIM will still be visible to the user but its mapping modem will not have any radio activity.
 * For example, we will disable a modem when user or system believes the corresponding SIM
 * is temporarily not needed (e.g. out of coverage), and will enable it back on when needed.
 *
 * Requires that the calling app has permission
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE}.
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param slotIndex which corresponding modem will operate on.
 * @param enable whether to enable or disable the modem stack.
 * @return whether the operation is successful.
 *
 * @hide
 */

public boolean enableModemForSlot(int slotIndex, boolean enable) { throw new RuntimeException("Stub!"); }

/**
 * Indicate if the user is allowed to use multiple SIM cards at the same time to register
 * on the network (e.g. Dual Standby or Dual Active) when the device supports it, or if the
 * usage is restricted. This API is used to prevent usage of multiple SIM card, based on
 * policies of the carrier.
 * <p>Note: the API does not prevent access to the SIM cards for operations that don't require
 * access to the network.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param isMultiSimCarrierRestricted true if usage of multiple SIMs is restricted, false
 * otherwise.
 *
 * @hide
 */

public void setMultiSimCarrierRestriction(boolean isMultiSimCarrierRestricted) { throw new RuntimeException("Stub!"); }

/**
 * Returns if the usage of multiple SIM cards at the same time to register on the network
 * (e.g. Dual Standby or Dual Active) is supported by the device and by the carrier.
 *
 * <p>Requires Permission: {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE}
 * or that the calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @return {@link #MULTISIM_ALLOWED} if the device supports multiple SIMs.
 * {@link #MULTISIM_NOT_SUPPORTED_BY_HARDWARE} if the device does not support multiple SIMs.
 * {@link #MULTISIM_NOT_SUPPORTED_BY_CARRIER} in the device supports multiple SIMs, but the
 * functionality is restricted by the carrier.
 
 * Value is {@link android.telephony.TelephonyManager#MULTISIM_ALLOWED}, {@link android.telephony.TelephonyManager#MULTISIM_NOT_SUPPORTED_BY_HARDWARE}, or {@link android.telephony.TelephonyManager#MULTISIM_NOT_SUPPORTED_BY_CARRIER}
 * @apiSince 29
 */

public int isMultiSimSupported() { throw new RuntimeException("Stub!"); }

/**
 * Switch configs to enable multi-sim or switch back to single-sim
 * <p>Requires Permission:
 * {@link android.Manifest.permission#MODIFY_PHONE_STATE MODIFY_PHONE_STATE} or that the
 * calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * Note: with only carrier privileges, it is not allowed to switch from multi-sim
 * to single-sim
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param numOfSims number of live SIMs we want to switch to
 * @throws android.os.RemoteException
 * @apiSince 29
 */

public void switchMultiSimConfig(int numOfSims) { throw new RuntimeException("Stub!"); }

/**
 * Get whether making changes to modem configurations by {@link #switchMultiSimConfig(int)} will
 * trigger device reboot.
 * The modem configuration change refers to switching from single SIM configuration to DSDS
 * or the other way around.
 *
 *  <p>Requires Permission:
 * {@link android.Manifest.permission#READ_PHONE_STATE READ_PHONE_STATE} or that the
 * calling app has carrier privileges (see {@link #hasCarrierPrivileges}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @return {@code true} if reboot will be triggered after making changes to modem
 * configurations, otherwise return {@code false}.
 * @apiSince 29
 */

public boolean doesSwitchMultiSimConfigTriggerReboot() { throw new RuntimeException("Stub!"); }

/**
 * Intent sent when an error occurs that debug tools should log and possibly take further
 * action such as capturing vendor-specific logs.
 *
 * A privileged application that reads these events should take appropriate vendor-specific
 * action to record the event and collect further information to assist in analysis, debugging,
 * and resolution of any associated issue.
 *
 * <p>This event should not be used for generic logging or diagnostic monitoring purposes and
 * should generally be sent at a low rate. Instead, this mechanism should be used for the
 * framework to notify a debugging application that an event (such as a bug) has occured
 * within the framework if that event should trigger the collection and preservation of other
 * more detailed device state for debugging.
 *
 * <p>At most one application can receive these events and should register a receiver in
 * in the application manifest. For performance reasons, if no application to receive these
 * events is detected at boot, then these events will not be sent.
 *
 * <p>Each event will include an {@link EXTRA_ANOMALY_ID} that will uniquely identify the
 * event that has occurred. Each event will be sent to the diagnostic monitor only once per
 * boot cycle (as another optimization).
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @see #EXTRA_ANOMALY_ID
 * @see #EXTRA_ANOMALY_DESCRIPTION
 * @hide
 */

public static final java.lang.String ACTION_ANOMALY_REPORTED = "android.telephony.action.ANOMALY_REPORTED";

/**
 * A service action that identifies
 * a {@link android.service.carrier.CarrierMessagingClientService} subclass in the
 * AndroidManifest.xml.
 *
 * <p>See {@link android.service.carrier.CarrierMessagingClientService} for the details.
 * @apiSince 29
 */

public static final java.lang.String ACTION_CARRIER_MESSAGING_CLIENT_SERVICE = "android.telephony.action.CARRIER_MESSAGING_CLIENT_SERVICE";

/**
 * Open the voicemail settings activity to make changes to voicemail configuration.
 *
 * <p>
 * The {@link #EXTRA_PHONE_ACCOUNT_HANDLE} extra indicates which {@link PhoneAccountHandle} to
 * configure voicemail.
 * The {@link #EXTRA_HIDE_PUBLIC_SETTINGS} hides settings the dialer will modify through public
 * API if set.
 *
 * @see #EXTRA_PHONE_ACCOUNT_HANDLE
 * @see #EXTRA_HIDE_PUBLIC_SETTINGS
 * @apiSince 23
 */

public static final java.lang.String ACTION_CONFIGURE_VOICEMAIL = "android.telephony.action.CONFIGURE_VOICEMAIL";

/**
 * Broadcast intent action for network country code changes.
 *
 * <p>
 * The {@link #EXTRA_NETWORK_COUNTRY} extra indicates the country code of the current
 * network returned by {@link #getNetworkCountryIso()}.
 *
 * <p>There may be a delay of several minutes before reporting that no country is detected.
 *
 * @see #EXTRA_NETWORK_COUNTRY
 * @see #getNetworkCountryIso()
 * @apiSince 29
 */

public static final java.lang.String ACTION_NETWORK_COUNTRY_CHANGED = "android.telephony.action.NETWORK_COUNTRY_CHANGED";

/**
 * Broadcast intent action indicating that the call state
 * on the device has changed.
 *
 * <p>
 * The {@link #EXTRA_STATE} extra indicates the new call state.
 * If a receiving app has {@link android.Manifest.permission#READ_CALL_LOG} permission, a second
 * extra {@link #EXTRA_INCOMING_NUMBER} provides the phone number for incoming and outgoing
 * calls as a String.
 * <p>
 * If the receiving app has
 * {@link android.Manifest.permission#READ_CALL_LOG} and
 * {@link android.Manifest.permission#READ_PHONE_STATE} permission, it will receive the
 * broadcast twice; one with the {@link #EXTRA_INCOMING_NUMBER} populated with the phone number,
 * and another with it blank.  Due to the nature of broadcasts, you cannot assume the order
 * in which these broadcasts will arrive, however you are guaranteed to receive two in this
 * case.  Apps which are interested in the {@link #EXTRA_INCOMING_NUMBER} can ignore the
 * broadcasts where {@link #EXTRA_INCOMING_NUMBER} is not present in the extras (e.g. where
 * {@link Intent#hasExtra(String)} returns {@code false}).
 * <p class="note">
 * This was a {@link android.content.Context#sendStickyBroadcast sticky}
 * broadcast in version 1.0, but it is no longer sticky.
 * Instead, use {@link #getCallState} to synchronously query the current call state.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PHONE_STATE}
 * @see #EXTRA_STATE
 * @see #EXTRA_INCOMING_NUMBER
 * @see #getCallState
 * @apiSince 3
 */

public static final java.lang.String ACTION_PHONE_STATE_CHANGED = "android.intent.action.PHONE_STATE";

/**
 * The Phone app sends this intent when a user opts to respond-via-message during an incoming
 * call. By default, the device's default SMS app consumes this message and sends a text message
 * to the caller. A third party app can also provide this functionality by consuming this Intent
 * with a {@link android.app.Service} and sending the message using its own messaging system.
 * <p>The intent contains a URI (available from {@link android.content.Intent#getData})
 * describing the recipient, using either the {@code sms:}, {@code smsto:}, {@code mms:},
 * or {@code mmsto:} URI schema. Each of these URI schema carry the recipient information the
 * same way: the path part of the URI contains the recipient's phone number or a comma-separated
 * set of phone numbers if there are multiple recipients. For example, {@code
 * smsto:2065551234}.</p>
 *
 * <p>The intent may also contain extras for the message text (in {@link
 * android.content.Intent#EXTRA_TEXT}) and a message subject
 * (in {@link android.content.Intent#EXTRA_SUBJECT}).</p>
 *
 * <p class="note"><strong>Note:</strong>
 * The intent-filter that consumes this Intent needs to be in a {@link android.app.Service}
 * that requires the
 * permission {@link android.Manifest.permission#SEND_RESPOND_VIA_MESSAGE}.</p>
 * <p>For example, the service that receives this intent can be declared in the manifest file
 * with an intent filter like this:</p>
 * <pre>
 * &lt;!-- Service that delivers SMS messages received from the phone "quick response" -->
 * &lt;service android:name=".HeadlessSmsSendService"
 *          android:permission="android.permission.SEND_RESPOND_VIA_MESSAGE"
 *          android:exported="true" >
 *   &lt;intent-filter>
 *     &lt;action android:name="android.intent.action.RESPOND_VIA_MESSAGE" />
 *     &lt;category android:name="android.intent.category.DEFAULT" />
 *     &lt;data android:scheme="sms" />
 *     &lt;data android:scheme="smsto" />
 *     &lt;data android:scheme="mms" />
 *     &lt;data android:scheme="mmsto" />
 *   &lt;/intent-filter>
 * &lt;/service></pre>
 * <p>
 * Output: nothing.
 * @apiSince 18
 */

public static final java.lang.String ACTION_RESPOND_VIA_MESSAGE = "android.intent.action.RESPOND_VIA_MESSAGE";

/**
 * Broadcast Action: A debug code has been entered in the dialer.
 * <p>
 * This intent is broadcast by the system and OEM telephony apps may need to receive these
 * broadcasts. And it requires the sender to be default dialer or has carrier privileges
 * (see {@link #hasCarrierPrivileges}).
 * <p>
 * These "secret codes" are used to activate developer menus by dialing certain codes.
 * And they are of the form {@code *#*#<code>#*#*}. The intent will have the data
 * URI: {@code android_secret_code://<code>}. It is possible that a manifest
 * receiver would be woken up even if it is not currently running.
 * <p>
 * It is supposed to replace {@link android.provider.Telephony.Sms.Intents#SECRET_CODE_ACTION}
 * in the next Android version.
 * Before that both of these two actions will be broadcast.
 * @apiSince 29
 */

public static final java.lang.String ACTION_SECRET_CODE = "android.telephony.action.SECRET_CODE";

/**
 * Broadcast intent action for letting the default dialer to know to show voicemail
 * notification.
 *
 * <p>
 * The {@link #EXTRA_PHONE_ACCOUNT_HANDLE} extra indicates which {@link PhoneAccountHandle} the
 * voicemail is received on.
 * The {@link #EXTRA_NOTIFICATION_COUNT} extra indicates the total numbers of unheard
 * voicemails.
 * The {@link #EXTRA_VOICEMAIL_NUMBER} extra indicates the voicemail number if available.
 * The {@link #EXTRA_CALL_VOICEMAIL_INTENT} extra is a {@link android.app.PendingIntent} that
 * will call the voicemail number when sent. This extra will be empty if the voicemail number
 * is not set, and {@link #EXTRA_LAUNCH_VOICEMAIL_SETTINGS_INTENT} will be set instead.
 * The {@link #EXTRA_LAUNCH_VOICEMAIL_SETTINGS_INTENT} extra is a
 * {@link android.app.PendingIntent} that will launch the voicemail settings. This extra is only
 * available when the voicemail number is not set.
 * The {@link #EXTRA_IS_REFRESH} extra indicates whether the notification is a refresh or a new
 * notification.
 *
 * @see #EXTRA_PHONE_ACCOUNT_HANDLE
 * @see #EXTRA_NOTIFICATION_COUNT
 * @see #EXTRA_VOICEMAIL_NUMBER
 * @see #EXTRA_CALL_VOICEMAIL_INTENT
 * @see #EXTRA_LAUNCH_VOICEMAIL_SETTINGS_INTENT
 * @see #EXTRA_IS_REFRESH
 * @apiSince 26
 */

public static final java.lang.String ACTION_SHOW_VOICEMAIL_NOTIFICATION = "android.telephony.action.SHOW_VOICEMAIL_NOTIFICATION";

/**
 * Broadcast Action: The sim application state has changed.
 * The intent will have the following extra values:</p>
 * <dl>
 *   <dt>{@link #EXTRA_SIM_STATE}</dt>
 *   <dd>The sim application state. One of:
 *     <dl>
 *       <dt>{@link #SIM_STATE_NOT_READY}</dt>
 *       <dd>SIM card applications not ready</dd>
 *       <dt>{@link #SIM_STATE_PIN_REQUIRED}</dt>
 *       <dd>SIM card PIN locked</dd>
 *       <dt>{@link #SIM_STATE_PUK_REQUIRED}</dt>
 *       <dd>SIM card PUK locked</dd>
 *       <dt>{@link #SIM_STATE_NETWORK_LOCKED}</dt>
 *       <dd>SIM card network locked</dd>
 *       <dt>{@link #SIM_STATE_PERM_DISABLED}</dt>
 *       <dd>SIM card permanently disabled due to PUK failures</dd>
 *       <dt>{@link #SIM_STATE_LOADED}</dt>
 *       <dd>SIM card data loaded</dd>
 *     </dl>
 *   </dd>
 * </dl>
 *
 * <p class="note">Requires the READ_PRIVILEGED_PHONE_STATE permission.
 *
 * <p class="note">The current state can also be queried using
 * {@link #getSimApplicationState()}.
 *
 * <p class="note">This is a protected intent that can only be sent by the system.
 * @hide
 */

public static final java.lang.String ACTION_SIM_APPLICATION_STATE_CHANGED = "android.telephony.action.SIM_APPLICATION_STATE_CHANGED";

/**
 * Broadcast Action: The sim card state has changed.
 * The intent will have the following extra values:</p>
 * <dl>
 *   <dt>{@link #EXTRA_SIM_STATE}</dt>
 *   <dd>The sim card state. One of:
 *     <dl>
 *       <dt>{@link #SIM_STATE_ABSENT}</dt>
 *       <dd>SIM card not found</dd>
 *       <dt>{@link #SIM_STATE_CARD_IO_ERROR}</dt>
 *       <dd>SIM card IO error</dd>
 *       <dt>{@link #SIM_STATE_CARD_RESTRICTED}</dt>
 *       <dd>SIM card is restricted</dd>
 *       <dt>{@link #SIM_STATE_PRESENT}</dt>
 *       <dd>SIM card is present</dd>
 *     </dl>
 *   </dd>
 * </dl>
 *
 * <p class="note">Requires the READ_PRIVILEGED_PHONE_STATE permission.
 *
 * <p class="note">The current state can also be queried using {@link #getSimCardState()}.
 *
 * <p class="note">This is a protected intent that can only be sent by the system.
 * @hide
 */

public static final java.lang.String ACTION_SIM_CARD_STATE_CHANGED = "android.telephony.action.SIM_CARD_STATE_CHANGED";

/**
 * Broadcast Action: Status of the SIM slots on the device has changed.
 *
 * <p class="note">Requires the READ_PRIVILEGED_PHONE_STATE permission.
 *
 * <p class="note">The status can be queried using
 * {@link #getUiccSlotsInfo()}
 *
 * <p class="note">This is a protected intent that can only be sent by the system.
 * @hide
 */

public static final java.lang.String ACTION_SIM_SLOT_STATUS_CHANGED = "android.telephony.action.SIM_SLOT_STATUS_CHANGED";

/**
 * Broadcast Action: The subscription carrier identity has changed.
 * This intent could be sent on the following events:
 * <ul>
 *   <li>Subscription absent. Carrier identity could change from a valid id to
 *   {@link TelephonyManager#UNKNOWN_CARRIER_ID}.</li>
 *   <li>Subscription loaded. Carrier identity could change from
 *   {@link TelephonyManager#UNKNOWN_CARRIER_ID} to a valid id.</li>
 *   <li>The subscription carrier is recognized after a remote update.</li>
 * </ul>
 * The intent will have the following extra values:
 * <ul>
 *   <li>{@link #EXTRA_CARRIER_ID} The up-to-date carrier id of the current subscription id.
 *   </li>
 *   <li>{@link #EXTRA_CARRIER_NAME} The up-to-date carrier name of the current subscription.
 *   </li>
 *   <li>{@link #EXTRA_SUBSCRIPTION_ID} The subscription id associated with the changed carrier
 *   identity.
 *   </li>
 * </ul>
 * <p class="note">This is a protected intent that can only be sent by the system.
 * @apiSince 28
 */

public static final java.lang.String ACTION_SUBSCRIPTION_CARRIER_IDENTITY_CHANGED = "android.telephony.action.SUBSCRIPTION_CARRIER_IDENTITY_CHANGED";

/**
 * Broadcast Action: The subscription specific carrier identity has changed.
 *
 * A specific carrier ID returns the fine-grained carrier ID of the current subscription.
 * It can represent the fact that a carrier may be in effect an aggregation of other carriers
 * (ie in an MVNO type scenario) where each of these specific carriers which are used to make
 * up the actual carrier service may have different carrier configurations.
 * A specific carrier ID could also be used, for example, in a scenario where a carrier requires
 * different carrier configuration for different service offering such as a prepaid plan.
 *
 * the specific carrier ID would be used for configuration purposes, but apps wishing to know
 * about the carrier itself should use the regular carrier ID returned by
 * {@link #getSimCarrierId()}.
 *
 * <p>Similar like {@link #ACTION_SUBSCRIPTION_CARRIER_IDENTITY_CHANGED}, this intent will be
 * sent on the event of {@link #ACTION_SUBSCRIPTION_CARRIER_IDENTITY_CHANGED} while its also
 * possible to be sent without {@link #ACTION_SUBSCRIPTION_CARRIER_IDENTITY_CHANGED} when
 * specific carrier ID changes while carrier ID remains the same.
 * e.g, the same subscription switches to different IMSI could potentially change its
 * specific carrier ID while carrier id remains the same.
 * @see #getSimSpecificCarrierId()
 * @see #getSimCarrierId()
 *
 * The intent will have the following extra values:
 * <ul>
 *   <li>{@link #EXTRA_SPECIFIC_CARRIER_ID} The up-to-date specific carrier id of the
 *   current subscription.
 *   </li>
 *   <li>{@link #EXTRA_SPECIFIC_CARRIER_NAME} The up-to-date name of the specific carrier id.
 *   </li>
 *   <li>{@link #EXTRA_SUBSCRIPTION_ID} The subscription id associated with the changed carrier
 *   identity.
 *   </li>
 * </ul>
 * <p class="note">This is a protected intent that can only be sent by the system.
 * @apiSince 29
 */

public static final java.lang.String ACTION_SUBSCRIPTION_SPECIFIC_CARRIER_IDENTITY_CHANGED = "android.telephony.action.SUBSCRIPTION_SPECIFIC_CARRIER_IDENTITY_CHANGED";

/**
 * UICC application type is CSIM
 * @apiSince 24
 */

public static final int APPTYPE_CSIM = 4; // 0x4

/**
 * UICC application type is ISIM
 * @apiSince 24
 */

public static final int APPTYPE_ISIM = 5; // 0x5

/**
 * UICC application type is RUIM
 * @apiSince 24
 */

public static final int APPTYPE_RUIM = 3; // 0x3

/**
 * UICC application type is SIM
 * @apiSince 24
 */

public static final int APPTYPE_SIM = 1; // 0x1

/**
 * UICC application type is USIM
 * @apiSince 24
 */

public static final int APPTYPE_USIM = 2; // 0x2

/**
 * Authentication type for UICC challenge is EAP AKA. See RFC 4187 for details.
 * @apiSince 24
 */

public static final int AUTHTYPE_EAP_AKA = 129; // 0x81

/**
 * Authentication type for UICC challenge is EAP SIM. See RFC 4186 for details.
 * @apiSince 24
 */

public static final int AUTHTYPE_EAP_SIM = 128; // 0x80

/**
 * Device call state: No activity.
 * @apiSince 1
 */

public static final int CALL_STATE_IDLE = 0; // 0x0

/**
 * Device call state: Off-hook. At least one call exists
 * that is dialing, active, or on hold, and no calls are ringing
 * or waiting.
 * @apiSince 1
 */

public static final int CALL_STATE_OFFHOOK = 2; // 0x2

/**
 * Device call state: Ringing. A new call arrived and is
 *  ringing or waiting. In the latter case, another call is
 *  already active.
 * @apiSince 1
 */

public static final int CALL_STATE_RINGING = 1; // 0x1

/** @hide */

public static final int CARRIER_PRIVILEGE_STATUS_ERROR_LOADING_RULES = -2; // 0xfffffffe

/** @hide */

public static final int CARRIER_PRIVILEGE_STATUS_HAS_ACCESS = 1; // 0x1

/** @hide */

public static final int CARRIER_PRIVILEGE_STATUS_NO_ACCESS = 0; // 0x0

/** @hide */

public static final int CARRIER_PRIVILEGE_STATUS_RULES_NOT_LOADED = -1; // 0xffffffff

/**
 * Value for {@link CarrierConfigManager#KEY_CDMA_ROAMING_MODE_INT} which permits roaming on
 * affiliated networks.
 * @apiSince 28
 */

public static final int CDMA_ROAMING_MODE_AFFILIATED = 1; // 0x1

/**
 * Value for {@link CarrierConfigManager#KEY_CDMA_ROAMING_MODE_INT} which permits roaming on
 * any network.
 * @apiSince 28
 */

public static final int CDMA_ROAMING_MODE_ANY = 2; // 0x2

/**
 * Value for {@link CarrierConfigManager#KEY_CDMA_ROAMING_MODE_INT} which only permits
 * connections on home networks.
 * @apiSince 28
 */

public static final int CDMA_ROAMING_MODE_HOME = 0; // 0x0

/**
 * Value for {@link CarrierConfigManager#KEY_CDMA_ROAMING_MODE_INT} which leaves the roaming
 * mode set to the radio default or to the user's preference if they've indicated one.
 * @apiSince 28
 */

public static final int CDMA_ROAMING_MODE_RADIO_DEFAULT = -1; // 0xffffffff

/**
 * Data connection is active, but physical link is down
 * @apiSince 4
 */

public static final int DATA_ACTIVITY_DORMANT = 4; // 0x4

/**
 * Data connection activity: Currently receiving IP PPP traffic.
 * @apiSince 1
 */

public static final int DATA_ACTIVITY_IN = 1; // 0x1

/** Data connection activity: Currently both sending and receiving
 *  IP PPP traffic.     * @apiSince 1
 */

public static final int DATA_ACTIVITY_INOUT = 3; // 0x3

/**
 * Data connection activity: No traffic.
 * @apiSince 1
 */

public static final int DATA_ACTIVITY_NONE = 0; // 0x0

/**
 * Data connection activity: Currently sending IP PPP traffic.
 * @apiSince 1
 */

public static final int DATA_ACTIVITY_OUT = 2; // 0x2

/**
 * Data connection state: Connected. IP traffic should be available.
 * @apiSince 1
 */

public static final int DATA_CONNECTED = 2; // 0x2

/**
 * Data connection state: Currently setting up a data connection.
 * @apiSince 1
 */

public static final int DATA_CONNECTING = 1; // 0x1

/**
 * Data connection state: Disconnected. IP traffic not available.
 * @apiSince 1
 */

public static final int DATA_DISCONNECTED = 0; // 0x0

/** Data connection state: Suspended. The connection is up, but IP
 * traffic is temporarily unavailable. For example, in a 2G network,
 * data activity may be suspended when a voice call arrives.     * @apiSince 1
 */

public static final int DATA_SUSPENDED = 3; // 0x3

/**
 * Data connection state: Unknown.  Used before we know the state.
 * @apiSince 29
 */

public static final int DATA_UNKNOWN = -1; // 0xffffffff

/**
 * A freeform string description of the Anomaly.
 *
 * This field is optional for all {@link ACTION_ANOMALY_REPORTED}s, as a guideline should not
 * exceed 80 characters, and should be as short as possible to convey the essence of the event.
 *
 * @see #ACTION_ANOMALY_REPORTED
 * @hide
 */

public static final java.lang.String EXTRA_ANOMALY_DESCRIPTION = "android.telephony.extra.ANOMALY_DESCRIPTION";

/**
 * An arbitrary ParcelUuid which should be consistent for each occurrence of a DebugEvent.
 *
 * This field must be included in all {@link ACTION_ANOMALY_REPORTED} events.
 *
 * @see #ACTION_ANOMALY_REPORTED
 * @hide
 */

public static final java.lang.String EXTRA_ANOMALY_ID = "android.telephony.extra.ANOMALY_ID";

/**
 * The intent to call voicemail.
 * @apiSince 26
 */

public static final java.lang.String EXTRA_CALL_VOICEMAIL_INTENT = "android.telephony.extra.CALL_VOICEMAIL_INTENT";

/**
 * An int extra used with {@link #ACTION_SUBSCRIPTION_CARRIER_IDENTITY_CHANGED} which indicates
 * the updated carrier id returned by {@link TelephonyManager#getSimCarrierId()}.
 * <p>Will be {@link TelephonyManager#UNKNOWN_CARRIER_ID} if the subscription is unavailable or
 * the carrier cannot be identified.
 * @apiSince 28
 */

public static final java.lang.String EXTRA_CARRIER_ID = "android.telephony.extra.CARRIER_ID";

/**
 * An string extra used with {@link #ACTION_SUBSCRIPTION_CARRIER_IDENTITY_CHANGED} which
 * indicates the updated carrier name of the current subscription.
 * @see TelephonyManager#getSimCarrierIdName()
 * <p>Carrier name is a user-facing name of the carrier id {@link #EXTRA_CARRIER_ID},
 * usually the brand name of the subsidiary (e.g. T-Mobile).
 * @apiSince 28
 */

public static final java.lang.String EXTRA_CARRIER_NAME = "android.telephony.extra.CARRIER_NAME";

/**
 * The boolean value indicating whether the voicemail settings activity launched by {@link
 * #ACTION_CONFIGURE_VOICEMAIL} should hide settings accessible through public API. This is
 * used by dialer implementations which provides their own voicemail settings UI, but still
 * needs to expose device specific voicemail settings to the user.
 *
 * @see #ACTION_CONFIGURE_VOICEMAIL
 * @see #METADATA_HIDE_VOICEMAIL_SETTINGS_MENU
 * @apiSince 26
 */

public static final java.lang.String EXTRA_HIDE_PUBLIC_SETTINGS = "android.telephony.extra.HIDE_PUBLIC_SETTINGS";

/**
 * Extra key used with the {@link #ACTION_PHONE_STATE_CHANGED} broadcast
 * for a String containing the incoming or outgoing phone number.
 * <p>
 * This extra is only populated for receivers of the {@link #ACTION_PHONE_STATE_CHANGED}
 * broadcast which have been granted the {@link android.Manifest.permission#READ_CALL_LOG} and
 * {@link android.Manifest.permission#READ_PHONE_STATE} permissions.
 * <p>
 * For incoming calls, the phone number is only guaranteed to be populated when the
 * {@link #EXTRA_STATE} changes from {@link #EXTRA_STATE_IDLE} to {@link #EXTRA_STATE_RINGING}.
 * If the incoming caller is from an unknown number, the extra will be populated with an empty
 * string.
 * For outgoing calls, the phone number is only guaranteed to be populated when the
 * {@link #EXTRA_STATE} changes from {@link #EXTRA_STATE_IDLE} to {@link #EXTRA_STATE_OFFHOOK}.
 * <p class="note">
 * Retrieve with
 * {@link android.content.Intent#getStringExtra(String)}.
 * <p>
 *
 * @deprecated Companion apps for wearable devices should use the {@link InCallService} API
 * to retrieve the phone number for calls instead.  Apps performing call screening should use
 * the {@link CallScreeningService} API instead.
 * @apiSince 3
 * @deprecatedSince 29
 */

@Deprecated public static final java.lang.String EXTRA_INCOMING_NUMBER = "incoming_number";

/**
 * Boolean value representing whether the {@link
 * TelephonyManager#ACTION_SHOW_VOICEMAIL_NOTIFICATION} is new or a refresh of an existing
 * notification. Notification refresh happens after reboot or connectivity changes. The user has
 * already been notified for the voicemail so it should not alert the user, and should not be
 * shown again if the user has dismissed it.
 * @apiSince 27
 */

public static final java.lang.String EXTRA_IS_REFRESH = "android.telephony.extra.IS_REFRESH";

/**
 * The intent to launch voicemail settings.
 * @apiSince 26
 */

public static final java.lang.String EXTRA_LAUNCH_VOICEMAIL_SETTINGS_INTENT = "android.telephony.extra.LAUNCH_VOICEMAIL_SETTINGS_INTENT";

/**
 * The extra used with an {@link #ACTION_NETWORK_COUNTRY_CHANGED} to specify the
 * the country code in ISO 3166 format.
 * <p class="note">
 * Retrieve with {@link android.content.Intent#getStringExtra(String)}.
 * @apiSince 29
 */

public static final java.lang.String EXTRA_NETWORK_COUNTRY = "android.telephony.extra.NETWORK_COUNTRY";

/**
 * The number of voice messages associated with the notification.
 * @apiSince 26
 */

public static final java.lang.String EXTRA_NOTIFICATION_COUNT = "android.telephony.extra.NOTIFICATION_COUNT";

/**
 * The extra used with an {@link #ACTION_CONFIGURE_VOICEMAIL} and
 * {@link #ACTION_SHOW_VOICEMAIL_NOTIFICATION} {@code Intent} to specify the
 * {@link PhoneAccountHandle} the configuration or notification is for.
 * <p class="note">
 * Retrieve with {@link android.content.Intent#getParcelableExtra(String)}.
 * @apiSince 26
 */

public static final java.lang.String EXTRA_PHONE_ACCOUNT_HANDLE = "android.telephony.extra.PHONE_ACCOUNT_HANDLE";

/**
 * Extra included in {@link #ACTION_SIM_CARD_STATE_CHANGED} and
 * {@link #ACTION_SIM_APPLICATION_STATE_CHANGED} to indicate the card/application state.
 *
 * @hide
 */

public static final java.lang.String EXTRA_SIM_STATE = "android.telephony.extra.SIM_STATE";

/**
 * An int extra used with {@link #ACTION_SUBSCRIPTION_SPECIFIC_CARRIER_IDENTITY_CHANGED} which
 * indicates the updated specific carrier id returned by
 * {@link TelephonyManager#getSimSpecificCarrierId()}. Note, its possible specific carrier id
 * changes while {@link #ACTION_SUBSCRIPTION_CARRIER_IDENTITY_CHANGED} remains the same
 * e.g, when subscription switch to different IMSIs.
 * <p>Will be {@link TelephonyManager#UNKNOWN_CARRIER_ID} if the subscription is unavailable or
 * the carrier cannot be identified.
 * @apiSince 29
 */

public static final java.lang.String EXTRA_SPECIFIC_CARRIER_ID = "android.telephony.extra.SPECIFIC_CARRIER_ID";

/**
 * An string extra used with {@link #ACTION_SUBSCRIPTION_SPECIFIC_CARRIER_IDENTITY_CHANGED}
 * which indicates the updated specific carrier name returned by
 * {@link TelephonyManager#getSimSpecificCarrierIdName()}.
 * <p>it's a user-facing name of the specific carrier id {@link #EXTRA_SPECIFIC_CARRIER_ID}
 * e.g, Tracfone-AT&T
 * @apiSince 29
 */

public static final java.lang.String EXTRA_SPECIFIC_CARRIER_NAME = "android.telephony.extra.SPECIFIC_CARRIER_NAME";

/**
 * The lookup key used with the {@link #ACTION_PHONE_STATE_CHANGED} broadcast
 * for a String containing the new call state.
 *
 * <p class="note">
 * Retrieve with
 * {@link android.content.Intent#getStringExtra(String)}.
 *
 * @see #EXTRA_STATE_IDLE
 * @see #EXTRA_STATE_RINGING
 * @see #EXTRA_STATE_OFFHOOK
 * @apiSince 3
 */

public static final java.lang.String EXTRA_STATE = "state";

/**
 * Value used with {@link #EXTRA_STATE} corresponding to
 * {@link #CALL_STATE_IDLE}.
 * @apiSince 3
 */

public static final java.lang.String EXTRA_STATE_IDLE;
static { EXTRA_STATE_IDLE = null; }

/**
 * Value used with {@link #EXTRA_STATE} corresponding to
 * {@link #CALL_STATE_OFFHOOK}.
 * @apiSince 3
 */

public static final java.lang.String EXTRA_STATE_OFFHOOK;
static { EXTRA_STATE_OFFHOOK = null; }

/**
 * Value used with {@link #EXTRA_STATE} corresponding to
 * {@link #CALL_STATE_RINGING}.
 * @apiSince 3
 */

public static final java.lang.String EXTRA_STATE_RINGING;
static { EXTRA_STATE_RINGING = null; }

/**
 * An int extra used with {@link #ACTION_SUBSCRIPTION_CARRIER_IDENTITY_CHANGED} to indicate the
 * subscription which has changed; or in general whenever a subscription ID needs specified.
 * @apiSince 28
 */

public static final java.lang.String EXTRA_SUBSCRIPTION_ID = "android.telephony.extra.SUBSCRIPTION_ID";

/**
 * Key in bundle returned by {@link #getVisualVoicemailPackageName()}, indicating whether visual
 * voicemail was enabled or disabled by the user. If the user never explicitly changed this
 * setting, this key will not exist.
 *
 * @see #getVisualVoicemailSettings()
 * @hide
 */

public static final java.lang.String EXTRA_VISUAL_VOICEMAIL_ENABLED_BY_USER_BOOL = "android.telephony.extra.VISUAL_VOICEMAIL_ENABLED_BY_USER_BOOL";

/**
 * The voicemail number.
 * @apiSince 26
 */

public static final java.lang.String EXTRA_VOICEMAIL_NUMBER = "android.telephony.extra.VOICEMAIL_NUMBER";

/**
 * Key in bundle returned by {@link #getVisualVoicemailPackageName()}, indicating the voicemail
 * access PIN scrambled during the auto provisioning process. The user is expected to reset
 * their PIN if this value is not {@code null}.
 *
 * @see #getVisualVoicemailSettings()
 * @hide
 */

public static final java.lang.String EXTRA_VOICEMAIL_SCRAMBLED_PIN_STRING = "android.telephony.extra.VOICEMAIL_SCRAMBLED_PIN_STRING";

/**
 * A boolean meta-data value indicating whether the voicemail settings should be hidden in the
 * call settings page launched by
 * {@link android.telecom.TelecomManager#ACTION_SHOW_CALL_SETTINGS}.
 * Dialer implementations (see {@link android.telecom.TelecomManager#getDefaultDialerPackage()})
 * which would also like to manage voicemail settings should set this meta-data to {@code true}
 * in the manifest registration of their application.
 *
 * @see android.telecom.TelecomManager#ACTION_SHOW_CALL_SETTINGS
 * @see #ACTION_CONFIGURE_VOICEMAIL
 * @see #EXTRA_HIDE_PUBLIC_SETTINGS
 * @apiSince 26
 */

public static final java.lang.String METADATA_HIDE_VOICEMAIL_SETTINGS_MENU = "android.telephony.HIDE_VOICEMAIL_SETTINGS_MENU";

/**
 * The usage of multiple SIM cards at the same time to register on the network (e.g. Dual
 * Standby or Dual Active) is supported.
 * @apiSince 29
 */

public static final int MULTISIM_ALLOWED = 0; // 0x0

/**
 * The usage of multiple SIM cards at the same time to register on the network (e.g. Dual
 * Standby or Dual Active) is supported by the hardware, but restricted by the carrier.
 * @apiSince 29
 */

public static final int MULTISIM_NOT_SUPPORTED_BY_CARRIER = 2; // 0x2

/**
 * The usage of multiple SIM cards at the same time to register on the network (e.g. Dual
 * Standby or Dual Active) is not supported by the hardware.
 * @apiSince 29
 */

public static final int MULTISIM_NOT_SUPPORTED_BY_HARDWARE = 1; // 0x1

/**
 * Current network is 1xRTT
 * @apiSince 4
 */

public static final int NETWORK_TYPE_1xRTT = 7; // 0x7

/**
 * network type bitmask indicating the support of radio tech 1xRTT.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_1xRTT = 64L; // 0x40L

/**
 * network type bitmask indicating the support of radio tech CDMA(IS95A/IS95B).
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_CDMA = 8L; // 0x8L

/**
 * network type bitmask indicating the support of radio tech EDGE.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_EDGE = 2L; // 0x2L

/**
 * network type bitmask indicating the support of radio tech EHRPD.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_EHRPD = 8192L; // 0x2000L

/**
 * network type bitmask indicating the support of radio tech EVDO 0.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_EVDO_0 = 16L; // 0x10L

/**
 * network type bitmask indicating the support of radio tech EVDO A.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_EVDO_A = 32L; // 0x20L

/**
 * network type bitmask indicating the support of radio tech EVDO B.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_EVDO_B = 2048L; // 0x800L

/**
 * network type bitmask indicating the support of radio tech GPRS.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_GPRS = 1L; // 0x1L

/**
 * network type bitmask indicating the support of radio tech GSM.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_GSM = 32768L; // 0x8000L

/**
 * network type bitmask indicating the support of radio tech HSDPA.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_HSDPA = 128L; // 0x80L

/**
 * network type bitmask indicating the support of radio tech HSPA.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_HSPA = 512L; // 0x200L

/**
 * network type bitmask indicating the support of radio tech HSPAP.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_HSPAP = 16384L; // 0x4000L

/**
 * network type bitmask indicating the support of radio tech HSUPA.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_HSUPA = 256L; // 0x100L

/**
 * network type bitmask indicating the support of radio tech IWLAN.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_IWLAN = 131072L; // 0x20000L

/**
 * network type bitmask indicating the support of radio tech LTE.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_LTE = 4096L; // 0x1000L

/**
 * network type bitmask indicating the support of radio tech LTE CA (carrier aggregation).
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_LTE_CA = 262144L; // 0x40000L

/**
 * network type bitmask indicating the support of radio tech NR(New Radio) 5G.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_NR = 524288L; // 0x80000L

/**
 * network type bitmask indicating the support of radio tech TD_SCDMA.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_TD_SCDMA = 65536L; // 0x10000L

/**
 * network type bitmask indicating the support of radio tech UMTS.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_UMTS = 4L; // 0x4L

/**
 * network type bitmask unknown.
 * @hide
 */

public static final long NETWORK_TYPE_BITMASK_UNKNOWN = 0L; // 0x0L

/**
 * Current network is CDMA: Either IS95A or IS95B
 * @apiSince 4
 */

public static final int NETWORK_TYPE_CDMA = 4; // 0x4

/**
 * Current network is EDGE
 * @apiSince 1
 */

public static final int NETWORK_TYPE_EDGE = 2; // 0x2

/**
 * Current network is eHRPD
 * @apiSince 11
 */

public static final int NETWORK_TYPE_EHRPD = 14; // 0xe

/**
 * Current network is EVDO revision 0
 * @apiSince 4
 */

public static final int NETWORK_TYPE_EVDO_0 = 5; // 0x5

/**
 * Current network is EVDO revision A
 * @apiSince 4
 */

public static final int NETWORK_TYPE_EVDO_A = 6; // 0x6

/**
 * Current network is EVDO revision B
 * @apiSince 9
 */

public static final int NETWORK_TYPE_EVDO_B = 12; // 0xc

/**
 * Current network is GPRS
 * @apiSince 1
 */

public static final int NETWORK_TYPE_GPRS = 1; // 0x1

/**
 * Current network is GSM
 * @apiSince 25
 */

public static final int NETWORK_TYPE_GSM = 16; // 0x10

/**
 * Current network is HSDPA
 * @apiSince 5
 */

public static final int NETWORK_TYPE_HSDPA = 8; // 0x8

/**
 * Current network is HSPA
 * @apiSince 5
 */

public static final int NETWORK_TYPE_HSPA = 10; // 0xa

/**
 * Current network is HSPA+
 * @apiSince 13
 */

public static final int NETWORK_TYPE_HSPAP = 15; // 0xf

/**
 * Current network is HSUPA
 * @apiSince 5
 */

public static final int NETWORK_TYPE_HSUPA = 9; // 0x9

/**
 * Current network is iDen
 * @apiSince 8
 */

public static final int NETWORK_TYPE_IDEN = 11; // 0xb

/**
 * Current network is IWLAN
 * @apiSince 25
 */

public static final int NETWORK_TYPE_IWLAN = 18; // 0x12

/**
 * Current network is LTE
 * @apiSince 11
 */

public static final int NETWORK_TYPE_LTE = 13; // 0xd

/**
 * Current network is NR(New Radio) 5G.
 * @apiSince 29
 */

public static final int NETWORK_TYPE_NR = 20; // 0x14

/**
 * Current network is TD_SCDMA
 * @apiSince 25
 */

public static final int NETWORK_TYPE_TD_SCDMA = 17; // 0x11

/**
 * Current network is UMTS
 * @apiSince 1
 */

public static final int NETWORK_TYPE_UMTS = 3; // 0x3

/**
 * Network type is unknown
 * @apiSince 1
 */

public static final int NETWORK_TYPE_UNKNOWN = 0; // 0x0

/**
 * Phone radio is CDMA.
 * @apiSince 4
 */

public static final int PHONE_TYPE_CDMA = 2; // 0x2

/**
 * Phone radio is GSM.
 * @apiSince 1
 */

public static final int PHONE_TYPE_GSM = 1; // 0x1

/**
 * No phone radio.
 * @apiSince 1
 */

public static final int PHONE_TYPE_NONE = 0; // 0x0

/**
 * Phone is via SIP.
 * @apiSince 11
 */

public static final int PHONE_TYPE_SIP = 3; // 0x3

/**
 * Radio explicitly powered off (e.g, airplane mode).
 * @hide
 */

public static final int RADIO_POWER_OFF = 0; // 0x0

/**
 * Radio power is on.
 * @hide
 */

public static final int RADIO_POWER_ON = 1; // 0x1

/**
 * Radio power unavailable (eg, modem resetting or not booted).
 * @hide
 */

public static final int RADIO_POWER_UNAVAILABLE = 2; // 0x2

/**
 * The setting of carrier restrictions failed.
 * @hide
 */

public static final int SET_CARRIER_RESTRICTION_ERROR = 2; // 0x2

/**
 * The carrier restrictions were not set due to lack of support in the modem. This can happen
 * if the modem does not support setting the carrier restrictions or if the configuration
 * passed in the {@code setCarrierRestrictionRules} is not supported by the modem.
 * @hide
 */

public static final int SET_CARRIER_RESTRICTION_NOT_SUPPORTED = 1; // 0x1

/**
 * The carrier restrictions were successfully set.
 * @hide
 */

public static final int SET_CARRIER_RESTRICTION_SUCCESS = 0; // 0x0

/**
 * The subscription is not valid. It must be an active opportunistic subscription.
 * @apiSince 29
 */

public static final int SET_OPPORTUNISTIC_SUB_INACTIVE_SUBSCRIPTION = 2; // 0x2

/**
 * No error. Operation succeeded.
 * @apiSince 29
 */

public static final int SET_OPPORTUNISTIC_SUB_SUCCESS = 0; // 0x0

/**
 * Validation failed when trying to switch to preferred subscription.
 * @apiSince 29
 */

public static final int SET_OPPORTUNISTIC_SUB_VALIDATION_FAILED = 1; // 0x1

/**
 * Indicate SIM has been successfully activated with full service
 * @hide
 */

public static final int SIM_ACTIVATION_STATE_ACTIVATED = 2; // 0x2

/**
 * indicate SIM is under activation procedure now.
 * intermediate state followed by another state update with activation procedure result:
 * @see #SIM_ACTIVATION_STATE_ACTIVATED
 * @see #SIM_ACTIVATION_STATE_DEACTIVATED
 * @see #SIM_ACTIVATION_STATE_RESTRICTED
 * @hide
 */

public static final int SIM_ACTIVATION_STATE_ACTIVATING = 1; // 0x1

/**
 * Indicate SIM has been deactivated by the carrier so that service is not available
 * and requires activation service to enable services.
 * Carrier apps could be signalled to set activation state to deactivated if detected
 * deactivated sim state and set it back to activated after successfully run activation service.
 * @hide
 */

public static final int SIM_ACTIVATION_STATE_DEACTIVATED = 3; // 0x3

/**
 * Restricted state indicate SIM has been activated but service are restricted.
 * note this is currently available for data activation state. For example out of byte sim.
 * @hide
 */

public static final int SIM_ACTIVATION_STATE_RESTRICTED = 4; // 0x4

/**
 * Initial SIM activation state, unknown. Not set by any carrier apps.
 * @hide
 */

public static final int SIM_ACTIVATION_STATE_UNKNOWN = 0; // 0x0

/**
 * SIM card state: no SIM card is available in the device
 * @apiSince 1
 */

public static final int SIM_STATE_ABSENT = 1; // 0x1

/**
 * SIM card state: SIM Card Error, present but faulty
 * @apiSince 26
 */

public static final int SIM_STATE_CARD_IO_ERROR = 8; // 0x8

/** SIM card state: SIM Card restricted, present but not usable due to
 * carrier restrictions.
 * @apiSince 26
 */

public static final int SIM_STATE_CARD_RESTRICTED = 9; // 0x9

/**
 * SIM card state: Loaded: SIM card applications have been loaded
 * @hide
 */

public static final int SIM_STATE_LOADED = 10; // 0xa

/**
 * SIM card state: Locked: requires a network PIN to unlock
 * @apiSince 1
 */

public static final int SIM_STATE_NETWORK_LOCKED = 4; // 0x4

/**
 * SIM card state: SIM Card is NOT READY
 * @apiSince 26
 */

public static final int SIM_STATE_NOT_READY = 6; // 0x6

/**
 * SIM card state: SIM Card Error, permanently disabled
 * @apiSince 26
 */

public static final int SIM_STATE_PERM_DISABLED = 7; // 0x7

/**
 * SIM card state: Locked: requires the user's SIM PIN to unlock
 * @apiSince 1
 */

public static final int SIM_STATE_PIN_REQUIRED = 2; // 0x2

/**
 * SIM card state: SIM Card is present
 * @hide
 */

public static final int SIM_STATE_PRESENT = 11; // 0xb

/**
 * SIM card state: Locked: requires the user's SIM PUK to unlock
 * @apiSince 1
 */

public static final int SIM_STATE_PUK_REQUIRED = 3; // 0x3

/**
 * SIM card state: Ready
 * @apiSince 1
 */

public static final int SIM_STATE_READY = 5; // 0x5

/**
 * SIM card state: Unknown. Signifies that the SIM is in transition
 * between states. For example, when the user inputs the SIM pin
 * under PIN_REQUIRED state, a query for sim status returns
 * this state before turning to SIM_STATE_READY.
 *
 * These are the ordinal value of IccCardConstants.State.
 * @apiSince 1
 */

public static final int SIM_STATE_UNKNOWN = 0; // 0x0

/**
 * Ongoing Single Radio Voice Call Continuity (SRVCC) handover has been canceled.
 * See TS 23.216 for more information.
 * @hide
 */

public static final int SRVCC_STATE_HANDOVER_CANCELED = 3; // 0x3

/**
 * Ongoing Single Radio Voice Call Continuity (SRVCC) handover has successfully completed.
 * See TS 23.216 for more information.
 * @hide
 */

public static final int SRVCC_STATE_HANDOVER_COMPLETED = 1; // 0x1

/**
 * Ongoing Single Radio Voice Call Continuity (SRVCC) handover has failed.
 * See TS 23.216 for more information.
 * @hide
 */

public static final int SRVCC_STATE_HANDOVER_FAILED = 2; // 0x2

/**
 * No Single Radio Voice Call Continuity (SRVCC) handover is active.
 * See TS 23.216 for more information.
 * @hide
 */

public static final int SRVCC_STATE_HANDOVER_NONE = -1; // 0xffffffff

/**
 * Single Radio Voice Call Continuity (SRVCC) handover has been started on the network.
 * See TS 23.216 for more information.
 * @hide
 */

public static final int SRVCC_STATE_HANDOVER_STARTED = 0; // 0x0

/**
 * A UICC card identifier used before the UICC card is loaded. See
 * {@link #getCardIdForDefaultEuicc()} and {@link UiccCardInfo#getCardId()}.
 * <p>
 * Note that once the UICC card is loaded, the card ID may become {@link #UNSUPPORTED_CARD_ID}.
 * @apiSince 29
 */

public static final int UNINITIALIZED_CARD_ID = -2; // 0xfffffffe

/**
 * An unknown carrier id. It could either be subscription unavailable or the subscription
 * carrier cannot be recognized. Unrecognized carriers here means
 * {@link #getSimOperator() MCC+MNC} cannot be identified.
 * @apiSince 28
 */

public static final int UNKNOWN_CARRIER_ID = -1; // 0xffffffff

/**
 * A UICC card identifier used if the device does not support the operation.
 * For example, {@link #getCardIdForDefaultEuicc()} returns this value if the device has no
 * eUICC, or the eUICC cannot be read.
 * @apiSince 29
 */

public static final int UNSUPPORTED_CARD_ID = -1; // 0xffffffff

/**
 * The request is aborted.
 * @apiSince 29
 */

public static final int UPDATE_AVAILABLE_NETWORKS_ABORTED = 2; // 0x2

/**
 * The parameter passed in is invalid.
 * @apiSince 29
 */

public static final int UPDATE_AVAILABLE_NETWORKS_INVALID_ARGUMENTS = 3; // 0x3

/**
 * No carrier privilege.
 * @apiSince 29
 */

public static final int UPDATE_AVAILABLE_NETWORKS_NO_CARRIER_PRIVILEGE = 4; // 0x4

/**
 * No error. Operation succeeded.
 * @apiSince 29
 */

public static final int UPDATE_AVAILABLE_NETWORKS_SUCCESS = 0; // 0x0

/**
 * There is a unknown failure happened.
 * @apiSince 29
 */

public static final int UPDATE_AVAILABLE_NETWORKS_UNKNOWN_FAILURE = 1; // 0x1

/**
 * Failure code returned when a USSD request has failed to execute because the Telephony
 * service is unavailable.
 * <p>
 * Returned via {@link TelephonyManager.UssdResponseCallback#onReceiveUssdResponseFailed(
 * TelephonyManager, String, int)}.
 * @apiSince 26
 */

public static final int USSD_ERROR_SERVICE_UNAVAIL = -2; // 0xfffffffe

/**
 * Failed code returned when the mobile network has failed to complete a USSD request.
 * <p>
 * Returned via {@link TelephonyManager.UssdResponseCallback#onReceiveUssdResponseFailed(
 * TelephonyManager, String, int)}.
 * @apiSince 26
 */

public static final int USSD_RETURN_FAILURE = -1; // 0xffffffff

/**
 * A flavor of OMTP protocol with a different mobile originated (MO) format
 * @apiSince 23
 */

public static final java.lang.String VVM_TYPE_CVVM = "vvm_type_cvvm";

/**
 * The OMTP protocol.
 * @apiSince 23
 */

public static final java.lang.String VVM_TYPE_OMTP = "vvm_type_omtp";
/**
 * Callback for providing asynchronous {@link CellInfo} on request
 * @apiSince 29
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class CellInfoCallback {

public CellInfoCallback() { throw new RuntimeException("Stub!"); }

/**
 * Success response to
 * {@link android.telephony.TelephonyManager#requestCellInfoUpdate requestCellInfoUpdate()}.
 *
 * Invoked when there is a response to
 * {@link android.telephony.TelephonyManager#requestCellInfoUpdate requestCellInfoUpdate()}
 * to provide a list of {@link CellInfo}. If no {@link CellInfo} is available then an empty
 * list will be provided. If an error occurs, null will be provided unless the onError
 * callback is overridden.
 *
 * @param cellInfo a list of {@link CellInfo}, an empty list, or null.
 *
 * {@see android.telephony.TelephonyManager#getAllCellInfo getAllCellInfo()}
 
 * This value must never be {@code null}.
 * @apiSince 29
 */

public abstract void onCellInfo(@android.annotation.NonNull java.util.List<android.telephony.CellInfo> cellInfo);

/**
 * Error response to
 * {@link android.telephony.TelephonyManager#requestCellInfoUpdate requestCellInfoUpdate()}.
 *
 * Invoked when an error condition prevents updated {@link CellInfo} from being fetched
 * and returned from the modem. Callers of requestCellInfoUpdate() should override this
 * function to receive detailed status information in the event of an error. By default,
 * this function will invoke onCellInfo() with null.
 *
 * @param errorCode an error code indicating the type of failure.
 * Value is {@link android.telephony.TelephonyManager.CellInfoCallback#ERROR_TIMEOUT}, or {@link android.telephony.TelephonyManager.CellInfoCallback#ERROR_MODEM_ERROR}
 * @param detail a Throwable object with additional detail regarding the failure if
 *     available, otherwise null.
 
 * This value may be {@code null}.
 * @apiSince 29
 */

public void onError(int errorCode, @android.annotation.Nullable java.lang.Throwable detail) { throw new RuntimeException("Stub!"); }

/**
 * The modem returned a failure.
 * @apiSince 29
 */

public static final int ERROR_MODEM_ERROR = 2; // 0x2

/**
 * The system timed out waiting for a response from the Radio.
 * @apiSince 29
 */

public static final int ERROR_TIMEOUT = 1; // 0x1
}

/**
 * Used to notify callers of
 * {@link TelephonyManager#sendUssdRequest(String, UssdResponseCallback, Handler)} when the
 * network either successfully executes a USSD request, or if there was a failure while
 * executing the request.
 * <p>
 * {@link #onReceiveUssdResponse(TelephonyManager, String, CharSequence)} will be called if the
 * USSD request has succeeded.
 * {@link #onReceiveUssdResponseFailed(TelephonyManager, String, int)} will be called if the
 * USSD request has failed.
 * @apiSince 26
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class UssdResponseCallback {

public UssdResponseCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called when a USSD request has succeeded.  The {@code response} contains the USSD
 * response received from the network.  The calling app can choose to either display the
 * response to the user or perform some operation based on the response.
 * <p>
 * USSD responses are unstructured text and their content is determined by the mobile network
 * operator.
 *
 * @param telephonyManager the TelephonyManager the callback is registered to.
 * @param request the USSD request sent to the mobile network.
 * @param response the response to the USSD request provided by the mobile network.
 *        * @apiSince 26
 */

public void onReceiveUssdResponse(android.telephony.TelephonyManager telephonyManager, java.lang.String request, java.lang.CharSequence response) { throw new RuntimeException("Stub!"); }

/**
 * Called when a USSD request has failed to complete.
 *
 * @param telephonyManager the TelephonyManager the callback is registered to.
 * @param request the USSD request sent to the mobile network.
 * @param failureCode failure code indicating why the request failed.  Will be either
 *        {@link TelephonyManager#USSD_RETURN_FAILURE} or
 *        {@link TelephonyManager#USSD_ERROR_SERVICE_UNAVAIL}.
 *        * @apiSince 26
 */

public void onReceiveUssdResponseFailed(android.telephony.TelephonyManager telephonyManager, java.lang.String request, int failureCode) { throw new RuntimeException("Stub!"); }
}

}

