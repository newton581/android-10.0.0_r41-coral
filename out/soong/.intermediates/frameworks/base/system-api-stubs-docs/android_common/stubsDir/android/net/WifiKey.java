/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.net;


/**
 * Information identifying a Wi-Fi network.
 * @see NetworkKey
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class WifiKey implements android.os.Parcelable {

/**
 * Construct a new {@link WifiKey} for the given Wi-Fi SSID/BSSID pair.
 *
 * @param ssid the service set identifier (SSID) of an 802.11 network. If the SSID can be
 *         decoded as UTF-8, it should be surrounded by double quotation marks. Otherwise,
 *         it should be a string of hex digits starting with 0x.
 * @param bssid the basic service set identifier (BSSID) of this network's access point.
 *         This should be in the form of a six-byte MAC address: {@code XX:XX:XX:XX:XX:XX},
 *         where each X is a hexadecimal digit.
 * @throws IllegalArgumentException if either the SSID or BSSID is invalid.
 * @apiSince REL
 */

public WifiKey(java.lang.String ssid, java.lang.String bssid) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.net.WifiKey> CREATOR;
static { CREATOR = null; }

/**
 * The basic service set identifier (BSSID) of an access point for this network. This will
 * be in the form of a six-byte MAC address: {@code XX:XX:XX:XX:XX:XX}, where each X is a
 * hexadecimal digit.
 * @apiSince REL
 */

public final java.lang.String bssid;
{ bssid = null; }

/**
 * The service set identifier (SSID) of an 802.11 network. If the SSID can be decoded as
 * UTF-8, it will be surrounded by double quotation marks. Otherwise, it will be a string of
 * hex digits starting with 0x.
 * @apiSince REL
 */

public final java.lang.String ssid;
{ ssid = null; }
}

