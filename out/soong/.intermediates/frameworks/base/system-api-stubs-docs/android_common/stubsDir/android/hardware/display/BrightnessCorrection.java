/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.display;

import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;

/**
 * BrightnessCorrection encapsulates a correction to the brightness, without comitting to the
 * actual correction scheme.
 * It is used by the BrightnessConfiguration, which maps context (e.g. the foreground app's package
 * name and category) to corrections that need to be applied to the brightness within that context.
 * Corrections are currently done by the app that has the top activity of the focused stack, either
 * by its package name, or (if its package name is not mapped to any correction) by its category.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class BrightnessCorrection implements android.os.Parcelable {

BrightnessCorrection() { throw new RuntimeException("Stub!"); }

/**
 * Creates a BrightnessCorrection that given {@code brightness}, corrects it to be
 * {@code exp(scale * ln(brightness) + translate)}.
 *
 * @param scale
 *      How much to scale the log (base e) brightness.
 * @param translate
 *      How much to translate the log (base e) brightness.
 *
 * @return A BrightnessCorrection that given {@code brightness}, corrects it to be
 * {@code exp(scale * ln(brightness) + translate)}.
 *
 * This value will never be {@code null}.
 * @throws IllegalArgumentException
 *      - scale or translate are NaN.
 * @apiSince REL
 */

@android.annotation.NonNull
public static android.hardware.display.BrightnessCorrection createScaleAndTranslateLog(float scale, float translate) { throw new RuntimeException("Stub!"); }

/**
 * Applies the brightness correction to a given brightness.
 *
 * @param brightness
 *      The brightness.
 *
 * Value is 0.0 or greater
 * @return The corrected brightness.
 
 * Value is 0.0 or greater
 * @apiSince REL
 */

public float apply(float brightness) { throw new RuntimeException("Stub!"); }

/**
 * Returns a string representation.
 *
 * @return A string representation.
 * @apiSince REL
 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.display.BrightnessCorrection> CREATOR;
static { CREATOR = null; }
}

