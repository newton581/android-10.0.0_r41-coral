/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.backup;


/**
 * Descriptive information about a set of backed-up app data available for restore.
 * Used by IRestoreSession clients.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class RestoreSet implements android.os.Parcelable {

/** @apiSince REL */

public RestoreSet() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public RestoreSet(java.lang.String _name, java.lang.String _dev, long _token) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.backup.RestoreSet> CREATOR;
static { CREATOR = null; }

/**
 * Identifier of the device whose data this is.  This will be as unique as
 * is practically possible; for example, it might be an IMEI.
 * @apiSince REL
 */

public java.lang.String device;

/**
 * Name of this restore set.  May be user generated, may simply be the name
 * of the handset model, e.g. "T-Mobile G1".
 * @apiSince REL
 */

public java.lang.String name;

/**
 * Token that identifies this backup set unambiguously to the backup/restore
 * transport.  This is guaranteed to be valid for the duration of a restore
 * session, but is meaningless once the session has ended.
 * @apiSince REL
 */

public long token;
}

