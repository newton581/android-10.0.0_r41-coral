
package android.os;


/**
 * Describes the source of some work that may be done by someone else.
 * Currently the public representation of what a work source is is not
 * defined; this is an opaque container.
 * @apiSince 9
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class WorkSource implements android.os.Parcelable {

/**
 * Create an empty work source.
 * @apiSince 9
 */

public WorkSource() { throw new RuntimeException("Stub!"); }

/**
 * Create a new WorkSource that is a copy of an existing one.
 * If <var>orig</var> is null, an empty WorkSource is created.
 * @apiSince 9
 */

public WorkSource(android.os.WorkSource orig) { throw new RuntimeException("Stub!"); }

/**
 * Clear this WorkSource to be empty.
 * @apiSince 9
 */

public void clear() { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * Compare this WorkSource with another.
 * @param other The WorkSource to compare against.
 * @return If there is a difference, true is returned.
 * @apiSince 9
 */

public boolean diff(android.os.WorkSource other) { throw new RuntimeException("Stub!"); }

/**
 * Replace the current contents of this work source with the given
 * work source.  If {@code other} is null, the current work source
 * will be made empty.
 * @apiSince 9
 */

public void set(android.os.WorkSource other) { throw new RuntimeException("Stub!"); }

/**
 * Merge the contents of <var>other</var> WorkSource in to this one.
 *
 * @param other The other WorkSource whose contents are to be merged.
 * @return Returns true if any new sources were added.
 * @apiSince 9
 */

public boolean add(android.os.WorkSource other) { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public boolean remove(android.os.WorkSource other) { throw new RuntimeException("Stub!"); }

/**
 * Create a new {@code WorkChain} associated with this WorkSource and return it.
 *
 * @hide
 */

public android.os.WorkSource.WorkChain createWorkChain() { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince 9 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.WorkSource> CREATOR;
static { CREATOR = null; }
/**
 * Represents an attribution chain for an item of work being performed. An attribution chain is
 * an indexed list of {code (uid, tag)} nodes. The node at {@code index == 0} is the initiator
 * of the work, and the node at the highest index performs the work. Nodes at other indices
 * are intermediaries that facilitate the work. Examples :
 *
 * (1) Work being performed by uid=2456 (no chaining):
 * <pre>
 * WorkChain {
 *   mUids = { 2456 }
 *   mTags = { null }
 *   mSize = 1;
 * }
 * </pre>
 *
 * (2) Work being performed by uid=2456 (from component "c1") on behalf of uid=5678:
 *
 * <pre>
 * WorkChain {
 *   mUids = { 5678, 2456 }
 *   mTags = { null, "c1" }
 *   mSize = 1
 * }
 * </pre>
 *
 * Attribution chains are mutable, though the only operation that can be performed on them
 * is the addition of a new node at the end of the attribution chain to represent
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class WorkChain implements android.os.Parcelable {

/** @apiSince REL */

public WorkChain() { throw new RuntimeException("Stub!"); }

/**
 * Append a node whose uid is {@code uid} and whose optional tag is {@code tag} to this
 * {@code WorkChain}.
 
 * @param tag This value may be {@code null}.
 * @apiSince REL
 */

public android.os.WorkSource.WorkChain addNode(int uid, @android.annotation.Nullable java.lang.String tag) { throw new RuntimeException("Stub!"); }

/**
 * Return the UID to which this WorkChain should be attributed to, i.e, the UID that
 * initiated the work and not the UID performing it. Returns -1 if the chain is empty.
 * @apiSince REL
 */

public int getAttributionUid() { throw new RuntimeException("Stub!"); }

/**
 * Return the tag associated with the attribution UID. See (@link #getAttributionUid}.
 * Returns null if the chain is empty.
 * @apiSince REL
 */

public java.lang.String getAttributionTag() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.WorkSource.WorkChain> CREATOR;
static { CREATOR = null; }
}

}

