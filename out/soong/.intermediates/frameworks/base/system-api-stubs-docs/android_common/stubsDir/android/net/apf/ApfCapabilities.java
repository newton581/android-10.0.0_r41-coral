/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.apf;


/**
 * APF program support capabilities. APF stands for Android Packet Filtering and it is a flexible
 * way to drop unwanted network packets to save power.
 *
 * See documentation at hardware/google/apf/apf.h
 *
 * This class is immutable.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ApfCapabilities implements android.os.Parcelable {

/** @apiSince REL */

public ApfCapabilities(int apfVersionSupported, int maximumApfProgramSize, int apfPacketFormat) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * Determines whether the APF interpreter advertises support for the data buffer access opcodes
 * LDDW (LoaD Data Word) and STDW (STore Data Word). Full LDDW (LoaD Data Word) and
 * STDW (STore Data Word) support is present from APFv4 on.
 *
 * @return {@code true} if the IWifiStaIface#readApfPacketFilterData is supported.
 * @apiSince REL
 */

public boolean hasDataAccess() { throw new RuntimeException("Stub!"); }

/**
 * @return Whether the APF Filter in the device should filter out IEEE 802.3 Frames.
 * @apiSince REL
 */

public static boolean getApfDrop8023Frames() { throw new RuntimeException("Stub!"); }

/**
 * @return An array of blacklisted EtherType, packets with EtherTypes within it will be dropped.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static int[] getApfEtherTypeBlackList() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final android.os.Parcelable.Creator<android.net.apf.ApfCapabilities> CREATOR;
static { CREATOR = null; }

/**
 * Format of packets passed to APF filter. Should be one of ARPHRD_*
 * @apiSince REL
 */

public final int apfPacketFormat;
{ apfPacketFormat = 0; }

/**
 * Version of APF instruction set supported for packet filtering. 0 indicates no support for
 * packet filtering using APF programs.
 * @apiSince REL
 */

public final int apfVersionSupported;
{ apfVersionSupported = 0; }

/**
 * Maximum size of APF program allowed.
 * @apiSince REL
 */

public final int maximumApfProgramSize;
{ maximumApfProgramSize = 0; }
}

