/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app.prediction;


/**
 * A representation of an app target event.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AppTargetEvent implements android.os.Parcelable {

AppTargetEvent(android.os.Parcel parcel) { throw new RuntimeException("Stub!"); }

/**
 * Returns the app target.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.app.prediction.AppTarget getTarget() { throw new RuntimeException("Stub!"); }

/**
 * Returns the launch location.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getLaunchLocation() { throw new RuntimeException("Stub!"); }

/**
 * Returns the action type.
 
 * @return Value is {@link android.app.prediction.AppTargetEvent#ACTION_LAUNCH}, {@link android.app.prediction.AppTargetEvent#ACTION_DISMISS}, or {@link android.app.prediction.AppTargetEvent#ACTION_PIN}
 * @apiSince REL
 */

public int getAction() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Event type constant indicating an app target has been dismissed.
 * @apiSince REL
 */

public static final int ACTION_DISMISS = 2; // 0x2

/**
 * Event type constant indicating an app target has been launched.
 * @apiSince REL
 */

public static final int ACTION_LAUNCH = 1; // 0x1

/**
 * Event type constant indicating an app target has been pinned.
 * @apiSince REL
 */

public static final int ACTION_PIN = 3; // 0x3

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.prediction.AppTargetEvent> CREATOR;
static { CREATOR = null; }
/**
 * A builder for app target events.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * @param target The app target that is associated with this event.
 * This value may be {@code null}.
 * @param actionType The event type, which is one of the values in {@link ActionType}.
 
 * Value is {@link android.app.prediction.AppTargetEvent#ACTION_LAUNCH}, {@link android.app.prediction.AppTargetEvent#ACTION_DISMISS}, or {@link android.app.prediction.AppTargetEvent#ACTION_PIN}
 * @apiSince REL
 */

public Builder(@android.annotation.Nullable android.app.prediction.AppTarget target, int actionType) { throw new RuntimeException("Stub!"); }

/**
 * Sets the launch location.
 
 * @param location This value may be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppTargetEvent.Builder setLaunchLocation(@android.annotation.Nullable java.lang.String location) { throw new RuntimeException("Stub!"); }

/**
 * Builds a new event instance.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppTargetEvent build() { throw new RuntimeException("Stub!"); }
}

}

