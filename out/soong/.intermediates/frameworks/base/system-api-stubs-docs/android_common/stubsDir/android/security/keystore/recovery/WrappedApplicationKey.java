/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.security.keystore.recovery;


/**
 * Helper class with data necessary recover a single application key, given a recovery key.
 *
 * <ul>
 *   <li>Alias - Keystore alias of the key.
 *   <li>Encrypted key material.
 * </ul>
 *
 * Note that Application info is not included. Recovery Agent can only make its own keys
 * recoverable.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class WrappedApplicationKey implements android.os.Parcelable {

WrappedApplicationKey() { throw new RuntimeException("Stub!"); }

/**
 * Application-specific alias of the key.
 *
 * @see java.security.KeyStore.aliases
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getAlias() { throw new RuntimeException("Stub!"); }

/**
 * Key material encrypted by recovery key.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public byte[] getEncryptedKeyMaterial() { throw new RuntimeException("Stub!"); }

/**
 * The metadata with the key.
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public byte[] getMetadata() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.security.keystore.recovery.WrappedApplicationKey> CREATOR;
static { CREATOR = null; }
/**
 * Builder for creating {@link WrappedApplicationKey}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Sets Application-specific alias of the key.
 *
 * @param alias The alias.
 * This value must never be {@code null}.
 * @return This builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.security.keystore.recovery.WrappedApplicationKey.Builder setAlias(@android.annotation.NonNull java.lang.String alias) { throw new RuntimeException("Stub!"); }

/**
 * Sets key material encrypted by recovery key.
 *
 * @param encryptedKeyMaterial The key material
 * This value must never be {@code null}.
 * @return This builder
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.security.keystore.recovery.WrappedApplicationKey.Builder setEncryptedKeyMaterial(@android.annotation.NonNull byte[] encryptedKeyMaterial) { throw new RuntimeException("Stub!"); }

/**
 * Sets the metadata that is authenticated (but unecrypted) with the key material.
 *
 * @param metadata The metadata
 * This value may be {@code null}.
 * @return This builder
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.security.keystore.recovery.WrappedApplicationKey.Builder setMetadata(@android.annotation.Nullable byte[] metadata) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new {@link WrappedApplicationKey} instance.
 *
 * @return new instance
 * This value will never be {@code null}.
 * @throws NullPointerException if some required fields were not set.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.security.keystore.recovery.WrappedApplicationKey build() { throw new RuntimeException("Stub!"); }
}

}

