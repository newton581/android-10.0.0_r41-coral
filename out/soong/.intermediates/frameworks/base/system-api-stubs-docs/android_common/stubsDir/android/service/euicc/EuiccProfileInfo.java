/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.euicc;


/**
 * Information about an embedded profile (subscription) on an eUICC.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class EuiccProfileInfo implements android.os.Parcelable {

EuiccProfileInfo(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Gets the ICCID string.
 * @apiSince REL
 */

public java.lang.String getIccid() { throw new RuntimeException("Stub!"); }

/**
 * Gets the access rules.
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.util.List<android.telephony.UiccAccessRule> getUiccAccessRules() { throw new RuntimeException("Stub!"); }

/**
 * Gets the nickname.
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getNickname() { throw new RuntimeException("Stub!"); }

/**
 * Gets the service provider name.
 * @apiSince REL
 */

public java.lang.String getServiceProviderName() { throw new RuntimeException("Stub!"); }

/**
 * Gets the profile name.
 * @apiSince REL
 */

public java.lang.String getProfileName() { throw new RuntimeException("Stub!"); }

/**
 * Gets the profile class.
 * @return Value is {@link android.service.euicc.EuiccProfileInfo#PROFILE_CLASS_TESTING}, {@link android.service.euicc.EuiccProfileInfo#PROFILE_CLASS_PROVISIONING}, {@link android.service.euicc.EuiccProfileInfo#PROFILE_CLASS_OPERATIONAL}, or android.service.euicc.EuiccProfileInfo.PROFILE_CLASS_UNSET
 * @apiSince REL
 */

public int getProfileClass() { throw new RuntimeException("Stub!"); }

/**
 * Gets the state of the subscription.
 * @return Value is {@link android.service.euicc.EuiccProfileInfo#PROFILE_STATE_DISABLED}, {@link android.service.euicc.EuiccProfileInfo#PROFILE_STATE_ENABLED}, or android.service.euicc.EuiccProfileInfo.PROFILE_STATE_UNSET
 * @apiSince REL
 */

public int getState() { throw new RuntimeException("Stub!"); }

/**
 * Gets the carrier identifier.
 * @apiSince REL
 */

public android.service.carrier.CarrierIdentifier getCarrierIdentifier() { throw new RuntimeException("Stub!"); }

/**
 * Gets the policy rules.
 * @return Value is either <code>0</code> or a combination of {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DO_NOT_DISABLE}, {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DO_NOT_DELETE}, and {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DELETE_AFTER_DISABLING}
 * @apiSince REL
 */

public int getPolicyRules() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether any policy rule exists.
 * @apiSince REL
 */

public boolean hasPolicyRules() { throw new RuntimeException("Stub!"); }

/**
 * Checks whether a certain policy rule exists.
 * @param policy Value is either <code>0</code> or a combination of {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DO_NOT_DISABLE}, {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DO_NOT_DELETE}, and {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DELETE_AFTER_DISABLING}
 * @apiSince REL
 */

public boolean hasPolicyRule(int policy) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.euicc.EuiccProfileInfo> CREATOR;
static { CREATOR = null; }

/**
 * This profile should be deleted after being disabled.
 * @apiSince REL
 */

public static final int POLICY_RULE_DELETE_AFTER_DISABLING = 4; // 0x4

/**
 * This profile cannot be deleted.
 * @apiSince REL
 */

public static final int POLICY_RULE_DO_NOT_DELETE = 2; // 0x2

/**
 * Once this profile is enabled, it cannot be disabled.
 * @apiSince REL
 */

public static final int POLICY_RULE_DO_NOT_DISABLE = 1; // 0x1

/**
 * Operational profiles which can be pre-loaded or downloaded
 * @apiSince REL
 */

public static final int PROFILE_CLASS_OPERATIONAL = 2; // 0x2

/**
 * Provisioning profiles which are pre-loaded on eUICC
 * @apiSince REL
 */

public static final int PROFILE_CLASS_PROVISIONING = 1; // 0x1

/**
 * Testing profiles
 * @apiSince REL
 */

public static final int PROFILE_CLASS_TESTING = 0; // 0x0

/**
 * Disabled profiles
 * @apiSince REL
 */

public static final int PROFILE_STATE_DISABLED = 0; // 0x0

/**
 * Enabled profile
 * @apiSince REL
 */

public static final int PROFILE_STATE_ENABLED = 1; // 0x1
/**
 * The builder to build a new {@link EuiccProfileInfo} instance.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/** @apiSince REL */

public Builder(java.lang.String value) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public Builder(android.service.euicc.EuiccProfileInfo baseProfile) { throw new RuntimeException("Stub!"); }

/**
 * Builds the profile instance.
 * @apiSince REL
 */

public android.service.euicc.EuiccProfileInfo build() { throw new RuntimeException("Stub!"); }

/**
 * Sets the iccId of the subscription.
 * @apiSince REL
 */

public android.service.euicc.EuiccProfileInfo.Builder setIccid(java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the nickname of the subscription.
 * @apiSince REL
 */

public android.service.euicc.EuiccProfileInfo.Builder setNickname(java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the service provider name of the subscription.
 * @apiSince REL
 */

public android.service.euicc.EuiccProfileInfo.Builder setServiceProviderName(java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the profile name of the subscription.
 * @apiSince REL
 */

public android.service.euicc.EuiccProfileInfo.Builder setProfileName(java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the profile class of the subscription.
 * @param value Value is {@link android.service.euicc.EuiccProfileInfo#PROFILE_CLASS_TESTING}, {@link android.service.euicc.EuiccProfileInfo#PROFILE_CLASS_PROVISIONING}, {@link android.service.euicc.EuiccProfileInfo#PROFILE_CLASS_OPERATIONAL}, or android.service.euicc.EuiccProfileInfo.PROFILE_CLASS_UNSET
 * @apiSince REL
 */

public android.service.euicc.EuiccProfileInfo.Builder setProfileClass(int value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the state of the subscription.
 * @param value Value is {@link android.service.euicc.EuiccProfileInfo#PROFILE_STATE_DISABLED}, {@link android.service.euicc.EuiccProfileInfo#PROFILE_STATE_ENABLED}, or android.service.euicc.EuiccProfileInfo.PROFILE_STATE_UNSET
 * @apiSince REL
 */

public android.service.euicc.EuiccProfileInfo.Builder setState(int value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the carrier identifier of the subscription.
 * @apiSince REL
 */

public android.service.euicc.EuiccProfileInfo.Builder setCarrierIdentifier(android.service.carrier.CarrierIdentifier value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the policy rules of the subscription.
 * @param value Value is either <code>0</code> or a combination of {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DO_NOT_DISABLE}, {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DO_NOT_DELETE}, and {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DELETE_AFTER_DISABLING}
 * @apiSince REL
 */

public android.service.euicc.EuiccProfileInfo.Builder setPolicyRules(int value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the access rules of the subscription.
 * @param value This value may be {@code null}.
 * @apiSince REL
 */

public android.service.euicc.EuiccProfileInfo.Builder setUiccAccessRule(@android.annotation.Nullable java.util.List<android.telephony.UiccAccessRule> value) { throw new RuntimeException("Stub!"); }
}

/**
 * Profile policy rules (bit mask)
 * <br>
 * Value is either <code>0</code> or a combination of {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DO_NOT_DISABLE}, {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DO_NOT_DELETE}, and {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DELETE_AFTER_DISABLING}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface PolicyRule {
}

/**
 * Class of the profile
 * <br>
 * Value is {@link android.service.euicc.EuiccProfileInfo#PROFILE_CLASS_TESTING}, {@link android.service.euicc.EuiccProfileInfo#PROFILE_CLASS_PROVISIONING}, {@link android.service.euicc.EuiccProfileInfo#PROFILE_CLASS_OPERATIONAL}, or android.service.euicc.EuiccProfileInfo.PROFILE_CLASS_UNSET
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface ProfileClass {
}

/**
 * State of the profile
 * <br>
 * Value is {@link android.service.euicc.EuiccProfileInfo#PROFILE_STATE_DISABLED}, {@link android.service.euicc.EuiccProfileInfo#PROFILE_STATE_ENABLED}, or android.service.euicc.EuiccProfileInfo.PROFILE_STATE_UNSET
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface ProfileState {
}

}

