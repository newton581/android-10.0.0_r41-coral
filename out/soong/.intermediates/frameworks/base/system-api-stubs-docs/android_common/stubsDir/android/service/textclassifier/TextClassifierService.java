/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.textclassifier;

import android.view.textclassifier.TextClassifier;
import android.view.textclassifier.TextClassificationManager;
import android.os.Looper;
import android.view.textclassifier.TextClassification;
import android.view.textclassifier.TextLinks;
import android.view.textclassifier.TextClassifierEvent;
import android.view.textclassifier.TextClassificationSessionId;
import android.content.Context;
import android.content.Intent;
import android.Manifest;

/**
 * Abstract base class for the TextClassifier service.
 *
 * <p>A TextClassifier service provides text classification related features for the system.
 * The system's default TextClassifierService is configured in
 * {@code config_defaultTextClassifierService}. If this config has no value, a
 * {@link android.view.textclassifier.TextClassifierImpl} is loaded in the calling app's process.
 *
 * <p>See: {@link TextClassifier}.
 * See: {@link TextClassificationManager}.
 *
 * <p>Include the following in the manifest:
 *
 * <pre>
 * {@literal
 * <service android:name=".YourTextClassifierService"
 *          android:permission="android.permission.BIND_TEXTCLASSIFIER_SERVICE">
 *     <intent-filter>
 *         <action android:name="android.service.textclassifier.TextClassifierService" />
 *     </intent-filter>
 * </service>}</pre>
 *
 * <p>From {@link android.os.Build.VERSION_CODES#Q} onward, all callbacks are called on the main
 * thread. Prior to Q, there is no guarantee on what thread the callback will happen. You should
 * make sure the callbacks are executed in your desired thread by using a executor, a handler or
 * something else along the line.
 *
 * @see TextClassifier
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class TextClassifierService extends android.app.Service {

public TextClassifierService() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public final android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Returns suggested text selection start and end indices, recognized entity types, and their
 * associated confidence scores. The entity types are ordered from highest to lowest scoring.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param sessionId the session id
 * This value may be {@code null}.
 * @param request the text selection request
 * This value must never be {@code null}.
 * @param cancellationSignal object to watch for canceling the current operation
 * This value must never be {@code null}.
 * @param callback the callback to return the result to
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onSuggestSelection(@android.annotation.Nullable android.view.textclassifier.TextClassificationSessionId sessionId, @android.annotation.NonNull android.view.textclassifier.TextSelection.Request request, @android.annotation.NonNull android.os.CancellationSignal cancellationSignal, @android.annotation.NonNull android.service.textclassifier.TextClassifierService.Callback<android.view.textclassifier.TextSelection> callback);

/**
 * Classifies the specified text and returns a {@link TextClassification} object that can be
 * used to generate a widget for handling the classified text.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param sessionId the session id
 * This value may be {@code null}.
 * @param request the text classification request
 * This value must never be {@code null}.
 * @param cancellationSignal object to watch for canceling the current operation
 * This value must never be {@code null}.
 * @param callback the callback to return the result to
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onClassifyText(@android.annotation.Nullable android.view.textclassifier.TextClassificationSessionId sessionId, @android.annotation.NonNull android.view.textclassifier.TextClassification.Request request, @android.annotation.NonNull android.os.CancellationSignal cancellationSignal, @android.annotation.NonNull android.service.textclassifier.TextClassifierService.Callback<android.view.textclassifier.TextClassification> callback);

/**
 * Generates and returns a {@link TextLinks} that may be applied to the text to annotate it with
 * links information.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param sessionId the session id
 * This value may be {@code null}.
 * @param request the text classification request
 * This value must never be {@code null}.
 * @param cancellationSignal object to watch for canceling the current operation
 * This value must never be {@code null}.
 * @param callback the callback to return the result to
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onGenerateLinks(@android.annotation.Nullable android.view.textclassifier.TextClassificationSessionId sessionId, @android.annotation.NonNull android.view.textclassifier.TextLinks.Request request, @android.annotation.NonNull android.os.CancellationSignal cancellationSignal, @android.annotation.NonNull android.service.textclassifier.TextClassifierService.Callback<android.view.textclassifier.TextLinks> callback);

/**
 * Detects and returns the language of the give text.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param sessionId the session id
 * This value may be {@code null}.
 * @param request the language detection request
 * This value must never be {@code null}.
 * @param cancellationSignal object to watch for canceling the current operation
 * This value must never be {@code null}.
 * @param callback the callback to return the result to
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onDetectLanguage(@android.annotation.Nullable android.view.textclassifier.TextClassificationSessionId sessionId, @android.annotation.NonNull android.view.textclassifier.TextLanguage.Request request, @android.annotation.NonNull android.os.CancellationSignal cancellationSignal, @android.annotation.NonNull android.service.textclassifier.TextClassifierService.Callback<android.view.textclassifier.TextLanguage> callback) { throw new RuntimeException("Stub!"); }

/**
 * Suggests and returns a list of actions according to the given conversation.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param sessionId the session id
 * This value may be {@code null}.
 * @param request the conversation actions request
 * This value must never be {@code null}.
 * @param cancellationSignal object to watch for canceling the current operation
 * This value must never be {@code null}.
 * @param callback the callback to return the result to
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onSuggestConversationActions(@android.annotation.Nullable android.view.textclassifier.TextClassificationSessionId sessionId, @android.annotation.NonNull android.view.textclassifier.ConversationActions.Request request, @android.annotation.NonNull android.os.CancellationSignal cancellationSignal, @android.annotation.NonNull android.service.textclassifier.TextClassifierService.Callback<android.view.textclassifier.ConversationActions> callback) { throw new RuntimeException("Stub!"); }

/**
 * Writes the selection event.
 * This is called when a selection event occurs. e.g. user changed selection; or smart selection
 * happened.
 *
 * <p>The default implementation ignores the event.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param sessionId the session id
 * This value may be {@code null}.
 * @param event the selection event
 * This value must never be {@code null}.
 * @deprecated
 *      Use {@link #onTextClassifierEvent(TextClassificationSessionId, TextClassifierEvent)}
 *      instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onSelectionEvent(@android.annotation.Nullable android.view.textclassifier.TextClassificationSessionId sessionId, @android.annotation.NonNull android.view.textclassifier.SelectionEvent event) { throw new RuntimeException("Stub!"); }

/**
 * Writes the TextClassifier event.
 * This is called when a TextClassifier event occurs. e.g. user changed selection,
 * smart selection happened, or a link was clicked.
 *
 * <p>The default implementation ignores the event.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param sessionId the session id
 * This value may be {@code null}.
 * @param event the TextClassifier event
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onTextClassifierEvent(@android.annotation.Nullable android.view.textclassifier.TextClassificationSessionId sessionId, @android.annotation.NonNull android.view.textclassifier.TextClassifierEvent event) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new text classification session for the specified context.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param context the text classification context
 * This value must never be {@code null}.
 * @param sessionId the session's Id
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onCreateTextClassificationSession(@android.annotation.NonNull android.view.textclassifier.TextClassificationContext context, @android.annotation.NonNull android.view.textclassifier.TextClassificationSessionId sessionId) { throw new RuntimeException("Stub!"); }

/**
 * Destroys the text classification session identified by the specified sessionId.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param sessionId the id of the session to destroy
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onDestroyTextClassificationSession(@android.annotation.NonNull android.view.textclassifier.TextClassificationSessionId sessionId) { throw new RuntimeException("Stub!"); }

/**
 * Returns a TextClassifier that runs in this service's process.
 * If the local TextClassifier is disabled, this returns {@link TextClassifier#NO_OP}.
 *
 * @deprecated Use {@link #getDefaultTextClassifierImplementation(Context)} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public final android.view.textclassifier.TextClassifier getLocalTextClassifier() { throw new RuntimeException("Stub!"); }

/**
 * Returns the platform's default TextClassifier implementation.
 
 * @param context This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static android.view.textclassifier.TextClassifier getDefaultTextClassifierImplementation(@android.annotation.NonNull android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * The {@link Intent} that must be declared as handled by the service.
 * To be supported, the service must also require the
 * {@link android.Manifest.permission#BIND_TEXTCLASSIFIER_SERVICE} permission so
 * that other applications can not abuse it.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.service.textclassifier.TextClassifierService";
/**
 * Callbacks for TextClassifierService results.
 *
 * @param <T> the type of the result
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface Callback<T> {

/**
 * Returns the result.
 * @apiSince REL
 */

public void onSuccess(T result);

/**
 * Signals a failure.
 * @apiSince REL
 */

public void onFailure(java.lang.CharSequence error);
}

}

