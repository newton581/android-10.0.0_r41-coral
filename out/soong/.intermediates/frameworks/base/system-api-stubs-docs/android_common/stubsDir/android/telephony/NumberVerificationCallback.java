/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;


/**
 * A callback for number verification. After a request for number verification is received,
 * the system will call {@link #onCallReceived(String)} if a phone call was received from a number
 * matching the provided {@link PhoneNumberRange} or it will call {@link #onVerificationFailed(int)}
 * if an error occurs.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface NumberVerificationCallback {

/**
 * Called when the device receives a phone call from the provided {@link PhoneNumberRange}.
 * @param phoneNumber The phone number within the range that called. May or may not contain the
 *                    country code, but will be entirely numeric.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public default void onCallReceived(@android.annotation.NonNull java.lang.String phoneNumber) { throw new RuntimeException("Stub!"); }

/**
 * Called when verification fails for some reason.
 * @param reason The reason for failure.
 
 * Value is {@link android.telephony.NumberVerificationCallback#REASON_UNSPECIFIED}, {@link android.telephony.NumberVerificationCallback#REASON_TIMED_OUT}, {@link android.telephony.NumberVerificationCallback#REASON_NETWORK_NOT_AVAILABLE}, {@link android.telephony.NumberVerificationCallback#REASON_TOO_MANY_CALLS}, {@link android.telephony.NumberVerificationCallback#REASON_CONCURRENT_REQUESTS}, {@link android.telephony.NumberVerificationCallback#REASON_IN_ECBM}, or {@link android.telephony.NumberVerificationCallback#REASON_IN_EMERGENCY_CALL}
 * @apiSince REL
 */

public default void onVerificationFailed(int reason) { throw new RuntimeException("Stub!"); }

/**
 * Verification failed because a previous request for verification has not yet completed.
 * @apiSince REL
 */

public static final int REASON_CONCURRENT_REQUESTS = 4; // 0x4

/**
 * Verification failed because the phone is in emergency callback mode.
 * @apiSince REL
 */

public static final int REASON_IN_ECBM = 5; // 0x5

/**
 * Verification failed because the phone is currently in an emergency call.
 * @apiSince REL
 */

public static final int REASON_IN_EMERGENCY_CALL = 6; // 0x6

/**
 * Verification failed because no cellular voice network is available.
 * @apiSince REL
 */

public static final int REASON_NETWORK_NOT_AVAILABLE = 2; // 0x2

/**
 * Verification failed because no phone call was received from a matching number within the
 * provided timeout.
 * @apiSince REL
 */

public static final int REASON_TIMED_OUT = 1; // 0x1

/**
 * Verification failed because there are currently too many ongoing phone calls for a new
 * incoming phone call to be received.
 * @apiSince REL
 */

public static final int REASON_TOO_MANY_CALLS = 3; // 0x3

/**
 * Verification failed for an unspecified reason.
 * @apiSince REL
 */

public static final int REASON_UNSPECIFIED = 0; // 0x0
}

