/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.provider;

import android.content.Context;

/**
 * The Indexable data for Search.
 *
 * This abstract class defines the common parts for all search indexable data.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class SearchIndexableData {

/**
 * Default constructor.
 * @apiSince REL
 */

public SearchIndexableData() { throw new RuntimeException("Stub!"); }

/**
 * Constructor with a {@link Context}.
 *
 * @param ctx the Context
 * @apiSince REL
 */

public SearchIndexableData(android.content.Context ctx) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * The class name associated with the data. Generally this is a Fragment class name for
 * referring where the data is coming from and for launching the associated Fragment for
 * displaying the data. This is used only when the data is provided "locally".
 *
 * If the data is provided "externally", the relevant information come from the
 * {@link SearchIndexableData#intentAction} and {@link SearchIndexableData#intentTargetPackage}
 * and {@link SearchIndexableData#intentTargetClass}.
 *
 * @see SearchIndexableData#intentAction
 * @see SearchIndexableData#intentTargetPackage
 * @see SearchIndexableData#intentTargetClass
 * @apiSince REL
 */

public java.lang.String className;

/**
 * The context for the data. Will usually allow retrieving some resources.
 *
 * @see Context
 * @apiSince REL
 */

public android.content.Context context;

/**
 * Tells if the data will be included into the search results. This is application specific.
 * @apiSince REL
 */

public boolean enabled;

/**
 * The icon resource ID associated with the data.
 *
 * @see SearchIndexableData#packageName
 * @apiSince REL
 */

public int iconResId;

/**
 * The Intent action associated with the data. This is used when the
 * {@link SearchIndexableData#className} is not relevant.
 *
 * @see SearchIndexableData#intentTargetPackage
 * @see SearchIndexableData#intentTargetClass
 * @apiSince REL
 */

public java.lang.String intentAction;

/**
 * The Intent target class associated with the data.
 *
 * @see SearchIndexableData#intentAction
 * @see SearchIndexableData#intentTargetPackage
 * @apiSince REL
 */

public java.lang.String intentTargetClass;

/**
 * The Intent target package associated with the data.
 *
 * @see SearchIndexableData#intentAction
 * @see SearchIndexableData#intentTargetClass
 * @apiSince REL
 */

public java.lang.String intentTargetPackage;

/**
 * The key for the data. This is application specific. Should be unique per data as the data
 * should be able to be retrieved by the key.
 * <p/>
 * This is required for indexing to work.
 * @apiSince REL
 */

public java.lang.String key;

/**
 * The locale for the data
 * @apiSince REL
 */

public java.util.Locale locale;

/**
 * The package name for retrieving the icon associated with the data.
 *
 * @see SearchIndexableData#iconResId
 * @apiSince REL
 */

public java.lang.String packageName;

/**
 * The rank for the data. This is application specific.
 * @apiSince REL
 */

public int rank;

/**
 * The UserID for the data (in a multi user context). This is application specific and -1 is the
 * default non initialized value.
 * @apiSince REL
 */

public int userId = -1; // 0xffffffff
}

