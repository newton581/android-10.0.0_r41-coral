/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;

import java.util.Set;

/**
 * Config to set Battery Saver policy flags.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class BatterySaverPolicyConfig implements android.os.Parcelable {

BatterySaverPolicyConfig(android.os.BatterySaverPolicyConfig.Builder in) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * How much to adjust the screen brightness while in Battery Saver. This will have no effect
 * if {@link #getEnableAdjustBrightness()} is {@code false}.
 * @apiSince REL
 */

public float getAdjustBrightnessFactor() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to tell the system (and other apps) that Battery Saver is currently enabled.
 * @apiSince REL
 */

public boolean getAdvertiseIsEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to defer full backup while in Battery Saver.
 * @apiSince REL
 */

public boolean getDeferFullBackup() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to defer key-value backup while in Battery Saver.
 * @apiSince REL
 */

public boolean getDeferKeyValueBackup() { throw new RuntimeException("Stub!"); }

/**
 * Returns the device-specific battery saver constants.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.Map<java.lang.String,java.lang.String> getDeviceSpecificSettings() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to disable animation while in Battery Saver.
 * @apiSince REL
 */

public boolean getDisableAnimation() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to disable Always On Display while in Battery Saver.
 * @apiSince REL
 */

public boolean getDisableAod() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to disable launch boost while in Battery Saver.
 * @apiSince REL
 */

public boolean getDisableLaunchBoost() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to disable optional sensors while in Battery Saver.
 * @apiSince REL
 */

public boolean getDisableOptionalSensors() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to disable {@link android.hardware.soundtrigger.SoundTrigger}
 * while in Battery Saver.
 * @apiSince REL
 */

public boolean getDisableSoundTrigger() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to disable vibration while in Battery Saver.
 * @apiSince REL
 */

public boolean getDisableVibration() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to enable brightness adjustment while in Battery Saver.
 * @apiSince REL
 */

public boolean getEnableAdjustBrightness() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to enable Data Saver while in Battery Saver.
 * @apiSince REL
 */

public boolean getEnableDataSaver() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to enable network firewall rules to restrict background network use
 * while in Battery Saver.
 * @apiSince REL
 */

public boolean getEnableFirewall() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to enable night mode while in Battery Saver.
 * @apiSince REL
 */

public boolean getEnableNightMode() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to enable Quick Doze while in Battery Saver.
 * @apiSince REL
 */

public boolean getEnableQuickDoze() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to force all apps to standby mode while in Battery Saver.
 * @apiSince REL
 */

public boolean getForceAllAppsStandby() { throw new RuntimeException("Stub!"); }

/**
 * Whether or not to force background check (disallow background services and manifest
 * broadcast receivers) on all apps (not just apps targeting Android
 * {@link Build.VERSION_CODES#O} and above)
 * while in Battery Saver.
 * @apiSince REL
 */

public boolean getForceBackgroundCheck() { throw new RuntimeException("Stub!"); }

/**
 * The location mode while in Battery Saver.
 * @apiSince REL
 */

public int getLocationMode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.BatterySaverPolicyConfig> CREATOR;
static { CREATOR = null; }
/**
 * Builder class for constructing {@link BatterySaverPolicyConfig} objects.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/** @apiSince REL */

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Set how much to adjust the screen brightness while in Battery Saver. The value should
 * be in the [0, 1] range, where 1 will not change the brightness. This will have no
 * effect if {@link #setEnableAdjustBrightness(boolean)} is not called with {@code true}.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setAdjustBrightnessFactor(float adjustBrightnessFactor) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to tell the system (and other apps) that Battery Saver is
 * currently enabled.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setAdvertiseIsEnabled(boolean advertiseIsEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to defer full backup while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setDeferFullBackup(boolean deferFullBackup) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to defer key-value backup while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setDeferKeyValueBackup(boolean deferKeyValueBackup) { throw new RuntimeException("Stub!"); }

/**
 * Adds a key-value pair for device-specific battery saver constants. The supported keys
 * and values are the same as those in
 * {@link android.provider.Settings.Global#BATTERY_SAVER_DEVICE_SPECIFIC_CONSTANTS}.
 *
 * @throws IllegalArgumentException if the provided key is invalid (empty, null, or all
 * whitespace)
 
 * @param key This value must never be {@code null}.
 
 * @param value This value must never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder addDeviceSpecificSetting(@android.annotation.NonNull java.lang.String key, @android.annotation.NonNull java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to disable animation while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setDisableAnimation(boolean disableAnimation) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to disable Always On Display while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setDisableAod(boolean disableAod) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to disable launch boost while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setDisableLaunchBoost(boolean disableLaunchBoost) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to disable optional sensors while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setDisableOptionalSensors(boolean disableOptionalSensors) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to disable  {@link android.hardware.soundtrigger.SoundTrigger}
 * while in Battery Saver.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setDisableSoundTrigger(boolean disableSoundTrigger) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to disable vibration while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setDisableVibration(boolean disableVibration) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to enable brightness adjustment while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setEnableAdjustBrightness(boolean enableAdjustBrightness) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to enable Data Saver while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setEnableDataSaver(boolean enableDataSaver) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to enable network firewall rules to restrict background network use
 * while in Battery Saver.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setEnableFirewall(boolean enableFirewall) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to enable night mode while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setEnableNightMode(boolean enableNightMode) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to enable Quick Doze while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setEnableQuickDoze(boolean enableQuickDoze) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to force all apps to standby mode while in Battery Saver.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setForceAllAppsStandby(boolean forceAllAppsStandby) { throw new RuntimeException("Stub!"); }

/**
 * Set whether or not to force background check (disallow background services and manifest
 * broadcast receivers) on all apps (not just apps targeting Android
 * {@link Build.VERSION_CODES#O} and above)
 * while in Battery Saver.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setForceBackgroundCheck(boolean forceBackgroundCheck) { throw new RuntimeException("Stub!"); }

/**
 * Set the location mode while in Battery Saver.
 * @param locationMode Value is {@link android.os.PowerManager#LOCATION_MODE_NO_CHANGE}, {@link android.os.PowerManager#LOCATION_MODE_GPS_DISABLED_WHEN_SCREEN_OFF}, {@link android.os.PowerManager#LOCATION_MODE_ALL_DISABLED_WHEN_SCREEN_OFF}, {@link android.os.PowerManager#LOCATION_MODE_FOREGROUND_ONLY}, or {@link android.os.PowerManager#LOCATION_MODE_THROTTLE_REQUESTS_WHEN_SCREEN_OFF}
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig.Builder setLocationMode(int locationMode) { throw new RuntimeException("Stub!"); }

/**
 * Build a {@link BatterySaverPolicyConfig} object using the set parameters. This object
 * is immutable.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.BatterySaverPolicyConfig build() { throw new RuntimeException("Stub!"); }
}

}

