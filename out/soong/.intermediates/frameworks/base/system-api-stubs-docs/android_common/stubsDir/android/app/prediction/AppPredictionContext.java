/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app.prediction;

import android.content.Context;

/**
 * Class that provides contextual information about the environment in which the app prediction is
 * used, such as package name, UI in which the app targets are shown, and number of targets.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AppPredictionContext implements android.os.Parcelable {

AppPredictionContext(@android.annotation.NonNull android.os.Parcel parcel) { throw new RuntimeException("Stub!"); }

/**
 * Returns the UI surface of the prediction context.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getUiSurface() { throw new RuntimeException("Stub!"); }

/**
 * Returns the predicted target count
 
 * @return Value is 0 or greater
 * @apiSince REL
 */

public int getPredictedTargetCount() { throw new RuntimeException("Stub!"); }

/**
 * Returns the package name of the prediction context.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the extras of the prediction context.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param dest This value must never be {@code null}.
 * @apiSince REL
 */

public void writeToParcel(@android.annotation.NonNull android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.prediction.AppPredictionContext> CREATOR;
static { CREATOR = null; }
/**
 * A builder for app prediction contexts.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * @param context The {@link Context} of the prediction client.
 *
 * This value must never be {@code null}.
 * @hide
 */

public Builder(@android.annotation.NonNull android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Sets the number of prediction targets as a hint.
 
 * @param predictedTargetCount Value is 0 or greater
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppPredictionContext.Builder setPredictedTargetCount(int predictedTargetCount) { throw new RuntimeException("Stub!"); }

/**
 * Sets the UI surface.
 
 * @param uiSurface This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppPredictionContext.Builder setUiSurface(@android.annotation.NonNull java.lang.String uiSurface) { throw new RuntimeException("Stub!"); }

/**
 * Sets the extras.
 
 * @param extras This value may be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppPredictionContext.Builder setExtras(@android.annotation.Nullable android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Builds a new context instance.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppPredictionContext build() { throw new RuntimeException("Stub!"); }
}

}

