/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;


/**
 * This class is used to represent a range of phone numbers. Each range corresponds to a contiguous
 * block of phone numbers.
 *
 * Example:
 * {@code
 * {
 *     mCountryCode = "1"
 *     mPrefix = "650555"
 *     mLowerBound = "0055"
 *     mUpperBound = "0899"
 * }
 * }
 * would match 16505550089 and 6505550472, but not 63827593759 or 16505550900
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class PhoneNumberRange implements android.os.Parcelable {

/**
 * @param countryCode The country code, omitting the leading "+"
 * This value must never be {@code null}.
 * @param prefix A prefix that all numbers matching the range must have.
 * This value must never be {@code null}.
 * @param lowerBound When concatenated with the prefix, represents the lower bound of phone
 *                   numbers that match this range.
 * This value must never be {@code null}.
 * @param upperBound When concatenated with the prefix, represents the upper bound of phone
 *                   numbers that match this range.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public PhoneNumberRange(@android.annotation.NonNull java.lang.String countryCode, @android.annotation.NonNull java.lang.String prefix, @android.annotation.NonNull java.lang.String lowerBound, @android.annotation.NonNull java.lang.String upperBound) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Checks to see if the provided phone number matches this range.
 * @param number A phone number, with or without separators or a country code.
 * This value must never be {@code null}.
 * @return {@code true} if the number matches, {@code false} otherwise.
 * @apiSince REL
 */

public boolean matches(@android.annotation.NonNull java.lang.String number) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.PhoneNumberRange> CREATOR;
static { CREATOR = null; }
}

