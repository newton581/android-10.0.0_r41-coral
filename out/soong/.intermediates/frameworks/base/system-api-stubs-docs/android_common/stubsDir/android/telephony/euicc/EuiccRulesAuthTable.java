/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.telephony.euicc;

import android.service.euicc.EuiccProfileInfo;

/**
 * This represents the RAT (Rules Authorisation Table) stored on eUICC.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class EuiccRulesAuthTable implements android.os.Parcelable {

EuiccRulesAuthTable(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/**
 * Finds the index of the first authorisation rule matching the given policy and carrier id. If
 * the returned index is not negative, the carrier is allowed to apply this policy to its
 * profile.
 *
 * @param policy The policy rule.
 * Value is either <code>0</code> or a combination of {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DO_NOT_DISABLE}, {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DO_NOT_DELETE}, and {@link android.service.euicc.EuiccProfileInfo#POLICY_RULE_DELETE_AFTER_DISABLING}
 * @param carrierId The carrier id.
 * @return The index of authorization rule. If no rule is found, -1 will be returned.
 * @apiSince REL
 */

public int findIndex(int policy, android.service.carrier.CarrierIdentifier carrierId) { throw new RuntimeException("Stub!"); }

/**
 * Tests if the entry in the table has the given policy rule flag.
 *
 * @param index The index of the entry.
 * @param flag The policy rule flag to be tested.
 * Value is either <code>0</code> or {@link android.telephony.euicc.EuiccRulesAuthTable#POLICY_RULE_FLAG_CONSENT_REQUIRED}
 * @throws ArrayIndexOutOfBoundsException If the {@code index} is negative or larger than the
 *     size of this table.
 * @apiSince REL
 */

public boolean hasPolicyRuleFlag(int index, int flag) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.euicc.EuiccRulesAuthTable> CREATOR;
static { CREATOR = null; }

/**
 * User consent is required to install the profile.
 * @apiSince REL
 */

public static final int POLICY_RULE_FLAG_CONSENT_REQUIRED = 1; // 0x1
/**
 * This is used to build new {@link EuiccRulesAuthTable} instance.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Creates a new builder.
 *
 * @param ruleNum The number of authorisation rules in the table.
 * @apiSince REL
 */

public Builder(int ruleNum) { throw new RuntimeException("Stub!"); }

/**
 * Builds the RAT instance. This builder should not be used anymore after this method is
 * called, otherwise {@link NullPointerException} will be thrown.
 * @apiSince REL
 */

public android.telephony.euicc.EuiccRulesAuthTable build() { throw new RuntimeException("Stub!"); }

/**
 * Adds an authorisation rule.
 *
 * @throws ArrayIndexOutOfBoundsException If the {@code mPosition} is larger than the size
 *     this table.
 * @apiSince REL
 */

public android.telephony.euicc.EuiccRulesAuthTable.Builder add(int policyRules, java.util.List<android.service.carrier.CarrierIdentifier> carrierId, int policyRuleFlags) { throw new RuntimeException("Stub!"); }
}

/**
 * Profile policy rule flags
 * <br>
 * Value is either <code>0</code> or {@link android.telephony.euicc.EuiccRulesAuthTable#POLICY_RULE_FLAG_CONSENT_REQUIRED}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface PolicyRuleFlag {
}

}

