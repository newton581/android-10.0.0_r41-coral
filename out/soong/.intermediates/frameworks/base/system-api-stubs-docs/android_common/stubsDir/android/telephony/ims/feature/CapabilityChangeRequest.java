/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims.feature;

import android.telephony.ims.stub.ImsRegistrationImplBase;
import java.util.List;

/**
 * Request to send to IMS provider, which will try to enable/disable capabilities that are added to
 * the request.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CapabilityChangeRequest implements android.os.Parcelable {

/** @hide */

CapabilityChangeRequest() { throw new RuntimeException("Stub!"); }

/**
 * Add one or many capabilities to the request to be enabled.
 *
 * @param capabilities A bitfield of capabilities to enable, valid values are defined in
 *   {@link MmTelFeature.MmTelCapabilities.MmTelCapability}.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @param radioTech  the radio tech that these capabilities should be enabled for, valid
 *   values are in {@link ImsRegistrationImplBase.ImsRegistrationTech}.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @apiSince REL
 */

public void addCapabilitiesToEnableForTech(int capabilities, int radioTech) { throw new RuntimeException("Stub!"); }

/**
 * Add one or many capabilities to the request to be disabled.
 * @param capabilities A bitfield of capabilities to diable, valid values are defined in
 *   {@link MmTelFeature.MmTelCapabilities.MmTelCapability}.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @param radioTech the radio tech that these capabilities should be disabled for, valid
 *   values are in {@link ImsRegistrationImplBase.ImsRegistrationTech}.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @apiSince REL
 */

public void addCapabilitiesToDisableForTech(int capabilities, int radioTech) { throw new RuntimeException("Stub!"); }

/**
 * @return a {@link List} of {@link CapabilityPair}s that are requesting to be enabled.
 * @apiSince REL
 */

public java.util.List<android.telephony.ims.feature.CapabilityChangeRequest.CapabilityPair> getCapabilitiesToEnable() { throw new RuntimeException("Stub!"); }

/**
 * @return a {@link List} of {@link CapabilityPair}s that are requesting to be disabled.
 * @apiSince REL
 */

public java.util.List<android.telephony.ims.feature.CapabilityChangeRequest.CapabilityPair> getCapabilitiesToDisable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.ims.feature.CapabilityChangeRequest> CREATOR;
static { CREATOR = null; }
/**
 * Contains a feature capability, defined as
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE},
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO},
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, or
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS},
 * along with an associated technology, defined as
 * {@link ImsRegistrationImplBase#REGISTRATION_TECH_LTE} or
 * {@link ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class CapabilityPair {

/**
 * @param capability Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 
 * @param radioTech Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @apiSince REL
 */

public CapabilityPair(int capability, int radioTech) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * @return The stored capability, defined as
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE},
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO},
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, or
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @apiSince REL
 */

public int getCapability() { throw new RuntimeException("Stub!"); }

/**
 * @return the stored radio technology, defined as
 * {@link ImsRegistrationImplBase#REGISTRATION_TECH_LTE} or
 * {@link ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @apiSince REL
 */

public int getRadioTech() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

}

