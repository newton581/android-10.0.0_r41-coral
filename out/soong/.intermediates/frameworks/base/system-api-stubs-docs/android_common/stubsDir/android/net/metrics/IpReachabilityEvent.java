/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.metrics;


/**
 * An event recorded when IpReachabilityMonitor sends a neighbor probe or receives
 * a neighbor probe result.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class IpReachabilityEvent implements android.net.metrics.IpConnectivityLog.Event {

/** @apiSince REL */

public IpReachabilityEvent(int eventType) { throw new RuntimeException("Stub!"); }

/** @hide */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @hide */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * Neighbor unreachable after a forced probe.
 * @apiSince REL
 */

public static final int NUD_FAILED = 512; // 0x200

/**
 * Neighbor unreachable notification from kernel.
 * @apiSince REL
 */

public static final int NUD_FAILED_ORGANIC = 1024; // 0x400

/**
 * A probe forced by IpReachabilityMonitor.
 * @apiSince REL
 */

public static final int PROBE = 256; // 0x100

/**
 * Neighbor unreachable after a forced probe, IP provisioning is also lost.
 * @apiSince REL
 */

public static final int PROVISIONING_LOST = 768; // 0x300

/**
 * Neighbor unreachable notification from kernel, IP provisioning is also lost.
 * @apiSince REL
 */

public static final int PROVISIONING_LOST_ORGANIC = 1280; // 0x500
}

