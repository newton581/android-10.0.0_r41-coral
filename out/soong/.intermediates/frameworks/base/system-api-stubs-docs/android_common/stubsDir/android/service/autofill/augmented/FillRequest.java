/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill.augmented;


/**
 * Represents a request to augment-fill an activity.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class FillRequest {

FillRequest() { throw new RuntimeException("Stub!"); }

/**
 * Gets the task of the activity associated with this request.
 * @apiSince REL
 */

public int getTaskId() { throw new RuntimeException("Stub!"); }

/**
 * Gets the name of the activity associated with this request.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.content.ComponentName getActivityComponent() { throw new RuntimeException("Stub!"); }

/**
 * Gets the id of the field that triggered the request.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.view.autofill.AutofillId getFocusedId() { throw new RuntimeException("Stub!"); }

/**
 * Gets the current value of the field that triggered the request.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.view.autofill.AutofillValue getFocusedValue() { throw new RuntimeException("Stub!"); }

/**
 * Gets the Smart Suggestions object used to embed the autofill UI.
 *
 * @return object used to embed the autofill UI, or {@code null} if not supported.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.service.autofill.augmented.PresentationParams getPresentationParams() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

