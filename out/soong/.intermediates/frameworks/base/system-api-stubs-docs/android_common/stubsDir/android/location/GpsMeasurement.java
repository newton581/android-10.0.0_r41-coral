/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.location;


/**
 * A class representing a GPS satellite measurement, containing raw and computed information.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class GpsMeasurement implements android.os.Parcelable {

GpsMeasurement() { throw new RuntimeException("Stub!"); }

/**
 * Sets all contents to the values stored in the provided object.
 * @apiSince REL
 */

public void set(android.location.GpsMeasurement measurement) { throw new RuntimeException("Stub!"); }

/**
 * Resets all the contents to its original state.
 * @apiSince REL
 */

public void reset() { throw new RuntimeException("Stub!"); }

/**
 * Gets the Pseudo-random number (PRN).
 * Range: [1, 32]
 * @apiSince REL
 */

public byte getPrn() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Pseud-random number (PRN).
 * @apiSince REL
 */

public void setPrn(byte value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the time offset at which the measurement was taken in nanoseconds.
 * The reference receiver's time is specified by {@link GpsClock#getTimeInNs()} and should be
 * interpreted in the same way as indicated by {@link GpsClock#getType()}.
 *
 * The sign of this value is given by the following equation:
 *      measurement time = time_ns + time_offset_ns
 *
 * The value provides an individual time-stamp for the measurement, and allows sub-nanosecond
 * accuracy.
 * @apiSince REL
 */

public double getTimeOffsetInNs() { throw new RuntimeException("Stub!"); }

/**
 * Sets the time offset at which the measurement was taken in nanoseconds.
 * @apiSince REL
 */

public void setTimeOffsetInNs(double value) { throw new RuntimeException("Stub!"); }

/**
 * Gets per-satellite sync state.
 * It represents the current sync state for the associated satellite.
 *
 * This value helps interpret {@link #getReceivedGpsTowInNs()}.
 * @apiSince REL
 */

public short getState() { throw new RuntimeException("Stub!"); }

/**
 * Sets the sync state.
 * @apiSince REL
 */

public void setState(short value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the received GPS Time-of-Week at the measurement time, in nanoseconds.
 * The value is relative to the beginning of the current GPS week.
 *
 * Given {@link #getState()} of the GPS receiver, the range of this field can be:
 *      Searching           : [ 0           ]   : {@link #STATE_UNKNOWN} is set
 *      Ranging code lock   : [ 0    1 ms   ]   : {@link #STATE_CODE_LOCK} is set
 *      Bit sync            : [ 0   20 ms   ]   : {@link #STATE_BIT_SYNC} is set
 *      Subframe sync       : [ 0    6 ms   ]   : {@link #STATE_SUBFRAME_SYNC} is set
 *      TOW decoded         : [ 0    1 week ]   : {@link #STATE_TOW_DECODED} is set
 * @apiSince REL
 */

public long getReceivedGpsTowInNs() { throw new RuntimeException("Stub!"); }

/**
 * Sets the received GPS time-of-week in nanoseconds.
 * @apiSince REL
 */

public void setReceivedGpsTowInNs(long value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the received GPS time-of-week's uncertainty (1-Sigma) in nanoseconds.
 * @apiSince REL
 */

public long getReceivedGpsTowUncertaintyInNs() { throw new RuntimeException("Stub!"); }

/**
 * Sets the received GPS time-of-week's uncertainty (1-Sigma) in nanoseconds.
 * @apiSince REL
 */

public void setReceivedGpsTowUncertaintyInNs(long value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the Carrier-to-noise density in dB-Hz.
 * Range: [0, 63].
 *
 * The value contains the measured C/N0 for the signal at the antenna input.
 * @apiSince REL
 */

public double getCn0InDbHz() { throw new RuntimeException("Stub!"); }

/**
 * Sets the carrier-to-noise density in dB-Hz.
 * @apiSince REL
 */

public void setCn0InDbHz(double value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the Pseudorange rate at the timestamp in m/s.
 * The reported value includes {@link #getPseudorangeRateUncertaintyInMetersPerSec()}.
 *
 * The correction of a given Pseudorange Rate value includes corrections from receiver and
 * satellite clock frequency errors.
 * {@link #isPseudorangeRateCorrected()} identifies the type of value reported.
 *
 * A positive 'uncorrected' value indicates that the SV is moving away from the receiver.
 * The sign of the 'uncorrected' Pseudorange Rate and its relation to the sign of
 * {@link #getDopplerShiftInHz()} is given by the equation:
 *      pseudorange rate = -k * doppler shift   (where k is a constant)
 * @apiSince REL
 */

public double getPseudorangeRateInMetersPerSec() { throw new RuntimeException("Stub!"); }

/**
 * Sets the pseudorange rate at the timestamp in m/s.
 * @apiSince REL
 */

public void setPseudorangeRateInMetersPerSec(double value) { throw new RuntimeException("Stub!"); }

/**
 * See {@link #getPseudorangeRateInMetersPerSec()} for more details.
 *
 * @return {@code true} if {@link #getPseudorangeRateInMetersPerSec()} contains a corrected
 *         value, {@code false} if it contains an uncorrected value.
 * @apiSince REL
 */

public boolean isPseudorangeRateCorrected() { throw new RuntimeException("Stub!"); }

/**
 * Gets the pseudorange's rate uncertainty (1-Sigma) in m/s.
 * The uncertainty is represented as an absolute (single sided) value.
 * @apiSince REL
 */

public double getPseudorangeRateUncertaintyInMetersPerSec() { throw new RuntimeException("Stub!"); }

/**
 * Sets the pseudorange's rate uncertainty (1-Sigma) in m/s.
 * @apiSince REL
 */

public void setPseudorangeRateUncertaintyInMetersPerSec(double value) { throw new RuntimeException("Stub!"); }

/**
 * Gets 'Accumulated Delta Range' state.
 * It indicates whether {@link #getAccumulatedDeltaRangeInMeters()} is reset or there is a
 * cycle slip (indicating 'loss of lock').
 * @apiSince REL
 */

public short getAccumulatedDeltaRangeState() { throw new RuntimeException("Stub!"); }

/**
 * Sets the 'Accumulated Delta Range' state.
 * @apiSince REL
 */

public void setAccumulatedDeltaRangeState(short value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the accumulated delta range since the last channel reset, in meters.
 * The reported value includes {@link #getAccumulatedDeltaRangeUncertaintyInMeters()}.
 *
 * The availability of the value is represented by {@link #getAccumulatedDeltaRangeState()}.
 *
 * A positive value indicates that the SV is moving away from the receiver.
 * The sign of {@link #getAccumulatedDeltaRangeInMeters()} and its relation to the sign of
 * {@link #getCarrierPhase()} is given by the equation:
 *          accumulated delta range = -k * carrier phase    (where k is a constant)
 * @apiSince REL
 */

public double getAccumulatedDeltaRangeInMeters() { throw new RuntimeException("Stub!"); }

/**
 * Sets the accumulated delta range in meters.
 * @apiSince REL
 */

public void setAccumulatedDeltaRangeInMeters(double value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the accumulated delta range's uncertainty (1-Sigma) in meters.
 * The uncertainty is represented as an absolute (single sided) value.
 *
 * The status of the value is represented by {@link #getAccumulatedDeltaRangeState()}.
 * @apiSince REL
 */

public double getAccumulatedDeltaRangeUncertaintyInMeters() { throw new RuntimeException("Stub!"); }

/**
 * Sets the accumulated delta range's uncertainty (1-sigma) in meters.
 *
 * The status of the value is represented by {@link #getAccumulatedDeltaRangeState()}.
 * @apiSince REL
 */

public void setAccumulatedDeltaRangeUncertaintyInMeters(double value) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getPseudorangeInMeters()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasPseudorangeInMeters() { throw new RuntimeException("Stub!"); }

/**
 * Gets the best derived pseudorange by the chipset, in meters.
 * The reported pseudorange includes {@link #getPseudorangeUncertaintyInMeters()}.
 *
 * The value is only available if {@link #hasPseudorangeInMeters()} is true.
 * @apiSince REL
 */

public double getPseudorangeInMeters() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Pseudo-range in meters.
 * @apiSince REL
 */

public void setPseudorangeInMeters(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Pseudo-range in meters.
 * @apiSince REL
 */

public void resetPseudorangeInMeters() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getPseudorangeUncertaintyInMeters()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasPseudorangeUncertaintyInMeters() { throw new RuntimeException("Stub!"); }

/**
 * Gets the pseudorange's uncertainty (1-Sigma) in meters.
 * The value contains the 'pseudorange' and 'clock' uncertainty in it.
 * The uncertainty is represented as an absolute (single sided) value.
 *
 * The value is only available if {@link #hasPseudorangeUncertaintyInMeters()} is true.
 * @apiSince REL
 */

public double getPseudorangeUncertaintyInMeters() { throw new RuntimeException("Stub!"); }

/**
 * Sets the pseudo-range's uncertainty (1-Sigma) in meters.
 * @apiSince REL
 */

public void setPseudorangeUncertaintyInMeters(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the pseudo-range's uncertainty (1-Sigma) in meters.
 * @apiSince REL
 */

public void resetPseudorangeUncertaintyInMeters() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getCodePhaseInChips()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasCodePhaseInChips() { throw new RuntimeException("Stub!"); }

/**
 * Gets the fraction of the current C/A code cycle.
 * Range: [0, 1023]
 * The reference frequency is given by the value of {@link #getCarrierFrequencyInHz()}.
 * The reported code-phase includes {@link #getCodePhaseUncertaintyInChips()}.
 *
 * The value is only available if {@link #hasCodePhaseInChips()} is true.
 * @apiSince REL
 */

public double getCodePhaseInChips() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Code-phase in chips.
 * @apiSince REL
 */

public void setCodePhaseInChips(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Code-phase in chips.
 * @apiSince REL
 */

public void resetCodePhaseInChips() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getCodePhaseUncertaintyInChips()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasCodePhaseUncertaintyInChips() { throw new RuntimeException("Stub!"); }

/**
 * Gets the code-phase's uncertainty (1-Sigma) as a fraction of chips.
 * The uncertainty is represented as an absolute (single sided) value.
 *
 * The value is only available if {@link #hasCodePhaseUncertaintyInChips()} is true.
 * @apiSince REL
 */

public double getCodePhaseUncertaintyInChips() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Code-phase's uncertainty (1-Sigma) in fractions of chips.
 * @apiSince REL
 */

public void setCodePhaseUncertaintyInChips(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Code-phase's uncertainty (1-Sigma) in fractions of chips.
 * @apiSince REL
 */

public void resetCodePhaseUncertaintyInChips() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getCarrierFrequencyInHz()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasCarrierFrequencyInHz() { throw new RuntimeException("Stub!"); }

/**
 * Gets the carrier frequency at which codes and messages are modulated, it can be L1 or L2.
 * If the field is not set, the carrier frequency corresponds to L1.
 *
 * The value is only available if {@link #hasCarrierFrequencyInHz()} is true.
 * @apiSince REL
 */

public float getCarrierFrequencyInHz() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Carrier frequency (L1 or L2) in Hz.
 * @apiSince REL
 */

public void setCarrierFrequencyInHz(float carrierFrequencyInHz) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Carrier frequency (L1 or L2) in Hz.
 * @apiSince REL
 */

public void resetCarrierFrequencyInHz() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getCarrierCycles()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasCarrierCycles() { throw new RuntimeException("Stub!"); }

/**
 * The number of full carrier cycles between the satellite and the receiver.
 * The reference frequency is given by the value of {@link #getCarrierFrequencyInHz()}.
 *
 * The value is only available if {@link #hasCarrierCycles()} is true.
 * @apiSince REL
 */

public long getCarrierCycles() { throw new RuntimeException("Stub!"); }

/**
 * Sets the number of full carrier cycles between the satellite and the receiver.
 * @apiSince REL
 */

public void setCarrierCycles(long value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the number of full carrier cycles between the satellite and the receiver.
 * @apiSince REL
 */

public void resetCarrierCycles() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getCarrierPhase()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasCarrierPhase() { throw new RuntimeException("Stub!"); }

/**
 * Gets the RF phase detected by the receiver.
 * Range: [0.0, 1.0].
 * This is usually the fractional part of the complete carrier phase measurement.
 *
 * The reference frequency is given by the value of {@link #getCarrierFrequencyInHz()}.
 * The reported carrier-phase includes {@link #getCarrierPhaseUncertainty()}.
 *
 * The value is only available if {@link #hasCarrierPhase()} is true.
 * @apiSince REL
 */

public double getCarrierPhase() { throw new RuntimeException("Stub!"); }

/**
 * Sets the RF phase detected by the receiver.
 * @apiSince REL
 */

public void setCarrierPhase(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the RF phase detected by the receiver.
 * @apiSince REL
 */

public void resetCarrierPhase() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getCarrierPhaseUncertainty()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasCarrierPhaseUncertainty() { throw new RuntimeException("Stub!"); }

/**
 * Gets the carrier-phase's uncertainty (1-Sigma).
 * The uncertainty is represented as an absolute (single sided) value.
 *
 * The value is only available if {@link #hasCarrierPhaseUncertainty()} is true.
 * @apiSince REL
 */

public double getCarrierPhaseUncertainty() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Carrier-phase's uncertainty (1-Sigma) in cycles.
 * @apiSince REL
 */

public void setCarrierPhaseUncertainty(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Carrier-phase's uncertainty (1-Sigma) in cycles.
 * @apiSince REL
 */

public void resetCarrierPhaseUncertainty() { throw new RuntimeException("Stub!"); }

/**
 * Gets a value indicating the 'loss of lock' state of the event.
 * @apiSince REL
 */

public byte getLossOfLock() { throw new RuntimeException("Stub!"); }

/**
 * Sets the 'loss of lock' status.
 * @apiSince REL
 */

public void setLossOfLock(byte value) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getBitNumber()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasBitNumber() { throw new RuntimeException("Stub!"); }

/**
 * Gets the number of GPS bits transmitted since Sat-Sun midnight (GPS week).
 *
 * The value is only available if {@link #hasBitNumber()} is true.
 * @apiSince REL
 */

public int getBitNumber() { throw new RuntimeException("Stub!"); }

/**
 * Sets the bit number within the broadcast frame.
 * @apiSince REL
 */

public void setBitNumber(int bitNumber) { throw new RuntimeException("Stub!"); }

/**
 * Resets the bit number within the broadcast frame.
 * @apiSince REL
 */

public void resetBitNumber() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getTimeFromLastBitInMs()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasTimeFromLastBitInMs() { throw new RuntimeException("Stub!"); }

/**
 * Gets the elapsed time since the last received bit in milliseconds.
 * Range: [0, 20].
 *
 * The value is only available if {@link #hasTimeFromLastBitInMs()} is true.
 * @apiSince REL
 */

public short getTimeFromLastBitInMs() { throw new RuntimeException("Stub!"); }

/**
 * Sets the elapsed time since the last received bit in milliseconds.
 * @apiSince REL
 */

public void setTimeFromLastBitInMs(short value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the elapsed time since the last received bit in milliseconds.
 * @apiSince REL
 */

public void resetTimeFromLastBitInMs() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getDopplerShiftInHz()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasDopplerShiftInHz() { throw new RuntimeException("Stub!"); }

/**
 * Gets the Doppler Shift in Hz.
 * A positive value indicates that the SV is moving toward the receiver.
 *
 * The reference frequency is given by the value of {@link #getCarrierFrequencyInHz()}.
 * The reported doppler shift includes {@link #getDopplerShiftUncertaintyInHz()}.
 *
 * The value is only available if {@link #hasDopplerShiftInHz()} is true.
 * @apiSince REL
 */

public double getDopplerShiftInHz() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Doppler shift in Hz.
 * @apiSince REL
 */

public void setDopplerShiftInHz(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Doppler shift in Hz.
 * @apiSince REL
 */

public void resetDopplerShiftInHz() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getDopplerShiftUncertaintyInHz()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasDopplerShiftUncertaintyInHz() { throw new RuntimeException("Stub!"); }

/**
 * Gets the Doppler's Shift uncertainty (1-Sigma) in Hz.
 * The uncertainty is represented as an absolute (single sided) value.
 *
 * The value is only available if {@link #hasDopplerShiftUncertaintyInHz()} is true.
 * @apiSince REL
 */

public double getDopplerShiftUncertaintyInHz() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Doppler's shift uncertainty (1-Sigma) in Hz.
 * @apiSince REL
 */

public void setDopplerShiftUncertaintyInHz(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Doppler's shift uncertainty (1-Sigma) in Hz.
 * @apiSince REL
 */

public void resetDopplerShiftUncertaintyInHz() { throw new RuntimeException("Stub!"); }

/**
 * Gets a value indicating the 'multipath' state of the event.
 * @apiSince REL
 */

public byte getMultipathIndicator() { throw new RuntimeException("Stub!"); }

/**
 * Sets the 'multi-path' indicator.
 * @apiSince REL
 */

public void setMultipathIndicator(byte value) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getSnrInDb()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasSnrInDb() { throw new RuntimeException("Stub!"); }

/**
 * Gets the Signal-to-Noise ratio (SNR) in dB.
 *
 * The value is only available if {@link #hasSnrInDb()} is true.
 * @apiSince REL
 */

public double getSnrInDb() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Signal-to-noise ratio (SNR) in dB.
 * @apiSince REL
 */

public void setSnrInDb(double snrInDb) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Signal-to-noise ratio (SNR) in dB.
 * @apiSince REL
 */

public void resetSnrInDb() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getElevationInDeg()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasElevationInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Gets the Elevation in degrees.
 * Range: [-90, 90]
 * The reported elevation includes {@link #getElevationUncertaintyInDeg()}.
 *
 * The value is only available if {@link #hasElevationInDeg()} is true.
 * @apiSince REL
 */

public double getElevationInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Elevation in degrees.
 * @apiSince REL
 */

public void setElevationInDeg(double elevationInDeg) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Elevation in degrees.
 * @apiSince REL
 */

public void resetElevationInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getElevationUncertaintyInDeg()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasElevationUncertaintyInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Gets the elevation's uncertainty (1-Sigma) in degrees.
 * Range: [0, 90]
 *
 * The uncertainty is represented as an absolute (single sided) value.
 *
 * The value is only available if {@link #hasElevationUncertaintyInDeg()} is true.
 * @apiSince REL
 */

public double getElevationUncertaintyInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Sets the elevation's uncertainty (1-Sigma) in degrees.
 * @apiSince REL
 */

public void setElevationUncertaintyInDeg(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the elevation's uncertainty (1-Sigma) in degrees.
 * @apiSince REL
 */

public void resetElevationUncertaintyInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getAzimuthInDeg()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasAzimuthInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Gets the azimuth in degrees.
 * Range: [0, 360).
 *
 * The reported azimuth includes {@link #getAzimuthUncertaintyInDeg()}.
 *
 * The value is only available if {@link #hasAzimuthInDeg()} is true.
 * @apiSince REL
 */

public double getAzimuthInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Azimuth in degrees.
 * @apiSince REL
 */

public void setAzimuthInDeg(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Azimuth in degrees.
 * @apiSince REL
 */

public void resetAzimuthInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getAzimuthUncertaintyInDeg()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasAzimuthUncertaintyInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Gets the azimuth's uncertainty (1-Sigma) in degrees.
 * Range: [0, 180].
 *
 * The uncertainty is represented as an absolute (single sided) value.
 *
 * The value is only available if {@link #hasAzimuthUncertaintyInDeg()} is true.
 * @apiSince REL
 */

public double getAzimuthUncertaintyInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Azimuth's uncertainty (1-Sigma) in degrees.
 * @apiSince REL
 */

public void setAzimuthUncertaintyInDeg(double value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the Azimuth's uncertainty (1-Sigma) in degrees.
 * @apiSince REL
 */

public void resetAzimuthUncertaintyInDeg() { throw new RuntimeException("Stub!"); }

/**
 * Gets a flag indicating whether the GPS represented by the measurement was used for computing
 * the most recent fix.
 *
 * @return A non-null value if the data is available, null otherwise.
 * @apiSince REL
 */

public boolean isUsedInFix() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Used-in-Fix flag.
 * @apiSince REL
 */

public void setUsedInFix(boolean value) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * The state of the 'Accumulated Delta Range' has a cycle slip detected.
 * @apiSince REL
 */

public static final short ADR_STATE_CYCLE_SLIP = 4; // 0x4

/**
 * The state of the 'Accumulated Delta Range' has detected a reset.
 * @apiSince REL
 */

public static final short ADR_STATE_RESET = 2; // 0x2

/**
 * The state of the 'Accumulated Delta Range' is invalid or unknown.
 * @apiSince REL
 */

public static final short ADR_STATE_UNKNOWN = 0; // 0x0

/**
 * The state of the 'Accumulated Delta Range' is valid.
 * @apiSince REL
 */

public static final short ADR_STATE_VALID = 1; // 0x1

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.location.GpsMeasurement> CREATOR;
static { CREATOR = null; }

/**
 * 'Loss of lock' detected between the previous and current observation: cycle slip possible.
 * @apiSince REL
 */

public static final byte LOSS_OF_LOCK_CYCLE_SLIP = 2; // 0x2

/**
 * The measurement does not present any indication of 'loss of lock'.
 * @apiSince REL
 */

public static final byte LOSS_OF_LOCK_OK = 1; // 0x1

/**
 * The indicator is not available or it is unknown.
 * @apiSince REL
 */

public static final byte LOSS_OF_LOCK_UNKNOWN = 0; // 0x0

/**
 * The measurement has been indicated to use multi-path.
 * @apiSince REL
 */

public static final byte MULTIPATH_INDICATOR_DETECTED = 1; // 0x1

/**
 * The measurement has been indicated not tu use multi-path.
 * @apiSince REL
 */

public static final byte MULTIPATH_INDICATOR_NOT_USED = 2; // 0x2

/**
 * The indicator is not available or it is unknown.
 * @apiSince REL
 */

public static final byte MULTIPATH_INDICATOR_UNKNOWN = 0; // 0x0

/**
 * The state of the GPS receiver is in bit sync.
 * @apiSince REL
 */

public static final short STATE_BIT_SYNC = 2; // 0x2

/**
 * The state of the GPS receiver is ranging code lock.
 * @apiSince REL
 */

public static final short STATE_CODE_LOCK = 1; // 0x1

/**
 * The state of the GPS receiver contains millisecond ambiguity.
 * @apiSince REL
 */

public static final short STATE_MSEC_AMBIGUOUS = 16; // 0x10

/**
 *The state of the GPS receiver is in sub-frame sync.
 @apiSince REL
 */

public static final short STATE_SUBFRAME_SYNC = 4; // 0x4

/**
 * The state of the GPS receiver has TOW decoded.
 * @apiSince REL
 */

public static final short STATE_TOW_DECODED = 8; // 0x8

/**
 * The state of GPS receiver the measurement is invalid or unknown.
 * @apiSince REL
 */

public static final short STATE_UNKNOWN = 0; // 0x0
}

