/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.euicc;


/**
 * Result of a {@link EuiccService#onDownloadSubscription} operation.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class DownloadSubscriptionResult implements android.os.Parcelable {

/**
 * @param result Value is {@link android.service.euicc.EuiccService#RESULT_OK}, {@link android.service.euicc.EuiccService#RESULT_MUST_DEACTIVATE_SIM}, {@link android.service.euicc.EuiccService#RESULT_RESOLVABLE_ERRORS}, {@link android.service.euicc.EuiccService#RESULT_NEED_CONFIRMATION_CODE}, or {@link android.service.euicc.EuiccService#RESULT_FIRST_USER}
 
 * @param resolvableErrors Value is either <code>0</code> or a combination of {@link android.service.euicc.EuiccService#RESOLVABLE_ERROR_CONFIRMATION_CODE}, and {@link android.service.euicc.EuiccService#RESOLVABLE_ERROR_POLICY_RULES}
 * @apiSince REL
 */

public DownloadSubscriptionResult(int result, int resolvableErrors, int cardId) { throw new RuntimeException("Stub!"); }

/**
 * Gets the result of the operation.
 * @return Value is {@link android.service.euicc.EuiccService#RESULT_OK}, {@link android.service.euicc.EuiccService#RESULT_MUST_DEACTIVATE_SIM}, {@link android.service.euicc.EuiccService#RESULT_RESOLVABLE_ERRORS}, {@link android.service.euicc.EuiccService#RESULT_NEED_CONFIRMATION_CODE}, or {@link android.service.euicc.EuiccService#RESULT_FIRST_USER}
 * @apiSince REL
 */

public int getResult() { throw new RuntimeException("Stub!"); }

/**
 * Gets the bit map of resolvable errors.
 *
 * <p>The value is passed from EuiccService. The values can be
 *
 * <ul>
 * <li>{@link EuiccService#RESOLVABLE_ERROR_CONFIRMATION_CODE}
 * <li>{@link EuiccService#RESOLVABLE_ERROR_POLICY_RULES}
 * </ul>
 
 * @return Value is either <code>0</code> or a combination of {@link android.service.euicc.EuiccService#RESOLVABLE_ERROR_CONFIRMATION_CODE}, and {@link android.service.euicc.EuiccService#RESOLVABLE_ERROR_POLICY_RULES}
 * @apiSince REL
 */

public int getResolvableErrors() { throw new RuntimeException("Stub!"); }

/**
 * Gets the card Id. This is used when resolving resolvable errors. The value is passed from
 * EuiccService.
 * @apiSince REL
 */

public int getCardId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.euicc.DownloadSubscriptionResult> CREATOR;
static { CREATOR = null; }
}

