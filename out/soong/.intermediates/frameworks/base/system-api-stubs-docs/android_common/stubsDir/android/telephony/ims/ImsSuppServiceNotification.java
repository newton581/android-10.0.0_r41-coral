/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */



package android.telephony.ims;

import android.os.Parcelable;

/**
 * Parcelable object to handle IMS supplementary service notifications.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ImsSuppServiceNotification implements android.os.Parcelable {

/** @apiSince REL */

public ImsSuppServiceNotification(int notificationType, int code, int index, int type, java.lang.String number, java.lang.String[] history) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.ims.ImsSuppServiceNotification> CREATOR;
static { CREATOR = null; }

/**
 * TS 27.007 7.17 "code1" or "code2"
 * @apiSince REL
 */

public final int code;
{ code = 0; }

/**
 * List of forwarded numbers, if any
 * @apiSince REL
 */

public final java.lang.String[] history;
{ history = new java.lang.String[0]; }

/**
 * TS 27.007 7.17 "index" - Not used currently
 * @apiSince REL
 */

public final int index;
{ index = 0; }

/**
 * Type of notification: 0 = MO; 1 = MT
 * @apiSince REL
 */

public final int notificationType;
{ notificationType = 0; }

/**
 * TS 27.007 7.17 "number" (MT only)
 * @apiSince REL
 */

public final java.lang.String number;
{ number = null; }

/**
 * TS 27.007 7.17 "type" (MT only) - Not used currently
 * @apiSince REL
 */

public final int type;
{ type = 0; }
}

