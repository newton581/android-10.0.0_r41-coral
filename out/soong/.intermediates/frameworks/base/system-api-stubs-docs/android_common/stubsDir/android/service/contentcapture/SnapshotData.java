/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.contentcapture;


/**
 * A container class for data taken from a snapshot of an activity.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class SnapshotData implements android.os.Parcelable {

SnapshotData(@android.annotation.NonNull android.os.Parcel parcel) { throw new RuntimeException("Stub!"); }

/**
 * Returns the assist data for this snapshot.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.Bundle getAssistData() { throw new RuntimeException("Stub!"); }

/**
 * Returns the assist structure for this snapshot.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.assist.AssistStructure getAssistStructure() { throw new RuntimeException("Stub!"); }

/**
 * Returns the assist context for this snapshot.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.app.assist.AssistContent getAssistContent() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param parcel This value must never be {@code null}.
 * @apiSince REL
 */

public void writeToParcel(@android.annotation.NonNull android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.contentcapture.SnapshotData> CREATOR;
static { CREATOR = null; }
}

