
package android.hardware.hdmi;


/**
 * Parent for classes of various HDMI-CEC device type used to access
 * the HDMI control system service. Contains methods and data used in common.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class HdmiClient {

HdmiClient() { throw new RuntimeException("Stub!"); }

/**
 * Returns the active source information.
 *
 * @return {@link HdmiDeviceInfo} object that describes the active source
 *         or active routing path
 * @apiSince REL
 */

public android.hardware.hdmi.HdmiDeviceInfo getActiveSource() { throw new RuntimeException("Stub!"); }

/**
 * Sends a key event to other logical device.
 *
 * @param keyCode key code to send. Defined in {@link android.view.KeyEvent}.
 * @param isPressed true if this is key press event
 * @apiSince REL
 */

public void sendKeyEvent(int keyCode, boolean isPressed) { throw new RuntimeException("Stub!"); }

/**
 * Sends vendor-specific command.
 *
 * @param targetAddress address of the target device
 * @param params vendor-specific parameter. For &lt;Vendor Command With ID&gt; do not
 *               include the first 3 bytes (vendor ID).
 * @param hasVendorId {@code true} if the command type will be &lt;Vendor Command With ID&gt;.
 *                    {@code false} if the command will be &lt;Vendor Command&gt;
 * @apiSince REL
 */

public void sendVendorCommand(int targetAddress, byte[] params, boolean hasVendorId) { throw new RuntimeException("Stub!"); }

/**
 * Sets a listener used to receive incoming vendor-specific command.
 *
 * @param listener listener object
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void setVendorCommandListener(@android.annotation.NonNull android.hardware.hdmi.HdmiControlManager.VendorCommandListener listener) { throw new RuntimeException("Stub!"); }
}

