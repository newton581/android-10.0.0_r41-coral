/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims;

import android.os.Parcelable;
import android.telecom.VideoProfile;
import android.os.Bundle;
import android.telephony.emergency.EmergencyNumber;

/**
 * Parcelable object to handle IMS call profile.
 * It is created from GSMA IR.92/IR.94, 3GPP TS 24.229/TS 26.114/TS26.111.
 * It provides the service and call type, the additional information related to the call.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ImsCallProfile implements android.os.Parcelable {

/**
 * Default Constructor that initializes the call profile with service type
 * {@link #SERVICE_TYPE_NORMAL} and call type {@link #CALL_TYPE_VIDEO_N_VOICE}
 * @apiSince REL
 */

public ImsCallProfile() { throw new RuntimeException("Stub!"); }

/**
 * Constructor.
 *
 * @param serviceType the service type for the call. Can be one of the following:
 *                    {@link #SERVICE_TYPE_NONE},
 *                    {@link #SERVICE_TYPE_NORMAL},
 *                    {@link #SERVICE_TYPE_EMERGENCY}
 * @param callType the call type. Can be one of the following:
 *                 {@link #CALL_TYPE_VOICE_N_VIDEO},
 *                 {@link #CALL_TYPE_VOICE},
 *                 {@link #CALL_TYPE_VIDEO_N_VOICE},
 *                 {@link #CALL_TYPE_VT},
 *                 {@link #CALL_TYPE_VT_TX},
 *                 {@link #CALL_TYPE_VT_RX},
 *                 {@link #CALL_TYPE_VT_NODIR},
 *                 {@link #CALL_TYPE_VS},
 *                 {@link #CALL_TYPE_VS_TX},
 *                 {@link #CALL_TYPE_VS_RX}
 * @apiSince REL
 */

public ImsCallProfile(int serviceType, int callType) { throw new RuntimeException("Stub!"); }

/**
 * Constructor.
 *
 * @param serviceType the service type for the call. Can be one of the following:
 *                    {@link #SERVICE_TYPE_NONE},
 *                    {@link #SERVICE_TYPE_NORMAL},
 *                    {@link #SERVICE_TYPE_EMERGENCY}
 * @param callType the call type. Can be one of the following:
 *                 {@link #CALL_TYPE_VOICE_N_VIDEO},
 *                 {@link #CALL_TYPE_VOICE},
 *                 {@link #CALL_TYPE_VIDEO_N_VOICE},
 *                 {@link #CALL_TYPE_VT},
 *                 {@link #CALL_TYPE_VT_TX},
 *                 {@link #CALL_TYPE_VT_RX},
 *                 {@link #CALL_TYPE_VT_NODIR},
 *                 {@link #CALL_TYPE_VS},
 *                 {@link #CALL_TYPE_VS_TX},
 *                 {@link #CALL_TYPE_VS_RX}
 * @param callExtras A bundle with the call extras.
 * @param mediaProfile The IMS stream media profile.
 * @apiSince REL
 */

public ImsCallProfile(int serviceType, int callType, android.os.Bundle callExtras, android.telephony.ims.ImsStreamMediaProfile mediaProfile) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getCallExtra(java.lang.String name) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getCallExtra(java.lang.String name, java.lang.String defaultValue) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean getCallExtraBoolean(java.lang.String name) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean getCallExtraBoolean(java.lang.String name, boolean defaultValue) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getCallExtraInt(java.lang.String name) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getCallExtraInt(java.lang.String name, int defaultValue) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setCallExtra(java.lang.String name, java.lang.String value) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setCallExtraBoolean(java.lang.String name, boolean value) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setCallExtraInt(java.lang.String name, int value) { throw new RuntimeException("Stub!"); }

/**
 * Set the call restrict cause, which provides the reason why a call has been restricted from
 * using High Definition media.
 
 * @param cause Value is {@link android.telephony.ims.ImsCallProfile#CALL_RESTRICT_CAUSE_NONE}, {@link android.telephony.ims.ImsCallProfile#CALL_RESTRICT_CAUSE_RAT}, {@link android.telephony.ims.ImsCallProfile#CALL_RESTRICT_CAUSE_DISABLED}, or {@link android.telephony.ims.ImsCallProfile#CALL_RESTRICT_CAUSE_HD}
 * @apiSince REL
 */

public void setCallRestrictCause(int cause) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void updateCallType(android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void updateCallExtras(android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * Updates the media profile for the call.
 *
 * @param profile Call profile with new media profile.
 * @apiSince REL
 */

public void updateMediaProfile(android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getServiceType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getCallType() { throw new RuntimeException("Stub!"); }

/**
 * @return The call restrict cause, which provides the reason why a call has been restricted
 * from using High Definition media.
 
 * Value is {@link android.telephony.ims.ImsCallProfile#CALL_RESTRICT_CAUSE_NONE}, {@link android.telephony.ims.ImsCallProfile#CALL_RESTRICT_CAUSE_RAT}, {@link android.telephony.ims.ImsCallProfile#CALL_RESTRICT_CAUSE_DISABLED}, or {@link android.telephony.ims.ImsCallProfile#CALL_RESTRICT_CAUSE_HD}
 * @apiSince REL
 */

public int getRestrictCause() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.Bundle getCallExtras() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.telephony.ims.ImsStreamMediaProfile getMediaProfile() { throw new RuntimeException("Stub!"); }

/**
 * Converts from the call types defined in {@link ImsCallProfile} to the
 * video state values defined in {@link VideoProfile}.
 *
 * @param callProfile The call profile.
 * @return The video state.
 * @apiSince REL
 */

public static int getVideoStateFromImsCallProfile(android.telephony.ims.ImsCallProfile callProfile) { throw new RuntimeException("Stub!"); }

/**
 * Translates a {@link ImsCallProfile} {@code CALL_TYPE_*} constant into a video state.
 * @param callType The call type.
 * @return The video state.
 * @apiSince REL
 */

public static int getVideoStateFromCallType(int callType) { throw new RuntimeException("Stub!"); }

/**
 * Converts from the video state values defined in {@link VideoProfile}
 * to the call types defined in {@link ImsCallProfile}.
 *
 * @param videoState The video state.
 * @return The call type.
 * @apiSince REL
 */

public static int getCallTypeFromVideoState(int videoState) { throw new RuntimeException("Stub!"); }

/**
 * Translate presentation value to OIR value
 * @param presentation
 * @return OIR values
 * @apiSince REL
 */

public static int presentationToOir(int presentation) { throw new RuntimeException("Stub!"); }

/**
 * Checks if video call is paused
 * @return true if call is video paused
 * @apiSince REL
 */

public boolean isVideoPaused() { throw new RuntimeException("Stub!"); }

/**
 * Determines if the {@link ImsCallProfile} represents a video call.
 *
 * @return {@code true} if the profile is for a video call, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isVideoCall() { throw new RuntimeException("Stub!"); }

/**
 * Set the emergency service categories. The set value is valid only if
 * {@link #getServiceType} returns {@link #SERVICE_TYPE_EMERGENCY}
 *
 * If valid, the value is the bitwise-OR combination of the following constants:
 * <ol>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_UNSPECIFIED} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_POLICE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AMBULANCE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_FIRE_BRIGADE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MARINE_GUARD} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MOUNTAIN_RESCUE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MIEC} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AIEC} </li>
 * </ol>
 *
 * Reference: 3gpp 23.167, Section 6 - Functional description;
 *            3gpp 22.101, Section 10 - Emergency Calls.
 
 * @param emergencyServiceCategories Value is either <code>0</code> or a combination of {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_UNSPECIFIED}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_POLICE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AMBULANCE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_FIRE_BRIGADE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MARINE_GUARD}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MOUNTAIN_RESCUE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MIEC}, and {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AIEC}
 * @apiSince REL
 */

public void setEmergencyServiceCategories(int emergencyServiceCategories) { throw new RuntimeException("Stub!"); }

/**
 * Set the emergency Uniform Resource Names (URN), only valid if {@link #getServiceType}
 * returns {@link #SERVICE_TYPE_EMERGENCY}.
 *
 * Reference: 3gpp 24.503, Section 5.1.6.8.1 - General;
 *            3gpp 22.101, Section 10 - Emergency Calls.
 
 * @param emergencyUrns This value must never be {@code null}.
 * @apiSince REL
 */

public void setEmergencyUrns(@android.annotation.NonNull java.util.List<java.lang.String> emergencyUrns) { throw new RuntimeException("Stub!"); }

/**
 * Set the emergency call routing, only valid if {@link #getServiceType} returns
 * {@link #SERVICE_TYPE_EMERGENCY}
 *
 * If valid, the value is any of the following constants:
 * <ol>
 * <li>{@link EmergencyNumber#EMERGENCY_CALL_ROUTING_UNKNOWN} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_CALL_ROUTING_NORMAL} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_CALL_ROUTING_EMERGENCY} </li>
 * </ol>
 
 * @param emergencyCallRouting Value is {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_CALL_ROUTING_UNKNOWN}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_CALL_ROUTING_EMERGENCY}, or {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_CALL_ROUTING_NORMAL}
 * @apiSince REL
 */

public void setEmergencyCallRouting(int emergencyCallRouting) { throw new RuntimeException("Stub!"); }

/**
 * Set if this is for testing emergency call, only valid if {@link #getServiceType} returns
 * {@link #SERVICE_TYPE_EMERGENCY}.
 * @apiSince REL
 */

public void setEmergencyCallTesting(boolean isTesting) { throw new RuntimeException("Stub!"); }

/**
 * Set if we have known the user intent of the call is emergency.
 *
 * This is only used to specify when the dialed number is ambiguous when it can be identified
 * as both emergency number and any other non-emergency number; e.g. in some situation, 611
 * could be both an emergency number in a country and a non-emergency number of a carrier's
 * customer service hotline.
 * @apiSince REL
 */

public void setHasKnownUserIntentEmergency(boolean hasKnownUserIntentEmergency) { throw new RuntimeException("Stub!"); }

/**
 * Get the emergency service categories, only valid if {@link #getServiceType} returns
 * {@link #SERVICE_TYPE_EMERGENCY}
 *
 * @return the emergency service categories,
 *
 * If valid, the value is the bitwise-OR combination of the following constants:
 * <ol>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_UNSPECIFIED} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_POLICE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AMBULANCE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_FIRE_BRIGADE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MARINE_GUARD} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MOUNTAIN_RESCUE} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MIEC} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AIEC} </li>
 * </ol>
 *
 * Reference: 3gpp 23.167, Section 6 - Functional description;
 *            3gpp 22.101, Section 10 - Emergency Calls.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_UNSPECIFIED}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_POLICE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AMBULANCE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_FIRE_BRIGADE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MARINE_GUARD}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MOUNTAIN_RESCUE}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_MIEC}, and {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_SERVICE_CATEGORY_AIEC}
 * @apiSince REL
 */

public int getEmergencyServiceCategories() { throw new RuntimeException("Stub!"); }

/**
 * Get the emergency Uniform Resource Names (URN), only valid if {@link #getServiceType}
 * returns {@link #SERVICE_TYPE_EMERGENCY}.
 *
 * Reference: 3gpp 24.503, Section 5.1.6.8.1 - General;
 *            3gpp 22.101, Section 10 - Emergency Calls.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<java.lang.String> getEmergencyUrns() { throw new RuntimeException("Stub!"); }

/**
 * Get the emergency call routing, only valid if {@link #getServiceType} returns
 * {@link #SERVICE_TYPE_EMERGENCY}
 *
 * If valid, the value is any of the following constants:
 * <ol>
 * <li>{@link EmergencyNumber#EMERGENCY_CALL_ROUTING_UNKNOWN} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_CALL_ROUTING_NORMAL} </li>
 * <li>{@link EmergencyNumber#EMERGENCY_CALL_ROUTING_EMERGENCY} </li>
 * </ol>
 
 * @return Value is {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_CALL_ROUTING_UNKNOWN}, {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_CALL_ROUTING_EMERGENCY}, or {@link android.telephony.emergency.EmergencyNumber#EMERGENCY_CALL_ROUTING_NORMAL}
 * @apiSince REL
 */

public int getEmergencyCallRouting() { throw new RuntimeException("Stub!"); }

/**
 * Get if the emergency call is for testing purpose.
 * @apiSince REL
 */

public boolean isEmergencyCallTesting() { throw new RuntimeException("Stub!"); }

/**
 * Checks if we have known the user intent of the call is emergency.
 *
 * This is only used to specify when the dialed number is ambiguous when it can be identified
 * as both emergency number and any other non-emergency number; e.g. in some situation, 611
 * could be both an emergency number in a country and a non-emergency number of a carrier's
 * customer service hotline.
 * @apiSince REL
 */

public boolean hasKnownUserIntentEmergency() { throw new RuntimeException("Stub!"); }

/**
 * The service has been disabled on the peer side.
 * @apiSince REL
 */

public static final int CALL_RESTRICT_CAUSE_DISABLED = 2; // 0x2

/**
 * High definition media is not currently supported.
 * @apiSince REL
 */

public static final int CALL_RESTRICT_CAUSE_HD = 3; // 0x3

/**
 * Call is not restricted on peer side and High Definition media is supported
 * @apiSince REL
 */

public static final int CALL_RESTRICT_CAUSE_NONE = 0; // 0x0

/**
 * High Definition media is not supported on the peer side due to the Radio Access Technology
 * (RAT) it is are connected to.
 * @apiSince REL
 */

public static final int CALL_RESTRICT_CAUSE_RAT = 1; // 0x1

/**
 * VT to support IR.92 & IR.94 (voice + video upgrade/downgrade)
 * @apiSince REL
 */

public static final int CALL_TYPE_VIDEO_N_VOICE = 3; // 0x3

/**
 * IR.92 (Voice only)
 * @apiSince REL
 */

public static final int CALL_TYPE_VOICE = 2; // 0x2

/**
 * IMSPhone to support IR.92 & IR.94 (voice + video upgrade/downgrade)
 * @apiSince REL
 */

public static final int CALL_TYPE_VOICE_N_VIDEO = 1; // 0x1

/**
 * VideoShare (video two way)
 * @apiSince REL
 */

public static final int CALL_TYPE_VS = 8; // 0x8

/**
 * VideoShare (video RX one way)
 * @apiSince REL
 */

public static final int CALL_TYPE_VS_RX = 10; // 0xa

/**
 * VideoShare (video TX one way)
 * @apiSince REL
 */

public static final int CALL_TYPE_VS_TX = 9; // 0x9

/**
 * Video Telephony (audio / video two way)
 * @apiSince REL
 */

public static final int CALL_TYPE_VT = 4; // 0x4

/**
 * Video Telephony (audio two way / video inactive)
 * @apiSince REL
 */

public static final int CALL_TYPE_VT_NODIR = 7; // 0x7

/**
 * Video Telephony (audio two way / video RX one way)
 * @apiSince REL
 */

public static final int CALL_TYPE_VT_RX = 6; // 0x6

/**
 * Video Telephony (audio two way / video TX one way)
 * @apiSince REL
 */

public static final int CALL_TYPE_VT_TX = 5; // 0x5

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.ims.ImsCallProfile> CREATOR;
static { CREATOR = null; }

/**
 * A default or normal normal call.
 * @apiSince REL
 */

public static final int DIALSTRING_NORMAL = 0; // 0x0

/**
 * Call for SIP-based user configuration
 * @apiSince REL
 */

public static final int DIALSTRING_SS_CONF = 1; // 0x1

/**
 * Call for USSD message
 * @apiSince REL
 */

public static final int DIALSTRING_USSD = 2; // 0x2

/** @apiSince REL */

public static final java.lang.String EXTRA_ADDITIONAL_CALL_INFO = "AdditionalCallInfo";

/**
 * String extra property
 *  Containing fields from the SIP INVITE message for an IMS call
 * @apiSince REL
 */

public static final java.lang.String EXTRA_ADDITIONAL_SIP_INVITE_FIELDS = "android.telephony.ims.extra.ADDITIONAL_SIP_INVITE_FIELDS";

/**
 * Extra key which the RIL can use to indicate the radio technology used for a call.
 * Valid values are:
 * {@link android.telephony.ServiceState#RIL_RADIO_TECHNOLOGY_LTE},
 * {@link android.telephony.ServiceState#RIL_RADIO_TECHNOLOGY_IWLAN}, and the other defined
 * {@code RIL_RADIO_TECHNOLOGY_*} constants.
 * Note: Despite the fact the {@link android.telephony.ServiceState} values are integer
 * constants, the values passed for the {@link #EXTRA_CALL_RAT_TYPE} should be strings (e.g.
 * "14" vs (int) 14).
 * Note: This is used by {@link com.android.internal.telephony.imsphone.ImsPhoneConnection#
 *      updateImsCallRatFromExtras(Bundle)} to determine whether to set the
 * {@link android.telecom.TelecomManager#EXTRA_CALL_NETWORK_TYPE} extra value and
 * {@link android.telecom.Connection#PROPERTY_WIFI} property on a connection.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_CALL_RAT_TYPE = "CallRadioTech";

/** @apiSince REL */

public static final java.lang.String EXTRA_CHILD_NUMBER = "ChildNum";

/** @apiSince REL */

public static final java.lang.String EXTRA_CNA = "cna";

/**
 * Rule for calling name presentation
 *      {@link ImsCallProfile#OIR_DEFAULT}
 *      {@link ImsCallProfile#OIR_PRESENTATION_RESTRICTED}
 *      {@link ImsCallProfile#OIR_PRESENTATION_NOT_RESTRICTED}
 * @apiSince REL
 */

public static final java.lang.String EXTRA_CNAP = "cnap";

/** @apiSince REL */

public static final java.lang.String EXTRA_CODEC = "Codec";

/**
 * To identify the Ims call type, MO
 *      {@link ImsCallProfile#DIALSTRING_NORMAL}
 *      {@link ImsCallProfile#DIALSTRING_SS_CONF}
 *      {@link ImsCallProfile#DIALSTRING_USSD}
 * @apiSince REL
 */

public static final java.lang.String EXTRA_DIALSTRING = "dialstring";

/** @apiSince REL */

public static final java.lang.String EXTRA_DISPLAY_TEXT = "DisplayText";

/**
 * Boolean extra property set on an {@link ImsCallProfile} to indicate that this call is an
 * emergency call.  The {@link ImsService} sets this on a call to indicate that the network has
 * identified the call as an emergency call.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_EMERGENCY_CALL = "e_call";

/** @apiSince REL */

public static final java.lang.String EXTRA_IS_CALL_PULL = "CallPull";

/**
 * String extra properties
 *  oi : Originating identity (number), MT only
 *  cna : Calling name
 *  ussd : For network-initiated USSD, MT only
 *  remote_uri : Connected user identity (it can be used for the conference)
 *  ChildNum: Child number info.
 *  Codec: Codec info.
 *  DisplayText: Display text for the call.
 *  AdditionalCallInfo: Additional call info.
 *  CallPull: Boolean value specifying if the call is a pulled call.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_OI = "oi";

/**
 * Rule for originating identity (number) presentation, MO/MT.
 *      {@link ImsCallProfile#OIR_DEFAULT}
 *      {@link ImsCallProfile#OIR_PRESENTATION_RESTRICTED}
 *      {@link ImsCallProfile#OIR_PRESENTATION_NOT_RESTRICTED}
 * @apiSince REL
 */

public static final java.lang.String EXTRA_OIR = "oir";

/** @apiSince REL */

public static final java.lang.String EXTRA_REMOTE_URI = "remote_uri";

/** @apiSince REL */

public static final java.lang.String EXTRA_USSD = "ussd";

/**
 * Default presentation for Originating Identity.
 * @apiSince REL
 */

public static final int OIR_DEFAULT = 0; // 0x0

/**
 * Not restricted presentation for Originating Identity.
 * @apiSince REL
 */

public static final int OIR_PRESENTATION_NOT_RESTRICTED = 2; // 0x2

/**
 * Payphone presentation for Originating Identity.
 * @apiSince REL
 */

public static final int OIR_PRESENTATION_PAYPHONE = 4; // 0x4

/**
 * Restricted presentation for Originating Identity.
 * @apiSince REL
 */

public static final int OIR_PRESENTATION_RESTRICTED = 1; // 0x1

/**
 * Presentation unknown for Originating Identity.
 * @apiSince REL
 */

public static final int OIR_PRESENTATION_UNKNOWN = 3; // 0x3

/**
 * It is for an emergency call.
 * @apiSince REL
 */

public static final int SERVICE_TYPE_EMERGENCY = 2; // 0x2

/**
 * It is for a special case. It helps that the application can make a call
 * without IMS connection (not registered).
 * In the moment of the call initiation, the device try to connect to the IMS network
 * and initiates the call.
 * @apiSince REL
 */

public static final int SERVICE_TYPE_NONE = 0; // 0x0

/**
 * It is a default type and can be selected when the device is connected to the IMS network.
 * @apiSince REL
 */

public static final int SERVICE_TYPE_NORMAL = 1; // 0x1
}

