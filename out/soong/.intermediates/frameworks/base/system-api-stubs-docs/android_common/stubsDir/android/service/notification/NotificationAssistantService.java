/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.notification;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.app.NotificationChannel;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;

/**
 * A service that helps the user manage notifications.
 * <p>
 * Only one notification assistant can be active at a time. Unlike notification listener services,
 * assistant services can additionally modify certain aspects about notifications
 * (see {@link Adjustment}) before they are posted.
 *<p>
 * A note about managed profiles: Unlike {@link NotificationListenerService listener services},
 * NotificationAssistantServices are allowed to run in managed profiles
 * (see {@link DevicePolicyManager#isManagedProfile(ComponentName)}), so they can access the
 * information they need to create good {@link Adjustment adjustments}. To maintain the contract
 * with {@link NotificationListenerService}, an assistant service will receive all of the
 * callbacks from {@link NotificationListenerService} for the current user, managed profiles of
 * that user, and ones that affect all users. However,
 * {@link #onNotificationEnqueued(StatusBarNotification)} will only be called for notifications
 * sent to the current user, and {@link Adjustment adjuments} will only be accepted for the
 * current user.
 * <p>
 *     All callbacks are called on the main thread.
 * </p>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class NotificationAssistantService extends android.service.notification.NotificationListenerService {

public NotificationAssistantService() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

protected void attachBaseContext(android.content.Context base) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param intent This value may be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public final android.os.IBinder onBind(@android.annotation.Nullable android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * A notification was snoozed until a context. For use with
 * {@link Adjustment#KEY_SNOOZE_CRITERIA}. When the device reaches the given context, the
 * assistant should restore the notification with {@link #unsnoozeNotification(String)}.
 *
 * @param sbn the notification to snooze
 * This value must never be {@code null}.
 * @param snoozeCriterionId the {@link SnoozeCriterion#getId()} representing a device context.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onNotificationSnoozedUntilContext(@android.annotation.NonNull android.service.notification.StatusBarNotification sbn, @android.annotation.NonNull java.lang.String snoozeCriterionId);

/**
 * A notification was posted by an app. Called before post.
 *
 * <p>Note: this method is only called if you don't override
 * {@link #onNotificationEnqueued(StatusBarNotification, NotificationChannel)}.</p>
 *
 * @param sbn the new notification
 * This value must never be {@code null}.
 * @return an adjustment or null to take no action, within 100ms.
 * @apiSince REL
 */

@android.annotation.Nullable
public abstract android.service.notification.Adjustment onNotificationEnqueued(@android.annotation.NonNull android.service.notification.StatusBarNotification sbn);

/**
 * A notification was posted by an app. Called before post.
 *
 * @param sbn the new notification
 * This value must never be {@code null}.
 * @param channel the channel the notification was posted to
 * This value must never be {@code null}.
 * @return an adjustment or null to take no action, within 100ms.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.service.notification.Adjustment onNotificationEnqueued(@android.annotation.NonNull android.service.notification.StatusBarNotification sbn, @android.annotation.NonNull android.app.NotificationChannel channel) { throw new RuntimeException("Stub!"); }

/**
 * Implement this method to learn when notifications are removed, how they were interacted with
 * before removal, and why they were removed.
 * <p>
 * This might occur because the user has dismissed the notification using system UI (or another
 * notification listener) or because the app has withdrawn the notification.
 * <p>
 * NOTE: The {@link StatusBarNotification} object you receive will be "light"; that is, the
 * result from {@link StatusBarNotification#getNotification} may be missing some heavyweight
 * fields such as {@link android.app.Notification#contentView} and
 * {@link android.app.Notification#largeIcon}. However, all other fields on
 * {@link StatusBarNotification}, sufficient to match this call with a prior call to
 * {@link #onNotificationPosted(StatusBarNotification)}, will be intact.
 *
 ** @param sbn A data structure encapsulating at least the original information (tag and id)
 *            and source (package name) used to post the {@link android.app.Notification} that
 *            was just removed.
 * This value must never be {@code null}.
 * @param rankingMap The current ranking map that can be used to retrieve ranking information
 *                   for active notifications.
 * This value must never be {@code null}.
 * @param stats Stats about how the user interacted with the notification before it was removed.
 * This value must never be {@code null}.
 * @param reason see {@link #REASON_LISTENER_CANCEL}, etc.
 * @apiSince REL
 */

public void onNotificationRemoved(@android.annotation.NonNull android.service.notification.StatusBarNotification sbn, @android.annotation.NonNull android.service.notification.NotificationListenerService.RankingMap rankingMap, @android.annotation.NonNull android.service.notification.NotificationStats stats, int reason) { throw new RuntimeException("Stub!"); }

/**
 * Implement this to know when a user has seen notifications, as triggered by
 * {@link #setNotificationsShown(String[])}.
 
 * @param keys This value must never be {@code null}.
 * @apiSince REL
 */

public void onNotificationsSeen(@android.annotation.NonNull java.util.List<java.lang.String> keys) { throw new RuntimeException("Stub!"); }

/**
 * Implement this to know when a notification change (expanded / collapsed) is visible to user.
 *
 * @param key the notification key
 * This value must never be {@code null}.
 * @param isUserAction whether the expanded change is caused by user action.
 * @param isExpanded whether the notification is expanded.
 * @apiSince REL
 */

public void onNotificationExpansionChanged(@android.annotation.NonNull java.lang.String key, boolean isUserAction, boolean isExpanded) { throw new RuntimeException("Stub!"); }

/**
 * Implement this to know when a direct reply is sent from a notification.
 * @param key the notification key
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onNotificationDirectReplied(@android.annotation.NonNull java.lang.String key) { throw new RuntimeException("Stub!"); }

/**
 * Implement this to know when a suggested reply is sent.
 * @param key the notification key
 * This value must never be {@code null}.
 * @param reply the reply that is just sent
 * This value must never be {@code null}.
 * @param source the source that provided the reply, e.g. SOURCE_FROM_APP
 
 * Value is {@link android.service.notification.NotificationAssistantService#SOURCE_FROM_APP}, or {@link android.service.notification.NotificationAssistantService#SOURCE_FROM_ASSISTANT}
 * @apiSince REL
 */

public void onSuggestedReplySent(@android.annotation.NonNull java.lang.String key, @android.annotation.NonNull java.lang.CharSequence reply, int source) { throw new RuntimeException("Stub!"); }

/**
 * Implement this to know when an action is clicked.
 * @param key the notification key
 * This value must never be {@code null}.
 * @param action the action that is just clicked
 * This value must never be {@code null}.
 * @param source the source that provided the action, e.g. SOURCE_FROM_APP
 
 * Value is {@link android.service.notification.NotificationAssistantService#SOURCE_FROM_APP}, or {@link android.service.notification.NotificationAssistantService#SOURCE_FROM_ASSISTANT}
 * @apiSince REL
 */

public void onActionInvoked(@android.annotation.NonNull java.lang.String key, @android.annotation.NonNull android.app.Notification.Action action, int source) { throw new RuntimeException("Stub!"); }

/**
 * Implement this to know when a user has changed which features of
 * their notifications the assistant can modify.
 * <p> Query {@link NotificationManager#getAllowedAssistantAdjustments()} to see what
 * {@link Adjustment adjustments} you are currently allowed to make.</p>
 * @apiSince REL
 */

public void onAllowedAdjustmentsChanged() { throw new RuntimeException("Stub!"); }

/**
 * Updates a notification.  N.B. this won’t cause
 * an existing notification to alert, but might allow a future update to
 * this notification to alert.
 *
 * @param adjustment the adjustment with an explanation
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public final void adjustNotification(@android.annotation.NonNull android.service.notification.Adjustment adjustment) { throw new RuntimeException("Stub!"); }

/**
 * Updates existing notifications. Re-ranking won't occur until all adjustments are applied.
 * N.B. this won’t cause an existing notification to alert, but might allow a future update to
 * these notifications to alert.
 *
 * @param adjustments a list of adjustments with explanations
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public final void adjustNotifications(@android.annotation.NonNull java.util.List<android.service.notification.Adjustment> adjustments) { throw new RuntimeException("Stub!"); }

/**
 * Inform the notification manager about un-snoozing a specific notification.
 * <p>
 * This should only be used for notifications snoozed because of a contextual snooze suggestion
 * you provided via {@link Adjustment#KEY_SNOOZE_CRITERIA}. Once un-snoozed, you will get a
 * {@link #onNotificationPosted(StatusBarNotification, RankingMap)} callback for the
 * notification.
 * @param key The key of the notification to snooze
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public final void unsnoozeNotification(@android.annotation.NonNull java.lang.String key) { throw new RuntimeException("Stub!"); }

/**
 * The {@link Intent} that must be declared as handled by the service.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.service.notification.NotificationAssistantService";

/**
 * To indicate an adjustment is from an app.
 * @apiSince REL
 */

public static final int SOURCE_FROM_APP = 0; // 0x0

/**
 * To indicate an adjustment is from a {@link NotificationAssistantService}.
 * @apiSince REL
 */

public static final int SOURCE_FROM_ASSISTANT = 1; // 0x1
}

