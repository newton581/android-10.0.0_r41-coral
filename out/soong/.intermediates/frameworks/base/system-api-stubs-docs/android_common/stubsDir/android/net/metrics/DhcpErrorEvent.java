/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.metrics;


/**
 * Event class used to record error events when parsing DHCP response packets.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class DhcpErrorEvent implements android.net.metrics.IpConnectivityLog.Event {

/** @apiSince REL */

public DhcpErrorEvent(int errorCode) { throw new RuntimeException("Stub!"); }

/** @hide */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @hide */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static int errorCodeWithOption(int errorCode, int option) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int BOOTP_TOO_SHORT = 67174400; // 0x4010000

/** @apiSince REL */

public static final int BUFFER_UNDERFLOW = 83951616; // 0x5010000

/** @apiSince REL */

public static final int DHCP_BAD_MAGIC_COOKIE = 67239936; // 0x4020000

/** @apiSince REL */

public static final int DHCP_ERROR = 4; // 0x4

/** @apiSince REL */

public static final int DHCP_INVALID_OPTION_LENGTH = 67305472; // 0x4030000

/** @apiSince REL */

public static final int DHCP_NO_COOKIE = 67502080; // 0x4060000

/** @apiSince REL */

public static final int DHCP_NO_MSG_TYPE = 67371008; // 0x4040000

/** @apiSince REL */

public static final int DHCP_UNKNOWN_MSG_TYPE = 67436544; // 0x4050000

/** @apiSince REL */

public static final int L2_ERROR = 1; // 0x1

/** @apiSince REL */

public static final int L2_TOO_SHORT = 16842752; // 0x1010000

/** @apiSince REL */

public static final int L2_WRONG_ETH_TYPE = 16908288; // 0x1020000

/** @apiSince REL */

public static final int L3_ERROR = 2; // 0x2

/** @apiSince REL */

public static final int L3_INVALID_IP = 33751040; // 0x2030000

/** @apiSince REL */

public static final int L3_NOT_IPV4 = 33685504; // 0x2020000

/** @apiSince REL */

public static final int L3_TOO_SHORT = 33619968; // 0x2010000

/** @apiSince REL */

public static final int L4_ERROR = 3; // 0x3

/** @apiSince REL */

public static final int L4_NOT_UDP = 50397184; // 0x3010000

/** @apiSince REL */

public static final int L4_WRONG_PORT = 50462720; // 0x3020000

/** @apiSince REL */

public static final int MISC_ERROR = 5; // 0x5

/** @apiSince REL */

public static final int PARSING_ERROR = 84082688; // 0x5030000

/** @apiSince REL */

public static final int RECEIVE_ERROR = 84017152; // 0x5020000
}

