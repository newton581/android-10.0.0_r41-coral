/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.audiopolicy;

import android.media.AudioManager;
import android.media.AudioAttributes;
import android.media.AudioDeviceInfo;
import java.util.List;
import android.content.pm.PackageManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.os.Looper;
import android.media.projection.MediaProjection;
import android.media.AudioFocusInfo;

/**
 * @hide
 * AudioPolicy provides access to the management of audio routing and audio focus.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AudioPolicy {

AudioPolicy() { throw new RuntimeException("Stub!"); }

/**
 * Update the current configuration of the set of audio mixes by adding new ones, while
 * keeping the policy registered.
 * This method can only be called on a registered policy.
 * @param mixes the list of {@link AudioMix} to add
 * This value must never be {@code null}.
 * @return {@link AudioManager#SUCCESS} if the change was successful, {@link AudioManager#ERROR}
 *    otherwise.
 * @apiSince REL
 */

public int attachMixes(@android.annotation.NonNull java.util.List<android.media.audiopolicy.AudioMix> mixes) { throw new RuntimeException("Stub!"); }

/**
 * Update the current configuration of the set of audio mixes by removing some, while
 * keeping the policy registered.
 * This method can only be called on a registered policy.
 * @param mixes the list of {@link AudioMix} to remove
 * This value must never be {@code null}.
 * @return {@link AudioManager#SUCCESS} if the change was successful, {@link AudioManager#ERROR}
 *    otherwise.
 * @apiSince REL
 */

public int detachMixes(@android.annotation.NonNull java.util.List<android.media.audiopolicy.AudioMix> mixes) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * Configures the audio framework so that all audio stream originating from the given UID
 * can only come from a set of audio devices.
 * For this routing to be operational, a number of {@link AudioMix} instances must have been
 * previously registered on this policy, and routed to a super-set of the given audio devices
 * with {@link AudioMix.Builder#setDevice(android.media.AudioDeviceInfo)}. Note that having
 * multiple devices in the list doesn't imply the signals will be duplicated on the different
 * audio devices, final routing will depend on the {@link AudioAttributes} of the sounds being
 * played.
 * @param uid UID of the application to affect.
 * @param devices list of devices to which the audio stream of the application may be routed.
 * This value must never be {@code null}.
 * @return true if the change was successful, false otherwise.
 */

public boolean setUidDeviceAffinity(int uid, @android.annotation.NonNull java.util.List<android.media.AudioDeviceInfo> devices) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * Removes audio device affinity previously set by
 * {@link #setUidDeviceAffinity(int, java.util.List)}.
 * @param uid UID of the application affected.
 * @return true if the change was successful, false otherwise.
 */

public boolean removeUidDeviceAffinity(int uid) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void setRegistration(java.lang.String regId) { throw new RuntimeException("Stub!"); }

/**
 * Returns the current behavior for audio focus-related ducking.
 * @return {@link #FOCUS_POLICY_DUCKING_IN_APP} or {@link #FOCUS_POLICY_DUCKING_IN_POLICY}
 * @apiSince REL
 */

public int getFocusDuckingBehavior() { throw new RuntimeException("Stub!"); }

/**
 * Sets the behavior for audio focus-related ducking.
 * There must be a focus listener if this policy is to handle ducking.
 * @param behavior {@link #FOCUS_POLICY_DUCKING_IN_APP} or
 *     {@link #FOCUS_POLICY_DUCKING_IN_POLICY}
 * @return {@link AudioManager#SUCCESS} or {@link AudioManager#ERROR} (for instance if there
 *     is already an audio policy that handles ducking).
 * @throws IllegalArgumentException
 * @throws IllegalStateException
 * @apiSince REL
 */

public int setFocusDuckingBehavior(int behavior) throws java.lang.IllegalArgumentException, java.lang.IllegalStateException { throw new RuntimeException("Stub!"); }

/**
 * Create an {@link AudioRecord} instance that is associated with the given {@link AudioMix}.
 * Audio buffers recorded through the created instance will contain the mix of the audio
 * streams that fed the given mixer.
 * @param mix a non-null {@link AudioMix} instance whose routing flags was defined with
 *     {@link AudioMix#ROUTE_FLAG_LOOP_BACK}, previously added to this policy.
 * @return a new {@link AudioRecord} instance whose data format is the one defined in the
 *     {@link AudioMix}, or null if this policy was not successfully registered
 *     with {@link AudioManager#registerAudioPolicy(AudioPolicy)}.
 * @throws IllegalArgumentException
 * @apiSince REL
 */

public android.media.AudioRecord createAudioRecordSink(android.media.audiopolicy.AudioMix mix) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Create an {@link AudioTrack} instance that is associated with the given {@link AudioMix}.
 * Audio buffers played through the created instance will be sent to the given mix
 * to be recorded through the recording APIs.
 * @param mix a non-null {@link AudioMix} instance whose routing flags was defined with
 *     {@link AudioMix#ROUTE_FLAG_LOOP_BACK}, previously added to this policy.
 * @return a new {@link AudioTrack} instance whose data format is the one defined in the
 *     {@link AudioMix}, or null if this policy was not successfully registered
 *     with {@link AudioManager#registerAudioPolicy(AudioPolicy)}.
 * @throws IllegalArgumentException
 * @apiSince REL
 */

public android.media.AudioTrack createAudioTrackSource(android.media.audiopolicy.AudioMix mix) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getStatus() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toLogFriendlyString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int FOCUS_POLICY_DUCKING_DEFAULT = 0; // 0x0

/**
 * The behavior of a policy with regards to audio focus where it relies on the application
 * to do the ducking, the is the legacy and default behavior.
 * @apiSince REL
 */

public static final int FOCUS_POLICY_DUCKING_IN_APP = 0; // 0x0

/**
 * The behavior of a policy with regards to audio focus where it handles ducking instead
 * of the application losing focus and being signaled it can duck (as communicated by
 * {@link android.media.AudioManager#AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK}).
 * <br>Can only be used after having set a listener with
 * {@link AudioPolicy#setAudioPolicyFocusListener(AudioPolicyFocusListener)}.
 * @apiSince REL
 */

public static final int FOCUS_POLICY_DUCKING_IN_POLICY = 1; // 0x1

/**
 * The status of an audio policy that is valid, successfully registered and thus active.
 * @apiSince REL
 */

public static final int POLICY_STATUS_REGISTERED = 2; // 0x2

/**
 * The status of an audio policy that is valid but cannot be used because it is not registered.
 * @apiSince REL
 */

public static final int POLICY_STATUS_UNREGISTERED = 1; // 0x1
/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class AudioPolicyFocusListener {

public AudioPolicyFocusListener() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onAudioFocusGrant(android.media.AudioFocusInfo afi, int requestResult) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onAudioFocusLoss(android.media.AudioFocusInfo afi, boolean wasNotified) { throw new RuntimeException("Stub!"); }

/**
 * Called whenever an application requests audio focus.
 * Only ever called if the {@link AudioPolicy} was built with
 * {@link AudioPolicy.Builder#setIsAudioFocusPolicy(boolean)} set to {@code true}.
 * @param afi information about the focus request and the requester
 * @param requestResult deprecated after the addition of
 *     {@link AudioManager#setFocusRequestResult(AudioFocusInfo, int, AudioPolicy)}
 *     in Android P, always equal to {@link #AUDIOFOCUS_REQUEST_GRANTED}.
 * @apiSince REL
 */

public void onAudioFocusRequest(android.media.AudioFocusInfo afi, int requestResult) { throw new RuntimeException("Stub!"); }

/**
 * Called whenever an application abandons audio focus.
 * Only ever called if the {@link AudioPolicy} was built with
 * {@link AudioPolicy.Builder#setIsAudioFocusPolicy(boolean)} set to {@code true}.
 * @param afi information about the focus request being abandoned and the original
 *     requester.
 * @apiSince REL
 */

public void onAudioFocusAbandon(android.media.AudioFocusInfo afi) { throw new RuntimeException("Stub!"); }
}

/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class AudioPolicyStatusListener {

public AudioPolicyStatusListener() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onStatusChange() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onMixStateUpdate(android.media.audiopolicy.AudioMix mix) { throw new RuntimeException("Stub!"); }
}

/**
 * Callback class to receive volume change-related events.
 * See {@link #Builder.setAudioPolicyVolumeCallback(AudioPolicyCallback)} to configure the
 * {@link AudioPolicy} to receive those events.
 *
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class AudioPolicyVolumeCallback {

/** @apiSince REL */

public AudioPolicyVolumeCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called when volume key-related changes are triggered, on the key down event.
 * @param adjustment the type of volume adjustment for the key.
 
 * Value is {@link android.media.AudioManager#ADJUST_RAISE}, {@link android.media.AudioManager#ADJUST_LOWER}, {@link android.media.AudioManager#ADJUST_SAME}, {@link android.media.AudioManager#ADJUST_MUTE}, {@link android.media.AudioManager#ADJUST_UNMUTE}, or {@link android.media.AudioManager#ADJUST_TOGGLE_MUTE}
 * @apiSince REL
 */

public void onVolumeAdjustment(int adjustment) { throw new RuntimeException("Stub!"); }
}

/**
 * Builder class for {@link AudioPolicy} objects.
 * By default the policy to be created doesn't govern audio focus decisions.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

/**
 * Constructs a new Builder with no audio mixes.
 * @param context the context for the policy
 * @apiSince REL
 */

public Builder(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Add an {@link AudioMix} to be part of the audio policy being built.
 * @param mix a non-null {@link AudioMix} to be part of the audio policy.
 * This value must never be {@code null}.
 * @return the same Builder instance.
 * @throws IllegalArgumentException
 * @apiSince REL
 */

@android.annotation.NonNull
public android.media.audiopolicy.AudioPolicy.Builder addMix(@android.annotation.NonNull android.media.audiopolicy.AudioMix mix) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Sets the {@link Looper} on which to run the event loop.
 * @param looper a non-null specific Looper.
 * This value must never be {@code null}.
 * @return the same Builder instance.
 * @throws IllegalArgumentException
 * @apiSince REL
 */

@android.annotation.NonNull
public android.media.audiopolicy.AudioPolicy.Builder setLooper(@android.annotation.NonNull android.os.Looper looper) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Sets the audio focus listener for the policy.
 * @param l a {@link AudioPolicy.AudioPolicyFocusListener}
 * @apiSince REL
 */

public void setAudioPolicyFocusListener(android.media.audiopolicy.AudioPolicy.AudioPolicyFocusListener l) { throw new RuntimeException("Stub!"); }

/**
 * Declares whether this policy will grant and deny audio focus through
 * the {@link AudioPolicy.AudioPolicyFocusListener}.
 * If set to {@code true}, it is mandatory to set an
 * {@link AudioPolicy.AudioPolicyFocusListener} in order to successfully build
 * an {@code AudioPolicy} instance.
 * @param enforce true if the policy will govern audio focus decisions.
 * @return the same Builder instance.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.media.audiopolicy.AudioPolicy.Builder setIsAudioFocusPolicy(boolean isFocusPolicy) { throw new RuntimeException("Stub!"); }

/**
 * Sets the audio policy status listener.
 * @param l a {@link AudioPolicy.AudioPolicyStatusListener}
 * @apiSince REL
 */

public void setAudioPolicyStatusListener(android.media.audiopolicy.AudioPolicy.AudioPolicyStatusListener l) { throw new RuntimeException("Stub!"); }

/**
 * Sets the callback to receive all volume key-related events.
 * The callback will only be called if the device is configured to handle volume events
 * in the PhoneWindowManager (see config_handleVolumeKeysInWindowManager)
 * @param vc
 * This value must never be {@code null}.
 * @return the same Builder instance.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.media.audiopolicy.AudioPolicy.Builder setAudioPolicyVolumeCallback(@android.annotation.NonNull android.media.audiopolicy.AudioPolicy.AudioPolicyVolumeCallback vc) { throw new RuntimeException("Stub!"); }

/**
 * Combines all of the attributes that have been set on this {@code Builder} and returns a
 * new {@link AudioPolicy} object.
 * @return a new {@code AudioPolicy} object.
 * This value will never be {@code null}.
 * @throws IllegalStateException if there is no
 *     {@link AudioPolicy.AudioPolicyStatusListener} but the policy was configured
 *     as an audio focus policy with {@link #setIsAudioFocusPolicy(boolean)}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.media.audiopolicy.AudioPolicy build() { throw new RuntimeException("Stub!"); }
}

}

