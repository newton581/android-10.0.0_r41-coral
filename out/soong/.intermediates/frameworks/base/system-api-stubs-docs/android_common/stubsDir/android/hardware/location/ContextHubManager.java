/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.location;

import android.content.Context;
import java.util.concurrent.Executor;
import android.os.Looper;
import android.os.Handler;
import android.content.Intent;
import android.app.PendingIntent;

/**
 * A class that exposes the Context hubs on a device to applications.
 *
 * Please note that this class is not expected to be used by unbundled applications. Also, calling
 * applications are expected to have LOCATION_HARDWARE permissions to use this class.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ContextHubManager {

ContextHubManager() { throw new RuntimeException("Stub!"); }

/**
 * Get a handle to all the context hubs in the system
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @return array of context hub handles
 *
 * @deprecated Use {@link #getContextHubs()} instead. The use of handles are deprecated in the
 *             new APIs.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int[] getContextHubHandles() { throw new RuntimeException("Stub!"); }

/**
 * Get more information about a specific hub.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubHandle Handle (system-wide unique identifier) of a context hub.
 * @return ContextHubInfo Information about the requested context hub.
 *
 * @see ContextHubInfo
 *
 * @deprecated Use {@link #getContextHubs()} instead. The use of handles are deprecated in the
 *             new APIs.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public android.hardware.location.ContextHubInfo getContextHubInfo(int hubHandle) { throw new RuntimeException("Stub!"); }

/**
 * Load a nano app on a specified context hub.
 *
 * Note that loading is asynchronous.  When we return from this method,
 * the nano app (probably) hasn't loaded yet.  Assuming a return of 0
 * from this method, then the final success/failure for the load, along
 * with the "handle" for the nanoapp, is all delivered in a byte
 * string via a call to Callback.onMessageReceipt.
 *
 * TODO(b/30784270): Provide a better success/failure and "handle" delivery.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubHandle handle of context hub to load the app on.
 * @param app the nanoApp to load on the hub
 *
 * This value must never be {@code null}.
 * @return 0 if the command for loading was sent to the context hub;
 *         -1 otherwise
 *
 * @see NanoApp
 *
 * @deprecated Use {@link #loadNanoApp(ContextHubInfo, NanoAppBinary)} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int loadNanoApp(int hubHandle, @android.annotation.NonNull android.hardware.location.NanoApp app) { throw new RuntimeException("Stub!"); }

/**
 * Unload a specified nanoApp
 *
 * Note that unloading is asynchronous.  When we return from this method,
 * the nano app (probably) hasn't unloaded yet.  Assuming a return of 0
 * from this method, then the final success/failure for the unload is
 * delivered in a byte string via a call to Callback.onMessageReceipt.
 *
 * TODO(b/30784270): Provide a better success/failure delivery.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param nanoAppHandle handle of the nanoApp to unload
 *
 * @return 0 if the command for unloading was sent to the context hub;
 *         -1 otherwise
 *
 * @deprecated Use {@link #unloadNanoApp(ContextHubInfo, long)} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int unloadNanoApp(int nanoAppHandle) { throw new RuntimeException("Stub!"); }

/**
 * get information about the nano app instance
 *
 * NOTE: The returned NanoAppInstanceInfo does _not_ contain correct
 * information for several fields, specifically:
 * - getName()
 * - getPublisher()
 * - getNeededExecMemBytes()
 * - getNeededReadMemBytes()
 * - getNeededWriteMemBytes()
 *
 * For example, say you call loadNanoApp() with a NanoApp that has
 * getName() returning "My Name".  Later, if you call getNanoAppInstanceInfo
 * for that nanoapp, the returned NanoAppInstanceInfo's getName()
 * method will claim "Preloaded app, unknown", even though you would
 * have expected "My Name".  For now, as the user, you'll need to
 * separately track the above fields if they are of interest to you.
 *
 * TODO(b/30943489): Have the returned NanoAppInstanceInfo contain the
 *     correct information.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param nanoAppHandle handle of the nanoapp instance
 * @return NanoAppInstanceInfo the NanoAppInstanceInfo of the nanoapp, or null if the nanoapp
 *                             does not exist
 *
 * @see NanoAppInstanceInfo
 *
 * @deprecated Use {@link #queryNanoApps(ContextHubInfo)} instead to explicitly query the hub
 *             for loaded nanoapps.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
@android.annotation.Nullable
public android.hardware.location.NanoAppInstanceInfo getNanoAppInstanceInfo(int nanoAppHandle) { throw new RuntimeException("Stub!"); }

/**
 * Find a specified nano app on the system
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubHandle handle of hub to search for nano app
 * @param filter filter specifying the search criteria for app
 *
 * This value must never be {@code null}.
 * @see NanoAppFilter
 *
 * @return int[] Array of handles to any found nano apps
 *
 * This value will never be {@code null}.
 * @deprecated Use {@link #queryNanoApps(ContextHubInfo)} instead to explicitly query the hub
 *             for loaded nanoapps.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
@android.annotation.NonNull
public int[] findNanoAppOnHub(int hubHandle, @android.annotation.NonNull android.hardware.location.NanoAppFilter filter) { throw new RuntimeException("Stub!"); }

/**
 * Send a message to a specific nano app instance on a context hub.
 *
 * Note that the return value of this method only speaks of success
 * up to the point of sending this to the Context Hub.  It is not
 * an assurance that the Context Hub successfully sent this message
 * on to the nanoapp.  If assurance is desired, a protocol should be
 * established between your code and the nanoapp, with the nanoapp
 * sending a confirmation message (which will be reported via
 * Callback.onMessageReceipt).
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubHandle handle of the hub to send the message to
 * @param nanoAppHandle  handle of the nano app to send to
 * @param message Message to be sent
 *
 * This value must never be {@code null}.
 * @see ContextHubMessage
 *
 * @return int 0 on success, -1 otherwise
 *
 * @deprecated Use {@link android.hardware.location.ContextHubClient#sendMessageToNanoApp(
 *             NanoAppMessage)} instead, after creating a
 *             {@link android.hardware.location.ContextHubClient} with
 *             {@link #createClient(ContextHubInfo, ContextHubClientCallback, Executor)}
 *             or {@link #createClient(ContextHubInfo, ContextHubClientCallback)}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int sendMessage(int hubHandle, int nanoAppHandle, @android.annotation.NonNull android.hardware.location.ContextHubMessage message) { throw new RuntimeException("Stub!"); }

/**
 * Returns the list of ContextHubInfo objects describing the available Context Hubs.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @return the list of ContextHubInfo objects
 *
 * This value will never be {@code null}.
 * @see ContextHubInfo
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.hardware.location.ContextHubInfo> getContextHubs() { throw new RuntimeException("Stub!"); }

/**
 * Loads a nanoapp at the specified Context Hub.
 *
 * After the nanoapp binary is successfully loaded at the specified hub, the nanoapp will be in
 * the enabled state.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubInfo the hub to load the nanoapp on
 * This value must never be {@code null}.
 * @param appBinary The app binary to load
 *
 * This value must never be {@code null}.
 * @return the ContextHubTransaction of the request
 *
 * @throws NullPointerException if hubInfo or NanoAppBinary is null
 *
 * @see NanoAppBinary
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.ContextHubTransaction<java.lang.Void> loadNanoApp(@android.annotation.NonNull android.hardware.location.ContextHubInfo hubInfo, @android.annotation.NonNull android.hardware.location.NanoAppBinary appBinary) { throw new RuntimeException("Stub!"); }

/**
 * Unloads a nanoapp at the specified Context Hub.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubInfo the hub to unload the nanoapp from
 * This value must never be {@code null}.
 * @param nanoAppId the app to unload
 *
 * @return the ContextHubTransaction of the request
 *
 * @throws NullPointerException if hubInfo is null
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.ContextHubTransaction<java.lang.Void> unloadNanoApp(@android.annotation.NonNull android.hardware.location.ContextHubInfo hubInfo, long nanoAppId) { throw new RuntimeException("Stub!"); }

/**
 * Enables a nanoapp at the specified Context Hub.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubInfo the hub to enable the nanoapp on
 * This value must never be {@code null}.
 * @param nanoAppId the app to enable
 *
 * @return the ContextHubTransaction of the request
 *
 * @throws NullPointerException if hubInfo is null
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.ContextHubTransaction<java.lang.Void> enableNanoApp(@android.annotation.NonNull android.hardware.location.ContextHubInfo hubInfo, long nanoAppId) { throw new RuntimeException("Stub!"); }

/**
 * Disables a nanoapp at the specified Context Hub.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubInfo the hub to disable the nanoapp on
 * This value must never be {@code null}.
 * @param nanoAppId the app to disable
 *
 * @return the ContextHubTransaction of the request
 *
 * @throws NullPointerException if hubInfo is null
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.ContextHubTransaction<java.lang.Void> disableNanoApp(@android.annotation.NonNull android.hardware.location.ContextHubInfo hubInfo, long nanoAppId) { throw new RuntimeException("Stub!"); }

/**
 * Requests a query for nanoapps loaded at the specified Context Hub.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubInfo the hub to query a list of nanoapps from
 *
 * This value must never be {@code null}.
 * @return the ContextHubTransaction of the request
 *
 * @throws NullPointerException if hubInfo is null
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.ContextHubTransaction<java.util.List<android.hardware.location.NanoAppState>> queryNanoApps(@android.annotation.NonNull android.hardware.location.ContextHubInfo hubInfo) { throw new RuntimeException("Stub!"); }

/**
 * Set a callback to receive messages from the context hub
 *
 * @param callback Callback object
 *
 * This value must never be {@code null}.
 * @see Callback
 *
 * @return int 0 on success, -1 otherwise
 *
 * @deprecated Use {@link #createClient(ContextHubInfo, ContextHubClientCallback, Executor)}
 *             or {@link #createClient(ContextHubInfo, ContextHubClientCallback)} instead to
 *             register a {@link android.hardware.location.ContextHubClientCallback}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int registerCallback(@android.annotation.NonNull android.hardware.location.ContextHubManager.Callback callback) { throw new RuntimeException("Stub!"); }

/**
 * Set a callback to receive messages from the context hub
 *
 * @param callback Callback object
 * @param handler Handler object, if null uses the Handler of the main Looper
 *
 * @see Callback
 *
 * @return int 0 on success, -1 otherwise
 *
 * @deprecated Use {@link #createClient(ContextHubInfo, ContextHubClientCallback, Executor)}
 *             or {@link #createClient(ContextHubInfo, ContextHubClientCallback)} instead to
 *             register a {@link android.hardware.location.ContextHubClientCallback}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int registerCallback(android.hardware.location.ContextHubManager.Callback callback, android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * Creates and registers a client and its callback with the Context Hub Service.
 *
 * A client is registered with the Context Hub Service for a specified Context Hub. When the
 * registration succeeds, the client can send messages to nanoapps through the returned
 * {@link ContextHubClient} object, and receive notifications through the provided callback.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubInfo  the hub to attach this client to
 * This value must never be {@code null}.
 * @param callback the notification callback to register
 * This value must never be {@code null}.
 * @param executor the executor to invoke the callback
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @return the registered client object
 *
 * @throws IllegalArgumentException if hubInfo does not represent a valid hub
 * @throws IllegalStateException    if there were too many registered clients at the service
 * @throws NullPointerException     if callback, hubInfo, or executor is null
 *
 * @see ContextHubClientCallback
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.ContextHubClient createClient(@android.annotation.NonNull android.hardware.location.ContextHubInfo hubInfo, @android.annotation.NonNull android.hardware.location.ContextHubClientCallback callback, @android.annotation.NonNull java.util.concurrent.Executor executor) { throw new RuntimeException("Stub!"); }

/**
 * Equivalent to {@link #createClient(ContextHubInfo, ContextHubClientCallback, Executor)}
 * with the executor using the main thread's Looper.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubInfo  the hub to attach this client to
 * This value must never be {@code null}.
 * @param callback the notification callback to register
 * This value must never be {@code null}.
 * @return the registered client object
 *
 * @throws IllegalArgumentException if hubInfo does not represent a valid hub
 * @throws IllegalStateException    if there were too many registered clients at the service
 * @throws NullPointerException     if callback or hubInfo is null
 *
 * @see ContextHubClientCallback
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.ContextHubClient createClient(@android.annotation.NonNull android.hardware.location.ContextHubInfo hubInfo, @android.annotation.NonNull android.hardware.location.ContextHubClientCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Creates a ContextHubClient that will receive notifications based on Intent events.
 *
 * This method should be used instead of {@link #createClient(ContextHubInfo,
 * ContextHubClientCallback)} or {@link #createClient(ContextHubInfo, ContextHubClientCallback,
 * Executor)} if the caller wants to preserve the messaging endpoint of a ContextHubClient, even
 * after a process exits. If the PendingIntent with the provided nanoapp has already been
 * registered at the service, then the same ContextHubClient will be regenerated without
 * creating a new client connection at the service. Note that the PendingIntent, nanoapp, and
 * Context Hub must all match in identifying a previously registered ContextHubClient.
 * If a client is regenerated, the host endpoint identifier attached to messages sent to the
 * nanoapp remains consistent, even if the original process has exited.
 *
 * If registered successfully, intents will be delivered regarding events or messages from the
 * specified nanoapp from the attached Context Hub. The intent will have an extra
 * {@link ContextHubManager.EXTRA_CONTEXT_HUB_INFO} of type {@link ContextHubInfo}, which
 * describes the Context Hub the intent event was for. The intent will also have an extra
 * {@link ContextHubManager.EXTRA_EVENT_TYPE} of type {@link ContextHubManager.Event}, which
 * will contain the type of the event. See {@link ContextHubManager.Event} for description of
 * each event type, along with event-specific extra fields. The client can also use
 * {@link ContextHubIntentEvent.fromIntent(Intent)} to parse the Intent generated by the event.
 *
 * Intent events will be delivered until {@link ContextHubClient.close()} is called. Note that
 * the registration of this ContextHubClient at the Context Hub Service will be maintained until
 * {@link ContextHubClient.close()} is called. If {@link PendingIntent.cancel()} is called
 * on the provided PendingIntent, then the client will be automatically unregistered by the
 * service.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param hubInfo       the hub to attach this client to
 * This value must never be {@code null}.
 * @param pendingIntent the PendingIntent to register to the client
 * This value must never be {@code null}.
 * @param nanoAppId     the ID of the nanoapp that Intent events will be generated for
 * @return the registered client object
 *
 * @throws IllegalArgumentException if hubInfo does not represent a valid hub
 * @throws IllegalStateException    if there were too many registered clients at the service
 * @throws NullPointerException     if pendingIntent or hubInfo is null
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.ContextHubClient createClient(@android.annotation.NonNull android.hardware.location.ContextHubInfo hubInfo, @android.annotation.NonNull android.app.PendingIntent pendingIntent, long nanoAppId) { throw new RuntimeException("Stub!"); }

/**
 * Unregister a callback for receive messages from the context hub.
 *
 * @see Callback
 *
 * @param callback method to deregister
 *
 * This value must never be {@code null}.
 * @return int 0 on success, -1 otherwise
 *
 * @deprecated Use {@link android.hardware.location.ContextHubClient#close()} to unregister
 *             a {@link android.hardware.location.ContextHubClientCallback}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int unregisterCallback(@android.annotation.NonNull android.hardware.location.ContextHubManager.Callback callback) { throw new RuntimeException("Stub!"); }

/**
 * An event describing that the Context Hub has reset.
 * @apiSince REL
 */

public static final int EVENT_HUB_RESET = 6; // 0x6

/**
 * An event describing that a nanoapp has aborted. Contains the EXTRA_NANOAPP_ID and
 * EXTRA_NANOAPP_ABORT_CODE extras.
 * @apiSince REL
 */

public static final int EVENT_NANOAPP_ABORTED = 4; // 0x4

/**
 * An event describing that a nanoapp has been disabled. Contains the EXTRA_NANOAPP_ID extra.
 * @apiSince REL
 */

public static final int EVENT_NANOAPP_DISABLED = 3; // 0x3

/**
 * An event describing that a nanoapp has been enabled. Contains the EXTRA_NANOAPP_ID extra.
 * @apiSince REL
 */

public static final int EVENT_NANOAPP_ENABLED = 2; // 0x2

/**
 * An event describing that a nanoapp has been loaded. Contains the EXTRA_NANOAPP_ID extra.
 * @apiSince REL
 */

public static final int EVENT_NANOAPP_LOADED = 0; // 0x0

/**
 * An event containing a message sent from a nanoapp. Contains the EXTRA_NANOAPP_ID and
 * EXTRA_NANOAPP_MESSAGE extras.
 * @apiSince REL
 */

public static final int EVENT_NANOAPP_MESSAGE = 5; // 0x5

/**
 * An event describing that a nanoapp has been unloaded. Contains the EXTRA_NANOAPP_ID extra.
 * @apiSince REL
 */

public static final int EVENT_NANOAPP_UNLOADED = 1; // 0x1

/**
 * An extra of type {@link ContextHubInfo} describing the source of the event.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_CONTEXT_HUB_INFO = "android.hardware.location.extra.CONTEXT_HUB_INFO";

/**
 * An extra of type {@link ContextHubManager.Event} describing the event type.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_EVENT_TYPE = "android.hardware.location.extra.EVENT_TYPE";

/**
 * An extra of type {@link NanoAppMessage} describing contents of a message from a nanoapp.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_MESSAGE = "android.hardware.location.extra.MESSAGE";

/**
 * An extra of type int describing the nanoapp-specific abort code.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_NANOAPP_ABORT_CODE = "android.hardware.location.extra.NANOAPP_ABORT_CODE";

/**
 * An extra of type long describing the ID of the nanoapp an event is for.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_NANOAPP_ID = "android.hardware.location.extra.NANOAPP_ID";
/**
 * An interface to receive asynchronous communication from the context hub.
 *
 * @deprecated Use the more refined {@link android.hardware.location.ContextHubClientCallback}
 *             instead for notification callbacks.
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract static class Callback {

/** @apiSince REL */

@Deprecated
protected Callback() { throw new RuntimeException("Stub!"); }

/**
 * Callback function called on message receipt from context hub.
 *
 * @param hubHandle Handle (system-wide unique identifier) of the hub of the message.
 * @param nanoAppHandle Handle (unique identifier) for app instance that sent the message.
 * @param message The context hub message.
 *
 * This value must never be {@code null}.
 * @see ContextHubMessage
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public abstract void onMessageReceipt(int hubHandle, int nanoAppHandle, @android.annotation.NonNull android.hardware.location.ContextHubMessage message);
}

}

