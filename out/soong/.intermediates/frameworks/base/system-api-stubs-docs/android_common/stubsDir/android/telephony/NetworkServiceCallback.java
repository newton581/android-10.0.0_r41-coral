/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;

import android.telephony.NetworkService.NetworkServiceProvider;

/**
 * Network service callback. Object of this class is passed to NetworkServiceProvider upon
 * calling requestNetworkRegistrationInfo, to receive asynchronous feedback from
 * NetworkServiceProvider upon onRequestNetworkRegistrationInfoComplete. It's like a wrapper of
 * INetworkServiceCallback because INetworkServiceCallback can't be a parameter type in public APIs.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class NetworkServiceCallback {

NetworkServiceCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called to indicate result of
 * {@link NetworkServiceProvider#requestNetworkRegistrationInfo(int, NetworkServiceCallback)}
 *
 * @param result Result status like {@link NetworkServiceCallback#RESULT_SUCCESS} or
 * {@link NetworkServiceCallback#RESULT_ERROR_UNSUPPORTED}
 * @param state The state information to be returned to callback.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void onRequestNetworkRegistrationInfoComplete(int result, @android.annotation.Nullable android.telephony.NetworkRegistrationInfo state) { throw new RuntimeException("Stub!"); }

/**
 * Service is busy
 * @apiSince REL
 */

public static final int RESULT_ERROR_BUSY = 3; // 0x3

/**
 * Request failed
 * @apiSince REL
 */

public static final int RESULT_ERROR_FAILED = 5; // 0x5

/**
 * Request sent in illegal state
 * @apiSince REL
 */

public static final int RESULT_ERROR_ILLEGAL_STATE = 4; // 0x4

/**
 * Request contains invalid arguments
 * @apiSince REL
 */

public static final int RESULT_ERROR_INVALID_ARG = 2; // 0x2

/**
 * Request is not support
 * @apiSince REL
 */

public static final int RESULT_ERROR_UNSUPPORTED = 1; // 0x1

/**
 * Request is completed successfully
 * @apiSince REL
 */

public static final int RESULT_SUCCESS = 0; // 0x0
}

