/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.tv;


/**
 * TvContentRatingSystemInfo class provides information about a specific TV content rating system
 * defined either by a system app or by a third-party app.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class TvContentRatingSystemInfo implements android.os.Parcelable {

TvContentRatingSystemInfo(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Creates a TvContentRatingSystemInfo object with given resource ID and receiver info.
 *
 * @param xmlResourceId The ID of an XML resource whose root element is
 *            <code> &lt;rating-system-definitions&gt;</code>
 * @param applicationInfo Information about the application that provides the TV content rating
 *            system definition.
 * @apiSince REL
 */

public static android.media.tv.TvContentRatingSystemInfo createTvContentRatingSystemInfo(int xmlResourceId, android.content.pm.ApplicationInfo applicationInfo) { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the TV content rating system is defined by a system app,
 * {@code false} otherwise.
 * @apiSince REL
 */

public boolean isSystemDefined() { throw new RuntimeException("Stub!"); }

/**
 * Returns the URI to the XML resource that defines the TV content rating system.
 *
 * TODO: Remove. Instead, parse the XML resource and provide an interface to directly access
 * parsed information.
 * @apiSince REL
 */

public android.net.Uri getXmlUri() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }
}

