/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.audiopolicy;

import android.media.AudioAttributes;

/**
 * @hide
 *
 * Here's an example of creating a mixing rule for all media playback:
 * <pre>
 * AudioAttributes mediaAttr = new AudioAttributes.Builder()
 *         .setUsage(AudioAttributes.USAGE_MEDIA)
 *         .build();
 * AudioMixingRule mediaRule = new AudioMixingRule.Builder()
 *         .addRule(mediaAttr, AudioMixingRule.RULE_MATCH_ATTRIBUTE_USAGE)
 *         .build();
 * </pre>
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AudioMixingRule {

AudioMixingRule() { throw new RuntimeException("Stub!"); }

/** @hide */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * A rule requiring the capture preset information of the {@link AudioAttributes} to match.
 * This mixing rule can be added with {@link Builder#addRule(AudioAttributes, int)} or
 * {@link Builder#addMixRule(int, Object)} where the Object parameter is an instance of
 * {@link AudioAttributes}.
 * @apiSince REL
 */

public static final int RULE_MATCH_ATTRIBUTE_CAPTURE_PRESET = 2; // 0x2

/**
 * A rule requiring the usage information of the {@link AudioAttributes} to match.
 * This mixing rule can be added with {@link Builder#addRule(AudioAttributes, int)} or
 * {@link Builder#addMixRule(int, Object)} where the Object parameter is an instance of
 * {@link AudioAttributes}.
 * @apiSince REL
 */

public static final int RULE_MATCH_ATTRIBUTE_USAGE = 1; // 0x1

/**
 * A rule requiring the UID of the audio stream to match that specified.
 * This mixing rule can be added with {@link Builder#addMixRule(int, Object)} where the Object
 * parameter is an instance of {@link java.lang.Integer}.
 * @apiSince REL
 */

public static final int RULE_MATCH_UID = 4; // 0x4
/**
 * Builder class for {@link AudioMixingRule} objects
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

/**
 * Constructs a new Builder with no rules.
 * @apiSince REL
 */

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Add a rule for the selection of which streams are mixed together.
 * @param attrToMatch a non-null AudioAttributes instance for which a contradictory
 *     rule hasn't been set yet.
 * @param rule {@link AudioMixingRule#RULE_MATCH_ATTRIBUTE_USAGE} or
 *     {@link AudioMixingRule#RULE_MATCH_ATTRIBUTE_CAPTURE_PRESET}.
 * @return the same Builder instance.
 * @throws IllegalArgumentException
 * @see #excludeRule(AudioAttributes, int)
 * @apiSince REL
 */

public android.media.audiopolicy.AudioMixingRule.Builder addRule(android.media.AudioAttributes attrToMatch, int rule) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Add a rule by exclusion for the selection of which streams are mixed together.
 * <br>For instance the following code
 * <br><pre>
 * AudioAttributes mediaAttr = new AudioAttributes.Builder()
 *         .setUsage(AudioAttributes.USAGE_MEDIA)
 *         .build();
 * AudioMixingRule noMediaRule = new AudioMixingRule.Builder()
 *         .excludeRule(mediaAttr, AudioMixingRule.RULE_MATCH_ATTRIBUTE_USAGE)
 *         .build();
 * </pre>
 * <br>will create a rule which maps to any usage value, except USAGE_MEDIA.
 * @param attrToMatch a non-null AudioAttributes instance for which a contradictory
 *     rule hasn't been set yet.
 * @param rule {@link AudioMixingRule#RULE_MATCH_ATTRIBUTE_USAGE} or
 *     {@link AudioMixingRule#RULE_MATCH_ATTRIBUTE_CAPTURE_PRESET}.
 * @return the same Builder instance.
 * @throws IllegalArgumentException
 * @see #addRule(AudioAttributes, int)
 * @apiSince REL
 */

public android.media.audiopolicy.AudioMixingRule.Builder excludeRule(android.media.AudioAttributes attrToMatch, int rule) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Add a rule for the selection of which streams are mixed together.
 * The rule defines what the matching will be made on. It also determines the type of the
 * property to match against.
 * @param rule one of {@link AudioMixingRule#RULE_MATCH_ATTRIBUTE_USAGE},
 *     {@link AudioMixingRule#RULE_MATCH_ATTRIBUTE_CAPTURE_PRESET} or
 *     {@link AudioMixingRule#RULE_MATCH_UID}.
 * @param property see the definition of each rule for the type to use (either an
 *     {@link AudioAttributes} or an {@link java.lang.Integer}).
 * @return the same Builder instance.
 * @throws IllegalArgumentException
 * @see #excludeMixRule(int, Object)
 * @apiSince REL
 */

public android.media.audiopolicy.AudioMixingRule.Builder addMixRule(int rule, java.lang.Object property) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Add a rule by exclusion for the selection of which streams are mixed together.
 * <br>For instance the following code
 * <br><pre>
 * AudioAttributes mediaAttr = new AudioAttributes.Builder()
 *         .setUsage(AudioAttributes.USAGE_MEDIA)
 *         .build();
 * AudioMixingRule noMediaRule = new AudioMixingRule.Builder()
 *         .addMixRule(AudioMixingRule.RULE_MATCH_ATTRIBUTE_USAGE, mediaAttr)
 *         .excludeMixRule(AudioMixingRule.RULE_MATCH_UID, new Integer(uidToExclude)
 *         .build();
 * </pre>
 * <br>will create a rule which maps to usage USAGE_MEDIA, but excludes any stream
 * coming from the specified UID.
 * @param rule one of {@link AudioMixingRule#RULE_MATCH_ATTRIBUTE_USAGE},
 *     {@link AudioMixingRule#RULE_MATCH_ATTRIBUTE_CAPTURE_PRESET} or
 *     {@link AudioMixingRule#RULE_MATCH_UID}.
 * @param property see the definition of each rule for the type to use (either an
 *     {@link AudioAttributes} or an {@link java.lang.Integer}).
 * @return the same Builder instance.
 * @throws IllegalArgumentException
 * @apiSince REL
 */

public android.media.audiopolicy.AudioMixingRule.Builder excludeMixRule(int rule, java.lang.Object property) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Set if the audio of app that opted out of audio playback capture should be captured.
 *
 * Caller of this method with <code>true</code>, MUST abide to the restriction listed in
 * {@link ALLOW_CAPTURE_BY_SYSTEM}, including but not limited to the captured audio
 * can not leave the capturing app, and the quality is limited to 16k mono.
 *
 * The permission {@link CAPTURE_AUDIO_OUTPUT} or {@link CAPTURE_MEDIA_OUTPUT} is needed
 * to ignore the opt-out.
 *
 * Only affects LOOPBACK|RENDER mix.
 *
 * @return the same Builder instance.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.media.audiopolicy.AudioMixingRule.Builder allowPrivilegedPlaybackCapture(boolean allow) { throw new RuntimeException("Stub!"); }

/**
 * Combines all of the matching and exclusion rules that have been set and return a new
 * {@link AudioMixingRule} object.
 * @return a new {@link AudioMixingRule} object
 * @apiSince REL
 */

public android.media.audiopolicy.AudioMixingRule build() { throw new RuntimeException("Stub!"); }
}

}

