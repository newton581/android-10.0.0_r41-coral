/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.settings.suggestions;


/**
 * Data object that has information about a device suggestion.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class Suggestion implements android.os.Parcelable {

Suggestion(android.service.settings.suggestions.Suggestion.Builder builder) { throw new RuntimeException("Stub!"); }

/**
 * Gets the id for the suggestion object.
 * @apiSince REL
 */

public java.lang.String getId() { throw new RuntimeException("Stub!"); }

/**
 * Title of the suggestion that is shown to the user.
 * @apiSince REL
 */

public java.lang.CharSequence getTitle() { throw new RuntimeException("Stub!"); }

/**
 * Optional summary describing what this suggestion controls.
 * @apiSince REL
 */

public java.lang.CharSequence getSummary() { throw new RuntimeException("Stub!"); }

/**
 * Optional icon for this suggestion.
 * @apiSince REL
 */

public android.graphics.drawable.Icon getIcon() { throw new RuntimeException("Stub!"); }

/**
 * Optional flags for this suggestion. This will influence UI when rendering suggestion in
 * different style.
 
 * @return Value is either <code>0</code> or a combination of {@link android.service.settings.suggestions.Suggestion#FLAG_HAS_BUTTON}, and android.service.settings.suggestions.Suggestion.FLAG_ICON_TINTABLE
 * @apiSince REL
 */

public int getFlags() { throw new RuntimeException("Stub!"); }

/**
 * The Intent to launch when the suggestion is activated.
 * @apiSince REL
 */

public android.app.PendingIntent getPendingIntent() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.settings.suggestions.Suggestion> CREATOR;
static { CREATOR = null; }

/**
 * Flag for suggestion type with a single button
 * @apiSince REL
 */

public static final int FLAG_HAS_BUTTON = 1; // 0x1
/**
 * Builder class for {@link Suggestion}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

/** @apiSince REL */

public Builder(java.lang.String id) { throw new RuntimeException("Stub!"); }

/**
 * Sets suggestion title
 * @apiSince REL
 */

public android.service.settings.suggestions.Suggestion.Builder setTitle(java.lang.CharSequence title) { throw new RuntimeException("Stub!"); }

/**
 * Sets suggestion summary
 * @apiSince REL
 */

public android.service.settings.suggestions.Suggestion.Builder setSummary(java.lang.CharSequence summary) { throw new RuntimeException("Stub!"); }

/**
 * Sets icon for the suggestion.
 * @apiSince REL
 */

public android.service.settings.suggestions.Suggestion.Builder setIcon(android.graphics.drawable.Icon icon) { throw new RuntimeException("Stub!"); }

/**
 * Sets a UI type for this suggestion. This will influence UI when rendering suggestion in
 * different style.
 
 * @param flags Value is either <code>0</code> or a combination of {@link android.service.settings.suggestions.Suggestion#FLAG_HAS_BUTTON}, and android.service.settings.suggestions.Suggestion.FLAG_ICON_TINTABLE
 * @apiSince REL
 */

public android.service.settings.suggestions.Suggestion.Builder setFlags(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Sets suggestion intent
 * @apiSince REL
 */

public android.service.settings.suggestions.Suggestion.Builder setPendingIntent(android.app.PendingIntent pendingIntent) { throw new RuntimeException("Stub!"); }

/**
 * Builds an immutable {@link Suggestion} object.
 * @apiSince REL
 */

public android.service.settings.suggestions.Suggestion build() { throw new RuntimeException("Stub!"); }
}

}

