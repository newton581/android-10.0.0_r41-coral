/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.location;


/**
 * A class implementing a container for data associated with a navigation message event.
 * Events are delivered to registered instances of {@link Listener}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class GpsNavigationMessageEvent implements android.os.Parcelable {

/** @apiSince REL */

public GpsNavigationMessageEvent(android.location.GpsNavigationMessage message) { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GpsNavigationMessage getNavigationMessage() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.location.GpsNavigationMessageEvent> CREATOR;
static { CREATOR = null; }

/**
 * GPS provider or Location is disabled, updated will not be received until they are enabled.
 * @apiSince REL
 */

public static int STATUS_GPS_LOCATION_DISABLED = 2; // 0x2

/**
 * The system does not support tracking of GPS Navigation Messages. This status will not change
 * in the future.
 * @apiSince REL
 */

public static int STATUS_NOT_SUPPORTED = 0; // 0x0

/**
 * GPS Navigation Messages are successfully being tracked, it will receive updates once they are
 * available.
 * @apiSince REL
 */

public static int STATUS_READY = 1; // 0x1
/**
 * Used for receiving GPS satellite Navigation Messages from the GPS engine.
 * You can implement this interface and call
 * {@link LocationManager#addGpsNavigationMessageListener}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface Listener {

/**
 * Returns the latest collected GPS Navigation Message.
 * @apiSince REL
 */

public void onGpsNavigationMessageReceived(android.location.GpsNavigationMessageEvent event);

/**
 * Returns the latest status of the GPS Navigation Messages sub-system.
 * @apiSince REL
 */

public void onStatusChanged(int status);
}

}

