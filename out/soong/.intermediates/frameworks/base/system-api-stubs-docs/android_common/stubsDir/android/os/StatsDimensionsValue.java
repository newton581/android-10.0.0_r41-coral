/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.os;

import java.util.List;

/**
 * Container for statsd dimension value information, corresponding to a
 * stats_log.proto's DimensionValue.
 *
 * This consists of a field (an int representing a statsd atom field)
 * and a value (which may be one of a number of types).
 *
 * <p>
 * Only a single value is held, and it is necessarily one of the following types:
 * {@link String}, int, long, boolean, float,
 * or tuple (i.e. {@link List} of {@code StatsDimensionsValue}).
 *
 * The type of value held can be retrieved using {@link #getValueType()}, which returns one of the
 * following ints, depending on the type of value:
 * <ul>
 *  <li>{@link #STRING_VALUE_TYPE}</li>
 *  <li>{@link #INT_VALUE_TYPE}</li>
 *  <li>{@link #LONG_VALUE_TYPE}</li>
 *  <li>{@link #BOOLEAN_VALUE_TYPE}</li>
 *  <li>{@link #FLOAT_VALUE_TYPE}</li>
 *  <li>{@link #TUPLE_VALUE_TYPE}</li>
 * </ul>
 * Alternatively, this can be determined using {@link #isValueType(int)} with one of these constants
 * as a parameter.
 * The value itself can be retrieved using the correct get...Value() function for its type.
 *
 * <p>
 * The field is always an int, and always exists; it can be obtained using {@link #getField()}.
 *
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class StatsDimensionsValue implements android.os.Parcelable {

/**
 * Creates a {@code StatsDimensionValue} from a parcel.
 *
 * @hide
 */

StatsDimensionsValue(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Return the field, i.e. the tag of a statsd atom.
 *
 * @return the field
 * @apiSince REL
 */

public int getField() { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the String held, if any.
 *
 * @return the {@link String} held if {@link #getValueType()} == {@link #STRING_VALUE_TYPE},
 *         null otherwise
 * @apiSince REL
 */

public java.lang.String getStringValue() { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the int held, if any.
 *
 * @return the int held if {@link #getValueType()} == {@link #INT_VALUE_TYPE}, 0 otherwise
 * @apiSince REL
 */

public int getIntValue() { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the long held, if any.
 *
 * @return the long held if {@link #getValueType()} == {@link #LONG_VALUE_TYPE}, 0 otherwise
 * @apiSince REL
 */

public long getLongValue() { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the boolean held, if any.
 *
 * @return the boolean held if {@link #getValueType()} == {@link #BOOLEAN_VALUE_TYPE},
 *         false otherwise
 * @apiSince REL
 */

public boolean getBooleanValue() { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the float held, if any.
 *
 * @return the float held if {@link #getValueType()} == {@link #FLOAT_VALUE_TYPE}, 0 otherwise
 * @apiSince REL
 */

public float getFloatValue() { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the tuple, in the form of a {@link List} of {@link StatsDimensionsValue}, held,
 * if any.
 *
 * @return the {@link List} of {@link StatsDimensionsValue} held
 *         if {@link #getValueType()} == {@link #TUPLE_VALUE_TYPE},
 *         null otherwise
 * @apiSince REL
 */

public java.util.List<android.os.StatsDimensionsValue> getTupleValueList() { throw new RuntimeException("Stub!"); }

/**
 * Returns the constant representing the type of value stored, namely one of
 * <ul>
 *   <li>{@link #STRING_VALUE_TYPE}</li>
 *   <li>{@link #INT_VALUE_TYPE}</li>
 *   <li>{@link #LONG_VALUE_TYPE}</li>
 *   <li>{@link #BOOLEAN_VALUE_TYPE}</li>
 *   <li>{@link #FLOAT_VALUE_TYPE}</li>
 *   <li>{@link #TUPLE_VALUE_TYPE}</li>
 * </ul>
 *
 * @return the constant representing the type of value stored
 * @apiSince REL
 */

public int getValueType() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the type of value stored is equal to the given type.
 *
 * @param valueType int representing the type of value stored, as used in {@link #getValueType}
 * @return true if {@link #getValueType()} is equal to {@code valueType}.
 * @apiSince REL
 */

public boolean isValueType(int valueType) { throw new RuntimeException("Stub!"); }

/**
 * Returns a String representing the information in this StatsDimensionValue.
 * No guarantees are made about the format of this String.
 *
 * @return String representation
 *
 * @hide
 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Indicates that this holds a boolean.
 * @apiSince REL
 */

public static final int BOOLEAN_VALUE_TYPE = 5; // 0x5

/**
 * Parcelable Creator for StatsDimensionsValue.
 * @apiSince REL
 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.StatsDimensionsValue> CREATOR;
static { CREATOR = null; }

/**
 * Indicates that this holds a float.
 * @apiSince REL
 */

public static final int FLOAT_VALUE_TYPE = 6; // 0x6

/**
 * Indicates that this holds an int.
 * @apiSince REL
 */

public static final int INT_VALUE_TYPE = 3; // 0x3

/**
 * Indicates that this holds a long.
 * @apiSince REL
 */

public static final int LONG_VALUE_TYPE = 4; // 0x4

/**
 * Indicates that this holds a String.
 * @apiSince REL
 */

public static final int STRING_VALUE_TYPE = 2; // 0x2

/**
 * Indicates that this holds a List of StatsDimensionsValues.
 * @apiSince REL
 */

public static final int TUPLE_VALUE_TYPE = 7; // 0x7
}

