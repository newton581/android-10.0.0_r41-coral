/*
 * Copyright (C) 2005 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;


/**
 * The arguments for an incident report.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class IncidentReportArgs implements android.os.Parcelable {

/**
 * Construct an incident report args with no fields.
 * @apiSince REL
 */

public IncidentReportArgs() { throw new RuntimeException("Stub!"); }

/**
 * Construct an incdent report args from the given parcel.
 * @apiSince REL
 */

public IncidentReportArgs(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void readFromParcel(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Print this report as a string.
 * @apiSince REL
 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Set this incident report to include all fields.
 * @apiSince REL
 */

public void setAll(boolean all) { throw new RuntimeException("Stub!"); }

/**
 * Set this incident report privacy policy spec.
 * @apiSince REL
 */

public void setPrivacyPolicy(int privacyPolicy) { throw new RuntimeException("Stub!"); }

/**
 * Add this section to the incident report. Skip if the input is smaller than 2 since section
 * id are only valid for positive integer as Protobuf field id. Here 1 is reserved for Header.
 * @apiSince REL
 */

public void addSection(int section) { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the incident report will include all fields.
 * @apiSince REL
 */

public boolean isAll() { throw new RuntimeException("Stub!"); }

/**
 * Returns whether this section will be included in the incident report.
 * @apiSince REL
 */

public boolean containsSection(int section) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int sectionCount() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void addHeader(byte[] header) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.IncidentReportArgs> CREATOR;
static { CREATOR = null; }
}

