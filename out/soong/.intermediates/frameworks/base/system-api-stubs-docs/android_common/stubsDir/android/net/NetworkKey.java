/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.net;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;

/**
 * Information which identifies a specific network.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class NetworkKey implements android.os.Parcelable {

/**
 * Construct a new {@link NetworkKey} for a Wi-Fi network.
 * @param wifiKey the {@link WifiKey} identifying this Wi-Fi network.
 * @apiSince REL
 */

public NetworkKey(android.net.WifiKey wifiKey) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.net.NetworkKey> CREATOR;
static { CREATOR = null; }

/**
 * A wifi network, for which {@link #wifiKey} will be populated.
 * @apiSince REL
 */

public static final int TYPE_WIFI = 1; // 0x1

/**
 * The type of this network.
 * @see #TYPE_WIFI
 * @apiSince REL
 */

public final int type;
{ type = 0; }

/**
 * Information identifying a Wi-Fi network. Only set when {@link #type} equals
 * {@link #TYPE_WIFI}.
 * @apiSince REL
 */

public final android.net.WifiKey wifiKey;
{ wifiKey = null; }
}

