/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims;

import android.telephony.CarrierConfigManager;
import android.telephony.ims.feature.RcsFeature;
import android.telephony.ims.feature.ImsFeature;
import android.telephony.ims.stub.ImsFeatureConfiguration;
import android.telephony.ims.feature.MmTelFeature;
import android.telephony.ims.stub.ImsConfigImplBase;
import android.telephony.ims.stub.ImsRegistrationImplBase;

/**
 * Main ImsService implementation, which binds via the Telephony ImsResolver. Services that extend
 * ImsService must register the service in their AndroidManifest to be detected by the framework.
 * First, the application must declare that they use the "android.permission.BIND_IMS_SERVICE"
 * permission. Then, the ImsService definition in the manifest must follow the following format:
 *
 * ...
 * <service android:name=".EgImsService"
 *     android:permission="android.permission.BIND_IMS_SERVICE" >
 *     ...
 *     <intent-filter>
 *         <action android:name="android.telephony.ims.ImsService" />
 *     </intent-filter>
 * </service>
 * ...
 *
 * The telephony framework will then bind to the ImsService you have defined in your manifest
 * if you are either:
 * 1) Defined as the default ImsService for the device in the device overlay using
 *    "config_ims_package".
 * 2) Defined as a Carrier Provided ImsService in the Carrier Configuration using
 *    {@link CarrierConfigManager#KEY_CONFIG_IMS_PACKAGE_OVERRIDE_STRING}.
 *
 * There are two ways to define to the platform which {@link ImsFeature}s this {@link ImsService}
 * supports, dynamic or static definitions.
 *
 * In the static definition, the {@link ImsFeature}s that are supported are defined in the service
 * definition of the AndroidManifest.xml file as metadata:
 * <!-- Apps must declare which features they support as metadata. The different categories are
 *      defined below. In this example, the MMTEL_FEATURE feature is supported. -->
 * <meta-data android:name="android.telephony.ims.MMTEL_FEATURE" android:value="true" />
 *
 * The features that are currently supported in an ImsService are:
 * - RCS_FEATURE: This ImsService implements the RcsFeature class.
 * - MMTEL_FEATURE: This ImsService implements the MmTelFeature class.
 * - EMERGENCY_MMTEL_FEATURE: This ImsService supports Emergency Calling for MMTEL, must be
 *   declared along with the MMTEL_FEATURE. If this is not specified, the framework will use
 *   circuit switch for emergency calling.
 *
 * In the dynamic definition, the supported features are not specified in the service definition
 * of the AndroidManifest. Instead, the framework binds to this service and calls
 * {@link #querySupportedImsFeatures()}. The {@link ImsService} then returns an
 * {@link ImsFeatureConfiguration}, which the framework uses to initialize the supported
 * {@link ImsFeature}s. If at any time, the list of supported {@link ImsFeature}s changes,
 * {@link #onUpdateSupportedImsFeatures(ImsFeatureConfiguration)} can be called to tell the
 * framework of the changes.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ImsService extends android.app.Service {

public ImsService() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * When called, provide the {@link ImsFeatureConfiguration} that this {@link ImsService}
 * currently supports. This will trigger the framework to set up the {@link ImsFeature}s that
 * correspond to the {@link ImsFeature}s configured here.
 *
 * Use {@link #onUpdateSupportedImsFeatures(ImsFeatureConfiguration)} to change the supported
 * {@link ImsFeature}s.
 *
 * @return an {@link ImsFeatureConfiguration} containing Features this ImsService supports.
 * @apiSince REL
 */

public android.telephony.ims.stub.ImsFeatureConfiguration querySupportedImsFeatures() { throw new RuntimeException("Stub!"); }

/**
 * Updates the framework with a new {@link ImsFeatureConfiguration} containing the updated
 * features, that this {@link ImsService} supports. This may trigger the framework to add/remove
 * new ImsFeatures, depending on the configuration.
 * @apiSince REL
 */

public final void onUpdateSupportedImsFeatures(android.telephony.ims.stub.ImsFeatureConfiguration c) throws android.os.RemoteException { throw new RuntimeException("Stub!"); }

/**
 * The ImsService has been bound and is ready for ImsFeature creation based on the Features that
 * the ImsService has registered for with the framework, either in the manifest or via
 * {@link #querySupportedImsFeatures()}.
 *
 * The ImsService should use this signal instead of onCreate/onBind or similar to perform
 * feature initialization because the framework may bind to this service multiple times to
 * query the ImsService's {@link ImsFeatureConfiguration} via
 * {@link #querySupportedImsFeatures()}before creating features.
 * @apiSince REL
 */

public void readyForFeatureCreation() { throw new RuntimeException("Stub!"); }

/**
 * The framework has enabled IMS for the slot specified, the ImsService should register for IMS
 * and perform all appropriate initialization to bring up all ImsFeatures.
 * @apiSince REL
 */

public void enableIms(int slotId) { throw new RuntimeException("Stub!"); }

/**
 * The framework has disabled IMS for the slot specified. The ImsService must deregister for IMS
 * and set capability status to false for all ImsFeatures.
 * @apiSince REL
 */

public void disableIms(int slotId) { throw new RuntimeException("Stub!"); }

/**
 * When called, the framework is requesting that a new {@link MmTelFeature} is created for the
 * specified slot.
 *
 * @param slotId The slot ID that the MMTEL Feature is being created for.
 * @return The newly created {@link MmTelFeature} associated with the slot or null if the
 * feature is not supported.
 * @apiSince REL
 */

public android.telephony.ims.feature.MmTelFeature createMmTelFeature(int slotId) { throw new RuntimeException("Stub!"); }

/**
 * When called, the framework is requesting that a new {@link RcsFeature} is created for the
 * specified slot.
 *
 * @param slotId The slot ID that the RCS Feature is being created for.
 * @return The newly created {@link RcsFeature} associated with the slot or null if the feature
 * is not supported.
 * @apiSince REL
 */

public android.telephony.ims.feature.RcsFeature createRcsFeature(int slotId) { throw new RuntimeException("Stub!"); }

/**
 * Return the {@link ImsConfigImplBase} implementation associated with the provided slot. This
 * will be used by the platform to get/set specific IMS related configurations.
 *
 * @param slotId The slot that the IMS configuration is associated with.
 * @return ImsConfig implementation that is associated with the specified slot.
 * @apiSince REL
 */

public android.telephony.ims.stub.ImsConfigImplBase getConfig(int slotId) { throw new RuntimeException("Stub!"); }

/**
 * Return the {@link ImsRegistrationImplBase} implementation associated with the provided slot.
 *
 * @param slotId The slot that is associated with the IMS Registration.
 * @return the ImsRegistration implementation associated with the slot.
 * @apiSince REL
 */

public android.telephony.ims.stub.ImsRegistrationImplBase getRegistration(int slotId) { throw new RuntimeException("Stub!"); }
}

