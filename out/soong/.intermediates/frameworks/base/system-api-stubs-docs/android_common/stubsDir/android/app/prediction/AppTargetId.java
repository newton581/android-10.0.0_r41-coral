/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app.prediction;


/**
 * The id for a prediction target. See {@link AppTarget}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AppTargetId implements android.os.Parcelable {

/**
 * Creates a new id for a prediction target.
 *
 * @hide

 * @param id This value must never be {@code null}.
 */

public AppTargetId(@android.annotation.NonNull java.lang.String id) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.prediction.AppTargetId> CREATOR;
static { CREATOR = null; }
}

