/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.location;


/**
 * This class represents the characteristics of the geofence.
 *
 * <p> Use this in conjunction with {@link GeofenceHardware} APIs.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class GeofenceHardwareRequest {

public GeofenceHardwareRequest() { throw new RuntimeException("Stub!"); }

/**
 * Create a circular geofence.
 *
 * @param latitude Latitude of the geofence
 * @param longitude Longitude of the geofence
 * @param radius Radius of the geofence (in meters)
 * @apiSince 18
 */

public static android.hardware.location.GeofenceHardwareRequest createCircularGeofence(double latitude, double longitude, double radius) { throw new RuntimeException("Stub!"); }

/**
 * Set the last known transition of the geofence.
 *
 * @param lastTransition The current state of the geofence. Can be one of
 *        {@link GeofenceHardware#GEOFENCE_ENTERED}, {@link GeofenceHardware#GEOFENCE_EXITED},
 *        {@link GeofenceHardware#GEOFENCE_UNCERTAIN}.
 * @apiSince 18
 */

public void setLastTransition(int lastTransition) { throw new RuntimeException("Stub!"); }

/**
 * Set the unknown timer for this geofence.
 *
 * @param unknownTimer  The time limit after which the
 *        {@link GeofenceHardware#GEOFENCE_UNCERTAIN} transition
 *        should be triggered. This paramter is defined in milliseconds.
 * @apiSince 18
 */

public void setUnknownTimer(int unknownTimer) { throw new RuntimeException("Stub!"); }

/**
 * Set the transitions to be monitored.
 *
 * @param monitorTransitions Bitwise OR of {@link GeofenceHardware#GEOFENCE_ENTERED},
 *        {@link GeofenceHardware#GEOFENCE_EXITED}, {@link GeofenceHardware#GEOFENCE_UNCERTAIN}
 * @apiSince 18
 */

public void setMonitorTransitions(int monitorTransitions) { throw new RuntimeException("Stub!"); }

/**
 * Set the notification responsiveness of the geofence.
 *
 * @param notificationResponsiveness (milliseconds) Defines the best-effort description
 *        of how soon should the callback be called when the transition
 *        associated with the Geofence is triggered. For instance, if
 *        set to 1000 millseconds with {@link GeofenceHardware#GEOFENCE_ENTERED},
 *        the callback will be called 1000 milliseconds within entering
 *        the geofence.
 * @apiSince 18
 */

public void setNotificationResponsiveness(int notificationResponsiveness) { throw new RuntimeException("Stub!"); }

/**
 * Set the source technologies to use while tracking the geofence.
 * The value is the bit-wise of one or several source fields defined in
 * {@link GeofenceHardware}.
 *
 * @param sourceTechnologies The set of source technologies to use.
 * @apiSince REL
 */

public void setSourceTechnologies(int sourceTechnologies) { throw new RuntimeException("Stub!"); }

/**
 * Returns the latitude of this geofence.
 * @apiSince 18
 */

public double getLatitude() { throw new RuntimeException("Stub!"); }

/**
 * Returns the longitude of this geofence.
 * @apiSince 18
 */

public double getLongitude() { throw new RuntimeException("Stub!"); }

/**
 * Returns the radius of this geofence.
 * @apiSince 18
 */

public double getRadius() { throw new RuntimeException("Stub!"); }

/**
 * Returns transitions monitored for this geofence.
 * @apiSince 18
 */

public int getMonitorTransitions() { throw new RuntimeException("Stub!"); }

/**
 * Returns the unknownTimer of this geofence.
 * @apiSince 18
 */

public int getUnknownTimer() { throw new RuntimeException("Stub!"); }

/**
 * Returns the notification responsiveness of this geofence.
 * @apiSince 18
 */

public int getNotificationResponsiveness() { throw new RuntimeException("Stub!"); }

/**
 * Returns the last transition of this geofence.
 * @apiSince 18
 */

public int getLastTransition() { throw new RuntimeException("Stub!"); }

/**
 * Returns the source technologies to track this geofence.
 * @apiSince REL
 */

public int getSourceTechnologies() { throw new RuntimeException("Stub!"); }
}

