/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.usb;

import android.Manifest;

/**
 * Represents a physical USB port and describes its characteristics.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class UsbPort {

/** @hide */

UsbPort(@android.annotation.NonNull android.hardware.usb.UsbManager usbManager, @android.annotation.NonNull java.lang.String id, int supportedModes, int supportedContaminantProtectionModes, boolean supportsEnableContaminantPresenceProtection, boolean supportsEnableContaminantPresenceDetection) { throw new RuntimeException("Stub!"); }

/**
 * Gets the status of this USB port.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_USB}
 * @return The status of the this port, or {@code null} if port is unknown.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.hardware.usb.UsbPortStatus getStatus() { throw new RuntimeException("Stub!"); }

/**
 * Sets the desired role combination of the port.
 * <p>
 * The supported role combinations depend on what is connected to the port and may be
 * determined by consulting
 * {@link UsbPortStatus#isRoleCombinationSupported UsbPortStatus.isRoleCombinationSupported}.
 * </p><p>
 * Note: This function is asynchronous and may fail silently without applying
 * the requested changes.  If this function does cause a status change to occur then
 * a {@link UsbManager#ACTION_USB_PORT_CHANGED} broadcast will be sent.
 * </p>
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_USB}
 * @param powerRole The desired power role: {@link UsbPortStatus#POWER_ROLE_SOURCE} or
 *                  {@link UsbPortStatus#POWER_ROLE_SINK}, or
 *                  {@link UsbPortStatus#POWER_ROLE_NONE} if no power role.
 * Value is {@link android.hardware.usb.UsbPortStatus#POWER_ROLE_NONE}, {@link android.hardware.usb.UsbPortStatus#POWER_ROLE_SOURCE}, or {@link android.hardware.usb.UsbPortStatus#POWER_ROLE_SINK}
 * @param dataRole The desired data role: {@link UsbPortStatus#DATA_ROLE_HOST} or
 *                 {@link UsbPortStatus#DATA_ROLE_DEVICE}, or
 *                 {@link UsbPortStatus#DATA_ROLE_NONE} if no data role.
 
 * Value is {@link android.hardware.usb.UsbPortStatus#DATA_ROLE_NONE}, {@link android.hardware.usb.UsbPortStatus#DATA_ROLE_HOST}, or {@link android.hardware.usb.UsbPortStatus#DATA_ROLE_DEVICE}
 * @apiSince REL
 */

public void setRoles(int powerRole, int dataRole) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

