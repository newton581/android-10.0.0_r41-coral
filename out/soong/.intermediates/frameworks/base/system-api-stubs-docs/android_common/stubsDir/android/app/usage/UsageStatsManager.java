/**
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */


package android.app.usage;

import java.util.Map;
import android.app.PendingIntent;
import java.time.Duration;

/**
 * Provides access to device usage history and statistics. Usage data is aggregated into
 * time intervals: days, weeks, months, and years.
 * <p />
 * When requesting usage data since a particular time, the request might look something like this:
 * <pre>
 * PAST                   REQUEST_TIME                    TODAY                   FUTURE
 * ————————————————————————————||———————————————————————————¦-----------------------|
 *                        YEAR ||                           ¦                       |
 * ————————————————————————————||———————————————————————————¦-----------------------|
 *  MONTH            |         ||                MONTH      ¦                       |
 * ——————————————————|—————————||———————————————————————————¦-----------------------|
 *   |      WEEK     |     WEEK||    |     WEEK     |     WE¦EK     |      WEEK     |
 * ————————————————————————————||———————————————————|———————¦-----------------------|
 *                             ||           |DAY|DAY|DAY|DAY¦DAY|DAY|DAY|DAY|DAY|DAY|
 * ————————————————————————————||———————————————————————————¦-----------------------|
 * </pre>
 * A request for data in the middle of a time interval will include that interval.
 * <p/>
 * <b>NOTE:</b> Most methods on this API require the permission
 * android.permission.PACKAGE_USAGE_STATS. However, declaring the permission implies intention to
 * use the API and the user of the device still needs to grant permission through the Settings
 * application.
 * See {@link android.provider.Settings#ACTION_USAGE_ACCESS_SETTINGS}.
 * Methods which only return the information for the calling package do not require this permission.
 * E.g. {@link #getAppStandbyBucket()} and {@link #queryEventsForSelf(long, long)}.
 * @apiSince 21
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class UsageStatsManager {

UsageStatsManager() { throw new RuntimeException("Stub!"); }

/**
 * Gets application usage stats for the given time range, aggregated by the specified interval.
 *
 * <p>
 * The returned list will contain one or more {@link UsageStats} objects for each package, with
 * usage data that covers at least the given time range.
 * Note: The begin and end times of the time range may be expanded to the nearest whole interval
 * period.
 * </p>
 *
 * <p> The caller must have {@link android.Manifest.permission#PACKAGE_USAGE_STATS} </p>
 *
 * @param intervalType The time interval by which the stats are aggregated.
 * @param beginTime The inclusive beginning of the range of stats to include in the results.
 *                  Defined in terms of "Unix time", see
 *                  {@link java.lang.System#currentTimeMillis}.
 * @param endTime The exclusive end of the range of stats to include in the results. Defined
 *                in terms of "Unix time", see {@link java.lang.System#currentTimeMillis}.
 * @return A list of {@link UsageStats}
 *
 * @see #INTERVAL_DAILY
 * @see #INTERVAL_WEEKLY
 * @see #INTERVAL_MONTHLY
 * @see #INTERVAL_YEARLY
 * @see #INTERVAL_BEST
 * @apiSince 21
 */

public java.util.List<android.app.usage.UsageStats> queryUsageStats(int intervalType, long beginTime, long endTime) { throw new RuntimeException("Stub!"); }

/**
 * Gets the hardware configurations the device was in for the given time range, aggregated by
 * the specified interval. The results are ordered as in
 * {@link #queryUsageStats(int, long, long)}.
 * <p> The caller must have {@link android.Manifest.permission#PACKAGE_USAGE_STATS} </p>
 *
 * @param intervalType The time interval by which the stats are aggregated.
 * @param beginTime The inclusive beginning of the range of stats to include in the results.
 *                  Defined in terms of "Unix time", see
 *                  {@link java.lang.System#currentTimeMillis}.
 * @param endTime The exclusive end of the range of stats to include in the results. Defined
 *                in terms of "Unix time", see {@link java.lang.System#currentTimeMillis}.
 * @return A list of {@link ConfigurationStats}
 * @apiSince 21
 */

public java.util.List<android.app.usage.ConfigurationStats> queryConfigurations(int intervalType, long beginTime, long endTime) { throw new RuntimeException("Stub!"); }

/**
 * Gets aggregated event stats for the given time range, aggregated by the specified interval.
 * <p>The returned list will contain a {@link EventStats} object for each event type that
 * is being aggregated and has data for an interval that is a subset of the time range given.
 *
 * <p>The current event types that will be aggregated here are:</p>
 * <ul>
 *     <li>{@link UsageEvents.Event#SCREEN_INTERACTIVE}</li>
 *     <li>{@link UsageEvents.Event#SCREEN_NON_INTERACTIVE}</li>
 *     <li>{@link UsageEvents.Event#KEYGUARD_SHOWN}</li>
 *     <li>{@link UsageEvents.Event#KEYGUARD_HIDDEN}</li>
 * </ul>
 *
 * <p> The caller must have {@link android.Manifest.permission#PACKAGE_USAGE_STATS} </p>
 *
 * @param intervalType The time interval by which the stats are aggregated.
 * @param beginTime The inclusive beginning of the range of stats to include in the results.
 *                  Defined in terms of "Unix time", see
 *                  {@link java.lang.System#currentTimeMillis}.
 * @param endTime The exclusive end of the range of stats to include in the results. Defined
 *                in terms of "Unix time", see {@link java.lang.System#currentTimeMillis}.
 * @return A list of {@link EventStats}
 *
 * @see #INTERVAL_DAILY
 * @see #INTERVAL_WEEKLY
 * @see #INTERVAL_MONTHLY
 * @see #INTERVAL_YEARLY
 * @see #INTERVAL_BEST
 * @apiSince 28
 */

public java.util.List<android.app.usage.EventStats> queryEventStats(int intervalType, long beginTime, long endTime) { throw new RuntimeException("Stub!"); }

/**
 * Query for events in the given time range. Events are only kept by the system for a few
 * days.
 * <p> The caller must have {@link android.Manifest.permission#PACKAGE_USAGE_STATS} </p>
 *
 * @param beginTime The inclusive beginning of the range of events to include in the results.
 *                 Defined in terms of "Unix time", see
 *                 {@link java.lang.System#currentTimeMillis}.
 * @param endTime The exclusive end of the range of events to include in the results. Defined
 *               in terms of "Unix time", see {@link java.lang.System#currentTimeMillis}.
 * @return A {@link UsageEvents}.
 * @apiSince 21
 */

public android.app.usage.UsageEvents queryEvents(long beginTime, long endTime) { throw new RuntimeException("Stub!"); }

/**
 * Like {@link #queryEvents(long, long)}, but only returns events for the calling package.
 *
 * @param beginTime The inclusive beginning of the range of events to include in the results.
 *                 Defined in terms of "Unix time", see
 *                 {@link java.lang.System#currentTimeMillis}.
 * @param endTime The exclusive end of the range of events to include in the results. Defined
 *               in terms of "Unix time", see {@link java.lang.System#currentTimeMillis}.
 * @return A {@link UsageEvents} object.
 *
 * @see #queryEvents(long, long)
 * @apiSince 28
 */

public android.app.usage.UsageEvents queryEventsForSelf(long beginTime, long endTime) { throw new RuntimeException("Stub!"); }

/**
 * A convenience method that queries for all stats in the given range (using the best interval
 * for that range), merges the resulting data, and keys it by package name.
 * See {@link #queryUsageStats(int, long, long)}.
 * <p> The caller must have {@link android.Manifest.permission#PACKAGE_USAGE_STATS} </p>
 *
 * @param beginTime The inclusive beginning of the range of stats to include in the results.
 *                  Defined in terms of "Unix time", see
 *                  {@link java.lang.System#currentTimeMillis}.
 * @param endTime The exclusive end of the range of stats to include in the results. Defined
 *                in terms of "Unix time", see {@link java.lang.System#currentTimeMillis}.
 * @return A {@link java.util.Map} keyed by package name
 * @apiSince 21
 */

public java.util.Map<java.lang.String,android.app.usage.UsageStats> queryAndAggregateUsageStats(long beginTime, long endTime) { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the specified app is currently considered inactive. This will be true if the
 * app hasn't been used directly or indirectly for a period of time defined by the system. This
 * could be of the order of several hours or days.
 * @param packageName The package name of the app to query
 * @return whether the app is currently considered inactive
 * @apiSince 23
 */

public boolean isAppInactive(java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Returns the current standby bucket of the calling app. The system determines the standby
 * state of the app based on app usage patterns. Standby buckets determine how much an app will
 * be restricted from running background tasks such as jobs and alarms.
 * <p>Restrictions increase progressively from {@link #STANDBY_BUCKET_ACTIVE} to
 * {@link #STANDBY_BUCKET_RARE}, with {@link #STANDBY_BUCKET_ACTIVE} being the least
 * restrictive. The battery level of the device might also affect the restrictions.
 * <p>Apps in buckets &le; {@link #STANDBY_BUCKET_ACTIVE} have no standby restrictions imposed.
 * Apps in buckets &gt; {@link #STANDBY_BUCKET_FREQUENT} may have network access restricted when
 * running in the background.
 * <p>The standby state of an app can change at any time either due to a user interaction or a
 * system interaction or some algorithm determining that the app can be restricted for a period
 * of time before the user has a need for it.
 * <p>You can also query the recent history of standby bucket changes by calling
 * {@link #queryEventsForSelf(long, long)} and searching for
 * {@link UsageEvents.Event#STANDBY_BUCKET_CHANGED}.
 *
 * @return the current standby bucket of the calling app. One of STANDBY_BUCKET_* constants.
 
 * Value is {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_EXEMPTED}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_ACTIVE}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_WORKING_SET}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_FREQUENT}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_RARE}, or {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_NEVER}
 * @apiSince 28
 */

public int getAppStandbyBucket() { throw new RuntimeException("Stub!"); }

/**
 * {@hide}
 * Returns the current standby bucket of the specified app. The caller must hold the permission
 * android.permission.PACKAGE_USAGE_STATS.
 * <br>
 * Requires {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param packageName the package for which to fetch the current standby bucket.

 * @return Value is {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_EXEMPTED}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_ACTIVE}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_WORKING_SET}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_FREQUENT}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_RARE}, or {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_NEVER}
 */

public int getAppStandbyBucket(java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * {@hide}
 * Changes an app's standby bucket to the provided value. The caller can only set the standby
 * bucket for a different app than itself.
 * <br>
 * Requires {@link android.Manifest.permission#CHANGE_APP_IDLE_STATE}
 * @param packageName the package name of the app to set the bucket for. A SecurityException
 *                    will be thrown if the package name is that of the caller.
 * @param bucket the standby bucket to set it to, which should be one of STANDBY_BUCKET_*.
 *               Setting a standby bucket outside of the range of STANDBY_BUCKET_ACTIVE to
 *               STANDBY_BUCKET_NEVER will result in a SecurityException.

 * Value is {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_EXEMPTED}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_ACTIVE}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_WORKING_SET}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_FREQUENT}, {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_RARE}, or {@link android.app.usage.UsageStatsManager#STANDBY_BUCKET_NEVER}
 */

public void setAppStandbyBucket(java.lang.String packageName, int bucket) { throw new RuntimeException("Stub!"); }

/**
 * {@hide}
 * Returns the current standby bucket of every app that has a bucket assigned to it.
 * The caller must hold the permission android.permission.PACKAGE_USAGE_STATS. The key of the
 * returned Map is the package name and the value is the bucket assigned to the package.
 * <br>
 * Requires {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @see #getAppStandbyBucket()
 */

public java.util.Map<java.lang.String,java.lang.Integer> getAppStandbyBuckets() { throw new RuntimeException("Stub!"); }

/**
 * {@hide}
 * Changes the app standby bucket for multiple apps at once. The Map is keyed by the package
 * name and the value is one of STANDBY_BUCKET_*.
 * <br>
 * Requires {@link android.Manifest.permission#CHANGE_APP_IDLE_STATE}
 * @param appBuckets a map of package name to bucket value.
 */

public void setAppStandbyBuckets(java.util.Map<java.lang.String,java.lang.Integer> appBuckets) { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#OBSERVE_APP_USAGE}
 * @hide
 * Register an app usage limit observer that receives a callback on the provided intent when
 * the sum of usages of apps and tokens in the {@code observed} array exceeds the
 * {@code timeLimit} specified. The structure of a token is a String with the reporting
 * package's name and a token the reporting app will use, separated by the forward slash
 * character. Example: com.reporting.package/5OM3*0P4QU3-7OK3N
 * The observer will automatically be unregistered when the time limit is reached and the
 * intent is delivered. Registering an {@code observerId} that was already registered will
 * override the previous one. No more than 1000 unique {@code observerId} may be registered by
 * a single uid at any one time.
 * @param observerId A unique id associated with the group of apps to be monitored. There can
 *                  be multiple groups with common packages and different time limits.
 * @param observedEntities The list of packages and token to observe for usage time. Cannot be
 *                         null and must include at least one package or token.
 * This value must never be {@code null}.
 * @param timeLimit The total time the set of apps can be in the foreground before the
 *                  callbackIntent is delivered. Must be at least one minute.
 * @param timeUnit The unit for time specified in {@code timeLimit}. Cannot be null.
 * This value must never be {@code null}.
 * @param callbackIntent The PendingIntent that will be dispatched when the usage limit is
 *                       exceeded by the group of apps. The delivered Intent will also contain
 *                       the extras {@link #EXTRA_OBSERVER_ID}, {@link #EXTRA_TIME_LIMIT} and
 *                       {@link #EXTRA_TIME_USED}. Cannot be null.
 * This value must never be {@code null}.
 * @throws SecurityException if the caller doesn't have the OBSERVE_APP_USAGE permission and
 *                           is not the profile owner of this user.
 */

public void registerAppUsageObserver(int observerId, @android.annotation.NonNull java.lang.String[] observedEntities, long timeLimit, @android.annotation.NonNull java.util.concurrent.TimeUnit timeUnit, @android.annotation.NonNull android.app.PendingIntent callbackIntent) { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#OBSERVE_APP_USAGE}
 * @hide
 * Unregister the app usage observer specified by the {@code observerId}. This will only apply
 * to any observer registered by this application. Unregistering an observer that was already
 * unregistered or never registered will have no effect.
 * @param observerId The id of the observer that was previously registered.
 * @throws SecurityException if the caller doesn't have the OBSERVE_APP_USAGE permission and is
 *                           not the profile owner of this user.
 */

public void unregisterAppUsageObserver(int observerId) { throw new RuntimeException("Stub!"); }

/**
 * Register a usage session observer that receives a callback on the provided {@code
 * limitReachedCallbackIntent} when the sum of usages of apps and tokens in the {@code
 * observed} array exceeds the {@code timeLimit} specified within a usage session. The
 * structure of a token is a String with the reporting packages' name and a token the
 * reporting app will use, separated by the forward slash character.
 * Example: com.reporting.package/5OM3*0P4QU3-7OK3N
 * After the {@code timeLimit} has been reached, the usage session observer will receive a
 * callback on the provided {@code sessionEndCallbackIntent} when the usage session ends.
 * Registering another session observer against a {@code sessionObserverId} that has already
 * been registered will override the previous session observer.
 *
 * <br>
 * Requires {@link android.Manifest.permission#OBSERVE_APP_USAGE}
 * @param sessionObserverId A unique id associated with the group of apps to be
 *                          monitored. There can be multiple groups with common
 *                          packages and different time limits.
 * @param observedEntities The list of packages and token to observe for usage time. Cannot be
 *                         null and must include at least one package or token.
 * This value must never be {@code null}.
 * @param timeLimit The total time the set of apps can be used continuously before the {@code
 *                  limitReachedCallbackIntent} is delivered. Must be at least one minute.
 * This value must never be {@code null}.
 * @param sessionThresholdTime The time that can take place between usage sessions before the
 *                             next session is considered a new session. Must be non-negative.
 * This value must never be {@code null}.
 * @param limitReachedCallbackIntent The {@link PendingIntent} that will be dispatched when the
 *                                   usage limit is exceeded by the group of apps. The
 *                                   delivered Intent will also contain the extras {@link
 *                                   #EXTRA_OBSERVER_ID}, {@link #EXTRA_TIME_LIMIT} and {@link
 *                                   #EXTRA_TIME_USED}. Cannot be null.
 * This value must never be {@code null}.
 * @param sessionEndCallbackIntent The {@link PendingIntent}  that will be dispatched when the
 *                                 session has ended after the usage limit has been exceeded.
 *                                 The session is considered at its end after the {@code
 *                                 observed} usage has stopped and an additional {@code
 *                                 sessionThresholdTime} has passed. The delivered Intent will
 *                                 also contain the extras {@link #EXTRA_OBSERVER_ID} and {@link
 *                                 #EXTRA_TIME_USED}. Can be null.
 * This value may be {@code null}.
 * @throws SecurityException if the caller doesn't have the OBSERVE_APP_USAGE permission and
 *                           is not the profile owner of this user.
 * @hide
 */

public void registerUsageSessionObserver(int sessionObserverId, @android.annotation.NonNull java.lang.String[] observedEntities, @android.annotation.NonNull java.time.Duration timeLimit, @android.annotation.NonNull java.time.Duration sessionThresholdTime, @android.annotation.NonNull android.app.PendingIntent limitReachedCallbackIntent, @android.annotation.Nullable android.app.PendingIntent sessionEndCallbackIntent) { throw new RuntimeException("Stub!"); }

/**
 * Unregister the usage session observer specified by the {@code sessionObserverId}. This will
 * only apply to any app session observer registered by this application. Unregistering an
 * observer that was already unregistered or never registered will have no effect.
 *
 * <br>
 * Requires {@link android.Manifest.permission#OBSERVE_APP_USAGE}
 * @param sessionObserverId The id of the observer that was previously registered.
 * @throws SecurityException if the caller doesn't have the OBSERVE_APP_USAGE permission and
 *                           is not the profile owner of this user.
 * @hide
 */

public void unregisterUsageSessionObserver(int sessionObserverId) { throw new RuntimeException("Stub!"); }

/**
 * Register a usage limit observer that receives a callback on the provided intent when the
 * sum of usages of apps and tokens in the provided {@code observedEntities} array exceeds the
 * {@code timeLimit} specified. The structure of a token is a {@link String} with the reporting
 * package's name and a token that the calling app will use, separated by the forward slash
 * character. Example: com.reporting.package/5OM3*0P4QU3-7OK3N
 * <p>
 * Registering an {@code observerId} that was already registered will override the previous one.
 * No more than 1000 unique {@code observerId} may be registered by a single uid
 * at any one time.
 * A limit is not cleared when the usage time is exceeded. It needs to be unregistered via
 * {@link #unregisterAppUsageLimitObserver}.
 * <p>
 * Note: usage limits are not persisted in the system and are cleared on reboots. Callers
 * must reset any limits that they need on reboots.
 * <p>
 * This method is similar to {@link #registerAppUsageObserver}, but the usage limit set here
 * will be visible to the launcher so that it can report the limit to the user and how much
 * of it is remaining.
 * <br>
 * Requires {@link android.Manifest.permission#SUSPEND_APPS} and {@link android.Manifest.permission#OBSERVE_APP_USAGE}
 * @see android.content.pm.LauncherApps#getAppUsageLimit
 *
 * @param observerId A unique id associated with the group of apps to be monitored. There can
 *                   be multiple groups with common packages and different time limits.
 * @param observedEntities The list of packages and token to observe for usage time. Cannot be
 *                         null and must include at least one package or token.
 * This value must never be {@code null}.
 * @param timeLimit The total time the set of apps can be in the foreground before the
 *                  {@code callbackIntent} is delivered. Must be at least one minute.
 * This value must never be {@code null}.
 * @param timeUsed The time that has already been used by the set of apps in
 *                 {@code observedEntities}. Note: a time used equal to or greater than
 *                 {@code timeLimit} can be set to indicate that the user has already exhausted
 *                 the limit for a group, in which case, the given {@code callbackIntent} will
 *                 be ignored.
 * This value must never be {@code null}.
 * @param callbackIntent The PendingIntent that will be dispatched when the usage limit is
 *                       exceeded by the group of apps. The delivered Intent will also contain
 *                       the extras {@link #EXTRA_OBSERVER_ID}, {@link #EXTRA_TIME_LIMIT} and
 *                       {@link #EXTRA_TIME_USED}. Cannot be {@code null} unless the observer is
 *                       being registered with a {@code timeUsed} equal to or greater than
 *                       {@code timeLimit}.
 * This value may be {@code null}.
 * @throws SecurityException if the caller doesn't have both SUSPEND_APPS and OBSERVE_APP_USAGE
 *                           permissions.
 * @hide
 */

public void registerAppUsageLimitObserver(int observerId, @android.annotation.NonNull java.lang.String[] observedEntities, @android.annotation.NonNull java.time.Duration timeLimit, @android.annotation.NonNull java.time.Duration timeUsed, @android.annotation.Nullable android.app.PendingIntent callbackIntent) { throw new RuntimeException("Stub!"); }

/**
 * Unregister the app usage limit observer specified by the {@code observerId}.
 * This will only apply to any observer registered by this application. Unregistering
 * an observer that was already unregistered or never registered will have no effect.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SUSPEND_APPS} and {@link android.Manifest.permission#OBSERVE_APP_USAGE}
 * @param observerId The id of the observer that was previously registered.
 * @throws SecurityException if the caller doesn't have both SUSPEND_APPS and OBSERVE_APP_USAGE
 *                           permissions.
 * @hide
 */

public void unregisterAppUsageLimitObserver(int observerId) { throw new RuntimeException("Stub!"); }

/**
 * Report usage associated with a particular {@code token} has started. Tokens are app defined
 * strings used to represent usage of in-app features. Apps with the {@link
 * android.Manifest.permission#OBSERVE_APP_USAGE} permission can register time limit observers
 * to monitor the usage of a token. In app usage can only associated with an {@code activity}
 * and usage will be considered stopped if the activity stops or crashes.
 * @see #registerAppUsageObserver
 * @see #registerUsageSessionObserver
 * @see #registerAppUsageLimitObserver
 *
 * @param activity The activity {@code token} is associated with.
 * This value must never be {@code null}.
 * @param token The token to report usage against.
 * This value must never be {@code null}.
 * @hide
 */

public void reportUsageStart(@android.annotation.NonNull android.app.Activity activity, @android.annotation.NonNull java.lang.String token) { throw new RuntimeException("Stub!"); }

/**
 * Report usage associated with a particular {@code token} had started some amount of time in
 * the past. Tokens are app defined strings used to represent usage of in-app features. Apps
 * with the {@link android.Manifest.permission#OBSERVE_APP_USAGE} permission can register time
 * limit observers to monitor the usage of a token. In app usage can only associated with an
 * {@code activity} and usage will be considered stopped if the activity stops or crashes.
 * @see #registerAppUsageObserver
 * @see #registerUsageSessionObserver
 * @see #registerAppUsageLimitObserver
 *
 * @param activity The activity {@code token} is associated with.
 * This value must never be {@code null}.
 * @param token The token to report usage against.
 * This value must never be {@code null}.
 * @param timeAgoMs How long ago the start of usage took place
 * @hide
 */

public void reportUsageStart(@android.annotation.NonNull android.app.Activity activity, @android.annotation.NonNull java.lang.String token, long timeAgoMs) { throw new RuntimeException("Stub!"); }

/**
 * Report the usage associated with a particular {@code token} has stopped.
 *
 * @param activity The activity {@code token} is associated with.
 * This value must never be {@code null}.
 * @param token The token to report usage against.
 * This value must never be {@code null}.
 * @hide
 */

public void reportUsageStop(@android.annotation.NonNull android.app.Activity activity, @android.annotation.NonNull java.lang.String token) { throw new RuntimeException("Stub!"); }

/**
 * Get what App Usage Observers will consider the source of usage for an activity. Usage Source
 * is decided at boot and will not change until next boot.
 * @see #USAGE_SOURCE_TASK_ROOT_ACTIVITY
 * @see #USAGE_SOURCE_CURRENT_ACTIVITY
 *
 * @throws SecurityException if the caller doesn't have the OBSERVE_APP_USAGE permission and
 *                           is not the profile owner of this user.
 * @hide

 * @return Value is {@link android.app.usage.UsageStatsManager#USAGE_SOURCE_TASK_ROOT_ACTIVITY}, or {@link android.app.usage.UsageStatsManager#USAGE_SOURCE_CURRENT_ACTIVITY}
 */

public int getUsageSource() { throw new RuntimeException("Stub!"); }

/**
 * {@hide}
 * Temporarily whitelist the specified app for a short duration. This is to allow an app
 * receiving a high priority message to be able to access the network and acquire wakelocks
 * even if the device is in power-save mode or the app is currently considered inactive.
 * <br>
 * Requires {@link android.Manifest.permission#CHANGE_DEVICE_IDLE_TEMP_WHITELIST}
 * @param packageName The package name of the app to whitelist.
 * @param duration Duration to whitelist the app for, in milliseconds. It is recommended that
 * this be limited to 10s of seconds. Requested duration will be clamped to a few minutes.
 * @param user The user for whom the package should be whitelisted. Passing in a user that is
 * not the same as the caller's process will require the INTERACT_ACROSS_USERS permission.
 * @see #isAppInactive(String)
 */

public void whitelistAppTemporarily(java.lang.String packageName, long duration, android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Observer id of the registered observer for the group of packages that reached the usage
 * time limit. Included as an extra in the PendingIntent that was registered.
 * @hide
 */

public static final java.lang.String EXTRA_OBSERVER_ID = "android.app.usage.extra.OBSERVER_ID";

/**
 * Original time limit in milliseconds specified by the registered observer for the group of
 * packages that reached the usage time limit. Included as an extra in the PendingIntent that
 * was registered.
 * @hide
 */

public static final java.lang.String EXTRA_TIME_LIMIT = "android.app.usage.extra.TIME_LIMIT";

/**
 * Actual usage time in milliseconds for the group of packages that reached the specified time
 * limit. Included as an extra in the PendingIntent that was registered.
 * @hide
 */

public static final java.lang.String EXTRA_TIME_USED = "android.app.usage.extra.TIME_USED";

/**
 * An interval type that will use the best fit interval for the given time range.
 * See {@link #queryUsageStats(int, long, long)}.
 * @apiSince 21
 */

public static final int INTERVAL_BEST = 4; // 0x4

/**
 * An interval type that spans a day. See {@link #queryUsageStats(int, long, long)}.
 * @apiSince 21
 */

public static final int INTERVAL_DAILY = 0; // 0x0

/**
 * An interval type that spans a month. See {@link #queryUsageStats(int, long, long)}.
 * @apiSince 21
 */

public static final int INTERVAL_MONTHLY = 2; // 0x2

/**
 * An interval type that spans a week. See {@link #queryUsageStats(int, long, long)}.
 * @apiSince 21
 */

public static final int INTERVAL_WEEKLY = 1; // 0x1

/**
 * An interval type that spans a year. See {@link #queryUsageStats(int, long, long)}.
 * @apiSince 21
 */

public static final int INTERVAL_YEARLY = 3; // 0x3

/**
 * The app was used very recently, currently in use or likely to be used very soon. Standby
 * bucket values that are &le; {@link #STANDBY_BUCKET_ACTIVE} will not be throttled by the
 * system while they are in this bucket. Buckets &gt; {@link #STANDBY_BUCKET_ACTIVE} will most
 * likely be restricted in some way. For instance, jobs and alarms may be deferred.
 * @see #getAppStandbyBucket()
 * @apiSince 28
 */

public static final int STANDBY_BUCKET_ACTIVE = 10; // 0xa

/**
 * The app is whitelisted for some reason and the bucket cannot be changed.
 * {@hide}
 */

public static final int STANDBY_BUCKET_EXEMPTED = 5; // 0x5

/**
 * The app was used in the last few days and/or likely to be used in the next few days.
 * Restrictions will apply to these apps, such as deferral of jobs and alarms. The delays may be
 * greater than for apps in higher buckets (lower bucket value). Bucket values &gt;
 * {@link #STANDBY_BUCKET_FREQUENT} may additionally have network access limited.
 * @see #getAppStandbyBucket()
 * @apiSince 28
 */

public static final int STANDBY_BUCKET_FREQUENT = 30; // 0x1e

/**
 * The app has never been used.
 * {@hide}
 */

public static final int STANDBY_BUCKET_NEVER = 50; // 0x32

/**
 * The app has not be used for several days and/or is unlikely to be used for several days.
 * Apps in this bucket will have the most restrictions, including network restrictions, except
 * during certain short periods (at a minimum, once a day) when they are allowed to execute
 * jobs, access the network, etc.
 * @see #getAppStandbyBucket()
 * @apiSince 28
 */

public static final int STANDBY_BUCKET_RARE = 40; // 0x28

/**
 * The app was used recently and/or likely to be used in the next few hours. Restrictions will
 * apply to these apps, such as deferral of jobs and alarms.
 * @see #getAppStandbyBucket()
 * @apiSince 28
 */

public static final int STANDBY_BUCKET_WORKING_SET = 20; // 0x14

/**
 * App usage observers will consider the visible activity's package the source of usage.
 * @hide
 */

public static final int USAGE_SOURCE_CURRENT_ACTIVITY = 2; // 0x2

/**
 * App usage observers will consider the task root package the source of usage.
 * @hide
 */

public static final int USAGE_SOURCE_TASK_ROOT_ACTIVITY = 1; // 0x1
}

