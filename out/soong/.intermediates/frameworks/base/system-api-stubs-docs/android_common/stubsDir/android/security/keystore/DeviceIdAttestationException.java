/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.security.keystore;


/**
 * Thrown when {@link AttestationUtils} is unable to attest the given device ids.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DeviceIdAttestationException extends java.lang.Exception {

/**
 * Constructs a new {@code DeviceIdAttestationException} with the current stack trace and the
 * specified detail message.
 *
 * @param detailMessage the detail message for this exception.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public DeviceIdAttestationException(@android.annotation.Nullable java.lang.String detailMessage) { throw new RuntimeException("Stub!"); }

/**
 * Constructs a new {@code DeviceIdAttestationException} with the current stack trace, the
 * specified detail message and the specified cause.
 *
 * @param message the detail message for this exception.
 * This value may be {@code null}.
 * @param cause the cause of this exception, may be {@code null}.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public DeviceIdAttestationException(@android.annotation.Nullable java.lang.String message, @android.annotation.Nullable java.lang.Throwable cause) { throw new RuntimeException("Stub!"); }
}

