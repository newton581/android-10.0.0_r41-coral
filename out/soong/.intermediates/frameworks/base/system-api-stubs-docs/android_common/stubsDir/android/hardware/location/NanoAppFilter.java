/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.location;


/**
 * @deprecated Use {@link android.hardware.location.ContextHubManager#queryNanoApps(ContextHubInfo)}
 *             to find loaded nanoapps, which doesn't require using this class as a parameter.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class NanoAppFilter implements android.os.Parcelable {

/**
 * Create a filter
 *
 * @param appId       application id
 * @param appVersion  application version
 * @param versionMask version
 * @param vendorMask  vendor
 * @apiSince REL
 */

@Deprecated
public NanoAppFilter(long appId, int appVersion, int versionMask, long vendorMask) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Test match method.
 *
 * @param info nano app instance info
 *
 * @return true if this is a match, false otherwise
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean testMatch(android.hardware.location.NanoAppInstanceInfo info) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * If this flag is set, only versions strictly equal to the version specified shall match.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int APP_ANY = -1; // 0xffffffff

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated @android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.location.NanoAppFilter> CREATOR;
static { CREATOR = null; }

/**
 * Flag indicating any version. With this flag set, all versions shall match provided version.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int FLAGS_VERSION_ANY = -1; // 0xffffffff

/**
 * If this flag is set, only versions strictly greater than the version specified shall match.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int FLAGS_VERSION_GREAT_THAN = 2; // 0x2

/**
 * If this flag is set, only versions strictly less than the version specified shall match.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int FLAGS_VERSION_LESS_THAN = 4; // 0x4

/**
 * If this flag is set, only versions strictly equal to the
 * version specified shall match.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int FLAGS_VERSION_STRICTLY_EQUAL = 8; // 0x8

/**
 * If this flag is set, any hub shall match.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int HUB_ANY = -1; // 0xffffffff

/**
 * If this flag is set, all vendors shall match.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int VENDOR_ANY = -1; // 0xffffffff
}

