/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony.ims;

import android.telephony.SubscriptionManager;
import android.content.Context;
import android.Manifest;
import java.util.concurrent.Executor;
import android.telephony.ims.stub.ImsConfigImplBase;
import android.telephony.CarrierConfigManager;
import android.telephony.ims.stub.ImsRegistrationImplBase;
import android.telephony.ims.feature.MmTelFeature;

/**
 * Manages IMS provisioning and configuration parameters, as well as callbacks for apps to listen
 * to changes in these configurations.
 *
 * IMS provisioning keys are defined per carrier or OEM using OMA-DM or other provisioning
 * applications and may vary. It is up to the carrier and OEM applications to ensure that the
 * correct provisioning keys are being used when integrating with a vendor's ImsService.
 *
 * Note: For compatibility purposes, the integer values [0 - 99] used in
 * {@link #setProvisioningIntValue(int, int)} have been reserved for existing provisioning keys
 * previously defined in the Android framework. Please do not redefine new provisioning keys in this
 * range or it may generate collisions with existing keys. Some common constants have also been
 * defined in this class to make integrating with other system apps easier.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ProvisioningManager {

ProvisioningManager(int subId) { throw new RuntimeException("Stub!"); }

/**
 * Create a new {@link ProvisioningManager} for the subscription specified.
 *
 * @param subId The ID of the subscription that this ProvisioningManager will use.
 * @see android.telephony.SubscriptionManager#getActiveSubscriptionInfoList()
 * @throws IllegalArgumentException if the subscription is invalid.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static android.telephony.ims.ProvisioningManager createForSubscriptionId(int subId) { throw new RuntimeException("Stub!"); }

/**
 * Register a new {@link Callback} to listen to changes to changes in IMS provisioning.
 *
 * When the subscription associated with this callback is removed (SIM removed, ESIM swap,
 * etc...), this callback will automatically be removed.
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param executor The {@link Executor} to call the callback methods on
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The provisioning callbackto be registered.
 * This value must never be {@code null}.
 * @see #unregisterProvisioningChangedCallback(Callback)
 * @see SubscriptionManager.OnSubscriptionsChangedListener
 * @throws IllegalArgumentException if the subscription associated with this callback is not
 * active (SIM is not inserted, ESIM inactive) or the subscription is invalid.
 * @throws ImsException if the subscription associated with this callback is valid, but
 * the {@link ImsService} associated with the subscription is not available. This can happen if
 * the service crashed, for example. See {@link ImsException#getCode()} for a more detailed
 * reason.
 * @apiSince REL
 */

public void registerProvisioningChangedCallback(@android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.telephony.ims.ProvisioningManager.Callback callback) throws android.telephony.ims.ImsException { throw new RuntimeException("Stub!"); }

/**
 * Unregister an existing {@link Callback}. When the subscription associated with this
 * callback is removed (SIM removed, ESIM swap, etc...), this callback will automatically be
 * removed. If this method is called for an inactive subscription, it will result in a no-op.
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param callback The existing {@link Callback} to be removed.
 * This value must never be {@code null}.
 * @see #registerProvisioningChangedCallback(Executor, Callback)
 *
 * @throws IllegalArgumentException if the subscription associated with this callback is
 * invalid.
 * @apiSince REL
 */

public void unregisterProvisioningChangedCallback(@android.annotation.NonNull android.telephony.ims.ProvisioningManager.Callback callback) { throw new RuntimeException("Stub!"); }

/**
 * Query for the integer value associated with the provided key.
 *
 * This operation is blocking and should not be performed on the UI thread.
 *
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param key An integer that represents the provisioning key, which is defined by the OEM.
 * @return an integer value for the provided key, or
 * {@link ImsConfigImplBase#CONFIG_RESULT_UNKNOWN} if the key doesn't exist.
 * @throws IllegalArgumentException if the key provided was invalid.
 * @apiSince REL
 */

public int getProvisioningIntValue(int key) { throw new RuntimeException("Stub!"); }

/**
 * Query for the String value associated with the provided key.
 *
 * This operation is blocking and should not be performed on the UI thread.
 *
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param key A String that represents the provisioning key, which is defined by the OEM.
 * @return a String value for the provided key, {@code null} if the key doesn't exist, or
 * {@link StringResultError} if there was an error getting the value for the provided key.
 * Value is {@link android.telephony.ims.ProvisioningManager#STRING_QUERY_RESULT_ERROR_GENERIC}, or {@link android.telephony.ims.ProvisioningManager#STRING_QUERY_RESULT_ERROR_NOT_READY}
 * @throws IllegalArgumentException if the key provided was invalid.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getProvisioningStringValue(int key) { throw new RuntimeException("Stub!"); }

/**
 * Set the integer value associated with the provided key.
 *
 * This operation is blocking and should not be performed on the UI thread.
 *
 * Use {@link #setProvisioningStringValue(int, String)} with proper namespacing (to be defined
 * per OEM or carrier) when possible instead to avoid key collision if needed.
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param key An integer that represents the provisioning key, which is defined by the OEM.
 * @param value a integer value for the provided key.
 * @return the result of setting the configuration value.
 
 * Value is {@link android.telephony.ims.stub.ImsConfigImplBase#CONFIG_RESULT_SUCCESS}, or {@link android.telephony.ims.stub.ImsConfigImplBase#CONFIG_RESULT_FAILED}
 * @apiSince REL
 */

public int setProvisioningIntValue(int key, int value) { throw new RuntimeException("Stub!"); }

/**
 * Set the String value associated with the provided key.
 *
 * This operation is blocking and should not be performed on the UI thread.
 *
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param key A String that represents the provisioning key, which is defined by the OEM and
 *     should be appropriately namespaced to avoid collision.
 * @param value a String value for the provided key.
 * This value must never be {@code null}.
 * @return the result of setting the configuration value.
 
 * Value is {@link android.telephony.ims.stub.ImsConfigImplBase#CONFIG_RESULT_SUCCESS}, or {@link android.telephony.ims.stub.ImsConfigImplBase#CONFIG_RESULT_FAILED}
 * @apiSince REL
 */

public int setProvisioningStringValue(int key, @android.annotation.NonNull java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Set the provisioning status for the IMS MmTel capability using the specified subscription.
 *
 * Provisioning may or may not be required, depending on the carrier configuration. If
 * provisioning is not required for the carrier associated with this subscription or the device
 * does not support the capability/technology combination specified, this operation will be a
 * no-op.
 *
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @see CarrierConfigManager#KEY_CARRIER_UT_PROVISIONING_REQUIRED_BOOL
 * @see CarrierConfigManager#KEY_CARRIER_VOLTE_PROVISIONING_REQUIRED_BOOL
 * @param isProvisioned true if the device is provisioned for UT over IMS, false otherwise.
 
 * @param capability Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 
 * @param tech Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @apiSince REL
 */

public void setProvisioningStatusForCapability(int capability, int tech, boolean isProvisioned) { throw new RuntimeException("Stub!"); }

/**
 * Get the provisioning status for the IMS MmTel capability specified.
 *
 * If provisioning is not required for the queried
 * {@link MmTelFeature.MmTelCapabilities.MmTelCapability} and
 * {@link ImsRegistrationImplBase.ImsRegistrationTech} combination specified, this method will
 * always return {@code true}.
 *
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @see CarrierConfigManager#KEY_CARRIER_UT_PROVISIONING_REQUIRED_BOOL
 * @see CarrierConfigManager#KEY_CARRIER_VOLTE_PROVISIONING_REQUIRED_BOOL
 * @param capability Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @param tech Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @return true if the device is provisioned for the capability or does not require
 * provisioning, false if the capability does require provisioning and has not been
 * provisioned yet.
 * @apiSince REL
 */

public boolean getProvisioningStatusForCapability(int capability, int tech) { throw new RuntimeException("Stub!"); }

/**
 * Override the user-defined WiFi mode for this subscription, defined in
 * {@link SubscriptionManager#WFC_MODE_CONTENT_URI}, for the purposes of provisioning
 * this subscription for WiFi Calling.
 *
 * Valid values for this key are:
 * {@link ImsMmTelManager#WIFI_MODE_WIFI_ONLY},
 * {@link ImsMmTelManager#WIFI_MODE_CELLULAR_PREFERRED}, or
 * {@link ImsMmTelManager#WIFI_MODE_WIFI_PREFERRED}.
 *
 * @see #getProvisioningIntValue(int)
 * @see #setProvisioningIntValue(int, int)
 * @apiSince REL
 */

public static final int KEY_VOICE_OVER_WIFI_MODE_OVERRIDE = 27; // 0x1b

/**
 * Override the user-defined WiFi Roaming enabled setting for this subscription, defined in
 * {@link SubscriptionManager#WFC_ROAMING_ENABLED_CONTENT_URI}, for the purposes of provisioning
 * the subscription for WiFi Calling.
 *
 * @see #getProvisioningIntValue(int)
 * @see #setProvisioningIntValue(int, int)
 * @apiSince REL
 */

public static final int KEY_VOICE_OVER_WIFI_ROAMING_ENABLED_OVERRIDE = 26; // 0x1a

/**
 * The integer result of provisioning for the queried key is disabled.
 * @apiSince REL
 */

public static final int PROVISIONING_VALUE_DISABLED = 0; // 0x0

/**
 * The integer result of provisioning for the queried key is enabled.
 * @apiSince REL
 */

public static final int PROVISIONING_VALUE_ENABLED = 1; // 0x1

/**
 * The query from {@link #getProvisioningStringValue(int)} has resulted in an unspecified error.
 * @apiSince REL
 */

public static final java.lang.String STRING_QUERY_RESULT_ERROR_GENERIC = "STRING_QUERY_RESULT_ERROR_GENERIC";

/**
 * The query from {@link #getProvisioningStringValue(int)} has resulted in an error because the
 * ImsService implementation was not ready for provisioning queries.
 * @apiSince REL
 */

public static final java.lang.String STRING_QUERY_RESULT_ERROR_NOT_READY = "STRING_QUERY_RESULT_ERROR_NOT_READY";
/**
 * Callback for IMS provisioning changes.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Callback {

public Callback() { throw new RuntimeException("Stub!"); }

/**
 * Called when a provisioning item has changed.
 * @param item the IMS provisioning key constant, as defined by the OEM.
 * @param value the new integer value of the IMS provisioning key.
 * @apiSince REL
 */

public void onProvisioningIntChanged(int item, int value) { throw new RuntimeException("Stub!"); }

/**
 * Called when a provisioning item has changed.
 * @param item the IMS provisioning key constant, as defined by the OEM.
 * @param value the new String value of the IMS configuration constant.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onProvisioningStringChanged(int item, @android.annotation.NonNull java.lang.String value) { throw new RuntimeException("Stub!"); }
}

}

