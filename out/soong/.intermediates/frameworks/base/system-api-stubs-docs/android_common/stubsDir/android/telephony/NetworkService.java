/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;


/**
 * Base class of network service. Services that extend NetworkService must register the service in
 * their AndroidManifest to be detected by the framework. They must be protected by the permission
 * "android.permission.BIND_TELEPHONY_NETWORK_SERVICE". The network service definition in the
 * manifest must follow the following format:
 * ...
 * <service android:name=".xxxNetworkService"
 *     android:permission="android.permission.BIND_TELEPHONY_NETWORK_SERVICE" >
 *     <intent-filter>
 *         <action android:name="android.telephony.NetworkService" />
 *     </intent-filter>
 * </service>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class NetworkService extends android.app.Service {

/**
 * Default constructor.
 * @apiSince REL
 */

public NetworkService() { throw new RuntimeException("Stub!"); }

/**
 * Create the instance of {@link NetworkServiceProvider}. Network service provider must override
 * this method to facilitate the creation of {@link NetworkServiceProvider} instances. The system
 * will call this method after binding the network service for each active SIM slot id.
 *
 * @param slotIndex SIM slot id the network service associated with.
 * @return Network service object. Null if failed to create the provider (e.g. invalid slot
 * index)
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public abstract android.telephony.NetworkService.NetworkServiceProvider onCreateNetworkServiceProvider(int slotIndex);

/** @apiSince REL */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean onUnbind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/** @hide */

public void onDestroy() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final java.lang.String SERVICE_INTERFACE = "android.telephony.NetworkService";
/**
 * The abstract class of the actual network service implementation. The network service provider
 * must extend this class to support network connection. Note that each instance of network
 * service is associated with one physical SIM slot.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class NetworkServiceProvider implements java.lang.AutoCloseable {

/**
 * Constructor
 * @param slotIndex SIM slot id the data service provider associated with.
 */

public NetworkServiceProvider(int slotIndex) { throw new RuntimeException("Stub!"); }

/**
 * @return SIM slot index the network service associated with.
 * @apiSince REL
 */

public final int getSlotIndex() { throw new RuntimeException("Stub!"); }

/**
 * Request network registration info. The result will be passed to the callback.
 *
 * @param domain Network domain
 * Value is {@link android.telephony.NetworkRegistrationInfo#DOMAIN_CS}, or {@link android.telephony.NetworkRegistrationInfo#DOMAIN_PS}
 * @param callback The callback for reporting network registration info
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void requestNetworkRegistrationInfo(int domain, @android.annotation.NonNull android.telephony.NetworkServiceCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Notify the system that network registration info is changed.
 * @apiSince REL
 */

public final void notifyNetworkRegistrationInfoChanged() { throw new RuntimeException("Stub!"); }

/**
 * Called when the instance of network service is destroyed (e.g. got unbind or binder died)
 * or when the network service provider is removed. The extended class should implement this
 * method to perform cleanup works.
 * @apiSince REL
 */

public abstract void close();
}

}

