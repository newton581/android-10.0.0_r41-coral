/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.provider;

import android.database.Cursor;
import android.app.slice.Slice;
import android.net.Uri;

/**
 * Base class for a search indexable provider. Such provider offers data to be indexed either
 * as a reference to an XML file (like a {@link android.preference.PreferenceScreen}) or either
 * as some raw data.
 *
 * @see SearchIndexableResource
 * @see SearchIndexableData
 * @see SearchIndexablesContract
 *
 * To create a search indexables provider, extend this class, then implement the abstract methods,
 * and add it to your manifest like this:
 *
 * <pre class="prettyprint">&lt;manifest&gt;
 *    ...
 *    &lt;application&gt;
 *        ...
 *        &lt;provider
 *            android:name="com.example.MyIndexablesProvider"
 *            android:authorities="com.example.myindexablesprovider"
 *            android:exported="true"
 *            android:grantUriPermissions="true"
 *            android:permission="android.permission.READ_SEARCH_INDEXABLES"
 *            &lt;intent-filter&gt;
 *                &lt;action android:name="android.content.action.SEARCH_INDEXABLES_PROVIDER" /&gt;
 *            &lt;/intent-filter&gt;
 *        &lt;/provider&gt;
 *        ...
 *    &lt;/application&gt;
 *&lt;/manifest&gt;</pre>
 * <p>
 * When defining your provider, you must protect it with
 * {@link android.Manifest.permission#READ_SEARCH_INDEXABLES}, which is a permission only the system
 * can obtain.
 * </p>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class SearchIndexablesProvider extends android.content.ContentProvider {

public SearchIndexablesProvider() { throw new RuntimeException("Stub!"); }

/**
 * Implementation is provided by the parent class.
 * @apiSince REL
 */

public void attachInfo(android.content.Context context, android.content.pm.ProviderInfo info) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.database.Cursor query(android.net.Uri uri, java.lang.String[] projection, java.lang.String selection, java.lang.String[] selectionArgs, java.lang.String sortOrder) { throw new RuntimeException("Stub!"); }

/**
 * Returns all {@link android.provider.SearchIndexablesContract.XmlResource}.
 *
 * Those are Xml resource IDs to some {@link android.preference.PreferenceScreen}.
 *
 * @param projection list of {@link android.provider.SearchIndexablesContract.XmlResource}
 *                   columns to put into the cursor. If {@code null} all supported columns
 *                   should be included.
 * @apiSince REL
 */

public abstract android.database.Cursor queryXmlResources(java.lang.String[] projection);

/**
 * Returns all {@link android.provider.SearchIndexablesContract.RawData}.
 *
 * Those are the raw indexable data.
 *
 * @param projection list of {@link android.provider.SearchIndexablesContract.RawData} columns
 *                   to put into the cursor. If {@code null} all supported columns should be
 *                   included.
 * @apiSince REL
 */

public abstract android.database.Cursor queryRawData(java.lang.String[] projection);

/**
 * Returns all {@link android.provider.SearchIndexablesContract.NonIndexableKey}.
 *
 * Those are the non indexable data keys.
 *
 * @param projection list of {@link android.provider.SearchIndexablesContract.NonIndexableKey}
 *                   columns to put into the cursor. If {@code null} all supported columns
 *                   should be included.
 * @apiSince REL
 */

public abstract android.database.Cursor queryNonIndexableKeys(java.lang.String[] projection);

/**
 * Returns a {@link Cursor} linking {@link Slice} {@link Uri Uris} to the
 * corresponding Settings key.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.database.Cursor querySliceUriPairs() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getType(android.net.Uri uri) { throw new RuntimeException("Stub!"); }

/**
 * Implementation is provided by the parent class. Throws by default, and cannot be overridden.
 * @apiSince REL
 */

public final android.net.Uri insert(android.net.Uri uri, android.content.ContentValues values) { throw new RuntimeException("Stub!"); }

/**
 * Implementation is provided by the parent class. Throws by default, and cannot be overridden.
 * @apiSince REL
 */

public final int delete(android.net.Uri uri, java.lang.String selection, java.lang.String[] selectionArgs) { throw new RuntimeException("Stub!"); }

/**
 * Implementation is provided by the parent class. Throws by default, and cannot be overridden.
 * @apiSince REL
 */

public final int update(android.net.Uri uri, android.content.ContentValues values, java.lang.String selection, java.lang.String[] selectionArgs) { throw new RuntimeException("Stub!"); }
}

