/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims.stub;

import android.telephony.ims.ImsCallSessionListener;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsStreamMediaProfile;
import android.telephony.ims.ImsReasonInfo;
import android.os.Message;
import android.os.RemoteException;
import android.telephony.ims.ImsVideoCallProvider;

/**
 * Base implementation of IImsCallSession, which implements stub versions of the methods available.
 *
 * Override the methods that your implementation of ImsCallSession supports.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ImsCallSessionImplBase implements java.lang.AutoCloseable {

public ImsCallSessionImplBase() { throw new RuntimeException("Stub!"); }

/**
 * Sets the listener to listen to the session events. An {@link ImsCallSession}
 * can only hold one listener at a time. Subsequent calls to this method
 * override the previous listener.
 *
 * @param listener {@link ImsCallSessionListener} used to notify the framework of updates
 * to the ImsCallSession
 * @apiSince REL
 */

public void setListener(android.telephony.ims.ImsCallSessionListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Closes the object. This {@link ImsCallSessionImplBase} is not usable after being closed.
 * @apiSince REL
 */

public void close() { throw new RuntimeException("Stub!"); }

/**
 * @return A String containing the unique call ID of this {@link ImsCallSessionImplBase}.
 * @apiSince REL
 */

public java.lang.String getCallId() { throw new RuntimeException("Stub!"); }

/**
 * @return The {@link ImsCallProfile} that this {@link ImsCallSessionImplBase} is associated
 * with.
 * @apiSince REL
 */

public android.telephony.ims.ImsCallProfile getCallProfile() { throw new RuntimeException("Stub!"); }

/**
 * @return The local {@link ImsCallProfile} that this {@link ImsCallSessionImplBase} is
 * associated with.
 * @apiSince REL
 */

public android.telephony.ims.ImsCallProfile getLocalCallProfile() { throw new RuntimeException("Stub!"); }

/**
 * @return The remote {@link ImsCallProfile} that this {@link ImsCallSessionImplBase} is
 * associated with.
 * @apiSince REL
 */

public android.telephony.ims.ImsCallProfile getRemoteCallProfile() { throw new RuntimeException("Stub!"); }

/**
 * @param name The String extra key.
 * @return The string extra value associated with the specified property.
 * @apiSince REL
 */

public java.lang.String getProperty(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * @return The {@link ImsCallSessionImplBase} state, defined in
 * {@link ImsCallSessionImplBase.State}.
 * @apiSince REL
 */

public int getState() { throw new RuntimeException("Stub!"); }

/**
 * @return true if the {@link ImsCallSessionImplBase} is in a call, false otherwise.
 * @apiSince REL
 */

public boolean isInCall() { throw new RuntimeException("Stub!"); }

/**
 * Mutes or unmutes the mic for the active call.
 *
 * @param muted true if the call should be muted, false otherwise.
 * @apiSince REL
 */

public void setMute(boolean muted) { throw new RuntimeException("Stub!"); }

/**
 * Initiates an IMS call with the specified number and call profile.
 * The session listener set in {@link #setListener(ImsCallSessionListener)} is called back upon
 * defined session events.
 * Only valid to call when the session state is in
 * {@link ImsCallSession.State#IDLE}.
 *
 * @param callee dialed string to make the call to
 * @param profile call profile to make the call with the specified service type,
 *      call type and media information
 * @see {@link ImsCallSession.Listener#callSessionStarted},
 * {@link ImsCallSession.Listener#callSessionStartFailed}
 * @apiSince REL
 */

public void start(java.lang.String callee, android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * Initiates an IMS call with the specified participants and call profile.
 * The session listener set in {@link #setListener(ImsCallSessionListener)} is called back upon
 * defined session events.
 * The method is only valid to call when the session state is in
 * {@link ImsCallSession.State#IDLE}.
 *
 * @param participants participant list to initiate an IMS conference call
 * @param profile call profile to make the call with the specified service type,
 *      call type and media information
 * @see {@link ImsCallSession.Listener#callSessionStarted},
 * {@link ImsCallSession.Listener#callSessionStartFailed}
 * @apiSince REL
 */

public void startConference(java.lang.String[] participants, android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * Accepts an incoming call or session update.
 *
 * @param callType call type specified in {@link ImsCallProfile} to be answered
 * @param profile stream media profile {@link ImsStreamMediaProfile} to be answered
 * @see {@link ImsCallSession.Listener#callSessionStarted}
 * @apiSince REL
 */

public void accept(int callType, android.telephony.ims.ImsStreamMediaProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * Deflects an incoming call.
 *
 * @param deflectNumber number to deflect the call
 * @apiSince REL
 */

public void deflect(java.lang.String deflectNumber) { throw new RuntimeException("Stub!"); }

/**
 * Rejects an incoming call or session update.
 *
 * @param reason reason code to reject an incoming call, defined in {@link ImsReasonInfo}.
 * {@link ImsCallSession.Listener#callSessionStartFailed}
 * @apiSince REL
 */

public void reject(int reason) { throw new RuntimeException("Stub!"); }

/**
 * Terminates a call.
 *
 * @param reason reason code to terminate a call, defined in {@link ImsReasonInfo}.
 *
 * @see {@link ImsCallSession.Listener#callSessionTerminated}
 * @apiSince REL
 */

public void terminate(int reason) { throw new RuntimeException("Stub!"); }

/**
 * Puts a call on hold. When it succeeds, {@link ImsCallSession.Listener#callSessionHeld} is
 * called.
 *
 * @param profile stream media profile {@link ImsStreamMediaProfile} to hold the call
 * @see {@link ImsCallSession.Listener#callSessionHeld},
 * {@link ImsCallSession.Listener#callSessionHoldFailed}
 * @apiSince REL
 */

public void hold(android.telephony.ims.ImsStreamMediaProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * Continues a call that's on hold. When it succeeds,
 * {@link ImsCallSession.Listener#callSessionResumed} is called.
 *
 * @param profile stream media profile with {@link ImsStreamMediaProfile} to resume the call
 * @see {@link ImsCallSession.Listener#callSessionResumed},
 * {@link ImsCallSession.Listener#callSessionResumeFailed}
 * @apiSince REL
 */

public void resume(android.telephony.ims.ImsStreamMediaProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * Merges the active and held call. When the merge starts,
 * {@link ImsCallSession.Listener#callSessionMergeStarted} is called.
 * {@link ImsCallSession.Listener#callSessionMergeComplete} is called if the merge is
 * successful, and {@link ImsCallSession.Listener#callSessionMergeFailed} is called if the merge
 * fails.
 *
 * @see {@link ImsCallSession.Listener#callSessionMergeStarted},
 * {@link ImsCallSession.Listener#callSessionMergeComplete},
 *      {@link ImsCallSession.Listener#callSessionMergeFailed}
 * @apiSince REL
 */

public void merge() { throw new RuntimeException("Stub!"); }

/**
 * Updates the current call's properties (ex. call mode change: video upgrade / downgrade).
 *
 * @param callType call type specified in {@link ImsCallProfile} to be updated
 * @param profile stream media profile {@link ImsStreamMediaProfile} to be updated
 * @see {@link ImsCallSession.Listener#callSessionUpdated},
 * {@link ImsCallSession.Listener#callSessionUpdateFailed}
 * @apiSince REL
 */

public void update(int callType, android.telephony.ims.ImsStreamMediaProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * Extends this call to the conference call with the specified recipients.
 *
 * @param participants participant list to be invited to the conference call after extending the
 * call
 * @see {@link ImsCallSession.Listener#callSessionConferenceExtended},
 * {@link ImsCallSession.Listener#callSessionConferenceExtendFailed}
 * @apiSince REL
 */

public void extendToConference(java.lang.String[] participants) { throw new RuntimeException("Stub!"); }

/**
 * Requests the conference server to invite an additional participants to the conference.
 *
 * @param participants participant list to be invited to the conference call
 * @see {@link ImsCallSession.Listener#callSessionInviteParticipantsRequestDelivered},
 *      {@link ImsCallSession.Listener#callSessionInviteParticipantsRequestFailed}
 * @apiSince REL
 */

public void inviteParticipants(java.lang.String[] participants) { throw new RuntimeException("Stub!"); }

/**
 * Requests the conference server to remove the specified participants from the conference.
 *
 * @param participants participant list to be removed from the conference call
 * @see {@link ImsCallSession.Listener#callSessionRemoveParticipantsRequestDelivered},
 *      {@link ImsCallSession.Listener#callSessionRemoveParticipantsRequestFailed}
 * @apiSince REL
 */

public void removeParticipants(java.lang.String[] participants) { throw new RuntimeException("Stub!"); }

/**
 * Sends a DTMF code. According to <a href="http://tools.ietf.org/html/rfc2833">RFC 2833</a>,
 * event 0 ~ 9 maps to decimal value 0 ~ 9, '*' to 10, '#' to 11, event 'A' ~ 'D' to 12 ~ 15,
 * and event flash to 16. Currently, event flash is not supported.
 *
 * @param c the DTMF to send. '0' ~ '9', 'A' ~ 'D', '*', '#' are valid inputs.
 * @param result If non-null, the {@link Message} to send when the operation is complete. This
 *         is done by using the associated {@link android.os.Messenger} in
 *         {@link Message#replyTo}. For example:
 * {@code
 *     // Send DTMF and other operations...
 *     try {
 *         // Notify framework that the DTMF was sent.
 *         Messenger dtmfMessenger = result.replyTo;
 *         if (dtmfMessenger != null) {
 *             dtmfMessenger.send(result);
 *         }
 *     } catch (RemoteException e) {
 *         // Remote side is dead
 *     }
 * }
 * @apiSince REL
 */

public void sendDtmf(char c, android.os.Message result) { throw new RuntimeException("Stub!"); }

/**
 * Start a DTMF code. According to <a href="http://tools.ietf.org/html/rfc2833">RFC 2833</a>,
 * event 0 ~ 9 maps to decimal value 0 ~ 9, '*' to 10, '#' to 11, event 'A' ~ 'D' to 12 ~ 15,
 * and event flash to 16. Currently, event flash is not supported.
 *
 * @param c the DTMF to send. '0' ~ '9', 'A' ~ 'D', '*', '#' are valid inputs.
 * @apiSince REL
 */

public void startDtmf(char c) { throw new RuntimeException("Stub!"); }

/**
 * Stop a DTMF code.
 * @apiSince REL
 */

public void stopDtmf() { throw new RuntimeException("Stub!"); }

/**
 * Sends an USSD message.
 *
 * @param ussdMessage USSD message to send
 * @apiSince REL
 */

public void sendUssd(java.lang.String ussdMessage) { throw new RuntimeException("Stub!"); }

/**
 * @return The {@link ImsVideoCallProvider} implementation contained within the IMS service
 * process.
 * @apiSince REL
 */

public android.telephony.ims.ImsVideoCallProvider getImsVideoCallProvider() { throw new RuntimeException("Stub!"); }

/**
 * Determines if the current session is multiparty.
 * @return {@code True} if the session is multiparty.
 * @apiSince REL
 */

public boolean isMultiparty() { throw new RuntimeException("Stub!"); }

/**
 * Device issues RTT modify request
 * @param toProfile The profile with requested changes made
 * @apiSince REL
 */

public void sendRttModifyRequest(android.telephony.ims.ImsCallProfile toProfile) { throw new RuntimeException("Stub!"); }

/**
 * Device responds to Remote RTT modify request
 * @param status true if the the request was accepted or false of the request is defined.
 * @apiSince REL
 */

public void sendRttModifyResponse(boolean status) { throw new RuntimeException("Stub!"); }

/**
 * Device sends RTT message
 * @param rttMessage RTT message to be sent
 * @apiSince REL
 */

public void sendRttMessage(java.lang.String rttMessage) { throw new RuntimeException("Stub!"); }

/**
 * Notify USSD Mode.
 * @apiSince REL
 */

public static final int USSD_MODE_NOTIFY = 0; // 0x0

/**
 * Request USSD Mode
 * @apiSince REL
 */

public static final int USSD_MODE_REQUEST = 1; // 0x1
/**
 * Defines IMS call session state.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class State {

/**
 * @hide
 */

State() { throw new RuntimeException("Stub!"); }

/**
 * Converts the state to string.
 * @apiSince REL
 */

public static java.lang.String toString(int state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int ESTABLISHED = 4; // 0x4

/** @apiSince REL */

public static final int ESTABLISHING = 3; // 0x3

/** @apiSince REL */

public static final int IDLE = 0; // 0x0

/** @apiSince REL */

public static final int INITIATED = 1; // 0x1

/** @apiSince REL */

public static final int INVALID = -1; // 0xffffffff

/** @apiSince REL */

public static final int NEGOTIATING = 2; // 0x2

/** @apiSince REL */

public static final int REESTABLISHING = 6; // 0x6

/** @apiSince REL */

public static final int RENEGOTIATING = 5; // 0x5

/** @apiSince REL */

public static final int TERMINATED = 8; // 0x8

/** @apiSince REL */

public static final int TERMINATING = 7; // 0x7
}

}

