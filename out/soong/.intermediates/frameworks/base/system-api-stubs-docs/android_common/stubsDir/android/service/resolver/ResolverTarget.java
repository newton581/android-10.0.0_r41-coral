/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.resolver;


/**
 * A ResolverTarget contains features by which an app or option will be ranked, in
 * {@link ResolverRankerService}.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ResolverTarget implements android.os.Parcelable {

/** @apiSince REL */

public ResolverTarget() { throw new RuntimeException("Stub!"); }

/**
 * Gets the score for how recently the target was used in the foreground.
 *
 * @return a float score whose range is [0, 1]. The higher the score is, the more recently the
 * target was used.
 * @apiSince REL
 */

public float getRecencyScore() { throw new RuntimeException("Stub!"); }

/**
 * Sets the score for how recently the target was used in the foreground.
 *
 * @param recencyScore a float score whose range is [0, 1]. The higher the score is, the more
 *                     recently the target was used.
 * @apiSince REL
 */

public void setRecencyScore(float recencyScore) { throw new RuntimeException("Stub!"); }

/**
 * Gets the score for how long the target has been used in the foreground.
 *
 * @return a float score whose range is [0, 1]. The higher the score is, the longer the target
 * has been used for.
 * @apiSince REL
 */

public float getTimeSpentScore() { throw new RuntimeException("Stub!"); }

/**
 * Sets the score for how long the target has been used in the foreground.
 *
 * @param timeSpentScore a float score whose range is [0, 1]. The higher the score is, the
 *                       longer the target has been used for.
 * @apiSince REL
 */

public void setTimeSpentScore(float timeSpentScore) { throw new RuntimeException("Stub!"); }

/**
 * Gets the score for how many times the target has been launched to the foreground.
 *
 * @return a float score whose range is [0, 1]. The higher the score is, the more times the
 * target has been launched.
 * @apiSince REL
 */

public float getLaunchScore() { throw new RuntimeException("Stub!"); }

/**
 * Sets the score for how many times the target has been launched to the foreground.
 *
 * @param launchScore a float score whose range is [0, 1]. The higher the score is, the more
 *                    times the target has been launched.
 * @apiSince REL
 */

public void setLaunchScore(float launchScore) { throw new RuntimeException("Stub!"); }

/**
 * Gets the score for how many times the target has been selected by the user to share the same
 * types of content.
 *
 * @return a float score whose range is [0, 1]. The higher the score is, the
 * more times the target has been selected by the user to share the same types of content for.
 * @apiSince REL
 */

public float getChooserScore() { throw new RuntimeException("Stub!"); }

/**
 * Sets the score for how many times the target has been selected by the user to share the same
 * types of content.
 *
 * @param chooserScore a float score whose range is [0, 1]. The higher the score is, the more
 *                     times the target has been selected by the user to share the same types
 *                     of content for.
 * @apiSince REL
 */

public void setChooserScore(float chooserScore) { throw new RuntimeException("Stub!"); }

/**
 * Gets the probability of how likely this target will be selected by the user.
 *
 * @return a float score whose range is [0, 1]. The higher the score is, the more likely the
 * user is going to select this target.
 * @apiSince REL
 */

public float getSelectProbability() { throw new RuntimeException("Stub!"); }

/**
 * Sets the probability for how like this target will be selected by the user.
 *
 * @param selectProbability a float score whose range is [0, 1]. The higher the score is, the
 *                          more likely tht user is going to select this target.
 * @apiSince REL
 */

public void setSelectProbability(float selectProbability) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.resolver.ResolverTarget> CREATOR;
static { CREATOR = null; }
}

