/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.location;


/**
 * A class containing a GPS clock timestamp.
 * It represents a measurement of the GPS receiver's clock.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class GpsClock implements android.os.Parcelable {

GpsClock() { throw new RuntimeException("Stub!"); }

/**
 * Sets all contents to the values stored in the provided object.
 * @apiSince REL
 */

public void set(android.location.GpsClock clock) { throw new RuntimeException("Stub!"); }

/**
 * Resets all the contents to its original state.
 * @apiSince REL
 */

public void reset() { throw new RuntimeException("Stub!"); }

/**
 * Gets the type of time reported by {@link #getTimeInNs()}.
 * @apiSince REL
 */

public byte getType() { throw new RuntimeException("Stub!"); }

/**
 * Sets the type of time reported.
 * @apiSince REL
 */

public void setType(byte value) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getLeapSecond()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasLeapSecond() { throw new RuntimeException("Stub!"); }

/**
 * Gets the leap second associated with the clock's time.
 * The sign of the value is defined by the following equation:
 *      utc_time_ns = time_ns + (full_bias_ns + bias_ns) - leap_second * 1,000,000,000
 *
 * The value is only available if {@link #hasLeapSecond()} is true.
 * @apiSince REL
 */

public short getLeapSecond() { throw new RuntimeException("Stub!"); }

/**
 * Sets the leap second associated with the clock's time.
 * @apiSince REL
 */

public void setLeapSecond(short leapSecond) { throw new RuntimeException("Stub!"); }

/**
 * Resets the leap second associated with the clock's time.
 * @apiSince REL
 */

public void resetLeapSecond() { throw new RuntimeException("Stub!"); }

/**
 * Gets the GPS receiver internal clock value in nanoseconds.
 * This can be either the 'local hardware clock' value ({@link #TYPE_LOCAL_HW_TIME}), or the
 * current GPS time derived inside GPS receiver ({@link #TYPE_GPS_TIME}).
 * {@link #getType()} defines the time reported.
 *
 * For 'local hardware clock' this value is expected to be monotonically increasing during the
 * reporting session. The real GPS time can be derived by compensating
 * {@link #getFullBiasInNs()} (when it is available) from this value.
 *
 * For 'GPS time' this value is expected to be the best estimation of current GPS time that GPS
 * receiver can achieve. {@link #getTimeUncertaintyInNs()} should be available when GPS time is
 * specified.
 *
 * Sub-nanosecond accuracy can be provided by means of {@link #getBiasInNs()}.
 * The reported time includes {@link #getTimeUncertaintyInNs()}.
 * @apiSince REL
 */

public long getTimeInNs() { throw new RuntimeException("Stub!"); }

/**
 * Sets the GPS receiver internal clock in nanoseconds.
 * @apiSince REL
 */

public void setTimeInNs(long timeInNs) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getTimeUncertaintyInNs()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasTimeUncertaintyInNs() { throw new RuntimeException("Stub!"); }

/**
 * Gets the clock's time Uncertainty (1-Sigma) in nanoseconds.
 * The uncertainty is represented as an absolute (single sided) value.
 *
 * The value is only available if {@link #hasTimeUncertaintyInNs()} is true.
 * @apiSince REL
 */

public double getTimeUncertaintyInNs() { throw new RuntimeException("Stub!"); }

/**
 * Sets the clock's Time Uncertainty (1-Sigma) in nanoseconds.
 * @apiSince REL
 */

public void setTimeUncertaintyInNs(double timeUncertaintyInNs) { throw new RuntimeException("Stub!"); }

/**
 * Resets the clock's Time Uncertainty (1-Sigma) in nanoseconds.
 * @apiSince REL
 */

public void resetTimeUncertaintyInNs() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link @getFullBiasInNs()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasFullBiasInNs() { throw new RuntimeException("Stub!"); }

/**
 * Gets the difference between hardware clock ({@link #getTimeInNs()}) inside GPS receiver and
 * the true GPS time since 0000Z, January 6, 1980, in nanoseconds.
 *
 * This value is available if {@link #TYPE_LOCAL_HW_TIME} is set, and GPS receiver has solved
 * the clock for GPS time.
 * {@link #getBiasUncertaintyInNs()} should be used for quality check.
 *
 * The sign of the value is defined by the following equation:
 *      true time (GPS time) = time_ns + (full_bias_ns + bias_ns)
 *
 * The reported full bias includes {@link #getBiasUncertaintyInNs()}.
 * The value is onl available if {@link #hasFullBiasInNs()} is true.
 * @apiSince REL
 */

public long getFullBiasInNs() { throw new RuntimeException("Stub!"); }

/**
 * Sets the full bias in nanoseconds.
 * @apiSince REL
 */

public void setFullBiasInNs(long value) { throw new RuntimeException("Stub!"); }

/**
 * Resets the full bias in nanoseconds.
 * @apiSince REL
 */

public void resetFullBiasInNs() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getBiasInNs()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasBiasInNs() { throw new RuntimeException("Stub!"); }

/**
 * Gets the clock's sub-nanosecond bias.
 * The reported bias includes {@link #getBiasUncertaintyInNs()}.
 *
 * The value is only available if {@link #hasBiasInNs()} is true.
 * @apiSince REL
 */

public double getBiasInNs() { throw new RuntimeException("Stub!"); }

/**
 * Sets the sub-nanosecond bias.
 * @apiSince REL
 */

public void setBiasInNs(double biasInNs) { throw new RuntimeException("Stub!"); }

/**
 * Resets the clock's Bias in nanoseconds.
 * @apiSince REL
 */

public void resetBiasInNs() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getBiasUncertaintyInNs()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasBiasUncertaintyInNs() { throw new RuntimeException("Stub!"); }

/**
 * Gets the clock's Bias Uncertainty (1-Sigma) in nanoseconds.
 *
 * The value is only available if {@link #hasBiasUncertaintyInNs()} is true.
 * @apiSince REL
 */

public double getBiasUncertaintyInNs() { throw new RuntimeException("Stub!"); }

/**
 * Sets the clock's Bias Uncertainty (1-Sigma) in nanoseconds.
 * @apiSince REL
 */

public void setBiasUncertaintyInNs(double biasUncertaintyInNs) { throw new RuntimeException("Stub!"); }

/**
 * Resets the clock's Bias Uncertainty (1-Sigma) in nanoseconds.
 * @apiSince REL
 */

public void resetBiasUncertaintyInNs() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getDriftInNsPerSec()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasDriftInNsPerSec() { throw new RuntimeException("Stub!"); }

/**
 * Gets the clock's Drift in nanoseconds per second.
 * A positive value indicates that the frequency is higher than the nominal frequency.
 * The reported drift includes {@link #getDriftUncertaintyInNsPerSec()}.
 *
 * The value is only available if {@link #hasDriftInNsPerSec()} is true.
 * @apiSince REL
 */

public double getDriftInNsPerSec() { throw new RuntimeException("Stub!"); }

/**
 * Sets the clock's Drift in nanoseconds per second.
 * @apiSince REL
 */

public void setDriftInNsPerSec(double driftInNsPerSec) { throw new RuntimeException("Stub!"); }

/**
 * Resets the clock's Drift in nanoseconds per second.
 * @apiSince REL
 */

public void resetDriftInNsPerSec() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if {@link #getDriftUncertaintyInNsPerSec()} is available, false otherwise.
 * @apiSince REL
 */

public boolean hasDriftUncertaintyInNsPerSec() { throw new RuntimeException("Stub!"); }

/**
 * Gets the clock's Drift Uncertainty (1-Sigma) in nanoseconds per second.
 *
 * The value is only available if {@link #hasDriftUncertaintyInNsPerSec()} is true.
 * @apiSince REL
 */

public double getDriftUncertaintyInNsPerSec() { throw new RuntimeException("Stub!"); }

/**
 * Sets the clock's Drift Uncertainty (1-Sigma) in nanoseconds per second.
 * @apiSince REL
 */

public void setDriftUncertaintyInNsPerSec(double driftUncertaintyInNsPerSec) { throw new RuntimeException("Stub!"); }

/**
 * Resets the clock's Drift Uncertainty (1-Sigma) in nanoseconds per second.
 * @apiSince REL
 */

public void resetDriftUncertaintyInNsPerSec() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.location.GpsClock> CREATOR;
static { CREATOR = null; }

/**
 * The source of the time value reported by this class is the 'GPS time' derived from
 * satellites (epoch = Jan 6, 1980).
 * @apiSince REL
 */

public static final byte TYPE_GPS_TIME = 2; // 0x2

/**
 * The source of the time value reported by this class is the 'Local Hardware Clock'.
 * @apiSince REL
 */

public static final byte TYPE_LOCAL_HW_TIME = 1; // 0x1

/**
 * The type of the time stored is not available or it is unknown.
 * @apiSince REL
 */

public static final byte TYPE_UNKNOWN = 0; // 0x0
}

