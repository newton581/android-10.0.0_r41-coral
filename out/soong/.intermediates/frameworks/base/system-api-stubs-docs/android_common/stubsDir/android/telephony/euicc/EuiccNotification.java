/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.telephony.euicc;


/**
 * This represents a signed notification which is defined in SGP.22. It can be either a profile
 * installation result or a notification generated for profile operations (e.g., enabling,
 * disabling, or deleting).
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class EuiccNotification implements android.os.Parcelable {

/**
 * Creates an instance.
 *
 * @param seq The sequence number of this notification.
 * @param targetAddr The target server where to send this notification.
 * @param event The event which causes this notification.
 * Value is either <code>0</code> or a combination of {@link android.telephony.euicc.EuiccNotification#EVENT_INSTALL}, {@link android.telephony.euicc.EuiccNotification#EVENT_ENABLE}, {@link android.telephony.euicc.EuiccNotification#EVENT_DISABLE}, and {@link android.telephony.euicc.EuiccNotification#EVENT_DELETE}
 * @param data The data which needs to be sent to the target server. This can be null for
 *     building a list of notification metadata without data.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public EuiccNotification(int seq, java.lang.String targetAddr, int event, @android.annotation.Nullable byte[] data) { throw new RuntimeException("Stub!"); }

/**
 * @return The sequence number of this notification.
 * @apiSince REL
 */

public int getSeq() { throw new RuntimeException("Stub!"); }

/**
 * @return The target server address where this notification should be sent to.
 * @apiSince REL
 */

public java.lang.String getTargetAddr() { throw new RuntimeException("Stub!"); }

/**
 * @return The event of this notification.
 * Value is either <code>0</code> or a combination of {@link android.telephony.euicc.EuiccNotification#EVENT_INSTALL}, {@link android.telephony.euicc.EuiccNotification#EVENT_ENABLE}, {@link android.telephony.euicc.EuiccNotification#EVENT_DISABLE}, and {@link android.telephony.euicc.EuiccNotification#EVENT_DELETE}
 * @apiSince REL
 */

public int getEvent() { throw new RuntimeException("Stub!"); }

/**
 * @return The notification data which needs to be sent to the target server.
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public byte[] getData() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Value of the bits of all the events including install, enable, disable and delete.
 * <br>
 * Value is either <code>0</code> or a combination of {@link android.telephony.euicc.EuiccNotification#EVENT_INSTALL}, {@link android.telephony.euicc.EuiccNotification#EVENT_ENABLE}, {@link android.telephony.euicc.EuiccNotification#EVENT_DISABLE}, and {@link android.telephony.euicc.EuiccNotification#EVENT_DELETE}
 * @apiSince REL
 */

public static final int ALL_EVENTS = 15; // 0xf

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.euicc.EuiccNotification> CREATOR;
static { CREATOR = null; }

/**
 * A profile is deleted.
 * @apiSince REL
 */

public static final int EVENT_DELETE = 8; // 0x8

/**
 * A profile is disabled.
 * @apiSince REL
 */

public static final int EVENT_DISABLE = 4; // 0x4

/**
 * A profile is enabled.
 * @apiSince REL
 */

public static final int EVENT_ENABLE = 2; // 0x2

/**
 * A profile is downloaded and installed.
 * @apiSince REL
 */

public static final int EVENT_INSTALL = 1; // 0x1
/**
 * Event
 * <br>
 * Value is either <code>0</code> or a combination of {@link android.telephony.euicc.EuiccNotification#EVENT_INSTALL}, {@link android.telephony.euicc.EuiccNotification#EVENT_ENABLE}, {@link android.telephony.euicc.EuiccNotification#EVENT_DISABLE}, and {@link android.telephony.euicc.EuiccNotification#EVENT_DELETE}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface Event {
}

}

