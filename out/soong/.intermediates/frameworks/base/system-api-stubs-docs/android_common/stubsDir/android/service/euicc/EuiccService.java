/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.euicc;

import android.app.Service;
import android.telephony.euicc.DownloadableSubscription;
import android.os.Bundle;
import android.telephony.euicc.EuiccInfo;
import android.content.Intent;

/**
 * Service interface linking the system with an eUICC local profile assistant (LPA) application.
 *
 * <p>An LPA consists of two separate components (which may both be implemented in the same APK):
 * the LPA backend, and the LPA UI or LUI.
 *
 * <p>To implement the LPA backend, you must extend this class and declare this service in your
 * manifest file. The service must require the
 * {@link android.Manifest.permission#BIND_EUICC_SERVICE} permission and include an intent filter
 * with the {@link #EUICC_SERVICE_INTERFACE} action. It's suggested that the priority of the intent
 * filter to be set to a non-zero value in case multiple implementations are present on the device.
 * See the below example. Note that there will be problem if two LPAs are present and they have the
 * same priority.
 * Example:
 *
 * <pre>{@code
 * <service android:name=".MyEuiccService"
 *          android:permission="android.permission.BIND_EUICC_SERVICE">
 *     <intent-filter android:priority="100">
 *         <action android:name="android.service.euicc.EuiccService" />
 *     </intent-filter>
 * </service>
 * }</pre>
 *
 * <p>To implement the LUI, you must provide an activity for the following actions:
 *
 * <ul>
 * <li>{@link #ACTION_MANAGE_EMBEDDED_SUBSCRIPTIONS}
 * <li>{@link #ACTION_PROVISION_EMBEDDED_SUBSCRIPTION}
 * </ul>
 *
 * <p>As with the service, each activity must require the
 * {@link android.Manifest.permission#BIND_EUICC_SERVICE} permission. Each should have an intent
 * filter with the appropriate action, the {@link #CATEGORY_EUICC_UI} category, and a non-zero
 * priority.
 *
 * <p>Old implementations of EuiccService may support passing in slot IDs equal to
 * {@link android.telephony.SubscriptionManager#INVALID_SIM_SLOT_INDEX}, which allows the LPA to
 * decide which eUICC to target when there are multiple eUICCs. This behavior is not supported in
 * Android Q or later.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class EuiccService extends android.app.Service {

/** @apiSince REL */

public EuiccService() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * If you override this method you <em>must</em> call through to the
 * superclass implementation.
 * @apiSince REL
 */

public void onCreate() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * If you override this method you <em>must</em> call through to the
 * superclass implementation.
 * @apiSince REL
 */

public void onDestroy() { throw new RuntimeException("Stub!"); }

/**
 * If overriding this method, call through to the super method for any unknown actions.
 * {@inheritDoc}
 
 * <br>
 * If you override this method you <em>must</em> call through to the
 * superclass implementation.
 * @apiSince REL
 */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Return the EID of the eUICC.
 *
 * @param slotId ID of the SIM slot being queried.
 * @return the EID.
 * @see android.telephony.euicc.EuiccManager#getEid
 * @apiSince REL
 */

public abstract java.lang.String onGetEid(int slotId);

/**
 * Return the status of OTA update.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @return The status of Euicc OTA update.
 * Value is {@link android.telephony.euicc.EuiccManager#EUICC_OTA_IN_PROGRESS}, {@link android.telephony.euicc.EuiccManager#EUICC_OTA_FAILED}, {@link android.telephony.euicc.EuiccManager#EUICC_OTA_SUCCEEDED}, {@link android.telephony.euicc.EuiccManager#EUICC_OTA_NOT_NEEDED}, or {@link android.telephony.euicc.EuiccManager#EUICC_OTA_STATUS_UNAVAILABLE}
 * @see android.telephony.euicc.EuiccManager#getOtaStatus
 * @apiSince REL
 */

public abstract int onGetOtaStatus(int slotId);

/**
 * Perform OTA if current OS is not the latest one.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @param statusChangedCallback Function called when OTA status changed.
 * @apiSince REL
 */

public abstract void onStartOtaIfNecessary(int slotId, android.service.euicc.EuiccService.OtaStatusChangedCallback statusChangedCallback);

/**
 * Populate {@link DownloadableSubscription} metadata for the given downloadable subscription.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @param subscription A subscription whose metadata needs to be populated.
 * @param forceDeactivateSim If true, and if an active SIM must be deactivated to access the
 *     eUICC, perform this action automatically. Otherwise, {@link #RESULT_MUST_DEACTIVATE_SIM)}
 *     should be returned to allow the user to consent to this operation first.
 * @return The result of the operation.
 * @see android.telephony.euicc.EuiccManager#getDownloadableSubscriptionMetadata
 * @apiSince REL
 */

public abstract android.service.euicc.GetDownloadableSubscriptionMetadataResult onGetDownloadableSubscriptionMetadata(int slotId, android.telephony.euicc.DownloadableSubscription subscription, boolean forceDeactivateSim);

/**
 * Return metadata for subscriptions which are available for download for this device.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @param forceDeactivateSim If true, and if an active SIM must be deactivated to access the
 *     eUICC, perform this action automatically. Otherwise, {@link #RESULT_MUST_DEACTIVATE_SIM)}
 *     should be returned to allow the user to consent to this operation first.
 * @return The result of the list operation.
 * @see android.telephony.euicc.EuiccManager#getDefaultDownloadableSubscriptionList
 * @apiSince REL
 */

public abstract android.service.euicc.GetDefaultDownloadableSubscriptionListResult onGetDefaultDownloadableSubscriptionList(int slotId, boolean forceDeactivateSim);

/**
 * Download the given subscription.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @param subscription The subscription to download.
 * This value must never be {@code null}.
 * @param switchAfterDownload If true, the subscription should be enabled upon successful
 *     download.
 * @param forceDeactivateSim If true, and if an active SIM must be deactivated to access the
 *     eUICC, perform this action automatically. Otherwise, {@link #RESULT_MUST_DEACTIVATE_SIM}
 *     should be returned to allow the user to consent to this operation first.
 * @param resolvedBundle The bundle containing information on resolved errors. It can contain
 *     a string of confirmation code for the key {@link #EXTRA_RESOLUTION_CONFIRMATION_CODE},
 *     and a boolean for key {@link #EXTRA_RESOLUTION_ALLOW_POLICY_RULES} indicating whether
 *     the user allows profile policy rules or not.
 * This value may be {@code null}.
 * @return a DownloadSubscriptionResult instance including a result code, a resolvable errors
 *     bit map, and original the card Id. The result code may be one of the predefined
 *     {@code RESULT_} constants or any implementation-specific code starting with
 *     {@link #RESULT_FIRST_USER}. The resolvable error bit map can be either 0 or values
 *     defined in {@code RESOLVABLE_ERROR_}. A subclass should override this method. Otherwise,
 *     this method does nothing and returns null by default.
 * @see android.telephony.euicc.EuiccManager#downloadSubscription
 * @apiSince REL
 */

public android.service.euicc.DownloadSubscriptionResult onDownloadSubscription(int slotId, @android.annotation.NonNull android.telephony.euicc.DownloadableSubscription subscription, boolean switchAfterDownload, boolean forceDeactivateSim, @android.annotation.Nullable android.os.Bundle resolvedBundle) { throw new RuntimeException("Stub!"); }

/**
 * Download the given subscription.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @param subscription The subscription to download.
 * This value must never be {@code null}.
 * @param switchAfterDownload If true, the subscription should be enabled upon successful
 *     download.
 * @param forceDeactivateSim If true, and if an active SIM must be deactivated to access the
 *     eUICC, perform this action automatically. Otherwise, {@link #RESULT_MUST_DEACTIVATE_SIM}
 *     should be returned to allow the user to consent to this operation first.
 * @return the result of the download operation. May be one of the predefined {@code RESULT_}
 *     constants or any implementation-specific code starting with {@link #RESULT_FIRST_USER}.
 * Value is {@link android.service.euicc.EuiccService#RESULT_OK}, {@link android.service.euicc.EuiccService#RESULT_MUST_DEACTIVATE_SIM}, {@link android.service.euicc.EuiccService#RESULT_RESOLVABLE_ERRORS}, {@link android.service.euicc.EuiccService#RESULT_NEED_CONFIRMATION_CODE}, or {@link android.service.euicc.EuiccService#RESULT_FIRST_USER}
 * @see android.telephony.euicc.EuiccManager#downloadSubscription
 *
 * @deprecated From Q, a subclass should use and override the above
 * {@link #onDownloadSubscription(int, DownloadableSubscription, boolean, boolean, Bundle)}. The
 * default return value for this one is Integer.MIN_VALUE.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int onDownloadSubscription(int slotId, @android.annotation.NonNull android.telephony.euicc.DownloadableSubscription subscription, boolean switchAfterDownload, boolean forceDeactivateSim) { throw new RuntimeException("Stub!"); }

/**
 * Return a list of all @link EuiccProfileInfo}s.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @return The result of the operation.
 * This value will never be {@code null}.
 * @see android.telephony.SubscriptionManager#getAvailableSubscriptionInfoList
 * @see android.telephony.SubscriptionManager#getAccessibleSubscriptionInfoList
 * @apiSince REL
 */

@android.annotation.NonNull
public abstract android.service.euicc.GetEuiccProfileInfoListResult onGetEuiccProfileInfoList(int slotId);

/**
 * Return info about the eUICC chip/device.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @return the {@link EuiccInfo} for the eUICC chip/device.
 * This value will never be {@code null}.
 * @see android.telephony.euicc.EuiccManager#getEuiccInfo
 * @apiSince REL
 */

@android.annotation.NonNull
public abstract android.telephony.euicc.EuiccInfo onGetEuiccInfo(int slotId);

/**
 * Delete the given subscription.
 *
 * <p>If the subscription is currently active, it should be deactivated first (equivalent to a
 * physical SIM being ejected).
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @param iccid the ICCID of the subscription to delete.
 * @return the result of the delete operation. May be one of the predefined {@code RESULT_}
 *     constants or any implementation-specific code starting with {@link #RESULT_FIRST_USER}.
 * Value is {@link android.service.euicc.EuiccService#RESULT_OK}, {@link android.service.euicc.EuiccService#RESULT_MUST_DEACTIVATE_SIM}, {@link android.service.euicc.EuiccService#RESULT_RESOLVABLE_ERRORS}, {@link android.service.euicc.EuiccService#RESULT_NEED_CONFIRMATION_CODE}, or {@link android.service.euicc.EuiccService#RESULT_FIRST_USER}
 * @see android.telephony.euicc.EuiccManager#deleteSubscription
 * @apiSince REL
 */

public abstract int onDeleteSubscription(int slotId, java.lang.String iccid);

/**
 * Switch to the given subscription.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @param iccid the ICCID of the subscription to enable. May be null, in which case the current
 *     profile should be deactivated and no profile should be activated to replace it - this is
 *     equivalent to a physical SIM being ejected.
 * This value may be {@code null}.
 * @param forceDeactivateSim If true, and if an active SIM must be deactivated to access the
 *     eUICC, perform this action automatically. Otherwise, {@link #RESULT_MUST_DEACTIVATE_SIM}
 *     should be returned to allow the user to consent to this operation first.
 * @return the result of the switch operation. May be one of the predefined {@code RESULT_}
 *     constants or any implementation-specific code starting with {@link #RESULT_FIRST_USER}.
 * Value is {@link android.service.euicc.EuiccService#RESULT_OK}, {@link android.service.euicc.EuiccService#RESULT_MUST_DEACTIVATE_SIM}, {@link android.service.euicc.EuiccService#RESULT_RESOLVABLE_ERRORS}, {@link android.service.euicc.EuiccService#RESULT_NEED_CONFIRMATION_CODE}, or {@link android.service.euicc.EuiccService#RESULT_FIRST_USER}
 * @see android.telephony.euicc.EuiccManager#switchToSubscription
 * @apiSince REL
 */

public abstract int onSwitchToSubscription(int slotId, @android.annotation.Nullable java.lang.String iccid, boolean forceDeactivateSim);

/**
 * Update the nickname of the given subscription.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @param iccid the ICCID of the subscription to update.
 * @param nickname the new nickname to apply.
 * @return the result of the update operation. May be one of the predefined {@code RESULT_}
 *     constants or any implementation-specific code starting with {@link #RESULT_FIRST_USER}.
 * @see android.telephony.euicc.EuiccManager#updateSubscriptionNickname
 * @apiSince REL
 */

public abstract int onUpdateSubscriptionNickname(int slotId, java.lang.String iccid, java.lang.String nickname);

/**
 * Erase all of the subscriptions on the device.
 *
 * <p>This is intended to be used for device resets. As such, the reset should be performed even
 * if an active SIM must be deactivated in order to access the eUICC.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @return the result of the erase operation. May be one of the predefined {@code RESULT_}
 *     constants or any implementation-specific code starting with {@link #RESULT_FIRST_USER}.
 * @see android.telephony.euicc.EuiccManager#eraseSubscriptions
 * @apiSince REL
 */

public abstract int onEraseSubscriptions(int slotId);

/**
 * Ensure that subscriptions will be retained on the next factory reset.
 *
 * <p>Called directly before a factory reset. Assumes that a normal factory reset will lead to
 * profiles being erased on first boot (to cover fastboot/recovery wipes), so the implementation
 * should persist some bit that will remain accessible after the factory reset to bypass this
 * flow when this method is called.
 *
 * @param slotId ID of the SIM slot to use for the operation.
 * @return the result of the operation. May be one of the predefined {@code RESULT_} constants
 *     or any implementation-specific code starting with {@link #RESULT_FIRST_USER}.
 * @apiSince REL
 */

public abstract int onRetainSubscriptionsForFactoryReset(int slotId);

/**
 * Action used to bind the carrier app and get the activation code from the carrier app. This
 * activation code will be used to download the eSIM profile during eSIM activation flow.
 * @apiSince REL
 */

public static final java.lang.String ACTION_BIND_CARRIER_PROVISIONING_SERVICE = "android.service.euicc.action.BIND_CARRIER_PROVISIONING_SERVICE";

/**
 * @see android.telephony.euicc.EuiccManager#ACTION_DELETE_SUBSCRIPTION_PRIVILEGED. This is
 * a protected intent that can only be sent by the system, and requires the
 * {@link android.Manifest.permission#BIND_EUICC_SERVICE} permission.
 * @apiSince REL
 */

public static final java.lang.String ACTION_DELETE_SUBSCRIPTION_PRIVILEGED = "android.service.euicc.action.DELETE_SUBSCRIPTION_PRIVILEGED";

/**
 * @see android.telephony.euicc.EuiccManager#ACTION_MANAGE_EMBEDDED_SUBSCRIPTIONS
 * The difference is this one is used by system to bring up the LUI.
 * @apiSince REL
 */

public static final java.lang.String ACTION_MANAGE_EMBEDDED_SUBSCRIPTIONS = "android.service.euicc.action.MANAGE_EMBEDDED_SUBSCRIPTIONS";

/**
 * @see android.telephony.euicc.EuiccManager#ACTION_PROVISION_EMBEDDED_SUBSCRIPTION
 * @apiSince REL
 */

public static final java.lang.String ACTION_PROVISION_EMBEDDED_SUBSCRIPTION = "android.service.euicc.action.PROVISION_EMBEDDED_SUBSCRIPTION";

/**
 * @see android.telephony.euicc.EuiccManager#ACTION_RENAME_SUBSCRIPTION_PRIVILEGED. This is
 * a protected intent that can only be sent by the system, and requires the
 * {@link android.Manifest.permission#BIND_EUICC_SERVICE} permission.
 * @apiSince REL
 */

public static final java.lang.String ACTION_RENAME_SUBSCRIPTION_PRIVILEGED = "android.service.euicc.action.RENAME_SUBSCRIPTION_PRIVILEGED";

/**
 * Ask the user to input carrier confirmation code.
 *
 * @deprecated From Q, the resolvable errors happened in the download step are presented as
 * bit map in {@link #EXTRA_RESOLVABLE_ERRORS}. The corresponding action would be
 * {@link #ACTION_RESOLVE_RESOLVABLE_ERRORS}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final java.lang.String ACTION_RESOLVE_CONFIRMATION_CODE = "android.service.euicc.action.RESOLVE_CONFIRMATION_CODE";

/**
 * Alert the user that this action will result in an active SIM being deactivated.
 * To implement the LUI triggered by the system, you need to define this in AndroidManifest.xml.
 * @apiSince REL
 */

public static final java.lang.String ACTION_RESOLVE_DEACTIVATE_SIM = "android.service.euicc.action.RESOLVE_DEACTIVATE_SIM";

/**
 * Alert the user about a download/switch being done for an app that doesn't currently have
 * carrier privileges.
 * @apiSince REL
 */

public static final java.lang.String ACTION_RESOLVE_NO_PRIVILEGES = "android.service.euicc.action.RESOLVE_NO_PRIVILEGES";

/**
 * Ask the user to resolve all the resolvable errors.
 * @apiSince REL
 */

public static final java.lang.String ACTION_RESOLVE_RESOLVABLE_ERRORS = "android.service.euicc.action.RESOLVE_RESOLVABLE_ERRORS";

/**
 * @see android.telephony.euicc.EuiccManager#ACTION_TOGGLE_SUBSCRIPTION_PRIVILEGED. This is
 * a protected intent that can only be sent by the system, and requires the
 * {@link android.Manifest.permission#BIND_EUICC_SERVICE} permission.
 * @apiSince REL
 */

public static final java.lang.String ACTION_TOGGLE_SUBSCRIPTION_PRIVILEGED = "android.service.euicc.action.TOGGLE_SUBSCRIPTION_PRIVILEGED";

/**
 * Category which must be defined to all UI actions, for efficient lookup.
 * @apiSince REL
 */

public static final java.lang.String CATEGORY_EUICC_UI = "android.service.euicc.category.EUICC_UI";

/**
 * Action which must be included in this service's intent filter.
 * @apiSince REL
 */

public static final java.lang.String EUICC_SERVICE_INTERFACE = "android.service.euicc.EuiccService";

/**
 * String extra for resolution actions indicating whether the user allows policy rules.
 * This is used and set by the implementation and used in {@code EuiccOperation}.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_RESOLUTION_ALLOW_POLICY_RULES = "android.service.euicc.extra.RESOLUTION_ALLOW_POLICY_RULES";

/**
 * Intent extra set for resolution requests containing the package name of the calling app.
 * This is used by the above actions including ACTION_RESOLVE_DEACTIVATE_SIM,
 * ACTION_RESOLVE_NO_PRIVILEGES and ACTION_RESOLVE_RESOLVABLE_ERRORS.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_RESOLUTION_CALLING_PACKAGE = "android.service.euicc.extra.RESOLUTION_CALLING_PACKAGE";

/**
 * Intent extra set for resolution requests containing an int indicating the current card Id.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_RESOLUTION_CARD_ID = "android.service.euicc.extra.RESOLUTION_CARD_ID";

/**
 * String extra for resolution actions indicating the carrier confirmation code.
 * This is used and set by the implementation and used in {@code EuiccOperation}.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_RESOLUTION_CONFIRMATION_CODE = "android.service.euicc.extra.RESOLUTION_CONFIRMATION_CODE";

/**
 * Intent extra set for resolution requests containing a boolean indicating whether to ask the
 * user to retry another confirmation code.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_RESOLUTION_CONFIRMATION_CODE_RETRIED = "android.service.euicc.extra.RESOLUTION_CONFIRMATION_CODE_RETRIED";

/**
 * Boolean extra for resolution actions indicating whether the user granted consent.
 * This is used and set by the implementation and used in {@code EuiccOperation}.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_RESOLUTION_CONSENT = "android.service.euicc.extra.RESOLUTION_CONSENT";

/**
 * Intent extra set for resolution requests containing the list of resolvable errors to be
 * resolved. Each resolvable error is an integer. Its possible values include:
 * <UL>
 * <LI>{@link #RESOLVABLE_ERROR_CONFIRMATION_CODE}
 * <LI>{@link #RESOLVABLE_ERROR_POLICY_RULES}
 * </UL>
 * @apiSince REL
 */

public static final java.lang.String EXTRA_RESOLVABLE_ERRORS = "android.service.euicc.extra.RESOLVABLE_ERRORS";

/**
 * Possible value for the bit map of resolvable errors indicating the download process needs
 * the user to input confirmation code.
 * @apiSince REL
 */

public static final int RESOLVABLE_ERROR_CONFIRMATION_CODE = 1; // 0x1

/**
 * Possible value for the bit map of resolvable errors indicating the download process needs
 * the user's consent to allow profile policy rules.
 * @apiSince REL
 */

public static final int RESOLVABLE_ERROR_POLICY_RULES = 2; // 0x2

/**
 * Start of implementation-specific error results.
 * @apiSince REL
 */

public static final int RESULT_FIRST_USER = 1; // 0x1

/**
 * Result code indicating that an active SIM must be deactivated to perform the operation.
 * @apiSince REL
 */

public static final int RESULT_MUST_DEACTIVATE_SIM = -1; // 0xffffffff

/**
 * Result code indicating that the user must input a carrier confirmation code.
 *
 * @deprecated From Q, the resolvable errors happened in the download step are presented as
 * bit map in {@link #EXTRA_RESOLVABLE_ERRORS}. The corresponding result would be
 * {@link #RESULT_RESOLVABLE_ERRORS}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RESULT_NEED_CONFIRMATION_CODE = -2; // 0xfffffffe

/**
 * Result code for a successful operation.
 * @apiSince REL
 */

public static final int RESULT_OK = 0; // 0x0

/**
 * Result code indicating that the user must resolve resolvable errors.
 * @apiSince REL
 */

public static final int RESULT_RESOLVABLE_ERRORS = -2; // 0xfffffffe
/**
 * Callback class for {@link #onStartOtaIfNecessary(int, OtaStatusChangedCallback)}
 *
 * The status of OTA which can be {@code android.telephony.euicc.EuiccManager#EUICC_OTA_}
 *
 * @see IEuiccService#startOtaIfNecessary
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class OtaStatusChangedCallback {

public OtaStatusChangedCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called when OTA status is changed.
 * @apiSince REL
 */

public abstract void onOtaStatusChanged(int status);
}

}

