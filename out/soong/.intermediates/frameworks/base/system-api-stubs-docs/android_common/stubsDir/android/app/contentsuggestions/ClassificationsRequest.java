/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.contentsuggestions;


/**
 * Request object used when asking {@link ContentSuggestionsManager} to classify content selections.
 *
 * <p>The request contains a list of {@link ContentSelection} objects to be classified along with
 * implementation specific extras.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ClassificationsRequest implements android.os.Parcelable {

ClassificationsRequest(@android.annotation.NonNull java.util.List<android.app.contentsuggestions.ContentSelection> selections, @android.annotation.Nullable android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Return request selections.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.app.contentsuggestions.ContentSelection> getSelections() { throw new RuntimeException("Stub!"); }

/**
 * Return the request extras, can be an empty bundle.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.contentsuggestions.ClassificationsRequest> CREATOR;
static { CREATOR = null; }
/**
 * A builder for classifications request events.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * @param selections This value must never be {@code null}.
 * @apiSince REL
 */

public Builder(@android.annotation.NonNull java.util.List<android.app.contentsuggestions.ContentSelection> selections) { throw new RuntimeException("Stub!"); }

/**
 * Sets the request extras.
 
 * @param extras This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.contentsuggestions.ClassificationsRequest.Builder setExtras(@android.annotation.NonNull android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Builds a new request instance.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.contentsuggestions.ClassificationsRequest build() { throw new RuntimeException("Stub!"); }
}

}

