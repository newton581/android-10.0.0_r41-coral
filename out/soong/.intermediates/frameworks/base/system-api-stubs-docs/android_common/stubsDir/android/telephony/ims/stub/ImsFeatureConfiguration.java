/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims.stub;

import android.telephony.ims.feature.ImsFeature;

/**
 * Container class for IMS Feature configuration. This class contains the features that the
 * ImsService supports, which are defined in {@link ImsFeature} as
 * {@link ImsFeature#FEATURE_EMERGENCY_MMTEL}, {@link ImsFeature#FEATURE_MMTEL}, and
 * {@link ImsFeature#FEATURE_RCS}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ImsFeatureConfiguration implements android.os.Parcelable {

/**
 * Creates with all registration features empty.
 * @hide
 */

ImsFeatureConfiguration() { throw new RuntimeException("Stub!"); }

/**
 * @return a set of supported slot ID to feature type pairs contained within a
 * {@link FeatureSlotPair}.
 * @apiSince REL
 */

public java.util.Set<android.telephony.ims.stub.ImsFeatureConfiguration.FeatureSlotPair> getServiceFeatures() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.ims.stub.ImsFeatureConfiguration> CREATOR;
static { CREATOR = null; }
/**
 * Builder for {@link ImsFeatureConfiguration}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

/** @apiSince REL */

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Adds an IMS feature associated with a SIM slot ID.
 * @param slotId The slot ID associated with the IMS feature.
 * @param featureType The feature that the slot ID supports. Supported values are
 * {@link ImsFeature#FEATURE_EMERGENCY_MMTEL}, {@link ImsFeature#FEATURE_MMTEL}, and
 * {@link ImsFeature#FEATURE_RCS}.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.ImsFeature#FEATURE_EMERGENCY_MMTEL}, {@link android.telephony.ims.feature.ImsFeature#FEATURE_MMTEL}, and {@link android.telephony.ims.feature.ImsFeature#FEATURE_RCS}
 * @return a {@link Builder} to continue constructing the ImsFeatureConfiguration.
 * @apiSince REL
 */

public android.telephony.ims.stub.ImsFeatureConfiguration.Builder addFeature(int slotId, int featureType) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.telephony.ims.stub.ImsFeatureConfiguration build() { throw new RuntimeException("Stub!"); }
}

/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class FeatureSlotPair {

/**
 * A mapping from slotId to IMS Feature type.
 * @param slotId the SIM slot ID associated with this feature.
 * @param featureType The feature that this slotId supports. Supported values are
 * {@link ImsFeature#FEATURE_EMERGENCY_MMTEL}, {@link ImsFeature#FEATURE_MMTEL}, and
 * {@link ImsFeature#FEATURE_RCS}.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.ImsFeature#FEATURE_EMERGENCY_MMTEL}, {@link android.telephony.ims.feature.ImsFeature#FEATURE_MMTEL}, and {@link android.telephony.ims.feature.ImsFeature#FEATURE_RCS}
 * @apiSince REL
 */

public FeatureSlotPair(int slotId, int featureType) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * The feature that this slotId supports. Supported values are
 * {@link ImsFeature#FEATURE_EMERGENCY_MMTEL}, {@link ImsFeature#FEATURE_MMTEL}, and
 * {@link ImsFeature#FEATURE_RCS}.
 
 * <br>
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.ImsFeature#FEATURE_EMERGENCY_MMTEL}, {@link android.telephony.ims.feature.ImsFeature#FEATURE_MMTEL}, and {@link android.telephony.ims.feature.ImsFeature#FEATURE_RCS}
 * @apiSince REL
 */

public final int featureType;
{ featureType = 0; }

/**
 * SIM slot that this feature is associated with.
 * @apiSince REL
 */

public final int slotId;
{ slotId = 0; }
}

}

