/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telecom;

import android.bluetooth.BluetoothDevice;

/**
 * A unified virtual device providing a means of voice (and other) communication on a device.
 *
 * @hide
 * @deprecated Use {@link InCallService} directly instead of using this class.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class Phone {

Phone() { throw new RuntimeException("Stub!"); }

/**
 * Adds a listener to this {@code Phone}.
 *
 * @param listener A {@code Listener} object.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void addListener(android.telecom.Phone.Listener listener) { throw new RuntimeException("Stub!"); }

/**
 * Removes a listener from this {@code Phone}.
 *
 * @param listener A {@code Listener} object.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void removeListener(android.telecom.Phone.Listener listener) { throw new RuntimeException("Stub!"); }

/**
 * Obtains the current list of {@code Call}s to be displayed by this in-call experience.
 *
 * @return A list of the relevant {@code Call}s.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public java.util.List<android.telecom.Call> getCalls() { throw new RuntimeException("Stub!"); }

/**
 * Returns if the {@code Phone} can support additional calls.
 *
 * @return Whether the phone supports adding more calls.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean canAddCall() { throw new RuntimeException("Stub!"); }

/**
 * Sets the microphone mute state. When this request is honored, there will be change to
 * the {@link #getAudioState()}.
 *
 * @param state {@code true} if the microphone should be muted; {@code false} otherwise.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setMuted(boolean state) { throw new RuntimeException("Stub!"); }

/**
 * Sets the audio route (speaker, bluetooth, etc...).  When this request is honored, there will
 * be change to the {@link #getAudioState()}.
 *
 * @param route The audio route to use.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setAudioRoute(int route) { throw new RuntimeException("Stub!"); }

/**
 * Request audio routing to a specific bluetooth device. Calling this method may result in
 * the device routing audio to a different bluetooth device than the one specified. A list of
 * available devices can be obtained via {@link CallAudioState#getSupportedBluetoothDevices()}
 *
 * @param bluetoothAddress The address of the bluetooth device to connect to, as returned by
 * {@link BluetoothDevice#getAddress()}, or {@code null} if no device is preferred.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void requestBluetoothAudio(java.lang.String bluetoothAddress) { throw new RuntimeException("Stub!"); }

/**
 * Obtains the current phone call audio state of the {@code Phone}.
 *
 * @return An object encapsulating the audio state.
 * @deprecated Use {@link #getCallAudioState()} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public android.telecom.AudioState getAudioState() { throw new RuntimeException("Stub!"); }

/**
 * Obtains the current phone call audio state of the {@code Phone}.
 *
 * @return An object encapsulating the audio state.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public android.telecom.CallAudioState getCallAudioState() { throw new RuntimeException("Stub!"); }
/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract static class Listener {

@Deprecated
public Listener() { throw new RuntimeException("Stub!"); }

/**
 * Called when the audio state changes.
 *
 * @param phone The {@code Phone} calling this method.
 * @param audioState The new {@link AudioState}.
 *
 * @deprecated Use {@link #onCallAudioStateChanged(Phone, CallAudioState)} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onAudioStateChanged(android.telecom.Phone phone, android.telecom.AudioState audioState) { throw new RuntimeException("Stub!"); }

/**
 * Called when the audio state changes.
 *
 * @param phone The {@code Phone} calling this method.
 * @param callAudioState The new {@link CallAudioState}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onCallAudioStateChanged(android.telecom.Phone phone, android.telecom.CallAudioState callAudioState) { throw new RuntimeException("Stub!"); }

/**
 * Called to bring the in-call screen to the foreground. The in-call experience should
 * respond immediately by coming to the foreground to inform the user of the state of
 * ongoing {@code Call}s.
 *
 * @param phone The {@code Phone} calling this method.
 * @param showDialpad If true, put up the dialpad when the screen is shown.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onBringToForeground(android.telecom.Phone phone, boolean showDialpad) { throw new RuntimeException("Stub!"); }

/**
 * Called when a {@code Call} has been added to this in-call session. The in-call user
 * experience should add necessary state listeners to the specified {@code Call} and
 * immediately start to show the user information about the existence
 * and nature of this {@code Call}. Subsequent invocations of {@link #getCalls()} will
 * include this {@code Call}.
 *
 * @param phone The {@code Phone} calling this method.
 * @param call A newly added {@code Call}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onCallAdded(android.telecom.Phone phone, android.telecom.Call call) { throw new RuntimeException("Stub!"); }

/**
 * Called when a {@code Call} has been removed from this in-call session. The in-call user
 * experience should remove any state listeners from the specified {@code Call} and
 * immediately stop displaying any information about this {@code Call}.
 * Subsequent invocations of {@link #getCalls()} will no longer include this {@code Call}.
 *
 * @param phone The {@code Phone} calling this method.
 * @param call A newly removed {@code Call}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onCallRemoved(android.telecom.Phone phone, android.telecom.Call call) { throw new RuntimeException("Stub!"); }

/**
 * Called when the {@code Phone} ability to add more calls changes.  If the phone cannot
 * support more calls then {@code canAddCall} is set to {@code false}.  If it can, then it
 * is set to {@code true}.
 *
 * @param phone The {@code Phone} calling this method.
 * @param canAddCall Indicates whether an additional call can be added.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onCanAddCallChanged(android.telecom.Phone phone, boolean canAddCall) { throw new RuntimeException("Stub!"); }

/**
 * Called to silence the ringer if a ringing call exists.
 *
 * @param phone The {@code Phone} calling this method.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onSilenceRinger(android.telecom.Phone phone) { throw new RuntimeException("Stub!"); }
}

}

