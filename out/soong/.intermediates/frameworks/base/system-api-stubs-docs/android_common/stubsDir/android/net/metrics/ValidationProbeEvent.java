/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.metrics;


/**
 * An event recorded by NetworkMonitor when sending a probe for finding captive portals.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ValidationProbeEvent implements android.net.metrics.IpConnectivityLog.Event {

ValidationProbeEvent(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @hide */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @hide */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Get the name of a probe specified by its probe type.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static java.lang.String getProbeName(int probeType) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int DNS_FAILURE = 0; // 0x0

/** @apiSince REL */

public static final int DNS_SUCCESS = 1; // 0x1

/** @apiSince REL */

public static final int PROBE_DNS = 0; // 0x0

/** @apiSince REL */

public static final int PROBE_FALLBACK = 4; // 0x4

/** @apiSince REL */

public static final int PROBE_HTTP = 1; // 0x1

/** @apiSince REL */

public static final int PROBE_HTTPS = 2; // 0x2

/** @apiSince REL */

public static final int PROBE_PAC = 3; // 0x3

/** @apiSince REL */

public static final int PROBE_PRIVDNS = 5; // 0x5
/**
 * Utility to create an instance of {@link ValidationProbeEvent}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Set the duration of the probe in milliseconds.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ValidationProbeEvent.Builder setDurationMs(long durationMs) { throw new RuntimeException("Stub!"); }

/**
 * Set the probe type based on whether it was the first validation.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ValidationProbeEvent.Builder setProbeType(int probeType, boolean firstValidation) { throw new RuntimeException("Stub!"); }

/**
 * Set the return code of the probe.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ValidationProbeEvent.Builder setReturnCode(int returnCode) { throw new RuntimeException("Stub!"); }

/**
 * Create a new {@link ValidationProbeEvent}.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.metrics.ValidationProbeEvent build() { throw new RuntimeException("Stub!"); }
}

}

