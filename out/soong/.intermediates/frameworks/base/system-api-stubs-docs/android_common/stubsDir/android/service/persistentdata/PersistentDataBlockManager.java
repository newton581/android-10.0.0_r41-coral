/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.persistentdata;

import android.service.oemlock.OemLockManager;

/**
 * Interface for reading and writing data blocks to a persistent partition.
 *
 * Allows writing one block at a time. Namely, each time
 * {@link PersistentDataBlockManager#write(byte[])}
 * is called, it will overwite the data that was previously written on the block.
 *
 * Clients can query the size of the currently written block via
 * {@link PersistentDataBlockManager#getDataBlockSize()}.
 *
 * Clients can query the maximum size for a block via
 * {@link PersistentDataBlockManager#getMaximumDataBlockSize()}
 *
 * Clients can read the currently written block by invoking
 * {@link PersistentDataBlockManager#read()}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class PersistentDataBlockManager {

PersistentDataBlockManager() { throw new RuntimeException("Stub!"); }

/**
 * Writes {@code data} to the persistent partition. Previously written data
 * will be overwritten. This data will persist across factory resets.
 *
 * Returns the number of bytes written or -1 on error. If the block is too big
 * to fit on the partition, returns -MAX_BLOCK_SIZE.
 *
 * {@link #wipe} will block any further {@link #write} operation until reboot,
 * in which case -1 will be returned.
 *
 * @param data the data to write
 * @apiSince REL
 */

public int write(byte[] data) { throw new RuntimeException("Stub!"); }

/**
 * Returns the data block stored on the persistent partition.
 * @apiSince REL
 */

public byte[] read() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the size of the block currently written to the persistent partition.
 *
 * Return -1 on error.
 
 * <br>
 * Requires android.Manifest.permission.ACCESS_PDB_STATE
 * @apiSince REL
 */

public int getDataBlockSize() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the maximum size allowed for a data block.
 *
 * Returns -1 on error.
 * @apiSince REL
 */

public long getMaximumDataBlockSize() { throw new RuntimeException("Stub!"); }

/**
 * Zeroes the previously written block in its entirety. Calling this method
 * will erase all data written to the persistent data partition.
 * It will also prevent any further {@link #write} operation until reboot,
 * in order to prevent a potential race condition. See b/30352311.
 
 * <br>
 * Requires android.Manifest.permission.OEM_UNLOCK_STATE
 * @apiSince REL
 */

public void wipe() { throw new RuntimeException("Stub!"); }

/**
 * Writes a byte enabling or disabling the ability to "OEM unlock" the device.
 *
 * <br>
 * Requires android.Manifest.permission.OEM_UNLOCK_STATE
 * @deprecated use {@link OemLockManager#setOemUnlockAllowedByUser(boolean)} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setOemUnlockEnabled(boolean enabled) { throw new RuntimeException("Stub!"); }

/**
 * Returns whether or not "OEM unlock" is enabled or disabled on this device.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_OEM_UNLOCK_STATE} or android.Manifest.permission.OEM_UNLOCK_STATE
 * @deprecated use {@link OemLockManager#isOemUnlockAllowedByUser()} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean getOemUnlockEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves available information about this device's flash lock state.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_OEM_UNLOCK_STATE} or android.Manifest.permission.OEM_UNLOCK_STATE
 * @return {@link #FLASH_LOCK_LOCKED} if device bootloader is locked,
 * {@link #FLASH_LOCK_UNLOCKED} if device bootloader is unlocked, or {@link #FLASH_LOCK_UNKNOWN}
 * if this information cannot be ascertained on this device.
 
 * Value is {@link android.service.persistentdata.PersistentDataBlockManager#FLASH_LOCK_UNKNOWN}, {@link android.service.persistentdata.PersistentDataBlockManager#FLASH_LOCK_LOCKED}, or {@link android.service.persistentdata.PersistentDataBlockManager#FLASH_LOCK_UNLOCKED}
 * @apiSince REL
 */

public int getFlashLockState() { throw new RuntimeException("Stub!"); }

/**
 * Indicates that the device's bootloader is LOCKED.
 * @apiSince REL
 */

public static final int FLASH_LOCK_LOCKED = 1; // 0x1

/**
 * Indicates that the device's bootloader lock state is UNKNOWN.
 * @apiSince REL
 */

public static final int FLASH_LOCK_UNKNOWN = -1; // 0xffffffff

/**
 * Indicates that the device's bootloader is UNLOCKED.
 * @apiSince REL
 */

public static final int FLASH_LOCK_UNLOCKED = 0; // 0x0
/**
 * Value is {@link android.service.persistentdata.PersistentDataBlockManager#FLASH_LOCK_UNKNOWN}, {@link android.service.persistentdata.PersistentDataBlockManager#FLASH_LOCK_LOCKED}, or {@link android.service.persistentdata.PersistentDataBlockManager#FLASH_LOCK_UNLOCKED}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface FlashLockState {
}

}

