/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;


/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class RemoteCallback implements android.os.Parcelable {

/** @apiSince REL */

public RemoteCallback(android.os.RemoteCallback.OnResultListener listener) { throw new RuntimeException("Stub!"); }

/**
 * @param listener This value must never be {@code null}.
 
 * @param handler This value may be {@code null}.
 * @apiSince REL
 */

public RemoteCallback(@android.annotation.NonNull android.os.RemoteCallback.OnResultListener listener, @android.annotation.Nullable android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * @param result This value may be {@code null}.
 * @apiSince REL
 */

public void sendResult(@android.annotation.Nullable android.os.Bundle result) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.RemoteCallback> CREATOR;
static { CREATOR = null; }
/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnResultListener {

/**
 * @param result This value may be {@code null}.
 * @apiSince REL
 */

public void onResult(@android.annotation.Nullable android.os.Bundle result);
}

}

