/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims;

import android.os.Parcelable;

/**
 * Parcelable object to handle IMS stream media profile.
 * It provides the media direction, quality of audio and/or video.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ImsStreamMediaProfile implements android.os.Parcelable {

/**
 * Constructor.
 *
 * @param audioQuality The audio quality. Can be one of the following:
 *                     {@link #AUDIO_QUALITY_AMR},
 *                     {@link #AUDIO_QUALITY_AMR_WB},
 *                     {@link #AUDIO_QUALITY_QCELP13K},
 *                     {@link #AUDIO_QUALITY_EVRC},
 *                     {@link #AUDIO_QUALITY_EVRC_B},
 *                     {@link #AUDIO_QUALITY_EVRC_WB},
 *                     {@link #AUDIO_QUALITY_EVRC_NW},
 *                     {@link #AUDIO_QUALITY_GSM_EFR},
 *                     {@link #AUDIO_QUALITY_GSM_FR},
 *                     {@link #AUDIO_QUALITY_GSM_HR},
 *                     {@link #AUDIO_QUALITY_G711U},
 *                     {@link #AUDIO_QUALITY_G723},
 *                     {@link #AUDIO_QUALITY_G711A},
 *                     {@link #AUDIO_QUALITY_G722},
 *                     {@link #AUDIO_QUALITY_G711AB},
 *                     {@link #AUDIO_QUALITY_G729},
 *                     {@link #AUDIO_QUALITY_EVS_NB},
 *                     {@link #AUDIO_QUALITY_EVS_WB},
 *                     {@link #AUDIO_QUALITY_EVS_SWB},
 *                     {@link #AUDIO_QUALITY_EVS_FB},
 * @param audioDirection The audio direction. Can be one of the following:
 *                       {@link #DIRECTION_INVALID},
 *                       {@link #DIRECTION_INACTIVE},
 *                       {@link #DIRECTION_RECEIVE},
 *                       {@link #DIRECTION_SEND},
 *                       {@link #DIRECTION_SEND_RECEIVE},
 * @param videoQuality The video quality. Can be one of the following:
 *                     {@link #VIDEO_QUALITY_NONE},
 *                     {@link #VIDEO_QUALITY_QCIF},
 *                     {@link #VIDEO_QUALITY_QVGA_LANDSCAPE},
 *                     {@link #VIDEO_QUALITY_QVGA_PORTRAIT},
 *                     {@link #VIDEO_QUALITY_VGA_LANDSCAPE},
 *                     {@link #VIDEO_QUALITY_VGA_PORTRAIT},
 * @param videoDirection The video direction. Can be one of the following:
 *                       {@link #DIRECTION_INVALID},
 *                       {@link #DIRECTION_INACTIVE},
 *                       {@link #DIRECTION_RECEIVE},
 *                       {@link #DIRECTION_SEND},
 *                       {@link #DIRECTION_SEND_RECEIVE},
 * @param rttMode The rtt mode. Can be one of the following:
 *                {@link #RTT_MODE_DISABLED},
 *                {@link #RTT_MODE_FULL}
 * @apiSince REL
 */

public ImsStreamMediaProfile(int audioQuality, int audioDirection, int videoQuality, int videoDirection, int rttMode) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void copyFrom(android.telephony.ims.ImsStreamMediaProfile profile) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Determines if it's RTT call
 * @return true if RTT call, false otherwise.
 * @apiSince REL
 */

public boolean isRttCall() { throw new RuntimeException("Stub!"); }

/**
 * Updates the RttCall attribute
 * @apiSince REL
 */

public void setRttMode(int rttMode) { throw new RuntimeException("Stub!"); }

/**
 * Sets whether the remote party is transmitting audio over the RTT call.
 * @param audioOn true if audio is being received, false otherwise.
 * @apiSince REL
 */

public void setReceivingRttAudio(boolean audioOn) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getAudioQuality() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getAudioDirection() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getVideoQuality() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getVideoDirection() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getRttMode() { throw new RuntimeException("Stub!"); }

/**
 * @return true if remote party is transmitting audio, false otherwise.
 * @apiSince REL
 */

public boolean isReceivingRttAudio() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int AUDIO_QUALITY_AMR = 1; // 0x1

/** @apiSince REL */

public static final int AUDIO_QUALITY_AMR_WB = 2; // 0x2

/** @apiSince REL */

public static final int AUDIO_QUALITY_EVRC = 4; // 0x4

/** @apiSince REL */

public static final int AUDIO_QUALITY_EVRC_B = 5; // 0x5

/** @apiSince REL */

public static final int AUDIO_QUALITY_EVRC_NW = 7; // 0x7

/** @apiSince REL */

public static final int AUDIO_QUALITY_EVRC_WB = 6; // 0x6

/** @apiSince REL */

public static final int AUDIO_QUALITY_EVS_FB = 20; // 0x14

/** @apiSince REL */

public static final int AUDIO_QUALITY_EVS_NB = 17; // 0x11

/** @apiSince REL */

public static final int AUDIO_QUALITY_EVS_SWB = 19; // 0x13

/** @apiSince REL */

public static final int AUDIO_QUALITY_EVS_WB = 18; // 0x12

/** @apiSince REL */

public static final int AUDIO_QUALITY_G711A = 13; // 0xd

/** @apiSince REL */

public static final int AUDIO_QUALITY_G711AB = 15; // 0xf

/** @apiSince REL */

public static final int AUDIO_QUALITY_G711U = 11; // 0xb

/** @apiSince REL */

public static final int AUDIO_QUALITY_G722 = 14; // 0xe

/** @apiSince REL */

public static final int AUDIO_QUALITY_G723 = 12; // 0xc

/** @apiSince REL */

public static final int AUDIO_QUALITY_G729 = 16; // 0x10

/** @apiSince REL */

public static final int AUDIO_QUALITY_GSM_EFR = 8; // 0x8

/** @apiSince REL */

public static final int AUDIO_QUALITY_GSM_FR = 9; // 0x9

/** @apiSince REL */

public static final int AUDIO_QUALITY_GSM_HR = 10; // 0xa

/**
 * Audio information
 * @apiSince REL
 */

public static final int AUDIO_QUALITY_NONE = 0; // 0x0

/** @apiSince REL */

public static final int AUDIO_QUALITY_QCELP13K = 3; // 0x3

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.ims.ImsStreamMediaProfile> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public static final int DIRECTION_INACTIVE = 0; // 0x0

/**
 * Media directions
 * @apiSince REL
 */

public static final int DIRECTION_INVALID = -1; // 0xffffffff

/** @apiSince REL */

public static final int DIRECTION_RECEIVE = 1; // 0x1

/** @apiSince REL */

public static final int DIRECTION_SEND = 2; // 0x2

/** @apiSince REL */

public static final int DIRECTION_SEND_RECEIVE = 3; // 0x3

/**
 * RTT Modes
 * @apiSince REL
 */

public static final int RTT_MODE_DISABLED = 0; // 0x0

/** @apiSince REL */

public static final int RTT_MODE_FULL = 1; // 0x1

/**
 * Video information
 * @apiSince REL
 */

public static final int VIDEO_QUALITY_NONE = 0; // 0x0

/** @apiSince REL */

public static final int VIDEO_QUALITY_QCIF = 1; // 0x1

/** @apiSince REL */

public static final int VIDEO_QUALITY_QVGA_LANDSCAPE = 2; // 0x2

/** @apiSince REL */

public static final int VIDEO_QUALITY_QVGA_PORTRAIT = 4; // 0x4

/** @apiSince REL */

public static final int VIDEO_QUALITY_VGA_LANDSCAPE = 8; // 0x8

/** @apiSince REL */

public static final int VIDEO_QUALITY_VGA_PORTRAIT = 16; // 0x10
}

