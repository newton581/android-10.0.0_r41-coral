/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.pm;


/**
 * Information about an instant application intent filter.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class InstantAppIntentFilter implements android.os.Parcelable {

/**
 * @param splitName This value may be {@code null}.
 
 * @param filters This value must never be {@code null}.
 * @apiSince REL
 */

public InstantAppIntentFilter(@android.annotation.Nullable java.lang.String splitName, @android.annotation.NonNull java.util.List<android.content.IntentFilter> filters) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getSplitName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.util.List<android.content.IntentFilter> getFilters() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.content.pm.InstantAppIntentFilter> CREATOR;
static { CREATOR = null; }
}

