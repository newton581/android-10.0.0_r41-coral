/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app.prediction;


/**
 * Class that provides methods to create prediction clients.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AppPredictionManager {

/**
 * @hide
 */

AppPredictionManager(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Creates a new app prediction session.
 
 * @param predictionContext This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.prediction.AppPredictor createAppPredictionSession(@android.annotation.NonNull android.app.prediction.AppPredictionContext predictionContext) { throw new RuntimeException("Stub!"); }
}

