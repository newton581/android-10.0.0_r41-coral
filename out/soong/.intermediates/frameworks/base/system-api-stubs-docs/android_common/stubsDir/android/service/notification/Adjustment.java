/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.notification;

import android.os.Bundle;
import android.app.Notification;
import android.os.Parcelable;

/**
 * Ranking updates from the Assistant.
 *
 * The updates are provides as a {@link Bundle} of signals, using the keys provided in this
 * class.
 * Each {@code KEY} specifies what type of data it supports and what kind of Adjustment it
 * realizes on the notification rankings.
 *
 * Notifications affected by the Adjustment will be re-ranked if necessary.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class Adjustment implements android.os.Parcelable {

/**
 * Create a notification adjustment.
 *
 * @param pkg The package of the notification.
 * @param key The notification key.
 * @param signals A bundle of signals that should inform notification display, ordering, and
 *                interruptiveness.
 * @param explanation A human-readable justification for the adjustment.
 * @hide
 */

public Adjustment(java.lang.String pkg, java.lang.String key, android.os.Bundle signals, java.lang.CharSequence explanation, int user) { throw new RuntimeException("Stub!"); }

/**
 * Create a notification adjustment.
 *
 * @param pkg The package of the notification.
 * This value must never be {@code null}.
 * @param key The notification key.
 * This value must never be {@code null}.
 * @param signals A bundle of signals that should inform notification display, ordering, and
 *                interruptiveness.
 * This value must never be {@code null}.
 * @param explanation A human-readable justification for the adjustment.
 * This value must never be {@code null}.
 * @param userHandle User handle for for whose the adjustments will be applied.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public Adjustment(@android.annotation.NonNull java.lang.String pkg, @android.annotation.NonNull java.lang.String key, @android.annotation.NonNull android.os.Bundle signals, @android.annotation.NonNull java.lang.CharSequence explanation, @android.annotation.NonNull android.os.UserHandle userHandle) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

protected Adjustment(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getPackage() { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getKey() { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.CharSequence getExplanation() { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.Bundle getSignals() { throw new RuntimeException("Stub!"); }

/** @hide */

public int getUser() { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.UserHandle getUserHandle() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.notification.Adjustment> CREATOR;
static { CREATOR = null; }

/**
 * Data type: ArrayList of {@link android.app.Notification.Action}.
 * Used to suggest contextual actions for a notification.
 *
 * @see Notification.Action.Builder#setContextual(boolean)
 * @apiSince REL
 */

public static final java.lang.String KEY_CONTEXTUAL_ACTIONS = "key_contextual_actions";

/**
 * Data type: int, one of importance values e.g.
 * {@link android.app.NotificationManager#IMPORTANCE_MIN}.
 *
 * <p> If used from
 * {@link NotificationAssistantService#onNotificationEnqueued(StatusBarNotification)}, and
 * received before the notification is posted, it can block a notification from appearing or
 * silence it. Importance adjustments received too late from
 * {@link NotificationAssistantService#onNotificationEnqueued(StatusBarNotification)} will be
 * ignored.
 * </p>
 * <p>If used from
 * {@link NotificationAssistantService#adjustNotification(Adjustment)}, it can
 * visually demote or cancel a notification, but use this with care if they notification was
 * recently posted because the notification may already have made noise.
 * </p>
 * @apiSince REL
 */

public static final java.lang.String KEY_IMPORTANCE = "key_importance";

/**
 * Data type: ArrayList of {@code String}, where each is a representation of a
 * {@link android.provider.ContactsContract.Contacts#CONTENT_LOOKUP_URI}.
 * See {@link android.app.Notification.Builder#addPerson(String)}.
 * @hide
 */

public static final java.lang.String KEY_PEOPLE = "key_people";

/**
 * Parcelable {@code ArrayList} of {@link SnoozeCriterion}. These criteria may be visible to
 * users. If a user chooses to snooze a notification until one of these criterion, the
 * assistant will be notified via
 * {@link NotificationAssistantService#onNotificationSnoozedUntilContext}.
 * @apiSince REL
 */

public static final java.lang.String KEY_SNOOZE_CRITERIA = "key_snooze_criteria";

/**
 * Data type: ArrayList of {@link CharSequence}.
 * Used to suggest smart replies for a notification.
 * @apiSince REL
 */

public static final java.lang.String KEY_TEXT_REPLIES = "key_text_replies";

/**
 * Data type: int, one of {@link NotificationListenerService.Ranking#USER_SENTIMENT_POSITIVE},
 * {@link NotificationListenerService.Ranking#USER_SENTIMENT_NEUTRAL},
 * {@link NotificationListenerService.Ranking#USER_SENTIMENT_NEGATIVE}. Used to express how
 * a user feels about notifications in the same {@link android.app.NotificationChannel} as
 * the notification represented by {@link #getKey()}.
 * @apiSince REL
 */

public static final java.lang.String KEY_USER_SENTIMENT = "key_user_sentiment";
}

