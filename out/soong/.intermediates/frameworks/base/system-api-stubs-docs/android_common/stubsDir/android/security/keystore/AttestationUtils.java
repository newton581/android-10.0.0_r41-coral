/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.security.keystore;

import android.Manifest;

/**
 * Utilities for attesting the device's hardware identifiers.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class AttestationUtils {

AttestationUtils() { throw new RuntimeException("Stub!"); }

/**
 * Performs attestation of the device's identifiers. This method returns a certificate chain
 * whose first element contains the requested device identifiers in an extension. The device's
 * manufacturer, model, brand, device and product are always also included in the attestation.
 * If the device supports attestation in secure hardware, the chain will be rooted at a
 * trustworthy CA key. Otherwise, the chain will be rooted at an untrusted certificate. See
 * <a href="https://developer.android.com/training/articles/security-key-attestation.html">
 * Key Attestation</a> for the format of the certificate extension.
 * <p>
 * Attestation will only be successful when all of the following are true:
 * 1) The device has been set up to support device identifier attestation at the factory.
 * 2) The user has not permanently disabled device identifier attestation.
 * 3) You have permission to access the device identifiers you are requesting attestation for.
 * <p>
 * For privacy reasons, you cannot distinguish between (1) and (2). If attestation is
 * unsuccessful, the device may not support it in general or the user may have permanently
 * disabled it.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param context the context to use for retrieving device identifiers.
 * @param idTypes the types of device identifiers to attest.
 * This value must never be {@code null}.
 * @param attestationChallenge a blob to include in the certificate alongside the device
 * identifiers.
 *
 * This value must never be {@code null}.
 * @return a certificate chain containing the requested device identifiers in the first element
 *
 * This value will never be {@code null}.
 * @exception SecurityException if you are not permitted to obtain an attestation of the
 * device's identifiers.
 * @exception DeviceIdAttestationException if the attestation operation fails.
 * @apiSince REL
 */

@android.annotation.NonNull
public static java.security.cert.X509Certificate[] attestDeviceIds(android.content.Context context, @android.annotation.NonNull int[] idTypes, @android.annotation.NonNull byte[] attestationChallenge) throws android.security.keystore.DeviceIdAttestationException { throw new RuntimeException("Stub!"); }

/**
 * Specifies that the device should attest its IMEIs. For use with {@link #attestDeviceIds}.
 *
 * @see #attestDeviceIds
 * @apiSince REL
 */

public static final int ID_TYPE_IMEI = 2; // 0x2

/**
 * Specifies that the device should attest its MEIDs. For use with {@link #attestDeviceIds}.
 *
 * @see #attestDeviceIds
 * @apiSince REL
 */

public static final int ID_TYPE_MEID = 3; // 0x3

/**
 * Specifies that the device should attest its serial number. For use with
 * {@link #attestDeviceIds}.
 *
 * @see #attestDeviceIds
 * @apiSince REL
 */

public static final int ID_TYPE_SERIAL = 1; // 0x1
}

