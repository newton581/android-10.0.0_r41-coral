// Baseline format: 1.0
AddedAbstractMethod: android.content.pm.PackageManager#arePermissionsIndividuallyControlled():
    
AddedAbstractMethod: android.webkit.WebViewProvider#getWebViewRenderProcess():
    
AddedAbstractMethod: android.webkit.WebViewProvider#getWebViewRenderProcessClient():
    
AddedAbstractMethod: android.webkit.WebViewProvider#setWebViewRenderProcessClient(java.util.concurrent.Executor, android.webkit.WebViewRenderProcessClient):
    


RemovedDeprecatedMethod: android.location.LocationManager#addGpsMeasurementListener(android.location.GpsMeasurementsEvent.Listener):
    
RemovedDeprecatedMethod: android.location.LocationManager#addGpsNavigationMessageListener(android.location.GpsNavigationMessageEvent.Listener):
    
RemovedDeprecatedMethod: android.location.LocationManager#removeGpsMeasurementListener(android.location.GpsMeasurementsEvent.Listener):
    
RemovedDeprecatedMethod: android.location.LocationManager#removeGpsNavigationMessageListener(android.location.GpsNavigationMessageEvent.Listener):
    
RemovedDeprecatedMethod: android.telephony.TelephonyManager#answerRingingCall():
    
RemovedDeprecatedMethod: android.telephony.TelephonyManager#endCall():
    
RemovedDeprecatedMethod: android.telephony.TelephonyManager#silenceRinger():
    


RemovedField: android.Manifest.permission#CAPTURE_SECURE_VIDEO_OUTPUT:
    
RemovedField: android.Manifest.permission#CAPTURE_VIDEO_OUTPUT:
    
RemovedField: android.Manifest.permission#MANAGE_DEVICE_ADMINS:
    
RemovedField: android.Manifest.permission#READ_FRAME_BUFFER:
    
