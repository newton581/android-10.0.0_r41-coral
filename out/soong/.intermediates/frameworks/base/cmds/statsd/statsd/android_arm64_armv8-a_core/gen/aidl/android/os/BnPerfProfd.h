#ifndef AIDL_GENERATED_ANDROID_OS_BN_PERF_PROFD_H_
#define AIDL_GENERATED_ANDROID_OS_BN_PERF_PROFD_H_

#include <binder/IInterface.h>
#include <android/os/IPerfProfd.h>

namespace android {

namespace os {

class BnPerfProfd : public ::android::BnInterface<IPerfProfd> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnPerfProfd

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_BN_PERF_PROFD_H_
