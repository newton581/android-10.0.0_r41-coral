#ifndef AIDL_GENERATED_ANDROID_OS_BP_PERF_PROFD_H_
#define AIDL_GENERATED_ANDROID_OS_BP_PERF_PROFD_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/os/IPerfProfd.h>

namespace android {

namespace os {

class BpPerfProfd : public ::android::BpInterface<IPerfProfd> {
public:
  explicit BpPerfProfd(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpPerfProfd() = default;
  ::android::binder::Status startProfiling(int32_t collectionInterval, int32_t iterations, int32_t process, int32_t samplingPeriod, int32_t samplingFrequency, int32_t sampleDuration, bool stackProfile, bool useElfSymbolizer, bool sendToDropbox) override;
  ::android::binder::Status startProfilingString(const ::android::String16& config) override;
  ::android::binder::Status startProfilingProtobuf(const ::std::vector<uint8_t>& config_proto) override;
  ::android::binder::Status stopProfiling() override;
};  // class BpPerfProfd

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_BP_PERF_PROFD_H_
