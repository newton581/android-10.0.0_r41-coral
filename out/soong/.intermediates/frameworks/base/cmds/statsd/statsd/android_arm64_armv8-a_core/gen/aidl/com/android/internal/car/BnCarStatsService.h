#ifndef AIDL_GENERATED_COM_ANDROID_INTERNAL_CAR_BN_CAR_STATS_SERVICE_H_
#define AIDL_GENERATED_COM_ANDROID_INTERNAL_CAR_BN_CAR_STATS_SERVICE_H_

#include <binder/IInterface.h>
#include <com/android/internal/car/ICarStatsService.h>

namespace com {

namespace android {

namespace internal {

namespace car {

class BnCarStatsService : public ::android::BnInterface<ICarStatsService> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnCarStatsService

}  // namespace car

}  // namespace internal

}  // namespace android

}  // namespace com

#endif  // AIDL_GENERATED_COM_ANDROID_INTERNAL_CAR_BN_CAR_STATS_SERVICE_H_
