#include <com/android/internal/car/ICarStatsService.h>
#include <com/android/internal/car/BpCarStatsService.h>

namespace com {

namespace android {

namespace internal {

namespace car {

IMPLEMENT_META_INTERFACE(CarStatsService, "com.android.internal.car.ICarStatsService")

::android::IBinder* ICarStatsServiceDefault::onAsBinder() {
  return nullptr;
}

::android::binder::Status ICarStatsServiceDefault::pullData(int32_t, ::std::vector<::android::os::StatsLogEventWrapper>* ) {
  return ::android::binder::Status::fromStatusT(::android::UNKNOWN_TRANSACTION);
}

}  // namespace car

}  // namespace internal

}  // namespace android

}  // namespace com
#include <com/android/internal/car/BpCarStatsService.h>
#include <binder/Parcel.h>
#include <android-base/macros.h>

namespace com {

namespace android {

namespace internal {

namespace car {

BpCarStatsService::BpCarStatsService(const ::android::sp<::android::IBinder>& _aidl_impl)
    : BpInterface<ICarStatsService>(_aidl_impl){
}

::android::binder::Status BpCarStatsService::pullData(int32_t atomId, ::std::vector<::android::os::StatsLogEventWrapper>* _aidl_return) {
  ::android::Parcel _aidl_data;
  ::android::Parcel _aidl_reply;
  ::android::status_t _aidl_ret_status = ::android::OK;
  ::android::binder::Status _aidl_status;
  _aidl_ret_status = _aidl_data.writeInterfaceToken(getInterfaceDescriptor());
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_data.writeInt32(atomId);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = remote()->transact(::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* pullData */, _aidl_data, &_aidl_reply);
  if (UNLIKELY(_aidl_ret_status == ::android::UNKNOWN_TRANSACTION && ICarStatsService::getDefaultImpl())) {
     return ICarStatsService::getDefaultImpl()->pullData(atomId, _aidl_return);
  }
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_ret_status = _aidl_status.readFromParcel(_aidl_reply);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  if (!_aidl_status.isOk()) {
    return _aidl_status;
  }
  _aidl_ret_status = _aidl_reply.readParcelableVector(_aidl_return);
  if (((_aidl_ret_status) != (::android::OK))) {
    goto _aidl_error;
  }
  _aidl_error:
  _aidl_status.setFromStatusT(_aidl_ret_status);
  return _aidl_status;
}

}  // namespace car

}  // namespace internal

}  // namespace android

}  // namespace com
#include <com/android/internal/car/BnCarStatsService.h>
#include <binder/Parcel.h>

namespace com {

namespace android {

namespace internal {

namespace car {

::android::status_t BnCarStatsService::onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) {
  ::android::status_t _aidl_ret_status = ::android::OK;
  switch (_aidl_code) {
  case ::android::IBinder::FIRST_CALL_TRANSACTION + 0 /* pullData */:
  {
    int32_t in_atomId;
    ::std::vector<::android::os::StatsLogEventWrapper> _aidl_return;
    if (!(_aidl_data.checkInterface(this))) {
      _aidl_ret_status = ::android::BAD_TYPE;
      break;
    }
    _aidl_ret_status = _aidl_data.readInt32(&in_atomId);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    ::android::binder::Status _aidl_status(pullData(in_atomId, &_aidl_return));
    _aidl_ret_status = _aidl_status.writeToParcel(_aidl_reply);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
    if (!_aidl_status.isOk()) {
      break;
    }
    _aidl_ret_status = _aidl_reply->writeParcelableVector(_aidl_return);
    if (((_aidl_ret_status) != (::android::OK))) {
      break;
    }
  }
  break;
  default:
  {
    _aidl_ret_status = ::android::BBinder::onTransact(_aidl_code, _aidl_data, _aidl_reply, _aidl_flags);
  }
  break;
  }
  if (_aidl_ret_status == ::android::UNEXPECTED_NULL) {
    _aidl_ret_status = ::android::binder::Status::fromExceptionCode(::android::binder::Status::EX_NULL_POINTER).writeToParcel(_aidl_reply);
  }
  return _aidl_ret_status;
}

}  // namespace car

}  // namespace internal

}  // namespace android

}  // namespace com
