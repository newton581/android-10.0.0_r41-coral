#ifndef AIDL_GENERATED_ANDROID_OS_I_PERF_PROFD_H_
#define AIDL_GENERATED_ANDROID_OS_I_PERF_PROFD_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <utils/String16.h>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace os {

class IPerfProfd : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(PerfProfd)
  virtual ::android::binder::Status startProfiling(int32_t collectionInterval, int32_t iterations, int32_t process, int32_t samplingPeriod, int32_t samplingFrequency, int32_t sampleDuration, bool stackProfile, bool useElfSymbolizer, bool sendToDropbox) = 0;
  virtual ::android::binder::Status startProfilingString(const ::android::String16& config) = 0;
  virtual ::android::binder::Status startProfilingProtobuf(const ::std::vector<uint8_t>& config_proto) = 0;
  virtual ::android::binder::Status stopProfiling() = 0;
};  // class IPerfProfd

class IPerfProfdDefault : public IPerfProfd {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status startProfiling(int32_t collectionInterval, int32_t iterations, int32_t process, int32_t samplingPeriod, int32_t samplingFrequency, int32_t sampleDuration, bool stackProfile, bool useElfSymbolizer, bool sendToDropbox) override;
  ::android::binder::Status startProfilingString(const ::android::String16& config) override;
  ::android::binder::Status startProfilingProtobuf(const ::std::vector<uint8_t>& config_proto) override;
  ::android::binder::Status stopProfiling() override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_PERF_PROFD_H_
