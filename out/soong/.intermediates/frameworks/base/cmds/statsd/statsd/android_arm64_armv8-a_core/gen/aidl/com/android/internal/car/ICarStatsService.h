#ifndef AIDL_GENERATED_COM_ANDROID_INTERNAL_CAR_I_CAR_STATS_SERVICE_H_
#define AIDL_GENERATED_COM_ANDROID_INTERNAL_CAR_I_CAR_STATS_SERVICE_H_

#include <android/os/StatsLogEventWrapper.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <utils/StrongPointer.h>
#include <vector>

namespace com {

namespace android {

namespace internal {

namespace car {

class ICarStatsService : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(CarStatsService)
  virtual ::android::binder::Status pullData(int32_t atomId, ::std::vector<::android::os::StatsLogEventWrapper>* _aidl_return) = 0;
};  // class ICarStatsService

class ICarStatsServiceDefault : public ICarStatsService {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status pullData(int32_t atomId, ::std::vector<::android::os::StatsLogEventWrapper>* _aidl_return) override;

};

}  // namespace car

}  // namespace internal

}  // namespace android

}  // namespace com

#endif  // AIDL_GENERATED_COM_ANDROID_INTERNAL_CAR_I_CAR_STATS_SERVICE_H_
