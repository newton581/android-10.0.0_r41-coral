#ifndef AIDL_GENERATED_COM_ANDROID_INTERNAL_CAR_BP_CAR_STATS_SERVICE_H_
#define AIDL_GENERATED_COM_ANDROID_INTERNAL_CAR_BP_CAR_STATS_SERVICE_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <com/android/internal/car/ICarStatsService.h>

namespace com {

namespace android {

namespace internal {

namespace car {

class BpCarStatsService : public ::android::BpInterface<ICarStatsService> {
public:
  explicit BpCarStatsService(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpCarStatsService() = default;
  ::android::binder::Status pullData(int32_t atomId, ::std::vector<::android::os::StatsLogEventWrapper>* _aidl_return) override;
};  // class BpCarStatsService

}  // namespace car

}  // namespace internal

}  // namespace android

}  // namespace com

#endif  // AIDL_GENERATED_COM_ANDROID_INTERNAL_CAR_BP_CAR_STATS_SERVICE_H_
