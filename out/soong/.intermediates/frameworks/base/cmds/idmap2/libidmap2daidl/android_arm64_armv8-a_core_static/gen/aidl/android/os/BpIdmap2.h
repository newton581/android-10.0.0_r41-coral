#ifndef AIDL_GENERATED_ANDROID_OS_BP_IDMAP2_H_
#define AIDL_GENERATED_ANDROID_OS_BP_IDMAP2_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <utils/Errors.h>
#include <android/os/IIdmap2.h>

namespace android {

namespace os {

class BpIdmap2 : public ::android::BpInterface<IIdmap2> {
public:
  explicit BpIdmap2(const ::android::sp<::android::IBinder>& _aidl_impl);
  virtual ~BpIdmap2() = default;
  ::android::binder::Status getIdmapPath(const ::std::string& overlayApkPath, int32_t userId, ::std::string* _aidl_return) override;
  ::android::binder::Status removeIdmap(const ::std::string& overlayApkPath, int32_t userId, bool* _aidl_return) override;
  ::android::binder::Status verifyIdmap(const ::std::string& overlayApkPath, int32_t fulfilledPolicies, bool enforceOverlayable, int32_t userId, bool* _aidl_return) override;
  ::android::binder::Status createIdmap(const ::std::string& targetApkPath, const ::std::string& overlayApkPath, int32_t fulfilledPolicies, bool enforceOverlayable, int32_t userId, ::std::unique_ptr<::std::string>* _aidl_return) override;
};  // class BpIdmap2

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_BP_IDMAP2_H_
