#ifndef AIDL_GENERATED_ANDROID_OS_I_IDMAP2_H_
#define AIDL_GENERATED_ANDROID_OS_I_IDMAP2_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <memory>
#include <string>
#include <utils/StrongPointer.h>

namespace android {

namespace os {

class IIdmap2 : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(Idmap2)
  enum  : int32_t {
    POLICY_PUBLIC = 1,
    POLICY_SYSTEM_PARTITION = 2,
    POLICY_VENDOR_PARTITION = 4,
    POLICY_PRODUCT_PARTITION = 8,
    POLICY_SIGNATURE = 16,
    POLICY_ODM_PARTITION = 32,
    POLICY_OEM_PARTITION = 64,
  };
  virtual ::android::binder::Status getIdmapPath(const ::std::string& overlayApkPath, int32_t userId, ::std::string* _aidl_return) = 0;
  virtual ::android::binder::Status removeIdmap(const ::std::string& overlayApkPath, int32_t userId, bool* _aidl_return) = 0;
  virtual ::android::binder::Status verifyIdmap(const ::std::string& overlayApkPath, int32_t fulfilledPolicies, bool enforceOverlayable, int32_t userId, bool* _aidl_return) = 0;
  virtual ::android::binder::Status createIdmap(const ::std::string& targetApkPath, const ::std::string& overlayApkPath, int32_t fulfilledPolicies, bool enforceOverlayable, int32_t userId, ::std::unique_ptr<::std::string>* _aidl_return) = 0;
};  // class IIdmap2

class IIdmap2Default : public IIdmap2 {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status getIdmapPath(const ::std::string& overlayApkPath, int32_t userId, ::std::string* _aidl_return) override;
  ::android::binder::Status removeIdmap(const ::std::string& overlayApkPath, int32_t userId, bool* _aidl_return) override;
  ::android::binder::Status verifyIdmap(const ::std::string& overlayApkPath, int32_t fulfilledPolicies, bool enforceOverlayable, int32_t userId, bool* _aidl_return) override;
  ::android::binder::Status createIdmap(const ::std::string& targetApkPath, const ::std::string& overlayApkPath, int32_t fulfilledPolicies, bool enforceOverlayable, int32_t userId, ::std::unique_ptr<::std::string>* _aidl_return) override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_IDMAP2_H_
