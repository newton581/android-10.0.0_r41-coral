#ifndef AIDL_GENERATED_ANDROID_OS_BN_IDMAP2_H_
#define AIDL_GENERATED_ANDROID_OS_BN_IDMAP2_H_

#include <binder/IInterface.h>
#include <android/os/IIdmap2.h>

namespace android {

namespace os {

class BnIdmap2 : public ::android::BnInterface<IIdmap2> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnIdmap2

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_BN_IDMAP2_H_
