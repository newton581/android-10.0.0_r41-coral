// Auto generated file. Do not modify

#include "incident_sections.h"

IncidentSection const INCIDENT_SECTIONS[] = {
    { 3012, "activities" },
    { 3016, "alarm" },
    { 3015, "amprocesses" },
    { 3014, "amservices" },
    { 3003, "appwidget" },
    { 3006, "battery" },
    { 3022, "battery_history" },
    { 2006, "battery_type" },
    { 3005, "batterystats" },
    { 3013, "broadcasts" },
    { 2004, "cpu_freq" },
    { 2003, "cpu_info" },
    { 1105, "crash_logs" },
    { 3007, "diskstats" },
    { 1100, "event_log_tag_map" },
    { 1103, "events_logs" },
    { 3000, "fingerprint" },
    { 3019, "graphicsstats" },
    { 1201, "hal_traces" },
    { 1, "header" },
    { 1202, "java_traces" },
    { 3020, "jobscheduler" },
    { 1108, "kernel_logs" },
    { 1002, "kernel_version" },
    { 2002, "kernel_wake_sources" },
    { 2007, "last_kmsg" },
    { 1101, "main_logs" },
    { 3018, "meminfo" },
    { 2, "metadata" },
    { 1200, "native_traces" },
    { 3001, "netstats" },
    { 3004, "notification" },
    { 3008, "package" },
    { 2001, "page_type_info" },
    { 3009, "power" },
    { 3010, "print" },
    { 2005, "processes_and_threads" },
    { 2000, "procrank" },
    { 3011, "procstats" },
    { 1102, "radio_logs" },
    { 3025, "restricted_images" },
    { 3024, "role" },
    { 1107, "security_logs" },
    { 3002, "settings" },
    { 3023, "stats_data" },
    { 1106, "stats_logs" },
    { 1104, "system_logs" },
    { 1000, "system_properties" },
    { 3026, "system_trace" },
    { 3021, "usb" },
    { 3017, "window" }
};
const int INCIDENT_SECTION_COUNT = 51;
