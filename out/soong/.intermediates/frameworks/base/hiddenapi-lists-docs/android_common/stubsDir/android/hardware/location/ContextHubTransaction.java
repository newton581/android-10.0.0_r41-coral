/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.location;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * A class describing a request sent to the Context Hub Service.
 *
 * This object is generated as a result of an asynchronous request sent to the Context Hub
 * through the ContextHubManager APIs. The caller can either retrieve the result
 * synchronously through a blocking call ({@link #waitForResponse(long, TimeUnit)}) or
 * asynchronously through a user-defined listener
 * ({@link #setOnCompleteListener(OnCompleteListener, Executor)} )}).
 *
 * @param <T> the type of the contents in the transaction response
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ContextHubTransaction<T> {

ContextHubTransaction(int type) { throw new RuntimeException("Stub!"); }

/**
 * Converts a transaction type to a human-readable string
 *
 * @param type the type of a transaction
 * Value is {@link android.hardware.location.ContextHubTransaction#TYPE_LOAD_NANOAPP}, {@link android.hardware.location.ContextHubTransaction#TYPE_UNLOAD_NANOAPP}, {@link android.hardware.location.ContextHubTransaction#TYPE_ENABLE_NANOAPP}, {@link android.hardware.location.ContextHubTransaction#TYPE_DISABLE_NANOAPP}, or {@link android.hardware.location.ContextHubTransaction#TYPE_QUERY_NANOAPPS}
 * @param upperCase {@code true} if upper case the first letter, {@code false} otherwise
 * @return a string describing the transaction
 * @apiSince REL
 */

public static java.lang.String typeToString(int type, boolean upperCase) { throw new RuntimeException("Stub!"); }

/**
 * @return the type of the transaction
 
 * Value is {@link android.hardware.location.ContextHubTransaction#TYPE_LOAD_NANOAPP}, {@link android.hardware.location.ContextHubTransaction#TYPE_UNLOAD_NANOAPP}, {@link android.hardware.location.ContextHubTransaction#TYPE_ENABLE_NANOAPP}, {@link android.hardware.location.ContextHubTransaction#TYPE_DISABLE_NANOAPP}, or {@link android.hardware.location.ContextHubTransaction#TYPE_QUERY_NANOAPPS}
 * @apiSince REL
 */

public int getType() { throw new RuntimeException("Stub!"); }

/**
 * Waits to receive the asynchronous transaction result.
 *
 * This function blocks until the Context Hub Service has received a response
 * for the transaction represented by this object by the Context Hub, or a
 * specified timeout period has elapsed.
 *
 * If the specified timeout has passed, a TimeoutException will be thrown and the caller may
 * retry the invocation of this method at a later time.
 *
 * @param timeout the timeout duration
 * @param unit the unit of the timeout
 *
 * @return the transaction response
 *
 * @throws InterruptedException if the current thread is interrupted while waiting for response
 * @throws TimeoutException if the timeout period has passed
 * @apiSince REL
 */

public android.hardware.location.ContextHubTransaction.Response<T> waitForResponse(long timeout, java.util.concurrent.TimeUnit unit) throws java.lang.InterruptedException, java.util.concurrent.TimeoutException { throw new RuntimeException("Stub!"); }

/**
 * Sets the listener to be invoked invoked when the transaction completes.
 *
 * This function provides an asynchronous approach to retrieve the result of the
 * transaction. When the transaction response has been provided by the Context Hub,
 * the given listener will be invoked.
 *
 * If the transaction has already completed at the time of invocation, the listener
 * will be immediately invoked. If the transaction has been invalidated,
 * the listener will never be invoked.
 *
 * A transaction can be invalidated if the process owning the transaction is no longer active
 * and the reference to this object is lost.
 *
 * This method or {@link #setOnCompleteListener(ContextHubTransaction.OnCompleteListener)} can
 * only be invoked once, or an IllegalStateException will be thrown.
 *
 * @param listener the listener to be invoked upon completion
 * This value must never be {@code null}.
 * @param executor the executor to invoke the callback
 *
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @throws IllegalStateException if this method is called multiple times
 * @throws NullPointerException if the callback or handler is null
 * @apiSince REL
 */

public void setOnCompleteListener(@android.annotation.NonNull android.hardware.location.ContextHubTransaction.OnCompleteListener<T> listener, @android.annotation.NonNull java.util.concurrent.Executor executor) { throw new RuntimeException("Stub!"); }

/**
 * Sets the listener to be invoked invoked when the transaction completes.
 *
 * Equivalent to {@link #setOnCompleteListener(ContextHubTransaction.OnCompleteListener,
 * Executor)} with the executor using the main thread's Looper.
 *
 * This method or {@link #setOnCompleteListener(ContextHubTransaction.OnCompleteListener,
 * Executor)} can only be invoked once, or an IllegalStateException will be thrown.
 *
 * @param listener the listener to be invoked upon completion
 *
 * This value must never be {@code null}.
 * @throws IllegalStateException if this method is called multiple times
 * @throws NullPointerException if the callback is null
 * @apiSince REL
 */

public void setOnCompleteListener(@android.annotation.NonNull android.hardware.location.ContextHubTransaction.OnCompleteListener<T> listener) { throw new RuntimeException("Stub!"); }

/**
 * Failure mode when the request went through, but failed asynchronously at the hub.
 * @apiSince REL
 */

public static final int RESULT_FAILED_AT_HUB = 5; // 0x5

/**
 * Failure mode when the request parameters were not valid.
 * @apiSince REL
 */

public static final int RESULT_FAILED_BAD_PARAMS = 2; // 0x2

/**
 * Failure mode when there are too many transactions pending.
 * @apiSince REL
 */

public static final int RESULT_FAILED_BUSY = 4; // 0x4

/**
 * Failure mode when the Context Hub HAL was not available.
 * @apiSince REL
 */

public static final int RESULT_FAILED_HAL_UNAVAILABLE = 8; // 0x8

/**
 * Failure mode when the transaction has failed internally at the service.
 * @apiSince REL
 */

public static final int RESULT_FAILED_SERVICE_INTERNAL_FAILURE = 7; // 0x7

/**
 * Failure mode when the transaction has timed out.
 * @apiSince REL
 */

public static final int RESULT_FAILED_TIMEOUT = 6; // 0x6

/**
 * Failure mode when the Context Hub is not initialized.
 * @apiSince REL
 */

public static final int RESULT_FAILED_UNINITIALIZED = 3; // 0x3

/**
 * Generic failure mode.
 * @apiSince REL
 */

public static final int RESULT_FAILED_UNKNOWN = 1; // 0x1

/** @apiSince REL */

public static final int RESULT_SUCCESS = 0; // 0x0

/** @apiSince REL */

public static final int TYPE_DISABLE_NANOAPP = 3; // 0x3

/** @apiSince REL */

public static final int TYPE_ENABLE_NANOAPP = 2; // 0x2

/** @apiSince REL */

public static final int TYPE_LOAD_NANOAPP = 0; // 0x0

/** @apiSince REL */

public static final int TYPE_QUERY_NANOAPPS = 4; // 0x4

/** @apiSince REL */

public static final int TYPE_UNLOAD_NANOAPP = 1; // 0x1
/**
 * An interface describing the listener for a transaction completion.
 *
 * @param <L> the type of the contents in the transaction response
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.FunctionalInterface
public static interface OnCompleteListener<L> {

/**
 * The listener function to invoke when the transaction completes.
 *
 * @param transaction the transaction that this callback was attached to.
 * @param response the response of the transaction.
 * @apiSince REL
 */

public void onComplete(android.hardware.location.ContextHubTransaction<L> transaction, android.hardware.location.ContextHubTransaction.Response<L> response);
}

/**
 * A class describing the response for a ContextHubTransaction.
 *
 * @param <R> the type of the contents in the response
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Response<R> {

Response(int result, R contents) { throw new RuntimeException("Stub!"); }

/**
 * @return Value is {@link android.hardware.location.ContextHubTransaction#RESULT_SUCCESS}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_UNKNOWN}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_BAD_PARAMS}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_UNINITIALIZED}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_BUSY}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_AT_HUB}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_TIMEOUT}, {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_SERVICE_INTERNAL_FAILURE}, or {@link android.hardware.location.ContextHubTransaction#RESULT_FAILED_HAL_UNAVAILABLE}
 * @apiSince REL
 */

public int getResult() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public R getContents() { throw new RuntimeException("Stub!"); }
}

}

