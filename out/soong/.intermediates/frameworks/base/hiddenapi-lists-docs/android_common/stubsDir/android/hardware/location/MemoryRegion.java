/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.location;


/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MemoryRegion implements android.os.Parcelable {

/** @apiSince REL */

public MemoryRegion(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/**
 * get the capacity of the memory region in bytes
 *
 * @return int - the memory capacity in bytes
 * @apiSince REL
 */

public int getCapacityBytes() { throw new RuntimeException("Stub!"); }

/**
 * get the free capacity of the memory region in bytes
 *
 * @return int - free bytes
 * @apiSince REL
 */

public int getFreeCapacityBytes() { throw new RuntimeException("Stub!"); }

/**
 * Is the memory readable
 *
 * @return boolean - true if memory is readable, false otherwise
 * @apiSince REL
 */

public boolean isReadable() { throw new RuntimeException("Stub!"); }

/**
 * Is the memory writable
 *
 * @return boolean - true if memory is writable, false otherwise
 * @apiSince REL
 */

public boolean isWritable() { throw new RuntimeException("Stub!"); }

/**
 * Is the memory executable
 *
 * @return boolean - true if memory is executable, false
 *         otherwise
 * @apiSince REL
 */

public boolean isExecutable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param object This value may be {@code null}.
 * @apiSince REL
 */

public boolean equals(@android.annotation.Nullable java.lang.Object object) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.location.MemoryRegion> CREATOR;
static { CREATOR = null; }
}

