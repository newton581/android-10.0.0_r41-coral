/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.contentsuggestions;


/**
 * Represents a suggested selection within a set of on screen content. The result of a
 * {@link SelectionsRequest} to {@link ContentSuggestionsManager}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ContentSelection implements android.os.Parcelable {

/**
 * Default constructor.
 *
 * @param selectionId implementation specific id for the selection.
 * This value must never be {@code null}.
 * @param extras containing the data that represents the selection.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public ContentSelection(@android.annotation.NonNull java.lang.String selectionId, @android.annotation.NonNull android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Return the selection id.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getId() { throw new RuntimeException("Stub!"); }

/**
 * Return the selection extras.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.Bundle getExtras() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.contentsuggestions.ContentSelection> CREATOR;
static { CREATOR = null; }
}

