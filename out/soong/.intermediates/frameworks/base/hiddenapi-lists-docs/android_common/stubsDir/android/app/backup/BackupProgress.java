/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.app.backup;


/**
 * Information about current progress of full data backup
 * Used in {@link BackupObserver#onUpdate(String, BackupProgress)}
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class BackupProgress implements android.os.Parcelable {

/** @apiSince REL */

public BackupProgress(long _bytesExpected, long _bytesTransferred) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.backup.BackupProgress> CREATOR;
static { CREATOR = null; }

/**
 * Expected size of data in full backup.
 * @apiSince REL
 */

public final long bytesExpected;
{ bytesExpected = 0; }

/**
 * Amount of backup data that is already saved in backup.
 * @apiSince REL
 */

public final long bytesTransferred;
{ bytesTransferred = 0; }
}

