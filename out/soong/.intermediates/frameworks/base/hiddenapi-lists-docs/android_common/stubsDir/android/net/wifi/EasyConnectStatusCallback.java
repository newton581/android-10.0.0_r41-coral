/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.wifi;

import android.os.Handler;

/**
 * Easy Connect (DPP) Status Callback. Use this callback to get status updates (success, failure,
 * progress) from the Easy Connect operation started with
 * {@link WifiManager#startEasyConnectAsConfiguratorInitiator(String,
 * int, int, Handler, EasyConnectStatusCallback)} or
 * {@link WifiManager#startEasyConnectAsEnrolleeInitiator(String,
 * Handler, EasyConnectStatusCallback)}
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class EasyConnectStatusCallback {

public EasyConnectStatusCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called when local Easy Connect Enrollee successfully receives a new Wi-Fi configuration from
 * the
 * peer Easy Connect configurator. This callback marks the successful end of the Easy Connect
 * current Easy Connect
 * session, and no further callbacks will be called. This callback is the successful outcome
 * of a Easy Connect flow starting with
 * {@link WifiManager#startEasyConnectAsEnrolleeInitiator(String,
 * Handler,
 * EasyConnectStatusCallback)}.
 *
 * @param newNetworkId New Wi-Fi configuration with a network ID received from the configurator
 * @apiSince REL
 */

public abstract void onEnrolleeSuccess(int newNetworkId);

/**
 * Called when a Easy Connect success event takes place, except for when configuration is
 * received from
 * an external Configurator. The callback onSuccessConfigReceived will be used in this case.
 * This callback marks the successful end of the current Easy Connect session, and no further
 * callbacks will be called. This callback is the successful outcome of a Easy Connect flow
 * starting with
 * {@link WifiManager#startEasyConnectAsConfiguratorInitiator(String, int, int, Handler,
 * EasyConnectStatusCallback)}.
 *
 * @param code Easy Connect success status code.
 
 * Value is {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_SUCCESS_CONFIGURATION_SENT}
 * @apiSince REL
 */

public abstract void onConfiguratorSuccess(int code);

/**
 * Called when a Easy Connect Failure event takes place. This callback marks the unsuccessful
 * end of the
 * current Easy Connect session, and no further callbacks will be called.
 *
 * @param code Easy Connect failure status code.
 
 * Value is {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_FAILURE_INVALID_URI}, {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_FAILURE_AUTHENTICATION}, {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_FAILURE_NOT_COMPATIBLE}, {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_FAILURE_CONFIGURATION}, {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_FAILURE_BUSY}, {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_FAILURE_TIMEOUT}, {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_FAILURE_GENERIC}, {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_FAILURE_NOT_SUPPORTED}, or {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_FAILURE_INVALID_NETWORK}
 * @apiSince REL
 */

public abstract void onFailure(int code);

/**
 * Called when Easy Connect events that indicate progress take place. Can be used by UI elements
 * to show progress.
 *
 * @param code Easy Connect progress status code.
 
 * Value is {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_PROGRESS_AUTHENTICATION_SUCCESS}, or {@link android.net.wifi.EasyConnectStatusCallback#EASY_CONNECT_EVENT_PROGRESS_RESPONSE_PENDING}
 * @apiSince REL
 */

public abstract void onProgress(int code);

/**
 * Easy Connect Failure event: Bootstrapping/Authentication initialization process failure.
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_FAILURE_AUTHENTICATION = -2; // 0xfffffffe

/**
 * Easy Connect Failure event: Easy Connect request while in another Easy Connect exchange.
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_FAILURE_BUSY = -5; // 0xfffffffb

/**
 * Easy Connect Failure event: Configuration process has failed due to malformed message.
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_FAILURE_CONFIGURATION = -4; // 0xfffffffc

/**
 * Easy Connect Failure event: General protocol failure.
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_FAILURE_GENERIC = -7; // 0xfffffff9

/**
 * Easy Connect Failure event: Invalid network provided to Easy Connect configurator.
 * Network must either be WPA3-Personal (SAE) or WPA2-Personal (PSK).
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_FAILURE_INVALID_NETWORK = -9; // 0xfffffff7

/**
 * Easy Connect Failure event: Scanned QR code is either not a Easy Connect URI, or the Easy
 * Connect URI has errors.
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_FAILURE_INVALID_URI = -1; // 0xffffffff

/**
 * Easy Connect Failure event: Both devices are implementing the same role and are incompatible.
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_FAILURE_NOT_COMPATIBLE = -3; // 0xfffffffd

/**
 * Easy Connect Failure event: Feature or option is not supported.
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_FAILURE_NOT_SUPPORTED = -8; // 0xfffffff8

/**
 * Easy Connect Failure event: No response from the peer.
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_FAILURE_TIMEOUT = -6; // 0xfffffffa

/**
 * Easy Connect Progress event: Initial authentication with peer succeeded.
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_PROGRESS_AUTHENTICATION_SUCCESS = 0; // 0x0

/**
 * Easy Connect Progress event: Peer requires more time to process bootstrapping.
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_PROGRESS_RESPONSE_PENDING = 1; // 0x1

/**
 * Easy Connect Success event: Configuration sent (Configurator mode).
 * @apiSince REL
 */

public static final int EASY_CONNECT_EVENT_SUCCESS_CONFIGURATION_SENT = 0; // 0x0
}

