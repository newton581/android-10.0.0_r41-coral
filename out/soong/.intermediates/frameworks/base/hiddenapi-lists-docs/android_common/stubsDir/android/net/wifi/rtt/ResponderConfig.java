/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.wifi.rtt;

import android.net.wifi.ScanResult;
import android.net.wifi.aware.PeerHandle;

/**
 * Defines the configuration of an IEEE 802.11mc Responder. The Responder may be an Access Point
 * (AP), a Wi-Fi Aware device, or a manually configured Responder.
 * <p>
 * A Responder configuration may be constructed from a {@link ScanResult} or manually (with the
 * data obtained out-of-band from a peer).
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ResponderConfig implements android.os.Parcelable {

/**
 * Constructs Responder configuration, using a MAC address to identify the Responder.
 *
 * @param macAddress      The MAC address of the Responder.
 * This value must never be {@code null}.
 * @param responderType   The type of the responder device, specified using
 *                        {@link ResponderType}.
 * Value is {@link android.net.wifi.rtt.ResponderConfig#RESPONDER_AP}, {@link android.net.wifi.rtt.ResponderConfig#RESPONDER_STA}, {@link android.net.wifi.rtt.ResponderConfig#RESPONDER_P2P_GO}, {@link android.net.wifi.rtt.ResponderConfig#RESPONDER_P2P_CLIENT}, or {@link android.net.wifi.rtt.ResponderConfig#RESPONDER_AWARE}
 * @param supports80211mc Indicates whether the responder supports IEEE 802.11mc.
 * @param channelWidth    Responder channel bandwidth, specified using {@link ChannelWidth}.
 * Value is {@link android.net.wifi.rtt.ResponderConfig#CHANNEL_WIDTH_20MHZ}, {@link android.net.wifi.rtt.ResponderConfig#CHANNEL_WIDTH_40MHZ}, {@link android.net.wifi.rtt.ResponderConfig#CHANNEL_WIDTH_80MHZ}, {@link android.net.wifi.rtt.ResponderConfig#CHANNEL_WIDTH_160MHZ}, or {@link android.net.wifi.rtt.ResponderConfig#CHANNEL_WIDTH_80MHZ_PLUS_MHZ}
 * @param frequency       The primary 20 MHz frequency (in MHz) of the channel of the Responder.
 * @param centerFreq0     Not used if the {@code channelWidth} is 20 MHz. If the Responder uses
 *                        40, 80 or 160 MHz, this is the center frequency (in MHz), if the
 *                        Responder uses 80 + 80 MHz, this is the center frequency of the first
 *                        segment (in MHz).
 * @param centerFreq1     Only used if the {@code channelWidth} is 80 + 80 MHz. If the
 *                        Responder
 *                        uses 80 + 80 MHz, this is the center frequency of the second segment
 *                        (in
 *                        MHz).
 * @param preamble        The preamble used by the Responder, specified using
 *                        {@link PreambleType}.
 
 * Value is {@link android.net.wifi.rtt.ResponderConfig#PREAMBLE_LEGACY}, {@link android.net.wifi.rtt.ResponderConfig#PREAMBLE_HT}, or {@link android.net.wifi.rtt.ResponderConfig#PREAMBLE_VHT}
 * @apiSince REL
 */

public ResponderConfig(@android.annotation.NonNull android.net.MacAddress macAddress, int responderType, boolean supports80211mc, int channelWidth, int frequency, int centerFreq0, int centerFreq1, int preamble) { throw new RuntimeException("Stub!"); }

/**
 * Constructs Responder configuration, using a Wi-Fi Aware PeerHandle to identify the Responder.
 *
 * @param peerHandle      The Wi-Fi Aware peer identifier of the Responder.
 * This value must never be {@code null}.
 * @param responderType   The type of the responder device, specified using
 *                        {@link ResponderType}.
 * Value is {@link android.net.wifi.rtt.ResponderConfig#RESPONDER_AP}, {@link android.net.wifi.rtt.ResponderConfig#RESPONDER_STA}, {@link android.net.wifi.rtt.ResponderConfig#RESPONDER_P2P_GO}, {@link android.net.wifi.rtt.ResponderConfig#RESPONDER_P2P_CLIENT}, or {@link android.net.wifi.rtt.ResponderConfig#RESPONDER_AWARE}
 * @param supports80211mc Indicates whether the responder supports IEEE 802.11mc.
 * @param channelWidth    Responder channel bandwidth, specified using {@link ChannelWidth}.
 * Value is {@link android.net.wifi.rtt.ResponderConfig#CHANNEL_WIDTH_20MHZ}, {@link android.net.wifi.rtt.ResponderConfig#CHANNEL_WIDTH_40MHZ}, {@link android.net.wifi.rtt.ResponderConfig#CHANNEL_WIDTH_80MHZ}, {@link android.net.wifi.rtt.ResponderConfig#CHANNEL_WIDTH_160MHZ}, or {@link android.net.wifi.rtt.ResponderConfig#CHANNEL_WIDTH_80MHZ_PLUS_MHZ}
 * @param frequency       The primary 20 MHz frequency (in MHz) of the channel of the Responder.
 * @param centerFreq0     Not used if the {@code channelWidth} is 20 MHz. If the Responder uses
 *                        40, 80 or 160 MHz, this is the center frequency (in MHz), if the
 *                        Responder uses 80 + 80 MHz, this is the center frequency of the first
 *                        segment (in MHz).
 * @param centerFreq1     Only used if the {@code channelWidth} is 80 + 80 MHz. If the
 *                        Responder
 *                        uses 80 + 80 MHz, this is the center frequency of the second segment
 *                        (in
 *                        MHz).
 * @param preamble        The preamble used by the Responder, specified using
 *                        {@link PreambleType}.
 
 * Value is {@link android.net.wifi.rtt.ResponderConfig#PREAMBLE_LEGACY}, {@link android.net.wifi.rtt.ResponderConfig#PREAMBLE_HT}, or {@link android.net.wifi.rtt.ResponderConfig#PREAMBLE_VHT}
 * @apiSince REL
 */

public ResponderConfig(@android.annotation.NonNull android.net.wifi.aware.PeerHandle peerHandle, int responderType, boolean supports80211mc, int channelWidth, int frequency, int centerFreq0, int centerFreq1, int preamble) { throw new RuntimeException("Stub!"); }

/**
 * Creates a Responder configuration from a {@link ScanResult} corresponding to an Access
 * Point (AP), which can be obtained from {@link android.net.wifi.WifiManager#getScanResults()}.
 * @apiSince REL
 */

public static android.net.wifi.rtt.ResponderConfig fromScanResult(android.net.wifi.ScanResult scanResult) { throw new RuntimeException("Stub!"); }

/**
 * Creates a Responder configuration from a MAC address corresponding to a Wi-Fi Aware
 * Responder. The Responder parameters are set to defaults.
 * @apiSince REL
 */

public static android.net.wifi.rtt.ResponderConfig fromWifiAwarePeerMacAddressWithDefaults(android.net.MacAddress macAddress) { throw new RuntimeException("Stub!"); }

/**
 * Creates a Responder configuration from a {@link PeerHandle} corresponding to a Wi-Fi Aware
 * Responder. The Responder parameters are set to defaults.
 * @apiSince REL
 */

public static android.net.wifi.rtt.ResponderConfig fromWifiAwarePeerHandleWithDefaults(android.net.wifi.aware.PeerHandle peerHandle) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @hide */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Channel bandwidth is 160 MHZ
 * @apiSince REL
 */

public static final int CHANNEL_WIDTH_160MHZ = 3; // 0x3

/**
 * Channel bandwidth is 20 MHZ
 * @apiSince REL
 */

public static final int CHANNEL_WIDTH_20MHZ = 0; // 0x0

/**
 * Channel bandwidth is 40 MHZ
 * @apiSince REL
 */

public static final int CHANNEL_WIDTH_40MHZ = 1; // 0x1

/**
 * Channel bandwidth is 80 MHZ
 * @apiSince REL
 */

public static final int CHANNEL_WIDTH_80MHZ = 2; // 0x2

/**
 * Channel bandwidth is 160 MHZ, but 80MHZ + 80MHZ
 * @apiSince REL
 */

public static final int CHANNEL_WIDTH_80MHZ_PLUS_MHZ = 4; // 0x4

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.net.wifi.rtt.ResponderConfig> CREATOR;
static { CREATOR = null; }

/**
 * Preamble type: HT.
 * @apiSince REL
 */

public static final int PREAMBLE_HT = 1; // 0x1

/**
 * Preamble type: Legacy.
 * @apiSince REL
 */

public static final int PREAMBLE_LEGACY = 0; // 0x0

/**
 * Preamble type: VHT.
 * @apiSince REL
 */

public static final int PREAMBLE_VHT = 2; // 0x2

/**
 * Responder is an AP.
 * @apiSince REL
 */

public static final int RESPONDER_AP = 0; // 0x0

/**
 * Responder is a Wi-Fi Aware device.
 * @apiSince REL
 */

public static final int RESPONDER_AWARE = 4; // 0x4

/**
 * Responder is a Wi-Fi Direct Group Client.
 * @apiSince REL
 */

public static final int RESPONDER_P2P_CLIENT = 3; // 0x3

/**
 * Responder is a Wi-Fi Direct Group Owner (GO).
 * @apiSince REL
 */

public static final int RESPONDER_P2P_GO = 2; // 0x2

/**
 * Responder is a STA.
 * @apiSince REL
 */

public static final int RESPONDER_STA = 1; // 0x1

/**
 * Not used if the {@link #channelWidth} is 20 MHz. If the Responder uses 40, 80 or 160 MHz,
 * this is the center frequency (in MHz), if the Responder uses 80 + 80 MHz, this is the
 * center frequency of the first segment (in MHz).
 * @apiSince REL
 */

public final int centerFreq0;
{ centerFreq0 = 0; }

/**
 * Only used if the {@link #channelWidth} is 80 + 80 MHz. If the Responder uses 80 + 80 MHz,
 * this is the center frequency of the second segment (in MHz).
 * @apiSince REL
 */

public final int centerFreq1;
{ centerFreq1 = 0; }

/**
 * Responder channel bandwidth, specified using {@link ChannelWidth}.
 * @apiSince REL
 */

public final int channelWidth;
{ channelWidth = 0; }

/**
 * The primary 20 MHz frequency (in MHz) of the channel of the Responder.
 * @apiSince REL
 */

public final int frequency;
{ frequency = 0; }

/**
 * The MAC address of the Responder. Will be null if a Wi-Fi Aware peer identifier (the
 * peerHandle field) ise used to identify the Responder.
 * @apiSince REL
 */

public final android.net.MacAddress macAddress;
{ macAddress = null; }

/**
 * The peer identifier of a Wi-Fi Aware Responder. Will be null if a MAC Address (the macAddress
 * field) is used to identify the Responder.
 * @apiSince REL
 */

public final android.net.wifi.aware.PeerHandle peerHandle;
{ peerHandle = null; }

/**
 * The preamble used by the Responder, specified using {@link PreambleType}.
 * @apiSince REL
 */

public final int preamble;
{ preamble = 0; }

/**
 * The device type of the Responder.
 * @apiSince REL
 */

public final int responderType;
{ responderType = 0; }

/**
 * Indicates whether the Responder device supports IEEE 802.11mc.
 * @apiSince REL
 */

public final boolean supports80211mc;
{ supports80211mc = false; }
}

