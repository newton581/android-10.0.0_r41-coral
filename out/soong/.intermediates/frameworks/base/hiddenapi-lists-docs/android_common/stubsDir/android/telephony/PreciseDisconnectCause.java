/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;


/**
 * Contains precise disconnect call causes generated by the framework and the RIL.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class PreciseDisconnectCause {

/** Private constructor to avoid class instantiation. */

PreciseDisconnectCause() { throw new RuntimeException("Stub!"); }

/**
 * Call setup failed because of access class barring.
 * @apiSince REL
 */

public static final int ACCESS_CLASS_BLOCKED = 260; // 0x104

/**
 * The network could not deliver access information to the remote user as requested.
 * @apiSince REL
 */

public static final int ACCESS_INFORMATION_DISCARDED = 43; // 0x2b

/**
 * The call clearing is due to ACM being greater than or equal to ACMmax.
 * @apiSince REL
 */

public static final int ACM_LIMIT_EXCEEDED = 68; // 0x44

/**
 * The mobile station is not authorized to use bearer capability requested.
 * @apiSince REL
 */

public static final int BEARER_CAPABILITY_NOT_AUTHORIZED = 57; // 0x39

/**
 * The requested bearer capability is not available at this time.
 * @apiSince REL
 */

public static final int BEARER_NOT_AVAIL = 58; // 0x3a

/**
 * The equipment sending this cause does not support the bearer capability requested.
 * @apiSince REL
 */

public static final int BEARER_SERVICE_NOT_IMPLEMENTED = 65; // 0x41

/**
 * The called user is unable to accept another call.
 * @apiSince REL
 */

public static final int BUSY = 17; // 0x11

/**
 * The call is restricted.
 * @apiSince REL
 */

public static final int CALL_BARRED = 240; // 0xf0

/**
 * The equipment sending this cause does not wish to accept this call.
 * @apiSince REL
 */

public static final int CALL_REJECTED = 21; // 0x15

/**
 * Access Blocked by CDMA network.
 * @apiSince REL
 */

public static final int CDMA_ACCESS_BLOCKED = 1009; // 0x3f1

/**
 * Unable to obtain access to the CDMA system.
 * @apiSince REL
 */

public static final int CDMA_ACCESS_FAILURE = 1006; // 0x3ee

/**
 * Drop call.
 * @apiSince REL
 */

public static final int CDMA_DROP = 1001; // 0x3e9

/**
 * INTERCEPT order received, Mobile station (MS) state idle entered.
 * @apiSince REL
 */

public static final int CDMA_INTERCEPT = 1002; // 0x3ea

/**
 * Mobile station (MS) is locked until next power cycle.
 * @apiSince REL
 */

public static final int CDMA_LOCKED_UNTIL_POWER_CYCLE = 1000; // 0x3e8

/**
 * Not an emergency call.
 * @apiSince REL
 */

public static final int CDMA_NOT_EMERGENCY = 1008; // 0x3f0

/**
 * Not a preempted call.
 * @apiSince REL
 */

public static final int CDMA_PREEMPTED = 1007; // 0x3ef

/**
 * Mobile station (MS) has been redirected, call is cancelled.
 * @apiSince REL
 */

public static final int CDMA_REORDER = 1003; // 0x3eb

/**
 * Requested service is rejected, retry delay is set.
 * @apiSince REL
 */

public static final int CDMA_RETRY_ORDER = 1005; // 0x3ed

/**
 * Service option rejection.
 * @apiSince REL
 */

public static final int CDMA_SO_REJECT = 1004; // 0x3ec

/**
 * The channel cannot be provided.
 * @apiSince REL
 */

public static final int CHANNEL_NOT_AVAIL = 44; // 0x2c

/**
 * The channel most recently identified is not acceptable to the sending entity for use in this
 * call.
 * @apiSince REL
 */

public static final int CHANNEL_UNACCEPTABLE = 6; // 0x6

/**
 * The equipment sending this cause has received a message with conditional IE errors.
 * @apiSince REL
 */

public static final int CONDITIONAL_IE_ERROR = 100; // 0x64

/**
 * The destination indicated by the mobile station cannot be reached because the interface to
 * the destination is not functioning correctly.
 * @apiSince REL
 */

public static final int DESTINATION_OUT_OF_ORDER = 27; // 0x1b

/**
 * Disconnected due to unspecified reasons.
 * @apiSince REL
 */

public static final int ERROR_UNSPECIFIED = 65535; // 0xffff

/**
 * The facility requested by user can not be provided by the network.
 * @apiSince REL
 */

public static final int FACILITY_REJECTED = 29; // 0x1d

/**
 * The call is blocked by the Fixed Dialing Number list.
 * @apiSince REL
 */

public static final int FDN_BLOCKED = 241; // 0xf1

/**
 * The network does not accept emergency call establishment using an IMEI or not accept attach
 * procedure for emergency services using an IMEI.
 * @apiSince REL
 */

public static final int IMEI_NOT_ACCEPTED = 243; // 0xf3

/**
 * The given IMSI is not known at the Visitor Location Register (VLR) TS 24.008 cause .
 * @apiSince REL
 */

public static final int IMSI_UNKNOWN_IN_VLR = 242; // 0xf2

/**
 * Incoming calls are not allowed within this calling user group (CUG).
 * @apiSince REL
 */

public static final int INCOMING_CALLS_BARRED_WITHIN_CUG = 55; // 0x37

/**
 * The equipment sending this cause has received a request which can't be accomodated.
 * @apiSince REL
 */

public static final int INCOMPATIBLE_DESTINATION = 88; // 0x58

/**
 * The equipment sending this cause has received a message which includes information
 * elements not recognized because its identifier is not defined or it is defined but not
 * implemented by the equipment sending the cause.
 * @apiSince REL
 */

public static final int INFORMATION_ELEMENT_NON_EXISTENT = 99; // 0x63

/**
 * Interworking with a network which does not provide causes for actions it takes thus, the
 * precise cause for a message which is being sent cannot be ascertained.
 * @apiSince REL
 */

public static final int INTERWORKING_UNSPECIFIED = 127; // 0x7f

/**
 * The equipment sending this cause has received a message with a non-semantical mandatory
 * information element (IE) error.
 * @apiSince REL
 */

public static final int INVALID_MANDATORY_INFORMATION = 96; // 0x60

/**
 * The called party number is not a valid format or is not complete.
 * @apiSince REL
 */

public static final int INVALID_NUMBER_FORMAT = 28; // 0x1c

/**
 * The equipment sending this cause has received a message with a transaction identifier
 * which is not currently in use on the mobile station network interface.
 * @apiSince REL
 */

public static final int INVALID_TRANSACTION_IDENTIFIER = 81; // 0x51

/**
 * The message has been received which is incompatible with the protocol state.
 * @apiSince REL
 */

public static final int MESSAGE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE = 101; // 0x65

/**
 * This is sent in response to a message which is not defined, or defined but not implemented
 * by the equipment sending this cause.
 * @apiSince REL
 */

public static final int MESSAGE_TYPE_NON_IMPLEMENTED = 97; // 0x61

/**
 * The equipment sending this cause has received a message not compatible with the protocol
 * state.
 * @apiSince REL
 */

public static final int MESSAGE_TYPE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE = 98; // 0x62

/**
 * Call failed/dropped because of a network detach.
 * @apiSince REL
 */

public static final int NETWORK_DETACH = 261; // 0x105

/**
 * The network is not functioning correctly and that the condition is likely to last a
 * relatively long period of time.
 * @apiSince REL
 */

public static final int NETWORK_OUT_OF_ORDER = 38; // 0x26

/**
 * Call failed because of a network reject.
 * @apiSince REL
 */

public static final int NETWORK_REJECT = 252; // 0xfc

/**
 * Call failed because of UE timer expired while waiting for a response from network.
 * @apiSince REL
 */

public static final int NETWORK_RESP_TIMEOUT = 251; // 0xfb

/**
 * One of the users involved in the call has requested that the call is cleared.
 * @apiSince REL
 */

public static final int NORMAL = 16; // 0x10

/**
 * Reports a normal disconnect only when no other normal cause applies.
 * @apiSince REL
 */

public static final int NORMAL_UNSPECIFIED = 31; // 0x1f

/**
 * The disconnect cause is not valid (Not received a disconnect cause).
 * @apiSince REL
 */

public static final int NOT_VALID = -1; // 0xffffffff

/**
 * The user has provided an alerting indication but has not provided a connect indication
 * within a prescribed period of time.
 * @apiSince REL
 */

public static final int NO_ANSWER_FROM_USER = 19; // 0x13

/**
 * There is no channel presently available to handle the call.
 * @apiSince REL
 */

public static final int NO_CIRCUIT_AVAIL = 34; // 0x22

/**
 * No disconnect cause provided. Generally a local disconnect or an incoming missed call.
 * @apiSince REL
 */

public static final int NO_DISCONNECT_CAUSE_AVAILABLE = 0; // 0x0

/**
 * The user cannot be reached because the network through which the call has been routed does
 * not serve the destination desired.
 * @apiSince REL
 */

public static final int NO_ROUTE_TO_DESTINATION = 3; // 0x3

/**
 * The user does not respond to a call establishment message with either an alerting or connect
 * indication within the prescribed period of time allocated.
 * @apiSince REL
 */

public static final int NO_USER_RESPONDING = 18; // 0x12

/**
 * The call cannot be established because of no valid SIM.
 * @apiSince REL
 */

public static final int NO_VALID_SIM = 249; // 0xf9

/**
 * The called number is no longer assigned.
 * @apiSince REL
 */

public static final int NUMBER_CHANGED = 22; // 0x16

/** @apiSince REL */

public static final int OEM_CAUSE_1 = 61441; // 0xf001

/** @apiSince REL */

public static final int OEM_CAUSE_10 = 61450; // 0xf00a

/** @apiSince REL */

public static final int OEM_CAUSE_11 = 61451; // 0xf00b

/** @apiSince REL */

public static final int OEM_CAUSE_12 = 61452; // 0xf00c

/** @apiSince REL */

public static final int OEM_CAUSE_13 = 61453; // 0xf00d

/** @apiSince REL */

public static final int OEM_CAUSE_14 = 61454; // 0xf00e

/** @apiSince REL */

public static final int OEM_CAUSE_15 = 61455; // 0xf00f

/** @apiSince REL */

public static final int OEM_CAUSE_2 = 61442; // 0xf002

/** @apiSince REL */

public static final int OEM_CAUSE_3 = 61443; // 0xf003

/** @apiSince REL */

public static final int OEM_CAUSE_4 = 61444; // 0xf004

/** @apiSince REL */

public static final int OEM_CAUSE_5 = 61445; // 0xf005

/** @apiSince REL */

public static final int OEM_CAUSE_6 = 61446; // 0xf006

/** @apiSince REL */

public static final int OEM_CAUSE_7 = 61447; // 0xf007

/** @apiSince REL */

public static final int OEM_CAUSE_8 = 61448; // 0xf008

/** @apiSince REL */

public static final int OEM_CAUSE_9 = 61449; // 0xf009

/**
 * The equipment sending this cause only supports the restricted version of the requested bearer
 * capability.
 * @apiSince REL
 */

public static final int ONLY_DIGITAL_INFORMATION_BEARER_AVAILABLE = 70; // 0x46

/**
 * The mobile station (MS) has tried to access a service that the MS's network operator or
 * service provider is not prepared to allow.
 * @apiSince REL
 */

public static final int OPERATOR_DETERMINED_BARRING = 8; // 0x8

/**
 * The call cannot be established because of no cell coverage.
 * @apiSince REL
 */

public static final int OUT_OF_SRV = 248; // 0xf8

/**
 * This cause is returned to the network when a mobile station clears an active call which is
 * being pre-empted by another call with higher precedence.
 * @apiSince REL
 */

public static final int PREEMPTION = 25; // 0x19

/**
 * This protocol error event is reported only when no other cause in the protocol error class
 * applies.
 * @apiSince REL
 */

public static final int PROTOCOL_ERROR_UNSPECIFIED = 111; // 0x6f

/**
 * The requested quality of service (ITU-T X.213) cannot be provided.
 * @apiSince REL
 */

public static final int QOS_NOT_AVAIL = 49; // 0x31

/**
 * Call failed because of radio access failure. ex. RACH failure.
 * @apiSince REL
 */

public static final int RADIO_ACCESS_FAILURE = 253; // 0xfd

/**
 * The call is dropped or failed internally by modem.
 * @apiSince REL
 */

public static final int RADIO_INTERNAL_ERROR = 250; // 0xfa

/**
 * Call failed/dropped because of a Radio Link Failure (RLF).
 * @apiSince REL
 */

public static final int RADIO_LINK_FAILURE = 254; // 0xfe

/**
 * Call failed/dropped because of radio link lost.
 * @apiSince REL
 */

public static final int RADIO_LINK_LOST = 255; // 0xff

/**
 * The call cannot be established because RADIO is OFF.
 * @apiSince REL
 */

public static final int RADIO_OFF = 247; // 0xf7

/**
 * Call failed/dropped because of RRC (Radio Resource Control) abnormally released by
 * modem/network.
 * @apiSince REL
 */

public static final int RADIO_RELEASE_ABNORMAL = 259; // 0x103

/**
 * Call failed/dropped because of RRC (Radio Resource Control) connection release from NW.
 * @apiSince REL
 */

public static final int RADIO_RELEASE_NORMAL = 258; // 0x102

/**
 * Call failed because of a RRC (Radio Resource Control) connection setup failure.
 * @apiSince REL
 */

public static final int RADIO_SETUP_FAILURE = 257; // 0x101

/**
 * Call failed because of a radio uplink issue.
 * @apiSince REL
 */

public static final int RADIO_UPLINK_FAILURE = 256; // 0x100

/**
 * The procedure has been initiated by the expiry of a timer in association with
 * 3GPP TS 24.008 error handling procedures.
 * @apiSince REL
 */

public static final int RECOVERY_ON_TIMER_EXPIRED = 102; // 0x66

/**
 * The equipment sending this cause does not support the requested facility.
 * @apiSince REL
 */

public static final int REQUESTED_FACILITY_NOT_IMPLEMENTED = 69; // 0x45

/**
 * The facility could not be provided by the network because the user has no complete
 * subscription.
 * @apiSince REL
 */

public static final int REQUESTED_FACILITY_NOT_SUBSCRIBED = 50; // 0x32

/**
 * This cause is used to report a resource unavailable event only when no other cause in the
 * resource unavailable class applies.
 * @apiSince REL
 */

public static final int RESOURCES_UNAVAILABLE_OR_UNSPECIFIED = 47; // 0x2f

/**
 * This cause is used to report receipt of a message with semantically incorrect contents.
 * @apiSince REL
 */

public static final int SEMANTICALLY_INCORRECT_MESSAGE = 95; // 0x5f

/**
 * The service option is not availble at this time.
 * @apiSince REL
 */

public static final int SERVICE_OPTION_NOT_AVAILABLE = 63; // 0x3f

/**
 * The service requested is not implemented at network.
 * @apiSince REL
 */

public static final int SERVICE_OR_OPTION_NOT_IMPLEMENTED = 79; // 0x4f

/**
 * Provided in response to a STATUS ENQUIRY message.
 * @apiSince REL
 */

public static final int STATUS_ENQUIRY = 30; // 0x1e

/**
 * The switching equipment is experiencing a period of high traffic.
 * @apiSince REL
 */

public static final int SWITCHING_CONGESTION = 42; // 0x2a

/**
 * The network is not functioning correctly and the condition is not likely to last a long
 * period of time.
 * @apiSince REL
 */

public static final int TEMPORARY_FAILURE = 41; // 0x29

/**
 * The destination cannot be reached because the number, although valid,
 * is not currently assigned.
 * @apiSince REL
 */

public static final int UNOBTAINABLE_NUMBER = 1; // 0x1

/**
 * The called user for the incoming CUG call is not a member of the specified calling user
 * group (CUG).
 * @apiSince REL
 */

public static final int USER_NOT_MEMBER_OF_CUG = 87; // 0x57
}

