/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telecom;

import android.os.Parcel;

/**
 *  Encapsulates the telecom audio state, including the current audio routing, supported audio
 *  routing and mute.
 *  @deprecated - use {@link CallAudioState} instead.
 *  @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class AudioState implements android.os.Parcelable {

/** @apiSince REL */

@Deprecated
public AudioState(boolean muted, int route, int supportedRouteMask) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@Deprecated
public AudioState(android.telecom.AudioState state) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@Deprecated
public AudioState(android.telecom.CallAudioState state) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public static java.lang.String audioRouteToString(int route) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Writes AudioState object into a serializeable Parcel.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeToParcel(android.os.Parcel destination, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if the call is muted, false otherwise.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean isMuted() { throw new RuntimeException("Stub!"); }

/**
 * @return The current audio route being used.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getRoute() { throw new RuntimeException("Stub!"); }

/**
 * @return Bit mask of all routes supported by this call.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getSupportedRouteMask() { throw new RuntimeException("Stub!"); }

/**
 * Responsible for creating AudioState objects for deserialized Parcels.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated @android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telecom.AudioState> CREATOR;
static { CREATOR = null; }

/**
 * Direct the audio stream through Bluetooth.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int ROUTE_BLUETOOTH = 2; // 0x2

/**
 * Direct the audio stream through the device's earpiece.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int ROUTE_EARPIECE = 1; // 0x1

/**
 * Direct the audio stream through the device's speakerphone.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int ROUTE_SPEAKER = 8; // 0x8

/**
 * Direct the audio stream through a wired headset.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int ROUTE_WIRED_HEADSET = 4; // 0x4

/**
 * Direct the audio stream through the device's earpiece or wired headset if one is
 * connected.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int ROUTE_WIRED_OR_EARPIECE = 5; // 0x5
}

