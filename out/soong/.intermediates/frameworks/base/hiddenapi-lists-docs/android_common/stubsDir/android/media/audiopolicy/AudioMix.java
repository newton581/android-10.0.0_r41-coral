/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.audiopolicy;

import android.media.AudioDeviceInfo;
import android.media.AudioFormat;

/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AudioMix {

/**
 * All parameters are guaranteed valid through the Builder.
 */

AudioMix(android.media.audiopolicy.AudioMixingRule rule, android.media.AudioFormat format, int routeFlags, int callbackFlags, int deviceType, java.lang.String deviceAddress) { throw new RuntimeException("Stub!"); }

/**
 * The current mixing state.
 * @return one of {@link #MIX_STATE_DISABLED}, {@link #MIX_STATE_IDLE},
 *          {@link #MIX_STATE_MIXING}.
 * @apiSince REL
 */

public int getMixState() { throw new RuntimeException("Stub!"); }

/** @hide */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @hide */

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * State of a mix before its policy is enabled.
 * @apiSince REL
 */

public static final int MIX_STATE_DISABLED = -1; // 0xffffffff

/**
 * State of a mix when there is no audio to mix.
 * @apiSince REL
 */

public static final int MIX_STATE_IDLE = 0; // 0x0

/**
 * State of a mix that is actively mixing audio.
 * @apiSince REL
 */

public static final int MIX_STATE_MIXING = 1; // 0x1

/**
 * An audio mix behavior where the output of the mix is rerouted back to the framework and
 * is accessible for injection or capture through the {@link AudioTrack} and {@link AudioRecord}
 * APIs.
 * @apiSince REL
 */

public static final int ROUTE_FLAG_LOOP_BACK = 2; // 0x2

/**
 * An audio mix behavior where the output of the mix is sent to the original destination of
 * the audio signal, i.e. an output device for an output mix, or a recording for an input mix.
 * @apiSince REL
 */

public static final int ROUTE_FLAG_RENDER = 1; // 0x1
/**
 * Builder class for {@link AudioMix} objects
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Builder {

/**
 * Construct an instance for the given {@link AudioMixingRule}.
 * @param rule a non-null {@link AudioMixingRule} instance.
 * @throws IllegalArgumentException
 * @apiSince REL
 */

public Builder(android.media.audiopolicy.AudioMixingRule rule) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Sets the {@link AudioFormat} for the mix.
 * @param format a non-null {@link AudioFormat} instance.
 * @return the same Builder instance.
 * @throws IllegalArgumentException
 * @apiSince REL
 */

public android.media.audiopolicy.AudioMix.Builder setFormat(android.media.AudioFormat format) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Sets the routing behavior for the mix. If not set, routing behavior will default to
 * {@link AudioMix#ROUTE_FLAG_LOOP_BACK}.
 * @param routeFlags one of {@link AudioMix#ROUTE_FLAG_LOOP_BACK},
 *     {@link AudioMix#ROUTE_FLAG_RENDER}
 * Value is either <code>0</code> or a combination of {@link android.media.audiopolicy.AudioMix#ROUTE_FLAG_RENDER}, and {@link android.media.audiopolicy.AudioMix#ROUTE_FLAG_LOOP_BACK}
 * @return the same Builder instance.
 * @throws IllegalArgumentException
 * @apiSince REL
 */

public android.media.audiopolicy.AudioMix.Builder setRouteFlags(int routeFlags) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Sets the audio device used for playback. Cannot be used in the context of an audio
 * policy used to inject audio to be recorded, or in a mix whose route flags doesn't
 * specify {@link AudioMix#ROUTE_FLAG_RENDER}.
 * @param device a non-null AudioDeviceInfo describing the audio device to play the output
 *     of this mix.
 * This value must never be {@code null}.
 * @return the same Builder instance
 * @throws IllegalArgumentException
 * @apiSince REL
 */

public android.media.audiopolicy.AudioMix.Builder setDevice(@android.annotation.NonNull android.media.AudioDeviceInfo device) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Combines all of the settings and return a new {@link AudioMix} object.
 * @return a new {@link AudioMix} object
 * @throws IllegalArgumentException if no {@link AudioMixingRule} has been set.
 * @apiSince REL
 */

public android.media.audiopolicy.AudioMix build() throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }
}

}

