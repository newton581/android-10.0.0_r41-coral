/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.resolver;

import android.content.Intent;
import android.service.resolver.ResolverTarget;

/**
 * A service to rank apps according to usage stats of apps, when the system is resolving targets for
 * an Intent.
 *
 * <p>To extend this class, you must declare the service in your manifest file with the
 * {@link android.Manifest.permission#BIND_RESOLVER_RANKER_SERVICE} permission, and include an
 * intent filter with the {@link #SERVICE_INTERFACE} action. For example:</p>
 * <pre>
 *     &lt;service android:name=".MyResolverRankerService"
 *             android:exported="true"
 *             android:priority="100"
 *             android:permission="android.permission.BIND_RESOLVER_RANKER_SERVICE"&gt;
 *         &lt;intent-filter&gt;
 *             &lt;action android:name="android.service.resolver.ResolverRankerService" /&gt;
 *         &lt;/intent-filter&gt;
 *     &lt;/service&gt;
 * </pre>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ResolverRankerService extends android.app.Service {

public ResolverRankerService() { throw new RuntimeException("Stub!"); }

/**
 * Called by the system to retrieve a list of probabilities to rank apps/options. To implement
 * it, set selectProbability of each input {@link ResolverTarget}. The higher the
 * selectProbability is, the more likely the {@link ResolverTarget} will be selected by the
 * user. Override this function to provide prediction results.
 *
 * @param targets a list of {@link ResolverTarget}, for the list of apps to be ranked.
 *
 * @throws Exception when the prediction task fails.
 * @apiSince REL
 */

public void onPredictSharingProbabilities(java.util.List<android.service.resolver.ResolverTarget> targets) { throw new RuntimeException("Stub!"); }

/**
 * Called by the system to train/update a ranking service, after the user makes a selection from
 * the ranked list of apps. Override this function to enable model updates.
 *
 * @param targets a list of {@link ResolverTarget}, for the list of apps to be ranked.
 * @param selectedPosition the position of the selected app in the list.
 *
 * @throws Exception when the training task fails.
 * @apiSince REL
 */

public void onTrainRankingModel(java.util.List<android.service.resolver.ResolverTarget> targets, int selectedPosition) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onDestroy() { throw new RuntimeException("Stub!"); }

/**
 * The permission that a service must require to ensure that only Android system can bind to it.
 * If this permission is not enforced in the AndroidManifest of the service, the system will
 * skip that service.
 * @apiSince REL
 */

public static final java.lang.String BIND_PERMISSION = "android.permission.BIND_RESOLVER_RANKER_SERVICE";

/**
 * The permission that a service must hold. If the service does not hold the permission, the
 * system will skip that service.
 * @apiSince REL
 */

public static final java.lang.String HOLD_PERMISSION = "android.permission.PROVIDE_RESOLVER_RANKER_SERVICE";

/**
 * The Intent action that a service must respond to. Add it to the intent filter of the service
 * in its manifest.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.service.resolver.ResolverRankerService";
}

