
package android.net.wifi;

import android.content.Context;
import android.net.wifi.rtt.WifiRttManager;
import android.Manifest;
import android.net.wifi.rtt.RangingResultCallback;
import android.net.wifi.rtt.RangingRequest;
import android.os.Parcelable;

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class RttManager {

/**
 * Create a new WifiScanner instance.
 * Applications will almost always want to use
 * {@link android.content.Context#getSystemService Context.getSystemService()} to retrieve
 * the standard {@link android.content.Context#WIFI_RTT_SERVICE Context.WIFI_RTT_SERVICE}.
 * @param service the new WifiRttManager service
 *
 * @hide
 */

RttManager(android.content.Context context, android.net.wifi.rtt.WifiRttManager service) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated Use the new {@link android.net.wifi.RttManager#getRttCapabilities()} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public android.net.wifi.RttManager.Capabilities getCapabilities() { throw new RuntimeException("Stub!"); }

/**
 * This method is deprecated. Please use the {@link WifiRttManager} API.
 
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public android.net.wifi.RttManager.RttCapabilities getRttCapabilities() { throw new RuntimeException("Stub!"); }

/**
 * Request to start an RTT ranging
 * <p>
 * This method is deprecated. Please use the
 * {@link WifiRttManager#startRanging(RangingRequest, java.util.concurrent.Executor, RangingResultCallback)}
 * API.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param params  -- RTT request Parameters
 * @param listener -- Call back to inform RTT result
 * @exception throw IllegalArgumentException when params are illegal
 *            throw IllegalStateException when RttCapabilities do not exist
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void startRanging(android.net.wifi.RttManager.RttParams[] params, android.net.wifi.RttManager.RttListener listener) { throw new RuntimeException("Stub!"); }

/**
 * This method is deprecated and performs no function. Please use the {@link WifiRttManager}
 * API.
 
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void stopRanging(android.net.wifi.RttManager.RttListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Enable Wi-Fi RTT responder mode on the device. The enabling result will be delivered via
 * {@code callback}.
 * <p>
 * Note calling this method with the same callback when the responder is already enabled won't
 * change the responder state, a cached {@link ResponderConfig} from the last enabling will be
 * returned through the callback.
 * <p>
 * This method is deprecated and will throw an {@link UnsupportedOperationException}
 * exception. Please use the {@link WifiRttManager} API to perform a Wi-Fi Aware peer-to-peer
 * ranging.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param callback Callback for responder enabling/disabling result.
 * @throws IllegalArgumentException If {@code callback} is null.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void enableResponder(android.net.wifi.RttManager.ResponderCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Disable Wi-Fi RTT responder mode on the device. The {@code callback} needs to be the
 * same one used in {@link #enableResponder(ResponderCallback)}.
 * <p>
 * Calling this method when responder isn't enabled won't have any effect. The callback can be
 * reused for enabling responder after this method is called.
 * <p>
 * This method is deprecated and will throw an {@link UnsupportedOperationException}
 * exception. Please use the {@link WifiRttManager} API to perform a Wi-Fi Aware peer-to-peer
 * ranging.
 *
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param callback The same callback used for enabling responder.
 * @throws IllegalArgumentException If {@code callback} is null.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void disableResponder(android.net.wifi.RttManager.ResponderCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int BASE = 160256; // 0x27200

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int CMD_OP_ABORTED = 160260; // 0x27204

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int CMD_OP_DISABLE_RESPONDER = 160262; // 0x27206

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int CMD_OP_ENABLE_RESPONDER = 160261; // 0x27205

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int CMD_OP_ENALBE_RESPONDER_FAILED = 160264; // 0x27208

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int CMD_OP_ENALBE_RESPONDER_SUCCEEDED = 160263; // 0x27207

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int CMD_OP_FAILED = 160258; // 0x27202

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int CMD_OP_START_RANGING = 160256; // 0x27200

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int CMD_OP_STOP_RANGING = 160257; // 0x27201

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int CMD_OP_SUCCEEDED = 160259; // 0x27203

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final java.lang.String DESCRIPTION_KEY = "android.net.wifi.RttManager.Description";

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PREAMBLE_HT = 2; // 0x2

/**
 * RTT Preamble Support bit mask
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PREAMBLE_LEGACY = 1; // 0x1

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PREAMBLE_VHT = 4; // 0x4

/**
 * Ranging failed because responder role is enabled in STA mode.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int REASON_INITIATOR_NOT_ALLOWED_WHEN_RESPONDER_ON = -6; // 0xfffffffa

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int REASON_INVALID_LISTENER = -3; // 0xfffffffd

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int REASON_INVALID_REQUEST = -4; // 0xfffffffc

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int REASON_NOT_AVAILABLE = -2; // 0xfffffffe

/**
 * Do not have required permission
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int REASON_PERMISSION_DENIED = -5; // 0xfffffffb

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int REASON_UNSPECIFIED = -1; // 0xffffffff

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_BW_10_SUPPORT = 2; // 0x2

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_BW_160_SUPPORT = 32; // 0x20

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_BW_20_SUPPORT = 4; // 0x4

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_BW_40_SUPPORT = 8; // 0x8

/**
 * RTT BW supported bit mask, used as RTT param bandWidth too
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_BW_5_SUPPORT = 1; // 0x1

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_BW_80_SUPPORT = 16; // 0x10

/**@deprecated It is not supported anymore.
 * Use {@link android.net.wifi.RttManager#RTT_BW_10_SUPPORT} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_CHANNEL_WIDTH_10 = 6; // 0x6

/**@deprecated It is not supported anymore.
 * Use {@link android.net.wifi.RttManager#RTT_BW_160_SUPPORT} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_CHANNEL_WIDTH_160 = 3; // 0x3

/**
 * @deprecated It is not supported anymore.
 * Use {@link android.net.wifi.RttManager#RTT_BW_20_SUPPORT} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_CHANNEL_WIDTH_20 = 0; // 0x0

/**
 * @deprecated It is not supported anymore.
 * Use {@link android.net.wifi.RttManager#RTT_BW_40_SUPPORT} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_CHANNEL_WIDTH_40 = 1; // 0x1

/**@deprecated It is not supported anymore.
 * Use {@link android.net.wifi.RttManager#RTT_BW_5_SUPPORT} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_CHANNEL_WIDTH_5 = 5; // 0x5

/**
 * @deprecated It is not supported anymore.
 * Use {@link android.net.wifi.RttManager#RTT_BW_80_SUPPORT} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_CHANNEL_WIDTH_80 = 2; // 0x2

/**
 *@deprecated not supported anymore
 * @apiSince REL
 @deprecatedSince REL
 */

@Deprecated public static final int RTT_CHANNEL_WIDTH_80P80 = 4; // 0x4

/**
 * @deprecated channel info must be specified.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_CHANNEL_WIDTH_UNSPECIFIED = -1; // 0xffffffff

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_PEER_NAN = 5; // 0x5

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_PEER_P2P_CLIENT = 4; // 0x4

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_PEER_P2P_GO = 3; // 0x3

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_PEER_TYPE_AP = 1; // 0x1

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_PEER_TYPE_STA = 2; // 0x2

/**
 * @deprecated It is not supported anymore.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_PEER_TYPE_UNSPECIFIED = 0; // 0x0

/**
 * Request abort fro uncertain reason
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_ABORTED = 8; // 0x8

/**
 * General failure
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAILURE = 1; // 0x1

/**
 * Destination is on a different channel from the RTT Request
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_AP_ON_DIFF_CHANNEL = 6; // 0x6

/**
 * destination is busy now, you can try after a specified time from destination
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_BUSY_TRY_LATER = 12; // 0xc

/**
 * Responder overrides param info, cannot range with new params 2-side RTT only
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_FTM_PARAM_OVERRIDE = 15; // 0xf

/**
 * The T1-T4 or TOD/TOA Timestamp is illegal
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_INVALID_TS = 9; // 0x9

/**
 *
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_NOT_SCHEDULED_YET = 4; // 0x4

/**
 * This type of Ranging is not support by Hardware
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_NO_CAPABILITY = 7; // 0x7

/**
 * Destination does not respond to RTT request
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_NO_RSP = 2; // 0x2

/**
 * 11mc protocol level failed, eg, unrecognized FTMR/FTM frame
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_PROTOCOL = 10; // 0xa

/**
 * RTT request is rejected by the destination. Double side RTT only
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_REJECTED = 3; // 0x3

/**
 * Request can not be scheduled by hardware
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_SCHEDULE = 11; // 0xb

/**
 * Timing measurement timeout
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_FAIL_TM_TIMEOUT = 5; // 0x5

/**
 * Bad Request argument
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_INVALID_REQ = 13; // 0xd

/**
 * Wifi is not enabled
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_NO_WIFI = 14; // 0xe

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_STATUS_SUCCESS = 0; // 0x0

/**
 * @deprecated It is not supported anymore.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_TYPE_11_MC = 4; // 0x4

/**
 * @deprecated It is not supported anymore.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_TYPE_11_V = 2; // 0x2

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_TYPE_ONE_SIDED = 1; // 0x1

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_TYPE_TWO_SIDED = 2; // 0x2

/**
 * @deprecated It is Not supported anymore.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RTT_TYPE_UNSPECIFIED = 0; // 0x0
/**
 * @deprecated Use the new {@link android.net.wifi.RttManager.RttCapabilities} API
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class Capabilities {

@Deprecated
public Capabilities() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int supportedPeerType;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int supportedType;
}

/**
 * pseudo-private class used to parcel arguments
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class ParcelableRttParams implements android.os.Parcelable {

/**
 * @hide
 */

ParcelableRttParams(android.net.wifi.RttManager.RttParams[] params) { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated @android.annotation.NonNull public android.net.wifi.RttManager.RttParams[] mParams;
}

/**
 * pseudo-private class used to parcel results.
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class ParcelableRttResults implements android.os.Parcelable {

/** @apiSince REL */

@Deprecated
public ParcelableRttResults(android.net.wifi.RttManager.RttResult[] results) { throw new RuntimeException("Stub!"); }

/**
 * {@hide}
 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public android.net.wifi.RttManager.RttResult[] mResults;
}

/**
 * Callbacks for responder operations.
 * <p>
 * A {@link ResponderCallback} is the handle to the calling client. {@link RttManager} will keep
 * a reference to the callback for the entire period when responder is enabled. The same
 * callback as used in enabling responder needs to be passed for disabling responder.
 * The client can freely destroy or reuse the callback after {@link RttManager#disableResponder}
 * is called.
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract static class ResponderCallback {

@Deprecated
public ResponderCallback() { throw new RuntimeException("Stub!"); }

/**
 * Callback when responder is enabled.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public abstract void onResponderEnabled(android.net.wifi.RttManager.ResponderConfig config);

/**
 * Callback when enabling responder failed.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public abstract void onResponderEnableFailure(int reason);
}

/**
 * Configuration used for RTT responder mode. The configuration information can be used by a
 * peer device to range the responder.
 *
 * @see ScanResult
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class ResponderConfig implements android.os.Parcelable {

@Deprecated
public ResponderConfig() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Implement {@link Parcelable} interface
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated @android.annotation.NonNull public static final android.os.Parcelable.Creator<android.net.wifi.RttManager.ResponderConfig> CREATOR;
static { CREATOR = null; }

/**
 * Center frequency of the channel where responder is enabled on. Only in use when channel
 * width is at least 40MHz.
 * @see ScanResult#centerFreq0
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int centerFreq0;

/**
 * Center frequency of the second segment when channel width is 80 + 80 MHz.
 * @see ScanResult#centerFreq1
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int centerFreq1;

/**
 * Width of the channel where responder is enabled on.
 * @see ScanResult#channelWidth
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int channelWidth;

/**
 * The primary 20 MHz frequency (in MHz) of the channel where responder is enabled.
 * @see ScanResult#frequency
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int frequency;

/**
 * Wi-Fi mac address used for responder mode.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public java.lang.String macAddress = "";

/**
 * Preamble supported by responder.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int preamble;
}

/**
 * This class describe the RTT capability of the Hardware
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class RttCapabilities implements android.os.Parcelable {

@Deprecated
public RttCapabilities() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int bwSupported;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean lciSupported;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean lcrSupported;

/**
 * Draft 11mc version supported, including major and minor version. e.g, draft 4.3 is 43
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int mcVersion;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean oneSidedRttSupported;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int preambleSupported;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean responderSupported;

/**
 * Whether the secure RTT protocol is supported.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean secureRttSupported;

/**
 * @deprecated It is not supported
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean supportedPeerType;

/**
 * @deprecated It is not supported
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean supportedType;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean twoSided11McRttSupported;
}

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static interface RttListener {

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onSuccess(android.net.wifi.RttManager.RttResult[] results);

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onFailure(int reason, java.lang.String description);

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onAborted();
}

/**
 * specifies parameters for RTT request
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class RttParams {

/** @apiSince REL */

@Deprecated
public RttParams() { throw new RuntimeException("Stub!"); }

/**
 * {@hide}
 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Request LCI information, only available when choose double side RTT measurement
 * need check RttCapabilties first.
 * Default value: false
 *         * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean LCIRequest;

/**
 * Request LCR information, only available when choose double side RTT measurement
 * need check RttCapabilties first.
 * Default value: false
 *         * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean LCRRequest;

/** bandWidth used for RTT measurement.User need verify the highest BW the destination
 * support (from scan result etc) before set this value. Wider channels result usually give
 * better accuracy. However, the frame loss can increase too.
 * should be one of RTT_BW_5_SUPPORT to RTT_BW_160_SUPPORT. However, need check
 * RttCapabilities firstto verify HW support this bandwidth.
 * Default value:RTT_BW_20_SUPPORT
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int bandwidth;

/**
 * mac address of the device being ranged
 * Default value: null
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public java.lang.String bssid;

/**
 * Timeout for each burst, (250 * 2^x) us,
 * Range 1-11 and 15. 15 means no control Default value: 15
 *         * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int burstTimeout;

/**
 * Not used if the AP bandwidth is 20 MHz
 * If the AP use 40, 80 or 160 MHz, this is the center frequency
 * if the AP use 80 + 80 MHz, this is the center frequency of the first segment
 * same as ScanResult.centerFreq0
 * Default value: 0
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int centerFreq0;

/**
 * Only used if the AP bandwidth is 80 + 80 MHz
 * if the AP use 80 + 80 MHz, this is the center frequency of the second segment
 * same as ScanResult.centerFreq1
 * Default value: 0
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int centerFreq1;

/**
 * channel width of the destination AP. Same as ScanResult.channelWidth
 * Default value: 0
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int channelWidth;

/**
 * type of destination device being ranged
 * currently only support RTT_PEER_TYPE_AP
 * Range:RTT_PEER_TYPE_xxxx Default value:RTT_PEER_TYPE_AP
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int deviceType;

/**
 * The primary control channel over which the client is
 * communicating with the AP.Same as ScanResult.frequency
 * Default value: 0
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int frequency;

/**
 * valid only if numberBurst > 1, interval between burst(100ms).
 * Range : 0-31, 0--means no specific
 * Default value: 0
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int interval;

/**
 * number of retries for FTMR frame (control frame) if it fails.
 * Only used by 80211MC double side RTT
 * Range: 0-3  Default Value : 0
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int numRetriesPerFTMR;

/** number of retries for each measurement frame if a sample fails
 *  Only used by single side RTT,
 *  Range 0 - 3 Default value: 0
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int numRetriesPerMeasurementFrame;

/**
 * number of samples to be taken in one burst
 * Range: 1-31
 * Default value: 8
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int numSamplesPerBurst;

/**
 * number of retries if a sample fails
 * @deprecated
 * Use {@link android.net.wifi.RttManager.RttParams#numRetriesPerMeasurementFrame} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int num_retries;

/**
 * number of samples to be taken
 * @deprecated Use the new {@link android.net.wifi.RttManager.RttParams#numSamplesPerBurst}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int num_samples;

/** Number of burst in exp , 2^x. 0 means single shot measurement, range 0-15
 * Currently only single shot is supported
 * Default value: 0
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int numberBurst;

/** preamble used for RTT measurement
 *  Range: PREAMBLE_LEGACY, PREAMBLE_HT, PREAMBLE_VHT
 *  Default value: PREAMBLE_HT
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int preamble;

/**
 * type of RTT measurement method. Need check scan result and RttCapabilities first
 * Range: RTT_TYPE_ONE_SIDED or RTT_TYPE_TWO_SIDED
 * Default value: RTT_TYPE_ONE_SIDED
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int requestType;

/**
 * Whether the secure RTT protocol needs to be used for ranging this peer device.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean secure;
}

/**
 * specifies RTT results
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class RttResult {

@Deprecated
public RttResult() { throw new RuntimeException("Stub!"); }

/**
 * LCI information Element, only available for double side RTT.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public android.net.wifi.RttManager.WifiInformationElement LCI;

/**
 * LCR information Element, only available to double side RTT.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public android.net.wifi.RttManager.WifiInformationElement LCR;

/**
 * mac address of the device being ranged.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public java.lang.String bssid;

/**
 * the duration of this measurement burst, unit ms.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int burstDuration;

/**
 * # of burst for this measurement.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int burstNumber;

/**
 * average distance in cm, computed based on rtt.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int distance;

/**
 * spread (i.e. max - min) distance in cm.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int distanceSpread;

/**
 * standard deviation observed in distance in cm.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int distanceStandardDeviation;

/**
 * average distance in centimeter, computed based on rtt_ns
 * @deprecated use {@link android.net.wifi.RttManager.RttResult#distance} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int distance_cm;

/**
 * standard deviation observed in distance
 * @deprecated
 * Use {@link .android.net.wifi.RttManager.RttResult#distanceStandardDeviation} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int distance_sd_cm;

/**
 * spread (i.e. max - min) distance
 * @deprecated Use {@link android.net.wifi.RttManager.RttResult#distanceSpread} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int distance_spread_cm;

/**
 * Maximum number of frames per burst supported by peer. Two side RTT only
 * Valid only if less than request
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int frameNumberPerBurstPeer;

/**
 * total number of measurement frames attempted in this measurement.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int measurementFrameNumber;

/** RTT measurement method type used, should be one of RTT_TYPE_ONE_SIDED or
 *  RTT_TYPE_TWO_SIDED.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int measurementType;

/**
 * Burst number supported by peer after negotiation, 2side RTT only
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int negotiatedBurstNum;

/**
 * type of the request used
 * @deprecated Use {@link android.net.wifi.RttManager.RttResult#measurementType}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int requestType;

/**
 * only valid when status ==  RTT_STATUS_FAIL_BUSY_TRY_LATER
 * please retry RTT measurement after this duration since peer indicate busy at ths moment
 *  Unit S  Range:1-31
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int retryAfterDuration;

/**
 * average RSSI observed, unit of 0.5 dB.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int rssi;

/**
 *RSSI spread (i.e. max - min), unit of 0.5 dB.
 * @apiSince REL
 @deprecatedSince REL
 */

@Deprecated public int rssiSpread;

/**
 * RSSI spread (i.e. max - min)
 * @deprecated Use {@link android.net.wifi.RttManager.RttResult#rssiSpread} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int rssi_spread;

/**
 * average round trip time in picoseconds.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public long rtt;

/**
 * spread (i.e. max - min) RTT in picoseconds.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public long rttSpread;

/**
 * standard deviation of RTT in picoseconds.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public long rttStandardDeviation;

/**
 * average round trip time in nano second
 * @deprecated  Use {@link android.net.wifi.RttManager.RttResult#rtt} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public long rtt_ns;

/**
 * standard deviation observed in round trip time
 * @deprecated Use {@link android.net.wifi.RttManager.RttResult#rttStandardDeviation} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public long rtt_sd_ns;

/**
 * spread (i.e. max - min) round trip time
 * @deprecated Use {@link android.net.wifi.RttManager.RttResult#rttSpread} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public long rtt_spread_ns;

/**
 * average receiving rate Unit (kbps).
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int rxRate;

/**
 * Whether the secure RTT protocol was used for ranging.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public boolean secure;

/**
 * status of the request
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int status;

/**
 * total successful number of measurement frames in this measurement.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int successMeasurementFrameNumber;

/**
 * timestamp of completion, in microsecond since boot.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public long ts;

/**
 * average transmit rate. Unit (kbps).
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int txRate;

/**
 * average transmit rate
 * @deprecated Use {@link android.net.wifi.RttManager.RttResult#txRate} API.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int tx_rate;
}

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class WifiInformationElement {

@Deprecated
public WifiInformationElement() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public byte[] data;

/**
 * Information Element ID 0xFF means element is invalid.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public byte id;
}

}

