/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.hdmi;


/**
 * HdmiPlaybackClient represents HDMI-CEC logical device of type Playback
 * in the Android system which acts as a playback device such as set-top box.
 *
 * <p>HdmiPlaybackClient provides methods that control, get information from TV/Display device
 * connected through HDMI bus.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class HdmiPlaybackClient extends android.hardware.hdmi.HdmiClient {

HdmiPlaybackClient() { throw new RuntimeException("Stub!"); }

/**
 * Performs the feature 'one touch play' from playback device to turn on display
 * and switch the input.
 *
 * @param callback {@link OneTouchPlayCallback} object to get informed
 *         of the result
 * @apiSince REL
 */

public void oneTouchPlay(android.hardware.hdmi.HdmiPlaybackClient.OneTouchPlayCallback callback) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getDeviceType() { throw new RuntimeException("Stub!"); }

/**
 * Gets the status of display device connected through HDMI bus.
 *
 * @param callback {@link DisplayStatusCallback} object to get informed
 *         of the result
 * @apiSince REL
 */

public void queryDisplayStatus(android.hardware.hdmi.HdmiPlaybackClient.DisplayStatusCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Sends a &lt;Standby&gt; command to TV.
 * @apiSince REL
 */

public void sendStandby() { throw new RuntimeException("Stub!"); }
/**
 * Listener used by the client to get display device status.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface DisplayStatusCallback {

/**
 * Called when display device status is reported.
 *
 * @param status display device status. It should be one of the following values.
 *            <ul>
 *            <li>{@link HdmiControlManager#POWER_STATUS_ON}
 *            <li>{@link HdmiControlManager#POWER_STATUS_STANDBY}
 *            <li>{@link HdmiControlManager#POWER_STATUS_TRANSIENT_TO_ON}
 *            <li>{@link HdmiControlManager#POWER_STATUS_TRANSIENT_TO_STANDBY}
 *            <li>{@link HdmiControlManager#POWER_STATUS_UNKNOWN}
 *            </ul>
 * @apiSince REL
 */

public void onComplete(int status);
}

/**
 * Listener used by the client to get the result of one touch play operation.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OneTouchPlayCallback {

/**
 * Called when the result of the feature one touch play is returned.
 *
 * @param result the result of the operation. {@link HdmiControlManager#RESULT_SUCCESS}
 *         if successful.
 * @apiSince REL
 */

public void onComplete(int result);
}

}

