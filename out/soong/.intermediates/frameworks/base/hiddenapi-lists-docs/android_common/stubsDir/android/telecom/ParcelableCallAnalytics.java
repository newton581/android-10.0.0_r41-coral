/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telecom;


/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ParcelableCallAnalytics implements android.os.Parcelable {

/** @apiSince REL */

public ParcelableCallAnalytics(long startTimeMillis, long callDurationMillis, int callType, boolean isAdditionalCall, boolean isInterrupted, int callTechnologies, int callTerminationCode, boolean isEmergencyCall, java.lang.String connectionService, boolean isCreatedFromExistingConnection, java.util.List<android.telecom.ParcelableCallAnalytics.AnalyticsEvent> analyticsEvents, java.util.List<android.telecom.ParcelableCallAnalytics.EventTiming> eventTimings) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public ParcelableCallAnalytics(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long getStartTimeMillis() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long getCallDurationMillis() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getCallType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isAdditionalCall() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isInterrupted() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getCallTechnologies() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getCallTerminationCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isEmergencyCall() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getConnectionService() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isCreatedFromExistingConnection() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.util.List<android.telecom.ParcelableCallAnalytics.AnalyticsEvent> analyticsEvents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.util.List<android.telecom.ParcelableCallAnalytics.EventTiming> getEventTimings() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int CALLTYPE_INCOMING = 1; // 0x1

/** @apiSince REL */

public static final int CALLTYPE_OUTGOING = 2; // 0x2

/** @apiSince REL */

public static final int CALLTYPE_UNKNOWN = 0; // 0x0

/** @apiSince REL */

public static final int CDMA_PHONE = 1; // 0x1

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telecom.ParcelableCallAnalytics> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public static final int GSM_PHONE = 2; // 0x2

/** @apiSince REL */

public static final int IMS_PHONE = 4; // 0x4

/** @apiSince REL */

public static final long MILLIS_IN_1_SECOND = 1000L; // 0x3e8L

/** @apiSince REL */

public static final long MILLIS_IN_5_MINUTES = 300000L; // 0x493e0L

/** @apiSince REL */

public static final int SIP_PHONE = 8; // 0x8

/** @apiSince REL */

public static final int STILL_CONNECTED = -1; // 0xffffffff

/** @apiSince REL */

public static final int THIRD_PARTY_PHONE = 16; // 0x10
/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class AnalyticsEvent implements android.os.Parcelable {

/** @apiSince REL */

public AnalyticsEvent(int eventName, long timestamp) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getEventName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long getTimeSinceLastEvent() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int AUDIO_ROUTE_BT = 204; // 0xcc

/** @apiSince REL */

public static final int AUDIO_ROUTE_EARPIECE = 205; // 0xcd

/** @apiSince REL */

public static final int AUDIO_ROUTE_HEADSET = 206; // 0xce

/** @apiSince REL */

public static final int AUDIO_ROUTE_SPEAKER = 207; // 0xcf

/** @apiSince REL */

public static final int BIND_CS = 5; // 0x5

/** @apiSince REL */

public static final int BLOCK_CHECK_FINISHED = 105; // 0x69

/** @apiSince REL */

public static final int BLOCK_CHECK_INITIATED = 104; // 0x68

/** @apiSince REL */

public static final int CONFERENCE_WITH = 300; // 0x12c

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telecom.ParcelableCallAnalytics.AnalyticsEvent> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public static final int CS_BOUND = 6; // 0x6

/** @apiSince REL */

public static final int DIRECT_TO_VM_FINISHED = 103; // 0x67

/** @apiSince REL */

public static final int DIRECT_TO_VM_INITIATED = 102; // 0x66

/** @apiSince REL */

public static final int FILTERING_COMPLETED = 107; // 0x6b

/** @apiSince REL */

public static final int FILTERING_INITIATED = 106; // 0x6a

/** @apiSince REL */

public static final int FILTERING_TIMED_OUT = 108; // 0x6c

/** @apiSince REL */

public static final int MUTE = 202; // 0xca

/** @apiSince REL */

public static final int REMOTELY_HELD = 402; // 0x192

/** @apiSince REL */

public static final int REMOTELY_UNHELD = 403; // 0x193

/** @apiSince REL */

public static final int REQUEST_ACCEPT = 7; // 0x7

/** @apiSince REL */

public static final int REQUEST_HOLD = 400; // 0x190

/** @apiSince REL */

public static final int REQUEST_PULL = 500; // 0x1f4

/** @apiSince REL */

public static final int REQUEST_REJECT = 8; // 0x8

/** @apiSince REL */

public static final int REQUEST_UNHOLD = 401; // 0x191

/** @apiSince REL */

public static final int SCREENING_COMPLETED = 101; // 0x65

/** @apiSince REL */

public static final int SCREENING_SENT = 100; // 0x64

/** @apiSince REL */

public static final int SET_ACTIVE = 1; // 0x1

/** @apiSince REL */

public static final int SET_DIALING = 4; // 0x4

/** @apiSince REL */

public static final int SET_DISCONNECTED = 2; // 0x2

/** @apiSince REL */

public static final int SET_HOLD = 404; // 0x194

/** @apiSince REL */

public static final int SET_PARENT = 302; // 0x12e

/** @apiSince REL */

public static final int SET_SELECT_PHONE_ACCOUNT = 0; // 0x0

/** @apiSince REL */

public static final int SILENCE = 201; // 0xc9

/** @apiSince REL */

public static final int SKIP_RINGING = 200; // 0xc8

/** @apiSince REL */

public static final int SPLIT_CONFERENCE = 301; // 0x12d

/** @apiSince REL */

public static final int START_CONNECTION = 3; // 0x3

/** @apiSince REL */

public static final int SWAP = 405; // 0x195

/** @apiSince REL */

public static final int UNMUTE = 203; // 0xcb
}

/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class EventTiming implements android.os.Parcelable {

/** @apiSince REL */

public EventTiming(int name, long time) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long getTime() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int ACCEPT_TIMING = 0; // 0x0

/** @apiSince REL */

public static final int BIND_CS_TIMING = 6; // 0x6

/** @apiSince REL */

public static final int BLOCK_CHECK_FINISHED_TIMING = 9; // 0x9

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telecom.ParcelableCallAnalytics.EventTiming> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public static final int DIRECT_TO_VM_FINISHED_TIMING = 8; // 0x8

/** @apiSince REL */

public static final int DISCONNECT_TIMING = 2; // 0x2

/** @apiSince REL */

public static final int FILTERING_COMPLETED_TIMING = 10; // 0xa

/** @apiSince REL */

public static final int FILTERING_TIMED_OUT_TIMING = 11; // 0xb

/** @apiSince REL */

public static final int HOLD_TIMING = 3; // 0x3

/** @apiSince REL */

public static final int INVALID = 999999; // 0xf423f

/** @apiSince REL */

public static final int OUTGOING_TIME_TO_DIALING_TIMING = 5; // 0x5

/** @apiSince REL */

public static final int REJECT_TIMING = 1; // 0x1

/** @apiSince REL */

public static final int SCREENING_COMPLETED_TIMING = 7; // 0x7

/** @apiSince REL */

public static final int UNHOLD_TIMING = 4; // 0x4
}

}

