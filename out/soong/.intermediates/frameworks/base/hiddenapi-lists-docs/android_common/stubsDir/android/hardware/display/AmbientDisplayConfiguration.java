/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.display;


/**
 * AmbientDisplayConfiguration encapsulates reading access to the configuration of ambient display.
 *
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AmbientDisplayConfiguration {

/** {@hide} */

public AmbientDisplayConfiguration(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Returns if Always-on-Display functionality is enabled on the display for a specified user.
 *
 * {@hide}
 */

public boolean alwaysOnEnabled(int user) { throw new RuntimeException("Stub!"); }

/**
 * Returns if Always-on-Display functionality is available on the display.
 *
 * {@hide}
 */

public boolean alwaysOnAvailable() { throw new RuntimeException("Stub!"); }

/**
 * Returns if Always-on-Display functionality is available on the display for a specified user.
 *
 *  {@hide}
 */

public boolean alwaysOnAvailableForUser(int user) { throw new RuntimeException("Stub!"); }
}

