/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.contentcapture;

import android.content.ComponentName;

/**
 * Represents an activity-level event that is not associated with a session.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ActivityEvent implements android.os.Parcelable {

/** @hide */

ActivityEvent(@android.annotation.NonNull android.content.ComponentName componentName, int type) { throw new RuntimeException("Stub!"); }

/**
 * Gests the {@link ComponentName} of the activity associated with the event.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.content.ComponentName getComponentName() { throw new RuntimeException("Stub!"); }

/**
 * Gets the event type.
 *
 * @return either {@link #TYPE_ACTIVITY_RESUMED}, {@value #TYPE_ACTIVITY_PAUSED},
 * {@value #TYPE_ACTIVITY_STOPPED}, or {@value #TYPE_ACTIVITY_DESTROYED}.
 
 * Value is {@link android.service.contentcapture.ActivityEvent#TYPE_ACTIVITY_RESUMED}, {@link android.service.contentcapture.ActivityEvent#TYPE_ACTIVITY_PAUSED}, {@link android.service.contentcapture.ActivityEvent#TYPE_ACTIVITY_STOPPED}, or {@link android.service.contentcapture.ActivityEvent#TYPE_ACTIVITY_DESTROYED}
 * @apiSince REL
 */

public int getEventType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param parcel This value must never be {@code null}.
 * @apiSince REL
 */

public void writeToParcel(@android.annotation.NonNull android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.contentcapture.ActivityEvent> CREATOR;
static { CREATOR = null; }

/**
 * The activity was destroyed.
 * @apiSince REL
 */

public static final int TYPE_ACTIVITY_DESTROYED = 24; // 0x18

/**
 * The activity paused.
 * @apiSince REL
 */

public static final int TYPE_ACTIVITY_PAUSED = 2; // 0x2

/**
 * The activity resumed.
 * @apiSince REL
 */

public static final int TYPE_ACTIVITY_RESUMED = 1; // 0x1

/**
 * The activity stopped.
 * @apiSince REL
 */

public static final int TYPE_ACTIVITY_STOPPED = 23; // 0x17
}

