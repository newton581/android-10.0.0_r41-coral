/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.permissionpresenterservice;

import android.permission.PermissionControllerService;
import android.content.pm.permission.RuntimePermissionPresentationInfo;
import android.content.Intent;

/**
 * This service presents information regarding runtime permissions that is
 * used for presenting them in the UI. Runtime permissions are presented as
 * a single permission in the UI but may be composed of several individual
 * permissions.
 *
 * @see RuntimePermissionPresentationInfo
 *
 * @hide
 *
 * @deprecated use {@link PermissionControllerService} instead
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public abstract class RuntimePermissionPresenterService extends android.app.Service {

@Deprecated
public RuntimePermissionPresenterService() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public final void attachBaseContext(android.content.Context base) { throw new RuntimeException("Stub!"); }

/**
 * Gets the runtime permissions for an app.
 *
 * @param packageName The package for which to query.
 
 * This value must never be {@code null}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public abstract java.util.List<android.content.pm.permission.RuntimePermissionPresentationInfo> onGetAppPermissions(@android.annotation.NonNull java.lang.String packageName);

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public final android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * The {@link Intent} action that must be declared as handled by a service
 * in its manifest for the system to recognize it as a runtime permission
 * presenter service.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final java.lang.String SERVICE_INTERFACE = "android.permissionpresenterservice.RuntimePermissionPresenterService";
}

