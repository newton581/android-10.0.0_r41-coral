/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill;


/**
 * Holds both a generic and package-specific userData used for
 * <a href="AutofillService.html#FieldClassification">field classification</a>.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CompositeUserData implements android.os.Parcelable {

/**
 * @param genericUserData This value may be {@code null}.
 
 * @param packageUserData This value must never be {@code null}.
 * @apiSince REL
 */

public CompositeUserData(@android.annotation.Nullable android.service.autofill.UserData genericUserData, @android.annotation.NonNull android.service.autofill.UserData packageUserData) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getFieldClassificationAlgorithm() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.Bundle getDefaultFieldClassificationArgs() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param categoryId This value must never be {@code null}.
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getFieldClassificationAlgorithmForCategory(@android.annotation.NonNull java.lang.String categoryId) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.util.ArrayMap<java.lang.String,java.lang.String> getFieldClassificationAlgorithms() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.util.ArrayMap<java.lang.String,android.os.Bundle> getFieldClassificationArgs() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String[] getCategoryIds() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String[] getValues() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.autofill.CompositeUserData> CREATOR;
static { CREATOR = null; }
}

