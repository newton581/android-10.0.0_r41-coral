/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.attention;

import android.content.Intent;

/**
 * Abstract base class for Attention service.
 *
 * <p> An attention service provides attention estimation related features to the system.
 * The system's default AttentionService implementation is configured in
 * {@code config_AttentionComponent}. If this config has no value, a stub is returned.
 *
 * See: {@link com.android.server.attention.AttentionManagerService}.
 *
 * <pre>
 * {@literal
 * <service android:name=".YourAttentionService"
 *          android:permission="android.permission.BIND_ATTENTION_SERVICE">
 * </service>}
 * </pre>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class AttentionService extends android.app.Service {

public AttentionService() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param intent This value must never be {@code null}.
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public final android.os.IBinder onBind(@android.annotation.NonNull android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Checks the user attention and calls into the provided callback.
 *
 * @param callback the callback to return the result to
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onCheckAttention(@android.annotation.NonNull android.service.attention.AttentionService.AttentionCallback callback);

/**
 * Cancels pending work for a given callback.
 *
 * Implementation must call back with a failure code of {@link #ATTENTION_FAILURE_CANCELLED}.
 
 * @param callback This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onCancelAttentionCheck(@android.annotation.NonNull android.service.attention.AttentionService.AttentionCallback callback);

/**
 * Camera permission is not granted.
 * @apiSince REL
 */

public static final int ATTENTION_FAILURE_CAMERA_PERMISSION_ABSENT = 6; // 0x6

/**
 * Request has been cancelled.
 * @apiSince REL
 */

public static final int ATTENTION_FAILURE_CANCELLED = 3; // 0x3

/**
 * Preempted by other client.
 * @apiSince REL
 */

public static final int ATTENTION_FAILURE_PREEMPTED = 4; // 0x4

/**
 * Request timed out.
 * @apiSince REL
 */

public static final int ATTENTION_FAILURE_TIMED_OUT = 5; // 0x5

/**
 * Unknown reasons for failing to determine the attention.
 * @apiSince REL
 */

public static final int ATTENTION_FAILURE_UNKNOWN = 2; // 0x2

/**
 * Attention is absent.
 * @apiSince REL
 */

public static final int ATTENTION_SUCCESS_ABSENT = 0; // 0x0

/**
 * Attention is present.
 * @apiSince REL
 */

public static final int ATTENTION_SUCCESS_PRESENT = 1; // 0x1

/**
 * The {@link Intent} that must be declared as handled by the service. To be supported, the
 * service must also require the {@link android.Manifest.permission#BIND_ATTENTION_SERVICE}
 * permission so that other applications can not abuse it.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.service.attention.AttentionService";
/**
 * Callbacks for AttentionService results.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class AttentionCallback {

AttentionCallback() { throw new RuntimeException("Stub!"); }

/**
 * Signals a success and provides the result code.
 *
 * @param timestamp of when the attention signal was computed; system throttles the requests
 *                  so this is useful to know how fresh the result is.
 
 * @param result Value is {@link android.service.attention.AttentionService#ATTENTION_SUCCESS_ABSENT}, or {@link android.service.attention.AttentionService#ATTENTION_SUCCESS_PRESENT}
 * @apiSince REL
 */

public void onSuccess(int result, long timestamp) { throw new RuntimeException("Stub!"); }

/**
 * Signals a failure and provides the error code.
 * @param error Value is {@link android.service.attention.AttentionService#ATTENTION_FAILURE_UNKNOWN}, {@link android.service.attention.AttentionService#ATTENTION_FAILURE_CANCELLED}, {@link android.service.attention.AttentionService#ATTENTION_FAILURE_PREEMPTED}, {@link android.service.attention.AttentionService#ATTENTION_FAILURE_TIMED_OUT}, or {@link android.service.attention.AttentionService#ATTENTION_FAILURE_CAMERA_PERMISSION_ABSENT}
 * @apiSince REL
 */

public void onFailure(int error) { throw new RuntimeException("Stub!"); }
}

}

