/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.soundtrigger;

import android.os.Bundle;
import java.util.UUID;

/**
 * A service that allows interaction with the actual sound trigger detection on the system.
 *
 * <p> Sound trigger detection refers to detectors that match generic sound patterns that are
 * not voice-based. The voice-based recognition models should utilize the {@link
 * android.service.voice.VoiceInteractionService} instead. Access to this class needs to be
 * protected by the {@value android.Manifest.permission.BIND_SOUND_TRIGGER_DETECTION_SERVICE}
 * permission granted only to the system.
 *
 * <p>This service has to be explicitly started by an app, the system does not scan for and start
 * these services.
 *
 * <p>If an operation ({@link #onGenericRecognitionEvent}, {@link #onError},
 * {@link #onRecognitionPaused}, {@link #onRecognitionResumed}) is triggered the service is
 * considered as running in the foreground. Once the operation is processed the service should call
 * {@link #operationFinished(UUID, int)}. If this does not happen in
 * {@link SoundTriggerManager#getDetectionServiceOperationsTimeout()} milliseconds
 * {@link #onStopOperation(UUID, Bundle, int)} is called and the service is unbound.
 *
 * <p>The total amount of operations per day might be limited.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class SoundTriggerDetectionService extends android.app.Service {

public SoundTriggerDetectionService() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

protected final void attachBaseContext(android.content.Context base) { throw new RuntimeException("Stub!"); }

/**
 * The system has connected to this service for the recognition registered for the model
 * {@code uuid}.
 *
 * <p> This is called before any operations are delivered.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param uuid   The {@code uuid} of the model the recognitions is registered for
 * This value must never be {@code null}.
 * @param params The {@code params} passed when the recognition was started
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void onConnected(@android.annotation.NonNull java.util.UUID uuid, @android.annotation.Nullable android.os.Bundle params) { throw new RuntimeException("Stub!"); }

/**
 * The system has disconnected from this service for the recognition registered for the model
 * {@code uuid}.
 *
 * <p>Once this is called {@link #operationFinished} cannot be called anymore for
 * {@code uuid}.
 *
 * <p> {@link #onConnected(UUID, Bundle)} is called before any further operations are delivered.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param uuid   The {@code uuid} of the model the recognitions is registered for
 * This value must never be {@code null}.
 * @param params The {@code params} passed when the recognition was started
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void onDisconnected(@android.annotation.NonNull java.util.UUID uuid, @android.annotation.Nullable android.os.Bundle params) { throw new RuntimeException("Stub!"); }

/**
 * A new generic sound trigger event has been detected.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param uuid   The {@code uuid} of the model the recognition is registered for
 * This value must never be {@code null}.
 * @param params The {@code params} passed when the recognition was started
 * This value may be {@code null}.
 * @param opId The id of this operation. Once the operation is done, this service needs to call
 *             {@link #operationFinished(UUID, int)}
 * @param event The event that has been detected
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onGenericRecognitionEvent(@android.annotation.NonNull java.util.UUID uuid, @android.annotation.Nullable android.os.Bundle params, int opId, @android.annotation.NonNull android.hardware.soundtrigger.SoundTrigger.RecognitionEvent event) { throw new RuntimeException("Stub!"); }

/**
 * A error has been detected.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param uuid   The {@code uuid} of the model the recognition is registered for
 * This value must never be {@code null}.
 * @param params The {@code params} passed when the recognition was started
 * This value may be {@code null}.
 * @param opId The id of this operation. Once the operation is done, this service needs to call
 *             {@link #operationFinished(UUID, int)}
 * @param status The error code detected
 * @apiSince REL
 */

public void onError(@android.annotation.NonNull java.util.UUID uuid, @android.annotation.Nullable android.os.Bundle params, int opId, int status) { throw new RuntimeException("Stub!"); }

/**
 * An operation took too long and should be stopped.
 *
 * <br>
 * This method must be called from the
 * {@linkplain android.os.Looper#getMainLooper() Looper#getMainLooper()} of your app.
 * @param uuid   The {@code uuid} of the model the recognition is registered for
 * This value must never be {@code null}.
 * @param params The {@code params} passed when the recognition was started
 * This value may be {@code null}.
 * @param opId The id of the operation that took too long
 * @apiSince REL
 */

public abstract void onStopOperation(@android.annotation.NonNull java.util.UUID uuid, @android.annotation.Nullable android.os.Bundle params, int opId);

/**
 * Tell that the system that an operation has been fully processed.
 *
 * @param uuid The {@code uuid} of the model the recognition is registered for
 * This value may be {@code null}.
 * @param opId The id of the operation that is processed
 * @apiSince REL
 */

public final void operationFinished(@android.annotation.Nullable java.util.UUID uuid, int opId) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public final android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * If you override this method you <em>must</em> call through to the
 * superclass implementation.
 * @apiSince REL
 */

public boolean onUnbind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }
}

