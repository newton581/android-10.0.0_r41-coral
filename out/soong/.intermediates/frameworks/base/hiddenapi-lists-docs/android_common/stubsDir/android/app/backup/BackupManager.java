/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.backup;

import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.os.UserHandle;

/**
 * The interface through which an application interacts with the Android backup service to
 * request backup and restore operations.
 * Applications instantiate it using the constructor and issue calls through that instance.
 * <p>
 * When an application has made changes to data which should be backed up, a
 * call to {@link #dataChanged()} will notify the backup service. The system
 * will then schedule a backup operation to occur in the near future. Repeated
 * calls to {@link #dataChanged()} have no further effect until the backup
 * operation actually occurs.
 * <p>
 * A backup or restore operation for your application begins when the system launches the
 * {@link android.app.backup.BackupAgent} subclass you've declared in your manifest. See the
 * documentation for {@link android.app.backup.BackupAgent} for a detailed description
 * of how the operation then proceeds.
 * <p>
 * Several attributes affecting the operation of the backup and restore mechanism
 * can be set on the <code>
 * <a href="{@docRoot}guide/topics/manifest/application-element.html">&lt;application&gt;</a></code>
 * tag in your application's AndroidManifest.xml file.
 *
 * <div class="special reference">
 * <h3>Developer Guides</h3>
 * <p>For more information about using BackupManager, read the
 * <a href="{@docRoot}guide/topics/data/backup.html">Data Backup</a> developer guide.</p></div>
 *
 * @attr ref android.R.styleable#AndroidManifestApplication_allowBackup
 * @attr ref android.R.styleable#AndroidManifestApplication_backupAgent
 * @attr ref android.R.styleable#AndroidManifestApplication_killAfterRestore
 * @attr ref android.R.styleable#AndroidManifestApplication_restoreAnyVersion
 * @apiSince 8
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class BackupManager {

/**
 * Constructs a BackupManager object through which the application can
 * communicate with the Android backup system.
 *
 * @param context The {@link android.content.Context} that was provided when
 *                one of your application's {@link android.app.Activity Activities}
 *                was created.
 * @apiSince 8
 */

public BackupManager(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Notifies the Android backup system that your application wishes to back up
 * new changes to its data.  A backup operation using your application's
 * {@link android.app.backup.BackupAgent} subclass will be scheduled when you
 * call this method.
 * @apiSince 8
 */

public void dataChanged() { throw new RuntimeException("Stub!"); }

/**
 * Convenience method for callers who need to indicate that some other package
 * needs a backup pass.  This can be useful in the case of groups of packages
 * that share a uid.
 * <p>
 * This method requires that the application hold the "android.permission.BACKUP"
 * permission if the package named in the argument does not run under the same uid
 * as the caller.
 *
 * @param packageName The package name identifying the application to back up.
 * @apiSince 8
 */

public static void dataChanged(java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated Applications shouldn't request a restore operation using this method. In Android
 * P and later, this method is a no-op.
 *
 * <p>Restore the calling application from backup. The data will be restored from the
 * current backup dataset if the application has stored data there, or from
 * the dataset used during the last full device setup operation if the current
 * backup dataset has no matching data.  If no backup data exists for this application
 * in either source, a non-zero value is returned.
 *
 * <p>If this method returns zero (meaning success), the OS attempts to retrieve a backed-up
 * dataset from the remote transport, instantiate the application's backup agent, and pass the
 * dataset to the agent's
 * {@link android.app.backup.BackupAgent#onRestore(BackupDataInput, int, android.os.ParcelFileDescriptor) onRestore()}
 * method.
 *
 * <p class="caution">Unlike other restore operations, this method doesn't terminate the
 * application after the restore. The application continues running to receive the
 * {@link RestoreObserver} callbacks on the {@code observer} argument. Full backups use an
 * {@link android.app.Application Application} base class while key-value backups use the
 * application subclass declared in the AndroidManifest.xml {@code <application>} tag.
 *
 * @param observer The {@link RestoreObserver} to receive callbacks during the restore
 * operation. This must not be null.
 *
 * @return Zero on success; nonzero on error.
 * @apiSince 8
 * @deprecatedSince 28
 */

@Deprecated
public int requestRestore(android.app.backup.RestoreObserver observer) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated Since Android P app can no longer request restoring of its backup.
 *
 * <p>Restore the calling application from backup.  The data will be restored from the
 * current backup dataset if the application has stored data there, or from
 * the dataset used during the last full device setup operation if the current
 * backup dataset has no matching data.  If no backup data exists for this application
 * in either source, a nonzero value will be returned.
 *
 * <p>If this method returns zero (meaning success), the OS will attempt to retrieve
 * a backed-up dataset from the remote transport, instantiate the application's
 * backup agent, and pass the dataset to the agent's
 * {@link android.app.backup.BackupAgent#onRestore(BackupDataInput, int, android.os.ParcelFileDescriptor) onRestore()}
 * method.
 *
 * @param observer The {@link RestoreObserver} to receive callbacks during the restore
 * operation. This must not be null.
 *
 * @param monitor the {@link BackupManagerMonitor} to receive callbacks during the restore
 * operation.
 *
 * @return Zero on success; nonzero on error.
 *
 * @hide
 */

@Deprecated
public int requestRestore(android.app.backup.RestoreObserver observer, android.app.backup.BackupManagerMonitor monitor) { throw new RuntimeException("Stub!"); }

/**
 * Begin the process of restoring data from backup.  See the
 * {@link android.app.backup.RestoreSession} class for documentation on that process.
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @hide
 */

public android.app.backup.RestoreSession beginRestoreSession() { throw new RuntimeException("Stub!"); }

/**
 * Enable/disable the backup service entirely.  When disabled, no backup
 * or restore operations will take place.  Data-changed notifications will
 * still be observed and collected, however, so that changes made while the
 * mechanism was disabled will still be backed up properly if it is enabled
 * at some point in the future.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @hide
 */

public void setBackupEnabled(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Report whether the backup mechanism is currently enabled.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @hide
 */

public boolean isBackupEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Report whether the backup mechanism is currently active.
 * When it is inactive, the device will not perform any backup operations, nor will it
 * deliver data for restore, although clients can still safely call BackupManager methods.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @hide
 */

public boolean isBackupServiceActive(android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Enable/disable data restore at application install time.  When enabled, app
 * installation will include an attempt to fetch the app's historical data from
 * the archival restore dataset (if any).  When disabled, no such attempt will
 * be made.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @hide
 */

public void setAutoRestore(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Identify the currently selected transport.
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @return The name of the currently active backup transport.  In case of
 *   failure or if no transport is currently active, this method returns {@code null}.
 *
 * @hide
 */

public java.lang.String getCurrentTransport() { throw new RuntimeException("Stub!"); }

/**
 * Returns the {@link ComponentName} of the host service of the selected transport or {@code
 * null} if no transport selected or if the transport selected is not registered.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @hide
 */

@android.annotation.Nullable
public android.content.ComponentName getCurrentTransportComponent() { throw new RuntimeException("Stub!"); }

/**
 * Request a list of all available backup transports' names.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @hide
 */

public java.lang.String[] listAllTransports() { throw new RuntimeException("Stub!"); }

/**
 * Update the attributes of the transport identified by {@code transportComponent}. If the
 * specified transport has not been bound at least once (for registration), this call will be
 * ignored. Only the host process of the transport can change its description, otherwise a
 * {@link SecurityException} will be thrown.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param transportComponent The identity of the transport being described.
 * This value must never be {@code null}.
 * @param name A {@link String} with the new name for the transport. This is NOT for
 *     identification. MUST NOT be {@code null}.
 * This value must never be {@code null}.
 * @param configurationIntent An {@link Intent} that can be passed to {@link
 *     Context#startActivity} in order to launch the transport's configuration UI. It may be
 *     {@code null} if the transport does not offer any user-facing configuration UI.
 * This value may be {@code null}.
 * @param currentDestinationString A {@link String} describing the destination to which the
 *     transport is currently sending data. MUST NOT be {@code null}.
 * This value must never be {@code null}.
 * @param dataManagementIntent An {@link Intent} that can be passed to {@link
 *     Context#startActivity} in order to launch the transport's data-management UI. It may be
 *     {@code null} if the transport does not offer any user-facing data management UI.
 * This value may be {@code null}.
 * @param dataManagementLabel A {@link String} to be used as the label for the transport's data
 *     management affordance. This MUST be {@code null} when dataManagementIntent is {@code
 *     null} and MUST NOT be {@code null} when dataManagementIntent is not {@code null}.
 * This value may be {@code null}.
 * @throws SecurityException If the UID of the calling process differs from the package UID of
 *     {@code transportComponent} or if the caller does NOT have BACKUP permission.
 * @deprecated Since Android Q, please use the variant {@link
 *     #updateTransportAttributes(ComponentName, String, Intent, String, Intent, CharSequence)}
 *     instead.
 * @hide
 */

@Deprecated
public void updateTransportAttributes(@android.annotation.NonNull android.content.ComponentName transportComponent, @android.annotation.NonNull java.lang.String name, @android.annotation.Nullable android.content.Intent configurationIntent, @android.annotation.NonNull java.lang.String currentDestinationString, @android.annotation.Nullable android.content.Intent dataManagementIntent, @android.annotation.Nullable java.lang.String dataManagementLabel) { throw new RuntimeException("Stub!"); }

/**
 * Update the attributes of the transport identified by {@code transportComponent}. If the
 * specified transport has not been bound at least once (for registration), this call will be
 * ignored. Only the host process of the transport can change its description, otherwise a
 * {@link SecurityException} will be thrown.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param transportComponent The identity of the transport being described.
 * This value must never be {@code null}.
 * @param name A {@link String} with the new name for the transport. This is NOT for
 *     identification. MUST NOT be {@code null}.
 * This value must never be {@code null}.
 * @param configurationIntent An {@link Intent} that can be passed to {@link
 *     Context#startActivity} in order to launch the transport's configuration UI. It may be
 *     {@code null} if the transport does not offer any user-facing configuration UI.
 * This value may be {@code null}.
 * @param currentDestinationString A {@link String} describing the destination to which the
 *     transport is currently sending data. MUST NOT be {@code null}.
 * This value must never be {@code null}.
 * @param dataManagementIntent An {@link Intent} that can be passed to {@link
 *     Context#startActivity} in order to launch the transport's data-management UI. It may be
 *     {@code null} if the transport does not offer any user-facing data management UI.
 * This value may be {@code null}.
 * @param dataManagementLabel A {@link CharSequence} to be used as the label for the transport's
 *     data management affordance. This MUST be {@code null} when dataManagementIntent is {@code
 *     null} and MUST NOT be {@code null} when dataManagementIntent is not {@code null}.
 * This value may be {@code null}.
 * @throws SecurityException If the UID of the calling process differs from the package UID of
 *     {@code transportComponent} or if the caller does NOT have BACKUP permission.
 * @hide
 */

public void updateTransportAttributes(@android.annotation.NonNull android.content.ComponentName transportComponent, @android.annotation.NonNull java.lang.String name, @android.annotation.Nullable android.content.Intent configurationIntent, @android.annotation.NonNull java.lang.String currentDestinationString, @android.annotation.Nullable android.content.Intent dataManagementIntent, @android.annotation.Nullable java.lang.CharSequence dataManagementLabel) { throw new RuntimeException("Stub!"); }

/**
 * Specify the current backup transport.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param transport The name of the transport to select.  This should be one
 *   of the names returned by {@link #listAllTransports()}. This is the String returned by
 *   {@link BackupTransport#name()} for the particular transport.
 * @return The name of the previously selected transport.  If the given transport
 *   name is not one of the currently available transports, no change is made to
 *   the current transport setting and the method returns null.
 *
 * @hide
 */

@Deprecated
public java.lang.String selectBackupTransport(java.lang.String transport) { throw new RuntimeException("Stub!"); }

/**
 * Specify the current backup transport and get notified when the transport is ready to be used.
 * This method is async because BackupManager might need to bind to the specified transport
 * which is in a separate process.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param transport ComponentName of the service hosting the transport. This is different from
 *                  the transport's name that is returned by {@link BackupTransport#name()}.
 * @param listener A listener object to get a callback on the transport being selected.
 *
 * @hide
 */

public void selectBackupTransport(android.content.ComponentName transport, android.app.backup.SelectBackupTransportCallback listener) { throw new RuntimeException("Stub!"); }

/**
 * Schedule an immediate backup attempt for all pending key/value updates.  This
 * is primarily intended for transports to use when they detect a suitable
 * opportunity for doing a backup pass.  If there are no pending updates to
 * be sent, no action will be taken.  Even if some updates are pending, the
 * transport will still be asked to confirm via the usual requestBackupTime()
 * method.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @hide
 */

public void backupNow() { throw new RuntimeException("Stub!"); }

/**
 * Ask the framework which dataset, if any, the given package's data would be
 * restored from if we were to install it right now.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param packageName The name of the package whose most-suitable dataset we
 *     wish to look up
 * @return The dataset token from which a restore should be attempted, or zero if
 *     no suitable data is available.
 *
 * @hide
 */

public long getAvailableRestoreToken(java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Ask the framework whether this app is eligible for backup.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param packageName The name of the package.
 * @return Whether this app is eligible for backup.
 *
 * @hide
 */

public boolean isAppEligibleForBackup(java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Request an immediate backup, providing an observer to which results of the backup operation
 * will be published. The Android backup system will decide for each package whether it will
 * be full app data backup or key/value-pair-based backup.
 *
 * <p>If this method returns {@link BackupManager#SUCCESS}, the OS will attempt to backup all
 * provided packages using the remote transport.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param packages List of package names to backup.
 * @param observer The {@link BackupObserver} to receive callbacks during the backup
 * operation. Could be {@code null}.
 * @return {@link BackupManager#SUCCESS} on success; nonzero on error.
 * @exception  IllegalArgumentException on null or empty {@code packages} param.
 *
 * @hide
 */

public int requestBackup(java.lang.String[] packages, android.app.backup.BackupObserver observer) { throw new RuntimeException("Stub!"); }

/**
 * Request an immediate backup, providing an observer to which results of the backup operation
 * will be published. The Android backup system will decide for each package whether it will
 * be full app data backup or key/value-pair-based backup.
 *
 * <p>If this method returns {@link BackupManager#SUCCESS}, the OS will attempt to backup all
 * provided packages using the remote transport.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param packages List of package names to backup.
 * @param observer The {@link BackupObserver} to receive callbacks during the backup
 *                 operation. Could be {@code null}.
 * @param monitor  The {@link BackupManagerMonitorWrapper} to receive callbacks of important
 *                 events during the backup operation. Could be {@code null}.
 * @param flags    {@link #FLAG_NON_INCREMENTAL_BACKUP}.
 * @return {@link BackupManager#SUCCESS} on success; nonzero on error.
 * @throws IllegalArgumentException on null or empty {@code packages} param.
 * @hide
 */

public int requestBackup(java.lang.String[] packages, android.app.backup.BackupObserver observer, android.app.backup.BackupManagerMonitor monitor, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Cancel all running backups. After this call returns, no currently running backups will
 * interact with the selected transport.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @hide
 */

public void cancelBackups() { throw new RuntimeException("Stub!"); }

/**
 * Returns a {@link UserHandle} for the user that has {@code ancestralSerialNumber} as the
 * serial number of the its ancestral work profile or {@code null} if there is none.
 *
 * <p> The ancestral serial number will have a corresponding {@link UserHandle} if the device
 * has a work profile that was restored from another work profile with serial number
 * {@code ancestralSerialNumber}.
 *
 * @see UserManager#getSerialNumberForUser(UserHandle)
 * @apiSince 29
 */

@android.annotation.Nullable
public android.os.UserHandle getUserForAncestralSerialNumber(long ancestralSerialNumber) { throw new RuntimeException("Stub!"); }

/**
 * Sets the ancestral work profile for the calling user.
 *
 * <p> The ancestral work profile corresponds to the profile that was used to restore to the
 * callers profile.
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @hide
 */

public void setAncestralSerialNumber(long ancestralSerialNumber) { throw new RuntimeException("Stub!"); }

/**
 * Returns an {@link Intent} for the specified transport's configuration UI.
 * This value is set by {@link #updateTransportAttributes(ComponentName, String, Intent, String,
 * Intent, CharSequence)}.
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param transportName The name of the registered transport.
 * @hide
 */

public android.content.Intent getConfigurationIntent(java.lang.String transportName) { throw new RuntimeException("Stub!"); }

/**
 * Returns a {@link String} describing where the specified transport is sending data.
 * This value is set by {@link #updateTransportAttributes(ComponentName, String, Intent, String,
 * Intent, CharSequence)}.
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param transportName The name of the registered transport.
 * @hide
 */

public java.lang.String getDestinationString(java.lang.String transportName) { throw new RuntimeException("Stub!"); }

/**
 * Returns an {@link Intent} for the specified transport's data management UI.
 * This value is set by {@link #updateTransportAttributes(ComponentName, String, Intent, String,
 * Intent, CharSequence)}.
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param transportName The name of the registered transport.
 * @hide
 */

public android.content.Intent getDataManagementIntent(java.lang.String transportName) { throw new RuntimeException("Stub!"); }

/**
 * Returns a {@link String} describing what the specified transport's data management intent is
 * used for. This value is set by {@link #updateTransportAttributes(ComponentName, String,
 * Intent, String, Intent, CharSequence)}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param transportName The name of the registered transport.
 * This value must never be {@code null}.
 * @deprecated Since Android Q, please use the variant {@link
 *     #getDataManagementIntentLabel(String)} instead.
 * @hide
 
 * @return This value may be {@code null}.
 */

@Deprecated
@android.annotation.Nullable
public java.lang.String getDataManagementLabel(@android.annotation.NonNull java.lang.String transportName) { throw new RuntimeException("Stub!"); }

/**
 * Returns a {@link CharSequence} describing what the specified transport's data management
 * intent is used for. This value is set by {@link #updateTransportAttributes(ComponentName,
 * String, Intent, String, Intent, CharSequence)}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#BACKUP}
 * @param transportName The name of the registered transport.
 * This value must never be {@code null}.
 * @hide
 
 * @return This value may be {@code null}.
 */

@android.annotation.Nullable
public java.lang.CharSequence getDataManagementIntentLabel(@android.annotation.NonNull java.lang.String transportName) { throw new RuntimeException("Stub!"); }

/**
 * The {@link BackupAgent} for the requested package failed for some reason
 * and didn't provide appropriate backup data.
 *
 * @hide
 */

public static final int ERROR_AGENT_FAILURE = -1003; // 0xfffffc15

/**
 * The backup operation was cancelled.
 *
 * @hide
 */

public static final int ERROR_BACKUP_CANCELLED = -2003; // 0xfffff82d

/**
 * Indicates that backup is either not enabled at all or
 * backup for the package was rejected by backup service
 * or backup transport,
 *
 * @hide
 */

public static final int ERROR_BACKUP_NOT_ALLOWED = -2001; // 0xfffff82f

/**
 * The requested app is not installed on the device.
 *
 * @hide
 */

public static final int ERROR_PACKAGE_NOT_FOUND = -2002; // 0xfffff82e

/**
 * The transport for some reason was not in a good state and
 * aborted the entire backup request. This is a transient
 * failure and should not be retried immediately.
 *
 * @hide
 */

public static final int ERROR_TRANSPORT_ABORTED = -1000; // 0xfffffc18

/**
 * This error code is passed to {@link SelectBackupTransportCallback#onFailure(int)} if the
 * requested transport is not a valid BackupTransport.
 *
 * @hide
 */

public static final int ERROR_TRANSPORT_INVALID = -2; // 0xfffffffe

/**
 * Returned when the transport was unable to process the
 * backup request for a given package, for example if the
 * transport hit a transient network failure. The remaining
 * packages provided to {@link #requestBackup(String[], BackupObserver)}
 * will still be attempted.
 *
 * @hide
 */

public static final int ERROR_TRANSPORT_PACKAGE_REJECTED = -1002; // 0xfffffc16

/**
 * Returned when the transport reject the attempt to backup because
 * backup data size exceeded current quota limit for this package.
 *
 * @hide
 */

public static final int ERROR_TRANSPORT_QUOTA_EXCEEDED = -1005; // 0xfffffc13

/**
 * This error code is passed to {@link SelectBackupTransportCallback#onFailure(int)}
 * if the requested transport is unavailable.
 *
 * @hide
 */

public static final int ERROR_TRANSPORT_UNAVAILABLE = -1; // 0xffffffff

/**
 * If this flag is passed to {@link #requestBackup(String[], BackupObserver, int)},
 * BackupManager will pass a blank old state to BackupAgents of requested packages.
 *
 * @hide
 */

public static final int FLAG_NON_INCREMENTAL_BACKUP = 1; // 0x1

/**
 * Use with {@link #requestBackup} to force backup of
 * package meta data. Typically you do not need to explicitly request this be backed up as it is
 * handled internally by the BackupManager. If you are requesting backups with
 * FLAG_NON_INCREMENTAL, this package won't automatically be backed up and you have to
 * explicitly request for its backup.
 *
 * @hide
 */

public static final java.lang.String PACKAGE_MANAGER_SENTINEL = "@pm@";

/**
 * Indicates that backup succeeded.
 *
 * @hide
 */

public static final int SUCCESS = 0; // 0x0
}

