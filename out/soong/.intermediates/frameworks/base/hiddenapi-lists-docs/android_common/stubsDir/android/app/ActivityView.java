/**
 * Copyright (c) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app;

import android.content.Intent;
import android.os.UserHandle;
import android.view.SurfaceView;
import android.view.KeyEvent;
import android.hardware.display.VirtualDisplay;
import android.view.SurfaceControl;

/**
 * Activity container that allows launching activities into itself.
 * <p>Activity launching into this container is restricted by the same rules that apply to launching
 * on VirtualDisplays.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ActivityView extends android.view.ViewGroup {

/** @apiSince REL */

public ActivityView(android.content.Context context) { super((android.content.Context)null); throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public ActivityView(android.content.Context context, android.util.AttributeSet attrs) { super((android.content.Context)null); throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public ActivityView(android.content.Context context, android.util.AttributeSet attrs, int defStyle) { super((android.content.Context)null); throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public ActivityView(android.content.Context context, android.util.AttributeSet attrs, int defStyle, boolean singleTaskInstance) { super((android.content.Context)null); throw new RuntimeException("Stub!"); }

/**
 * Set the callback to be notified about state changes.
 * <p>This class must finish initializing before {@link #startActivity(Intent)} can be called.
 * <p>Note: If the instance was ready prior to this call being made, then
 * {@link StateCallback#onActivityViewReady(ActivityView)} will be called from within
 * this method call.
 *
 * @param callback The callback to report events to.
 *
 * @see StateCallback
 * @see #startActivity(Intent)
 * @apiSince REL
 */

public void setCallback(android.app.ActivityView.StateCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Launch a new activity into this container.
 * <p>Activity resolved by the provided {@link Intent} must have
 * {@link android.R.attr#resizeableActivity} attribute set to {@code true} in order to be
 * launched here. Also, if activity is not owned by the owner of this container, it must allow
 * embedding and the caller must have permission to embed.
 * <p>Note: This class must finish initializing and
 * {@link StateCallback#onActivityViewReady(ActivityView)} callback must be triggered before
 * this method can be called.
 *
 * @param intent Intent used to launch an activity.
 *
 * This value must never be {@code null}.
 * @see StateCallback
 * @see #startActivity(PendingIntent)
 * @apiSince REL
 */

public void startActivity(@android.annotation.NonNull android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Launch a new activity into this container.
 * <p>Activity resolved by the provided {@link Intent} must have
 * {@link android.R.attr#resizeableActivity} attribute set to {@code true} in order to be
 * launched here. Also, if activity is not owned by the owner of this container, it must allow
 * embedding and the caller must have permission to embed.
 * <p>Note: This class must finish initializing and
 * {@link StateCallback#onActivityViewReady(ActivityView)} callback must be triggered before
 * this method can be called.
 *
 * @param intent Intent used to launch an activity.
 * This value must never be {@code null}.
 * @param user The UserHandle of the user to start this activity for.
 *
 *
 * @see StateCallback
 * @see #startActivity(PendingIntent)
 * @apiSince REL
 */

public void startActivity(@android.annotation.NonNull android.content.Intent intent, android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Launch a new activity into this container.
 * <p>Activity resolved by the provided {@link PendingIntent} must have
 * {@link android.R.attr#resizeableActivity} attribute set to {@code true} in order to be
 * launched here. Also, if activity is not owned by the owner of this container, it must allow
 * embedding and the caller must have permission to embed.
 * <p>Note: This class must finish initializing and
 * {@link StateCallback#onActivityViewReady(ActivityView)} callback must be triggered before
 * this method can be called.
 *
 * @param pendingIntent Intent used to launch an activity.
 *
 * This value must never be {@code null}.
 * @see StateCallback
 * @see #startActivity(Intent)
 * @apiSince REL
 */

public void startActivity(@android.annotation.NonNull android.app.PendingIntent pendingIntent) { throw new RuntimeException("Stub!"); }

/**
 * Launch a new activity into this container.
 * <p>Activity resolved by the provided {@link PendingIntent} must have
 * {@link android.R.attr#resizeableActivity} attribute set to {@code true} in order to be
 * launched here. Also, if activity is not owned by the owner of this container, it must allow
 * embedding and the caller must have permission to embed.
 * <p>Note: This class must finish initializing and
 * {@link StateCallback#onActivityViewReady(ActivityView)} callback must be triggered before
 * this method can be called.
 *
 * @param pendingIntent Intent used to launch an activity.
 * This value must never be {@code null}.
 * @param options options for the activity
 *
 * This value must never be {@code null}.
 * @see StateCallback
 * @see #startActivity(Intent)
 * @apiSince REL
 */

public void startActivity(@android.annotation.NonNull android.app.PendingIntent pendingIntent, @android.annotation.NonNull android.app.ActivityOptions options) { throw new RuntimeException("Stub!"); }

/**
 * Release this container. Activity launching will no longer be permitted.
 * <p>Note: Calling this method is allowed after
 * {@link StateCallback#onActivityViewReady(ActivityView)} callback was triggered and before
 * {@link StateCallback#onActivityViewDestroyed(ActivityView)}.
 *
 * @see StateCallback
 * @apiSince REL
 */

public void release() { throw new RuntimeException("Stub!"); }

/**
 * Triggers an update of {@link ActivityView}'s location in window to properly set tap exclude
 * regions and avoid focus switches by touches on this view.
 * @apiSince REL
 */

public void onLocationChanged() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onLayout(boolean changed, int l, int t, int r, int b) { throw new RuntimeException("Stub!"); }

/**
 * Sets the alpha value when the content of {@link SurfaceView} needs to show or hide.
 * <p>Note: The surface view may ignore the alpha value in some cases. Refer to
 * {@link SurfaceView#setAlpha} for more details.
 *
 * @param alpha The opacity of the view.
 * @apiSince REL
 */

public void setAlpha(float alpha) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public float getAlpha() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean gatherTransparentRegion(android.graphics.Region region) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

protected void onVisibilityChanged(android.view.View changedView, int visibility) { throw new RuntimeException("Stub!"); }

/**
 * @return the display id of the virtual display.
 * @apiSince REL
 */

public int getVirtualDisplayId() { throw new RuntimeException("Stub!"); }

/**
 * Injects a pair of down/up key events with keycode {@link KeyEvent#KEYCODE_BACK} to the
 * virtual display.
 * @apiSince REL
 */

public void performBackPress() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

protected void finalize() throws java.lang.Throwable { throw new RuntimeException("Stub!"); }

/**
 * Set forwarded insets on the virtual display.
 *
 * @see IWindowManager#setForwardedInsets
 * @apiSince REL
 */

public void setForwardedInsets(android.graphics.Insets insets) { throw new RuntimeException("Stub!"); }
/**
 * Callback that notifies when the container is ready or destroyed.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class StateCallback {

public StateCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called when the container is ready for launching activities. Calling
 * {@link #startActivity(Intent)} prior to this callback will result in an
 * {@link IllegalStateException}.
 *
 * @see #startActivity(Intent)
 * @apiSince REL
 */

public abstract void onActivityViewReady(android.app.ActivityView view);

/**
 * Called when the container can no longer launch activities. Calling
 * {@link #startActivity(Intent)} after this callback will result in an
 * {@link IllegalStateException}.
 *
 * @see #startActivity(Intent)
 * @apiSince REL
 */

public abstract void onActivityViewDestroyed(android.app.ActivityView view);

/**
 * Called when a task is created inside the container.
 * This is a filtered version of {@link TaskStackListener}
 * @apiSince REL
 */

public void onTaskCreated(int taskId, android.content.ComponentName componentName) { throw new RuntimeException("Stub!"); }

/**
 * Called when a task is moved to the front of the stack inside the container.
 * This is a filtered version of {@link TaskStackListener}
 * @apiSince REL
 */

public void onTaskMovedToFront(int taskId) { throw new RuntimeException("Stub!"); }

/**
 * Called when a task is about to be removed from the stack inside the container.
 * This is a filtered version of {@link TaskStackListener}
 * @apiSince REL
 */

public void onTaskRemovalStarted(int taskId) { throw new RuntimeException("Stub!"); }
}

}

