/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.util.proto;


/**
 * Abstract base class for both protobuf streams.
 *
 * Contains a set of useful constants and methods used by both
 * ProtoOutputStream and ProtoInputStream
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ProtoStream {

public ProtoStream() { throw new RuntimeException("Stub!"); }

/**
 * Get the developer-usable name of a field type.
 * @apiSince REL
 */

public static java.lang.String getFieldTypeString(long fieldType) { throw new RuntimeException("Stub!"); }

/**
 * Get the developer-usable name of a field count.
 * @apiSince REL
 */

public static java.lang.String getFieldCountString(long fieldCount) { throw new RuntimeException("Stub!"); }

/**
 * Get the developer-usable name of a wire type.
 * @apiSince REL
 */

public static java.lang.String getWireTypeString(int wireType) { throw new RuntimeException("Stub!"); }

/**
 * Get a debug string for a fieldId.
 * @apiSince REL
 */

public static java.lang.String getFieldIdString(long fieldId) { throw new RuntimeException("Stub!"); }

/**
 * Combine a fieldId (the field keys in the proto file) and the field flags.
 * Mostly useful for testing because the generated code contains the fieldId
 * constants.
 * @apiSince REL
 */

public static long makeFieldId(int id, long fieldFlags) { throw new RuntimeException("Stub!"); }

/**
 * Make a token.
 * Bits 61-63 - tag size (So we can go backwards later if the object had not data)
 *            - 3 bits, max value 7, max value needed 5
 * Bit  60    - true if the object is repeated (lets us require endObject or endRepeatedObject)
 * Bits 59-51 - depth (For error checking)
 *            - 9 bits, max value 512, when checking, value is masked (if we really
 *              are more than 512 levels deep)
 * Bits 32-50 - objectId (For error checking)
 *            - 19 bits, max value 524,288. that's a lot of objects. IDs will wrap
 *              because of the overflow, and only the tokens are compared.
 * Bits  0-31 - offset of interest for the object.
 * @apiSince REL
 */

public static long makeToken(int tagSize, boolean repeated, int depth, int objectId, int offset) { throw new RuntimeException("Stub!"); }

/**
 * Get the encoded tag size from the token.
 * @apiSince REL
 */

public static int getTagSizeFromToken(long token) { throw new RuntimeException("Stub!"); }

/**
 * Get whether this is a call to startObject (false) or startRepeatedObject (true).
 * @apiSince REL
 */

public static boolean getRepeatedFromToken(long token) { throw new RuntimeException("Stub!"); }

/**
 * Get the nesting depth of startObject calls from the token.
 * @apiSince REL
 */

public static int getDepthFromToken(long token) { throw new RuntimeException("Stub!"); }

/**
 * Get the object ID from the token. The object ID is a serial number for the
 * startObject calls that have happened on this object.  The values are truncated
 * to 9 bits, but that is sufficient for error checking.
 * @apiSince REL
 */

public static int getObjectIdFromToken(long token) { throw new RuntimeException("Stub!"); }

/**
 * Get the location of the offset recorded in the token.
 * @apiSince REL
 */

public static int getOffsetFromToken(long token) { throw new RuntimeException("Stub!"); }

/**
 * Convert the object ID to the ordinal value -- the n-th call to startObject.
 * The object IDs start at -1 and count backwards, so that the value is unlikely
 * to alias with an actual size field that had been written.
 * @apiSince REL
 */

public static int convertObjectIdToOrdinal(int objectId) { throw new RuntimeException("Stub!"); }

/**
 * Return a debugging string of a token.
 * @apiSince REL
 */

public static java.lang.String token2String(long token) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final long FIELD_COUNT_MASK = 16492674416640L; // 0xf0000000000L

/** @apiSince REL */

public static final long FIELD_COUNT_PACKED = 5497558138880L; // 0x50000000000L

/** @apiSince REL */

public static final long FIELD_COUNT_REPEATED = 2199023255552L; // 0x20000000000L

/** @apiSince REL */

public static final int FIELD_COUNT_SHIFT = 40; // 0x28

/** @apiSince REL */

public static final long FIELD_COUNT_SINGLE = 1099511627776L; // 0x10000000000L

/** @apiSince REL */

public static final long FIELD_COUNT_UNKNOWN = 0L; // 0x0L

/** @apiSince REL */

public static final int FIELD_ID_MASK = -8; // 0xfffffff8

/** @apiSince REL */

public static final int FIELD_ID_SHIFT = 3; // 0x3

/** @apiSince REL */

public static final long FIELD_TYPE_BOOL = 34359738368L; // 0x800000000L

/** @apiSince REL */

public static final long FIELD_TYPE_BYTES = 51539607552L; // 0xc00000000L

/**
 * The types are copied from external/protobuf/src/google/protobuf/descriptor.h directly,
 * so no extra mapping needs to be maintained in this case.
 * @apiSince REL
 */

public static final long FIELD_TYPE_DOUBLE = 4294967296L; // 0x100000000L

/** @apiSince REL */

public static final long FIELD_TYPE_ENUM = 60129542144L; // 0xe00000000L

/** @apiSince REL */

public static final long FIELD_TYPE_FIXED32 = 30064771072L; // 0x700000000L

/** @apiSince REL */

public static final long FIELD_TYPE_FIXED64 = 25769803776L; // 0x600000000L

/** @apiSince REL */

public static final long FIELD_TYPE_FLOAT = 8589934592L; // 0x200000000L

/** @apiSince REL */

public static final long FIELD_TYPE_INT32 = 21474836480L; // 0x500000000L

/** @apiSince REL */

public static final long FIELD_TYPE_INT64 = 12884901888L; // 0x300000000L

/**
 * Mask for the field types stored in a fieldId.  Leaves a whole
 * byte for future expansion, even though there are currently only 17 types.
 * @apiSince REL
 */

public static final long FIELD_TYPE_MASK = 1095216660480L; // 0xff00000000L

/** @apiSince REL */

public static final long FIELD_TYPE_MESSAGE = 47244640256L; // 0xb00000000L

/** @apiSince REL */

protected static final java.lang.String[] FIELD_TYPE_NAMES;
static { FIELD_TYPE_NAMES = new java.lang.String[0]; }

/** @apiSince REL */

public static final long FIELD_TYPE_SFIXED32 = 64424509440L; // 0xf00000000L

/** @apiSince REL */

public static final long FIELD_TYPE_SFIXED64 = 68719476736L; // 0x1000000000L

/**
 * Position of the field type in a (long) fieldId.
 * @apiSince REL
 */

public static final int FIELD_TYPE_SHIFT = 32; // 0x20

/** @apiSince REL */

public static final long FIELD_TYPE_SINT32 = 73014444032L; // 0x1100000000L

/** @apiSince REL */

public static final long FIELD_TYPE_SINT64 = 77309411328L; // 0x1200000000L

/** @apiSince REL */

public static final long FIELD_TYPE_STRING = 38654705664L; // 0x900000000L

/** @apiSince REL */

public static final long FIELD_TYPE_UINT32 = 55834574848L; // 0xd00000000L

/** @apiSince REL */

public static final long FIELD_TYPE_UINT64 = 17179869184L; // 0x400000000L

/** @apiSince REL */

public static final long FIELD_TYPE_UNKNOWN = 0L; // 0x0L

/** @apiSince REL */

public static final int WIRE_TYPE_END_GROUP = 4; // 0x4

/** @apiSince REL */

public static final int WIRE_TYPE_FIXED32 = 5; // 0x5

/** @apiSince REL */

public static final int WIRE_TYPE_FIXED64 = 1; // 0x1

/** @apiSince REL */

public static final int WIRE_TYPE_LENGTH_DELIMITED = 2; // 0x2

/** @apiSince REL */

public static final int WIRE_TYPE_MASK = 7; // 0x7

/** @apiSince REL */

public static final int WIRE_TYPE_START_GROUP = 3; // 0x3

/** @apiSince REL */

public static final int WIRE_TYPE_VARINT = 0; // 0x0
}

