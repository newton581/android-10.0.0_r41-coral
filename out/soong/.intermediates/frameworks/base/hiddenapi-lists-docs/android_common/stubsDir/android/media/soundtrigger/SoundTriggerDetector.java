/**
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.soundtrigger;


/**
 * A class that allows interaction with the actual sound trigger detection on the system.
 * Sound trigger detection refers to a detectors that match generic sound patterns that are
 * not voice-based. The voice-based recognition models should utilize the {@link
 * VoiceInteractionService} instead. Access to this class is protected by a permission
 * granted only to system or privileged apps.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class SoundTriggerDetector {

SoundTriggerDetector() { throw new RuntimeException("Stub!"); }

/**
 * Starts recognition on the associated sound model. Result is indicated via the
 * {@link Callback}.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_SOUND_TRIGGER}
 * @param recognitionFlags Value is either <code>0</code> or a combination of android.media.soundtrigger.SoundTriggerDetector.RECOGNITION_FLAG_NONE, {@link android.media.soundtrigger.SoundTriggerDetector#RECOGNITION_FLAG_CAPTURE_TRIGGER_AUDIO}, and {@link android.media.soundtrigger.SoundTriggerDetector#RECOGNITION_FLAG_ALLOW_MULTIPLE_TRIGGERS}
 * @return Indicates whether the call succeeded or not.
 * @apiSince REL
 */

public boolean startRecognition(int recognitionFlags) { throw new RuntimeException("Stub!"); }

/**
 * Stops recognition for the associated model.
 
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_SOUND_TRIGGER}
 * @apiSince REL
 */

public boolean stopRecognition() { throw new RuntimeException("Stub!"); }

/**
 * Recognition flag for {@link #startRecognition(int)} that indicates
 * whether the recognition should keep going on even after the
 * model triggers.
 * If this flag is specified, it's possible to get multiple
 * triggers after a call to {@link #startRecognition(int)}, if the model
 * triggers multiple times.
 * When this isn't specified, the default behavior is to stop recognition once the
 * trigger happenss, till the caller starts recognition again.
 * @apiSince REL
 */

public static final int RECOGNITION_FLAG_ALLOW_MULTIPLE_TRIGGERS = 2; // 0x2

/**
 * Recognition flag for {@link #startRecognition(int)} that indicates
 * whether the trigger audio for hotword needs to be captured.
 * @apiSince REL
 */

public static final int RECOGNITION_FLAG_CAPTURE_TRIGGER_AUDIO = 1; // 0x1
/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class Callback {

public Callback() { throw new RuntimeException("Stub!"); }

/**
 * Called when the availability of the sound model changes.
 * @apiSince REL
 */

public abstract void onAvailabilityChanged(int status);

/**
 * Called when the sound model has triggered (such as when it matched a
 * given sound pattern).
 
 * @param eventPayload This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onDetected(@android.annotation.NonNull android.media.soundtrigger.SoundTriggerDetector.EventPayload eventPayload);

/**
 *  Called when the detection fails due to an error.
 * @apiSince REL
 */

public abstract void onError();

/**
 * Called when the recognition is paused temporarily for some reason.
 * This is an informational callback, and the clients shouldn't be doing anything here
 * except showing an indication on their UI if they have to.
 * @apiSince REL
 */

public abstract void onRecognitionPaused();

/**
 * Called when the recognition is resumed after it was temporarily paused.
 * This is an informational callback, and the clients shouldn't be doing anything here
 * except showing an indication on their UI if they have to.
 * @apiSince REL
 */

public abstract void onRecognitionResumed();
}

/**
 * Additional payload for {@link Callback#onDetected}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class EventPayload {

EventPayload(boolean triggerAvailable, boolean captureAvailable, android.media.AudioFormat audioFormat, int captureSession, byte[] data) { throw new RuntimeException("Stub!"); }

/**
 * Gets the format of the audio obtained using {@link #getTriggerAudio()}.
 * May be null if there's no audio present.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.media.AudioFormat getCaptureAudioFormat() { throw new RuntimeException("Stub!"); }

/**
 * Gets the raw audio that triggered the detector.
 * This may be null if the trigger audio isn't available.
 * If non-null, the format of the audio can be obtained by calling
 * {@link #getCaptureAudioFormat()}.
 *
 * @see AlwaysOnHotwordDetector#RECOGNITION_FLAG_CAPTURE_TRIGGER_AUDIO
 * @apiSince REL
 */

@android.annotation.Nullable
public byte[] getTriggerAudio() { throw new RuntimeException("Stub!"); }
}

}

