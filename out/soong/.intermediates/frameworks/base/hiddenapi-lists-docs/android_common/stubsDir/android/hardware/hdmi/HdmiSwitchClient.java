/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.hdmi;

import java.util.concurrent.Executor;
import android.hardware.hdmi.HdmiControlManager.ControlCallbackResult;

/**
 * An {@code HdmiSwitchClient} represents a HDMI-CEC switch device.
 *
 * <p>HdmiSwitchClient has a CEC device type of HdmiDeviceInfo.DEVICE_PURE_CEC_SWITCH, but it is
 * used by all Android devices that have multiple HDMI inputs, even if it is not a "pure" swicth
 * and has another device type like TV or Player.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HdmiSwitchClient extends android.hardware.hdmi.HdmiClient {

HdmiSwitchClient() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getDeviceType() { throw new RuntimeException("Stub!"); }

/**
 * Selects a HDMI port to be a new route path.
 *
 * @param portId HDMI port to select
 * @see {@link android.media.tv.TvInputHardwareInfo#getHdmiPortId()}
 *     to get portId of a specific TV Input.
 * @param listener listener to get the result with
 *
 * This value must never be {@code null}.
 * @hide
 */

public void selectPort(int portId, @android.annotation.NonNull android.hardware.hdmi.HdmiSwitchClient.OnSelectListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Selects a HDMI port to be a new route path.
 *
 * @param portId HDMI port to select
 * @param executor executor to allow the develper to specify the thread upon which the listeners
 *     will be invoked
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param listener listener to get the result with
 *
 * This value must never be {@code null}.
 * @hide
 */

public void selectPort(int portId, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.hardware.hdmi.HdmiSwitchClient.OnSelectListener listener) { throw new RuntimeException("Stub!"); }
/**
 * Listener interface used to get the result of {@link #deviceSelect} or {@link #portSelect}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnSelectListener {

/**
 * Called when the operation is finished.
 *
 * @param result callback result.
 * Value is {@link android.hardware.hdmi.HdmiControlManager#RESULT_SUCCESS}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_TIMEOUT}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_SOURCE_NOT_AVAILABLE}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_TARGET_NOT_AVAILABLE}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_ALREADY_IN_PROGRESS}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_EXCEPTION}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_INCORRECT_MODE}, or {@link android.hardware.hdmi.HdmiControlManager#RESULT_COMMUNICATION_FAILED}
 * @see {@link ControlCallbackResult}
 * @apiSince REL
 */

public void onSelect(@android.hardware.hdmi.HdmiControlManager.ControlCallbackResult int result);
}

}

