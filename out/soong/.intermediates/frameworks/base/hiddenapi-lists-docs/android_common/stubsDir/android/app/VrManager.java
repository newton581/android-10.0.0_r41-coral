
package android.app;

import android.content.Context;
import java.util.concurrent.Executor;
import android.content.ComponentName;

/**
 * Used to control aspects of a devices Virtual Reality (VR) capabilities.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class VrManager {

VrManager() { throw new RuntimeException("Stub!"); }

/**
 * Registers a callback to be notified of changes to the VR Mode state.
 *
 * <br>
 * Requires {@link android.Manifest.permission#RESTRICTED_VR_ACCESS} or android.Manifest.permission.ACCESS_VR_STATE
 * @param callback The callback to register.
 
 * This value must never be {@code null}.
 * @param executor This value must never be {@code null}.
 
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @apiSince REL
 */

public void registerVrStateCallback(@android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.app.VrStateCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Deregisters VR State callbacks.
 *
 * <br>
 * Requires {@link android.Manifest.permission#RESTRICTED_VR_ACCESS} or android.Manifest.permission.ACCESS_VR_STATE
 * @param callback The callback to deregister.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void unregisterVrStateCallback(@android.annotation.NonNull android.app.VrStateCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Returns the current VrMode state.
 
 * <br>
 * Requires {@link android.Manifest.permission#RESTRICTED_VR_ACCESS} or android.Manifest.permission.ACCESS_VR_STATE
 * @apiSince REL
 */

public boolean isVrModeEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Returns the current VrMode state.
 
 * <br>
 * Requires {@link android.Manifest.permission#RESTRICTED_VR_ACCESS} or android.Manifest.permission.ACCESS_VR_STATE
 * @apiSince REL
 */

public boolean isPersistentVrModeEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Sets the persistent VR mode state of a device. When a device is in persistent VR mode it will
 * remain in VR mode even if the foreground does not specify Vr mode being enabled. Mainly used
 * by VR viewers to indicate that a device is placed in a VR viewer.
 *
 * <br>
 * Requires {@link android.Manifest.permission#RESTRICTED_VR_ACCESS}
 * @see Activity#setVrModeEnabled(boolean, ComponentName)
 * @param enabled true if the device should be placed in persistent VR mode.
 * @apiSince REL
 */

public void setPersistentVrModeEnabled(boolean enabled) { throw new RuntimeException("Stub!"); }

/**
 * Sets the resolution and DPI of the vr2d virtual display used to display 2D
 * applications in VR mode.
 *
 * <br>
 * Requires {@link android.Manifest.permission#RESTRICTED_VR_ACCESS}
 * @param vr2dDisplayProp properties to be set to the virtual display for
 * 2D applications in VR mode.
 *
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void setVr2dDisplayProperties(@android.annotation.NonNull android.app.Vr2dDisplayProperties vr2dDisplayProp) { throw new RuntimeException("Stub!"); }

/**
 * Set the component name of the compositor service to bind.
 *
 * <br>
 * Requires {@link android.Manifest.permission#RESTRICTED_VR_ACCESS}
 * @param componentName ComponentName of a Service in the application's compositor process to
 * bind to, or null to clear the current binding.
 * @apiSince REL
 */

public void setAndBindVrCompositor(android.content.ComponentName componentName) { throw new RuntimeException("Stub!"); }

/**
 * Sets the current standby status of the VR device. Standby mode is only used on standalone vr
 * devices. Standby mode is a deep sleep state where it's appropriate to turn off vr mode.
 *
 * <br>
 * Requires android.Manifest.permission.ACCESS_VR_MANAGER
 * @param standby True if the device is entering standby, false if it's exiting standby.
 * @apiSince REL
 */

public void setStandbyEnabled(boolean standby) { throw new RuntimeException("Stub!"); }

/**
 * This method is not implemented.
 *
 * <br>
 * Requires {@link android.Manifest.permission#RESTRICTED_VR_ACCESS}
 * @param componentName not used
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void setVrInputMethod(@android.annotation.Nullable android.content.ComponentName componentName) { throw new RuntimeException("Stub!"); }

/**
 * Returns the display id of VR's {@link VirtualDisplay}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#RESTRICTED_VR_ACCESS}
 * @see DisplayManager#getDisplay(int)
 * @apiSince REL
 */

public int getVr2dDisplayId() { throw new RuntimeException("Stub!"); }
}

