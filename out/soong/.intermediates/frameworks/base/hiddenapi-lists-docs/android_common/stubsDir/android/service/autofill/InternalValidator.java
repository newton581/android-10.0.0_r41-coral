/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill;


/**
 * Superclass of all validators the system understands. As this is not public all public subclasses
 * have to implement {@link Validator} again.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class InternalValidator implements android.service.autofill.Validator, android.os.Parcelable {

public InternalValidator() { throw new RuntimeException("Stub!"); }

/**
 * Decides whether the contents of the screen are valid.
 *
 * @param finder object used to find the value of a field in the screen.
 * This value must never be {@code null}.
 * @return {@code true} if the contents are valid, {@code false} otherwise.
 * @apiSince REL
 */

public abstract boolean isValid(@android.annotation.NonNull android.service.autofill.ValueFinder finder);
}

