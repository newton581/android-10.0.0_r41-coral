/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.net.wifi.hotspot2;

import java.util.Map;
import java.util.List;
import android.graphics.drawable.Icon;

/**
 * Contained information for a Hotspot 2.0 OSU (Online Sign-Up provider).
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class OsuProvider implements android.os.Parcelable {

/**
 * Copy constructor.
 *
 * @param source The source to copy from
 * @hide
 */

OsuProvider(android.net.wifi.hotspot2.OsuProvider source) { throw new RuntimeException("Stub!"); }

/**
 * Return the friendly Name for current language from the list of friendly names of OSU
 * provider.
 *
 * The string matching the default locale will be returned if it is found, otherwise the string
 * in english or the first string in the list will be returned if english is not found.
 * A null will be returned if the list is empty.
 *
 * @return String matching the default locale, null otherwise
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getFriendlyName() { throw new RuntimeException("Stub!"); }

/**
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.net.Uri getServerUri() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object thatObject) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.net.wifi.hotspot2.OsuProvider> CREATOR;
static { CREATOR = null; }
}

