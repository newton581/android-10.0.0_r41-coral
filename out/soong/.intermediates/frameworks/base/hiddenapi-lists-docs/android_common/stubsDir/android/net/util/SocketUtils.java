/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.util;

import java.io.FileDescriptor;

/**
 * Collection of utilities to interact with raw sockets.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class SocketUtils {

SocketUtils() { throw new RuntimeException("Stub!"); }

/**
 * Create a raw datagram socket that is bound to an interface.
 *
 * <p>Data sent through the socket will go directly to the underlying network, ignoring VPNs.
 
 * @param socket This value must never be {@code null}.
 
 * @param iface This value must never be {@code null}.
 * @apiSince REL
 */

public static void bindSocketToInterface(@android.annotation.NonNull java.io.FileDescriptor socket, @android.annotation.NonNull java.lang.String iface) throws android.system.ErrnoException { throw new RuntimeException("Stub!"); }

/**
 * Make a socket address to communicate with netlink.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static java.net.SocketAddress makeNetlinkSocketAddress(int portId, int groupsMask) { throw new RuntimeException("Stub!"); }

/**
 * Make socket address that packet sockets can bind to.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static java.net.SocketAddress makePacketSocketAddress(int protocol, int ifIndex) { throw new RuntimeException("Stub!"); }

/**
 * Make a socket address that packet socket can send packets to.
 
 * @param hwAddr This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static java.net.SocketAddress makePacketSocketAddress(int ifIndex, @android.annotation.NonNull byte[] hwAddr) { throw new RuntimeException("Stub!"); }

/**
 * @see IoBridge#closeAndSignalBlockedThreads(FileDescriptor)
 
 * @param fd This value may be {@code null}.
 * @apiSince REL
 */

public static void closeSocket(@android.annotation.Nullable java.io.FileDescriptor fd) throws java.io.IOException { throw new RuntimeException("Stub!"); }
}

