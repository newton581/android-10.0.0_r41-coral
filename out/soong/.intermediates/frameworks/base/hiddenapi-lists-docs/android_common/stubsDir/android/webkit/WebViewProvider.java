/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.webkit;

import java.util.Map;
import android.view.View;

/**
 * WebView backend provider interface: this interface is the abstract backend to a WebView
 * instance; each WebView object is bound to exactly one WebViewProvider object which implements
 * the runtime behavior of that WebView.
 *
 * All methods must behave as per their namesake in {@link WebView}, unless otherwise noted.
 *
 * @hide Not part of the public API; only required by system implementors.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface WebViewProvider {

/**
 * Initialize this WebViewProvider instance. Called after the WebView has fully constructed.
 * @param javaScriptInterfaces is a Map of interface names, as keys, and
 * object implementing those interfaces, as values.
 * @param privateBrowsing If {@code true} the web view will be initialized in private /
 * incognito mode.
 * @apiSince REL
 */

public void init(java.util.Map<java.lang.String,java.lang.Object> javaScriptInterfaces, boolean privateBrowsing);

/** @apiSince REL */

public void setHorizontalScrollbarOverlay(boolean overlay);

/** @apiSince REL */

public void setVerticalScrollbarOverlay(boolean overlay);

/** @apiSince REL */

public boolean overlayHorizontalScrollbar();

/** @apiSince REL */

public boolean overlayVerticalScrollbar();

/** @apiSince REL */

public int getVisibleTitleHeight();

/** @apiSince REL */

public android.net.http.SslCertificate getCertificate();

/** @apiSince REL */

public void setCertificate(android.net.http.SslCertificate certificate);

/** @apiSince REL */

public void savePassword(java.lang.String host, java.lang.String username, java.lang.String password);

/** @apiSince REL */

public void setHttpAuthUsernamePassword(java.lang.String host, java.lang.String realm, java.lang.String username, java.lang.String password);

/** @apiSince REL */

public java.lang.String[] getHttpAuthUsernamePassword(java.lang.String host, java.lang.String realm);

/**
 * See {@link WebView#destroy()}.
 * As well as releasing the internal state and resources held by the implementation,
 * the provider should null all references it holds on the WebView proxy class, and ensure
 * no further method calls are made to it.
 * @apiSince REL
 */

public void destroy();

/** @apiSince REL */

public void setNetworkAvailable(boolean networkUp);

/** @apiSince REL */

public android.webkit.WebBackForwardList saveState(android.os.Bundle outState);

/** @apiSince REL */

public boolean savePicture(android.os.Bundle b, java.io.File dest);

/** @apiSince REL */

public boolean restorePicture(android.os.Bundle b, java.io.File src);

/** @apiSince REL */

public android.webkit.WebBackForwardList restoreState(android.os.Bundle inState);

/** @apiSince REL */

public void loadUrl(java.lang.String url, java.util.Map<java.lang.String,java.lang.String> additionalHttpHeaders);

/** @apiSince REL */

public void loadUrl(java.lang.String url);

/** @apiSince REL */

public void postUrl(java.lang.String url, byte[] postData);

/** @apiSince REL */

public void loadData(java.lang.String data, java.lang.String mimeType, java.lang.String encoding);

/** @apiSince REL */

public void loadDataWithBaseURL(java.lang.String baseUrl, java.lang.String data, java.lang.String mimeType, java.lang.String encoding, java.lang.String historyUrl);

/** @apiSince REL */

public void evaluateJavaScript(java.lang.String script, android.webkit.ValueCallback<java.lang.String> resultCallback);

/** @apiSince REL */

public void saveWebArchive(java.lang.String filename);

/** @apiSince REL */

public void saveWebArchive(java.lang.String basename, boolean autoname, android.webkit.ValueCallback<java.lang.String> callback);

/** @apiSince REL */

public void stopLoading();

/** @apiSince REL */

public void reload();

/** @apiSince REL */

public boolean canGoBack();

/** @apiSince REL */

public void goBack();

/** @apiSince REL */

public boolean canGoForward();

/** @apiSince REL */

public void goForward();

/** @apiSince REL */

public boolean canGoBackOrForward(int steps);

/** @apiSince REL */

public void goBackOrForward(int steps);

/** @apiSince REL */

public boolean isPrivateBrowsingEnabled();

/** @apiSince REL */

public boolean pageUp(boolean top);

/** @apiSince REL */

public boolean pageDown(boolean bottom);

/** @apiSince REL */

public void insertVisualStateCallback(long requestId, android.webkit.WebView.VisualStateCallback callback);

/** @apiSince REL */

public void clearView();

/** @apiSince REL */

public android.graphics.Picture capturePicture();

/** @apiSince REL */

public android.print.PrintDocumentAdapter createPrintDocumentAdapter(java.lang.String documentName);

/** @apiSince REL */

public float getScale();

/** @apiSince REL */

public void setInitialScale(int scaleInPercent);

/** @apiSince REL */

public void invokeZoomPicker();

/** @apiSince REL */

public android.webkit.WebView.HitTestResult getHitTestResult();

/** @apiSince REL */

public void requestFocusNodeHref(android.os.Message hrefMsg);

/** @apiSince REL */

public void requestImageRef(android.os.Message msg);

/** @apiSince REL */

public java.lang.String getUrl();

/** @apiSince REL */

public java.lang.String getOriginalUrl();

/** @apiSince REL */

public java.lang.String getTitle();

/** @apiSince REL */

public android.graphics.Bitmap getFavicon();

/** @apiSince REL */

public java.lang.String getTouchIconUrl();

/** @apiSince REL */

public int getProgress();

/** @apiSince REL */

public int getContentHeight();

/** @apiSince REL */

public int getContentWidth();

/** @apiSince REL */

public void pauseTimers();

/** @apiSince REL */

public void resumeTimers();

/** @apiSince REL */

public void onPause();

/** @apiSince REL */

public void onResume();

/** @apiSince REL */

public boolean isPaused();

/** @apiSince REL */

public void freeMemory();

/** @apiSince REL */

public void clearCache(boolean includeDiskFiles);

/** @apiSince REL */

public void clearFormData();

/** @apiSince REL */

public void clearHistory();

/** @apiSince REL */

public void clearSslPreferences();

/** @apiSince REL */

public android.webkit.WebBackForwardList copyBackForwardList();

/** @apiSince REL */

public void setFindListener(android.webkit.WebView.FindListener listener);

/** @apiSince REL */

public void findNext(boolean forward);

/** @apiSince REL */

public int findAll(java.lang.String find);

/** @apiSince REL */

public void findAllAsync(java.lang.String find);

/** @apiSince REL */

public boolean showFindDialog(java.lang.String text, boolean showIme);

/** @apiSince REL */

public void clearMatches();

/** @apiSince REL */

public void documentHasImages(android.os.Message response);

/** @apiSince REL */

public void setWebViewClient(android.webkit.WebViewClient client);

/** @apiSince REL */

public android.webkit.WebViewClient getWebViewClient();

/**
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.webkit.WebViewRenderProcess getWebViewRenderProcess();

/**
 * @param executor This value may be {@code null}.
 
 * @param client This value may be {@code null}.
 * @apiSince REL
 */

public void setWebViewRenderProcessClient(@android.annotation.Nullable java.util.concurrent.Executor executor, @android.annotation.Nullable android.webkit.WebViewRenderProcessClient client);

/**
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.webkit.WebViewRenderProcessClient getWebViewRenderProcessClient();

/** @apiSince REL */

public void setDownloadListener(android.webkit.DownloadListener listener);

/** @apiSince REL */

public void setWebChromeClient(android.webkit.WebChromeClient client);

/** @apiSince REL */

public android.webkit.WebChromeClient getWebChromeClient();

/** @apiSince REL */

public void setPictureListener(android.webkit.WebView.PictureListener listener);

/** @apiSince REL */

public void addJavascriptInterface(java.lang.Object obj, java.lang.String interfaceName);

/** @apiSince REL */

public void removeJavascriptInterface(java.lang.String interfaceName);

/** @apiSince REL */

public android.webkit.WebMessagePort[] createWebMessageChannel();

/** @apiSince REL */

public void postMessageToMainFrame(android.webkit.WebMessage message, android.net.Uri targetOrigin);

/** @apiSince REL */

public android.webkit.WebSettings getSettings();

/** @apiSince REL */

public void setMapTrackballToArrowKeys(boolean setMap);

/** @apiSince REL */

public void flingScroll(int vx, int vy);

/** @apiSince REL */

public android.view.View getZoomControls();

/** @apiSince REL */

public boolean canZoomIn();

/** @apiSince REL */

public boolean canZoomOut();

/** @apiSince REL */

public boolean zoomBy(float zoomFactor);

/** @apiSince REL */

public boolean zoomIn();

/** @apiSince REL */

public boolean zoomOut();

/** @apiSince REL */

public void dumpViewHierarchyWithProperties(java.io.BufferedWriter out, int level);

/** @apiSince REL */

public android.view.View findHierarchyView(java.lang.String className, int hashCode);

/** @apiSince REL */

public void setRendererPriorityPolicy(int rendererRequestedPriority, boolean waivedWhenNotVisible);

/** @apiSince REL */

public int getRendererRequestedPriority();

/** @apiSince REL */

public boolean getRendererPriorityWaivedWhenNotVisible();

/**
 * @param textClassifier This value may be {@code null}.
 * @apiSince REL
 */

public default void setTextClassifier(@android.annotation.Nullable android.view.textclassifier.TextClassifier textClassifier) { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public default android.view.textclassifier.TextClassifier getTextClassifier() { throw new RuntimeException("Stub!"); }

/**
 * @return the ViewDelegate implementation. This provides the functionality to back all of
 * the name-sake functions from the View and ViewGroup base classes of WebView.
 * @apiSince REL
 */

public android.webkit.WebViewProvider.ViewDelegate getViewDelegate();

/**
 * @return a ScrollDelegate implementation. Normally this would be same object as is
 * returned by getViewDelegate().
 * @apiSince REL
 */

public android.webkit.WebViewProvider.ScrollDelegate getScrollDelegate();

/**
 * Only used by FindActionModeCallback to inform providers that the find dialog has
 * been dismissed.
 * @apiSince REL
 */

public void notifyFindDialogDismissed();
/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface ScrollDelegate {

/**
 * See {@link android.webkit.WebView#computeHorizontalScrollRange}
 * @apiSince REL
 */

public int computeHorizontalScrollRange();

/**
 * See {@link android.webkit.WebView#computeHorizontalScrollOffset}
 * @apiSince REL
 */

public int computeHorizontalScrollOffset();

/**
 * See {@link android.webkit.WebView#computeVerticalScrollRange}
 * @apiSince REL
 */

public int computeVerticalScrollRange();

/**
 * See {@link android.webkit.WebView#computeVerticalScrollOffset}
 * @apiSince REL
 */

public int computeVerticalScrollOffset();

/**
 * See {@link android.webkit.WebView#computeVerticalScrollExtent}
 * @apiSince REL
 */

public int computeVerticalScrollExtent();

/**
 * See {@link android.webkit.WebView#computeScroll}
 * @apiSince REL
 */

public void computeScroll();
}

/**
 * Provides mechanism for the name-sake methods declared in View and ViewGroup to be delegated
 * into the WebViewProvider instance.
 * NOTE: For many of these methods, the WebView will provide a super.Foo() call before or after
 * making the call into the provider instance. This is done for convenience in the common case
 * of maintaining backward compatibility. For remaining super class calls (e.g. where the
 * provider may need to only conditionally make the call based on some internal state) see the
 * {@link WebView.PrivateAccess} callback class.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface ViewDelegate {

/** @apiSince REL */

public boolean shouldDelayChildPressedState();

/** @apiSince REL */

public void onProvideVirtualStructure(android.view.ViewStructure structure);

/** @apiSince REL */

public default void onProvideAutofillVirtualStructure(android.view.ViewStructure structure, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public default void autofill(android.util.SparseArray<android.view.autofill.AutofillValue> values) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public default boolean isVisibleToUserForAutofill(int virtualId) { throw new RuntimeException("Stub!"); }

/**
 * @param structure This value must never be {@code null}.
 * @apiSince REL
 */

public default void onProvideContentCaptureStructure(@android.annotation.NonNull android.view.ViewStructure structure, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.view.accessibility.AccessibilityNodeProvider getAccessibilityNodeProvider();

/** @apiSince REL */

public void onInitializeAccessibilityNodeInfo(android.view.accessibility.AccessibilityNodeInfo info);

/** @apiSince REL */

public void onInitializeAccessibilityEvent(android.view.accessibility.AccessibilityEvent event);

/** @apiSince REL */

public boolean performAccessibilityAction(int action, android.os.Bundle arguments);

/** @apiSince REL */

public void setOverScrollMode(int mode);

/** @apiSince REL */

public void setScrollBarStyle(int style);

/** @apiSince REL */

public void onDrawVerticalScrollBar(android.graphics.Canvas canvas, android.graphics.drawable.Drawable scrollBar, int l, int t, int r, int b);

/** @apiSince REL */

public void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY);

/** @apiSince REL */

public void onWindowVisibilityChanged(int visibility);

/** @apiSince REL */

public void onDraw(android.graphics.Canvas canvas);

/** @apiSince REL */

public void setLayoutParams(android.view.ViewGroup.LayoutParams layoutParams);

/** @apiSince REL */

public boolean performLongClick();

/** @apiSince REL */

public void onConfigurationChanged(android.content.res.Configuration newConfig);

/** @apiSince REL */

public android.view.inputmethod.InputConnection onCreateInputConnection(android.view.inputmethod.EditorInfo outAttrs);

/** @apiSince REL */

public boolean onDragEvent(android.view.DragEvent event);

/** @apiSince REL */

public boolean onKeyMultiple(int keyCode, int repeatCount, android.view.KeyEvent event);

/** @apiSince REL */

public boolean onKeyDown(int keyCode, android.view.KeyEvent event);

/** @apiSince REL */

public boolean onKeyUp(int keyCode, android.view.KeyEvent event);

/** @apiSince REL */

public void onAttachedToWindow();

/** @apiSince REL */

public void onDetachedFromWindow();

/** @apiSince REL */

public default void onMovedToDisplay(int displayId, android.content.res.Configuration config) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onVisibilityChanged(android.view.View changedView, int visibility);

/** @apiSince REL */

public void onWindowFocusChanged(boolean hasWindowFocus);

/** @apiSince REL */

public void onFocusChanged(boolean focused, int direction, android.graphics.Rect previouslyFocusedRect);

/** @apiSince REL */

public boolean setFrame(int left, int top, int right, int bottom);

/** @apiSince REL */

public void onSizeChanged(int w, int h, int ow, int oh);

/** @apiSince REL */

public void onScrollChanged(int l, int t, int oldl, int oldt);

/** @apiSince REL */

public boolean dispatchKeyEvent(android.view.KeyEvent event);

/** @apiSince REL */

public boolean onTouchEvent(android.view.MotionEvent ev);

/** @apiSince REL */

public boolean onHoverEvent(android.view.MotionEvent event);

/** @apiSince REL */

public boolean onGenericMotionEvent(android.view.MotionEvent event);

/** @apiSince REL */

public boolean onTrackballEvent(android.view.MotionEvent ev);

/** @apiSince REL */

public boolean requestFocus(int direction, android.graphics.Rect previouslyFocusedRect);

/** @apiSince REL */

public void onMeasure(int widthMeasureSpec, int heightMeasureSpec);

/** @apiSince REL */

public boolean requestChildRectangleOnScreen(android.view.View child, android.graphics.Rect rect, boolean immediate);

/** @apiSince REL */

public void setBackgroundColor(int color);

/** @apiSince REL */

public void setLayerType(int layerType, android.graphics.Paint paint);

/** @apiSince REL */

public void preDispatchDraw(android.graphics.Canvas canvas);

/** @apiSince REL */

public void onStartTemporaryDetach();

/** @apiSince REL */

public void onFinishTemporaryDetach();

/** @apiSince REL */

public void onActivityResult(int requestCode, int resultCode, android.content.Intent data);

/** @apiSince REL */

public android.os.Handler getHandler(android.os.Handler originalHandler);

/** @apiSince REL */

public android.view.View findFocus(android.view.View originalFocusedView);

/** @apiSince REL */

public default boolean onCheckIsTextEditor() { throw new RuntimeException("Stub!"); }
}

}

