/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.location;


/**
 * A class implementing a container for data associated with a measurement event.
 * Events are delivered to registered instances of {@link Listener}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class GpsMeasurementsEvent implements android.os.Parcelable {

/** @apiSince REL */

public GpsMeasurementsEvent(android.location.GpsClock clock, android.location.GpsMeasurement[] measurements) { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GpsClock getClock() { throw new RuntimeException("Stub!"); }

/**
 * Gets a read-only collection of measurements associated with the current event.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.Collection<android.location.GpsMeasurement> getMeasurements() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.location.GpsMeasurementsEvent> CREATOR;
static { CREATOR = null; }

/**
 * GPS provider or Location is disabled, updates will not be received until they are enabled.
 * @apiSince REL
 */

public static final int STATUS_GPS_LOCATION_DISABLED = 2; // 0x2

/**
 * The system does not support tracking of GPS Measurements. This status will not change in the
 * future.
 * @apiSince REL
 */

public static final int STATUS_NOT_SUPPORTED = 0; // 0x0

/**
 * GPS Measurements are successfully being tracked, it will receive updates once they are
 * available.
 * @apiSince REL
 */

public static final int STATUS_READY = 1; // 0x1
/**
 * Used for receiving GPS satellite measurements from the GPS engine.
 * Each measurement contains raw and computed data identifying a satellite.
 * You can implement this interface and call {@link LocationManager#addGpsMeasurementListener}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface Listener {

/**
 * Returns the latest collected GPS Measurements.
 * @apiSince REL
 */

public void onGpsMeasurementsReceived(android.location.GpsMeasurementsEvent eventArgs);

/**
 * Returns the latest status of the GPS Measurements sub-system.
 * @apiSince REL
 */

public void onStatusChanged(int status);
}

}

