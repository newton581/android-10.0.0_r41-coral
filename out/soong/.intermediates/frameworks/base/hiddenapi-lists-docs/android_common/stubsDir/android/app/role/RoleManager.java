/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.role;

import android.content.Intent;
import android.Manifest;
import android.os.UserHandle;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import android.content.Context;

/**
 * This class provides information about and manages roles.
 * <p>
 * A role is a unique name within the system associated with certain privileges. The list of
 * available roles might change with a system app update, so apps should not make assumption about
 * the availability of roles. Instead, they should always query if the role is available using
 * {@link #isRoleAvailable(String)} before trying to do anything with it. Some predefined role names
 * are available as constants in this class, and a list of possibly available roles can be found in
 * the <a href="{@docRoot}reference/androidx/core/role/package-summary.html">AndroidX Role
 * library</a>.
 * <p>
 * There can be multiple applications qualifying for a role, but only a subset of them can become
 * role holders. To qualify for a role, an application must meet certain requirements, including
 * defining certain components in its manifest. These requirements can be found in the AndroidX
 * Libraries. Then the application will need user consent to become a role holder, which can be
 * requested using {@link android.app.Activity#startActivityForResult(Intent, int)} with the
 * {@code Intent} obtained from {@link #createRequestRoleIntent(String)}.
 * <p>
 * Upon becoming a role holder, the application may be granted certain privileges that are role
 * specific. When the application loses its role, these privileges will also be revoked.
 * @apiSince 29
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class RoleManager {

RoleManager() { throw new RuntimeException("Stub!"); }

/**
 * Returns an {@code Intent} suitable for passing to
 * {@link android.app.Activity#startActivityForResult(Intent, int)} which prompts the user to
 * grant a role to this application.
 * <p>
 * If the role is granted, the {@code resultCode} will be
 * {@link android.app.Activity#RESULT_OK}, otherwise it will be
 * {@link android.app.Activity#RESULT_CANCELED}.
 *
 * @param roleName the name of requested role
 *
 * This value must never be {@code null}.
 * @return the {@code Intent} to prompt user to grant the role
 
 * This value will never be {@code null}.
 * @apiSince 29
 */

@android.annotation.NonNull
public android.content.Intent createRequestRoleIntent(@android.annotation.NonNull java.lang.String roleName) { throw new RuntimeException("Stub!"); }

/**
 * Check whether a role is available in the system.
 *
 * @param roleName the name of role to checking for
 *
 * This value must never be {@code null}.
 * @return whether the role is available in the system
 * @apiSince 29
 */

public boolean isRoleAvailable(@android.annotation.NonNull java.lang.String roleName) { throw new RuntimeException("Stub!"); }

/**
 * Check whether the calling application is holding a particular role.
 *
 * @param roleName the name of the role to check for
 *
 * This value must never be {@code null}.
 * @return whether the calling application is holding the role
 * @apiSince 29
 */

public boolean isRoleHeld(@android.annotation.NonNull java.lang.String roleName) { throw new RuntimeException("Stub!"); }

/**
 * Get package names of the applications holding the role.
 * <p>
 * <strong>Note:</strong> Using this API requires holding
 * {@code android.permission.MANAGE_ROLE_HOLDERS}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ROLE_HOLDERS}
 * @param roleName the name of the role to get the role holder for
 *
 * This value must never be {@code null}.
 * @return a list of package names of the role holders, or an empty list if none.
 *
 * This value will never be {@code null}.
 * @see #getRoleHoldersAsUser(String, UserHandle)
 *
 * @hide
 */

@android.annotation.NonNull
public java.util.List<java.lang.String> getRoleHolders(@android.annotation.NonNull java.lang.String roleName) { throw new RuntimeException("Stub!"); }

/**
 * Get package names of the applications holding the role.
 * <p>
 * <strong>Note:</strong> Using this API requires holding
 * {@code android.permission.MANAGE_ROLE_HOLDERS} and if the user id is not the current user
 * {@code android.permission.INTERACT_ACROSS_USERS_FULL}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ROLE_HOLDERS}
 * @param roleName the name of the role to get the role holder for
 * This value must never be {@code null}.
 * @param user the user to get the role holder for
 *
 * This value must never be {@code null}.
 * @return a list of package names of the role holders, or an empty list if none.
 *
 * This value will never be {@code null}.
 * @see #addRoleHolderAsUser(String, String, int, UserHandle, Executor, Consumer)
 * @see #removeRoleHolderAsUser(String, String, int, UserHandle, Executor, Consumer)
 * @see #clearRoleHoldersAsUser(String, int, UserHandle, Executor, Consumer)
 *
 * @hide
 */

@android.annotation.NonNull
public java.util.List<java.lang.String> getRoleHoldersAsUser(@android.annotation.NonNull java.lang.String roleName, @android.annotation.NonNull android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Add a specific application to the holders of a role. If the role is exclusive, the previous
 * holder will be replaced.
 * <p>
 * <strong>Note:</strong> Using this API requires holding
 * {@code android.permission.MANAGE_ROLE_HOLDERS} and if the user id is not the current user
 * {@code android.permission.INTERACT_ACROSS_USERS_FULL}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ROLE_HOLDERS}
 * @param roleName the name of the role to add the role holder for
 * This value must never be {@code null}.
 * @param packageName the package name of the application to add to the role holders
 * This value must never be {@code null}.
 * @param flags optional behavior flags
 * Value is either <code>0</code> or {@link android.app.role.RoleManager#MANAGE_HOLDERS_FLAG_DONT_KILL_APP}
 * @param user the user to add the role holder for
 * This value must never be {@code null}.
 * @param executor the {@code Executor} to run the callback on.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * This value must never be {@code null}.
 * @param callback the callback for whether this call is successful
 *
 * This value must never be {@code null}.
 * @see #getRoleHoldersAsUser(String, UserHandle)
 * @see #removeRoleHolderAsUser(String, String, int, UserHandle, Executor, Consumer)
 * @see #clearRoleHoldersAsUser(String, int, UserHandle, Executor, Consumer)
 *
 * @hide
 */

public void addRoleHolderAsUser(@android.annotation.NonNull java.lang.String roleName, @android.annotation.NonNull java.lang.String packageName, int flags, @android.annotation.NonNull android.os.UserHandle user, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull java.util.function.Consumer<java.lang.Boolean> callback) { throw new RuntimeException("Stub!"); }

/**
 * Remove a specific application from the holders of a role.
 * <p>
 * <strong>Note:</strong> Using this API requires holding
 * {@code android.permission.MANAGE_ROLE_HOLDERS} and if the user id is not the current user
 * {@code android.permission.INTERACT_ACROSS_USERS_FULL}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ROLE_HOLDERS}
 * @param roleName the name of the role to remove the role holder for
 * This value must never be {@code null}.
 * @param packageName the package name of the application to remove from the role holders
 * This value must never be {@code null}.
 * @param flags optional behavior flags
 * Value is either <code>0</code> or {@link android.app.role.RoleManager#MANAGE_HOLDERS_FLAG_DONT_KILL_APP}
 * @param user the user to remove the role holder for
 * This value must never be {@code null}.
 * @param executor the {@code Executor} to run the callback on.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * This value must never be {@code null}.
 * @param callback the callback for whether this call is successful
 *
 * This value must never be {@code null}.
 * @see #getRoleHoldersAsUser(String, UserHandle)
 * @see #addRoleHolderAsUser(String, String, int, UserHandle, Executor, Consumer)
 * @see #clearRoleHoldersAsUser(String, int, UserHandle, Executor, Consumer)
 *
 * @hide
 */

public void removeRoleHolderAsUser(@android.annotation.NonNull java.lang.String roleName, @android.annotation.NonNull java.lang.String packageName, int flags, @android.annotation.NonNull android.os.UserHandle user, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull java.util.function.Consumer<java.lang.Boolean> callback) { throw new RuntimeException("Stub!"); }

/**
 * Remove all holders of a role.
 * <p>
 * <strong>Note:</strong> Using this API requires holding
 * {@code android.permission.MANAGE_ROLE_HOLDERS} and if the user id is not the current user
 * {@code android.permission.INTERACT_ACROSS_USERS_FULL}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ROLE_HOLDERS}
 * @param roleName the name of the role to remove role holders for
 * This value must never be {@code null}.
 * @param flags optional behavior flags
 * Value is either <code>0</code> or {@link android.app.role.RoleManager#MANAGE_HOLDERS_FLAG_DONT_KILL_APP}
 * @param user the user to remove role holders for
 * This value must never be {@code null}.
 * @param executor the {@code Executor} to run the callback on.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * This value must never be {@code null}.
 * @param callback the callback for whether this call is successful
 *
 * This value must never be {@code null}.
 * @see #getRoleHoldersAsUser(String, UserHandle)
 * @see #addRoleHolderAsUser(String, String, int, UserHandle, Executor, Consumer)
 * @see #removeRoleHolderAsUser(String, String, int, UserHandle, Executor, Consumer)
 *
 * @hide
 */

public void clearRoleHoldersAsUser(@android.annotation.NonNull java.lang.String roleName, int flags, @android.annotation.NonNull android.os.UserHandle user, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull java.util.function.Consumer<java.lang.Boolean> callback) { throw new RuntimeException("Stub!"); }

/**
 * Add a listener to observe role holder changes
 * <p>
 * <strong>Note:</strong> Using this API requires holding
 * {@code android.permission.OBSERVE_ROLE_HOLDERS} and if the user id is not the current user
 * {@code android.permission.INTERACT_ACROSS_USERS_FULL}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#OBSERVE_ROLE_HOLDERS}
 * @param executor the {@code Executor} to call the listener on.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * This value must never be {@code null}.
 * @param listener the listener to be added
 * This value must never be {@code null}.
 * @param user the user to add the listener for
 *
 * This value must never be {@code null}.
 * @see #removeOnRoleHoldersChangedListenerAsUser(OnRoleHoldersChangedListener, UserHandle)
 *
 * @hide
 */

public void addOnRoleHoldersChangedListenerAsUser(@android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.app.role.OnRoleHoldersChangedListener listener, @android.annotation.NonNull android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Remove a listener observing role holder changes
 * <p>
 * <strong>Note:</strong> Using this API requires holding
 * {@code android.permission.OBSERVE_ROLE_HOLDERS} and if the user id is not the current user
 * {@code android.permission.INTERACT_ACROSS_USERS_FULL}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#OBSERVE_ROLE_HOLDERS}
 * @param listener the listener to be removed
 * This value must never be {@code null}.
 * @param user the user to remove the listener for
 *
 * This value must never be {@code null}.
 * @see #addOnRoleHoldersChangedListenerAsUser(Executor, OnRoleHoldersChangedListener,
 *                                             UserHandle)
 *
 * @hide
 */

public void removeOnRoleHoldersChangedListenerAsUser(@android.annotation.NonNull android.app.role.OnRoleHoldersChangedListener listener, @android.annotation.NonNull android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Set the names of all the available roles. Should only be called from
 * {@link android.app.role.RoleControllerService}.
 * <p>
 * <strong>Note:</strong> Using this API requires holding
 * {@link #PERMISSION_MANAGE_ROLES_FROM_CONTROLLER}.
 *
 * <br>
 * Requires android.app.role.RoleManager.PERMISSION_MANAGE_ROLES_FROM_CONTROLLER
 * @param roleNames the names of all the available roles
 *
 * This value must never be {@code null}.
 * @hide
 */

public void setRoleNamesFromController(@android.annotation.NonNull java.util.List<java.lang.String> roleNames) { throw new RuntimeException("Stub!"); }

/**
 * Add a specific application to the holders of a role, only modifying records inside
 * {@link RoleManager}. Should only be called from
 * {@link android.app.role.RoleControllerService}.
 * <p>
 * <strong>Note:</strong> Using this API requires holding
 * {@link #PERMISSION_MANAGE_ROLES_FROM_CONTROLLER}.
 *
 * <br>
 * Requires android.app.role.RoleManager.PERMISSION_MANAGE_ROLES_FROM_CONTROLLER
 * @param roleName the name of the role to add the role holder for
 * This value must never be {@code null}.
 * @param packageName the package name of the application to add to the role holders
 *
 * This value must never be {@code null}.
 * @return whether the operation was successful, and will also be {@code true} if a matching
 *         role holder is already found.
 *
 * @see #getRoleHolders(String)
 * @see #removeRoleHolderFromController(String, String)
 *
 * @hide
 */

public boolean addRoleHolderFromController(@android.annotation.NonNull java.lang.String roleName, @android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Remove a specific application from the holders of a role, only modifying records inside
 * {@link RoleManager}. Should only be called from
 * {@link android.app.role.RoleControllerService}.
 * <p>
 * <strong>Note:</strong> Using this API requires holding
 * {@link #PERMISSION_MANAGE_ROLES_FROM_CONTROLLER}.
 *
 * <br>
 * Requires android.app.role.RoleManager.PERMISSION_MANAGE_ROLES_FROM_CONTROLLER
 * @param roleName the name of the role to remove the role holder for
 * This value must never be {@code null}.
 * @param packageName the package name of the application to remove from the role holders
 *
 * This value must never be {@code null}.
 * @return whether the operation was successful, and will also be {@code true} if no matching
 *         role holder was found to remove.
 *
 * @see #getRoleHolders(String)
 * @see #addRoleHolderFromController(String, String)
 *
 * @hide
 */

public boolean removeRoleHolderFromController(@android.annotation.NonNull java.lang.String roleName, @android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Returns the list of all roles that the given package is currently holding
 *
 * <br>
 * Requires android.app.role.RoleManager.PERMISSION_MANAGE_ROLES_FROM_CONTROLLER
 * @param packageName the package name
 * This value must never be {@code null}.
 * @return the list of role names
 *
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public java.util.List<java.lang.String> getHeldRolesFromController(@android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Flag parameter for {@link #addRoleHolderAsUser}, {@link #removeRoleHolderAsUser} and
 * {@link #clearRoleHoldersAsUser} to indicate that apps should not be killed when changing
 * their role holder status.
 *
 * @hide
 */

public static final int MANAGE_HOLDERS_FLAG_DONT_KILL_APP = 1; // 0x1

/**
 * The name of the assistant app role.
 *
 * @see android.service.voice.VoiceInteractionService
 * @apiSince 29
 */

public static final java.lang.String ROLE_ASSISTANT = "android.app.role.ASSISTANT";

/**
 * The name of the browser role.
 *
 * @see Intent#CATEGORY_APP_BROWSER
 * @apiSince 29
 */

public static final java.lang.String ROLE_BROWSER = "android.app.role.BROWSER";

/**
 * The name of the call redirection role.
 * <p>
 * A call redirection app provides a means to re-write the phone number for an outgoing call to
 * place the call through a call redirection service.
 *
 * @see android.telecom.CallRedirectionService
 * @apiSince 29
 */

public static final java.lang.String ROLE_CALL_REDIRECTION = "android.app.role.CALL_REDIRECTION";

/**
 * The name of the call screening and caller id role.
 *
 * @see android.telecom.CallScreeningService
 * @apiSince 29
 */

public static final java.lang.String ROLE_CALL_SCREENING = "android.app.role.CALL_SCREENING";

/**
 * The name of the dialer role.
 *
 * @see Intent#ACTION_DIAL
 * @apiSince 29
 */

public static final java.lang.String ROLE_DIALER = "android.app.role.DIALER";

/**
 * The name of the emergency role
 *
 * @see android.telephony.TelephonyManager#ACTION_EMERGENCY_ASSISTANCE
 * @apiSince 29
 */

public static final java.lang.String ROLE_EMERGENCY = "android.app.role.EMERGENCY";

/**
 * The name of the home role.
 *
 * @see Intent#CATEGORY_HOME
 * @apiSince 29
 */

public static final java.lang.String ROLE_HOME = "android.app.role.HOME";

/**
 * The name of the SMS role.
 *
 * @see Intent#CATEGORY_APP_MESSAGING
 * @apiSince 29
 */

public static final java.lang.String ROLE_SMS = "android.app.role.SMS";
}

