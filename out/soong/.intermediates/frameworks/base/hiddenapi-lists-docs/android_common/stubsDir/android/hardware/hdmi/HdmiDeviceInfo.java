/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.hdmi;

import android.os.Parcelable;
import android.os.Parcel;

/**
 * A class to encapsulate device information for HDMI devices including CEC and MHL. In terms of
 * CEC, this container includes basic information such as logical address, physical address and
 * device type, and additional information like vendor id and osd name. In terms of MHL device, this
 * container includes adopter id and device type. Otherwise, it keeps the information of other type
 * devices for which only port ID, physical address are meaningful.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HdmiDeviceInfo implements android.os.Parcelable {

/**
 * Constructor. Used to initialize the instance representing an inactivated device.
 * Can be passed input change listener to indicate the active source yielded
 * its status, hence the listener should take an appropriate action such as
 * switching to other input.
 * @apiSince REL
 */

public HdmiDeviceInfo() { throw new RuntimeException("Stub!"); }

/**
 * Returns the id of the device.
 * @apiSince REL
 */

public int getId() { throw new RuntimeException("Stub!"); }

/**
 * Returns the id to be used for CEC device.
 *
 * @param address logical address of CEC device
 * @return id for CEC device
 * @apiSince REL
 */

public static int idForCecDevice(int address) { throw new RuntimeException("Stub!"); }

/**
 * Returns the id to be used for MHL device.
 *
 * @param portId port which the MHL device is connected to
 * @return id for MHL device
 * @apiSince REL
 */

public static int idForMhlDevice(int portId) { throw new RuntimeException("Stub!"); }

/**
 * Returns the id to be used for hardware port.
 *
 * @param portId port id
 * @return id for hardware port
 * @apiSince REL
 */

public static int idForHardware(int portId) { throw new RuntimeException("Stub!"); }

/**
 * Returns the CEC logical address of the device.
 * @apiSince REL
 */

public int getLogicalAddress() { throw new RuntimeException("Stub!"); }

/**
 * Returns the physical address of the device.
 * @apiSince REL
 */

public int getPhysicalAddress() { throw new RuntimeException("Stub!"); }

/**
 * Returns the port ID.
 * @apiSince REL
 */

public int getPortId() { throw new RuntimeException("Stub!"); }

/**
 * Returns CEC type of the device. For more details, refer constants between {@link #DEVICE_TV}
 * and {@link #DEVICE_INACTIVE}.
 * @apiSince REL
 */

public int getDeviceType() { throw new RuntimeException("Stub!"); }

/**
 * Returns device's power status. It should be one of the following values.
 * <ul>
 * <li>{@link HdmiControlManager#POWER_STATUS_ON}
 * <li>{@link HdmiControlManager#POWER_STATUS_STANDBY}
 * <li>{@link HdmiControlManager#POWER_STATUS_TRANSIENT_TO_ON}
 * <li>{@link HdmiControlManager#POWER_STATUS_TRANSIENT_TO_STANDBY}
 * <li>{@link HdmiControlManager#POWER_STATUS_UNKNOWN}
 * </ul>
 * @apiSince REL
 */

public int getDevicePowerStatus() { throw new RuntimeException("Stub!"); }

/**
 * Returns MHL device id. Return -1 for non-MHL device.
 * @apiSince REL
 */

public int getDeviceId() { throw new RuntimeException("Stub!"); }

/**
 * Returns MHL adopter id. Return -1 for non-MHL device.
 * @apiSince REL
 */

public int getAdopterId() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the device is of a type that can be an input source.
 * @apiSince REL
 */

public boolean isSourceType() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the device represents an HDMI-CEC device. {@code false} if the device
 * is either MHL or other device.
 * @apiSince REL
 */

public boolean isCecDevice() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the device represents an MHL device. {@code false} if the device is
 * either CEC or other device.
 * @apiSince REL
 */

public boolean isMhlDevice() { throw new RuntimeException("Stub!"); }

/**
 * Return {@code true} if the device represents an inactivated device that relinquishes
 * its status as active source by &lt;Active Source&gt; (HDMI-CEC) or Content-off (MHL).
 * @apiSince REL
 */

public boolean isInactivated() { throw new RuntimeException("Stub!"); }

/**
 * Returns display (OSD) name of the device.
 * @apiSince REL
 */

public java.lang.String getDisplayName() { throw new RuntimeException("Stub!"); }

/**
 * Returns vendor id of the device. Vendor id is used to distinguish devices built by other
 * manufactures. This is required for vendor-specific command on CEC standard.
 * @apiSince REL
 */

public int getVendorId() { throw new RuntimeException("Stub!"); }

/**
 * Describes the kinds of special objects contained in this Parcelable's marshalled
 * representation.
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Serializes this object into a {@link Parcel}.
 *
 * @param dest The Parcel in which the object should be written.
 * @param flags Additional flags about how the object should be written. May be 0 or
 *            {@link Parcelable#PARCELABLE_WRITE_RETURN_VALUE}.
 * @apiSince REL
 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * Logical address used to indicate the source comes from internal device. The logical address
 * of TV(0) is used.
 * @apiSince REL
 */

public static final int ADDR_INTERNAL = 0; // 0x0

/**
 * A helper class to deserialize {@link HdmiDeviceInfo} for a parcel.
 * @apiSince REL
 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.hdmi.HdmiDeviceInfo> CREATOR;
static { CREATOR = null; }

/**
 * Audio system device type.
 * @apiSince REL
 */

public static final int DEVICE_AUDIO_SYSTEM = 5; // 0x5

/** @apiSince REL */

public static final int DEVICE_INACTIVE = -1; // 0xffffffff

/**
 * Playback device type.
 * @apiSince REL
 */

public static final int DEVICE_PLAYBACK = 4; // 0x4

/**
 * Recording device type.
 * @apiSince REL
 */

public static final int DEVICE_RECORDER = 1; // 0x1

/**
 * Device type reserved for future usage.
 * @apiSince REL
 */

public static final int DEVICE_RESERVED = 2; // 0x2

/**
 * Tuner device type.
 * @apiSince REL
 */

public static final int DEVICE_TUNER = 3; // 0x3

/**
 * TV device type.
 * @apiSince REL
 */

public static final int DEVICE_TV = 0; // 0x0

/**
 * Invalid device ID
 * @apiSince REL
 */

public static final int ID_INVALID = 65535; // 0xffff

/**
 * Device info used to indicate an inactivated device.
 * @apiSince REL
 */

public static final android.hardware.hdmi.HdmiDeviceInfo INACTIVE_DEVICE;
static { INACTIVE_DEVICE = null; }

/**
 * Physical address used to indicate the source comes from internal device. The physical address
 * of TV(0) is used.
 * @apiSince REL
 */

public static final int PATH_INTERNAL = 0; // 0x0

/**
 * Invalid physical address (routing path)
 * @apiSince REL
 */

public static final int PATH_INVALID = 65535; // 0xffff

/**
 * Invalid port ID
 * @apiSince REL
 */

public static final int PORT_INVALID = -1; // 0xffffffff
}

