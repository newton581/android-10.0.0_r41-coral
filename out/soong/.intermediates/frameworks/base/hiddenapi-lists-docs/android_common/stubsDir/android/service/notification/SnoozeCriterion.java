/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.notification;


/**
 * Represents an option to be shown to users for snoozing a notification until a given context
 * instead of for a fixed amount of time.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class SnoozeCriterion implements android.os.Parcelable {

/** @apiSince REL */

public SnoozeCriterion(java.lang.String id, java.lang.CharSequence explanation, java.lang.CharSequence confirmation) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

protected SnoozeCriterion(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Returns the id of this criterion.
 * @apiSince REL
 */

public java.lang.String getId() { throw new RuntimeException("Stub!"); }

/**
 * Returns the user visible explanation of how long a notification will be snoozed if
 * this criterion is chosen.
 * @apiSince REL
 */

public java.lang.CharSequence getExplanation() { throw new RuntimeException("Stub!"); }

/**
 * Returns the user visible confirmation message shown when this criterion is chosen.
 * @apiSince REL
 */

public java.lang.CharSequence getConfirmation() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.notification.SnoozeCriterion> CREATOR;
static { CREATOR = null; }
}

