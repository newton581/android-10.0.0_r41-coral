/**
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.trust;

import android.Manifest;
import android.app.admin.DevicePolicyManager;
import android.os.PersistableBundle;
import android.content.ComponentName;
import android.os.Message;
import android.content.Intent;

/**
 * A service that notifies the system about whether it believes the environment of the device
 * to be trusted.
 *
 * <p>Trust agents may only be provided by the platform. It is expected that there is only
 * one trust agent installed on the platform. In the event there is more than one,
 * either trust agent can enable trust.
 * </p>
 *
 * <p>To extend this class, you must declare the service in your manifest file with
 * the {@link android.Manifest.permission#BIND_TRUST_AGENT} permission
 * and include an intent filter with the {@link #SERVICE_INTERFACE} action. For example:</p>
 * <pre>
 * &lt;service android:name=".TrustAgent"
 *          android:label="&#64;string/service_name"
 *          android:permission="android.permission.BIND_TRUST_AGENT">
 *     &lt;intent-filter>
 *         &lt;action android:name="android.service.trust.TrustAgentService" />
 *     &lt;/intent-filter>
 *     &lt;meta-data android:name="android.service.trust.trustagent"
 *          android:value="&#64;xml/trust_agent" />
 * &lt;/service></pre>
 *
 * <p>The associated meta-data file can specify an activity that is accessible through Settings
 * and should allow configuring the trust agent, as defined in
 * {@link android.R.styleable#TrustAgent}. For example:</p>
 *
 * <pre>
 * &lt;trust-agent xmlns:android="http://schemas.android.com/apk/res/android"
 *          android:settingsActivity=".TrustAgentSettings" /></pre>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class TrustAgentService extends android.app.Service {

public TrustAgentService() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onCreate() { throw new RuntimeException("Stub!"); }

/**
 * Called after the user attempts to authenticate in keyguard with their device credentials,
 * such as pin, pattern or password.
 *
 * @param successful true if the user successfully completed the challenge.
 * @apiSince REL
 */

public void onUnlockAttempt(boolean successful) { throw new RuntimeException("Stub!"); }

/**
 * Called when the timeout provided by the agent expires.  Note that this may be called earlier
 * than requested by the agent if the trust timeout is adjusted by the system or
 * {@link DevicePolicyManager}.  The agent is expected to re-evaluate the trust state and only
 * call {@link #grantTrust(CharSequence, long, boolean)} if the trust state should be
 * continued.
 * @apiSince REL
 */

public void onTrustTimeout() { throw new RuntimeException("Stub!"); }

/**
 * Called when the device enters a state where a PIN, pattern or
 * password must be entered to unlock it.
 * @apiSince REL
 */

public void onDeviceLocked() { throw new RuntimeException("Stub!"); }

/**
 * Called when the device leaves a state where a PIN, pattern or
 * password must be entered to unlock it.
 * @apiSince REL
 */

public void onDeviceUnlocked() { throw new RuntimeException("Stub!"); }

/**
 * Called when the device enters a temporary unlock lockout.
 *
 * <p>This occurs when the user has consecutively failed to unlock the device too many times,
 * and must wait until a timeout has passed to perform another attempt. The user may then only
 * use strong authentication mechanisms (PIN, pattern or password) to unlock the device.
 * Calls to {@link #grantTrust(CharSequence, long, int)} will be ignored until the user has
 * unlocked the device and {@link #onDeviceUnlocked()} is called.
 *
 * @param timeoutMs The amount of time, in milliseconds, that needs to elapse before the user
 *    can attempt to unlock the device again.
 * @apiSince REL
 */

public void onDeviceUnlockLockout(long timeoutMs) { throw new RuntimeException("Stub!"); }

/**
 * Called when an escrow token is added for user userId.
 *
 * @param token the added token
 * @param handle the handle to the corresponding internal synthetic password. A user is unlocked
 * by presenting both handle and escrow token.
 * @param user the user to which the escrow token is added.
 *
 * @apiSince REL
 */

public void onEscrowTokenAdded(byte[] token, long handle, android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Called when an escrow token state is received upon request.
 *
 * @param handle the handle to the internal synthetic password.
 * @param state the state of the requested escrow token, see {@link TokenState}.
 *
 
 * @param tokenState Value is either <code>0</code> or a combination of {@link android.service.trust.TrustAgentService#TOKEN_STATE_ACTIVE}, and {@link android.service.trust.TrustAgentService#TOKEN_STATE_INACTIVE}
 * @apiSince REL
 */

public void onEscrowTokenStateReceived(long handle, int tokenState) { throw new RuntimeException("Stub!"); }

/**
 * Called when an escrow token is removed.
 *
 * @param handle the handle to the removed the synthetic password.
 * @param successful whether the removing operaiton is achieved.
 *
 * @apiSince REL
 */

public void onEscrowTokenRemoved(long handle, boolean successful) { throw new RuntimeException("Stub!"); }

/**
 * Called when device policy admin wants to enable specific options for agent in response to
 * {@link DevicePolicyManager#setKeyguardDisabledFeatures(ComponentName, int)} and
 * {@link DevicePolicyManager#setTrustAgentConfiguration(ComponentName, ComponentName,
 * PersistableBundle)}.
 * <p>Agents that support configuration options should overload this method and return 'true'.
 *
 * @param options The aggregated list of options or an empty list if no restrictions apply.
 * @return true if it supports configuration options.
 * @apiSince REL
 */

public boolean onConfigure(java.util.List<android.os.PersistableBundle> options) { throw new RuntimeException("Stub!"); }

/**
 * Call to grant trust on the device.
 *
 * @param message describes why the device is trusted, e.g. "Trusted by location".
 * @param durationMs amount of time in milliseconds to keep the device in a trusted state.
 *    Trust for this agent will automatically be revoked when the timeout expires unless
 *    extended by a subsequent call to this function. The timeout is measured from the
 *    invocation of this function as dictated by {@link SystemClock#elapsedRealtime())}.
 *    For security reasons, the value should be no larger than necessary.
 *    The value may be adjusted by the system as necessary to comply with a policy controlled
 *    by the system or {@link DevicePolicyManager} restrictions. See {@link #onTrustTimeout()}
 *    for determining when trust expires.
 * @param initiatedByUser this is a hint to the system that trust is being granted as the
 *    direct result of user action - such as solving a security challenge. The hint is used
 *    by the system to optimize the experience. Behavior may vary by device and release, so
 *    one should only set this parameter if it meets the above criteria rather than relying on
 *    the behavior of any particular device or release. Corresponds to
 *    {@link #FLAG_GRANT_TRUST_INITIATED_BY_USER}.
 * @throws IllegalStateException if the agent is not currently managing trust.
 *
 * @deprecated use {@link #grantTrust(CharSequence, long, int)} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public final void grantTrust(java.lang.CharSequence message, long durationMs, boolean initiatedByUser) { throw new RuntimeException("Stub!"); }

/**
 * Call to grant trust on the device.
 *
 * @param message describes why the device is trusted, e.g. "Trusted by location".
 * @param durationMs amount of time in milliseconds to keep the device in a trusted state.
 *    Trust for this agent will automatically be revoked when the timeout expires unless
 *    extended by a subsequent call to this function. The timeout is measured from the
 *    invocation of this function as dictated by {@link SystemClock#elapsedRealtime())}.
 *    For security reasons, the value should be no larger than necessary.
 *    The value may be adjusted by the system as necessary to comply with a policy controlled
 *    by the system or {@link DevicePolicyManager} restrictions. See {@link #onTrustTimeout()}
 *    for determining when trust expires.
 * @param flags TBDocumented
 * Value is either <code>0</code> or a combination of {@link android.service.trust.TrustAgentService#FLAG_GRANT_TRUST_INITIATED_BY_USER}, and {@link android.service.trust.TrustAgentService#FLAG_GRANT_TRUST_DISMISS_KEYGUARD}
 * @throws IllegalStateException if the agent is not currently managing trust.
 * @apiSince REL
 */

public final void grantTrust(java.lang.CharSequence message, long durationMs, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Call to revoke trust on the device.
 * @apiSince REL
 */

public final void revokeTrust() { throw new RuntimeException("Stub!"); }

/**
 * Call to notify the system if the agent is ready to manage trust.
 *
 * This property is not persistent across recreating the service and defaults to false.
 * Therefore this method is typically called when initializing the agent in {@link #onCreate}.
 *
 * @param managingTrust indicates if the agent would like to manage trust.
 * @apiSince REL
 */

public final void setManagingTrust(boolean managingTrust) { throw new RuntimeException("Stub!"); }

/**
 * Call to add an escrow token to derive a synthetic password. A synthetic password is an
 * alternaive to the user-set password/pin/pattern in order to unlock encrypted disk. An escrow
 * token can be taken and internally derive the synthetic password. The new added token will not
 * be acivated until the user input the correct PIN/Passcode/Password once.
 *
 * Result will be return by callback {@link #onEscrowTokenAdded(long, int)}
 *
 * @param token an escrow token of high entropy.
 * @param user the user which the escrow token will be added to.
 *
 * @apiSince REL
 */

public final void addEscrowToken(byte[] token, android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Call to check the active state of an escrow token.
 *
 * Result will be return in callback {@link #onEscrowTokenStateReceived(long, boolean)}
 *
 * @param handle the handle of escrow token to the internal synthetic password.
 * @param user the user which the escrow token is added to.
 *
 * @apiSince REL
 */

public final void isEscrowTokenActive(long handle, android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Call to remove the escrow token.
 *
 * Result will be return in callback {@link #onEscrowTokenRemoved(long, boolean)}
 *
 * @param handle the handle of escrow tokent to the internal synthetic password.
 * @param user the user id which the escrow token is added to.
 *
 * @apiSince REL
 */

public final void removeEscrowToken(long handle, android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Call to unlock user's FBE.
 *
 * @param handle the handle of escrow tokent to the internal synthetic password.
 * @param token the escrow token
 * @param user the user about to be unlocked.
 *
 * @apiSince REL
 */

public final void unlockUserWithToken(long handle, byte[] token, android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Request showing a transient error message on the keyguard.
 * The message will be visible on the lock screen or always on display if possible but can be
 * overridden by other keyguard events of higher priority - eg. fingerprint auth error.
 * Other trust agents may override your message if posted simultaneously.
 *
 * @param message Message to show.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public final void showKeyguardErrorMessage(@android.annotation.NonNull java.lang.CharSequence message) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public final android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Flag for {@link #grantTrust(CharSequence, long, int)} indicating that the agent would like
 * to dismiss the keyguard. When using this flag, the {@code TrustAgentService} must ensure
 * it is only set in response to a direct user action with the expectation of dismissing the
 * keyguard.
 * @apiSince REL
 */

public static final int FLAG_GRANT_TRUST_DISMISS_KEYGUARD = 2; // 0x2

/**
 * Flag for {@link #grantTrust(CharSequence, long, int)} indicating that trust is being granted
 * as the direct result of user action - such as solving a security challenge. The hint is used
 * by the system to optimize the experience. Behavior may vary by device and release, so
 * one should only set this parameter if it meets the above criteria rather than relying on
 * the behavior of any particular device or release.
 * @apiSince REL
 */

public static final int FLAG_GRANT_TRUST_INITIATED_BY_USER = 1; // 0x1

/**
 * The {@link Intent} that must be declared as handled by the service.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.service.trust.TrustAgentService";

/**
 * Int enum indicating that escrow token is active.
 * See {@link #onEscrowTokenStateReceived(long, int)}
 *
 * @apiSince REL
 */

public static final int TOKEN_STATE_ACTIVE = 1; // 0x1

/**
 * Int enum indicating that escow token is inactive.
 * See {@link #onEscrowTokenStateReceived(long, int)}
 *
 * @apiSince REL
 */

public static final int TOKEN_STATE_INACTIVE = 0; // 0x0

/**
 * The name of the {@code meta-data} tag pointing to additional configuration of the trust
 * agent.
 * @apiSince REL
 */

public static final java.lang.String TRUST_AGENT_META_DATA = "android.service.trust.trustagent";
}

