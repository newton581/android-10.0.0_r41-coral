/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.usage;

import android.content.Intent;

/**
 * CacheQuoteService defines a service which accepts cache quota requests and processes them,
 * thereby filling out how much quota each request deserves.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class CacheQuotaService extends android.app.Service {

public CacheQuotaService() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onCreate() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Processes the cache quota list upon receiving a list of requests.
 * @param requests A list of cache quotas to fulfill.
 * @return A completed list of cache quota requests.
 * @apiSince REL
 */

public abstract java.util.List<android.app.usage.CacheQuotaHint> onComputeCacheQuotaHints(java.util.List<android.app.usage.CacheQuotaHint> requests);

/**
 * The {@link Intent} action that must be declared as handled by a service
 * in its manifest for the system to recognize it as a quota providing service.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.app.usage.CacheQuotaService";
}

