/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;

import android.telephony.DisconnectCause;
import android.telephony.PreciseDisconnectCause;

/**
 * Contains precise call states.
 *
 * The following call information is included in returned PreciseCallState:
 *
 * <ul>
 *   <li>Precise ringing call state.
 *   <li>Precise foreground call state.
 *   <li>Precise background call state.
 * </ul>
 *
 * @see android.telephony.TelephonyManager.CallState which contains generic call states.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class PreciseCallState implements android.os.Parcelable {

/**
 * Empty Constructor
 *
 * @hide
 */

PreciseCallState() { throw new RuntimeException("Stub!"); }

/**
 * Returns the precise ringing call state.
 
 * @return Value is {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_NOT_VALID}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_IDLE}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_ACTIVE}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_HOLDING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_DIALING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_ALERTING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_INCOMING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_WAITING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_DISCONNECTED}, or {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_DISCONNECTING}
 * @apiSince REL
 */

public int getRingingCallState() { throw new RuntimeException("Stub!"); }

/**
 * Returns the precise foreground call state.
 
 * @return Value is {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_NOT_VALID}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_IDLE}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_ACTIVE}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_HOLDING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_DIALING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_ALERTING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_INCOMING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_WAITING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_DISCONNECTED}, or {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_DISCONNECTING}
 * @apiSince REL
 */

public int getForegroundCallState() { throw new RuntimeException("Stub!"); }

/**
 * Returns the precise background call state.
 
 * @return Value is {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_NOT_VALID}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_IDLE}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_ACTIVE}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_HOLDING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_DIALING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_ALERTING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_INCOMING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_WAITING}, {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_DISCONNECTED}, or {@link android.telephony.PreciseCallState#PRECISE_CALL_STATE_DISCONNECTING}
 * @apiSince REL
 */

public int getBackgroundCallState() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.PreciseCallState> CREATOR;
static { CREATOR = null; }

/**
 * Call state: Active.
 * @apiSince REL
 */

public static final int PRECISE_CALL_STATE_ACTIVE = 1; // 0x1

/**
 * Call state: Alerting.
 * @apiSince REL
 */

public static final int PRECISE_CALL_STATE_ALERTING = 4; // 0x4

/**
 * Call state: Dialing.
 * @apiSince REL
 */

public static final int PRECISE_CALL_STATE_DIALING = 3; // 0x3

/**
 * Call state: Disconnected.
 * @apiSince REL
 */

public static final int PRECISE_CALL_STATE_DISCONNECTED = 7; // 0x7

/**
 * Call state: Disconnecting.
 * @apiSince REL
 */

public static final int PRECISE_CALL_STATE_DISCONNECTING = 8; // 0x8

/**
 * Call state: On hold.
 * @apiSince REL
 */

public static final int PRECISE_CALL_STATE_HOLDING = 2; // 0x2

/**
 * Call state: No activity.
 * @apiSince REL
 */

public static final int PRECISE_CALL_STATE_IDLE = 0; // 0x0

/**
 * Call state: Incoming.
 * @apiSince REL
 */

public static final int PRECISE_CALL_STATE_INCOMING = 5; // 0x5

/**
 * Call state is not valid (Not received a call state).
 * @apiSince REL
 */

public static final int PRECISE_CALL_STATE_NOT_VALID = -1; // 0xffffffff

/**
 * Call state: Waiting.
 * @apiSince REL
 */

public static final int PRECISE_CALL_STATE_WAITING = 6; // 0x6
}

