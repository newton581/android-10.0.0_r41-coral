/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony.ims;

import android.telephony.SubscriptionManager;
import android.content.Context;
import android.Manifest;
import java.util.concurrent.Executor;
import android.telephony.ims.stub.ImsRegistrationImplBase;
import android.telephony.ims.feature.MmTelFeature;
import android.telephony.AccessNetworkConstants;
import android.net.Uri;

/**
 * A manager for the MmTel (Multimedia Telephony) feature of an IMS network, given an associated
 * subscription.
 *
 * Allows a user to query the IMS MmTel feature information for a subscription, register for
 * registration and MmTel capability status callbacks, as well as query/modify user settings for the
 * associated subscription.
 *
 * @see #createForSubscriptionId(int)
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ImsMmTelManager {

/**
 * Only visible for testing, use {@link #createForSubscriptionId(int)} instead.
 * @hide
 */

ImsMmTelManager(int subId) { throw new RuntimeException("Stub!"); }

/**
 * Create an instance of {@link ImsMmTelManager} for the subscription id specified.
 *
 * @param subId The ID of the subscription that this ImsMmTelManager will use.
 * @see android.telephony.SubscriptionManager#getActiveSubscriptionInfoList()
 * @throws IllegalArgumentException if the subscription is invalid.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static android.telephony.ims.ImsMmTelManager createForSubscriptionId(int subId) { throw new RuntimeException("Stub!"); }

/**
 * Registers a {@link RegistrationCallback} with the system, which will provide registration
 * updates for the subscription specified in {@link #createForSubscriptionId(int)}. Use
 * {@link SubscriptionManager.OnSubscriptionsChangedListener} to listen to Subscription changed
 * events and call {@link #unregisterImsRegistrationCallback(RegistrationCallback)} to clean up.
 *
 * When the callback is registered, it will initiate the callback c to be called with the
 * current registration state.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param executor The executor the callback events should be run on.
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param c The {@link RegistrationCallback} to be added.
 * This value must never be {@code null}.
 * @see #unregisterImsRegistrationCallback(RegistrationCallback)
 * @throws IllegalArgumentException if the subscription associated with this callback is not
 * active (SIM is not inserted, ESIM inactive) or invalid, or a null {@link Executor} or
 * {@link CapabilityCallback} callback.
 * @throws ImsException if the subscription associated with this callback is valid, but
 * the {@link ImsService} associated with the subscription is not available. This can happen if
 * the service crashed, for example. See {@link ImsException#getCode()} for a more detailed
 * reason.
 * @apiSince REL
 */

public void registerImsRegistrationCallback(@android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.telephony.ims.ImsMmTelManager.RegistrationCallback c) throws android.telephony.ims.ImsException { throw new RuntimeException("Stub!"); }

/**
 * Removes an existing {@link RegistrationCallback}.
 *
 * When the subscription associated with this callback is removed (SIM removed, ESIM swap,
 * etc...), this callback will automatically be removed. If this method is called for an
 * inactive subscription, it will result in a no-op.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param c The {@link RegistrationCallback} to be removed.
 * This value must never be {@code null}.
 * @see SubscriptionManager.OnSubscriptionsChangedListener
 * @see #registerImsRegistrationCallback(Executor, RegistrationCallback)
 * @throws IllegalArgumentException if the subscription ID associated with this callback is
 * invalid.
 * @apiSince REL
 */

public void unregisterImsRegistrationCallback(@android.annotation.NonNull android.telephony.ims.ImsMmTelManager.RegistrationCallback c) { throw new RuntimeException("Stub!"); }

/**
 * Registers a {@link CapabilityCallback} with the system, which will provide MmTel service
 * availability updates for the subscription specified in
 * {@link #createForSubscriptionId(int)}. The method {@link #isAvailable(int, int)}
 * can also be used to query this information at any time.
 *
 * Use {@link SubscriptionManager.OnSubscriptionsChangedListener} to listen to
 * subscription changed events and call
 * {@link #unregisterImsRegistrationCallback(RegistrationCallback)} to clean up.
 *
 * When the callback is registered, it will initiate the callback c to be called with the
 * current capabilities.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param executor The executor the callback events should be run on.
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param c The MmTel {@link CapabilityCallback} to be registered.
 * This value must never be {@code null}.
 * @see #unregisterMmTelCapabilityCallback(CapabilityCallback)
 * @throws IllegalArgumentException if the subscription associated with this callback is not
 * active (SIM is not inserted, ESIM inactive) or invalid, or a null {@link Executor} or
 * {@link CapabilityCallback} callback.
 * @throws ImsException if the subscription associated with this callback is valid, but
 * the {@link ImsService} associated with the subscription is not available. This can happen if
 * the service crashed, for example. See {@link ImsException#getCode()} for a more detailed
 * reason.
 * @apiSince REL
 */

public void registerMmTelCapabilityCallback(@android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.telephony.ims.ImsMmTelManager.CapabilityCallback c) throws android.telephony.ims.ImsException { throw new RuntimeException("Stub!"); }

/**
 * Removes an existing MmTel {@link CapabilityCallback}.
 *
 * When the subscription associated with this callback is removed (SIM removed, ESIM swap,
 * etc...), this callback will automatically be removed. If this method is called for an
 * inactive subscription, it will result in a no-op.
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @param c The MmTel {@link CapabilityCallback} to be removed.
 * This value must never be {@code null}.
 * @see #registerMmTelCapabilityCallback(Executor, CapabilityCallback)
 * @throws IllegalArgumentException if the subscription ID associated with this callback is
 * invalid.
 * @apiSince REL
 */

public void unregisterMmTelCapabilityCallback(@android.annotation.NonNull android.telephony.ims.ImsMmTelManager.CapabilityCallback c) { throw new RuntimeException("Stub!"); }

/**
 * Query the user’s setting for “Advanced Calling” or "Enhanced 4G LTE", which is used to
 * enable MmTel IMS features, depending on the carrier configuration for the current
 * subscription. If this setting is enabled, IMS voice and video telephony over IWLAN/LTE will
 * be enabled as long as the carrier has provisioned these services for the specified
 * subscription. Other IMS services (SMS/UT) are not affected by this user setting and depend on
 * carrier requirements.
 *
 * Modifying this value may also trigger an IMS registration or deregistration, depending on
 * whether or not the new value is enabled or disabled.
 *
 * Note: If the carrier configuration for advanced calling is not editable or hidden, this
 * method will do nothing and will instead always use the default value.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @see android.telephony.CarrierConfigManager#KEY_CARRIER_VOLTE_PROVISIONING_REQUIRED_BOOL
 * @see android.telephony.CarrierConfigManager#KEY_EDITABLE_ENHANCED_4G_LTE_BOOL
 * @see android.telephony.CarrierConfigManager#KEY_HIDE_ENHANCED_4G_LTE_BOOL
 * @see android.telephony.CarrierConfigManager#KEY_ENHANCED_4G_LTE_ON_BY_DEFAULT_BOOL
 * @see android.telephony.CarrierConfigManager#KEY_CARRIER_VOLTE_AVAILABLE_BOOL
 * @see #setAdvancedCallingSettingEnabled(boolean)
 * @return true if the user's setting for advanced calling is enabled, false otherwise.
 * @apiSince REL
 */

public boolean isAdvancedCallingSettingEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Modify the user’s setting for “Advanced Calling” or "Enhanced 4G LTE", which is used to
 * enable MmTel IMS features, depending on the carrier configuration for the current
 * subscription. If this setting is enabled, IMS voice and video telephony over IWLAN/LTE will
 * be enabled as long as the carrier has provisioned these services for the specified
 * subscription. Other IMS services (SMS/UT) are not affected by this user setting and depend on
 * carrier requirements.
 *
 * Modifying this value may also trigger an IMS registration or deregistration, depending on
 * whether or not the new value is enabled or disabled.
 *
 * Note: If the carrier configuration for advanced calling is not editable or hidden, this
 * method will do nothing and will instead always use the default value.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @see android.telephony.CarrierConfigManager#KEY_CARRIER_VOLTE_PROVISIONING_REQUIRED_BOOL
 * @see android.telephony.CarrierConfigManager#KEY_EDITABLE_ENHANCED_4G_LTE_BOOL
 * @see android.telephony.CarrierConfigManager#KEY_HIDE_ENHANCED_4G_LTE_BOOL
 * @see android.telephony.CarrierConfigManager#KEY_ENHANCED_4G_LTE_ON_BY_DEFAULT_BOOL
 * @see android.telephony.CarrierConfigManager#KEY_CARRIER_VOLTE_AVAILABLE_BOOL
 * @see #isAdvancedCallingSettingEnabled()
 * @apiSince REL
 */

public void setAdvancedCallingSettingEnabled(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Query the IMS MmTel capability for a given registration technology. This does not
 * necessarily mean that we are registered and the capability is available, but rather the
 * subscription is capable of this service over IMS.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @see android.telephony.CarrierConfigManager#KEY_CARRIER_VOLTE_AVAILABLE_BOOL
 * @see android.telephony.CarrierConfigManager#KEY_CARRIER_VT_AVAILABLE_BOOL
 * @see android.telephony.CarrierConfigManager#KEY_CARRIER_IMS_GBA_REQUIRED_BOOL
 * @see #isAvailable(int, int)
 *
 * @param imsRegTech The IMS registration technology, can be one of the following:
 *         {@link ImsRegistrationImplBase#REGISTRATION_TECH_LTE},
 *         {@link ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @param capability The IMS MmTel capability to query, can be one of the following:
 *         {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE},
 *         {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO,
 *         {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT},
 *         {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @return {@code true} if the MmTel IMS capability is capable for this subscription, false
 *         otherwise.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @apiSince REL
 */

public boolean isCapable(int capability, int imsRegTech) { throw new RuntimeException("Stub!"); }

/**
 * Query the availability of an IMS MmTel capability for a given registration technology. If
 * a capability is available, IMS is registered and the service is currently available over IMS.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @see #isCapable(int, int)
 *
 * @param imsRegTech The IMS registration technology, can be one of the following:
 *         {@link ImsRegistrationImplBase#REGISTRATION_TECH_LTE},
 *         {@link ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @param capability The IMS MmTel capability to query, can be one of the following:
 *         {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE},
 *         {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO,
 *         {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT},
 *         {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @return {@code true} if the MmTel IMS capability is available for this subscription, false
 *         otherwise.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @apiSince REL
 */

public boolean isAvailable(int capability, int imsRegTech) { throw new RuntimeException("Stub!"); }

/**
 * The user's setting for whether or not they have enabled the "Video Calling" setting.
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return true if the user’s “Video Calling” setting is currently enabled.
 * @see #setVtSettingEnabled(boolean)
 * @apiSince REL
 */

public boolean isVtSettingEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Change the user's setting for Video Telephony and enable the Video Telephony capability.
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @see #isVtSettingEnabled()
 * @apiSince REL
 */

public void setVtSettingEnabled(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return true if the user's setting for Voice over WiFi is enabled and false if it is not.
 * @see #setVoWiFiSettingEnabled(boolean)
 * @apiSince REL
 */

public boolean isVoWiFiSettingEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Sets the user's setting for whether or not Voice over WiFi is enabled.
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param isEnabled true if the user's setting for Voice over WiFi is enabled, false otherwise=
 * @see #isVoWiFiSettingEnabled()
 * @apiSince REL
 */

public void setVoWiFiSettingEnabled(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return true if the user's setting for Voice over WiFi while roaming is enabled, false
 * if disabled.
 * @see #setVoWiFiRoamingSettingEnabled(boolean)
 * @apiSince REL
 */

public boolean isVoWiFiRoamingSettingEnabled() { throw new RuntimeException("Stub!"); }

/**
 * Change the user's setting for Voice over WiFi while roaming.
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param isEnabled true if the user's setting for Voice over WiFi while roaming is enabled,
 *     false otherwise.
 * @see #isVoWiFiRoamingSettingEnabled()
 * @apiSince REL
 */

public void setVoWiFiRoamingSettingEnabled(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Overrides the Voice over WiFi capability to true for IMS, but do not persist the setting.
 * Typically used during the Voice over WiFi registration process for some carriers.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param isCapable true if the IMS stack should try to register for IMS over IWLAN, false
 *     otherwise.
 * @param mode the Voice over WiFi mode preference to set, which can be one of the following:
 * - {@link #WIFI_MODE_WIFI_ONLY}
 * - {@link #WIFI_MODE_CELLULAR_PREFERRED}
 * - {@link #WIFI_MODE_WIFI_PREFERRED}
 * @see #setVoWiFiSettingEnabled(boolean)
 * @apiSince REL
 */

public void setVoWiFiNonPersistent(boolean isCapable, int mode) { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return The Voice over WiFi Mode preference set by the user, which can be one of the
 * following:
 * - {@link #WIFI_MODE_WIFI_ONLY}
 * - {@link #WIFI_MODE_CELLULAR_PREFERRED}
 * - {@link #WIFI_MODE_WIFI_PREFERRED}
 * Value is {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_WIFI_ONLY}, {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_CELLULAR_PREFERRED}, or {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_WIFI_PREFERRED}
 * @see #setVoWiFiSettingEnabled(boolean)
 * @apiSince REL
 */

public int getVoWiFiModeSetting() { throw new RuntimeException("Stub!"); }

/**
 * Set the user's preference for Voice over WiFi calling mode.
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param mode The user's preference for the technology to register for IMS over, can be one of
 *    the following:
 * - {@link #WIFI_MODE_WIFI_ONLY}
 * - {@link #WIFI_MODE_CELLULAR_PREFERRED}
 * - {@link #WIFI_MODE_WIFI_PREFERRED}
 * Value is {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_WIFI_ONLY}, {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_CELLULAR_PREFERRED}, or {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_WIFI_PREFERRED}
 * @see #getVoWiFiModeSetting()
 * @apiSince REL
 */

public void setVoWiFiModeSetting(int mode) { throw new RuntimeException("Stub!"); }

/**
 * Set the user's preference for Voice over WiFi calling mode while the device is roaming on
 * another network.
 *
 * <br>
 * Requires {@link android.Manifest.permission#READ_PRIVILEGED_PHONE_STATE}
 * @return The user's preference for the technology to register for IMS over when roaming on
 *     another network, can be one of the following:
 *     - {@link #WIFI_MODE_WIFI_ONLY}
 *     - {@link #WIFI_MODE_CELLULAR_PREFERRED}
 *     - {@link #WIFI_MODE_WIFI_PREFERRED}
 * Value is {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_WIFI_ONLY}, {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_CELLULAR_PREFERRED}, or {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_WIFI_PREFERRED}
 * @see #setVoWiFiRoamingSettingEnabled(boolean)
 * @apiSince REL
 */

public int getVoWiFiRoamingModeSetting() { throw new RuntimeException("Stub!"); }

/**
 * Set the user's preference for Voice over WiFi mode while the device is roaming on another
 * network.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param mode The user's preference for the technology to register for IMS over when roaming on
 *     another network, can be one of the following:
 *     - {@link #WIFI_MODE_WIFI_ONLY}
 *     - {@link #WIFI_MODE_CELLULAR_PREFERRED}
 *     - {@link #WIFI_MODE_WIFI_PREFERRED}
 * Value is {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_WIFI_ONLY}, {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_CELLULAR_PREFERRED}, or {@link android.telephony.ims.ImsMmTelManager#WIFI_MODE_WIFI_PREFERRED}
 * @see #getVoWiFiRoamingModeSetting()
 * @apiSince REL
 */

public void setVoWiFiRoamingModeSetting(int mode) { throw new RuntimeException("Stub!"); }

/**
 * Sets the capability of RTT for IMS calls placed on this subscription.
 *
 * Note: This does not affect the value of
 * {@link android.provider.Settings.Secure#RTT_CALLING_MODE}, which is the global user setting
 * for RTT. That value is enabled/disabled separately by the user through the Accessibility
 * settings.
 * <br>
 * Requires {@link android.Manifest.permission#MODIFY_PHONE_STATE}
 * @param isEnabled if true RTT should be enabled during calls made on this subscription.
 * @apiSince REL
 */

public void setRttCapabilitySetting(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Prefer registering for IMS over LTE if LTE signal quality is high enough.
 * @apiSince REL
 */

public static final int WIFI_MODE_CELLULAR_PREFERRED = 1; // 0x1

/**
 * Register for IMS over IWLAN if WiFi signal quality is high enough. Do not hand over to LTE
 * registration if signal quality degrades.
 * @apiSince REL
 */

public static final int WIFI_MODE_WIFI_ONLY = 0; // 0x0

/**
 * Prefer registering for IMS over IWLAN if possible if WiFi signal quality is high enough.
 * @apiSince REL
 */

public static final int WIFI_MODE_WIFI_PREFERRED = 2; // 0x2
/**
 * Receives IMS capability status updates from the ImsService. This information is also
 * available via the {@link #isAvailable(int, int)} method below.
 *
 * @see #registerMmTelCapabilityCallback(Executor, CapabilityCallback) (CapabilityCallback)
 * @see #unregisterMmTelCapabilityCallback(CapabilityCallback)
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class CapabilityCallback {

public CapabilityCallback() { throw new RuntimeException("Stub!"); }

/**
 * The status of the feature's capabilities has changed to either available or unavailable.
 * If unavailable, the feature is not able to support the unavailable capability at this
 * time.
 *
 * This information can also be queried using the {@link #isAvailable(int, int)} API.
 *
 * @param capabilities The new availability of the capabilities.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onCapabilitiesStatusChanged(@android.annotation.NonNull android.telephony.ims.feature.MmTelFeature.MmTelCapabilities capabilities) { throw new RuntimeException("Stub!"); }
}

/**
 * Callback class for receiving IMS network Registration callback events.
 * @see #registerImsRegistrationCallback(Executor, RegistrationCallback) (RegistrationCallback)
 * @see #unregisterImsRegistrationCallback(RegistrationCallback)
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class RegistrationCallback {

public RegistrationCallback() { throw new RuntimeException("Stub!"); }

/**
 * Notifies the framework when the IMS Provider is registered to the IMS network.
 *
 * @param imsTransportType the radio access technology. Valid values are defined in
 * {@link android.telephony.AccessNetworkConstants.TransportType}.
 * @apiSince REL
 */

public void onRegistered(int imsTransportType) { throw new RuntimeException("Stub!"); }

/**
 * Notifies the framework when the IMS Provider is trying to register the IMS network.
 *
 * @param imsTransportType the radio access technology. Valid values are defined in
 * {@link android.telephony.AccessNetworkConstants.TransportType}.
 * @apiSince REL
 */

public void onRegistering(int imsTransportType) { throw new RuntimeException("Stub!"); }

/**
 * Notifies the framework when the IMS Provider is deregistered from the IMS network.
 *
 * @param info the {@link ImsReasonInfo} associated with why registration was disconnected.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void onUnregistered(@android.annotation.Nullable android.telephony.ims.ImsReasonInfo info) { throw new RuntimeException("Stub!"); }

/**
 * A failure has occurred when trying to handover registration to another technology type,
 * defined in {@link android.telephony.AccessNetworkConstants.TransportType}
 *
 * @param imsTransportType The
 *         {@link android.telephony.AccessNetworkConstants.TransportType}
 *         transport type that has failed to handover registration to.
 * @param info A {@link ImsReasonInfo} that identifies the reason for failure.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void onTechnologyChangeFailed(int imsTransportType, @android.annotation.Nullable android.telephony.ims.ImsReasonInfo info) { throw new RuntimeException("Stub!"); }
}

}

