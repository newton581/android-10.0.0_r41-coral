/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.location;


/**
 * A container with measurement corrections for a single visible satellite
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class GnssSingleSatCorrection implements android.os.Parcelable {

GnssSingleSatCorrection(android.location.GnssSingleSatCorrection.Builder builder) { throw new RuntimeException("Stub!"); }

/**
 * Gets the constellation type.
 *
 * <p>The return value is one of those constants with {@code CONSTELLATION_} prefix in {@link
 * GnssStatus}.
 
 * @return Value is {@link android.location.GnssStatus#CONSTELLATION_UNKNOWN}, {@link android.location.GnssStatus#CONSTELLATION_GPS}, {@link android.location.GnssStatus#CONSTELLATION_SBAS}, {@link android.location.GnssStatus#CONSTELLATION_GLONASS}, {@link android.location.GnssStatus#CONSTELLATION_QZSS}, {@link android.location.GnssStatus#CONSTELLATION_BEIDOU}, {@link android.location.GnssStatus#CONSTELLATION_GALILEO}, or {@link android.location.GnssStatus#CONSTELLATION_IRNSS}
 * @apiSince REL
 */

public int getConstellationType() { throw new RuntimeException("Stub!"); }

/**
 * Gets the satellite ID.
 *
 * <p>Interpretation depends on {@link #getConstellationType()}. See {@link
 * GnssStatus#getSvid(int)}.
 
 * @return Value is 0 or greater
 * @apiSince REL
 */

public int getSatelliteId() { throw new RuntimeException("Stub!"); }

/**
 * Gets the carrier frequency of the tracked signal.
 *
 * <p>For example it can be the GPS central frequency for L1 = 1575.45 MHz, or L2 = 1227.60 MHz,
 * L5 = 1176.45 MHz, varying GLO channels, etc.
 *
 * <p>For an L1, L5 receiver tracking a satellite on L1 and L5 at the same time, two correction
 * objects will be reported for this same satellite, in one of the correction objects, all the
 * values related to L1 will be filled, and in the other all of the values related to L5 will be
 * filled.
 *
 * @return the carrier frequency of the signal tracked in Hz.
 
 * Value is 0.0f or greater
 * @apiSince REL
 */

public float getCarrierFrequencyHz() { throw new RuntimeException("Stub!"); }

/**
 * Returns the probability that the satellite is in line-of-sight condition at the given
 * location.
 
 * @return Value is between 0.0f and 1.0f inclusive
 * @apiSince REL
 */

public float getProbabilityLineOfSight() { throw new RuntimeException("Stub!"); }

/**
 * Returns the Excess path length to be subtracted from pseudorange before using it in
 * calculating location.
 
 * @return Value is 0.0f or greater
 * @apiSince REL
 */

public float getExcessPathLengthMeters() { throw new RuntimeException("Stub!"); }

/**
 * Returns the error estimate (1-sigma) for the Excess path length estimate
 * @return Value is 0.0f or greater
 * @apiSince REL
 */

public float getExcessPathLengthUncertaintyMeters() { throw new RuntimeException("Stub!"); }

/**
 * Returns the reflecting plane characteristics at which the signal has bounced
 *
 * <p>The flag HAS_REFLECTING_PLANE will be used to set this value to invalid if the satellite
 * signal goes through multiple reflections or if reflection plane serving is not supported
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.location.GnssReflectingPlane getReflectingPlane() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if {@link #getProbabilityLineOfSight()} is valid.
 * @apiSince REL
 */

public boolean hasValidSatelliteLineOfSight() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if {@link #getExcessPathLengthMeters()} is valid.
 * @apiSince REL
 */

public boolean hasExcessPathLength() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if {@link #getExcessPathLengthUncertaintyMeters()} is valid.
 * @apiSince REL
 */

public boolean hasExcessPathLengthUncertainty() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if {@link #getReflectingPlane()} is valid.
 * @apiSince REL
 */

public boolean hasReflectingPlane() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param parcel This value must never be {@code null}.
 * @apiSince REL
 */

public void writeToParcel(@android.annotation.NonNull android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final android.os.Parcelable.Creator<android.location.GnssSingleSatCorrection> CREATOR;
static { CREATOR = null; }
/**
 * Builder for {@link GnssSingleSatCorrection}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Sets the constellation type.
 * @param constellationType Value is {@link android.location.GnssStatus#CONSTELLATION_UNKNOWN}, {@link android.location.GnssStatus#CONSTELLATION_GPS}, {@link android.location.GnssStatus#CONSTELLATION_SBAS}, {@link android.location.GnssStatus#CONSTELLATION_GLONASS}, {@link android.location.GnssStatus#CONSTELLATION_QZSS}, {@link android.location.GnssStatus#CONSTELLATION_BEIDOU}, {@link android.location.GnssStatus#CONSTELLATION_GALILEO}, or {@link android.location.GnssStatus#CONSTELLATION_IRNSS}
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssSingleSatCorrection.Builder setConstellationType(int constellationType) { throw new RuntimeException("Stub!"); }

/**
 * Sets the Satellite ID defined in the ICD of the given constellation.
 * @param satId Value is 0 or greater
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssSingleSatCorrection.Builder setSatelliteId(int satId) { throw new RuntimeException("Stub!"); }

/**
 * Sets the Carrier frequency in Hz.
 * @param carrierFrequencyHz Value is 0.0f or greater
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssSingleSatCorrection.Builder setCarrierFrequencyHz(float carrierFrequencyHz) { throw new RuntimeException("Stub!"); }

/**
 * Sets the line-of-sight probability of the satellite at the given location in the range
 * between 0 and 1.
 
 * @param probSatIsLos Value is between 0.0f and 1.0f inclusive
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssSingleSatCorrection.Builder setProbabilityLineOfSight(float probSatIsLos) { throw new RuntimeException("Stub!"); }

/**
 * Sets the Excess path length to be subtracted from pseudorange before using it in
 * calculating location.
 
 * @param excessPathLengthMeters Value is 0.0f or greater
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssSingleSatCorrection.Builder setExcessPathLengthMeters(float excessPathLengthMeters) { throw new RuntimeException("Stub!"); }

/**
 * Sets the error estimate (1-sigma) for the Excess path length estimate
 * @param excessPathLengthUncertaintyMeters Value is 0.0f or greater
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssSingleSatCorrection.Builder setExcessPathLengthUncertaintyMeters(float excessPathLengthUncertaintyMeters) { throw new RuntimeException("Stub!"); }

/**
 * Sets the reflecting plane information
 * @param reflectingPlane This value may be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssSingleSatCorrection.Builder setReflectingPlane(@android.annotation.Nullable android.location.GnssReflectingPlane reflectingPlane) { throw new RuntimeException("Stub!"); }

/**
 * Builds a {@link GnssSingleSatCorrection} instance as specified by this builder.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.location.GnssSingleSatCorrection build() { throw new RuntimeException("Stub!"); }
}

}

