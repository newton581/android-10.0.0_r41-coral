/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.radio;

import android.graphics.Bitmap;
import java.util.Set;

/**
 * Contains meta data about a radio program such as station name, song title, artist etc...
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class RadioMetadata implements android.os.Parcelable {

RadioMetadata() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the given key is contained in the meta data
 *
 * @param key a String key
 * @return {@code true} if the key exists in this meta data, {@code false} otherwise
 * @apiSince REL
 */

public boolean containsKey(java.lang.String key) { throw new RuntimeException("Stub!"); }

/**
 * Returns the text value associated with the given key as a String, or null
 * if the key is not found in the meta data.
 *
 * @param key The key the value is stored under
 * @return a String value, or null
 * @apiSince REL
 */

public java.lang.String getString(java.lang.String key) { throw new RuntimeException("Stub!"); }

/**
 * Returns the value associated with the given key,
 * or 0 if the key is not found in the meta data.
 *
 * @param key The key the value is stored under
 * @return an int value
 * @apiSince REL
 */

public int getInt(java.lang.String key) { throw new RuntimeException("Stub!"); }

/**
 * Returns a {@link Bitmap} for the given key or null if the key is not found in the meta data.
 *
 * @param key The key the value is stored under
 * @return a {@link Bitmap} or null
 * @deprecated Use getBitmapId(String) instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public android.graphics.Bitmap getBitmap(java.lang.String key) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.hardware.radio.RadioMetadata.Clock getClock(java.lang.String key) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Returns the number of fields in this meta data.
 *
 * @return the number of fields in the meta data.
 * @apiSince REL
 */

public int size() { throw new RuntimeException("Stub!"); }

/**
 * Returns a Set containing the Strings used as keys in this meta data.
 *
 * @return a Set of String keys
 * @apiSince REL
 */

public java.util.Set<java.lang.String> keySet() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.RadioMetadata> CREATOR;
static { CREATOR = null; }

/**
 * The album name.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_ALBUM = "android.hardware.radio.metadata.ALBUM";

/**
 * The artwork for the song/album {@link Bitmap}.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_ART = "android.hardware.radio.metadata.ART";

/**
 * The artist name.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_ARTIST = "android.hardware.radio.metadata.ARTIST";

/**
 * The clock.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_CLOCK = "android.hardware.radio.metadata.CLOCK";

/**
 * DAB component name.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_DAB_COMPONENT_NAME = "android.hardware.radio.metadata.DAB_COMPONENT_NAME";

/**
 * DAB component name.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_DAB_COMPONENT_NAME_SHORT = "android.hardware.radio.metadata.DAB_COMPONENT_NAME_SHORT";

/**
 * DAB ensemble name.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_DAB_ENSEMBLE_NAME = "android.hardware.radio.metadata.DAB_ENSEMBLE_NAME";

/**
 * DAB ensemble name - short version (up to 8 characters).
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_DAB_ENSEMBLE_NAME_SHORT = "android.hardware.radio.metadata.DAB_ENSEMBLE_NAME_SHORT";

/**
 * DAB service name.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_DAB_SERVICE_NAME = "android.hardware.radio.metadata.DAB_SERVICE_NAME";

/**
 * DAB service name - short version (up to 8 characters).
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_DAB_SERVICE_NAME_SHORT = "android.hardware.radio.metadata.DAB_SERVICE_NAME_SHORT";

/**
 * The music genre.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_GENRE = "android.hardware.radio.metadata.GENRE";

/**
 * The radio station icon {@link Bitmap}.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_ICON = "android.hardware.radio.metadata.ICON";

/**
 * Technology-independent program name (station name).
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_PROGRAM_NAME = "android.hardware.radio.metadata.PROGRAM_NAME";

/**
 * The RBDS PTY.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_RBDS_PTY = "android.hardware.radio.metadata.RBDS_PTY";

/**
 * The RDS Program Information.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_RDS_PI = "android.hardware.radio.metadata.RDS_PI";

/**
 * The RDS Program Service.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_RDS_PS = "android.hardware.radio.metadata.RDS_PS";

/**
 * The RDS PTY.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_RDS_PTY = "android.hardware.radio.metadata.RDS_PTY";

/**
 * The RBDS Radio Text.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_RDS_RT = "android.hardware.radio.metadata.RDS_RT";

/**
 * The song title.
 * @apiSince REL
 */

public static final java.lang.String METADATA_KEY_TITLE = "android.hardware.radio.metadata.TITLE";
/**
 * Use to build RadioMetadata objects.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Create an empty Builder. Any field that should be included in the
 * {@link RadioMetadata} must be added.
 * @apiSince REL
 */

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Create a Builder using a {@link RadioMetadata} instance to set the
 * initial values. All fields in the source meta data will be included in
 * the new meta data. Fields can be overwritten by adding the same key.
 *
 * @param source
 * @apiSince REL
 */

public Builder(android.hardware.radio.RadioMetadata source) { throw new RuntimeException("Stub!"); }

/**
 * Put a String value into the meta data. Custom keys may be used, but if
 * the METADATA_KEYs defined in this class are used they may only be one
 * of the following:
 * <ul>
 * <li>{@link #METADATA_KEY_RDS_PS}</li>
 * <li>{@link #METADATA_KEY_RDS_RT}</li>
 * <li>{@link #METADATA_KEY_TITLE}</li>
 * <li>{@link #METADATA_KEY_ARTIST}</li>
 * <li>{@link #METADATA_KEY_ALBUM}</li>
 * <li>{@link #METADATA_KEY_GENRE}</li>
 * </ul>
 *
 * @param key The key for referencing this value
 * @param value The String value to store
 * @return the same Builder instance
 * @apiSince REL
 */

public android.hardware.radio.RadioMetadata.Builder putString(java.lang.String key, java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Put an int value into the meta data. Custom keys may be used, but if
 * the METADATA_KEYs defined in this class are used they may only be one
 * of the following:
 * <ul>
 * <li>{@link #METADATA_KEY_RDS_PI}</li>
 * <li>{@link #METADATA_KEY_RDS_PTY}</li>
 * <li>{@link #METADATA_KEY_RBDS_PTY}</li>
 * </ul>
 * or any bitmap represented by its identifier.
 *
 * @param key The key for referencing this value
 * @param value The int value to store
 * @return the same Builder instance
 * @apiSince REL
 */

public android.hardware.radio.RadioMetadata.Builder putInt(java.lang.String key, int value) { throw new RuntimeException("Stub!"); }

/**
 * Put a {@link Bitmap} into the meta data. Custom keys may be used, but
 * if the METADATA_KEYs defined in this class are used they may only be
 * one of the following:
 * <ul>
 * <li>{@link #METADATA_KEY_ICON}</li>
 * <li>{@link #METADATA_KEY_ART}</li>
 * </ul>
 * <p>
 *
 * @param key The key for referencing this value
 * @param value The Bitmap to store
 * @return the same Builder instance
 * @apiSince REL
 */

public android.hardware.radio.RadioMetadata.Builder putBitmap(java.lang.String key, android.graphics.Bitmap value) { throw new RuntimeException("Stub!"); }

/**
 * Put a {@link RadioMetadata.Clock} into the meta data. Custom keys may be used, but if the
 * METADATA_KEYs defined in this class are used they may only be one of the following:
 * <ul>
 * <li>{@link #MEADATA_KEY_CLOCK}</li>
 * </ul>
 *
 * @param utcSecondsSinceEpoch Number of seconds since epoch for UTC + 0 timezone.
 * @param timezoneOffsetInMinutes Offset of timezone from UTC + 0 in minutes.
 * @return the same Builder instance.
 * @apiSince REL
 */

public android.hardware.radio.RadioMetadata.Builder putClock(java.lang.String key, long utcSecondsSinceEpoch, int timezoneOffsetMinutes) { throw new RuntimeException("Stub!"); }

/**
 * Creates a {@link RadioMetadata} instance with the specified fields.
 *
 * @return a new {@link RadioMetadata} object
 * @apiSince REL
 */

public android.hardware.radio.RadioMetadata build() { throw new RuntimeException("Stub!"); }
}

/**
 * Provides a Clock that can be used to describe time as provided by the Radio.
 *
 * The clock is defined by the seconds since epoch at the UTC + 0 timezone
 * and timezone offset from UTC + 0 represented in number of minutes.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Clock implements android.os.Parcelable {

/** @apiSince REL */

public Clock(long utcEpochSeconds, int timezoneOffsetMinutes) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long getUtcEpochSeconds() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getTimezoneOffsetMinutes() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.RadioMetadata.Clock> CREATOR;
static { CREATOR = null; }
}

}

