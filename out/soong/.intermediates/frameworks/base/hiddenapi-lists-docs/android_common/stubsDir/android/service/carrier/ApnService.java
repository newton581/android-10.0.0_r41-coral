/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.carrier;

import android.content.ContentValues;

/**
 * A service that the system can call to restore default APNs.
 * <p>
 * To extend this class, specify the full name of your implementation in the resource file
 * {@code packages/providers/TelephonyProvider/res/values/config.xml} as the
 * {@code apn_source_service}.
 * </p>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ApnService extends android.app.Service {

public ApnService() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 
 * @param intent This value may be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.os.IBinder onBind(@android.annotation.Nullable android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Override this method to restore default user APNs with a carrier service instead of the
 * built in platform xml APNs list.
 * <p>
 * This method is called by the TelephonyProvider when the user requests restoring the default
 * APNs. It should return a list of ContentValues representing the default APNs for the given
 * subId.
 
 * <br>
 * This method may take several seconds to complete, so it should
 * only be called from a worker thread.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public abstract java.util.List<android.content.ContentValues> onRestoreApns(int subId);
}

