/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.telephony;

import android.content.pm.Signature;

/**
 * Describes a single UICC access rule according to the GlobalPlatform Secure Element Access Control
 * specification.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class UiccAccessRule implements android.os.Parcelable {

/**
 * @param packageName This value may be {@code null}.
 * @apiSince REL
 */

public UiccAccessRule(byte[] certificateHash, @android.annotation.Nullable java.lang.String packageName, long accessType) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return the package name this rule applies to.
 *
 * @return the package name, or null if this rule applies to any package signed with the given
 *     certificate.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the hex string of the certificate hash.
 * @apiSince REL
 */

public java.lang.String getCertificateHexString() { throw new RuntimeException("Stub!"); }

/**
 * Returns the carrier privilege status associated with the given package.
 *
 * @param packageInfo package info fetched from
 *     {@link android.content.pm.PackageManager#getPackageInfo}.
 *     {@link android.content.pm.PackageManager#GET_SIGNING_CERTIFICATES} must have been
 *         passed in.
 * @return either {@link TelephonyManager#CARRIER_PRIVILEGE_STATUS_HAS_ACCESS} or
 *     {@link TelephonyManager#CARRIER_PRIVILEGE_STATUS_NO_ACCESS}.
 * @apiSince REL
 */

public int getCarrierPrivilegeStatus(android.content.pm.PackageInfo packageInfo) { throw new RuntimeException("Stub!"); }

/**
 * Returns the carrier privilege status for the given certificate and package name.
 *
 * @param signature The signature of the certificate.
 * @param packageName name of the package.
 * @return either {@link TelephonyManager#CARRIER_PRIVILEGE_STATUS_HAS_ACCESS} or
 *     {@link TelephonyManager#CARRIER_PRIVILEGE_STATUS_NO_ACCESS}.
 * @apiSince REL
 */

public int getCarrierPrivilegeStatus(android.content.pm.Signature signature, java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.UiccAccessRule> CREATOR;
static { CREATOR = null; }
}

