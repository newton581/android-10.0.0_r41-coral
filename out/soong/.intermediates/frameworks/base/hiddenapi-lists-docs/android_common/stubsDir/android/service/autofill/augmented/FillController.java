/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill.augmented;


/**
 * Object used to interact with the autofill system.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class FillController {

FillController() { throw new RuntimeException("Stub!"); }

/**
 * Fills the activity with the provided values.
 *
 * <p>As a side effect, the {@link FillWindow} associated with the {@link FillResponse} will be
 * automatically {@link FillWindow#destroy() destroyed}.
 
 * @param values This value must never be {@code null}.
 * @apiSince REL
 */

public void autofill(@android.annotation.NonNull java.util.List<android.util.Pair<android.view.autofill.AutofillId,android.view.autofill.AutofillValue>> values) { throw new RuntimeException("Stub!"); }
}

