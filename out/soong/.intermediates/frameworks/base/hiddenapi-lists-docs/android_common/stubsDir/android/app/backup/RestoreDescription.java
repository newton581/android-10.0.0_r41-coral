/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.backup;


/**
 * Description of the available restore data for a given package.  Returned by a
 * BackupTransport in response to a request about the next available restorable
 * package.
 *
 * @see BackupTransport#nextRestorePackage()
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class RestoreDescription implements android.os.Parcelable {

/** @apiSince REL */

public RestoreDescription(java.lang.String packageName, int dataType) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getDataType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.backup.RestoreDescription> CREATOR;
static { CREATOR = null; }

/**
 * Return this constant RestoreDescription from BackupTransport.nextRestorePackage()
 * to indicate that no more package data is available in the current restore operation.
 * @apiSince REL
 */

public static final android.app.backup.RestoreDescription NO_MORE_PACKAGES;
static { NO_MORE_PACKAGES = null; }

/**
 * This package's restore data is a tarball-type full data stream
 * @apiSince REL
 */

public static final int TYPE_FULL_STREAM = 2; // 0x2

/**
 * This package's restore data is an original-style key/value dataset
 * @apiSince REL
 */

public static final int TYPE_KEY_VALUE = 1; // 0x1
}

