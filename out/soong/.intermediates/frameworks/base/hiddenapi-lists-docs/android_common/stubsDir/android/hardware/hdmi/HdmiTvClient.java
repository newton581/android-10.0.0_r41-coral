/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.hdmi;

import android.hardware.hdmi.HdmiTimerRecordSources.TimerRecordSource;

/**
 * HdmiTvClient represents HDMI-CEC logical device of type TV in the Android system
 * which acts as TV/Display.
 *
 * <p>HdmiTvClient provides methods that manage, interact with other devices on the CEC bus.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class HdmiTvClient extends android.hardware.hdmi.HdmiClient {

HdmiTvClient() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getDeviceType() { throw new RuntimeException("Stub!"); }

/**
 * Selects a CEC logical device to be a new active source.
 *
 * @param logicalAddress logical address of the device to select
 * @param callback callback to get the result with
 * This value must never be {@code null}.
 * @throws {@link IllegalArgumentException} if the {@code callback} is null
 * @apiSince REL
 */

public void deviceSelect(int logicalAddress, @android.annotation.NonNull android.hardware.hdmi.HdmiTvClient.SelectCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Selects a HDMI port to be a new route path.
 *
 * @param portId HDMI port to select
 * @param callback callback to get the result with
 * This value must never be {@code null}.
 * @throws {@link IllegalArgumentException} if the {@code callback} is null
 * @apiSince REL
 */

public void portSelect(int portId, @android.annotation.NonNull android.hardware.hdmi.HdmiTvClient.SelectCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Sets the listener used to get informed of the input change event.
 *
 * @param listener listener object
 * @apiSince REL
 */

public void setInputChangeListener(android.hardware.hdmi.HdmiTvClient.InputChangeListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Returns all the CEC devices connected to TV.
 *
 * @return list of {@link HdmiDeviceInfo} for connected CEC devices.
 *         Empty list is returned if there is none.
 * @apiSince REL
 */

public java.util.List<android.hardware.hdmi.HdmiDeviceInfo> getDeviceList() { throw new RuntimeException("Stub!"); }

/**
 * Sets system audio mode.
 *
 * @param enabled set to {@code true} to enable the mode; otherwise {@code false}
 * @param callback callback to get the result with
 * @throws {@link IllegalArgumentException} if the {@code callback} is null
 * @apiSince REL
 */

public void setSystemAudioMode(boolean enabled, android.hardware.hdmi.HdmiTvClient.SelectCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Sets system audio volume.
 *
 * @param oldIndex current volume index
 * @param newIndex volume index to be set
 * @param maxIndex maximum volume index
 * @apiSince REL
 */

public void setSystemAudioVolume(int oldIndex, int newIndex, int maxIndex) { throw new RuntimeException("Stub!"); }

/**
 * Sets system audio mute status.
 *
 * @param mute {@code true} if muted; otherwise, {@code false}
 * @apiSince REL
 */

public void setSystemAudioMute(boolean mute) { throw new RuntimeException("Stub!"); }

/**
 * Sets record listener.
 *
 * @param listener
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void setRecordListener(@android.annotation.NonNull android.hardware.hdmi.HdmiRecordListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Sends a &lt;Standby&gt; command to other device.
 *
 * @param deviceId device id to send the command to
 * @apiSince REL
 */

public void sendStandby(int deviceId) { throw new RuntimeException("Stub!"); }

/**
 * Starts one touch recording with the given recorder address and recorder source.
 * <p>
 * Usage
 * <pre>
 * HdmiTvClient tvClient = ....;
 * // for own source.
 * OwnSource ownSource = HdmiRecordSources.ofOwnSource();
 * tvClient.startOneTouchRecord(recorderAddress, ownSource);
 * </pre>
 
 * @param source This value must never be {@code null}.
 * @apiSince REL
 */

public void startOneTouchRecord(int recorderAddress, @android.annotation.NonNull android.hardware.hdmi.HdmiRecordSources.RecordSource source) { throw new RuntimeException("Stub!"); }

/**
 * Stops one touch record.
 *
 * @param recorderAddress recorder address where recoding will be stopped
 * @apiSince REL
 */

public void stopOneTouchRecord(int recorderAddress) { throw new RuntimeException("Stub!"); }

/**
 * Starts timer recording with the given recoder address and recorder source.
 * <p>
 * Usage
 * <pre>
 * HdmiTvClient tvClient = ....;
 * // create timer info
 * TimerInfo timerInfo = HdmiTimerRecourdSources.timerInfoOf(...);
 * // for digital source.
 * DigitalServiceSource recordSource = HdmiRecordSources.ofDigitalService(...);
 * // create timer recording source.
 * TimerRecordSource source = HdmiTimerRecourdSources.ofDigitalSource(timerInfo, recordSource);
 * tvClient.startTimerRecording(recorderAddress, source);
 * </pre>
 *
 * @param recorderAddress target recorder address
 * @param sourceType type of record source. It should be one of
 *          {@link HdmiControlManager#TIMER_RECORDING_TYPE_DIGITAL},
 *          {@link HdmiControlManager#TIMER_RECORDING_TYPE_ANALOGUE},
 *          {@link HdmiControlManager#TIMER_RECORDING_TYPE_EXTERNAL}.
 * @param source record source to be used
 * @apiSince REL
 */

public void startTimerRecording(int recorderAddress, int sourceType, android.hardware.hdmi.HdmiTimerRecordSources.TimerRecordSource source) { throw new RuntimeException("Stub!"); }

/**
 * Clears timer recording with the given recorder address and recording source.
 * For more details, please refer {@link #startTimerRecording(int, int, TimerRecordSource)}.
 * @apiSince REL
 */

public void clearTimerRecording(int recorderAddress, int sourceType, android.hardware.hdmi.HdmiTimerRecordSources.TimerRecordSource source) { throw new RuntimeException("Stub!"); }

/**
 * Sets {@link HdmiMhlVendorCommandListener} to get incoming MHL vendor command.
 *
 * @param listener to receive incoming MHL vendor command
 * @apiSince REL
 */

public void setHdmiMhlVendorCommandListener(android.hardware.hdmi.HdmiTvClient.HdmiMhlVendorCommandListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Sends MHL vendor command to the device connected to a port of the given portId.
 *
 * @param portId id of port to send MHL vendor command
 * @param offset offset in the in given data
 * @param length length of data. offset + length should be bound to length of data.
 * @param data container for vendor command data. It should be 16 bytes.
 * @throws IllegalArgumentException if the given parameters are invalid
 * @apiSince REL
 */

public void sendMhlVendorCommand(int portId, int offset, int length, byte[] data) { throw new RuntimeException("Stub!"); }

/**
 * Size of MHL register for vendor command
 * @apiSince REL
 */

public static final int VENDOR_DATA_SIZE = 16; // 0x10
/**
 * Interface used to get incoming MHL vendor command.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface HdmiMhlVendorCommandListener {

/** @apiSince REL */

public void onReceived(int portId, int offset, int length, byte[] data);
}

/**
 * Callback interface used to get the input change event.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface InputChangeListener {

/**
 * Called when the input was changed.
 *
 * @param info newly selected HDMI input
 * @apiSince REL
 */

public void onChanged(android.hardware.hdmi.HdmiDeviceInfo info);
}

/**
 * Callback interface used to get the result of {@link #deviceSelect}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface SelectCallback {

/**
 * Called when the operation is finished.
 *
 * @param result the result value of {@link #deviceSelect}
 * @apiSince REL
 */

public void onComplete(int result);
}

}

