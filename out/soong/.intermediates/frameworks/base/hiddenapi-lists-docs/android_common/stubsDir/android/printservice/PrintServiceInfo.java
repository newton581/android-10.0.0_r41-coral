/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.printservice;

import android.content.Context;
import android.content.pm.ResolveInfo;

/**
 * This class describes a {@link PrintService}. A print service knows
 * how to communicate with one or more printers over one or more protocols
 * and exposes printers for use by the applications via the platform print
 * APIs.
 *
 * @see PrintService
 * @see android.print.PrintManager
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class PrintServiceInfo implements android.os.Parcelable {

/**
 * Creates a new instance.
 *
 * @hide
 */

PrintServiceInfo(android.os.Parcel parcel) { throw new RuntimeException("Stub!"); }

/**
 * Return the component name for this print service.
 *
 * @return The component name for this print service.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.content.ComponentName getComponentName() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flagz) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.printservice.PrintServiceInfo> CREATOR;
static { CREATOR = null; }
}

