/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.telephony;


/**
 * Class for the information of a UICC slot.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class UiccSlotInfo implements android.os.Parcelable {

/**
 * Construct a UiccSlotInfo.
 * @deprecated apps should not be constructing UiccSlotInfo objects
 
 * @param cardStateInfo Value is {@link android.telephony.UiccSlotInfo#CARD_STATE_INFO_ABSENT}, {@link android.telephony.UiccSlotInfo#CARD_STATE_INFO_PRESENT}, {@link android.telephony.UiccSlotInfo#CARD_STATE_INFO_ERROR}, or {@link android.telephony.UiccSlotInfo#CARD_STATE_INFO_RESTRICTED}
 * @apiSince REL
 */

@Deprecated
public UiccSlotInfo(boolean isActive, boolean isEuicc, java.lang.String cardId, int cardStateInfo, int logicalSlotIdx, boolean isExtendedApduSupported) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean getIsActive() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean getIsEuicc() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String getCardId() { throw new RuntimeException("Stub!"); }

/**
 * @return Value is {@link android.telephony.UiccSlotInfo#CARD_STATE_INFO_ABSENT}, {@link android.telephony.UiccSlotInfo#CARD_STATE_INFO_PRESENT}, {@link android.telephony.UiccSlotInfo#CARD_STATE_INFO_ERROR}, or {@link android.telephony.UiccSlotInfo#CARD_STATE_INFO_RESTRICTED}
 * @apiSince REL
 */

public int getCardStateInfo() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getLogicalSlotIdx() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if this slot supports extended APDU from ATR, {@code false} otherwise.
 * @apiSince REL
 */

public boolean getIsExtendedApduSupported() { throw new RuntimeException("Stub!"); }

/**
 * Return whether the UICC slot is for a removable UICC.
 * <p>
 * UICCs are generally removable, but eUICCs may be removable or built in to the device.
 * @return true if the slot is for removable UICCs
 * @apiSince REL
 */

public boolean isRemovable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Card state absent.
 * @apiSince REL
 */

public static final int CARD_STATE_INFO_ABSENT = 1; // 0x1

/**
 * Card state error.
 * @apiSince REL
 */

public static final int CARD_STATE_INFO_ERROR = 3; // 0x3

/**
 * Card state present.
 * @apiSince REL
 */

public static final int CARD_STATE_INFO_PRESENT = 2; // 0x2

/**
 * Card state restricted.
 * @apiSince REL
 */

public static final int CARD_STATE_INFO_RESTRICTED = 4; // 0x4

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.UiccSlotInfo> CREATOR;
static { CREATOR = null; }
}

