/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill.augmented;


/**
 * Response to a {@link FillRequest}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class FillResponse {

FillResponse(@android.annotation.NonNull android.service.autofill.augmented.FillResponse.Builder builder) { throw new RuntimeException("Stub!"); }
/**
 * Builder for {@link FillResponse} objects.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Sets the {@link FillWindow} used to display the Autofill UI.
 *
 * <p>Must be called when the service is handling the request so the Android System can
 * properly synchronize the UI.
 *
 * @param fillWindow This value must never be {@code null}.
 * @return this builder
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.service.autofill.augmented.FillResponse.Builder setFillWindow(@android.annotation.NonNull android.service.autofill.augmented.FillWindow fillWindow) { throw new RuntimeException("Stub!"); }

/**
 * Builds a new {@link FillResponse} instance.
 *
 * @throws IllegalStateException if any of the following conditions occur:
 * <ol>
 *   <li>{@link #build()} was already called.
 *   <li>No call was made to {@link #setFillWindow(FillWindow)} or
 *   {@ling #setIgnoredIds(List<AutofillId>)}.
 * </ol>
 *
 * @return A built response.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.service.autofill.augmented.FillResponse build() { throw new RuntimeException("Stub!"); }
}

}

