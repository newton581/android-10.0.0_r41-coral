/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.metrics;


/**
 * Read platform logs.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MetricsReader {

public MetricsReader() { throw new RuntimeException("Stub!"); }

/**
 * Read the available logs into a new session.
 *
 * The session will contain events starting from the oldest available
 * log on the system up to the most recent at the time of this call.
 *
 * A call to {@link #checkpoint()} will cause the session to contain
 * only events that occured after that call.
 *
 * This call will not return until the system buffer overflows the
 * specified timestamp. If the specified timestamp is 0, then the
 * call will return immediately since any logs 1970 have already been
 * overwritten (n.b. if the underlying system has the capability to
 * store many decades of system logs, this call may fail in
 * interesting ways.)
 *
 * @param horizonMs block until this timestamp is overwritten, 0 for non-blocking read.
 * @apiSince REL
 */

public void read(long horizonMs) { throw new RuntimeException("Stub!"); }

/**
 * Empties the session and causes the next {@link #read(long)} to
 * yeild a session containing only events that occur after this call.
 * @apiSince REL
 */

public void checkpoint() { throw new RuntimeException("Stub!"); }

/**
 * Rewind the session to the beginning of time and replay all available logs.
 * @apiSince REL
 */

public void reset() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean hasNext() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.metrics.LogMaker next() { throw new RuntimeException("Stub!"); }
}

