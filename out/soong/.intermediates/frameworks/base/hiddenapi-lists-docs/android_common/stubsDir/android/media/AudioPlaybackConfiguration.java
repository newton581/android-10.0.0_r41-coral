/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media;


/**
 * The AudioPlaybackConfiguration class collects the information describing an audio playback
 * session.
 * @apiSince 26
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AudioPlaybackConfiguration implements android.os.Parcelable {

/**
 * Never use without initializing parameters afterwards
 */

AudioPlaybackConfiguration(int piid) { throw new RuntimeException("Stub!"); }

/**
 * Return the {@link AudioAttributes} of the corresponding player.
 * @return the audio attributes of the player
 * @apiSince 26
 */

public android.media.AudioAttributes getAudioAttributes() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * Return the uid of the client application that created this player.
 * @return the uid of the client
 */

public int getClientUid() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * Return the pid of the client application that created this player.
 * @return the pid of the client
 */

public int getClientPid() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * Return the type of player linked to this configuration. The return value is one of
 * {@link #PLAYER_TYPE_JAM_AUDIOTRACK}, {@link #PLAYER_TYPE_JAM_MEDIAPLAYER},
 * {@link #PLAYER_TYPE_JAM_SOUNDPOOL}, {@link #PLAYER_TYPE_SLES_AUDIOPLAYER_BUFFERQUEUE},
 * {@link #PLAYER_TYPE_SLES_AUDIOPLAYER_URI_FD}, or {@link #PLAYER_TYPE_UNKNOWN}.
 * <br>Note that player types not exposed in the system API will be represented as
 * {@link #PLAYER_TYPE_UNKNOWN}.
 * @return the type of the player.

 * Value is {@link android.media.AudioPlaybackConfiguration#PLAYER_TYPE_UNKNOWN}, {@link android.media.AudioPlaybackConfiguration#PLAYER_TYPE_JAM_AUDIOTRACK}, {@link android.media.AudioPlaybackConfiguration#PLAYER_TYPE_JAM_MEDIAPLAYER}, {@link android.media.AudioPlaybackConfiguration#PLAYER_TYPE_JAM_SOUNDPOOL}, {@link android.media.AudioPlaybackConfiguration#PLAYER_TYPE_SLES_AUDIOPLAYER_BUFFERQUEUE}, or {@link android.media.AudioPlaybackConfiguration#PLAYER_TYPE_SLES_AUDIOPLAYER_URI_FD}
 */

public int getPlayerType() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * Return the current state of the player linked to this configuration. The return value is one
 * of {@link #PLAYER_STATE_IDLE}, {@link #PLAYER_STATE_PAUSED}, {@link #PLAYER_STATE_STARTED},
 * {@link #PLAYER_STATE_STOPPED}, {@link #PLAYER_STATE_RELEASED} or
 * {@link #PLAYER_STATE_UNKNOWN}.
 * @return the state of the player.

 * Value is {@link android.media.AudioPlaybackConfiguration#PLAYER_STATE_UNKNOWN}, {@link android.media.AudioPlaybackConfiguration#PLAYER_STATE_RELEASED}, {@link android.media.AudioPlaybackConfiguration#PLAYER_STATE_IDLE}, {@link android.media.AudioPlaybackConfiguration#PLAYER_STATE_STARTED}, {@link android.media.AudioPlaybackConfiguration#PLAYER_STATE_PAUSED}, or {@link android.media.AudioPlaybackConfiguration#PLAYER_STATE_STOPPED}
 */

public int getPlayerState() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * Return an identifier unique for the lifetime of the player.
 * @return a player interface identifier
 */

public int getPlayerInterfaceId() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * Return a proxy for the player associated with this playback configuration
 * @return a proxy player
 */

public android.media.PlayerProxy getPlayerProxy() { throw new RuntimeException("Stub!"); }

/** @apiSince 26 */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince 26 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince 26 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince 26 */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince 26 */

@androidx.annotation.RecentlyNonNull public static final android.os.Parcelable.Creator<android.media.AudioPlaybackConfiguration> CREATOR;
static { CREATOR = null; }

/**
 * @hide
 * The state of a player when it's created
 */

public static final int PLAYER_STATE_IDLE = 1; // 0x1

/**
 * @hide
 * The state of a player where playback is paused
 */

public static final int PLAYER_STATE_PAUSED = 3; // 0x3

/**
 * @hide
 * The resources of the player have been released, it cannot play anymore
 */

public static final int PLAYER_STATE_RELEASED = 0; // 0x0

/**
 * @hide
 * The state of a player that is actively playing
 */

public static final int PLAYER_STATE_STARTED = 2; // 0x2

/**
 * @hide
 * The state of a player where playback is stopped
 */

public static final int PLAYER_STATE_STOPPED = 4; // 0x4

/**
 * @hide
 * An unknown player state
 */

public static final int PLAYER_STATE_UNKNOWN = -1; // 0xffffffff

/**
 * @hide
 * Player backed by a java android.media.AudioTrack player
 */

public static final int PLAYER_TYPE_JAM_AUDIOTRACK = 1; // 0x1

/**
 * @hide
 * Player backed by a java android.media.MediaPlayer player
 */

public static final int PLAYER_TYPE_JAM_MEDIAPLAYER = 2; // 0x2

/**
 * @hide
 * Player backed by a java android.media.SoundPool player
 */

public static final int PLAYER_TYPE_JAM_SOUNDPOOL = 3; // 0x3

/**
 * @hide
 * Player backed by a C OpenSL ES AudioPlayer player with a BufferQueue source
 */

public static final int PLAYER_TYPE_SLES_AUDIOPLAYER_BUFFERQUEUE = 11; // 0xb

/**
 * @hide
 * Player backed by a C OpenSL ES AudioPlayer player with a URI or FD source
 */

public static final int PLAYER_TYPE_SLES_AUDIOPLAYER_URI_FD = 12; // 0xc

/**
 * @hide
 * An unknown type of player
 */

public static final int PLAYER_TYPE_UNKNOWN = -1; // 0xffffffff
}

