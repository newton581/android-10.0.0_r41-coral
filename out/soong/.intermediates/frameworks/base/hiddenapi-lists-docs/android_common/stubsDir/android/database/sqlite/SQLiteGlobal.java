/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.database.sqlite;


/**
 * Provides access to SQLite functions that affect all database connection,
 * such as memory management.
 *
 * The native code associated with SQLiteGlobal is also sets global configuration options
 * using sqlite3_config() then calls sqlite3_initialize() to ensure that the SQLite
 * library is properly initialized exactly once before any other framework or application
 * code has a chance to run.
 *
 * Verbose SQLite logging is enabled if the "log.tag.SQLiteLog" property is set to "V".
 * (per {@link SQLiteDebug#DEBUG_SQL_LOG}).
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class SQLiteGlobal {

SQLiteGlobal() { throw new RuntimeException("Stub!"); }

/**
 * Attempts to release memory by pruning the SQLite page cache and other
 * internal data structures.
 *
 * @return The number of bytes that were freed.
 * @apiSince REL
 */

public static int releaseMemory() { throw new RuntimeException("Stub!"); }

/**
 * Gets the default page size to use when creating a database.
 * @apiSince REL
 */

public static int getDefaultPageSize() { throw new RuntimeException("Stub!"); }

/**
 * Gets the default journal mode when WAL is not in use.
 * @apiSince REL
 */

public static java.lang.String getDefaultJournalMode() { throw new RuntimeException("Stub!"); }

/**
 * Gets the journal size limit in bytes.
 * @apiSince REL
 */

public static int getJournalSizeLimit() { throw new RuntimeException("Stub!"); }

/**
 * Gets the default database synchronization mode when WAL is not in use.
 * @apiSince REL
 */

public static java.lang.String getDefaultSyncMode() { throw new RuntimeException("Stub!"); }

/**
 * Gets the database synchronization mode when in WAL mode.
 * @apiSince REL
 */

public static java.lang.String getWALSyncMode() { throw new RuntimeException("Stub!"); }

/**
 * Gets the WAL auto-checkpoint integer in database pages.
 * @apiSince REL
 */

public static int getWALAutoCheckpoint() { throw new RuntimeException("Stub!"); }

/**
 * Gets the connection pool size when in WAL mode.
 * @apiSince REL
 */

public static int getWALConnectionPoolSize() { throw new RuntimeException("Stub!"); }

/**
 * The default number of milliseconds that SQLite connection is allowed to be idle before it
 * is closed and removed from the pool.
 * @apiSince REL
 */

public static int getIdleConnectionTimeout() { throw new RuntimeException("Stub!"); }
}

