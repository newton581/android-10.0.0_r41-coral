/*
 * Copyright (C) 2009 Qualcomm Innovation Center, Inc.  All Rights Reserved.
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony.data;

import android.telephony.DataFailCause;

/**
 * Description of the response of a setup data call connection request.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class DataCallResponse implements android.os.Parcelable {

/** @hide */

DataCallResponse(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/**
 * @return Data call fail cause. {@link DataFailCause#NONE} indicates no error.
 
 * Value is {@link android.telephony.DataFailCause#NONE}, {@link android.telephony.DataFailCause#OPERATOR_BARRED}, {@link android.telephony.DataFailCause#NAS_SIGNALLING}, {@link android.telephony.DataFailCause#LLC_SNDCP}, {@link android.telephony.DataFailCause#INSUFFICIENT_RESOURCES}, {@link android.telephony.DataFailCause#MISSING_UNKNOWN_APN}, {@link android.telephony.DataFailCause#UNKNOWN_PDP_ADDRESS_TYPE}, {@link android.telephony.DataFailCause#USER_AUTHENTICATION}, {@link android.telephony.DataFailCause#ACTIVATION_REJECT_GGSN}, {@link android.telephony.DataFailCause#ACTIVATION_REJECT_UNSPECIFIED}, {@link android.telephony.DataFailCause#SERVICE_OPTION_NOT_SUPPORTED}, {@link android.telephony.DataFailCause#SERVICE_OPTION_NOT_SUBSCRIBED}, {@link android.telephony.DataFailCause#SERVICE_OPTION_OUT_OF_ORDER}, {@link android.telephony.DataFailCause#NSAPI_IN_USE}, {@link android.telephony.DataFailCause#REGULAR_DEACTIVATION}, {@link android.telephony.DataFailCause#QOS_NOT_ACCEPTED}, {@link android.telephony.DataFailCause#NETWORK_FAILURE}, {@link android.telephony.DataFailCause#UMTS_REACTIVATION_REQ}, {@link android.telephony.DataFailCause#FEATURE_NOT_SUPP}, {@link android.telephony.DataFailCause#TFT_SEMANTIC_ERROR}, {@link android.telephony.DataFailCause#TFT_SYTAX_ERROR}, {@link android.telephony.DataFailCause#UNKNOWN_PDP_CONTEXT}, {@link android.telephony.DataFailCause#FILTER_SEMANTIC_ERROR}, {@link android.telephony.DataFailCause#FILTER_SYTAX_ERROR}, {@link android.telephony.DataFailCause#PDP_WITHOUT_ACTIVE_TFT}, {@link android.telephony.DataFailCause#ACTIVATION_REJECTED_BCM_VIOLATION}, {@link android.telephony.DataFailCause#ONLY_IPV4_ALLOWED}, {@link android.telephony.DataFailCause#ONLY_IPV6_ALLOWED}, {@link android.telephony.DataFailCause#ONLY_SINGLE_BEARER_ALLOWED}, {@link android.telephony.DataFailCause#ESM_INFO_NOT_RECEIVED}, {@link android.telephony.DataFailCause#PDN_CONN_DOES_NOT_EXIST}, {@link android.telephony.DataFailCause#MULTI_CONN_TO_SAME_PDN_NOT_ALLOWED}, {@link android.telephony.DataFailCause#COLLISION_WITH_NETWORK_INITIATED_REQUEST}, {@link android.telephony.DataFailCause#ONLY_IPV4V6_ALLOWED}, {@link android.telephony.DataFailCause#ONLY_NON_IP_ALLOWED}, {@link android.telephony.DataFailCause#UNSUPPORTED_QCI_VALUE}, {@link android.telephony.DataFailCause#BEARER_HANDLING_NOT_SUPPORTED}, {@link android.telephony.DataFailCause#ACTIVE_PDP_CONTEXT_MAX_NUMBER_REACHED}, {@link android.telephony.DataFailCause#UNSUPPORTED_APN_IN_CURRENT_PLMN}, {@link android.telephony.DataFailCause#INVALID_TRANSACTION_ID}, {@link android.telephony.DataFailCause#MESSAGE_INCORRECT_SEMANTIC}, {@link android.telephony.DataFailCause#INVALID_MANDATORY_INFO}, {@link android.telephony.DataFailCause#MESSAGE_TYPE_UNSUPPORTED}, {@link android.telephony.DataFailCause#MSG_TYPE_NONCOMPATIBLE_STATE}, {@link android.telephony.DataFailCause#UNKNOWN_INFO_ELEMENT}, {@link android.telephony.DataFailCause#CONDITIONAL_IE_ERROR}, {@link android.telephony.DataFailCause#MSG_AND_PROTOCOL_STATE_UNCOMPATIBLE}, {@link android.telephony.DataFailCause#PROTOCOL_ERRORS}, {@link android.telephony.DataFailCause#APN_TYPE_CONFLICT}, {@link android.telephony.DataFailCause#INVALID_PCSCF_ADDR}, {@link android.telephony.DataFailCause#INTERNAL_CALL_PREEMPT_BY_HIGH_PRIO_APN}, {@link android.telephony.DataFailCause#EMM_ACCESS_BARRED}, {@link android.telephony.DataFailCause#EMERGENCY_IFACE_ONLY}, {@link android.telephony.DataFailCause#IFACE_MISMATCH}, {@link android.telephony.DataFailCause#COMPANION_IFACE_IN_USE}, {@link android.telephony.DataFailCause#IP_ADDRESS_MISMATCH}, {@link android.telephony.DataFailCause#IFACE_AND_POL_FAMILY_MISMATCH}, {@link android.telephony.DataFailCause#EMM_ACCESS_BARRED_INFINITE_RETRY}, {@link android.telephony.DataFailCause#AUTH_FAILURE_ON_EMERGENCY_CALL}, {@link android.telephony.DataFailCause#INVALID_DNS_ADDR}, {@link android.telephony.DataFailCause#INVALID_PCSCF_OR_DNS_ADDRESS}, {@link android.telephony.DataFailCause#CALL_PREEMPT_BY_EMERGENCY_APN}, {@link android.telephony.DataFailCause#UE_INITIATED_DETACH_OR_DISCONNECT}, {@link android.telephony.DataFailCause#MIP_FA_REASON_UNSPECIFIED}, {@link android.telephony.DataFailCause#MIP_FA_ADMIN_PROHIBITED}, {@link android.telephony.DataFailCause#MIP_FA_INSUFFICIENT_RESOURCES}, {@link android.telephony.DataFailCause#MIP_FA_MOBILE_NODE_AUTHENTICATION_FAILURE}, {@link android.telephony.DataFailCause#MIP_FA_HOME_AGENT_AUTHENTICATION_FAILURE}, {@link android.telephony.DataFailCause#MIP_FA_REQUESTED_LIFETIME_TOO_LONG}, {@link android.telephony.DataFailCause#MIP_FA_MALFORMED_REQUEST}, {@link android.telephony.DataFailCause#MIP_FA_MALFORMED_REPLY}, {@link android.telephony.DataFailCause#MIP_FA_ENCAPSULATION_UNAVAILABLE}, {@link android.telephony.DataFailCause#MIP_FA_VJ_HEADER_COMPRESSION_UNAVAILABLE}, {@link android.telephony.DataFailCause#MIP_FA_REVERSE_TUNNEL_UNAVAILABLE}, {@link android.telephony.DataFailCause#MIP_FA_REVERSE_TUNNEL_IS_MANDATORY}, {@link android.telephony.DataFailCause#MIP_FA_DELIVERY_STYLE_NOT_SUPPORTED}, {@link android.telephony.DataFailCause#MIP_FA_MISSING_NAI}, {@link android.telephony.DataFailCause#MIP_FA_MISSING_HOME_AGENT}, {@link android.telephony.DataFailCause#MIP_FA_MISSING_HOME_ADDRESS}, {@link android.telephony.DataFailCause#MIP_FA_UNKNOWN_CHALLENGE}, {@link android.telephony.DataFailCause#MIP_FA_MISSING_CHALLENGE}, {@link android.telephony.DataFailCause#MIP_FA_STALE_CHALLENGE}, {@link android.telephony.DataFailCause#MIP_HA_REASON_UNSPECIFIED}, {@link android.telephony.DataFailCause#MIP_HA_ADMIN_PROHIBITED}, {@link android.telephony.DataFailCause#MIP_HA_INSUFFICIENT_RESOURCES}, {@link android.telephony.DataFailCause#MIP_HA_MOBILE_NODE_AUTHENTICATION_FAILURE}, {@link android.telephony.DataFailCause#MIP_HA_FOREIGN_AGENT_AUTHENTICATION_FAILURE}, {@link android.telephony.DataFailCause#MIP_HA_REGISTRATION_ID_MISMATCH}, {@link android.telephony.DataFailCause#MIP_HA_MALFORMED_REQUEST}, {@link android.telephony.DataFailCause#MIP_HA_UNKNOWN_HOME_AGENT_ADDRESS}, {@link android.telephony.DataFailCause#MIP_HA_REVERSE_TUNNEL_UNAVAILABLE}, {@link android.telephony.DataFailCause#MIP_HA_REVERSE_TUNNEL_IS_MANDATORY}, {@link android.telephony.DataFailCause#MIP_HA_ENCAPSULATION_UNAVAILABLE}, {@link android.telephony.DataFailCause#CLOSE_IN_PROGRESS}, {@link android.telephony.DataFailCause#NETWORK_INITIATED_TERMINATION}, {@link android.telephony.DataFailCause#MODEM_APP_PREEMPTED}, {@link android.telephony.DataFailCause#PDN_IPV4_CALL_DISALLOWED}, {@link android.telephony.DataFailCause#PDN_IPV4_CALL_THROTTLED}, {@link android.telephony.DataFailCause#PDN_IPV6_CALL_DISALLOWED}, {@link android.telephony.DataFailCause#PDN_IPV6_CALL_THROTTLED}, {@link android.telephony.DataFailCause#MODEM_RESTART}, {@link android.telephony.DataFailCause#PDP_PPP_NOT_SUPPORTED}, {@link android.telephony.DataFailCause#UNPREFERRED_RAT}, {@link android.telephony.DataFailCause#PHYSICAL_LINK_CLOSE_IN_PROGRESS}, {@link android.telephony.DataFailCause#APN_PENDING_HANDOVER}, {@link android.telephony.DataFailCause#PROFILE_BEARER_INCOMPATIBLE}, {@link android.telephony.DataFailCause#SIM_CARD_CHANGED}, {@link android.telephony.DataFailCause#LOW_POWER_MODE_OR_POWERING_DOWN}, {@link android.telephony.DataFailCause#APN_DISABLED}, {@link android.telephony.DataFailCause#MAX_PPP_INACTIVITY_TIMER_EXPIRED}, {@link android.telephony.DataFailCause#IPV6_ADDRESS_TRANSFER_FAILED}, {@link android.telephony.DataFailCause#TRAT_SWAP_FAILED}, {@link android.telephony.DataFailCause#EHRPD_TO_HRPD_FALLBACK}, {@link android.telephony.DataFailCause#MIP_CONFIG_FAILURE}, {@link android.telephony.DataFailCause#PDN_INACTIVITY_TIMER_EXPIRED}, {@link android.telephony.DataFailCause#MAX_IPV4_CONNECTIONS}, {@link android.telephony.DataFailCause#MAX_IPV6_CONNECTIONS}, {@link android.telephony.DataFailCause#APN_MISMATCH}, {@link android.telephony.DataFailCause#IP_VERSION_MISMATCH}, {@link android.telephony.DataFailCause#DUN_CALL_DISALLOWED}, {@link android.telephony.DataFailCause#INTERNAL_EPC_NONEPC_TRANSITION}, {@link android.telephony.DataFailCause#INTERFACE_IN_USE}, {@link android.telephony.DataFailCause#APN_DISALLOWED_ON_ROAMING}, {@link android.telephony.DataFailCause#APN_PARAMETERS_CHANGED}, {@link android.telephony.DataFailCause#NULL_APN_DISALLOWED}, {@link android.telephony.DataFailCause#THERMAL_MITIGATION}, {@link android.telephony.DataFailCause#DATA_SETTINGS_DISABLED}, {@link android.telephony.DataFailCause#DATA_ROAMING_SETTINGS_DISABLED}, {@link android.telephony.DataFailCause#DDS_SWITCHED}, {@link android.telephony.DataFailCause#FORBIDDEN_APN_NAME}, {@link android.telephony.DataFailCause#DDS_SWITCH_IN_PROGRESS}, {@link android.telephony.DataFailCause#CALL_DISALLOWED_IN_ROAMING}, {@link android.telephony.DataFailCause#NON_IP_NOT_SUPPORTED}, {@link android.telephony.DataFailCause#PDN_NON_IP_CALL_THROTTLED}, {@link android.telephony.DataFailCause#PDN_NON_IP_CALL_DISALLOWED}, {@link android.telephony.DataFailCause#CDMA_LOCK}, {@link android.telephony.DataFailCause#CDMA_INTERCEPT}, {@link android.telephony.DataFailCause#CDMA_REORDER}, {@link android.telephony.DataFailCause#CDMA_RELEASE_DUE_TO_SO_REJECTION}, {@link android.telephony.DataFailCause#CDMA_INCOMING_CALL}, {@link android.telephony.DataFailCause#CDMA_ALERT_STOP}, {@link android.telephony.DataFailCause#CHANNEL_ACQUISITION_FAILURE}, {@link android.telephony.DataFailCause#MAX_ACCESS_PROBE}, {@link android.telephony.DataFailCause#CONCURRENT_SERVICE_NOT_SUPPORTED_BY_BASE_STATION}, {@link android.telephony.DataFailCause#NO_RESPONSE_FROM_BASE_STATION}, {@link android.telephony.DataFailCause#REJECTED_BY_BASE_STATION}, {@link android.telephony.DataFailCause#CONCURRENT_SERVICES_INCOMPATIBLE}, {@link android.telephony.DataFailCause#NO_CDMA_SERVICE}, {@link android.telephony.DataFailCause#RUIM_NOT_PRESENT}, {@link android.telephony.DataFailCause#CDMA_RETRY_ORDER}, {@link android.telephony.DataFailCause#ACCESS_BLOCK}, {@link android.telephony.DataFailCause#ACCESS_BLOCK_ALL}, {@link android.telephony.DataFailCause#IS707B_MAX_ACCESS_PROBES}, {@link android.telephony.DataFailCause#THERMAL_EMERGENCY}, {@link android.telephony.DataFailCause#CONCURRENT_SERVICES_NOT_ALLOWED}, {@link android.telephony.DataFailCause#INCOMING_CALL_REJECTED}, {@link android.telephony.DataFailCause#NO_SERVICE_ON_GATEWAY}, {@link android.telephony.DataFailCause#NO_GPRS_CONTEXT}, {@link android.telephony.DataFailCause#ILLEGAL_MS}, {@link android.telephony.DataFailCause#ILLEGAL_ME}, {@link android.telephony.DataFailCause#GPRS_SERVICES_AND_NON_GPRS_SERVICES_NOT_ALLOWED}, {@link android.telephony.DataFailCause#GPRS_SERVICES_NOT_ALLOWED}, {@link android.telephony.DataFailCause#MS_IDENTITY_CANNOT_BE_DERIVED_BY_THE_NETWORK}, {@link android.telephony.DataFailCause#IMPLICITLY_DETACHED}, {@link android.telephony.DataFailCause#PLMN_NOT_ALLOWED}, {@link android.telephony.DataFailCause#LOCATION_AREA_NOT_ALLOWED}, {@link android.telephony.DataFailCause#GPRS_SERVICES_NOT_ALLOWED_IN_THIS_PLMN}, {@link android.telephony.DataFailCause#PDP_DUPLICATE}, {@link android.telephony.DataFailCause#UE_RAT_CHANGE}, {@link android.telephony.DataFailCause#CONGESTION}, {@link android.telephony.DataFailCause#NO_PDP_CONTEXT_ACTIVATED}, {@link android.telephony.DataFailCause#ACCESS_CLASS_DSAC_REJECTION}, {@link android.telephony.DataFailCause#PDP_ACTIVATE_MAX_RETRY_FAILED}, {@link android.telephony.DataFailCause#RADIO_ACCESS_BEARER_FAILURE}, {@link android.telephony.DataFailCause#ESM_UNKNOWN_EPS_BEARER_CONTEXT}, {@link android.telephony.DataFailCause#DRB_RELEASED_BY_RRC}, {@link android.telephony.DataFailCause#CONNECTION_RELEASED}, {@link android.telephony.DataFailCause#EMM_DETACHED}, {@link android.telephony.DataFailCause#EMM_ATTACH_FAILED}, {@link android.telephony.DataFailCause#EMM_ATTACH_STARTED}, {@link android.telephony.DataFailCause#LTE_NAS_SERVICE_REQUEST_FAILED}, {@link android.telephony.DataFailCause#DUPLICATE_BEARER_ID}, {@link android.telephony.DataFailCause#ESM_COLLISION_SCENARIOS}, {@link android.telephony.DataFailCause#ESM_BEARER_DEACTIVATED_TO_SYNC_WITH_NETWORK}, {@link android.telephony.DataFailCause#ESM_NW_ACTIVATED_DED_BEARER_WITH_ID_OF_DEF_BEARER}, {@link android.telephony.DataFailCause#ESM_BAD_OTA_MESSAGE}, {@link android.telephony.DataFailCause#ESM_DOWNLOAD_SERVER_REJECTED_THE_CALL}, {@link android.telephony.DataFailCause#ESM_CONTEXT_TRANSFERRED_DUE_TO_IRAT}, {@link android.telephony.DataFailCause#DS_EXPLICIT_DEACTIVATION}, {@link android.telephony.DataFailCause#ESM_LOCAL_CAUSE_NONE}, {@link android.telephony.DataFailCause#LTE_THROTTLING_NOT_REQUIRED}, {@link android.telephony.DataFailCause#ACCESS_CONTROL_LIST_CHECK_FAILURE}, {@link android.telephony.DataFailCause#SERVICE_NOT_ALLOWED_ON_PLMN}, {@link android.telephony.DataFailCause#EMM_T3417_EXPIRED}, {@link android.telephony.DataFailCause#EMM_T3417_EXT_EXPIRED}, {@link android.telephony.DataFailCause#RRC_UPLINK_DATA_TRANSMISSION_FAILURE}, {@link android.telephony.DataFailCause#RRC_UPLINK_DELIVERY_FAILED_DUE_TO_HANDOVER}, {@link android.telephony.DataFailCause#RRC_UPLINK_CONNECTION_RELEASE}, {@link android.telephony.DataFailCause#RRC_UPLINK_RADIO_LINK_FAILURE}, {@link android.telephony.DataFailCause#RRC_UPLINK_ERROR_REQUEST_FROM_NAS}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ACCESS_STRATUM_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ANOTHER_PROCEDURE_IN_PROGRESS}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ACCESS_BARRED}, {@link android.telephony.DataFailCause#RRC_CONNECTION_CELL_RESELECTION}, {@link android.telephony.DataFailCause#RRC_CONNECTION_CONFIG_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_TIMER_EXPIRED}, {@link android.telephony.DataFailCause#RRC_CONNECTION_LINK_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_CELL_NOT_CAMPED}, {@link android.telephony.DataFailCause#RRC_CONNECTION_SYSTEM_INTERVAL_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_REJECT_BY_NETWORK}, {@link android.telephony.DataFailCause#RRC_CONNECTION_NORMAL_RELEASE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_RADIO_LINK_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_REESTABLISHMENT_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_OUT_OF_SERVICE_DURING_CELL_REGISTER}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ABORT_REQUEST}, {@link android.telephony.DataFailCause#RRC_CONNECTION_SYSTEM_INFORMATION_BLOCK_READ_ERROR}, {@link android.telephony.DataFailCause#NETWORK_INITIATED_DETACH_WITH_AUTO_REATTACH}, {@link android.telephony.DataFailCause#NETWORK_INITIATED_DETACH_NO_AUTO_REATTACH}, {@link android.telephony.DataFailCause#ESM_PROCEDURE_TIME_OUT}, {@link android.telephony.DataFailCause#INVALID_CONNECTION_ID}, {@link android.telephony.DataFailCause#MAXIMIUM_NSAPIS_EXCEEDED}, {@link android.telephony.DataFailCause#INVALID_PRIMARY_NSAPI}, {@link android.telephony.DataFailCause#CANNOT_ENCODE_OTA_MESSAGE}, {@link android.telephony.DataFailCause#RADIO_ACCESS_BEARER_SETUP_FAILURE}, {@link android.telephony.DataFailCause#PDP_ESTABLISH_TIMEOUT_EXPIRED}, {@link android.telephony.DataFailCause#PDP_MODIFY_TIMEOUT_EXPIRED}, {@link android.telephony.DataFailCause#PDP_INACTIVE_TIMEOUT_EXPIRED}, {@link android.telephony.DataFailCause#PDP_LOWERLAYER_ERROR}, {@link android.telephony.DataFailCause#PDP_MODIFY_COLLISION}, {@link android.telephony.DataFailCause#MAXINUM_SIZE_OF_L2_MESSAGE_EXCEEDED}, {@link android.telephony.DataFailCause#NAS_REQUEST_REJECTED_BY_NETWORK}, {@link android.telephony.DataFailCause#RRC_CONNECTION_INVALID_REQUEST}, {@link android.telephony.DataFailCause#RRC_CONNECTION_TRACKING_AREA_ID_CHANGED}, {@link android.telephony.DataFailCause#RRC_CONNECTION_RF_UNAVAILABLE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ABORTED_DUE_TO_IRAT_CHANGE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_RELEASED_SECURITY_NOT_ACTIVE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ABORTED_AFTER_HANDOVER}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ABORTED_AFTER_IRAT_CELL_CHANGE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ABORTED_DURING_IRAT_CELL_CHANGE}, {@link android.telephony.DataFailCause#IMSI_UNKNOWN_IN_HOME_SUBSCRIBER_SERVER}, {@link android.telephony.DataFailCause#IMEI_NOT_ACCEPTED}, {@link android.telephony.DataFailCause#EPS_SERVICES_AND_NON_EPS_SERVICES_NOT_ALLOWED}, {@link android.telephony.DataFailCause#EPS_SERVICES_NOT_ALLOWED_IN_PLMN}, {@link android.telephony.DataFailCause#MSC_TEMPORARILY_NOT_REACHABLE}, {@link android.telephony.DataFailCause#CS_DOMAIN_NOT_AVAILABLE}, {@link android.telephony.DataFailCause#ESM_FAILURE}, {@link android.telephony.DataFailCause#MAC_FAILURE}, {@link android.telephony.DataFailCause#SYNCHRONIZATION_FAILURE}, {@link android.telephony.DataFailCause#UE_SECURITY_CAPABILITIES_MISMATCH}, {@link android.telephony.DataFailCause#SECURITY_MODE_REJECTED}, {@link android.telephony.DataFailCause#UNACCEPTABLE_NON_EPS_AUTHENTICATION}, {@link android.telephony.DataFailCause#CS_FALLBACK_CALL_ESTABLISHMENT_NOT_ALLOWED}, {@link android.telephony.DataFailCause#NO_EPS_BEARER_CONTEXT_ACTIVATED}, {@link android.telephony.DataFailCause#INVALID_EMM_STATE}, {@link android.telephony.DataFailCause#NAS_LAYER_FAILURE}, {@link android.telephony.DataFailCause#MULTIPLE_PDP_CALL_NOT_ALLOWED}, {@link android.telephony.DataFailCause#EMBMS_NOT_ENABLED}, {@link android.telephony.DataFailCause#IRAT_HANDOVER_FAILED}, {@link android.telephony.DataFailCause#EMBMS_REGULAR_DEACTIVATION}, {@link android.telephony.DataFailCause#TEST_LOOPBACK_REGULAR_DEACTIVATION}, {@link android.telephony.DataFailCause#LOWER_LAYER_REGISTRATION_FAILURE}, {@link android.telephony.DataFailCause#DATA_PLAN_EXPIRED}, {@link android.telephony.DataFailCause#UMTS_HANDOVER_TO_IWLAN}, {@link android.telephony.DataFailCause#EVDO_CONNECTION_DENY_BY_GENERAL_OR_NETWORK_BUSY}, {@link android.telephony.DataFailCause#EVDO_CONNECTION_DENY_BY_BILLING_OR_AUTHENTICATION_FAILURE}, {@link android.telephony.DataFailCause#EVDO_HDR_CHANGED}, {@link android.telephony.DataFailCause#EVDO_HDR_EXITED}, {@link android.telephony.DataFailCause#EVDO_HDR_NO_SESSION}, {@link android.telephony.DataFailCause#EVDO_USING_GPS_FIX_INSTEAD_OF_HDR_CALL}, {@link android.telephony.DataFailCause#EVDO_HDR_CONNECTION_SETUP_TIMEOUT}, {@link android.telephony.DataFailCause#FAILED_TO_ACQUIRE_COLOCATED_HDR}, {@link android.telephony.DataFailCause#OTASP_COMMIT_IN_PROGRESS}, {@link android.telephony.DataFailCause#NO_HYBRID_HDR_SERVICE}, {@link android.telephony.DataFailCause#HDR_NO_LOCK_GRANTED}, {@link android.telephony.DataFailCause#DBM_OR_SMS_IN_PROGRESS}, {@link android.telephony.DataFailCause#HDR_FADE}, {@link android.telephony.DataFailCause#HDR_ACCESS_FAILURE}, {@link android.telephony.DataFailCause#UNSUPPORTED_1X_PREV}, {@link android.telephony.DataFailCause#LOCAL_END}, {@link android.telephony.DataFailCause#NO_SERVICE}, {@link android.telephony.DataFailCause#FADE}, {@link android.telephony.DataFailCause#NORMAL_RELEASE}, {@link android.telephony.DataFailCause#ACCESS_ATTEMPT_ALREADY_IN_PROGRESS}, {@link android.telephony.DataFailCause#REDIRECTION_OR_HANDOFF_IN_PROGRESS}, {@link android.telephony.DataFailCause#EMERGENCY_MODE}, {@link android.telephony.DataFailCause#PHONE_IN_USE}, {@link android.telephony.DataFailCause#INVALID_MODE}, {@link android.telephony.DataFailCause#INVALID_SIM_STATE}, {@link android.telephony.DataFailCause#NO_COLLOCATED_HDR}, {@link android.telephony.DataFailCause#UE_IS_ENTERING_POWERSAVE_MODE}, {@link android.telephony.DataFailCause#DUAL_SWITCH}, {@link android.telephony.DataFailCause#PPP_TIMEOUT}, {@link android.telephony.DataFailCause#PPP_AUTH_FAILURE}, {@link android.telephony.DataFailCause#PPP_OPTION_MISMATCH}, {@link android.telephony.DataFailCause#PPP_PAP_FAILURE}, {@link android.telephony.DataFailCause#PPP_CHAP_FAILURE}, {@link android.telephony.DataFailCause#PPP_CLOSE_IN_PROGRESS}, {@link android.telephony.DataFailCause#LIMITED_TO_IPV4}, {@link android.telephony.DataFailCause#LIMITED_TO_IPV6}, {@link android.telephony.DataFailCause#VSNCP_TIMEOUT}, {@link android.telephony.DataFailCause#VSNCP_GEN_ERROR}, {@link android.telephony.DataFailCause#VSNCP_APN_UNATHORIZED}, {@link android.telephony.DataFailCause#VSNCP_PDN_LIMIT_EXCEEDED}, {@link android.telephony.DataFailCause#VSNCP_NO_PDN_GATEWAY_ADDRESS}, {@link android.telephony.DataFailCause#VSNCP_PDN_GATEWAY_UNREACHABLE}, {@link android.telephony.DataFailCause#VSNCP_PDN_GATEWAY_REJECT}, {@link android.telephony.DataFailCause#VSNCP_INSUFFICIENT_PARAMETERS}, {@link android.telephony.DataFailCause#VSNCP_RESOURCE_UNAVAILABLE}, {@link android.telephony.DataFailCause#VSNCP_ADMINISTRATIVELY_PROHIBITED}, {@link android.telephony.DataFailCause#VSNCP_PDN_ID_IN_USE}, {@link android.telephony.DataFailCause#VSNCP_SUBSCRIBER_LIMITATION}, {@link android.telephony.DataFailCause#VSNCP_PDN_EXISTS_FOR_THIS_APN}, {@link android.telephony.DataFailCause#VSNCP_RECONNECT_NOT_ALLOWED}, {@link android.telephony.DataFailCause#IPV6_PREFIX_UNAVAILABLE}, {@link android.telephony.DataFailCause#HANDOFF_PREFERENCE_CHANGED}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_1}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_2}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_3}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_4}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_5}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_6}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_7}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_8}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_9}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_10}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_11}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_12}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_13}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_14}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_15}, {@link android.telephony.DataFailCause#REGISTRATION_FAIL}, {@link android.telephony.DataFailCause#GPRS_REGISTRATION_FAIL}, {@link android.telephony.DataFailCause#SIGNAL_LOST}, {@link android.telephony.DataFailCause#PREF_RADIO_TECH_CHANGED}, {@link android.telephony.DataFailCause#RADIO_POWER_OFF}, {@link android.telephony.DataFailCause#TETHERED_CALL_ACTIVE}, {@link android.telephony.DataFailCause#ERROR_UNSPECIFIED}, {@link android.telephony.DataFailCause#UNKNOWN}, {@link android.telephony.DataFailCause#RADIO_NOT_AVAILABLE}, android.telephony.DataFailCause.UNACCEPTABLE_NETWORK_PARAMETER, android.telephony.DataFailCause.CONNECTION_TO_DATACONNECTIONAC_BROKEN, {@link android.telephony.DataFailCause#LOST_CONNECTION}, or android.telephony.DataFailCause.RESET_BY_FRAMEWORK
 * @apiSince REL
 */

public int getCause() { throw new RuntimeException("Stub!"); }

/**
 * @return The suggested data retry time in milliseconds.
 * @apiSince REL
 */

public int getSuggestedRetryTime() { throw new RuntimeException("Stub!"); }

/**
 * @return The unique id of the data connection.
 * @apiSince REL
 */

public int getId() { throw new RuntimeException("Stub!"); }

/**
 * @return The link status
 
 * Value is {@link android.telephony.data.DataCallResponse#LINK_STATUS_UNKNOWN}, {@link android.telephony.data.DataCallResponse#LINK_STATUS_INACTIVE}, {@link android.telephony.data.DataCallResponse#LINK_STATUS_DORMANT}, or {@link android.telephony.data.DataCallResponse#LINK_STATUS_ACTIVE}
 * @apiSince REL
 */

public int getLinkStatus() { throw new RuntimeException("Stub!"); }

/**
 * @return The connection protocol type.
 
 * Value is {@link android.telephony.data.ApnSetting#PROTOCOL_IP}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV6}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV4V6}, {@link android.telephony.data.ApnSetting#PROTOCOL_PPP}, {@link android.telephony.data.ApnSetting#PROTOCOL_NON_IP}, or {@link android.telephony.data.ApnSetting#PROTOCOL_UNSTRUCTURED}
 * @apiSince REL
 */

public int getProtocolType() { throw new RuntimeException("Stub!"); }

/**
 * @return The network interface name (e.g. "rmnet_data1").
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getInterfaceName() { throw new RuntimeException("Stub!"); }

/**
 * @return A list of addresses of this data connection.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.net.LinkAddress> getAddresses() { throw new RuntimeException("Stub!"); }

/**
 * @return A list of DNS server addresses, e.g., "192.0.1.3" or
 * "192.0.1.11 2001:db8::1". Empty list if no dns server addresses returned.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<java.net.InetAddress> getDnsAddresses() { throw new RuntimeException("Stub!"); }

/**
 * @return A list of default gateway addresses, e.g., "192.0.1.3" or
 * "192.0.1.11 2001:db8::1". Empty list if the addresses represent point to point connections.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<java.net.InetAddress> getGatewayAddresses() { throw new RuntimeException("Stub!"); }

/**
 * @return A list of Proxy Call State Control Function address via PCO (Protocol Configuration
 * Option) for IMS client.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<java.net.InetAddress> getPcscfAddresses() { throw new RuntimeException("Stub!"); }

/**
 * @return MTU (maximum transmission unit) in bytes received from network. Zero or negative
 * values means network has either not sent a value or sent an invalid value.
 * @apiSince REL
 */

public int getMtu() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.data.DataCallResponse> CREATOR;
static { CREATOR = null; }

/**
 * Indicates the data connection is active with physical link up.
 * @apiSince REL
 */

public static final int LINK_STATUS_ACTIVE = 2; // 0x2

/**
 * Indicates the data connection is active with physical link dormant.
 * @apiSince REL
 */

public static final int LINK_STATUS_DORMANT = 1; // 0x1

/**
 * Indicates the data connection is inactive.
 * @apiSince REL
 */

public static final int LINK_STATUS_INACTIVE = 0; // 0x0

/**
 * Unknown status
 * @apiSince REL
 */

public static final int LINK_STATUS_UNKNOWN = -1; // 0xffffffff
/**
 * Provides a convenient way to set the fields of a {@link DataCallResponse} when creating a new
 * instance.
 *
 * <p>The example below shows how you might create a new {@code DataCallResponse}:
 *
 * <pre><code>
 *
 * DataCallResponse response = new DataCallResponse.Builder()
 *     .setAddresses(Arrays.asList("192.168.1.2"))
 *     .setProtocolType(ApnSetting.PROTOCOL_IPV4V6)
 *     .build();
 * </code></pre>
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Default constructor for Builder.
 * @apiSince REL
 */

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Set data call fail cause.
 *
 * @param cause Data call fail cause. {@link DataFailCause#NONE} indicates no error.
 * Value is {@link android.telephony.DataFailCause#NONE}, {@link android.telephony.DataFailCause#OPERATOR_BARRED}, {@link android.telephony.DataFailCause#NAS_SIGNALLING}, {@link android.telephony.DataFailCause#LLC_SNDCP}, {@link android.telephony.DataFailCause#INSUFFICIENT_RESOURCES}, {@link android.telephony.DataFailCause#MISSING_UNKNOWN_APN}, {@link android.telephony.DataFailCause#UNKNOWN_PDP_ADDRESS_TYPE}, {@link android.telephony.DataFailCause#USER_AUTHENTICATION}, {@link android.telephony.DataFailCause#ACTIVATION_REJECT_GGSN}, {@link android.telephony.DataFailCause#ACTIVATION_REJECT_UNSPECIFIED}, {@link android.telephony.DataFailCause#SERVICE_OPTION_NOT_SUPPORTED}, {@link android.telephony.DataFailCause#SERVICE_OPTION_NOT_SUBSCRIBED}, {@link android.telephony.DataFailCause#SERVICE_OPTION_OUT_OF_ORDER}, {@link android.telephony.DataFailCause#NSAPI_IN_USE}, {@link android.telephony.DataFailCause#REGULAR_DEACTIVATION}, {@link android.telephony.DataFailCause#QOS_NOT_ACCEPTED}, {@link android.telephony.DataFailCause#NETWORK_FAILURE}, {@link android.telephony.DataFailCause#UMTS_REACTIVATION_REQ}, {@link android.telephony.DataFailCause#FEATURE_NOT_SUPP}, {@link android.telephony.DataFailCause#TFT_SEMANTIC_ERROR}, {@link android.telephony.DataFailCause#TFT_SYTAX_ERROR}, {@link android.telephony.DataFailCause#UNKNOWN_PDP_CONTEXT}, {@link android.telephony.DataFailCause#FILTER_SEMANTIC_ERROR}, {@link android.telephony.DataFailCause#FILTER_SYTAX_ERROR}, {@link android.telephony.DataFailCause#PDP_WITHOUT_ACTIVE_TFT}, {@link android.telephony.DataFailCause#ACTIVATION_REJECTED_BCM_VIOLATION}, {@link android.telephony.DataFailCause#ONLY_IPV4_ALLOWED}, {@link android.telephony.DataFailCause#ONLY_IPV6_ALLOWED}, {@link android.telephony.DataFailCause#ONLY_SINGLE_BEARER_ALLOWED}, {@link android.telephony.DataFailCause#ESM_INFO_NOT_RECEIVED}, {@link android.telephony.DataFailCause#PDN_CONN_DOES_NOT_EXIST}, {@link android.telephony.DataFailCause#MULTI_CONN_TO_SAME_PDN_NOT_ALLOWED}, {@link android.telephony.DataFailCause#COLLISION_WITH_NETWORK_INITIATED_REQUEST}, {@link android.telephony.DataFailCause#ONLY_IPV4V6_ALLOWED}, {@link android.telephony.DataFailCause#ONLY_NON_IP_ALLOWED}, {@link android.telephony.DataFailCause#UNSUPPORTED_QCI_VALUE}, {@link android.telephony.DataFailCause#BEARER_HANDLING_NOT_SUPPORTED}, {@link android.telephony.DataFailCause#ACTIVE_PDP_CONTEXT_MAX_NUMBER_REACHED}, {@link android.telephony.DataFailCause#UNSUPPORTED_APN_IN_CURRENT_PLMN}, {@link android.telephony.DataFailCause#INVALID_TRANSACTION_ID}, {@link android.telephony.DataFailCause#MESSAGE_INCORRECT_SEMANTIC}, {@link android.telephony.DataFailCause#INVALID_MANDATORY_INFO}, {@link android.telephony.DataFailCause#MESSAGE_TYPE_UNSUPPORTED}, {@link android.telephony.DataFailCause#MSG_TYPE_NONCOMPATIBLE_STATE}, {@link android.telephony.DataFailCause#UNKNOWN_INFO_ELEMENT}, {@link android.telephony.DataFailCause#CONDITIONAL_IE_ERROR}, {@link android.telephony.DataFailCause#MSG_AND_PROTOCOL_STATE_UNCOMPATIBLE}, {@link android.telephony.DataFailCause#PROTOCOL_ERRORS}, {@link android.telephony.DataFailCause#APN_TYPE_CONFLICT}, {@link android.telephony.DataFailCause#INVALID_PCSCF_ADDR}, {@link android.telephony.DataFailCause#INTERNAL_CALL_PREEMPT_BY_HIGH_PRIO_APN}, {@link android.telephony.DataFailCause#EMM_ACCESS_BARRED}, {@link android.telephony.DataFailCause#EMERGENCY_IFACE_ONLY}, {@link android.telephony.DataFailCause#IFACE_MISMATCH}, {@link android.telephony.DataFailCause#COMPANION_IFACE_IN_USE}, {@link android.telephony.DataFailCause#IP_ADDRESS_MISMATCH}, {@link android.telephony.DataFailCause#IFACE_AND_POL_FAMILY_MISMATCH}, {@link android.telephony.DataFailCause#EMM_ACCESS_BARRED_INFINITE_RETRY}, {@link android.telephony.DataFailCause#AUTH_FAILURE_ON_EMERGENCY_CALL}, {@link android.telephony.DataFailCause#INVALID_DNS_ADDR}, {@link android.telephony.DataFailCause#INVALID_PCSCF_OR_DNS_ADDRESS}, {@link android.telephony.DataFailCause#CALL_PREEMPT_BY_EMERGENCY_APN}, {@link android.telephony.DataFailCause#UE_INITIATED_DETACH_OR_DISCONNECT}, {@link android.telephony.DataFailCause#MIP_FA_REASON_UNSPECIFIED}, {@link android.telephony.DataFailCause#MIP_FA_ADMIN_PROHIBITED}, {@link android.telephony.DataFailCause#MIP_FA_INSUFFICIENT_RESOURCES}, {@link android.telephony.DataFailCause#MIP_FA_MOBILE_NODE_AUTHENTICATION_FAILURE}, {@link android.telephony.DataFailCause#MIP_FA_HOME_AGENT_AUTHENTICATION_FAILURE}, {@link android.telephony.DataFailCause#MIP_FA_REQUESTED_LIFETIME_TOO_LONG}, {@link android.telephony.DataFailCause#MIP_FA_MALFORMED_REQUEST}, {@link android.telephony.DataFailCause#MIP_FA_MALFORMED_REPLY}, {@link android.telephony.DataFailCause#MIP_FA_ENCAPSULATION_UNAVAILABLE}, {@link android.telephony.DataFailCause#MIP_FA_VJ_HEADER_COMPRESSION_UNAVAILABLE}, {@link android.telephony.DataFailCause#MIP_FA_REVERSE_TUNNEL_UNAVAILABLE}, {@link android.telephony.DataFailCause#MIP_FA_REVERSE_TUNNEL_IS_MANDATORY}, {@link android.telephony.DataFailCause#MIP_FA_DELIVERY_STYLE_NOT_SUPPORTED}, {@link android.telephony.DataFailCause#MIP_FA_MISSING_NAI}, {@link android.telephony.DataFailCause#MIP_FA_MISSING_HOME_AGENT}, {@link android.telephony.DataFailCause#MIP_FA_MISSING_HOME_ADDRESS}, {@link android.telephony.DataFailCause#MIP_FA_UNKNOWN_CHALLENGE}, {@link android.telephony.DataFailCause#MIP_FA_MISSING_CHALLENGE}, {@link android.telephony.DataFailCause#MIP_FA_STALE_CHALLENGE}, {@link android.telephony.DataFailCause#MIP_HA_REASON_UNSPECIFIED}, {@link android.telephony.DataFailCause#MIP_HA_ADMIN_PROHIBITED}, {@link android.telephony.DataFailCause#MIP_HA_INSUFFICIENT_RESOURCES}, {@link android.telephony.DataFailCause#MIP_HA_MOBILE_NODE_AUTHENTICATION_FAILURE}, {@link android.telephony.DataFailCause#MIP_HA_FOREIGN_AGENT_AUTHENTICATION_FAILURE}, {@link android.telephony.DataFailCause#MIP_HA_REGISTRATION_ID_MISMATCH}, {@link android.telephony.DataFailCause#MIP_HA_MALFORMED_REQUEST}, {@link android.telephony.DataFailCause#MIP_HA_UNKNOWN_HOME_AGENT_ADDRESS}, {@link android.telephony.DataFailCause#MIP_HA_REVERSE_TUNNEL_UNAVAILABLE}, {@link android.telephony.DataFailCause#MIP_HA_REVERSE_TUNNEL_IS_MANDATORY}, {@link android.telephony.DataFailCause#MIP_HA_ENCAPSULATION_UNAVAILABLE}, {@link android.telephony.DataFailCause#CLOSE_IN_PROGRESS}, {@link android.telephony.DataFailCause#NETWORK_INITIATED_TERMINATION}, {@link android.telephony.DataFailCause#MODEM_APP_PREEMPTED}, {@link android.telephony.DataFailCause#PDN_IPV4_CALL_DISALLOWED}, {@link android.telephony.DataFailCause#PDN_IPV4_CALL_THROTTLED}, {@link android.telephony.DataFailCause#PDN_IPV6_CALL_DISALLOWED}, {@link android.telephony.DataFailCause#PDN_IPV6_CALL_THROTTLED}, {@link android.telephony.DataFailCause#MODEM_RESTART}, {@link android.telephony.DataFailCause#PDP_PPP_NOT_SUPPORTED}, {@link android.telephony.DataFailCause#UNPREFERRED_RAT}, {@link android.telephony.DataFailCause#PHYSICAL_LINK_CLOSE_IN_PROGRESS}, {@link android.telephony.DataFailCause#APN_PENDING_HANDOVER}, {@link android.telephony.DataFailCause#PROFILE_BEARER_INCOMPATIBLE}, {@link android.telephony.DataFailCause#SIM_CARD_CHANGED}, {@link android.telephony.DataFailCause#LOW_POWER_MODE_OR_POWERING_DOWN}, {@link android.telephony.DataFailCause#APN_DISABLED}, {@link android.telephony.DataFailCause#MAX_PPP_INACTIVITY_TIMER_EXPIRED}, {@link android.telephony.DataFailCause#IPV6_ADDRESS_TRANSFER_FAILED}, {@link android.telephony.DataFailCause#TRAT_SWAP_FAILED}, {@link android.telephony.DataFailCause#EHRPD_TO_HRPD_FALLBACK}, {@link android.telephony.DataFailCause#MIP_CONFIG_FAILURE}, {@link android.telephony.DataFailCause#PDN_INACTIVITY_TIMER_EXPIRED}, {@link android.telephony.DataFailCause#MAX_IPV4_CONNECTIONS}, {@link android.telephony.DataFailCause#MAX_IPV6_CONNECTIONS}, {@link android.telephony.DataFailCause#APN_MISMATCH}, {@link android.telephony.DataFailCause#IP_VERSION_MISMATCH}, {@link android.telephony.DataFailCause#DUN_CALL_DISALLOWED}, {@link android.telephony.DataFailCause#INTERNAL_EPC_NONEPC_TRANSITION}, {@link android.telephony.DataFailCause#INTERFACE_IN_USE}, {@link android.telephony.DataFailCause#APN_DISALLOWED_ON_ROAMING}, {@link android.telephony.DataFailCause#APN_PARAMETERS_CHANGED}, {@link android.telephony.DataFailCause#NULL_APN_DISALLOWED}, {@link android.telephony.DataFailCause#THERMAL_MITIGATION}, {@link android.telephony.DataFailCause#DATA_SETTINGS_DISABLED}, {@link android.telephony.DataFailCause#DATA_ROAMING_SETTINGS_DISABLED}, {@link android.telephony.DataFailCause#DDS_SWITCHED}, {@link android.telephony.DataFailCause#FORBIDDEN_APN_NAME}, {@link android.telephony.DataFailCause#DDS_SWITCH_IN_PROGRESS}, {@link android.telephony.DataFailCause#CALL_DISALLOWED_IN_ROAMING}, {@link android.telephony.DataFailCause#NON_IP_NOT_SUPPORTED}, {@link android.telephony.DataFailCause#PDN_NON_IP_CALL_THROTTLED}, {@link android.telephony.DataFailCause#PDN_NON_IP_CALL_DISALLOWED}, {@link android.telephony.DataFailCause#CDMA_LOCK}, {@link android.telephony.DataFailCause#CDMA_INTERCEPT}, {@link android.telephony.DataFailCause#CDMA_REORDER}, {@link android.telephony.DataFailCause#CDMA_RELEASE_DUE_TO_SO_REJECTION}, {@link android.telephony.DataFailCause#CDMA_INCOMING_CALL}, {@link android.telephony.DataFailCause#CDMA_ALERT_STOP}, {@link android.telephony.DataFailCause#CHANNEL_ACQUISITION_FAILURE}, {@link android.telephony.DataFailCause#MAX_ACCESS_PROBE}, {@link android.telephony.DataFailCause#CONCURRENT_SERVICE_NOT_SUPPORTED_BY_BASE_STATION}, {@link android.telephony.DataFailCause#NO_RESPONSE_FROM_BASE_STATION}, {@link android.telephony.DataFailCause#REJECTED_BY_BASE_STATION}, {@link android.telephony.DataFailCause#CONCURRENT_SERVICES_INCOMPATIBLE}, {@link android.telephony.DataFailCause#NO_CDMA_SERVICE}, {@link android.telephony.DataFailCause#RUIM_NOT_PRESENT}, {@link android.telephony.DataFailCause#CDMA_RETRY_ORDER}, {@link android.telephony.DataFailCause#ACCESS_BLOCK}, {@link android.telephony.DataFailCause#ACCESS_BLOCK_ALL}, {@link android.telephony.DataFailCause#IS707B_MAX_ACCESS_PROBES}, {@link android.telephony.DataFailCause#THERMAL_EMERGENCY}, {@link android.telephony.DataFailCause#CONCURRENT_SERVICES_NOT_ALLOWED}, {@link android.telephony.DataFailCause#INCOMING_CALL_REJECTED}, {@link android.telephony.DataFailCause#NO_SERVICE_ON_GATEWAY}, {@link android.telephony.DataFailCause#NO_GPRS_CONTEXT}, {@link android.telephony.DataFailCause#ILLEGAL_MS}, {@link android.telephony.DataFailCause#ILLEGAL_ME}, {@link android.telephony.DataFailCause#GPRS_SERVICES_AND_NON_GPRS_SERVICES_NOT_ALLOWED}, {@link android.telephony.DataFailCause#GPRS_SERVICES_NOT_ALLOWED}, {@link android.telephony.DataFailCause#MS_IDENTITY_CANNOT_BE_DERIVED_BY_THE_NETWORK}, {@link android.telephony.DataFailCause#IMPLICITLY_DETACHED}, {@link android.telephony.DataFailCause#PLMN_NOT_ALLOWED}, {@link android.telephony.DataFailCause#LOCATION_AREA_NOT_ALLOWED}, {@link android.telephony.DataFailCause#GPRS_SERVICES_NOT_ALLOWED_IN_THIS_PLMN}, {@link android.telephony.DataFailCause#PDP_DUPLICATE}, {@link android.telephony.DataFailCause#UE_RAT_CHANGE}, {@link android.telephony.DataFailCause#CONGESTION}, {@link android.telephony.DataFailCause#NO_PDP_CONTEXT_ACTIVATED}, {@link android.telephony.DataFailCause#ACCESS_CLASS_DSAC_REJECTION}, {@link android.telephony.DataFailCause#PDP_ACTIVATE_MAX_RETRY_FAILED}, {@link android.telephony.DataFailCause#RADIO_ACCESS_BEARER_FAILURE}, {@link android.telephony.DataFailCause#ESM_UNKNOWN_EPS_BEARER_CONTEXT}, {@link android.telephony.DataFailCause#DRB_RELEASED_BY_RRC}, {@link android.telephony.DataFailCause#CONNECTION_RELEASED}, {@link android.telephony.DataFailCause#EMM_DETACHED}, {@link android.telephony.DataFailCause#EMM_ATTACH_FAILED}, {@link android.telephony.DataFailCause#EMM_ATTACH_STARTED}, {@link android.telephony.DataFailCause#LTE_NAS_SERVICE_REQUEST_FAILED}, {@link android.telephony.DataFailCause#DUPLICATE_BEARER_ID}, {@link android.telephony.DataFailCause#ESM_COLLISION_SCENARIOS}, {@link android.telephony.DataFailCause#ESM_BEARER_DEACTIVATED_TO_SYNC_WITH_NETWORK}, {@link android.telephony.DataFailCause#ESM_NW_ACTIVATED_DED_BEARER_WITH_ID_OF_DEF_BEARER}, {@link android.telephony.DataFailCause#ESM_BAD_OTA_MESSAGE}, {@link android.telephony.DataFailCause#ESM_DOWNLOAD_SERVER_REJECTED_THE_CALL}, {@link android.telephony.DataFailCause#ESM_CONTEXT_TRANSFERRED_DUE_TO_IRAT}, {@link android.telephony.DataFailCause#DS_EXPLICIT_DEACTIVATION}, {@link android.telephony.DataFailCause#ESM_LOCAL_CAUSE_NONE}, {@link android.telephony.DataFailCause#LTE_THROTTLING_NOT_REQUIRED}, {@link android.telephony.DataFailCause#ACCESS_CONTROL_LIST_CHECK_FAILURE}, {@link android.telephony.DataFailCause#SERVICE_NOT_ALLOWED_ON_PLMN}, {@link android.telephony.DataFailCause#EMM_T3417_EXPIRED}, {@link android.telephony.DataFailCause#EMM_T3417_EXT_EXPIRED}, {@link android.telephony.DataFailCause#RRC_UPLINK_DATA_TRANSMISSION_FAILURE}, {@link android.telephony.DataFailCause#RRC_UPLINK_DELIVERY_FAILED_DUE_TO_HANDOVER}, {@link android.telephony.DataFailCause#RRC_UPLINK_CONNECTION_RELEASE}, {@link android.telephony.DataFailCause#RRC_UPLINK_RADIO_LINK_FAILURE}, {@link android.telephony.DataFailCause#RRC_UPLINK_ERROR_REQUEST_FROM_NAS}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ACCESS_STRATUM_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ANOTHER_PROCEDURE_IN_PROGRESS}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ACCESS_BARRED}, {@link android.telephony.DataFailCause#RRC_CONNECTION_CELL_RESELECTION}, {@link android.telephony.DataFailCause#RRC_CONNECTION_CONFIG_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_TIMER_EXPIRED}, {@link android.telephony.DataFailCause#RRC_CONNECTION_LINK_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_CELL_NOT_CAMPED}, {@link android.telephony.DataFailCause#RRC_CONNECTION_SYSTEM_INTERVAL_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_REJECT_BY_NETWORK}, {@link android.telephony.DataFailCause#RRC_CONNECTION_NORMAL_RELEASE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_RADIO_LINK_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_REESTABLISHMENT_FAILURE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_OUT_OF_SERVICE_DURING_CELL_REGISTER}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ABORT_REQUEST}, {@link android.telephony.DataFailCause#RRC_CONNECTION_SYSTEM_INFORMATION_BLOCK_READ_ERROR}, {@link android.telephony.DataFailCause#NETWORK_INITIATED_DETACH_WITH_AUTO_REATTACH}, {@link android.telephony.DataFailCause#NETWORK_INITIATED_DETACH_NO_AUTO_REATTACH}, {@link android.telephony.DataFailCause#ESM_PROCEDURE_TIME_OUT}, {@link android.telephony.DataFailCause#INVALID_CONNECTION_ID}, {@link android.telephony.DataFailCause#MAXIMIUM_NSAPIS_EXCEEDED}, {@link android.telephony.DataFailCause#INVALID_PRIMARY_NSAPI}, {@link android.telephony.DataFailCause#CANNOT_ENCODE_OTA_MESSAGE}, {@link android.telephony.DataFailCause#RADIO_ACCESS_BEARER_SETUP_FAILURE}, {@link android.telephony.DataFailCause#PDP_ESTABLISH_TIMEOUT_EXPIRED}, {@link android.telephony.DataFailCause#PDP_MODIFY_TIMEOUT_EXPIRED}, {@link android.telephony.DataFailCause#PDP_INACTIVE_TIMEOUT_EXPIRED}, {@link android.telephony.DataFailCause#PDP_LOWERLAYER_ERROR}, {@link android.telephony.DataFailCause#PDP_MODIFY_COLLISION}, {@link android.telephony.DataFailCause#MAXINUM_SIZE_OF_L2_MESSAGE_EXCEEDED}, {@link android.telephony.DataFailCause#NAS_REQUEST_REJECTED_BY_NETWORK}, {@link android.telephony.DataFailCause#RRC_CONNECTION_INVALID_REQUEST}, {@link android.telephony.DataFailCause#RRC_CONNECTION_TRACKING_AREA_ID_CHANGED}, {@link android.telephony.DataFailCause#RRC_CONNECTION_RF_UNAVAILABLE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ABORTED_DUE_TO_IRAT_CHANGE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_RELEASED_SECURITY_NOT_ACTIVE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ABORTED_AFTER_HANDOVER}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ABORTED_AFTER_IRAT_CELL_CHANGE}, {@link android.telephony.DataFailCause#RRC_CONNECTION_ABORTED_DURING_IRAT_CELL_CHANGE}, {@link android.telephony.DataFailCause#IMSI_UNKNOWN_IN_HOME_SUBSCRIBER_SERVER}, {@link android.telephony.DataFailCause#IMEI_NOT_ACCEPTED}, {@link android.telephony.DataFailCause#EPS_SERVICES_AND_NON_EPS_SERVICES_NOT_ALLOWED}, {@link android.telephony.DataFailCause#EPS_SERVICES_NOT_ALLOWED_IN_PLMN}, {@link android.telephony.DataFailCause#MSC_TEMPORARILY_NOT_REACHABLE}, {@link android.telephony.DataFailCause#CS_DOMAIN_NOT_AVAILABLE}, {@link android.telephony.DataFailCause#ESM_FAILURE}, {@link android.telephony.DataFailCause#MAC_FAILURE}, {@link android.telephony.DataFailCause#SYNCHRONIZATION_FAILURE}, {@link android.telephony.DataFailCause#UE_SECURITY_CAPABILITIES_MISMATCH}, {@link android.telephony.DataFailCause#SECURITY_MODE_REJECTED}, {@link android.telephony.DataFailCause#UNACCEPTABLE_NON_EPS_AUTHENTICATION}, {@link android.telephony.DataFailCause#CS_FALLBACK_CALL_ESTABLISHMENT_NOT_ALLOWED}, {@link android.telephony.DataFailCause#NO_EPS_BEARER_CONTEXT_ACTIVATED}, {@link android.telephony.DataFailCause#INVALID_EMM_STATE}, {@link android.telephony.DataFailCause#NAS_LAYER_FAILURE}, {@link android.telephony.DataFailCause#MULTIPLE_PDP_CALL_NOT_ALLOWED}, {@link android.telephony.DataFailCause#EMBMS_NOT_ENABLED}, {@link android.telephony.DataFailCause#IRAT_HANDOVER_FAILED}, {@link android.telephony.DataFailCause#EMBMS_REGULAR_DEACTIVATION}, {@link android.telephony.DataFailCause#TEST_LOOPBACK_REGULAR_DEACTIVATION}, {@link android.telephony.DataFailCause#LOWER_LAYER_REGISTRATION_FAILURE}, {@link android.telephony.DataFailCause#DATA_PLAN_EXPIRED}, {@link android.telephony.DataFailCause#UMTS_HANDOVER_TO_IWLAN}, {@link android.telephony.DataFailCause#EVDO_CONNECTION_DENY_BY_GENERAL_OR_NETWORK_BUSY}, {@link android.telephony.DataFailCause#EVDO_CONNECTION_DENY_BY_BILLING_OR_AUTHENTICATION_FAILURE}, {@link android.telephony.DataFailCause#EVDO_HDR_CHANGED}, {@link android.telephony.DataFailCause#EVDO_HDR_EXITED}, {@link android.telephony.DataFailCause#EVDO_HDR_NO_SESSION}, {@link android.telephony.DataFailCause#EVDO_USING_GPS_FIX_INSTEAD_OF_HDR_CALL}, {@link android.telephony.DataFailCause#EVDO_HDR_CONNECTION_SETUP_TIMEOUT}, {@link android.telephony.DataFailCause#FAILED_TO_ACQUIRE_COLOCATED_HDR}, {@link android.telephony.DataFailCause#OTASP_COMMIT_IN_PROGRESS}, {@link android.telephony.DataFailCause#NO_HYBRID_HDR_SERVICE}, {@link android.telephony.DataFailCause#HDR_NO_LOCK_GRANTED}, {@link android.telephony.DataFailCause#DBM_OR_SMS_IN_PROGRESS}, {@link android.telephony.DataFailCause#HDR_FADE}, {@link android.telephony.DataFailCause#HDR_ACCESS_FAILURE}, {@link android.telephony.DataFailCause#UNSUPPORTED_1X_PREV}, {@link android.telephony.DataFailCause#LOCAL_END}, {@link android.telephony.DataFailCause#NO_SERVICE}, {@link android.telephony.DataFailCause#FADE}, {@link android.telephony.DataFailCause#NORMAL_RELEASE}, {@link android.telephony.DataFailCause#ACCESS_ATTEMPT_ALREADY_IN_PROGRESS}, {@link android.telephony.DataFailCause#REDIRECTION_OR_HANDOFF_IN_PROGRESS}, {@link android.telephony.DataFailCause#EMERGENCY_MODE}, {@link android.telephony.DataFailCause#PHONE_IN_USE}, {@link android.telephony.DataFailCause#INVALID_MODE}, {@link android.telephony.DataFailCause#INVALID_SIM_STATE}, {@link android.telephony.DataFailCause#NO_COLLOCATED_HDR}, {@link android.telephony.DataFailCause#UE_IS_ENTERING_POWERSAVE_MODE}, {@link android.telephony.DataFailCause#DUAL_SWITCH}, {@link android.telephony.DataFailCause#PPP_TIMEOUT}, {@link android.telephony.DataFailCause#PPP_AUTH_FAILURE}, {@link android.telephony.DataFailCause#PPP_OPTION_MISMATCH}, {@link android.telephony.DataFailCause#PPP_PAP_FAILURE}, {@link android.telephony.DataFailCause#PPP_CHAP_FAILURE}, {@link android.telephony.DataFailCause#PPP_CLOSE_IN_PROGRESS}, {@link android.telephony.DataFailCause#LIMITED_TO_IPV4}, {@link android.telephony.DataFailCause#LIMITED_TO_IPV6}, {@link android.telephony.DataFailCause#VSNCP_TIMEOUT}, {@link android.telephony.DataFailCause#VSNCP_GEN_ERROR}, {@link android.telephony.DataFailCause#VSNCP_APN_UNATHORIZED}, {@link android.telephony.DataFailCause#VSNCP_PDN_LIMIT_EXCEEDED}, {@link android.telephony.DataFailCause#VSNCP_NO_PDN_GATEWAY_ADDRESS}, {@link android.telephony.DataFailCause#VSNCP_PDN_GATEWAY_UNREACHABLE}, {@link android.telephony.DataFailCause#VSNCP_PDN_GATEWAY_REJECT}, {@link android.telephony.DataFailCause#VSNCP_INSUFFICIENT_PARAMETERS}, {@link android.telephony.DataFailCause#VSNCP_RESOURCE_UNAVAILABLE}, {@link android.telephony.DataFailCause#VSNCP_ADMINISTRATIVELY_PROHIBITED}, {@link android.telephony.DataFailCause#VSNCP_PDN_ID_IN_USE}, {@link android.telephony.DataFailCause#VSNCP_SUBSCRIBER_LIMITATION}, {@link android.telephony.DataFailCause#VSNCP_PDN_EXISTS_FOR_THIS_APN}, {@link android.telephony.DataFailCause#VSNCP_RECONNECT_NOT_ALLOWED}, {@link android.telephony.DataFailCause#IPV6_PREFIX_UNAVAILABLE}, {@link android.telephony.DataFailCause#HANDOFF_PREFERENCE_CHANGED}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_1}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_2}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_3}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_4}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_5}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_6}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_7}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_8}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_9}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_10}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_11}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_12}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_13}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_14}, {@link android.telephony.DataFailCause#OEM_DCFAILCAUSE_15}, {@link android.telephony.DataFailCause#REGISTRATION_FAIL}, {@link android.telephony.DataFailCause#GPRS_REGISTRATION_FAIL}, {@link android.telephony.DataFailCause#SIGNAL_LOST}, {@link android.telephony.DataFailCause#PREF_RADIO_TECH_CHANGED}, {@link android.telephony.DataFailCause#RADIO_POWER_OFF}, {@link android.telephony.DataFailCause#TETHERED_CALL_ACTIVE}, {@link android.telephony.DataFailCause#ERROR_UNSPECIFIED}, {@link android.telephony.DataFailCause#UNKNOWN}, {@link android.telephony.DataFailCause#RADIO_NOT_AVAILABLE}, android.telephony.DataFailCause.UNACCEPTABLE_NETWORK_PARAMETER, android.telephony.DataFailCause.CONNECTION_TO_DATACONNECTIONAC_BROKEN, {@link android.telephony.DataFailCause#LOST_CONNECTION}, or android.telephony.DataFailCause.RESET_BY_FRAMEWORK
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setCause(int cause) { throw new RuntimeException("Stub!"); }

/**
 * Set the suggested data retry time.
 *
 * @param suggestedRetryTime The suggested data retry time in milliseconds.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setSuggestedRetryTime(int suggestedRetryTime) { throw new RuntimeException("Stub!"); }

/**
 * Set the unique id of the data connection.
 *
 * @param id The unique id of the data connection.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setId(int id) { throw new RuntimeException("Stub!"); }

/**
 * Set the link status
 *
 * @param linkStatus The link status
 * Value is {@link android.telephony.data.DataCallResponse#LINK_STATUS_UNKNOWN}, {@link android.telephony.data.DataCallResponse#LINK_STATUS_INACTIVE}, {@link android.telephony.data.DataCallResponse#LINK_STATUS_DORMANT}, or {@link android.telephony.data.DataCallResponse#LINK_STATUS_ACTIVE}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setLinkStatus(int linkStatus) { throw new RuntimeException("Stub!"); }

/**
 * Set the connection protocol type.
 *
 * @param protocolType The connection protocol type.
 * Value is {@link android.telephony.data.ApnSetting#PROTOCOL_IP}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV6}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV4V6}, {@link android.telephony.data.ApnSetting#PROTOCOL_PPP}, {@link android.telephony.data.ApnSetting#PROTOCOL_NON_IP}, or {@link android.telephony.data.ApnSetting#PROTOCOL_UNSTRUCTURED}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setProtocolType(int protocolType) { throw new RuntimeException("Stub!"); }

/**
 * Set the network interface name.
 *
 * @param interfaceName The network interface name (e.g. "rmnet_data1").
 * This value must never be {@code null}.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setInterfaceName(@android.annotation.NonNull java.lang.String interfaceName) { throw new RuntimeException("Stub!"); }

/**
 * Set the addresses of this data connection.
 *
 * @param addresses The list of address of the data connection.
 * This value must never be {@code null}.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setAddresses(@android.annotation.NonNull java.util.List<android.net.LinkAddress> addresses) { throw new RuntimeException("Stub!"); }

/**
 * Set the DNS addresses of this data connection
 *
 * @param dnsAddresses The list of DNS address of the data connection.
 * This value must never be {@code null}.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setDnsAddresses(@android.annotation.NonNull java.util.List<java.net.InetAddress> dnsAddresses) { throw new RuntimeException("Stub!"); }

/**
 * Set the gateway addresses of this data connection
 *
 * @param gatewayAddresses The list of gateway address of the data connection.
 * This value must never be {@code null}.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setGatewayAddresses(@android.annotation.NonNull java.util.List<java.net.InetAddress> gatewayAddresses) { throw new RuntimeException("Stub!"); }

/**
 * Set the Proxy Call State Control Function address via PCO(Protocol Configuration
 * Option) for IMS client.
 *
 * @param pcscfAddresses The list of pcscf address of the data connection.
 * This value must never be {@code null}.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setPcscfAddresses(@android.annotation.NonNull java.util.List<java.net.InetAddress> pcscfAddresses) { throw new RuntimeException("Stub!"); }

/**
 * Set maximum transmission unit of the data connection.
 *
 * @param mtu MTU (maximum transmission unit) in bytes received from network. Zero or
 * negative values means network has either not sent a value or sent an invalid value.
 *
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse.Builder setMtu(int mtu) { throw new RuntimeException("Stub!"); }

/**
 * Build the DataCallResponse.
 *
 * @return the DataCallResponse object.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataCallResponse build() { throw new RuntimeException("Stub!"); }
}

}

