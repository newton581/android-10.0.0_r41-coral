/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app;

import android.content.Intent;
import android.os.UserHandle;
import android.content.pm.InstantAppResolveInfo;
import android.os.Looper;

/**
 * Base class for implementing the resolver service.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class InstantAppResolverService extends android.app.Service {

public InstantAppResolverService() { throw new RuntimeException("Stub!"); }

/**
 * Called to retrieve resolve info for instant applications immediately.
 *
 * @param digestPrefix The hash prefix of the instant app's domain.
 * This value may be {@code null}.
 * @deprecated Should implement {@link #onGetInstantAppResolveInfo(Intent, int[], UserHandle,
 *             String, InstantAppResolutionCallback)}.
 
 * @param token This value must never be {@code null}.
 
 * @param callback This value must never be {@code null}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onGetInstantAppResolveInfo(@android.annotation.Nullable int[] digestPrefix, @android.annotation.NonNull java.lang.String token, @android.annotation.NonNull android.app.InstantAppResolverService.InstantAppResolutionCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Called to retrieve intent filters for instant applications from potentially expensive
 * sources.
 *
 * @param digestPrefix The hash prefix of the instant app's domain.
 * This value may be {@code null}.
 * @deprecated Should implement {@link #onGetInstantAppIntentFilter(Intent, int[], UserHandle,
 *             String, InstantAppResolutionCallback)}.
 
 * @param token This value must never be {@code null}.
 
 * @param callback This value must never be {@code null}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onGetInstantAppIntentFilter(@android.annotation.Nullable int[] digestPrefix, @android.annotation.NonNull java.lang.String token, @android.annotation.NonNull android.app.InstantAppResolverService.InstantAppResolutionCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Called to retrieve resolve info for instant applications immediately. The response will be
 * ignored if not provided within a reasonable time. {@link InstantAppResolveInfo}s provided
 * in response to this method may be partial to request a second phase of resolution which will
 * result in a subsequent call to
 * {@link #onGetInstantAppIntentFilter(Intent, int[], String, InstantAppResolutionCallback)}
 *
 * @param sanitizedIntent The sanitized {@link Intent} used for resolution. A sanitized Intent
 *                        is an intent with potential PII removed from the original intent.
 *                        Fields removed include extras and the host + path of the data, if
 *                        defined.
 * This value must never be {@code null}.
 * @param hostDigestPrefix The hash prefix of the instant app's domain.
 * This value may be {@code null}.
 * @param token A unique identifier that will be provided in calls to
 *              {@link #onGetInstantAppIntentFilter(Intent, int[], String,
 *              InstantAppResolutionCallback)}
 *              and provided to the installer via {@link Intent#EXTRA_INSTANT_APP_TOKEN} to
 *              tie a single launch together.
 * This value must never be {@code null}.
 * @param callback The {@link InstantAppResolutionCallback} to provide results to.
 *
 * This value must never be {@code null}.
 * @see InstantAppResolveInfo
 *
 * @deprecated Should implement {@link #onGetInstantAppResolveInfo(Intent, int[], UserHandle,
 *             String, InstantAppResolutionCallback)}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onGetInstantAppResolveInfo(@android.annotation.NonNull android.content.Intent sanitizedIntent, @android.annotation.Nullable int[] hostDigestPrefix, @android.annotation.NonNull java.lang.String token, @android.annotation.NonNull android.app.InstantAppResolverService.InstantAppResolutionCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Called to retrieve intent filters for potentially matching instant applications. Unlike
 * {@link #onGetInstantAppResolveInfo(Intent, int[], String, InstantAppResolutionCallback)},
 * the response may take as long as necessary to respond. All {@link InstantAppResolveInfo}s
 * provided in response to this method must be completely populated.
 *
 * @param sanitizedIntent The sanitized {@link Intent} used for resolution.
 * This value must never be {@code null}.
 * @param hostDigestPrefix The hash prefix of the instant app's domain or null if no host is
 *                         defined.
 * This value may be {@code null}.
 * @param token A unique identifier that was provided in
 *              {@link #onGetInstantAppResolveInfo(Intent, int[], String,
 *              InstantAppResolutionCallback)}
 *              and provided to the currently visible installer via
 *              {@link Intent#EXTRA_INSTANT_APP_TOKEN}.
 * This value must never be {@code null}.
 * @param callback The {@link InstantAppResolutionCallback} to provide results to.
 *
 * This value must never be {@code null}.
 * @deprecated Should implement {@link #onGetInstantAppIntentFilter(Intent, int[], UserHandle,
 *             String, InstantAppResolutionCallback)}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onGetInstantAppIntentFilter(@android.annotation.NonNull android.content.Intent sanitizedIntent, @android.annotation.Nullable int[] hostDigestPrefix, @android.annotation.NonNull java.lang.String token, @android.annotation.NonNull android.app.InstantAppResolverService.InstantAppResolutionCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Called to retrieve resolve info for instant applications immediately. The response will be
 * ignored if not provided within a reasonable time. {@link InstantAppResolveInfo}s provided
 * in response to this method may be partial to request a second phase of resolution which will
 * result in a subsequent call to {@link #onGetInstantAppIntentFilter(Intent, int[], UserHandle,
 * String, InstantAppResolutionCallback)}
 *
 * @param sanitizedIntent The sanitized {@link Intent} used for resolution. A sanitized Intent
 *                        is an intent with potential PII removed from the original intent.
 *                        Fields removed include extras and the host + path of the data, if
 *                        defined.
 * This value must never be {@code null}.
 * @param hostDigestPrefix The hash prefix of the instant app's domain.
 * This value may be {@code null}.
 * @param userHandle The user for which to resolve the instant app.
 * This value must never be {@code null}.
 * @param token A unique identifier that will be provided in calls to {@link
 *              #onGetInstantAppIntentFilter(Intent, int[], UserHandle, String,
 *              InstantAppResolutionCallback)} and provided to the installer via {@link
 *              Intent#EXTRA_INSTANT_APP_TOKEN} to tie a single launch together.
 * This value must never be {@code null}.
 * @param callback The {@link InstantAppResolutionCallback} to provide results to.
 *
 * This value must never be {@code null}.
 * @see InstantAppResolveInfo
 * @apiSince REL
 */

public void onGetInstantAppResolveInfo(@android.annotation.NonNull android.content.Intent sanitizedIntent, @android.annotation.Nullable int[] hostDigestPrefix, @android.annotation.NonNull android.os.UserHandle userHandle, @android.annotation.NonNull java.lang.String token, @android.annotation.NonNull android.app.InstantAppResolverService.InstantAppResolutionCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Called to retrieve intent filters for potentially matching instant applications. Unlike
 * {@link #onGetInstantAppResolveInfo(Intent, int[], UserHandle, String,
 * InstantAppResolutionCallback)}, the response may take as long as necessary to respond. All
 * {@link InstantAppResolveInfo}s provided in response to this method must be completely
 * populated.
 *
 * @param sanitizedIntent The sanitized {@link Intent} used for resolution.
 * This value must never be {@code null}.
 * @param hostDigestPrefix The hash prefix of the instant app's domain or null if no host is
 *                         defined.
 * This value may be {@code null}.
 * @param userHandle The user for which to resolve the instant app.
 * This value must never be {@code null}.
 * @param token A unique identifier that was provided in {@link #onGetInstantAppResolveInfo(
 *              Intent, int[], UserHandle, String, InstantAppResolutionCallback)} and provided
 *              to the currently visible installer via {@link Intent#EXTRA_INSTANT_APP_TOKEN}.
 * This value must never be {@code null}.
 * @param callback The {@link InstantAppResolutionCallback} to provide results to.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onGetInstantAppIntentFilter(@android.annotation.NonNull android.content.Intent sanitizedIntent, @android.annotation.Nullable int[] hostDigestPrefix, @android.annotation.NonNull android.os.UserHandle userHandle, @android.annotation.NonNull java.lang.String token, @android.annotation.NonNull android.app.InstantAppResolverService.InstantAppResolutionCallback callback) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public final void attachBaseContext(android.content.Context base) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public final android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }
/**
 * Callback to post results from instant app resolution.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class InstantAppResolutionCallback {

InstantAppResolutionCallback() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onInstantAppResolveInfo(java.util.List<android.content.pm.InstantAppResolveInfo> resolveInfo) { throw new RuntimeException("Stub!"); }
}

}

