/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony.data;

import android.os.Build;

/**
 * Description of a mobile data profile used for establishing
 * data connections.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class DataProfile implements android.os.Parcelable {

DataProfile(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/**
 * @return Id of the data profile.
 * @apiSince REL
 */

public int getProfileId() { throw new RuntimeException("Stub!"); }

/**
 * @return The APN (Access Point Name) to establish data connection. This is a string
 * specifically defined by the carrier.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getApn() { throw new RuntimeException("Stub!"); }

/**
 * @return The connection protocol defined in 3GPP TS 27.007 section 10.1.1.
 
 * Value is {@link android.telephony.data.ApnSetting#PROTOCOL_IP}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV6}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV4V6}, {@link android.telephony.data.ApnSetting#PROTOCOL_PPP}, {@link android.telephony.data.ApnSetting#PROTOCOL_NON_IP}, or {@link android.telephony.data.ApnSetting#PROTOCOL_UNSTRUCTURED}
 * @apiSince REL
 */

public int getProtocolType() { throw new RuntimeException("Stub!"); }

/**
 * @return The authentication protocol used for this PDP context.
 
 * Value is {@link android.telephony.data.ApnSetting#AUTH_TYPE_NONE}, {@link android.telephony.data.ApnSetting#AUTH_TYPE_PAP}, {@link android.telephony.data.ApnSetting#AUTH_TYPE_CHAP}, or {@link android.telephony.data.ApnSetting#AUTH_TYPE_PAP_OR_CHAP}
 * @apiSince REL
 */

public int getAuthType() { throw new RuntimeException("Stub!"); }

/**
 * @return The username for APN. Can be null.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getUserName() { throw new RuntimeException("Stub!"); }

/**
 * @return The password for APN. Can be null.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getPassword() { throw new RuntimeException("Stub!"); }

/**
 * @return The profile type.
 
 * Value is {@link android.telephony.data.DataProfile#TYPE_COMMON}, {@link android.telephony.data.DataProfile#TYPE_3GPP}, or {@link android.telephony.data.DataProfile#TYPE_3GPP2}
 * @apiSince REL
 */

public int getType() { throw new RuntimeException("Stub!"); }

/**
 * @return True if the profile is enabled.
 * @apiSince REL
 */

public boolean isEnabled() { throw new RuntimeException("Stub!"); }

/**
 * @return The supported APN types bitmask.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.data.ApnSetting#TYPE_DEFAULT}, {@link android.telephony.data.ApnSetting#TYPE_MMS}, {@link android.telephony.data.ApnSetting#TYPE_SUPL}, {@link android.telephony.data.ApnSetting#TYPE_DUN}, {@link android.telephony.data.ApnSetting#TYPE_HIPRI}, {@link android.telephony.data.ApnSetting#TYPE_FOTA}, {@link android.telephony.data.ApnSetting#TYPE_IMS}, {@link android.telephony.data.ApnSetting#TYPE_CBS}, {@link android.telephony.data.ApnSetting#TYPE_IA}, {@link android.telephony.data.ApnSetting#TYPE_EMERGENCY}, and {@link android.telephony.data.ApnSetting#TYPE_MCX}
 * @apiSince REL
 */

public int getSupportedApnTypesBitmask() { throw new RuntimeException("Stub!"); }

/**
 * @return The connection protocol on roaming network defined in 3GPP TS 27.007 section 10.1.1.
 
 * Value is {@link android.telephony.data.ApnSetting#PROTOCOL_IP}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV6}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV4V6}, {@link android.telephony.data.ApnSetting#PROTOCOL_PPP}, {@link android.telephony.data.ApnSetting#PROTOCOL_NON_IP}, or {@link android.telephony.data.ApnSetting#PROTOCOL_UNSTRUCTURED}
 * @apiSince REL
 */

public int getRoamingProtocolType() { throw new RuntimeException("Stub!"); }

/**
 * @return The bearer bitmask indicating the applicable networks for this data profile.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_LTE_CA}, and {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_NR}
 * @apiSince REL
 */

public int getBearerBitmask() { throw new RuntimeException("Stub!"); }

/**
 * @return The maximum transmission unit (MTU) size in bytes.
 * @apiSince REL
 */

public int getMtu() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if modem must persist this data profile.
 * @apiSince REL
 */

public boolean isPersistent() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if this data profile was used to bring up the last default
 * (i.e internet) data connection successfully, or the one chosen by the user in Settings'
 * APN editor. For one carrier there can be only one profiled preferred.
 * @apiSince REL
 */

public boolean isPreferred() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.data.DataProfile> CREATOR;
static { CREATOR = null; }

/**
 * 3GPP type data profile
 * @apiSince REL
 */

public static final int TYPE_3GPP = 1; // 0x1

/**
 * 3GPP2 type data profile
 * @apiSince REL
 */

public static final int TYPE_3GPP2 = 2; // 0x2

/**
 * Common data profile
 * @apiSince REL
 */

public static final int TYPE_COMMON = 0; // 0x0
/**
 * Provides a convenient way to set the fields of a {@link DataProfile} when creating a new
 * instance.
 *
 * <p>The example below shows how you might create a new {@code DataProfile}:
 *
 * <pre><code>
 *
 * DataProfile dp = new DataProfile.Builder()
 *     .setApn("apn.xyz.com")
 *     .setProtocol(ApnSetting.PROTOCOL_IPV4V6)
 *     .build();
 * </code></pre>
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Default constructor for Builder.
 * @apiSince REL
 */

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Set profile id. Note that this is not a global unique id of the data profile. This id
 * is only used by certain CDMA carriers to identify the type of data profile.
 *
 * @param profileId Network domain.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setProfileId(int profileId) { throw new RuntimeException("Stub!"); }

/**
 * Set the APN (Access Point Name) to establish data connection. This is a string
 * specifically defined by the carrier.
 *
 * @param apn Access point name
 * This value must never be {@code null}.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setApn(@android.annotation.NonNull java.lang.String apn) { throw new RuntimeException("Stub!"); }

/**
 * Set the connection protocol type.
 *
 * @param protocolType The connection protocol defined in 3GPP TS 27.007 section 10.1.1.
 * Value is {@link android.telephony.data.ApnSetting#PROTOCOL_IP}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV6}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV4V6}, {@link android.telephony.data.ApnSetting#PROTOCOL_PPP}, {@link android.telephony.data.ApnSetting#PROTOCOL_NON_IP}, or {@link android.telephony.data.ApnSetting#PROTOCOL_UNSTRUCTURED}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setProtocolType(int protocolType) { throw new RuntimeException("Stub!"); }

/**
 * Set the authentication type.
 *
 * @param authType The authentication type
 * Value is {@link android.telephony.data.ApnSetting#AUTH_TYPE_NONE}, {@link android.telephony.data.ApnSetting#AUTH_TYPE_PAP}, {@link android.telephony.data.ApnSetting#AUTH_TYPE_CHAP}, or {@link android.telephony.data.ApnSetting#AUTH_TYPE_PAP_OR_CHAP}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setAuthType(int authType) { throw new RuntimeException("Stub!"); }

/**
 * Set the user name
 *
 * @param userName The user name
 * This value must never be {@code null}.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setUserName(@android.annotation.NonNull java.lang.String userName) { throw new RuntimeException("Stub!"); }

/**
 * Set the password
 *
 * @param password The password
 * This value must never be {@code null}.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setPassword(@android.annotation.NonNull java.lang.String password) { throw new RuntimeException("Stub!"); }

/**
 * Set the type
 *
 * @param type The profile type
 * Value is {@link android.telephony.data.DataProfile#TYPE_COMMON}, {@link android.telephony.data.DataProfile#TYPE_3GPP}, or {@link android.telephony.data.DataProfile#TYPE_3GPP2}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setType(int type) { throw new RuntimeException("Stub!"); }

/**
 * Enable the data profile
 *
 * @param isEnabled {@code true} to enable the data profile, otherwise disable.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder enable(boolean isEnabled) { throw new RuntimeException("Stub!"); }

/**
 * Set the supported APN types bitmask.
 *
 * @param supportedApnTypesBitmask The supported APN types bitmask.
 * Value is either <code>0</code> or a combination of {@link android.telephony.data.ApnSetting#TYPE_DEFAULT}, {@link android.telephony.data.ApnSetting#TYPE_MMS}, {@link android.telephony.data.ApnSetting#TYPE_SUPL}, {@link android.telephony.data.ApnSetting#TYPE_DUN}, {@link android.telephony.data.ApnSetting#TYPE_HIPRI}, {@link android.telephony.data.ApnSetting#TYPE_FOTA}, {@link android.telephony.data.ApnSetting#TYPE_IMS}, {@link android.telephony.data.ApnSetting#TYPE_CBS}, {@link android.telephony.data.ApnSetting#TYPE_IA}, {@link android.telephony.data.ApnSetting#TYPE_EMERGENCY}, and {@link android.telephony.data.ApnSetting#TYPE_MCX}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setSupportedApnTypesBitmask(int supportedApnTypesBitmask) { throw new RuntimeException("Stub!"); }

/**
 * Set the connection protocol type for roaming.
 *
 * @param protocolType The connection protocol defined in 3GPP TS 27.007 section 10.1.1.
 * Value is {@link android.telephony.data.ApnSetting#PROTOCOL_IP}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV6}, {@link android.telephony.data.ApnSetting#PROTOCOL_IPV4V6}, {@link android.telephony.data.ApnSetting#PROTOCOL_PPP}, {@link android.telephony.data.ApnSetting#PROTOCOL_NON_IP}, or {@link android.telephony.data.ApnSetting#PROTOCOL_UNSTRUCTURED}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setRoamingProtocolType(int protocolType) { throw new RuntimeException("Stub!"); }

/**
 * Set the bearer bitmask indicating the applicable networks for this data profile.
 *
 * @param bearerBitmask The bearer bitmask indicating the applicable networks for this data
 * profile.
 * Value is either <code>0</code> or a combination of {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_LTE_CA}, and {@link android.telephony.TelephonyManager#NETWORK_TYPE_BITMASK_NR}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setBearerBitmask(int bearerBitmask) { throw new RuntimeException("Stub!"); }

/**
 * Set the maximum transmission unit (MTU) size in bytes.
 *
 * @param mtu The maximum transmission unit (MTU) size in bytes.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setMtu(int mtu) { throw new RuntimeException("Stub!"); }

/**
 * Set data profile as preferred/non-preferred.
 *
 * @param isPreferred {@code true} if this data profile was used to bring up the last
 * default (i.e internet) data connection successfully, or the one chosen by the user in
 * Settings' APN editor. For one carrier there can be only one profiled preferred.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setPreferred(boolean isPreferred) { throw new RuntimeException("Stub!"); }

/**
 * Set data profile as persistent/non-persistent
 *
 * @param isPersistent {@code true} if this data profile was used to bring up the last
 * default (i.e internet) data connection successfully.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile.Builder setPersistent(boolean isPersistent) { throw new RuntimeException("Stub!"); }

/**
 * Build the DataProfile object
 *
 * @return The data profile object
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.data.DataProfile build() { throw new RuntimeException("Stub!"); }
}

}

