/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.display;

import java.time.LocalDate;

/**
 * AmbientBrightnessDayStats stores and manipulates brightness stats over a single day.
 * {@see DisplayManager.getAmbientBrightnessStats()}
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AmbientBrightnessDayStats implements android.os.Parcelable {

AmbientBrightnessDayStats(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/**
 * @return The {@link LocalDate} for which brightness stats are being tracked.
 * @apiSince REL
 */

public java.time.LocalDate getLocalDate() { throw new RuntimeException("Stub!"); }

/**
 * @return Aggregated stats of time spent (in seconds) in various buckets.
 * @apiSince REL
 */

public float[] getStats() { throw new RuntimeException("Stub!"); }

/**
 * Returns the bucket boundaries (in lux) used for creating buckets. For eg., if the bucket
 * boundaries array is {b1, b2, b3}, the buckets will be [b1, b2), [b2, b3), [b3, inf).
 *
 * @return The list of bucket boundaries.
 * @apiSince REL
 */

public float[] getBucketBoundaries() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.display.AmbientBrightnessDayStats> CREATOR;
static { CREATOR = null; }
}

