/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app.backup;

import java.util.Set;

/**
 * Interface for managing a restore session.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class RestoreSession {

RestoreSession() { throw new RuntimeException("Stub!"); }

/**
 * Ask the current transport what the available restore sets are.
 *
 * @param observer a RestoreObserver object whose restoreSetsAvailable() method will
 *   be called on the application's main thread in order to supply the results of
 *   the restore set lookup by the backup transport.  This parameter must not be
 *   null.
 * @param monitor a BackupManagerMonitor object will supply data about important events.
 * @return Zero on success, nonzero on error.  The observer's restoreSetsAvailable()
 *   method will only be called if this method returned zero.
 * @apiSince REL
 */

public int getAvailableRestoreSets(android.app.backup.RestoreObserver observer, android.app.backup.BackupManagerMonitor monitor) { throw new RuntimeException("Stub!"); }

/**
 * Ask the current transport what the available restore sets are.
 *
 * @param observer a RestoreObserver object whose restoreSetsAvailable() method will
 *   be called on the application's main thread in order to supply the results of
 *   the restore set lookup by the backup transport.  This parameter must not be
 *   null.
 * @return Zero on success, nonzero on error.  The observer's restoreSetsAvailable()
 *   method will only be called if this method returned zero.
 * @apiSince REL
 */

public int getAvailableRestoreSets(android.app.backup.RestoreObserver observer) { throw new RuntimeException("Stub!"); }

/**
 * Restore the given set onto the device, replacing the current data of any app
 * contained in the restore set with the data previously backed up.
 *
 * <p>Callers must hold the android.permission.BACKUP permission to use this method.
 *
 * @return Zero on success; nonzero on error.  The observer will only receive
 *   progress callbacks if this method returned zero.
 * @param token The token from {@link #getAvailableRestoreSets()} corresponding to
 *   the restore set that should be used.
 * @param observer If non-null, this binder points to an object that will receive
 *   progress callbacks during the restore operation.
 * @param monitor If non-null, this binder points to an object that will receive
 *   progress callbacks during the restore operation.
 * @apiSince REL
 */

public int restoreAll(long token, android.app.backup.RestoreObserver observer, android.app.backup.BackupManagerMonitor monitor) { throw new RuntimeException("Stub!"); }

/**
 * Restore the given set onto the device, replacing the current data of any app
 * contained in the restore set with the data previously backed up.
 *
 * <p>Callers must hold the android.permission.BACKUP permission to use this method.
 *
 * @return Zero on success; nonzero on error.  The observer will only receive
 *   progress callbacks if this method returned zero.
 * @param token The token from {@link #getAvailableRestoreSets()} corresponding to
 *   the restore set that should be used.
 * @param observer If non-null, this binder points to an object that will receive
 *   progress callbacks during the restore operation.
 * @apiSince REL
 */

public int restoreAll(long token, android.app.backup.RestoreObserver observer) { throw new RuntimeException("Stub!"); }

/**
 * Restore select packages from the given set onto the device, replacing the
 * current data of any app contained in the set with the data previously
 * backed up.
 *
 * <p>Callers must hold the android.permission.BACKUP permission to use this method.
 *
 * @return Zero on success, nonzero on error. The observer will only receive
 *   progress callbacks if this method returned zero.
 * @param token The token from {@link getAvailableRestoreSets()} corresponding to
 *   the restore set that should be used.
 * @param observer If non-null, this binder points to an object that will receive
 *   progress callbacks during the restore operation.
 * This value may be {@code null}.
 * @param packages The set of packages for which to attempt a restore.  Regardless of
 *   the contents of the actual back-end dataset named by {@code token}, only
 *   applications mentioned in this list will have their data restored.
 * This value must never be {@code null}.
 * @param monitor If non-null, this binder points to an object that will receive
 *   progress callbacks during the restore operation containing detailed information on any
 *   failures or important decisions made by {@link BackupManager}.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public int restorePackages(long token, @android.annotation.Nullable android.app.backup.RestoreObserver observer, @android.annotation.NonNull java.util.Set<java.lang.String> packages, @android.annotation.Nullable android.app.backup.BackupManagerMonitor monitor) { throw new RuntimeException("Stub!"); }

/**
 * Restore select packages from the given set onto the device, replacing the
 * current data of any app contained in the set with the data previously
 * backed up.
 *
 * <p>Callers must hold the android.permission.BACKUP permission to use this method.
 *
 * @return Zero on success, nonzero on error. The observer will only receive
 *   progress callbacks if this method returned zero.
 * @param token The token from {@link getAvailableRestoreSets()} corresponding to
 *   the restore set that should be used.
 * @param observer If non-null, this binder points to an object that will receive
 *   progress callbacks during the restore operation.
 * This value may be {@code null}.
 * @param packages The set of packages for which to attempt a restore.  Regardless of
 *   the contents of the actual back-end dataset named by {@code token}, only
 *   applications mentioned in this list will have their data restored.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public int restorePackages(long token, @android.annotation.Nullable android.app.backup.RestoreObserver observer, @android.annotation.NonNull java.util.Set<java.lang.String> packages) { throw new RuntimeException("Stub!"); }

/**
 * Restore a single application from backup.  The data will be restored from the
 * current backup dataset if the given package has stored data there, or from
 * the dataset used during the last full device setup operation if the current
 * backup dataset has no matching data.  If no backup data exists for this package
 * in either source, a nonzero value will be returned.
 *
 * <p class="caution">Note: Unlike other restore operations, this method doesn't terminate the
 * application after the restore. The application continues running to receive the
 * {@link RestoreObserver} callbacks on the {@code observer} argument.
 *
 * @return Zero on success; nonzero on error.  The observer will only receive
 *   progress callbacks if this method returned zero.
 * @param packageName The name of the package whose data to restore.  If this is
 *   not the name of the caller's own package, then the android.permission.BACKUP
 *   permission must be held.
 * @param observer If non-null, this binder points to an object that will receive
 *   progress callbacks during the restore operation.
 *
 * @param monitor If non-null, this binder points to an object that will receive
 *   event callbacks during the restore operation.
 * @apiSince REL
 */

public int restorePackage(java.lang.String packageName, android.app.backup.RestoreObserver observer, android.app.backup.BackupManagerMonitor monitor) { throw new RuntimeException("Stub!"); }

/**
 * Restore a single application from backup.  The data will be restored from the
 * current backup dataset if the given package has stored data there, or from
 * the dataset used during the last full device setup operation if the current
 * backup dataset has no matching data.  If no backup data exists for this package
 * in either source, a nonzero value will be returned.
 *
 * @return Zero on success; nonzero on error.  The observer will only receive
 *   progress callbacks if this method returned zero.
 * @param packageName The name of the package whose data to restore.  If this is
 *   not the name of the caller's own package, then the android.permission.BACKUP
 *   permission must be held.
 * @param observer If non-null, this binder points to an object that will receive
 *   progress callbacks during the restore operation.
 * @apiSince REL
 */

public int restorePackage(java.lang.String packageName, android.app.backup.RestoreObserver observer) { throw new RuntimeException("Stub!"); }

/**
 * End this restore session.  After this method is called, the RestoreSession
 * object is no longer valid.
 *
 * <p><b>Note:</b> The caller <i>must</i> invoke this method to end the restore session,
 *   even if {@link #restorePackage(String, RestoreObserver)} failed.
 * @apiSince REL
 */

public void endRestoreSession() { throw new RuntimeException("Stub!"); }
}

