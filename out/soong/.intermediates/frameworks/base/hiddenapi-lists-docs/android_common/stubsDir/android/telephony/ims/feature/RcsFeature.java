/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims.feature;


/**
 * Base implementation of the RcsFeature APIs. Any ImsService wishing to support RCS should extend
 * this class and provide implementations of the RcsFeature methods that they support.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class RcsFeature extends android.telephony.ims.feature.ImsFeature {

/** @apiSince REL */

public RcsFeature() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}
 * @apiSince REL
 */

public void changeEnabledCapabilities(android.telephony.ims.feature.CapabilityChangeRequest request, android.telephony.ims.feature.ImsFeature.CapabilityCallbackProxy c) { throw new RuntimeException("Stub!"); }

/**
 *{@inheritDoc}
 * @apiSince REL
 */

public void onFeatureRemoved() { throw new RuntimeException("Stub!"); }

/**
 *{@inheritDoc}
 * @apiSince REL
 */

public void onFeatureReady() { throw new RuntimeException("Stub!"); }
}

