/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app;

import android.Manifest;
import java.util.concurrent.Executor;
import android.os.Binder;
import android.app.usage.UsageStatsManager;
import android.os.UserManager;
import android.os.Process;

/**
 * API for interacting with "application operation" tracking.
 *
 * <p>This API is not generally intended for third party application developers; most
 * features are only available to system applications.
 * @apiSince 19
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AppOpsManager {

AppOpsManager() { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the permission associated with an operation, or null if there is not one.
 * @hide
 */

public static java.lang.String opToPermission(int op) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the permission associated with an operation, or null if there is not one.
 *
 * @param op The operation name.
 *
 * This value must never be {@code null}.
 * @hide
 */

@android.annotation.Nullable
public static java.lang.String opToPermission(@android.annotation.NonNull java.lang.String op) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the app op code for a permission, or null if there is not one.
 * This API is intended to be used for mapping runtime or appop permissions
 * to the corresponding app op.
 * @hide
 */

public static int permissionToOpCode(java.lang.String permission) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve the default mode for the app op.
 *
 * @param appOp The app op name
 *
 * This value must never be {@code null}.
 * @return the default mode for the app op
 *
 * @hide
 */

public static int opToDefaultMode(@android.annotation.NonNull java.lang.String appOp) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve current operation state for all applications.
 *
 * The mode of the ops returned are set for the package but may not reflect their effective
 * state due to UID policy or because it's controlled by a different master op.
 *
 * Use {@link #unsafeCheckOp(String, int, String)}} or {@link #noteOp(String, int, String)}
 * if the effective mode is needed.
 *
 * <br>
 * Requires {@link android.Manifest.permission#GET_APP_OPS_STATS}
 * @param ops The set of operations you are interested in, or null if you want all of them.
 * This value may be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public java.util.List<android.app.AppOpsManager.PackageOps> getPackagesForOps(@android.annotation.Nullable java.lang.String[] ops) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve current operation state for one application. The UID and the
 * package must match.
 *
 * The mode of the ops returned are set for the package but may not reflect their effective
 * state due to UID policy or because it's controlled by a different master op.
 *
 * Use {@link #unsafeCheckOp(String, int, String)}} or {@link #noteOp(String, int, String)}
 * if the effective mode is needed.
 *
 * <br>
 * Requires {@link android.Manifest.permission#GET_APP_OPS_STATS}
 * @param uid The uid of the application of interest.
 * @param packageName The name of the application of interest.
 * This value must never be {@code null}.
 * @param ops The set of operations you are interested in, or null if you want all of them.
 *
 * This value may be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public java.util.List<android.app.AppOpsManager.PackageOps> getOpsForPackage(int uid, @android.annotation.NonNull java.lang.String packageName, @android.annotation.Nullable java.lang.String... ops) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve historical app op stats for a period.
 *
 * <br>
 * Requires {@link android.Manifest.permission#GET_APP_OPS_STATS}
 * @param request A request object describing the data being queried for.
 * This value must never be {@code null}.
 * @param executor Executor on which to run the callback. If <code>null</code>
 *     the callback is executed on the default executor running on the main thread.
 * This value must never be {@code null}.
 * @param callback Callback on which to deliver the result.
 *
 * This value must never be {@code null}.
 * @throws IllegalArgumentException If any of the argument contracts is violated.
 *
 * @hide
 */

public void getHistoricalOps(@android.annotation.NonNull android.app.AppOpsManager.HistoricalOpsRequest request, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull java.util.function.Consumer<android.app.AppOpsManager.HistoricalOps> callback) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve historical app op stats for a period.
 *  <p>
 *  This method queries only the on disk state and the returned ops are raw,
 *  which is their times are relative to the history start as opposed to the
 *  epoch start.
 *
 * <br>
 * Requires android.Manifest.permission.MANAGE_APPOPS
 * @param request A request object describing the data being queried for.
 * This value must never be {@code null}.
 * @param executor Executor on which to run the callback. If <code>null</code>
 *     the callback is executed on the default executor running on the main thread.
 * This value may be {@code null}.
 * @param callback Callback on which to deliver the result.
 *
 * This value must never be {@code null}.
 * @throws IllegalArgumentException If any of the argument contracts is violated.
 *
 * @hide
 */

public void getHistoricalOpsFromDiskRaw(@android.annotation.NonNull android.app.AppOpsManager.HistoricalOpsRequest request, @android.annotation.Nullable java.util.concurrent.Executor executor, @android.annotation.NonNull java.util.function.Consumer<android.app.AppOpsManager.HistoricalOps> callback) { throw new RuntimeException("Stub!"); }

/**
 * Reloads the non historical state to allow testing the read/write path.
 *
 * <br>
 * Requires android.Manifest.permission.MANAGE_APPOPS
 * @hide
 */

public void reloadNonHistoricalState() { throw new RuntimeException("Stub!"); }

/**
 * Sets given app op in the specified mode for app ops in the UID.
 * This applies to all apps currently in the UID or installed in
 * this UID in the future.
 *
 * <br>
 * Requires android.Manifest.permission.MANAGE_APP_OPS_MODES
 * @param appOp The app op.
 * @param uid The UID for which to set the app.
 * @param mode The app op mode to set.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#MODE_ALLOWED}, {@link android.app.AppOpsManager#MODE_IGNORED}, {@link android.app.AppOpsManager#MODE_ERRORED}, {@link android.app.AppOpsManager#MODE_DEFAULT}, and {@link android.app.AppOpsManager#MODE_FOREGROUND}
 * @hide
 */

public void setUidMode(java.lang.String appOp, int uid, int mode) { throw new RuntimeException("Stub!"); }

/**
 *
 * Requires android.Manifest.permission.MANAGE_APP_OPS_MODES
 * @hide
 * @param mode Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#MODE_ALLOWED}, {@link android.app.AppOpsManager#MODE_IGNORED}, {@link android.app.AppOpsManager#MODE_ERRORED}, {@link android.app.AppOpsManager#MODE_DEFAULT}, and {@link android.app.AppOpsManager#MODE_FOREGROUND}
 */

public void setMode(int code, int uid, java.lang.String packageName, int mode) { throw new RuntimeException("Stub!"); }

/**
 * Change the operating mode for the given op in the given app package.  You must pass
 * in both the uid and name of the application whose mode is being modified; if these
 * do not match, the modification will not be applied.
 *
 * <br>
 * Requires android.Manifest.permission.MANAGE_APP_OPS_MODES
 * @param op The operation to modify.  One of the OPSTR_* constants.
 * @param uid The user id of the application whose mode will be changed.
 * @param packageName The name of the application package name whose mode will
 * be changed.
 * @hide

 * @param mode Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#MODE_ALLOWED}, {@link android.app.AppOpsManager#MODE_IGNORED}, {@link android.app.AppOpsManager#MODE_ERRORED}, {@link android.app.AppOpsManager#MODE_DEFAULT}, and {@link android.app.AppOpsManager#MODE_FOREGROUND}
 */

public void setMode(java.lang.String op, int uid, java.lang.String packageName, int mode) { throw new RuntimeException("Stub!"); }

/**
 * Gets the app op name associated with a given permission.
 * The app op name is one of the public constants defined
 * in this class such as {@link #OPSTR_COARSE_LOCATION}.
 * This API is intended to be used for mapping runtime
 * permissions to the corresponding app op.
 *
 * @param permission The permission.
 * @return The app op associated with the permission or null.
 * @apiSince 23
 */

public static java.lang.String permissionToOp(java.lang.String permission) { throw new RuntimeException("Stub!"); }

/**
 * Monitor for changes to the operating mode for the given op in the given app package.
 * You can watch op changes only for your UID.
 *
 * @param op The operation to monitor, one of OPSTR_*.
 * This value must never be {@code null}.
 * @param packageName The name of the application to monitor.
 * This value may be {@code null}.
 * @param callback Where to report changes.
 
 * This value must never be {@code null}.
 * @apiSince 19
 */

public void startWatchingMode(@androidx.annotation.RecentlyNonNull java.lang.String op, @androidx.annotation.RecentlyNullable java.lang.String packageName, @androidx.annotation.RecentlyNonNull android.app.AppOpsManager.OnOpChangedListener callback) { throw new RuntimeException("Stub!"); }

/**
 * Monitor for changes to the operating mode for the given op in the given app package.
 * You can watch op changes only for your UID.
 *
 * @param op The operation to monitor, one of OPSTR_*.
 * This value must never be {@code null}.
 * @param packageName The name of the application to monitor.
 * This value may be {@code null}.
 * @param flags Option flags: any combination of {@link #WATCH_FOREGROUND_CHANGES} or 0.
 * @param callback Where to report changes.
 
 * This value must never be {@code null}.
 * @apiSince 29
 */

public void startWatchingMode(@android.annotation.NonNull java.lang.String op, @android.annotation.Nullable java.lang.String packageName, int flags, @android.annotation.NonNull android.app.AppOpsManager.OnOpChangedListener callback) { throw new RuntimeException("Stub!"); }

/**
 * Stop monitoring that was previously started with {@link #startWatchingMode}.  All
 * monitoring associated with this callback will be removed.
 
 * @param callback This value must never be {@code null}.
 * @apiSince 19
 */

public void stopWatchingMode(@androidx.annotation.RecentlyNonNull android.app.AppOpsManager.OnOpChangedListener callback) { throw new RuntimeException("Stub!"); }

/**
 * Start watching for changes to the active state of app ops. An app op may be
 * long running and it has a clear start and stop delimiters. If an op is being
 * started or stopped by any package you will get a callback. To change the
 * watched ops for a registered callback you need to unregister and register it
 * again.
 *
 * <p> If you don't hold the {@link android.Manifest.permission#WATCH_APPOPS} permission
 * you can watch changes only for your UID.
 *
 * @param ops The ops to watch.
 * This value must never be {@code null}.
 * @param callback Where to report changes.
 *
 * This value must never be {@code null}.
 * @see #isOperationActive(int, int, String)
 * @see #stopWatchingActive(OnOpActiveChangedListener)
 * @see #startOp(int, int, String)
 * @see #finishOp(int, int, String)
 *
 * @hide
 */

public void startWatchingActive(@android.annotation.NonNull int[] ops, @android.annotation.NonNull android.app.AppOpsManager.OnOpActiveChangedListener callback) { throw new RuntimeException("Stub!"); }

/**
 * Stop watching for changes to the active state of an app op. An app op may be
 * long running and it has a clear start and stop delimiters. Unregistering a
 * non-registered callback has no effect.
 *
 * @see #isOperationActive#(int, int, String)
 * @see #startWatchingActive(int[], OnOpActiveChangedListener)
 * @see #startOp(int, int, String)
 * @see #finishOp(int, int, String)
 *
 * @hide

 * @param callback This value must never be {@code null}.
 */

public void stopWatchingActive(@android.annotation.NonNull android.app.AppOpsManager.OnOpActiveChangedListener callback) { throw new RuntimeException("Stub!"); }

/**
 * {@hide}

 * @param op This value must never be {@code null}.
 */

public static int strOpToOp(@android.annotation.NonNull java.lang.String op) { throw new RuntimeException("Stub!"); }

/**
 * Do a quick check for whether an application might be able to perform an operation.
 * This is <em>not</em> a security check; you must use {@link #noteOp(String, int, String)}
 * or {@link #startOp(String, int, String)} for your actual security checks, which also
 * ensure that the given uid and package name are consistent.  This function can just be
 * used for a quick check to see if an operation has been disabled for the application,
 * as an early reject of some work.  This does not modify the time stamp or other data
 * about the operation.
 *
 * <p>Important things this will not do (which you need to ultimate use
 * {@link #noteOp(String, int, String)} or {@link #startOp(String, int, String)} to cover):</p>
 * <ul>
 *     <li>Verifying the uid and package are consistent, so callers can't spoof
 *     their identity.</li>
 *     <li>Taking into account the current foreground/background state of the
 *     app; apps whose mode varies by this state will always be reported
 *     as {@link #MODE_ALLOWED}.</li>
 * </ul>
 *
 * @param op The operation to check.  One of the OPSTR_* constants.
 * This value must never be {@code null}.
 * @param uid The user id of the application attempting to perform the operation.
 * @param packageName The name of the application attempting to perform the operation.
 * This value must never be {@code null}.
 * @return Returns {@link #MODE_ALLOWED} if the operation is allowed, or
 * {@link #MODE_IGNORED} if it is not allowed and should be silently ignored (without
 * causing the app to crash).
 * @throws SecurityException If the app has been configured to crash on this op.
 * @apiSince 29
 */

public int unsafeCheckOp(@android.annotation.NonNull java.lang.String op, int uid, @android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated Renamed to {@link #unsafeCheckOp(String, int, String)}.
 
 * @param op This value must never be {@code null}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 19
 * @deprecatedSince 29
 */

@Deprecated
public int checkOp(@androidx.annotation.RecentlyNonNull java.lang.String op, int uid, @androidx.annotation.RecentlyNonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Like {@link #checkOp} but instead of throwing a {@link SecurityException} it
 * returns {@link #MODE_ERRORED}.
 
 * @param op This value must never be {@code null}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 29
 */

public int unsafeCheckOpNoThrow(@android.annotation.NonNull java.lang.String op, int uid, @android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated Renamed to {@link #unsafeCheckOpNoThrow(String, int, String)}.
 
 * @param op This value must never be {@code null}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 19
 * @deprecatedSince 29
 */

@Deprecated
public int checkOpNoThrow(@androidx.annotation.RecentlyNonNull java.lang.String op, int uid, @androidx.annotation.RecentlyNonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Like {@link #checkOp} but returns the <em>raw</em> mode associated with the op.
 * Does not throw a security exception, does not translate {@link #MODE_FOREGROUND}.
 
 * @param op This value must never be {@code null}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 29
 */

public int unsafeCheckOpRaw(@android.annotation.NonNull java.lang.String op, int uid, @android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Like {@link #unsafeCheckOpNoThrow(String, int, String)} but returns the <em>raw</em>
 * mode associated with the op. Does not throw a security exception, does not translate
 * {@link #MODE_FOREGROUND}.
 
 * @param op This value must never be {@code null}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 29
 */

public int unsafeCheckOpRawNoThrow(@android.annotation.NonNull java.lang.String op, int uid, @android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Make note of an application performing an operation.  Note that you must pass
 * in both the uid and name of the application to be checked; this function will verify
 * that these two match, and if not, return {@link #MODE_IGNORED}.  If this call
 * succeeds, the last execution time of the operation for this app will be updated to
 * the current time.
 * @param op The operation to note.  One of the OPSTR_* constants.
 * This value must never be {@code null}.
 * @param uid The user id of the application attempting to perform the operation.
 * @param packageName The name of the application attempting to perform the operation.
 * This value must never be {@code null}.
 * @return Returns {@link #MODE_ALLOWED} if the operation is allowed, or
 * {@link #MODE_IGNORED} if it is not allowed and should be silently ignored (without
 * causing the app to crash).
 * @throws SecurityException If the app has been configured to crash on this op.
 * @apiSince 19
 */

public int noteOp(@androidx.annotation.RecentlyNonNull java.lang.String op, int uid, @androidx.annotation.RecentlyNonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Like {@link #noteOp} but instead of throwing a {@link SecurityException} it
 * returns {@link #MODE_ERRORED}.
 
 * @param op This value must never be {@code null}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 19
 */

public int noteOpNoThrow(@androidx.annotation.RecentlyNonNull java.lang.String op, int uid, @androidx.annotation.RecentlyNonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Make note of an application performing an operation on behalf of another
 * application when handling an IPC. Note that you must pass the package name
 * of the application that is being proxied while its UID will be inferred from
 * the IPC state; this function will verify that the calling uid and proxied
 * package name match, and if not, return {@link #MODE_IGNORED}. If this call
 * succeeds, the last execution time of the operation for the proxied app and
 * your app will be updated to the current time.
 * @param op The operation to note.  One of the OPSTR_* constants.
 * This value must never be {@code null}.
 * @param proxiedPackageName The name of the application calling into the proxy application.
 * This value must never be {@code null}.
 * @return Returns {@link #MODE_ALLOWED} if the operation is allowed, or
 * {@link #MODE_IGNORED} if it is not allowed and should be silently ignored (without
 * causing the app to crash).
 * @throws SecurityException If the app has been configured to crash on this op.
 * @apiSince 23
 */

public int noteProxyOp(@androidx.annotation.RecentlyNonNull java.lang.String op, @androidx.annotation.RecentlyNonNull java.lang.String proxiedPackageName) { throw new RuntimeException("Stub!"); }

/**
 * Like {@link #noteProxyOp(String, String)} but instead
 * of throwing a {@link SecurityException} it returns {@link #MODE_ERRORED}.
 *
 * <p>This API requires the package with the {@code proxiedPackageName} to belongs to
 * {@link Binder#getCallingUid()}.
 
 * @param op This value must never be {@code null}.
 
 * @param proxiedPackageName This value must never be {@code null}.
 * @apiSince 23
 */

public int noteProxyOpNoThrow(@androidx.annotation.RecentlyNonNull java.lang.String op, @androidx.annotation.RecentlyNonNull java.lang.String proxiedPackageName) { throw new RuntimeException("Stub!"); }

/**
 * Like {@link #noteProxyOpNoThrow(String, String)} but allows to specify the proxied uid.
 *
 * <p>This API requires package with the {@code proxiedPackageName} to belong to
 * {@code proxiedUid}.
 *
 * @param op The op to note
 * This value must never be {@code null}.
 * @param proxiedPackageName The package to note the op for or {@code null} if the op should be
 *                           noted for the "android" package
 * This value may be {@code null}.
 * @param proxiedUid The uid the package belongs to
 * @apiSince 29
 */

public int noteProxyOpNoThrow(@android.annotation.NonNull java.lang.String op, @android.annotation.Nullable java.lang.String proxiedPackageName, int proxiedUid) { throw new RuntimeException("Stub!"); }

/**
 * Report that an application has started executing a long-running operation.  Note that you
 * must pass in both the uid and name of the application to be checked; this function will
 * verify that these two match, and if not, return {@link #MODE_IGNORED}.  If this call
 * succeeds, the last execution time of the operation for this app will be updated to
 * the current time and the operation will be marked as "running".  In this case you must
 * later call {@link #finishOp(String, int, String)} to report when the application is no
 * longer performing the operation.
 * @param op The operation to start.  One of the OPSTR_* constants.
 * This value must never be {@code null}.
 * @param uid The user id of the application attempting to perform the operation.
 * @param packageName The name of the application attempting to perform the operation.
 * This value must never be {@code null}.
 * @return Returns {@link #MODE_ALLOWED} if the operation is allowed, or
 * {@link #MODE_IGNORED} if it is not allowed and should be silently ignored (without
 * causing the app to crash).
 * @throws SecurityException If the app has been configured to crash on this op.
 * @apiSince 19
 */

public int startOp(@androidx.annotation.RecentlyNonNull java.lang.String op, int uid, @androidx.annotation.RecentlyNonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Like {@link #startOp} but instead of throwing a {@link SecurityException} it
 * returns {@link #MODE_ERRORED}.
 
 * @param op This value must never be {@code null}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 19
 */

public int startOpNoThrow(@androidx.annotation.RecentlyNonNull java.lang.String op, int uid, @androidx.annotation.RecentlyNonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Report that an application is no longer performing an operation that had previously
 * been started with {@link #startOp(String, int, String)}.  There is no validation of input
 * or result; the parameters supplied here must be the exact same ones previously passed
 * in when starting the operation.
 
 * @param op This value must never be {@code null}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 19
 */

public void finishOp(@androidx.annotation.RecentlyNonNull java.lang.String op, int uid, @androidx.annotation.RecentlyNonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Do a quick check to validate if a package name belongs to a UID.
 *
 * @throws SecurityException if the package name doesn't belong to the given
 *             UID, or if ownership cannot be verified.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 19
 */

public void checkPackage(int uid, @androidx.annotation.RecentlyNonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether the given op for a UID and package is active.
 *
 * <p> If you don't hold the {@link android.Manifest.permission#WATCH_APPOPS} permission
 * you can query only for your UID.
 *
 * @see #startWatchingActive(int[], OnOpActiveChangedListener)
 * @see #stopWatchingMode(OnOpChangedListener)
 * @see #finishOp(int)
 * @see #startOp(int)
 *
 * @hide */

public boolean isOperationActive(int code, int uid, java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Configures the app ops persistence for testing.
 *
 * <br>
 * Requires android.Manifest.permission.MANAGE_APPOPS
 * @param mode The mode in which the historical registry operates.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#HISTORICAL_MODE_DISABLED}, {@link android.app.AppOpsManager#HISTORICAL_MODE_ENABLED_ACTIVE}, and {@link android.app.AppOpsManager#HISTORICAL_MODE_ENABLED_PASSIVE}
 * @param baseSnapshotInterval The base interval on which we would be persisting a snapshot of
 *   the historical data. The history is recursive where every subsequent step encompasses
 *   {@code compressionStep} longer interval with {@code compressionStep} distance between
 *    snapshots.
 * @param compressionStep The compression step in every iteration.
 *
 * @see #HISTORICAL_MODE_DISABLED
 * @see #HISTORICAL_MODE_ENABLED_ACTIVE
 * @see #HISTORICAL_MODE_ENABLED_PASSIVE
 *
 * @hide
 */

public void setHistoryParameters(int mode, long baseSnapshotInterval, int compressionStep) { throw new RuntimeException("Stub!"); }

/**
 * Offsets the history by the given duration.
 *
 * <br>
 * Requires android.Manifest.permission.MANAGE_APPOPS
 * @param offsetMillis The offset duration.
 *
 * @hide
 */

public void offsetHistory(long offsetMillis) { throw new RuntimeException("Stub!"); }

/**
 * Adds ops to the history directly. This could be useful for testing especially
 * when the historical registry operates in {@link #HISTORICAL_MODE_ENABLED_PASSIVE}
 * mode.
 *
 * <br>
 * Requires android.Manifest.permission.MANAGE_APPOPS
 * @param ops The ops to add to the history.
 *
 * This value must never be {@code null}.
 * @see #setHistoryParameters(int, long, int)
 * @see #HISTORICAL_MODE_ENABLED_PASSIVE
 *
 * @hide
 */

public void addHistoricalOps(@android.annotation.NonNull android.app.AppOpsManager.HistoricalOps ops) { throw new RuntimeException("Stub!"); }

/**
 * Resets the app ops persistence for testing.
 *
 * <br>
 * Requires android.Manifest.permission.MANAGE_APPOPS
 * @see #setHistoryParameters(int, long, int)
 *
 * @hide
 */

public void resetHistoryParameters() { throw new RuntimeException("Stub!"); }

/**
 * Clears all app ops history.
 *
 * <br>
 * Requires android.Manifest.permission.MANAGE_APPOPS
 * @hide
 */

public void clearHistory() { throw new RuntimeException("Stub!"); }

/**
 * Returns all supported operation names.
 * @hide
 */

public static java.lang.String[] getOpStrs() { throw new RuntimeException("Stub!"); }

/**
 * @return number of App ops
 * @hide
 */

public static int getNumOps() { throw new RuntimeException("Stub!"); }

/**
 * Mode in which app op history is completely disabled.
 * @hide
 */

public static final int HISTORICAL_MODE_DISABLED = 0; // 0x0

/**
 * Mode in which app op history is enabled and app ops performed by apps would
 * be tracked. This is the mode in which the feature is completely enabled.
 * @hide
 */

public static final int HISTORICAL_MODE_ENABLED_ACTIVE = 1; // 0x1

/**
 * Mode in which app op history is enabled but app ops performed by apps would
 * not be tracked and the only way to add ops to the history is via explicit calls
 * to dedicated APIs. This mode is useful for testing to allow full control of
 * the historical content.
 * @hide
 */

public static final int HISTORICAL_MODE_ENABLED_PASSIVE = 2; // 0x2

/**
 * Result from {@link #checkOp}, {@link #noteOp}, {@link #startOp}: the given caller is
 * allowed to perform the given operation.
 * @apiSince 19
 */

public static final int MODE_ALLOWED = 0; // 0x0

/**
 * Result from {@link #checkOp}, {@link #noteOp}, {@link #startOp}: the given caller should
 * use its default security check.  This mode is not normally used; it should only be used
 * with appop permissions, and callers must explicitly check for it and deal with it.
 * @apiSince 21
 */

public static final int MODE_DEFAULT = 3; // 0x3

/**
 * Result from {@link #checkOpNoThrow}, {@link #noteOpNoThrow}, {@link #startOpNoThrow}: the
 * given caller is not allowed to perform the given operation, and this attempt should
 * cause it to have a fatal error, typically a {@link SecurityException}.
 * @apiSince 19
 */

public static final int MODE_ERRORED = 2; // 0x2

/**
 * Special mode that means "allow only when app is in foreground."  This is <b>not</b>
 * returned from {@link #unsafeCheckOp}, {@link #noteOp}, {@link #startOp}.  Rather,
 * {@link #unsafeCheckOp} will always return {@link #MODE_ALLOWED} (because it is always
 * possible for it to be ultimately allowed, depending on the app's background state),
 * and {@link #noteOp} and {@link #startOp} will return {@link #MODE_ALLOWED} when the app
 * being checked is currently in the foreground, otherwise {@link #MODE_IGNORED}.
 *
 * <p>The only place you will this normally see this value is through
 * {@link #unsafeCheckOpRaw}, which returns the actual raw mode of the op.  Note that because
 * you can't know the current state of the app being checked (and it can change at any
 * point), you can only treat the result here as an indication that it will vary between
 * {@link #MODE_ALLOWED} and {@link #MODE_IGNORED} depending on changes in the background
 * state of the app.  You thus must always use {@link #noteOp} or {@link #startOp} to do
 * the actual check for access to the op.</p>
 * @apiSince 29
 */

public static final int MODE_FOREGROUND = 4; // 0x4

/**
 * Result from {@link #checkOp}, {@link #noteOp}, {@link #startOp}: the given caller is
 * not allowed to perform the given operation, and this attempt should
 * <em>silently fail</em> (it should not cause the app to crash).
 * @apiSince 19
 */

public static final int MODE_IGNORED = 1; // 0x1

/**
 * Accept call handover
 * @hide
 */

public static final java.lang.String OPSTR_ACCEPT_HANDOVER = "android:accept_handover";

/** @hide Interact with accessibility. */

public static final java.lang.String OPSTR_ACCESS_ACCESSIBILITY = "android:access_accessibility";

/** @hide */

public static final java.lang.String OPSTR_ACCESS_NOTIFICATIONS = "android:access_notifications";

/** Activate a VPN connection without user intervention. @hide */

public static final java.lang.String OPSTR_ACTIVATE_VPN = "android:activate_vpn";

/**
 * Required to access phone state related information.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_ADD_VOICEMAIL = "android:add_voicemail";

/**
 * Answer incoming phone calls
 * @apiSince 26
 */

public static final java.lang.String OPSTR_ANSWER_PHONE_CALLS = "android:answer_phone_calls";

/** @hide */

public static final java.lang.String OPSTR_ASSIST_SCREENSHOT = "android:assist_screenshot";

/** @hide */

public static final java.lang.String OPSTR_ASSIST_STRUCTURE = "android:assist_structure";

/** @hide */

public static final java.lang.String OPSTR_AUDIO_ACCESSIBILITY_VOLUME = "android:audio_accessibility_volume";

/** @hide */

public static final java.lang.String OPSTR_AUDIO_ALARM_VOLUME = "android:audio_alarm_volume";

/** @hide */

public static final java.lang.String OPSTR_AUDIO_BLUETOOTH_VOLUME = "android:audio_bluetooth_volume";

/** @hide */

public static final java.lang.String OPSTR_AUDIO_MASTER_VOLUME = "android:audio_master_volume";

/** @hide */

public static final java.lang.String OPSTR_AUDIO_MEDIA_VOLUME = "android:audio_media_volume";

/** @hide */

public static final java.lang.String OPSTR_AUDIO_NOTIFICATION_VOLUME = "android:audio_notification_volume";

/** @hide */

public static final java.lang.String OPSTR_AUDIO_RING_VOLUME = "android:audio_ring_volume";

/** @hide */

public static final java.lang.String OPSTR_AUDIO_VOICE_VOLUME = "android:audio_voice_volume";

/** @hide */

public static final java.lang.String OPSTR_BIND_ACCESSIBILITY_SERVICE = "android:bind_accessibility_service";

/**
 * Access to body sensors such as heart rate, etc.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_BODY_SENSORS = "android:body_sensors";

/**
 * Allows an application to initiate a phone call.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_CALL_PHONE = "android:call_phone";

/**
 * Required to be able to access the camera device.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_CAMERA = "android:camera";

/** @hide */

public static final java.lang.String OPSTR_CHANGE_WIFI_STATE = "android:change_wifi_state";

/**
 * Access to coarse location information.
 * @apiSince 19
 */

public static final java.lang.String OPSTR_COARSE_LOCATION = "android:coarse_location";

/**
 * Access to fine location information.
 * @apiSince 19
 */

public static final java.lang.String OPSTR_FINE_LOCATION = "android:fine_location";

/** @hide Get device accounts. */

public static final java.lang.String OPSTR_GET_ACCOUNTS = "android:get_accounts";

/**
 * Access to {@link android.app.usage.UsageStatsManager}.
 * @apiSince 21
 */

public static final java.lang.String OPSTR_GET_USAGE_STATS = "android:get_usage_stats";

/** @hide */

public static final java.lang.String OPSTR_GPS = "android:gps";

/** @hide */

public static final java.lang.String OPSTR_INSTANT_APP_START_FOREGROUND = "android:instant_app_start_foreground";

/** @hide Has a legacy (non-isolated) view of storage. */

public static final java.lang.String OPSTR_LEGACY_STORAGE = "android:legacy_storage";

/** @hide */

public static final java.lang.String OPSTR_MANAGE_IPSEC_TUNNELS = "android:manage_ipsec_tunnels";

/**
 * Inject mock location into the system.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_MOCK_LOCATION = "android:mock_location";

/**
 * Continually monitoring location data with a relatively high power request.
 * @apiSince 19
 */

public static final java.lang.String OPSTR_MONITOR_HIGH_POWER_LOCATION = "android:monitor_location_high_power";

/**
 * Continually monitoring location data.
 * @apiSince 19
 */

public static final java.lang.String OPSTR_MONITOR_LOCATION = "android:monitor_location";

/** @hide */

public static final java.lang.String OPSTR_MUTE_MICROPHONE = "android:mute_microphone";

/** @hide */

public static final java.lang.String OPSTR_NEIGHBORING_CELLS = "android:neighboring_cells";

/**
 * Access to picture-in-picture.
 * @apiSince 26
 */

public static final java.lang.String OPSTR_PICTURE_IN_PICTURE = "android:picture_in_picture";

/** @hide */

public static final java.lang.String OPSTR_PLAY_AUDIO = "android:play_audio";

/** @hide */

public static final java.lang.String OPSTR_POST_NOTIFICATION = "android:post_notification";

/**
 * Access APIs for diverting outgoing calls
 * @apiSince 26
 */

public static final java.lang.String OPSTR_PROCESS_OUTGOING_CALLS = "android:process_outgoing_calls";

/** @hide */

public static final java.lang.String OPSTR_PROJECT_MEDIA = "android:project_media";

/**
 * Allows an application to read the user's calendar data.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_READ_CALENDAR = "android:read_calendar";

/**
 * Allows an application to read the user's call log.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_READ_CALL_LOG = "android:read_call_log";

/**
 * Read previously received cell broadcast messages.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_READ_CELL_BROADCASTS = "android:read_cell_broadcasts";

/** @hide */

public static final java.lang.String OPSTR_READ_CLIPBOARD = "android:read_clipboard";

/**
 * Allows an application to read the user's contacts data.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_READ_CONTACTS = "android:read_contacts";

/**
 * Read external storage.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_READ_EXTERNAL_STORAGE = "android:read_external_storage";

/** @hide */

public static final java.lang.String OPSTR_READ_ICC_SMS = "android:read_icc_sms";

/** @apiSince 26 */

public static final java.lang.String OPSTR_READ_PHONE_NUMBERS = "android:read_phone_numbers";

/**
 * Required to access phone state related information.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_READ_PHONE_STATE = "android:read_phone_state";

/**
 * Allows an application to read SMS messages.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_READ_SMS = "android:read_sms";

/** @hide */

public static final java.lang.String OPSTR_RECEIVE_EMERGENCY_BROADCAST = "android:receive_emergency_broadcast";

/**
 * Allows an application to receive MMS messages.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_RECEIVE_MMS = "android:receive_mms";

/**
 * Allows an application to receive SMS messages.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_RECEIVE_SMS = "android:receive_sms";

/**
 * Allows an application to receive WAP push messages.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_RECEIVE_WAP_PUSH = "android:receive_wap_push";

/**
 * Required to be able to access the microphone device.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_RECORD_AUDIO = "android:record_audio";

/** @hide */

public static final java.lang.String OPSTR_REQUEST_DELETE_PACKAGES = "android:request_delete_packages";

/** @hide */

public static final java.lang.String OPSTR_REQUEST_INSTALL_PACKAGES = "android:request_install_packages";

/** @hide */

public static final java.lang.String OPSTR_RUN_ANY_IN_BACKGROUND = "android:run_any_in_background";

/** @hide */

public static final java.lang.String OPSTR_RUN_IN_BACKGROUND = "android:run_in_background";

/**
 * Allows an application to send SMS messages.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_SEND_SMS = "android:send_sms";

/** @hide */

public static final java.lang.String OPSTR_START_FOREGROUND = "android:start_foreground";

/**
 * Required to draw on top of other apps.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_SYSTEM_ALERT_WINDOW = "android:system_alert_window";

/** @hide */

public static final java.lang.String OPSTR_TAKE_AUDIO_FOCUS = "android:take_audio_focus";

/** @hide */

public static final java.lang.String OPSTR_TAKE_MEDIA_BUTTONS = "android:take_media_buttons";

/** @hide */

public static final java.lang.String OPSTR_TOAST_WINDOW = "android:toast_window";

/** @hide */

public static final java.lang.String OPSTR_TURN_SCREEN_ON = "android:turn_screen_on";

/**
 * Use the fingerprint API.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_USE_FINGERPRINT = "android:use_fingerprint";

/**
 * Access APIs for SIP calling over VOIP or WiFi
 * @apiSince 23
 */

public static final java.lang.String OPSTR_USE_SIP = "android:use_sip";

/** @hide */

public static final java.lang.String OPSTR_VIBRATE = "android:vibrate";

/** @hide */

public static final java.lang.String OPSTR_WAKE_LOCK = "android:wake_lock";

/** @hide */

public static final java.lang.String OPSTR_WIFI_SCAN = "android:wifi_scan";

/**
 * Allows an application to write to the user's calendar data.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_WRITE_CALENDAR = "android:write_calendar";

/**
 * Allows an application to write to the user's call log.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_WRITE_CALL_LOG = "android:write_call_log";

/** @hide */

public static final java.lang.String OPSTR_WRITE_CLIPBOARD = "android:write_clipboard";

/**
 * Allows an application to write to the user's contacts data.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_WRITE_CONTACTS = "android:write_contacts";

/**
 * Write external storage.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_WRITE_EXTERNAL_STORAGE = "android:write_external_storage";

/** @hide */

public static final java.lang.String OPSTR_WRITE_ICC_SMS = "android:write_icc_sms";

/**
 * Required to write/modify/update system settingss.
 * @apiSince 23
 */

public static final java.lang.String OPSTR_WRITE_SETTINGS = "android:write_settings";

/** @hide */

public static final java.lang.String OPSTR_WRITE_SMS = "android:write_sms";

/** @hide */

public static final java.lang.String OPSTR_WRITE_WALLPAPER = "android:write_wallpaper";

/** @hide Access to coarse location information. */

public static final int OP_COARSE_LOCATION = 0; // 0x0

/**
 * Flags: all operations. These include operations matched
 * by {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}.
 *
 * @hide
 */

public static final int OP_FLAGS_ALL = 31; // 0x1f

/**
 * Flags: all trusted operations which is ones either the app did {@link #OP_FLAG_SELF},
 * or it was blamed for by a trusted app {@link #OP_FLAG_TRUSTED_PROXIED}, or ones the
 * app if untrusted blamed on other apps {@link #OP_FLAG_UNTRUSTED_PROXY}.
 *
 * @hide
 */

public static final int OP_FLAGS_ALL_TRUSTED = 13; // 0xd

/**
 * Flag: non proxy operations. These are operations
 * performed on behalf of the app itself and not on behalf of
 * another one.
 *
 * @hide
 */

public static final int OP_FLAG_SELF = 1; // 0x1

/**
 * Flag: trusted proxied operations. These are operations
 * performed by a trusted other app on behalf of an app.
 * Which is work an app was blamed for by a trusted app.
 *
 * @hide
 */

public static final int OP_FLAG_TRUSTED_PROXIED = 8; // 0x8

/**
 * Flag: trusted proxy operations. These are operations
 * performed on behalf of another app by a trusted app.
 * Which is work a trusted app blames on another app.
 *
 * @hide
 */

public static final int OP_FLAG_TRUSTED_PROXY = 2; // 0x2

/**
 * Flag: untrusted proxied operations. These are operations
 * performed by an untrusted other app on behalf of an app.
 * Which is work an app was blamed for by an untrusted app.
 *
 * @hide
 */

public static final int OP_FLAG_UNTRUSTED_PROXIED = 16; // 0x10

/**
 * Flag: untrusted proxy operations. These are operations
 * performed on behalf of another app by an untrusted app.
 * Which is work an untrusted app blames on another app.
 *
 * @hide
 */

public static final int OP_FLAG_UNTRUSTED_PROXY = 4; // 0x4

/** @hide */

public static final int OP_RECORD_AUDIO = 27; // 0x1b

/** @hide Any app start foreground service. */

public static final int OP_START_FOREGROUND = 76; // 0x4c

/** @hide Required to draw on top of other apps. */

public static final int OP_SYSTEM_ALERT_WINDOW = 24; // 0x18

/**
 * Uid state: The UID is a background app. The lower the UID
 * state the more important the UID is for the user.
 * @hide
 */

public static final int UID_STATE_BACKGROUND = 600; // 0x258

/**
 * Uid state: The UID is a cached app. The lower the UID
 * state the more important the UID is for the user.
 * @hide
 */

public static final int UID_STATE_CACHED = 700; // 0x2bc

/**
 * Uid state: The UID is a foreground app. The lower the UID
 * state the more important the UID is for the user.
 * @hide
 */

public static final int UID_STATE_FOREGROUND = 500; // 0x1f4

/**
 * Uid state: The UID is running a foreground service. The lower the UID
 * state the more important the UID is for the user.
 * @hide
 */

public static final int UID_STATE_FOREGROUND_SERVICE = 400; // 0x190

/**
 * Uid state: The UID is running a foreground service of location type.
 * The lower the UID state the more important the UID is for the user.
 * @hide
 */

public static final int UID_STATE_FOREGROUND_SERVICE_LOCATION = 300; // 0x12c

/**
 * Uid state: The UID is a foreground persistent app. The lower the UID
 * state the more important the UID is for the user.
 * @hide
 */

public static final int UID_STATE_PERSISTENT = 100; // 0x64

/**
 * Uid state: The UID is top foreground app. The lower the UID
 * state the more important the UID is for the user.
 * @hide
 */

public static final int UID_STATE_TOP = 200; // 0xc8

/**
 * Flag for {@link #startWatchingMode(String, String, int, OnOpChangedListener)}:
 * Also get reports if the foreground state of an op's uid changes.  This only works
 * when watching a particular op, not when watching a package.
 * @apiSince 29
 */

public static final int WATCH_FOREGROUND_CHANGES = 1; // 0x1
/**
 * This class represents historical information about an app op.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HistoricalOp implements android.os.Parcelable {

/** @hide */

HistoricalOp(int op) { throw new RuntimeException("Stub!"); }

/**
 * Gets the op name.
 *
 * @return The op name.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getOpName() { throw new RuntimeException("Stub!"); }

/**
 * Gets the number times the op was accessed (performed) in the foreground.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The times the op was accessed in the foreground.
 *
 * @see #getBackgroundAccessCount(int)
 * @see #getAccessCount(int, int, int)
 * @apiSince REL
 */

public long getForegroundAccessCount(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Gets the number times the op was accessed (performed) in the background.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The times the op was accessed in the background.
 *
 * @see #getForegroundAccessCount(int)
 * @see #getAccessCount(int, int, int)
 * @apiSince REL
 */

public long getBackgroundAccessCount(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Gets the number times the op was accessed (performed) for a
 * range of uid states.
 *
 * @param fromUidState The UID state from which to query. Could be one of
 * {@link #UID_STATE_PERSISTENT}, {@link #UID_STATE_TOP},
 * {@link #UID_STATE_FOREGROUND_SERVICE_LOCATION},
 * {@link #UID_STATE_FOREGROUND_SERVICE}, {@link #UID_STATE_FOREGROUND},
 * {@link #UID_STATE_BACKGROUND}, {@link #UID_STATE_CACHED}.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param toUidState The UID state to which to query.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 *
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The times the op was accessed for the given UID state.
 *
 * @see #getForegroundAccessCount(int)
 * @see #getBackgroundAccessCount(int)
 * @apiSince REL
 */

public long getAccessCount(int fromUidState, int toUidState, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Gets the number times the op was rejected in the foreground.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The times the op was rejected in the foreground.
 *
 * @see #getBackgroundRejectCount(int)
 * @see #getRejectCount(int, int, int)
 * @apiSince REL
 */

public long getForegroundRejectCount(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Gets the number times the op was rejected in the background.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The times the op was rejected in the background.
 *
 * @see #getForegroundRejectCount(int)
 * @see #getRejectCount(int, int, int)
 * @apiSince REL
 */

public long getBackgroundRejectCount(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Gets the number times the op was rejected for a given range of UID states.
 *
 * @param fromUidState The UID state from which to query. Could be one of
 * {@link #UID_STATE_PERSISTENT}, {@link #UID_STATE_TOP},
 * {@link #UID_STATE_FOREGROUND_SERVICE_LOCATION},
 * {@link #UID_STATE_FOREGROUND_SERVICE}, {@link #UID_STATE_FOREGROUND},
 * {@link #UID_STATE_BACKGROUND}, {@link #UID_STATE_CACHED}.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param toUidState The UID state to which to query.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 *
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The times the op was rejected for the given UID state.
 *
 * @see #getForegroundRejectCount(int)
 * @see #getBackgroundRejectCount(int)
 * @apiSince REL
 */

public long getRejectCount(int fromUidState, int toUidState, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Gets the total duration the app op was accessed (performed) in the foreground.
 * The duration is in wall time.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The total duration the app op was accessed in the foreground.
 *
 * @see #getBackgroundAccessDuration(int)
 * @see #getAccessDuration(int, int, int)
 * @apiSince REL
 */

public long getForegroundAccessDuration(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Gets the total duration the app op was accessed (performed) in the background.
 * The duration is in wall time.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The total duration the app op was accessed in the background.
 *
 * @see #getForegroundAccessDuration(int)
 * @see #getAccessDuration(int, int, int)
 * @apiSince REL
 */

public long getBackgroundAccessDuration(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Gets the total duration the app op was accessed (performed) for a given
 * range of UID states. The duration is in wall time.
 *
 * @param fromUidState The UID state from which to query. Could be one of
 * {@link #UID_STATE_PERSISTENT}, {@link #UID_STATE_TOP},
 * {@link #UID_STATE_FOREGROUND_SERVICE_LOCATION},
 * {@link #UID_STATE_FOREGROUND_SERVICE}, {@link #UID_STATE_FOREGROUND},
 * {@link #UID_STATE_BACKGROUND}, {@link #UID_STATE_CACHED}.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param toUidState The UID state from which to query.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 *
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The total duration the app op was accessed for the given UID state.
 *
 * @see #getForegroundAccessDuration(int)
 * @see #getBackgroundAccessDuration(int)
 * @apiSince REL
 */

public long getAccessDuration(int fromUidState, int toUidState, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.AppOpsManager.HistoricalOp> CREATOR;
static { CREATOR = null; }
}

/**
 * This class represents historical app op state of all UIDs for a given time interval.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HistoricalOps implements android.os.Parcelable {

/** @hide */

public HistoricalOps(long beginTimeMillis, long endTimeMillis) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * @param packageName This value must never be {@code null}.
 
 * @param uidState Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}

 * @param flags Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 */

public void increaseAccessCount(int opCode, int uid, @android.annotation.NonNull java.lang.String packageName, int uidState, int flags, long increment) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * @param packageName This value must never be {@code null}.
 
 * @param uidState Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}

 * @param flags Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 */

public void increaseRejectCount(int opCode, int uid, @android.annotation.NonNull java.lang.String packageName, int uidState, int flags, long increment) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 * @param packageName This value must never be {@code null}.
 
 * @param uidState Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}

 * @param flags Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 */

public void increaseAccessDuration(int opCode, int uid, @android.annotation.NonNull java.lang.String packageName, int uidState, int flags, long increment) { throw new RuntimeException("Stub!"); }

/** @hide */

public void offsetBeginAndEndTime(long offsetMillis) { throw new RuntimeException("Stub!"); }

/**
 * @return The beginning of the interval in milliseconds since
 *    epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian).
 * @apiSince REL
 */

public long getBeginTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * @return The end of the interval in milliseconds since
 *    epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian).
 * @apiSince REL
 */

public long getEndTimeMillis() { throw new RuntimeException("Stub!"); }

/**
 * Gets number of UIDs with historical ops.
 *
 * @return The number of UIDs with historical ops.
 *
 * Value is 0 or greater
 * @see #getUidOpsAt(int)
 * @apiSince REL
 */

public int getUidCount() { throw new RuntimeException("Stub!"); }

/**
 * Gets the historical UID ops at a given index.
 *
 * @param index The index.
 *
 * Value is 0 or greater
 * @return The historical UID ops at the given index.
 *
 * This value will never be {@code null}.
 * @see #getUidCount()
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.AppOpsManager.HistoricalUidOps getUidOpsAt(int index) { throw new RuntimeException("Stub!"); }

/**
 * Gets the historical UID ops for a given UID.
 *
 * @param uid The UID.
 *
 * @return The historical ops for the UID.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.app.AppOpsManager.HistoricalUidOps getUidOps(int uid) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.AppOpsManager.HistoricalOps> CREATOR;
static { CREATOR = null; }
}

/**
 * Request for getting historical app op usage. The request acts
 * as a filtering criteria when querying historical op usage.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HistoricalOpsRequest {

HistoricalOpsRequest(int uid, @android.annotation.Nullable java.lang.String packageName, @android.annotation.Nullable java.util.List<java.lang.String> opNames, long beginTimeMillis, long endTimeMillis, int flags) { throw new RuntimeException("Stub!"); }
/**
 * Builder for creating a {@link HistoricalOpsRequest}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Creates a new builder.
 *
 * @param beginTimeMillis The beginning of the interval in milliseconds since
 *     epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian). Must be non
 *     negative.
 * @param endTimeMillis The end of the interval in milliseconds since
 *     epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian). Must be after
 *     {@code beginTimeMillis}. Pass {@link Long#MAX_VALUE} to get the most recent
 *     history including ops that happen while this call is in flight.
 * @apiSince REL
 */

public Builder(long beginTimeMillis, long endTimeMillis) { throw new RuntimeException("Stub!"); }

/**
 * Sets the UID to query for.
 *
 * @param uid The uid. Pass {@link android.os.Process#INVALID_UID} for any uid.
 * @return This builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.AppOpsManager.HistoricalOpsRequest.Builder setUid(int uid) { throw new RuntimeException("Stub!"); }

/**
 * Sets the package to query for.
 *
 * @param packageName The package name. <code>Null</code> for any package.
 * This value may be {@code null}.
 * @return This builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.AppOpsManager.HistoricalOpsRequest.Builder setPackageName(@android.annotation.Nullable java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Sets the op names to query for.
 *
 * @param opNames The op names. <code>Null</code> for any op.
 * This value may be {@code null}.
 * @return This builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.AppOpsManager.HistoricalOpsRequest.Builder setOpNames(@android.annotation.Nullable java.util.List<java.lang.String> opNames) { throw new RuntimeException("Stub!"); }

/**
 * Sets the op flags to query for. The flags specify the type of
 * op data being queried.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return This builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.AppOpsManager.HistoricalOpsRequest.Builder setFlags(int flags) { throw new RuntimeException("Stub!"); }

/**
 * @return a new {@link HistoricalOpsRequest}.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.AppOpsManager.HistoricalOpsRequest build() { throw new RuntimeException("Stub!"); }
}

}

/**
 * This class represents historical app op information about a package.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HistoricalPackageOps implements android.os.Parcelable {

/** @hide */

HistoricalPackageOps(@android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Gets the package name which the data represents.
 *
 * @return The package name which the data represents.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Gets number historical app ops.
 *
 * @return The number historical app ops.
 * Value is 0 or greater
 * @see #getOpAt(int)
 * @apiSince REL
 */

public int getOpCount() { throw new RuntimeException("Stub!"); }

/**
 * Gets the historical op at a given index.
 *
 * @param index The index to lookup.
 * Value is 0 or greater
 * @return The op at the given index.
 * This value will never be {@code null}.
 * @see #getOpCount()
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.AppOpsManager.HistoricalOp getOpAt(int index) { throw new RuntimeException("Stub!"); }

/**
 * Gets the historical entry for a given op name.
 *
 * @param opName The op name.
 * This value must never be {@code null}.
 * @return The historical entry for that op name.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.app.AppOpsManager.HistoricalOp getOp(@android.annotation.NonNull java.lang.String opName) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param parcel This value must never be {@code null}.
 * @apiSince REL
 */

public void writeToParcel(@android.annotation.NonNull android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.AppOpsManager.HistoricalPackageOps> CREATOR;
static { CREATOR = null; }
}

/**
 * This class represents historical app op state for a UID.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HistoricalUidOps implements android.os.Parcelable {

/** @hide */

HistoricalUidOps(int uid) { throw new RuntimeException("Stub!"); }

/**
 * @return The UID for which the data is related.
 * @apiSince REL
 */

public int getUid() { throw new RuntimeException("Stub!"); }

/**
 * Gets number of packages with historical ops.
 *
 * @return The number of packages with historical ops.
 *
 * Value is 0 or greater
 * @see #getPackageOpsAt(int)
 * @apiSince REL
 */

public int getPackageCount() { throw new RuntimeException("Stub!"); }

/**
 * Gets the historical package ops at a given index.
 *
 * @param index The index.
 *
 * Value is 0 or greater
 * @return The historical package ops at the given index.
 *
 * This value will never be {@code null}.
 * @see #getPackageCount()
 * @apiSince REL
 */

@android.annotation.NonNull
public android.app.AppOpsManager.HistoricalPackageOps getPackageOpsAt(int index) { throw new RuntimeException("Stub!"); }

/**
 * Gets the historical package ops for a given package.
 *
 * @param packageName The package.
 *
 * This value must never be {@code null}.
 * @return The historical ops for the package.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.app.AppOpsManager.HistoricalPackageOps getPackageOps(@android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.AppOpsManager.HistoricalUidOps> CREATOR;
static { CREATOR = null; }
}

/**
 * Callback for notification of changes to operation active state.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnOpActiveChangedListener {

/**
 * Called when the active state of an app op changes.
 *
 * @param code The op code.
 * @param uid The UID performing the operation.
 * @param packageName The package performing the operation.
 * @param active Whether the operation became active or inactive.
 * @apiSince REL
 */

public void onOpActiveChanged(int code, int uid, java.lang.String packageName, boolean active);
}

/**
 * Callback for notification of changes to operation state.
 * @apiSince 19
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnOpChangedListener {

/** @apiSince 19 */

public void onOpChanged(java.lang.String op, java.lang.String packageName);
}

/**
 * Class holding the information about one unique operation of an application.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class OpEntry implements android.os.Parcelable {

OpEntry(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/**
 * @return This entry's op string name, such as {@link #OPSTR_COARSE_LOCATION}.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getOpStr() { throw new RuntimeException("Stub!"); }

/**
 * @return this entry's current mode, such as {@link #MODE_ALLOWED}.
 
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#MODE_ALLOWED}, {@link android.app.AppOpsManager#MODE_IGNORED}, {@link android.app.AppOpsManager#MODE_ERRORED}, {@link android.app.AppOpsManager#MODE_DEFAULT}, and {@link android.app.AppOpsManager#MODE_FOREGROUND}
 * @apiSince REL
 */

public int getMode() { throw new RuntimeException("Stub!"); }

/**
 * Return the last wall clock time in milliseconds this op was accessed.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the last access time in milliseconds since
 * epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian).
 *
 * @see #getLastAccessForegroundTime(int)
 * @see #getLastAccessBackgroundTime(int)
 * @see #getLastAccessTime(int, int, int)
 * @apiSince REL
 */

public long getLastAccessTime(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return the last wall clock time in milliseconds this op was accessed
 * by the app while in the foreground.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the last foreground access time in milliseconds since
 * epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian).
 *
 * @see #getLastAccessBackgroundTime(int)
 * @see #getLastAccessTime(int)
 * @see #getLastAccessTime(int, int, int)
 * @apiSince REL
 */

public long getLastAccessForegroundTime(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return the last wall clock time in milliseconds this op was accessed
 * by the app while in the background.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the last foreground access time in milliseconds since
 * epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian).
 *
 * @see #getLastAccessForegroundTime(int)
 * @see #getLastAccessTime(int)
 * @see #getLastAccessTime(int, int, int)
 * @apiSince REL
 */

public long getLastAccessBackgroundTime(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return the last wall clock time  in milliseconds this op was accessed
 * by the app for a given range of UID states.
 *
 * @param fromUidState The UID state for which to query. Could be one of
 * {@link #UID_STATE_PERSISTENT}, {@link #UID_STATE_TOP},
 * {@link #UID_STATE_FOREGROUND_SERVICE}, {@link #UID_STATE_FOREGROUND},
 * {@link #UID_STATE_BACKGROUND}, {@link #UID_STATE_CACHED}.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param toUidState The UID state for which to query.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 *
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the last foreground access time in milliseconds since
 * epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian).
 *
 * @see #getLastAccessForegroundTime(int)
 * @see #getLastAccessBackgroundTime(int)
 * @see #getLastAccessTime(int)
 * @apiSince REL
 */

public long getLastAccessTime(int fromUidState, int toUidState, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return the last wall clock time in milliseconds the app made an attempt
 * to access this op but was rejected.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the last reject time in milliseconds since
 * epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian).
 *
 * @see #getLastRejectBackgroundTime(int)
 * @see #getLastRejectForegroundTime(int)
 * @see #getLastRejectTime(int, int, int)
 * @apiSince REL
 */

public long getLastRejectTime(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return the last wall clock time in milliseconds the app made an attempt
 * to access this op while in the foreground but was rejected.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the last foreground reject time in milliseconds since
 * epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian).
 *
 * @see #getLastRejectBackgroundTime(int)
 * @see #getLastRejectTime(int, int, int)
 * @see #getLastRejectTime(int)
 * @apiSince REL
 */

public long getLastRejectForegroundTime(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return the last wall clock time in milliseconds the app made an attempt
 * to access this op while in the background but was rejected.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the last background reject time in milliseconds since
 * epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian).
 *
 * @see #getLastRejectForegroundTime(int)
 * @see #getLastRejectTime(int, int, int)
 * @see #getLastRejectTime(int)
 * @apiSince REL
 */

public long getLastRejectBackgroundTime(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return the last wall clock time state in milliseconds the app made an
 * attempt to access this op for a given range of UID states.
 *
 * @param fromUidState The UID state from which to query. Could be one of
 * {@link #UID_STATE_PERSISTENT}, {@link #UID_STATE_TOP},
 * {@link #UID_STATE_FOREGROUND_SERVICE}, {@link #UID_STATE_FOREGROUND},
 * {@link #UID_STATE_BACKGROUND}, {@link #UID_STATE_CACHED}.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param toUidState The UID state to which to query.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the last foreground access time in milliseconds since
 * epoch start (January 1, 1970 00:00:00.000 GMT - Gregorian).
 *
 * @see #getLastRejectForegroundTime(int)
 * @see #getLastRejectBackgroundTime(int)
 * @see #getLastRejectTime(int)
 * @apiSince REL
 */

public long getLastRejectTime(int fromUidState, int toUidState, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @return Whether the operation is running.
 * @apiSince REL
 */

public boolean isRunning() { throw new RuntimeException("Stub!"); }

/**
 * @return The duration of the operation in milliseconds. The duration is in wall time.
 * @apiSince REL
 */

public long getDuration() { throw new RuntimeException("Stub!"); }

/**
 * Return the duration in milliseconds the app accessed this op while
 * in the foreground. The duration is in wall time.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the foreground access duration in milliseconds.
 *
 * @see #getLastBackgroundDuration(int)
 * @see #getLastDuration(int, int, int)
 * @apiSince REL
 */

public long getLastForegroundDuration(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return the duration in milliseconds the app accessed this op while
 * in the background. The duration is in wall time.
 *
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the background access duration in milliseconds.
 *
 * @see #getLastForegroundDuration(int)
 * @see #getLastDuration(int, int, int)
 * @apiSince REL
 */

public long getLastBackgroundDuration(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return the duration in milliseconds the app accessed this op for
 * a given range of UID states. The duration is in wall time.
 *
 * @param fromUidState The UID state for which to query. Could be one of
 * {@link #UID_STATE_PERSISTENT}, {@link #UID_STATE_TOP},
 * {@link #UID_STATE_FOREGROUND_SERVICE}, {@link #UID_STATE_FOREGROUND},
 * {@link #UID_STATE_BACKGROUND}, {@link #UID_STATE_CACHED}.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param toUidState The UID state for which to query.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return the access duration in milliseconds.
 * @apiSince REL
 */

public long getLastDuration(int fromUidState, int toUidState, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Gets the UID of the app that performed the op on behalf of this app and
 * as a result blamed the op on this app or {@link Process#INVALID_UID} if
 * there is no proxy.
 *
 * @return The proxy UID.
 * @apiSince REL
 */

public int getProxyUid() { throw new RuntimeException("Stub!"); }

/**
 * Gets the UID of the app that performed the op on behalf of this app and
 * as a result blamed the op on this app or {@link Process#INVALID_UID} if
 * there is no proxy.
 *
 * @param uidState The UID state for which to query. Could be one of
 * {@link #UID_STATE_PERSISTENT}, {@link #UID_STATE_TOP},
 * {@link #UID_STATE_FOREGROUND_SERVICE}, {@link #UID_STATE_FOREGROUND},
 * {@link #UID_STATE_BACKGROUND}, {@link #UID_STATE_CACHED}.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 *
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The proxy UID.
 * @apiSince REL
 */

public int getProxyUid(int uidState, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Gets the package name of the app that performed the op on behalf of this
 * app and as a result blamed the op on this app or {@code null}
 * if there is no proxy.
 *
 * @return The proxy package name.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getProxyPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Gets the package name of the app that performed the op on behalf of this
 * app and as a result blamed the op on this app for a UID state or
 * {@code null} if there is no proxy.
 *
 * @param uidState The UID state for which to query. Could be one of
 * {@link #UID_STATE_PERSISTENT}, {@link #UID_STATE_TOP},
 * {@link #UID_STATE_FOREGROUND_SERVICE}, {@link #UID_STATE_FOREGROUND},
 * {@link #UID_STATE_BACKGROUND}, {@link #UID_STATE_CACHED}.
 * Value is {@link android.app.AppOpsManager#UID_STATE_PERSISTENT}, {@link android.app.AppOpsManager#UID_STATE_TOP}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE_LOCATION}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND_SERVICE}, {@link android.app.AppOpsManager#UID_STATE_FOREGROUND}, {@link android.app.AppOpsManager#UID_STATE_BACKGROUND}, or {@link android.app.AppOpsManager#UID_STATE_CACHED}
 * @param flags The flags which are any combination of
 * {@link #OP_FLAG_SELF}, {@link #OP_FLAG_TRUSTED_PROXY},
 * {@link #OP_FLAG_UNTRUSTED_PROXY}, {@link #OP_FLAG_TRUSTED_PROXIED},
 * {@link #OP_FLAG_UNTRUSTED_PROXIED}. You can use {@link #OP_FLAGS_ALL}
 * for any flag.
 * Value is either <code>0</code> or a combination of {@link android.app.AppOpsManager#OP_FLAG_SELF}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXY}, {@link android.app.AppOpsManager#OP_FLAG_TRUSTED_PROXIED}, and {@link android.app.AppOpsManager#OP_FLAG_UNTRUSTED_PROXIED}
 * @return The proxy package name.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getProxyPackageName(int uidState, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.AppOpsManager.OpEntry> CREATOR;
static { CREATOR = null; }
}

/**
 * Class holding all of the operation information associated with an app.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class PackageOps implements android.os.Parcelable {

PackageOps(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/**
 * @return The name of the package.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * @return The uid of the package.
 * @apiSince REL
 */

public int getUid() { throw new RuntimeException("Stub!"); }

/**
 * @return The ops of the package.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.app.AppOpsManager.OpEntry> getOps() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.app.AppOpsManager.PackageOps> CREATOR;
static { CREATOR = null; }
}

}

