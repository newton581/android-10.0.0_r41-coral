/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os.health;

import android.os.Parcel;

/**
 * Class to write the health stats data into a parcel, so it can then be
 * retrieved via a {@link HealthStats} object.
 *
 * There is an attempt to keep this class as low overhead as possible, for
 * example storing an int[] and a long[] instead of a TimerStat[].
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HealthStatsWriter {

/**
 * Construct a HealthStatsWriter object with the given constants.
 *
 * The "getDataType()" of the resulting HealthStats object will be the
 * short name of the java class that the Constants object was initalized
 * with.
 * @apiSince REL
 */

public HealthStatsWriter(android.os.health.HealthKeys.Constants constants) { throw new RuntimeException("Stub!"); }

/**
 * Add a timer for the given key.
 * @apiSince REL
 */

public void addTimer(int timerId, int count, long time) { throw new RuntimeException("Stub!"); }

/**
 * Add a measurement for the given key.
 * @apiSince REL
 */

public void addMeasurement(int measurementId, long value) { throw new RuntimeException("Stub!"); }

/**
 * Add a recursive HealthStats object for the given key and string name. The value
 * is stored as a HealthStatsWriter until this object is written to a parcel, so
 * don't attempt to reuse the HealthStatsWriter.
 *
 * The value field should not be null.
 * @apiSince REL
 */

public void addStats(int key, java.lang.String name, android.os.health.HealthStatsWriter value) { throw new RuntimeException("Stub!"); }

/**
 * Add a TimerStat for the given key and string name.
 *
 * The value field should not be null.
 * @apiSince REL
 */

public void addTimers(int key, java.lang.String name, android.os.health.TimerStat value) { throw new RuntimeException("Stub!"); }

/**
 * Add a measurement for the given key and string name.
 * @apiSince REL
 */

public void addMeasurements(int key, java.lang.String name, long value) { throw new RuntimeException("Stub!"); }

/**
 * Flattens the data in this HealthStatsWriter to the Parcel format
 * that can be unparceled into a HealthStat.
 * @more
 * (Called flattenToParcel because this HealthStatsWriter itself is
 * not parcelable and we don't flatten all the business about the
 * HealthKeys.Constants, only the values that were actually supplied)
 * @apiSince REL
 */

public void flattenToParcel(android.os.Parcel out) { throw new RuntimeException("Stub!"); }
}

