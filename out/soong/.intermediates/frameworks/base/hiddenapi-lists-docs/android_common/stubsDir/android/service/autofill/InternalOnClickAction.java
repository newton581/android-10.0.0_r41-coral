/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill;


/**
 * Superclass of all {@link OnClickAction} the system understands. As this is not public, all public
 * subclasses have to implement {@link OnClickAction} again.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class InternalOnClickAction implements android.service.autofill.OnClickAction, android.os.Parcelable {

public InternalOnClickAction() { throw new RuntimeException("Stub!"); }

/**
 * Applies the action to the children of the {@code rootView} when clicked.
 
 * @param rootView This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onClick(@android.annotation.NonNull android.view.ViewGroup rootView);
}

