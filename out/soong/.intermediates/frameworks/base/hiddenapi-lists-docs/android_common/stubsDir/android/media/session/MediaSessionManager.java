/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.session;

import android.media.Session2Token;
import android.media.MediaSession2;
import android.content.ComponentName;
import android.service.notification.NotificationListenerService;
import android.view.KeyEvent;
import android.media.AudioManager;
import android.service.media.MediaBrowserService;
import android.os.Handler;

/**
 * Provides support for interacting with {@link MediaSession media sessions}
 * that applications have published to express their ongoing media playback
 * state.
 *
 * @see MediaSession
 * @see MediaController
 * @apiSince 21
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class MediaSessionManager {

/**
 * @hide
 */

MediaSessionManager(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * This API is not generally intended for third party application developers.
 * Use the <a href="{@docRoot}jetpack/androidx.html">AndroidX</a>
 * <a href="{@docRoot}reference/androidx/media2/session/package-summary.html">Media2 session
 * Library</a> for consistent behavior across all devices.
 * <p>
 * Notifies that a new {@link MediaSession2} with type {@link Session2Token#TYPE_SESSION} is
 * created.
 * <p>
 * Do not use this API directly, but create a new instance through the
 * {@link MediaSession2.Builder} instead.
 *
 * @param token newly created session2 token
 
 * This value must never be {@code null}.
 * @apiSince 29
 */

public void notifySession2Created(@android.annotation.NonNull android.media.Session2Token token) { throw new RuntimeException("Stub!"); }

/**
 * Get a list of controllers for all ongoing sessions. The controllers will
 * be provided in priority order with the most important controller at index
 * 0.
 * <p>
 * This requires the android.Manifest.permission.MEDIA_CONTENT_CONTROL
 * permission be held by the calling app. You may also retrieve this list if
 * your app is an enabled notification listener using the
 * {@link NotificationListenerService} APIs, in which case you must pass the
 * {@link ComponentName} of your enabled listener.
 *
 * @param notificationListener The enabled notification listener component.
 *            May be null.
 * This value may be {@code null}.
 * @return A list of controllers for ongoing sessions.
 * @apiSince 21
 */

@android.annotation.NonNull
public java.util.List<android.media.session.MediaController> getActiveSessions(@android.annotation.Nullable android.content.ComponentName notificationListener) { throw new RuntimeException("Stub!"); }

/**
 * This API is not generally intended for third party application developers.
 * Use the <a href="{@docRoot}jetpack/androidx.html">AndroidX</a>
 * <a href="{@docRoot}reference/androidx/media2/session/package-summary.html">Media2 session
 * Library</a> for consistent behavior across all devices.
 * <p>
 * Gets a list of {@link Session2Token} with type {@link Session2Token#TYPE_SESSION} for the
 * current user.
 * <p>
 * Although this API can be used without any restriction, each session owners can accept or
 * reject your uses of {@link MediaSession2}.
 *
 * @return A list of {@link Session2Token}.
 
 * This value will never be {@code null}.
 * @apiSince 29
 */

@android.annotation.NonNull
public java.util.List<android.media.Session2Token> getSession2Tokens() { throw new RuntimeException("Stub!"); }

/**
 * Add a listener to be notified when the list of active sessions
 * changes.This requires the
 * android.Manifest.permission.MEDIA_CONTENT_CONTROL permission be held by
 * the calling app. You may also retrieve this list if your app is an
 * enabled notification listener using the
 * {@link NotificationListenerService} APIs, in which case you must pass the
 * {@link ComponentName} of your enabled listener. Updates will be posted to
 * the thread that registered the listener.
 *
 * @param sessionListener The listener to add.
 * This value must never be {@code null}.
 * @param notificationListener The enabled notification listener component.
 *            May be null.
 
 * This value may be {@code null}.
 * @apiSince 21
 */

public void addOnActiveSessionsChangedListener(@android.annotation.NonNull android.media.session.MediaSessionManager.OnActiveSessionsChangedListener sessionListener, @android.annotation.Nullable android.content.ComponentName notificationListener) { throw new RuntimeException("Stub!"); }

/**
 * Add a listener to be notified when the list of active sessions
 * changes.This requires the
 * android.Manifest.permission.MEDIA_CONTENT_CONTROL permission be held by
 * the calling app. You may also retrieve this list if your app is an
 * enabled notification listener using the
 * {@link NotificationListenerService} APIs, in which case you must pass the
 * {@link ComponentName} of your enabled listener. Updates will be posted to
 * the handler specified or to the caller's thread if the handler is null.
 *
 * @param sessionListener The listener to add.
 * This value must never be {@code null}.
 * @param notificationListener The enabled notification listener component.
 *            May be null.
 * This value may be {@code null}.
 * @param handler The handler to post events to.
 
 * This value may be {@code null}.
 * @apiSince 21
 */

public void addOnActiveSessionsChangedListener(@android.annotation.NonNull android.media.session.MediaSessionManager.OnActiveSessionsChangedListener sessionListener, @android.annotation.Nullable android.content.ComponentName notificationListener, @android.annotation.Nullable android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * Stop receiving active sessions updates on the specified listener.
 *
 * @param listener The listener to remove.
 
 * This value must never be {@code null}.
 * @apiSince 21
 */

public void removeOnActiveSessionsChangedListener(@android.annotation.NonNull android.media.session.MediaSessionManager.OnActiveSessionsChangedListener listener) { throw new RuntimeException("Stub!"); }

/**
 * This API is not generally intended for third party application developers.
 * Use the <a href="{@docRoot}jetpack/androidx.html">AndroidX</a>
 * <a href="{@docRoot}reference/androidx/media2/session/package-summary.html">Media2 session
 * Library</a> for consistent behavior across all devices.
 * <p>
 * Adds a listener to be notified when the {@link #getSession2Tokens()} changes.
 *
 * @param listener The listener to add
 
 * This value must never be {@code null}.
 * @apiSince 29
 */

public void addOnSession2TokensChangedListener(@android.annotation.NonNull android.media.session.MediaSessionManager.OnSession2TokensChangedListener listener) { throw new RuntimeException("Stub!"); }

/**
 * This API is not generally intended for third party application developers.
 * Use the <a href="{@docRoot}jetpack/androidx.html">AndroidX</a>
 * <a href="{@docRoot}reference/androidx/media2/session/package-summary.html">Media2 session
 * Library</a> for consistent behavior across all devices.
 * <p>
 * Adds a listener to be notified when the {@link #getSession2Tokens()} changes.
 *
 * @param listener The listener to add
 * This value must never be {@code null}.
 * @param handler The handler to call listener on.
 
 * This value must never be {@code null}.
 * @apiSince 29
 */

public void addOnSession2TokensChangedListener(@android.annotation.NonNull android.media.session.MediaSessionManager.OnSession2TokensChangedListener listener, @android.annotation.NonNull android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * This API is not generally intended for third party application developers.
 * Use the <a href="{@docRoot}jetpack/androidx.html">AndroidX</a>
 * <a href="{@docRoot}reference/androidx/media2/session/package-summary.html">Media2 session
 * Library</a> for consistent behavior across all devices.
 * <p>
 * Removes the {@link OnSession2TokensChangedListener} to stop receiving session token updates.
 *
 * @param listener The listener to remove.
 
 * This value must never be {@code null}.
 * @apiSince 29
 */

public void removeOnSession2TokensChangedListener(@android.annotation.NonNull android.media.session.MediaSessionManager.OnSession2TokensChangedListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Checks whether the remote user is a trusted app.
 * <p>
 * An app is trusted if the app holds the android.Manifest.permission.MEDIA_CONTENT_CONTROL
 * permission or has an enabled notification listener.
 *
 * @param userInfo The remote user info from either
 *            {@link MediaSession#getCurrentControllerInfo()} or
 *            {@link MediaBrowserService#getCurrentBrowserInfo()}.
 * This value must never be {@code null}.
 * @return {@code true} if the remote user is trusted and its package name matches with the UID.
 *            {@code false} otherwise.
 * @apiSince 28
 */

public boolean isTrustedForMediaControl(@android.annotation.NonNull android.media.session.MediaSessionManager.RemoteUserInfo userInfo) { throw new RuntimeException("Stub!"); }

/**
 * Set the volume key long-press listener. While the listener is set, the listener
 * gets the volume key long-presses instead of changing volume.
 *
 * <p>System can only have a single volume key long-press listener.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SET_VOLUME_KEY_LONG_PRESS_LISTENER}
 * @param listener The volume key long-press listener. {@code null} to reset.
 * @param handler The handler on which the listener should be invoked, or {@code null}
 *            if the listener should be invoked on the calling thread's looper.
 * This value may be {@code null}.
 * @hide
 */

public void setOnVolumeKeyLongPressListener(android.media.session.MediaSessionManager.OnVolumeKeyLongPressListener listener, @android.annotation.Nullable android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * Set the media key listener. While the listener is set, the listener
 * gets the media key before any other media sessions but after the global priority session.
 * If the listener handles the key (i.e. returns {@code true}),
 * other sessions will not get the event.
 *
 * <p>System can only have a single media key listener.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SET_MEDIA_KEY_LISTENER}
 * @param listener The media key listener. {@code null} to reset.
 * @param handler The handler on which the listener should be invoked, or {@code null}
 *            if the listener should be invoked on the calling thread's looper.
 * This value may be {@code null}.
 * @hide
 */

public void setOnMediaKeyListener(android.media.session.MediaSessionManager.OnMediaKeyListener listener, @android.annotation.Nullable android.os.Handler handler) { throw new RuntimeException("Stub!"); }
/**
 * Listens for changes to the list of active sessions. This can be added
 * using {@link #addOnActiveSessionsChangedListener}.
 * @apiSince 21
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnActiveSessionsChangedListener {

/**
 * @param controllers This value may be {@code null}.
 * @apiSince 21
 */

public void onActiveSessionsChanged(@android.annotation.Nullable java.util.List<android.media.session.MediaController> controllers);
}

/**
 * Listens the media key.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnMediaKeyListener {

/**
 * Called when the media key is pressed.
 * <p>If the listener consumes the initial down event (i.e. ACTION_DOWN with
 * repeat count zero), it must also comsume all following key events.
 * (i.e. ACTION_DOWN with repeat count more than zero, and ACTION_UP).
 * <p>If it takes more than 1s to return, the key event will be sent to
 * other media sessions.
 * @apiSince REL
 */

public boolean onMediaKey(android.view.KeyEvent event);
}

/**
 * This API is not generally intended for third party application developers.
 * Use the <a href="{@docRoot}jetpack/androidx.html">AndroidX</a>
 * <a href="{@docRoot}reference/androidx/media2/session/package-summary.html">Media2 session
 * Library</a> for consistent behavior across all devices.
 * <p>
 * Listens for changes to the {@link #getSession2Tokens()}. This can be added
 * using {@link #addOnSession2TokensChangedListener(OnSession2TokensChangedListener, Handler)}.
 * @apiSince 29
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnSession2TokensChangedListener {

/**
 * Called when the {@link #getSession2Tokens()} is changed.
 *
 * @param tokens list of {@link Session2Token}
 
 * This value must never be {@code null}.
 * @apiSince 29
 */

public void onSession2TokensChanged(@android.annotation.NonNull java.util.List<android.media.Session2Token> tokens);
}

/**
 * Listens the volume key long-presses.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnVolumeKeyLongPressListener {

/**
 * Called when the volume key is long-pressed.
 * <p>This will be called for both down and up events.
 * @apiSince REL
 */

public void onVolumeKeyLongPress(android.view.KeyEvent event);
}

/**
 * Information of a remote user of {@link MediaSession} or {@link MediaBrowserService}.
 * This can be used to decide whether the remote user is trusted app, and also differentiate
 * caller of {@link MediaSession} and {@link MediaBrowserService} callbacks.
 * <p>
 * See {@link #equals(Object)} to take a look at how it differentiate media controller.
 *
 * @see #isTrustedForMediaControl(RemoteUserInfo)
 * @apiSince 28
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class RemoteUserInfo {

/**
 * Create a new remote user information.
 *
 * @param packageName The package name of the remote user
 * This value must never be {@code null}.
 * @param pid The pid of the remote user
 * @param uid The uid of the remote user
 * @apiSince 28
 */

public RemoteUserInfo(@android.annotation.NonNull java.lang.String packageName, int pid, int uid) { throw new RuntimeException("Stub!"); }

/**
 * @return package name of the controller
 * @apiSince 28
 */

public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * @return pid of the controller
 * @apiSince 28
 */

public int getPid() { throw new RuntimeException("Stub!"); }

/**
 * @return uid of the controller
 * @apiSince 28
 */

public int getUid() { throw new RuntimeException("Stub!"); }

/**
 * Returns equality of two RemoteUserInfo. Two RemoteUserInfo objects are equal
 * if and only if they have the same package name, same pid, and same uid.
 *
 * @param obj the reference object with which to compare.
 * @return {@code true} if equals, {@code false} otherwise
 * @apiSince 28
 */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince 28 */

public int hashCode() { throw new RuntimeException("Stub!"); }
}

}

