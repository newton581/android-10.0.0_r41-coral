/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;

import android.service.carrier.CarrierIdentifier;
import android.os.Parcelable;

/**
 * Contains the list of carrier restrictions.
 * Allowed list: it indicates the list of carriers that are allowed.
 * Excluded list: it indicates the list of carriers that are excluded.
 * Default carrier restriction: it indicates the default behavior and the priority between the two
 * lists:
 *  - not allowed: the device only allows usage of carriers that are present in the allowed list
 *    and not present in the excluded list. This implies that if a carrier is not present in either
 *    list, it is not allowed.
 *  - allowed: the device allows all carriers, except those present in the excluded list and not
 *    present in the allowed list. This implies that if a carrier is not present in either list,
 *    it is allowed.
 * MultiSim policy: it indicates the behavior in case of devices with two or more SIM cards.
 *  - MULTISIM_POLICY_NONE: the same configuration is applied to all SIM slots independently. This
 *    is the default value if none is set.
 *  - MULTISIM_POLICY_ONE_VALID_SIM_MUST_BE_PRESENT: it indicates that any SIM card can be used
 *    as far as one SIM card matching the configuration is present in the device.
 *
 * Both lists support the character '?' as wild character. For example, an entry indicating
 * MCC=310 and MNC=??? will match all networks with MCC=310.
 *
 * Example 1: Allowed list contains MCC and MNC of operator A. Excluded list contains operator B,
 *            which has same MCC and MNC, but also GID1 value. The priority allowed list is set
 *            to true. Only SIM cards of operator A are allowed, but not those of B or any other
 *            operator.
 * Example 2: Allowed list contains MCC and MNC of operator A. Excluded list contains an entry
 *            with same MCC, and '???' as MNC. The priority allowed list is set to false.
 *            SIM cards of operator A and all SIM cards with a different MCC value are allowed.
 *            SIM cards of operators with same MCC value and different MNC are not allowed.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CarrierRestrictionRules implements android.os.Parcelable {

CarrierRestrictionRules() { throw new RuntimeException("Stub!"); }

/**
 * Indicates if all carriers are allowed
 * @apiSince REL
 */

public boolean isAllCarriersAllowed() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves list of allowed carriers
 *
 * @return the list of allowed carriers
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.service.carrier.CarrierIdentifier> getAllowedCarriers() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves list of excluded carriers
 *
 * @return the list of excluded carriers
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.service.carrier.CarrierIdentifier> getExcludedCarriers() { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the default behavior of carrier restrictions
 
 * @return Value is {@link android.telephony.CarrierRestrictionRules#CARRIER_RESTRICTION_DEFAULT_NOT_ALLOWED}, or {@link android.telephony.CarrierRestrictionRules#CARRIER_RESTRICTION_DEFAULT_ALLOWED}
 * @apiSince REL
 */

public int getDefaultCarrierRestriction() { throw new RuntimeException("Stub!"); }

/**
 * @return The policy used for multi-SIM devices
 
 * Value is {@link android.telephony.CarrierRestrictionRules#MULTISIM_POLICY_NONE}, or {@link android.telephony.CarrierRestrictionRules#MULTISIM_POLICY_ONE_VALID_SIM_MUST_BE_PRESENT}
 * @apiSince REL
 */

public int getMultiSimPolicy() { throw new RuntimeException("Stub!"); }

/**
 * Tests an array of carriers with the carrier restriction configuration. The list of carrier
 * ids passed as argument does not need to be the same as currently present in the device.
 *
 * @param carrierIds list of {@link CarrierIdentifier}, one for each SIM slot on the device
 * This value must never be {@code null}.
 * @return a list of boolean with the same size as input, indicating if each
 * {@link CarrierIdentifier} is allowed or not.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<java.lang.Boolean> areCarrierIdentifiersAllowed(@android.annotation.NonNull java.util.List<android.service.carrier.CarrierIdentifier> carrierIds) { throw new RuntimeException("Stub!"); }

/**
 * {@link Parcelable#writeToParcel}
 * @apiSince REL
 */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/**
 * {@link Parcelable#describeContents}
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * The device allows all carriers, except those present in the excluded list and not present
 * in the allowed list. This implies that if a carrier is not present in either list, it is
 * allowed.
 * @apiSince REL
 */

public static final int CARRIER_RESTRICTION_DEFAULT_ALLOWED = 1; // 0x1

/**
 * The device only allows usage of carriers that are present in the allowed list and not
 * present in the excluded list. This implies that if a carrier is not present in either list,
 * it is not allowed.
 * @apiSince REL
 */

public static final int CARRIER_RESTRICTION_DEFAULT_NOT_ALLOWED = 0; // 0x0

/**
 * {@link Parcelable.Creator}
 * @apiSince REL
 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.CarrierRestrictionRules> CREATOR;
static { CREATOR = null; }

/**
 * The same configuration is applied to all SIM slots independently.
 * @apiSince REL
 */

public static final int MULTISIM_POLICY_NONE = 0; // 0x0

/**
 * Any SIM card can be used as far as one SIM card matching the configuration is present.
 * @apiSince REL
 */

public static final int MULTISIM_POLICY_ONE_VALID_SIM_MUST_BE_PRESENT = 1; // 0x1
/**
 * Builder for a {@link CarrierRestrictionRules}.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/** @apiSince REL */

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * build command
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.CarrierRestrictionRules build() { throw new RuntimeException("Stub!"); }

/**
 * Indicate that all carriers are allowed.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.CarrierRestrictionRules.Builder setAllCarriersAllowed() { throw new RuntimeException("Stub!"); }

/**
 * Set list of allowed carriers.
 *
 * @param allowedCarriers list of allowed carriers
 
 * This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.CarrierRestrictionRules.Builder setAllowedCarriers(@android.annotation.NonNull java.util.List<android.service.carrier.CarrierIdentifier> allowedCarriers) { throw new RuntimeException("Stub!"); }

/**
 * Set list of excluded carriers.
 *
 * @param excludedCarriers list of excluded carriers
 
 * This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.CarrierRestrictionRules.Builder setExcludedCarriers(@android.annotation.NonNull java.util.List<android.service.carrier.CarrierIdentifier> excludedCarriers) { throw new RuntimeException("Stub!"); }

/**
 * Set the default behavior of the carrier restrictions
 *
 * @param carrierRestrictionDefault prioritized carrier list
 
 * Value is {@link android.telephony.CarrierRestrictionRules#CARRIER_RESTRICTION_DEFAULT_NOT_ALLOWED}, or {@link android.telephony.CarrierRestrictionRules#CARRIER_RESTRICTION_DEFAULT_ALLOWED}
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.CarrierRestrictionRules.Builder setDefaultCarrierRestriction(int carrierRestrictionDefault) { throw new RuntimeException("Stub!"); }

/**
 * Set the policy to be used for multi-SIM devices
 *
 * @param multiSimPolicy multi SIM policy
 
 * Value is {@link android.telephony.CarrierRestrictionRules#MULTISIM_POLICY_NONE}, or {@link android.telephony.CarrierRestrictionRules#MULTISIM_POLICY_ONE_VALID_SIM_MUST_BE_PRESENT}
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.CarrierRestrictionRules.Builder setMultiSimPolicy(int multiSimPolicy) { throw new RuntimeException("Stub!"); }
}

}

