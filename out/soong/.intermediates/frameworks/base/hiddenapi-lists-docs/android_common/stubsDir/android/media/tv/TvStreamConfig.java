/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.tv;


/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class TvStreamConfig implements android.os.Parcelable {

TvStreamConfig() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getStreamId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getMaxWidth() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getMaxHeight() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getGeneration() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.media.tv.TvStreamConfig> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public static final int STREAM_TYPE_BUFFER_PRODUCER = 2; // 0x2

/** @apiSince REL */

public static final int STREAM_TYPE_INDEPENDENT_VIDEO_SOURCE = 1; // 0x1
/**
 * A helper class for creating a TvStreamConfig object.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/** @apiSince REL */

public Builder() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvStreamConfig.Builder streamId(int streamId) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvStreamConfig.Builder type(int type) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvStreamConfig.Builder maxWidth(int maxWidth) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvStreamConfig.Builder maxHeight(int maxHeight) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvStreamConfig.Builder generation(int generation) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.media.tv.TvStreamConfig build() { throw new RuntimeException("Stub!"); }
}

}

