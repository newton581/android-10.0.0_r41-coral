/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app;

import java.util.List;
import android.os.IBinder;

/**
 * This class gives information about, and interacts with activities and their containers like task,
 * stacks, and displays.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ActivityTaskManager {

ActivityTaskManager(android.content.Context context, android.os.Handler handler) { throw new RuntimeException("Stub!"); }

/**
 * Sets the windowing mode for a specific task. Only works on tasks of type
 * {@link WindowConfiguration#ACTIVITY_TYPE_STANDARD}
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @param taskId The id of the task to set the windowing mode for.
 * @param windowingMode The windowing mode to set for the task.
 * @param toTop If the task should be moved to the top once the windowing mode changes.
 * @apiSince REL
 */

public void setTaskWindowingMode(int taskId, int windowingMode, boolean toTop) throws java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/**
 * Moves the input task to the primary-split-screen stack.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @param taskId Id of task to move.
 * @param createMode The mode the primary split screen stack should be created in if it doesn't
 *                   exist already. See
 *                   {@link ActivityTaskManager#SPLIT_SCREEN_CREATE_MODE_TOP_OR_LEFT}
 *                   and
 *                   {@link android.app.ActivityManager
 *                        #SPLIT_SCREEN_CREATE_MODE_BOTTOM_OR_RIGHT}
 * @param toTop If the task and stack should be moved to the top.
 * @param animate Whether we should play an animation for the moving the task
 * @param initialBounds If the primary stack gets created, it will use these bounds for the
 *                      docked stack. Pass {@code null} to use default bounds.
 * @param showRecents If the recents activity should be shown on the other side of the task
 *                    going into split-screen mode.
 * @apiSince REL
 */

public void setTaskWindowingModeSplitScreenPrimary(int taskId, int createMode, boolean toTop, boolean animate, android.graphics.Rect initialBounds, boolean showRecents) throws java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/**
 * Resizes the input stack id to the given bounds.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @param stackId Id of the stack to resize.
 * @param bounds Bounds to resize the stack to or {@code null} for fullscreen.
 * @apiSince REL
 */

public void resizeStack(int stackId, android.graphics.Rect bounds) throws java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/**
 * Removes stacks in the windowing modes from the system if they are of activity type
 * ACTIVITY_TYPE_STANDARD or ACTIVITY_TYPE_UNDEFINED
 
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @apiSince REL
 */

public void removeStacksInWindowingModes(int[] windowingModes) throws java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/**
 * Removes stack of the activity types from the system.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @apiSince REL
 */

public void removeStacksWithActivityTypes(int[] activityTypes) throws java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/**
 * Returns true if the system supports at least one form of multi-window.
 * E.g. freeform, split-screen, picture-in-picture.
 * @apiSince REL
 */

public static boolean supportsMultiWindow(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if the system supports split screen multi-window.
 * @apiSince REL
 */

public static boolean supportsSplitScreenMultiWindow(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Moves the top activity in the input stackId to the pinned stack.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @param stackId Id of stack to move the top activity to pinned stack.
 * @param bounds Bounds to use for pinned stack.
 * @return True if the top activity of stack was successfully moved to the pinned stack.
 * @apiSince REL
 */

public boolean moveTopActivityToPinnedStack(int stackId, android.graphics.Rect bounds) { throw new RuntimeException("Stub!"); }

/**
 * Start to enter lock task mode for given task by system(UI).
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @param taskId Id of task to lock.
 * @apiSince REL
 */

public void startSystemLockTaskMode(int taskId) { throw new RuntimeException("Stub!"); }

/**
 * Stop lock task mode by system(UI).
 
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @apiSince REL
 */

public void stopSystemLockTaskMode() { throw new RuntimeException("Stub!"); }

/**
 * Move task to stack with given id.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @param taskId Id of the task to move.
 * @param stackId Id of the stack for task moving.
 * @param toTop Whether the given task should shown to top of stack.
 * @apiSince REL
 */

public void moveTaskToStack(int taskId, int stackId, boolean toTop) { throw new RuntimeException("Stub!"); }

/**
 * Resize the input stack id to the given bounds with animate setting.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @param stackId Id of the stack to resize.
 * @param bounds Bounds to resize the stack to or {@code null} for fullscreen.
 * @param animate Whether we should play an animation for resizing stack.
 * @apiSince REL
 */

public void resizeStack(int stackId, android.graphics.Rect bounds, boolean animate) { throw new RuntimeException("Stub!"); }

/**
 * Resize task to given bounds.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @param taskId Id of task to resize.
 * @param bounds Bounds to resize task.
 * @apiSince REL
 */

public void resizeTask(int taskId, android.graphics.Rect bounds) { throw new RuntimeException("Stub!"); }

/**
 * Resize docked stack & its task to given stack & task bounds.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @param stackBounds Bounds to resize stack.
 * @param taskBounds Bounds to resize task.
 * @apiSince REL
 */

public void resizeDockedStack(android.graphics.Rect stackBounds, android.graphics.Rect taskBounds) { throw new RuntimeException("Stub!"); }

/**
 * List all activity stacks information.
 
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @apiSince REL
 */

public java.lang.String listAllStacks() { throw new RuntimeException("Stub!"); }

/**
 * Clears launch params for the given package.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @param packageNames the names of the packages of which the launch params are to be cleared
 * @apiSince REL
 */

public void clearLaunchParamsForPackages(java.util.List<java.lang.String> packageNames) { throw new RuntimeException("Stub!"); }

/**
 * Makes the display with the given id a single task instance display. I.e the display can only
 * contain one task.
 
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_ACTIVITY_STACKS}
 * @apiSince REL
 */

public void setDisplayToSingleTaskInstance(int displayId) { throw new RuntimeException("Stub!"); }

/**
 * Invalid stack ID.
 * @apiSince REL
 */

public static final int INVALID_STACK_ID = -1; // 0xffffffff

/**
 * Parameter to {@link IActivityTaskManager#setTaskWindowingModeSplitScreenPrimary} which
 * specifies the position of the created docked stack at the bottom half of the screen if
 * in portrait mode or at the right half of the screen if in landscape mode.
 * @apiSince REL
 */

public static final int SPLIT_SCREEN_CREATE_MODE_BOTTOM_OR_RIGHT = 1; // 0x1

/**
 * Parameter to {@link IActivityTaskManager#setTaskWindowingModeSplitScreenPrimary} which
 * specifies the position of the created docked stack at the top half of the screen if
 * in portrait mode or at the left half of the screen if in landscape mode.
 * @apiSince REL
 */

public static final int SPLIT_SCREEN_CREATE_MODE_TOP_OR_LEFT = 0; // 0x0
}

