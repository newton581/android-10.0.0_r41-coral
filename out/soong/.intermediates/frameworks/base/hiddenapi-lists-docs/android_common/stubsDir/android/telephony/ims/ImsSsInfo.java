/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims;


/**
 * Provides the result to the update operation for the supplementary service configuration.
 *
 * Also supports IMS specific Incoming Communication Barring (ICB) as well as Anonymous
 * Communication Rejection (ACR), as per 3GPP 24.611.
 *
 * @see Builder
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ImsSsInfo implements android.os.Parcelable {

/**
 *
 * @param status The status of the service registration of activation/deactiviation.
 * Value is {@link android.telephony.ims.ImsSsInfo#NOT_REGISTERED}, {@link android.telephony.ims.ImsSsInfo#DISABLED}, or {@link android.telephony.ims.ImsSsInfo#ENABLED}
 * @param icbNum The Incoming barring number.
 * This value may be {@code null}.
 * @deprecated use {@link ImsSsInfo.Builder} instead.
 @apiSince REL
 */

@Deprecated
public ImsSsInfo(int status, @android.annotation.Nullable java.lang.String icbNum) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * @return Supplementary Service Configuration status.
 
 * Value is {@link android.telephony.ims.ImsSsInfo#NOT_REGISTERED}, {@link android.telephony.ims.ImsSsInfo#DISABLED}, or {@link android.telephony.ims.ImsSsInfo#ENABLED}
 * @apiSince REL
 */

public int getStatus() { throw new RuntimeException("Stub!"); }

/**
 * @deprecated Use {@link #getIncomingCommunicationBarringNumber()} instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public java.lang.String getIcbNum() { throw new RuntimeException("Stub!"); }

/**
 * @return The Incoming Communication Barring (ICB) number.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getIncomingCommunicationBarringNumber() { throw new RuntimeException("Stub!"); }

/**
 * @return Supplementary Service Provision status.
 
 * Value is {@link android.telephony.ims.ImsSsInfo#SERVICE_PROVISIONING_UNKNOWN}, {@link android.telephony.ims.ImsSsInfo#SERVICE_NOT_PROVISIONED}, or {@link android.telephony.ims.ImsSsInfo#SERVICE_PROVISIONED}
 * @apiSince REL
 */

public int getProvisionStatus() { throw new RuntimeException("Stub!"); }

/**
 * @return the Calling Line Identification Restriction State for outgoing calls with respect to
 * this subscription. Will be {@link #CLIR_OUTGOING_DEFAULT} if not applicable to this SS info.
 
 * Value is {@link android.telephony.ims.ImsSsInfo#CLIR_OUTGOING_DEFAULT}, {@link android.telephony.ims.ImsSsInfo#CLIR_OUTGOING_INVOCATION}, or {@link android.telephony.ims.ImsSsInfo#CLIR_OUTGOING_SUPPRESSION}
 * @apiSince REL
 */

public int getClirOutgoingState() { throw new RuntimeException("Stub!"); }

/**
 * @return the calling line identification restriction provisioning status upon interrogation of
 * the service for this subscription. Will be {@link #CLIR_STATUS_UNKNOWN} if not applicable to
 * this SS info.
 
 * Value is {@link android.telephony.ims.ImsSsInfo#CLIR_STATUS_NOT_PROVISIONED}, {@link android.telephony.ims.ImsSsInfo#CLIR_STATUS_PROVISIONED_PERMANENT}, {@link android.telephony.ims.ImsSsInfo#CLIR_STATUS_UNKNOWN}, {@link android.telephony.ims.ImsSsInfo#CLIR_STATUS_TEMPORARILY_RESTRICTED}, or {@link android.telephony.ims.ImsSsInfo#CLIR_STATUS_TEMPORARILY_ALLOWED}
 * @apiSince REL
 */

public int getClirInterrogationStatus() { throw new RuntimeException("Stub!"); }

/**
 * Calling line identification restriction (CLIR) is set to the default according to the
 * subscription of the CLIR service.
 *
 * See TS 27.007, section 7.7 for more information.
 * @apiSince REL
 */

public static final int CLIR_OUTGOING_DEFAULT = 0; // 0x0

/**
 * Activate Calling line identification restriction for outgoing calls.
 *
 * See TS 27.007, section 7.7 for more information.
 * @apiSince REL
 */

public static final int CLIR_OUTGOING_INVOCATION = 1; // 0x1

/**
 * Deactivate Calling line identification restriction for outgoing calls.
 *
 * See TS 27.007, section 7.7 for more information.
 * @apiSince REL
 */

public static final int CLIR_OUTGOING_SUPPRESSION = 2; // 0x2

/**
 * Calling line identification restriction is currently not provisioned.
 *
 * See TS 27.007, section 7.7 for more information.
 * @apiSince REL
 */

public static final int CLIR_STATUS_NOT_PROVISIONED = 0; // 0x0

/**
 * Calling line identification restriction is currently provisioned in permanent mode.
 *
 * See TS 27.007, section 7.7 for more information.
 * @apiSince REL
 */

public static final int CLIR_STATUS_PROVISIONED_PERMANENT = 1; // 0x1

/**
 * Calling line identification restriction temporary mode, temporarily allowed.
 *
 * See TS 27.007, section 7.7 for more information.
 * @apiSince REL
 */

public static final int CLIR_STATUS_TEMPORARILY_ALLOWED = 4; // 0x4

/**
 * Calling line identification restriction temporary mode, temporarily restricted.
 *
 * See TS 27.007, section 7.7 for more information.
 * @apiSince REL
 */

public static final int CLIR_STATUS_TEMPORARILY_RESTRICTED = 3; // 0x3

/**
 * Calling line identification restriction is currently unknown, e.g. no network, etc.
 *
 * See TS 27.007, section 7.7 for more information.
 * @apiSince REL
 */

public static final int CLIR_STATUS_UNKNOWN = 2; // 0x2

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.ims.ImsSsInfo> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public static final int DISABLED = 0; // 0x0

/** @apiSince REL */

public static final int ENABLED = 1; // 0x1

/**
 * For the status of service registration or activation/deactivation.
 * @apiSince REL
 */

public static final int NOT_REGISTERED = -1; // 0xffffffff

/**
 * Service is not provisioned.
 * @apiSince REL
 */

public static final int SERVICE_NOT_PROVISIONED = 0; // 0x0

/**
 * Service is provisioned.
 * @apiSince REL
 */

public static final int SERVICE_PROVISIONED = 1; // 0x1

/**
 * Unknown provision status for the service.
 * @apiSince REL
 */

public static final int SERVICE_PROVISIONING_UNKNOWN = -1; // 0xffffffff
/**
 * Builds {@link ImsSsInfo} instances, which may include optional parameters.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * @param status Value is {@link android.telephony.ims.ImsSsInfo#NOT_REGISTERED}, {@link android.telephony.ims.ImsSsInfo#DISABLED}, or {@link android.telephony.ims.ImsSsInfo#ENABLED}
 * @apiSince REL
 */

public Builder(int status) { throw new RuntimeException("Stub!"); }

/**
 * Set the ICB number for IMS call barring.
 * @param number The number in E.164 international format.
 
 * This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.ImsSsInfo.Builder setIncomingCommunicationBarringNumber(@android.annotation.NonNull java.lang.String number) { throw new RuntimeException("Stub!"); }

/**
 * Set the provisioning status for a Supplementary Service interrogation response.
 
 * @param provisionStatus Value is {@link android.telephony.ims.ImsSsInfo#SERVICE_PROVISIONING_UNKNOWN}, {@link android.telephony.ims.ImsSsInfo#SERVICE_NOT_PROVISIONED}, or {@link android.telephony.ims.ImsSsInfo#SERVICE_PROVISIONED}
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.ImsSsInfo.Builder setProvisionStatus(int provisionStatus) { throw new RuntimeException("Stub!"); }

/**
 * Set the Calling Line Identification Restriction (CLIR) status for a supplementary service
 * interrogation response.
 
 * @param status Value is {@link android.telephony.ims.ImsSsInfo#CLIR_STATUS_NOT_PROVISIONED}, {@link android.telephony.ims.ImsSsInfo#CLIR_STATUS_PROVISIONED_PERMANENT}, {@link android.telephony.ims.ImsSsInfo#CLIR_STATUS_UNKNOWN}, {@link android.telephony.ims.ImsSsInfo#CLIR_STATUS_TEMPORARILY_RESTRICTED}, or {@link android.telephony.ims.ImsSsInfo#CLIR_STATUS_TEMPORARILY_ALLOWED}
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.ImsSsInfo.Builder setClirInterrogationStatus(int status) { throw new RuntimeException("Stub!"); }

/**
 * Set the Calling line identification Restriction (CLIR) state for outgoing calls.
 
 * @param state Value is {@link android.telephony.ims.ImsSsInfo#CLIR_OUTGOING_DEFAULT}, {@link android.telephony.ims.ImsSsInfo#CLIR_OUTGOING_INVOCATION}, or {@link android.telephony.ims.ImsSsInfo#CLIR_OUTGOING_SUPPRESSION}
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.ImsSsInfo.Builder setClirOutgoingState(int state) { throw new RuntimeException("Stub!"); }

/**
 * @return a built {@link ImsSsInfo} containing optional the parameters that were set.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.ImsSsInfo build() { throw new RuntimeException("Stub!"); }
}

}

