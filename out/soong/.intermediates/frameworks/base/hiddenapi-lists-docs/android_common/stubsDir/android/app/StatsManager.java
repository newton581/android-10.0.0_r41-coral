/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app;


/**
 * API for statsd clients to send configurations and retrieve data.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class StatsManager {

/**
 * Constructor for StatsManagerClient.
 *
 * @hide
 */

StatsManager(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Adds the given configuration and associates it with the given configKey. If a config with the
 * given configKey already exists for the caller's uid, it is replaced with the new one.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param configKey An arbitrary integer that allows clients to track the configuration.
 * @param config    Wire-encoded StatsdConfig proto that specifies metrics (and all
 *                  dependencies eg, conditions and matchers).
 * @throws StatsUnavailableException if unsuccessful due to failing to connect to stats service
 * @throws IllegalArgumentException if config is not a wire-encoded StatsdConfig proto
 * @apiSince REL
 */

public void addConfig(long configKey, byte[] config) throws android.app.StatsManager.StatsUnavailableException { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @deprecated Use {@link #addConfig(long, byte[])}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean addConfiguration(long configKey, byte[] config) { throw new RuntimeException("Stub!"); }

/**
 * Remove a configuration from logging.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param configKey Configuration key to remove.
 * @throws StatsUnavailableException if unsuccessful due to failing to connect to stats service
 * @apiSince REL
 */

public void removeConfig(long configKey) throws android.app.StatsManager.StatsUnavailableException { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @deprecated Use {@link #removeConfig(long)}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean removeConfiguration(long configKey) { throw new RuntimeException("Stub!"); }

/**
 * Set the PendingIntent to be used when broadcasting subscriber information to the given
 * subscriberId within the given config.
 * <p>
 * Suppose that the calling uid has added a config with key configKey, and that in this config
 * it is specified that when a particular anomaly is detected, a broadcast should be sent to
 * a BroadcastSubscriber with id subscriberId. This function links the given pendingIntent with
 * that subscriberId (for that config), so that this pendingIntent is used to send the broadcast
 * when the anomaly is detected.
 * <p>
 * When statsd sends the broadcast, the PendingIntent will used to send an intent with
 * information of
 * {@link #EXTRA_STATS_CONFIG_UID},
 * {@link #EXTRA_STATS_CONFIG_KEY},
 * {@link #EXTRA_STATS_SUBSCRIPTION_ID},
 * {@link #EXTRA_STATS_SUBSCRIPTION_RULE_ID},
 * {@link #EXTRA_STATS_BROADCAST_SUBSCRIBER_COOKIES}, and
 * {@link #EXTRA_STATS_DIMENSIONS_VALUE}.
 * <p>
 * This function can only be called by the owner (uid) of the config. It must be called each
 * time statsd starts. The config must have been added first (via {@link #addConfig}).
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param pendingIntent the PendingIntent to use when broadcasting info to the subscriber
 *                      associated with the given subscriberId. May be null, in which case
 *                      it undoes any previous setting of this subscriberId.
 * @param configKey     The integer naming the config to which this subscriber is attached.
 * @param subscriberId  ID of the subscriber, as used in the config.
 * @throws StatsUnavailableException if unsuccessful due to failing to connect to stats service
 * @apiSince REL
 */

public void setBroadcastSubscriber(android.app.PendingIntent pendingIntent, long configKey, long subscriberId) throws android.app.StatsManager.StatsUnavailableException { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @deprecated Use {@link #setBroadcastSubscriber(PendingIntent, long, long)}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean setBroadcastSubscriber(long configKey, long subscriberId, android.app.PendingIntent pendingIntent) { throw new RuntimeException("Stub!"); }

/**
 * Registers the operation that is called to retrieve the metrics data. This must be called
 * each time statsd starts. The config must have been added first (via {@link #addConfig},
 * although addConfig could have been called on a previous boot). This operation allows
 * statsd to send metrics data whenever statsd determines that the metrics in memory are
 * approaching the memory limits. The fetch operation should call {@link #getReports} to fetch
 * the data, which also deletes the retrieved metrics from statsd's memory.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param pendingIntent the PendingIntent to use when broadcasting info to the subscriber
 *                      associated with the given subscriberId. May be null, in which case
 *                      it removes any associated pending intent with this configKey.
 * @param configKey     The integer naming the config to which this operation is attached.
 * @throws StatsUnavailableException if unsuccessful due to failing to connect to stats service
 * @apiSince REL
 */

public void setFetchReportsOperation(android.app.PendingIntent pendingIntent, long configKey) throws android.app.StatsManager.StatsUnavailableException { throw new RuntimeException("Stub!"); }

/**
 * Registers the operation that is called whenever there is a change in which configs are
 * active. This must be called each time statsd starts. This operation allows
 * statsd to inform clients that they should pull data of the configs that are currently
 * active. The activeConfigsChangedOperation should set periodic alarms to pull data of configs
 * that are active and stop pulling data of configs that are no longer active.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param pendingIntent the PendingIntent to use when broadcasting info to the subscriber
 *                      associated with the given subscriberId. May be null, in which case
 *                      it removes any associated pending intent for this client.
 * This value may be {@code null}.
 * @return A list of configs that are currently active for this client. If the pendingIntent is
 *         null, this will be an empty list.
 * @throws StatsUnavailableException if unsuccessful due to failing to connect to stats service
 * @apiSince REL
 */

@android.annotation.NonNull
public long[] setActiveConfigsChangedOperation(@android.annotation.Nullable android.app.PendingIntent pendingIntent) throws android.app.StatsManager.StatsUnavailableException { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @deprecated Use {@link #setFetchReportsOperation(PendingIntent, long)}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean setDataFetchOperation(long configKey, android.app.PendingIntent pendingIntent) { throw new RuntimeException("Stub!"); }

/**
 * Request the data collected for the given configKey.
 * This getter is destructive - it also clears the retrieved metrics from statsd's memory.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @param configKey Configuration key to retrieve data from.
 * @return Serialized ConfigMetricsReportList proto.
 * @throws StatsUnavailableException if unsuccessful due to failing to connect to stats service
 * @apiSince REL
 */

public byte[] getReports(long configKey) throws android.app.StatsManager.StatsUnavailableException { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @deprecated Use {@link #getReports(long)}
 
 * @return This value may be {@code null}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
@android.annotation.Nullable
public byte[] getData(long configKey) { throw new RuntimeException("Stub!"); }

/**
 * Clients can request metadata for statsd. Will contain stats across all configurations but not
 * the actual metrics themselves (metrics must be collected via {@link #getReports(long)}.
 * This getter is not destructive and will not reset any metrics/counters.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @return Serialized StatsdStatsReport proto.
 * @throws StatsUnavailableException if unsuccessful due to failing to connect to stats service
 * @apiSince REL
 */

public byte[] getStatsMetadata() throws android.app.StatsManager.StatsUnavailableException { throw new RuntimeException("Stub!"); }

/**
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @deprecated Use {@link #getStatsMetadata()}
 
 * @return This value may be {@code null}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
@android.annotation.Nullable
public byte[] getMetadata() { throw new RuntimeException("Stub!"); }

/**
 * Returns the experiments IDs registered with statsd, or an empty array if there aren't any.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP} and {@link android.Manifest.permission#PACKAGE_USAGE_STATS}
 * @throws StatsUnavailableException if unsuccessful due to failing to connect to stats service
 * @apiSince REL
 */

public long[] getRegisteredExperimentIds() throws android.app.StatsManager.StatsUnavailableException { throw new RuntimeException("Stub!"); }

/**
 * Broadcast Action: Statsd has started.
 * Configurations and PendingIntents can now be sent to it.
 * @apiSince REL
 */

public static final java.lang.String ACTION_STATSD_STARTED = "android.app.action.STATSD_STARTED";

/**
 * Long array extra of the active configs for the uid that added those configs.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_STATS_ACTIVE_CONFIG_KEYS = "android.app.extra.STATS_ACTIVE_CONFIG_KEYS";

/**
 *   List<String> of the relevant statsd_config.proto's BroadcastSubscriberDetails.cookie.
 *   Obtain using {@link android.content.Intent#getStringArrayListExtra(String)}.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_STATS_BROADCAST_SUBSCRIBER_COOKIES = "android.app.extra.STATS_BROADCAST_SUBSCRIBER_COOKIES";

/**
 * Long extra of the relevant stats config's configKey.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_STATS_CONFIG_KEY = "android.app.extra.STATS_CONFIG_KEY";

/**
 * Long extra of uid that added the relevant stats config.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_STATS_CONFIG_UID = "android.app.extra.STATS_CONFIG_UID";

/**
 * Extra of a {@link android.os.StatsDimensionsValue} representing sliced dimension value
 * information.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_STATS_DIMENSIONS_VALUE = "android.app.extra.STATS_DIMENSIONS_VALUE";

/**
 * Long extra of the relevant statsd_config.proto's Subscription.id.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_STATS_SUBSCRIPTION_ID = "android.app.extra.STATS_SUBSCRIPTION_ID";

/**
 * Long extra of the relevant statsd_config.proto's Subscription.rule_id.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_STATS_SUBSCRIPTION_RULE_ID = "android.app.extra.STATS_SUBSCRIPTION_RULE_ID";
/**
 * Exception thrown when communication with the stats service fails (eg if it is not available).
 * This might be thrown early during boot before the stats service has started or if it crashed.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class StatsUnavailableException extends android.util.AndroidException {

/** @apiSince REL */

public StatsUnavailableException(java.lang.String reason) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public StatsUnavailableException(java.lang.String reason, java.lang.Throwable e) { throw new RuntimeException("Stub!"); }
}

}

