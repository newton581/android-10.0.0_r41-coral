/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.telephony;

import java.util.Map;

/**
 * Returned as the reason for a data connection failure as defined by modem and some local errors.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class DataFailCause {

DataFailCause() { throw new RuntimeException("Stub!"); }

/**
 * Access attempt is already in progress.
 * @apiSince REL
 */

public static final int ACCESS_ATTEMPT_ALREADY_IN_PROGRESS = 2219; // 0x8ab

/**
 * Access blocked by the base station.
 * @apiSince REL
 */

public static final int ACCESS_BLOCK = 2087; // 0x827

/**
 * Access blocked by the base station for all mobile devices.
 * @apiSince REL
 */

public static final int ACCESS_BLOCK_ALL = 2088; // 0x828

/**
 * Access class blocking restrictions for the current camped cell.
 * @apiSince REL
 */

public static final int ACCESS_CLASS_DSAC_REJECTION = 2108; // 0x83c

/**
 * Access control list check failure at the lower layer.
 * @apiSince REL
 */

public static final int ACCESS_CONTROL_LIST_CHECK_FAILURE = 2128; // 0x850

/**
 * UE requested to modify QoS parameters or the bearer control mode, which is not compatible
 * with the selected bearer control mode.
 * @apiSince REL
 */

public static final int ACTIVATION_REJECTED_BCM_VIOLATION = 48; // 0x30

/**
 * Activation rejected by Gateway GPRS Support Node (GGSN), Serving Gateway or PDN Gateway.
 * @apiSince REL
 */

public static final int ACTIVATION_REJECT_GGSN = 30; // 0x1e

/**
 * Activation rejected, unspecified.
 * @apiSince REL
 */

public static final int ACTIVATION_REJECT_UNSPECIFIED = 31; // 0x1f

/**
 * Max number of Packet Data Protocol (PDP) context reached.
 * @apiSince REL
 */

public static final int ACTIVE_PDP_CONTEXT_MAX_NUMBER_REACHED = 65; // 0x41

/**
 * APN has been disabled.
 * @apiSince REL
 */

public static final int APN_DISABLED = 2045; // 0x7fd

/**
 * PDN connection to the APN is disallowed on the roaming network.
 * @apiSince REL
 */

public static final int APN_DISALLOWED_ON_ROAMING = 2059; // 0x80b

/**
 * New PDN bring up is rejected during interface selection because the UE has already allotted
 * the available interfaces for other PDNs.
 * @apiSince REL
 */

public static final int APN_MISMATCH = 2054; // 0x806

/**
 * APN-related parameters are changed.
 * @apiSince REL
 */

public static final int APN_PARAMETERS_CHANGED = 2060; // 0x80c

/**
 * Interface bring up is attempted for an APN that is yet to be handed over to target RAT.
 * @apiSince REL
 */

public static final int APN_PENDING_HANDOVER = 2041; // 0x7f9

/**
 * APN type conflict.
 * @apiSince REL
 */

public static final int APN_TYPE_CONFLICT = 112; // 0x70

/**
 * Authentication failure on emergency call.
 * @apiSince REL
 */

public static final int AUTH_FAILURE_ON_EMERGENCY_CALL = 122; // 0x7a

/**
 * Procedure requested by the UE was rejected because the bearer handling is not supported.
 * @apiSince REL
 */

public static final int BEARER_HANDLING_NOT_SUPPORTED = 60; // 0x3c

/**
 * Roaming is disallowed during call bring up.
 * @apiSince REL
 */

public static final int CALL_DISALLOWED_IN_ROAMING = 2068; // 0x814

/**
 * Emergency call bring up on a different ePDG.
 * @apiSince REL
 */

public static final int CALL_PREEMPT_BY_EMERGENCY_APN = 127; // 0x7f

/**
 * Unable to encode the OTA message for MT PDP or deactivate PDP.
 * @apiSince REL
 */

public static final int CANNOT_ENCODE_OTA_MESSAGE = 2159; // 0x86f

/**
 * Received an alert stop from the base station due to incoming only.
 * @apiSince REL
 */

public static final int CDMA_ALERT_STOP = 2077; // 0x81d

/**
 * Receiving an incoming call from the base station.
 * @apiSince REL
 */

public static final int CDMA_INCOMING_CALL = 2076; // 0x81c

/**
 * Received an intercept order from the base station.
 * @apiSince REL
 */

public static final int CDMA_INTERCEPT = 2073; // 0x819

/**
 * Device in CDMA locked state.
 * @apiSince REL
 */

public static final int CDMA_LOCK = 2072; // 0x818

/**
 * Receiving a release from the base station with a SO (Service Option) Reject reason.
 * @apiSince REL
 */

public static final int CDMA_RELEASE_DUE_TO_SO_REJECTION = 2075; // 0x81b

/**
 * Receiving a reorder from the base station.
 * @apiSince REL
 */

public static final int CDMA_REORDER = 2074; // 0x81a

/**
 * Receiving a retry order from the base station.
 * @apiSince REL
 */

public static final int CDMA_RETRY_ORDER = 2086; // 0x826

/**
 * Channel acquisition failures. This indicates that device has failed acquiring all the
 * channels in the PRL.
 * @apiSince REL
 */

public static final int CHANNEL_ACQUISITION_FAILURE = 2078; // 0x81e

/**
 * Tearing down is in progress.
 * @apiSince REL
 */

public static final int CLOSE_IN_PROGRESS = 2030; // 0x7ee

/**
 * Network has already initiated the activation, modification, or deactivation of bearer
 * resources that was requested by the UE.
 * @apiSince REL
 */

public static final int COLLISION_WITH_NETWORK_INITIATED_REQUEST = 56; // 0x38

/**
 * Companion interface in use.
 * @apiSince REL
 */

public static final int COMPANION_IFACE_IN_USE = 118; // 0x76

/**
 * The concurrent services requested were not compatible.
 * @apiSince REL
 */

public static final int CONCURRENT_SERVICES_INCOMPATIBLE = 2083; // 0x823

/**
 * In favor of a voice call or SMS when concurrent voice and data are not supported.
 * @apiSince REL
 */

public static final int CONCURRENT_SERVICES_NOT_ALLOWED = 2091; // 0x82b

/**
 * Concurrent service is not supported by base station.
 * @apiSince REL
 */

public static final int CONCURRENT_SERVICE_NOT_SUPPORTED_BY_BASE_STATION = 2080; // 0x820

/**
 * Conditional Information Element (IE) error.
 * @apiSince REL
 */

public static final int CONDITIONAL_IE_ERROR = 100; // 0x64

/**
 * Network cannot serve a request from the MS due to congestion.
 * @apiSince REL
 */

public static final int CONGESTION = 2106; // 0x83a

/**
 * Indicate the connection was released.
 * @apiSince REL
 */

public static final int CONNECTION_RELEASED = 2113; // 0x841

/**
 * CS domain is not available.
 * @apiSince REL
 */

public static final int CS_DOMAIN_NOT_AVAILABLE = 2181; // 0x885

/**
 * CS fallback call establishment is not allowed.
 * @apiSince REL
 */

public static final int CS_FALLBACK_CALL_ESTABLISHMENT_NOT_ALLOWED = 2188; // 0x88c

/**
 * Network initiates a detach on LTE with error cause ""data plan has been replenished or has
 * expired.
 * @apiSince REL
 */

public static final int DATA_PLAN_EXPIRED = 2198; // 0x896

/**
 * PDN Connection to a given APN is disallowed because data roaming is disabled from the device
 * user interface settings and the UE is roaming.
 * @apiSince REL
 */

public static final int DATA_ROAMING_SETTINGS_DISABLED = 2064; // 0x810

/**
 * PDN Connection to a given APN is disallowed because data is disabled from the device user
 * interface settings.
 * @apiSince REL
 */

public static final int DATA_SETTINGS_DISABLED = 2063; // 0x80f

/**
 * DBM or SMS is in progress.
 * @apiSince REL
 */

public static final int DBM_OR_SMS_IN_PROGRESS = 2211; // 0x8a3

/**
 * DDS (Default data subscription) switch occurs.
 * @apiSince REL
 */

public static final int DDS_SWITCHED = 2065; // 0x811

/**
 * Default data subscription switch is in progress.
 * @apiSince REL
 */

public static final int DDS_SWITCH_IN_PROGRESS = 2067; // 0x813

/**
 * Data radio bearer is released by the RRC.
 * @apiSince REL
 */

public static final int DRB_RELEASED_BY_RRC = 2112; // 0x840

/**
 * Dedicated bearer will be deactivated regardless of the network response.
 * @apiSince REL
 */

public static final int DS_EXPLICIT_DEACTIVATION = 2125; // 0x84d

/**
 * Dual switch from single standby to dual standby is in progress.
 * @apiSince REL
 */

public static final int DUAL_SWITCH = 2227; // 0x8b3

/**
 * Dial up networking (DUN) call bring up is rejected since UE is in eHRPD RAT.
 * @apiSince REL
 */

public static final int DUN_CALL_DISALLOWED = 2056; // 0x808

/**
 * Active dedicated bearer was requested using the same default bearer ID.
 * @apiSince REL
 */

public static final int DUPLICATE_BEARER_ID = 2118; // 0x846

/**
 * Device falls back from eHRPD to HRPD.
 * @apiSince REL
 */

public static final int EHRPD_TO_HRPD_FALLBACK = 2049; // 0x801

/**
 * Data call has been brought down because EMBMS is not enabled at the RRC layer.
 * @apiSince REL
 */

public static final int EMBMS_NOT_ENABLED = 2193; // 0x891

/**
 * EMBMS data call has been successfully brought down.
 * @apiSince REL
 */

public static final int EMBMS_REGULAR_DEACTIVATION = 2195; // 0x893

/**
 * Emergency interface only.
 * @apiSince REL
 */

public static final int EMERGENCY_IFACE_ONLY = 116; // 0x74

/**
 * Device is operating in Emergency mode.
 * @apiSince REL
 */

public static final int EMERGENCY_MODE = 2221; // 0x8ad

/**
 * EPS (Evolved Packet System) Mobility Management (EMM) access barred.
 * @apiSince REL
 */

public static final int EMM_ACCESS_BARRED = 115; // 0x73

/**
 * EPS (Evolved Packet System) Mobility Management (EMM) access barred infinity retry. *
 * @apiSince REL
 */

public static final int EMM_ACCESS_BARRED_INFINITE_RETRY = 121; // 0x79

/**
 * Attach procedure is rejected by the network.
 * @apiSince REL
 */

public static final int EMM_ATTACH_FAILED = 2115; // 0x843

/**
 * Attach procedure is started for EMC purposes.
 * @apiSince REL
 */

public static final int EMM_ATTACH_STARTED = 2116; // 0x844

/**
 * UE is detached.
 * @apiSince REL
 */

public static final int EMM_DETACHED = 2114; // 0x842

/**
 * T3417 timer expiration of the service request procedure.
 * @apiSince REL
 */

public static final int EMM_T3417_EXPIRED = 2130; // 0x852

/**
 * Extended service request fails due to expiration of the T3417 EXT timer.
 * @apiSince REL
 */

public static final int EMM_T3417_EXT_EXPIRED = 2131; // 0x853

/**
 * EPS and non-EPS services are not allowed by the network.
 * @apiSince REL
 */

public static final int EPS_SERVICES_AND_NON_EPS_SERVICES_NOT_ALLOWED = 2178; // 0x882

/**
 * EPS services are not allowed in the PLMN.
 * @apiSince REL
 */

public static final int EPS_SERVICES_NOT_ALLOWED_IN_PLMN = 2179; // 0x883

/**
 * Data call fail due to unspecific errors.
 * @apiSince REL
 */

public static final int ERROR_UNSPECIFIED = 65535; // 0xffff

/**
 * Bad OTA message is received from the network.
 * @apiSince REL
 */

public static final int ESM_BAD_OTA_MESSAGE = 2122; // 0x84a

/**
 * Bearer must be deactivated to synchronize with the network.
 * @apiSince REL
 */

public static final int ESM_BEARER_DEACTIVATED_TO_SYNC_WITH_NETWORK = 2120; // 0x848

/**
 * Collision scenarios for the UE and network-initiated procedures.
 * @apiSince REL
 */

public static final int ESM_COLLISION_SCENARIOS = 2119; // 0x847

/**
 * PDN was disconnected by the downlaod server due to IRAT.
 * @apiSince REL
 */

public static final int ESM_CONTEXT_TRANSFERRED_DUE_TO_IRAT = 2124; // 0x84c

/**
 * Download server rejected the call.
 * @apiSince REL
 */

public static final int ESM_DOWNLOAD_SERVER_REJECTED_THE_CALL = 2123; // 0x84b

/**
 * ESM level failure.
 * @apiSince REL
 */

public static final int ESM_FAILURE = 2182; // 0x886

/**
 * EPS Session Management (ESM) information is not received.
 * @apiSince REL
 */

public static final int ESM_INFO_NOT_RECEIVED = 53; // 0x35

/**
 * No specific local cause is mentioned, usually a valid OTA cause.
 * @apiSince REL
 */

public static final int ESM_LOCAL_CAUSE_NONE = 2126; // 0x84e

/**
 * Active dedicated bearer was requested for an existing default bearer.
 * @apiSince REL
 */

public static final int ESM_NW_ACTIVATED_DED_BEARER_WITH_ID_OF_DEF_BEARER = 2121; // 0x849

/**
 * ESM procedure maximum attempt timeout failure.
 * @apiSince REL
 */

public static final int ESM_PROCEDURE_TIME_OUT = 2155; // 0x86b

/**
 * Invalid EPS bearer identity in the request.
 * @apiSince REL
 */

public static final int ESM_UNKNOWN_EPS_BEARER_CONTEXT = 2111; // 0x83f

/**
 * Received a connection deny due to billing or authentication failure on EVDO network.
 * @apiSince REL
 */

public static final int EVDO_CONNECTION_DENY_BY_BILLING_OR_AUTHENTICATION_FAILURE = 2201; // 0x899

/**
 * Received a connection deny due to general or network busy on EVDO network.
 * @apiSince REL
 */

public static final int EVDO_CONNECTION_DENY_BY_GENERAL_OR_NETWORK_BUSY = 2200; // 0x898

/**
 * HDR system has been changed due to redirection or the PRL was not preferred.
 * @apiSince REL
 */

public static final int EVDO_HDR_CHANGED = 2202; // 0x89a

/**
 * Connection setup on the HDR system was time out.
 * @apiSince REL
 */

public static final int EVDO_HDR_CONNECTION_SETUP_TIMEOUT = 2206; // 0x89e

/**
 * Device exited HDR due to redirection or the PRL was not preferred.
 * @apiSince REL
 */

public static final int EVDO_HDR_EXITED = 2203; // 0x89b

/**
 * Device does not have an HDR session.
 * @apiSince REL
 */

public static final int EVDO_HDR_NO_SESSION = 2204; // 0x89c

/**
 * It is ending an HDR call origination in favor of a GPS fix.
 * @apiSince REL
 */

public static final int EVDO_USING_GPS_FIX_INSTEAD_OF_HDR_CALL = 2205; // 0x89d

/**
 * Device lost the system due to fade.
 * @apiSince REL
 */

public static final int FADE = 2217; // 0x8a9

/**
 * Device failed to acquire a co-located HDR for origination.
 * @apiSince REL
 */

public static final int FAILED_TO_ACQUIRE_COLOCATED_HDR = 2207; // 0x89f

/**
 * Feature not supported.
 * @apiSince REL
 */

public static final int FEATURE_NOT_SUPP = 40; // 0x28

/**
 * Semantic errors in packet filter.
 * @apiSince REL
 */

public static final int FILTER_SEMANTIC_ERROR = 44; // 0x2c

/**
 * Syntactical errors in packet filter(s).
 * @apiSince REL
 */

public static final int FILTER_SYTAX_ERROR = 45; // 0x2d

/**
 * PDN being brought up with an APN that is part of forbidden APN Name list.
 * @apiSince REL
 */

public static final int FORBIDDEN_APN_NAME = 2066; // 0x812

/**
 * Data fail due to GPRS registration failure.
 * @apiSince REL
 */

public static final int GPRS_REGISTRATION_FAIL = -2; // 0xfffffffe

/**
 * Not allowed to operate either GPRS or non-GPRS services.
 * @apiSince REL
 */

public static final int GPRS_SERVICES_AND_NON_GPRS_SERVICES_NOT_ALLOWED = 2097; // 0x831

/**
 * MS is not allowed to operate GPRS services.
 * @apiSince REL
 */

public static final int GPRS_SERVICES_NOT_ALLOWED = 2098; // 0x832

/**
 * UE requests GPRS service or the network initiates a detach request in a PLMN that does not
 * offer roaming for GPRS services.
 * @apiSince REL
 */

public static final int GPRS_SERVICES_NOT_ALLOWED_IN_THIS_PLMN = 2103; // 0x837

/**
 * System preference change back to SRAT during handoff
 * @apiSince REL
 */

public static final int HANDOFF_PREFERENCE_CHANGED = 2251; // 0x8cb

/**
 * HDR system access failure.
 * @apiSince REL
 */

public static final int HDR_ACCESS_FAILURE = 2213; // 0x8a5

/**
 * HDR module released the call due to fade.
 * @apiSince REL
 */

public static final int HDR_FADE = 2212; // 0x8a4

/**
 * HDR module could not be obtained because of the RF locked.
 * @apiSince REL
 */

public static final int HDR_NO_LOCK_GRANTED = 2210; // 0x8a2

/** @apiSince REL */

public static final int IFACE_AND_POL_FAMILY_MISMATCH = 120; // 0x78

/**
 * Interface mismatch.
 * @apiSince REL
 */

public static final int IFACE_MISMATCH = 117; // 0x75

/**
 * ME could not be authenticated and the ME used is not acceptable to the network.
 * @apiSince REL
 */

public static final int ILLEGAL_ME = 2096; // 0x830

/**
 * Network refuses service to the MS because either an identity of the MS is not acceptable to
 * the network or the MS does not pass the authentication check.
 * @apiSince REL
 */

public static final int ILLEGAL_MS = 2095; // 0x82f

/**
 * IMEI of the UE is not accepted by the network.
 * @apiSince REL
 */

public static final int IMEI_NOT_ACCEPTED = 2177; // 0x881

/**
 * Mobile reachable timer has expired, or the GMM context data related to the subscription does
 * not exist in the SGSN.
 * @apiSince REL
 */

public static final int IMPLICITLY_DETACHED = 2100; // 0x834

/**
 * IMSI present in the UE is unknown in the home subscriber server.
 * @apiSince REL
 */

public static final int IMSI_UNKNOWN_IN_HOME_SUBSCRIBER_SERVER = 2176; // 0x880

/**
 * The other clients rejected incoming call.
 * @apiSince REL
 */

public static final int INCOMING_CALL_REJECTED = 2092; // 0x82c

/**
 * Insufficient resources.
 * @apiSince REL
 */

public static final int INSUFFICIENT_RESOURCES = 26; // 0x1a

/**
 * The current interface is being in use.
 * @apiSince REL
 */

public static final int INTERFACE_IN_USE = 2058; // 0x80a

/**
 * Internal data call preempt by high priority APN.
 * @apiSince REL
 */

public static final int INTERNAL_CALL_PREEMPT_BY_HIGH_PRIO_APN = 114; // 0x72

/**
 ** Rejected/Brought down since UE is transition between EPC and NONEPC RAT.
 * @apiSince REL
 */

public static final int INTERNAL_EPC_NONEPC_TRANSITION = 2057; // 0x809

/**
 * No PDP exists with the given connection ID while modifying or deactivating or activation for
 * an already active PDP.
 * @apiSince REL
 */

public static final int INVALID_CONNECTION_ID = 2156; // 0x86c

/**
 * Not receiving a DNS address that was mandatory.
 * @apiSince REL
 */

public static final int INVALID_DNS_ADDR = 123; // 0x7b

/**
 * Invalid EMM state.
 * @apiSince REL
 */

public static final int INVALID_EMM_STATE = 2190; // 0x88e

/**
 * Invalid mandatory information.
 * @apiSince REL
 */

public static final int INVALID_MANDATORY_INFO = 96; // 0x60

/**
 * Device operational mode is different from the mode requested in the traffic channel bring up.
 * @apiSince REL
 */

public static final int INVALID_MODE = 2223; // 0x8af

/**
 * Invalid Proxy-Call Session Control Function (P-CSCF) address.
 * @apiSince REL
 */

public static final int INVALID_PCSCF_ADDR = 113; // 0x71

/**
 * Not receiving either a PCSCF or a DNS address, one of them being mandatory.
 * @apiSince REL
 */

public static final int INVALID_PCSCF_OR_DNS_ADDRESS = 124; // 0x7c

/**
 * Primary context for NSAPI does not exist.
 * @apiSince REL
 */

public static final int INVALID_PRIMARY_NSAPI = 2158; // 0x86e

/**
 * SIM was marked by the network as invalid for the circuit and/or packet service domain.
 * @apiSince REL
 */

public static final int INVALID_SIM_STATE = 2224; // 0x8b0

/**
 * Invalid transaction id.
 * @apiSince REL
 */

public static final int INVALID_TRANSACTION_ID = 81; // 0x51

/**
 * IPv6 address transfer failed.
 * @apiSince REL
 */

public static final int IPV6_ADDRESS_TRANSFER_FAILED = 2047; // 0x7ff

/**
 * Device failure to obtain the prefix from the network.
 * @apiSince REL
 */

public static final int IPV6_PREFIX_UNAVAILABLE = 2250; // 0x8ca

/**
 * IP address mismatch.
 * @apiSince REL
 */

public static final int IP_ADDRESS_MISMATCH = 119; // 0x77

/**
 * New call bring up is rejected since the existing data call IP type doesn't match the
 * requested IP.
 * @apiSince REL
 */

public static final int IP_VERSION_MISMATCH = 2055; // 0x807

/**
 * Data call was unsuccessfully transferred during the IRAT handover.
 * @apiSince REL
 */

public static final int IRAT_HANDOVER_FAILED = 2194; // 0x892

/**
 * Maximum access probes for the IS-707B call.
 * @apiSince REL
 */

public static final int IS707B_MAX_ACCESS_PROBES = 2089; // 0x829

/**
 * IPv6 interface bring up fails because the network provided only the IPv4 address for the
 * upcoming PDN permanent client can reattempt a IPv6 call bring up after the IPv4 interface is
 * also brought down. However, there is no guarantee that the network will provide a IPv6
 * address.
 * @apiSince REL
 */

public static final int LIMITED_TO_IPV4 = 2234; // 0x8ba

/**
 * IPv4 interface bring up fails because the network provided only the IPv6 address for the
 * upcoming PDN permanent client can reattempt a IPv4 call bring up after the IPv6 interface is
 * also brought down. However there is no guarantee that the network will provide a IPv4
 * address.
 * @apiSince REL
 */

public static final int LIMITED_TO_IPV6 = 2235; // 0x8bb

/**
 * Logical Link Control (LLC) Sub Network Dependent Convergence Protocol (SNDCP).
 * @apiSince REL
 */

public static final int LLC_SNDCP = 25; // 0x19

/**
 * Client ended the data call.
 * @apiSince REL
 */

public static final int LOCAL_END = 2215; // 0x8a7

/**
 * MS requests service, or the network initiates a detach request, in a location area where the
 * HPLMN determines that the MS, by subscription, is not allowed to operate.
 * @apiSince REL
 */

public static final int LOCATION_AREA_NOT_ALLOWED = 2102; // 0x836

/**
 * Data connection was lost.
 * @apiSince REL
 */

public static final int LOST_CONNECTION = 65540; // 0x10004

/**
 * Lower layer registration failure.
 * @apiSince REL
 */

public static final int LOWER_LAYER_REGISTRATION_FAILURE = 2197; // 0x895

/**
 * Device is going into lower power mode or powering down.
 * @apiSince REL
 */

public static final int LOW_POWER_MODE_OR_POWERING_DOWN = 2044; // 0x7fc

/**
 * Service request procedure failure.
 * @apiSince REL
 */

public static final int LTE_NAS_SERVICE_REQUEST_FAILED = 2117; // 0x845

/**
 * Throttling is not needed for this service request failure.
 * @apiSince REL
 */

public static final int LTE_THROTTLING_NOT_REQUIRED = 2127; // 0x84f

/**
 * MAC level failure.
 * @apiSince REL
 */

public static final int MAC_FAILURE = 2183; // 0x887

/**
 * Maximum NSAPIs have been exceeded during PDP activation.
 * @apiSince REL
 */

public static final int MAXIMIUM_NSAPIS_EXCEEDED = 2157; // 0x86d

/**
 * Maximum size of the L2 message was exceeded.
 * @apiSince REL
 */

public static final int MAXINUM_SIZE_OF_L2_MESSAGE_EXCEEDED = 2166; // 0x876

/**
 * Maximum access probes transmitted.
 * @apiSince REL
 */

public static final int MAX_ACCESS_PROBE = 2079; // 0x81f

/**
 * IPv4 data call bring up is rejected because the UE already maintains the allotted maximum
 * number of IPv4 data connections.
 * @apiSince REL
 */

public static final int MAX_IPV4_CONNECTIONS = 2052; // 0x804

/**
 * IPv6 data call bring up is rejected because the UE already maintains the allotted maximum
 * number of IPv6 data connections.
 * @apiSince REL
 */

public static final int MAX_IPV6_CONNECTIONS = 2053; // 0x805

/**
 * Maximum PPP inactivity timer expired.
 * @apiSince REL
 */

public static final int MAX_PPP_INACTIVITY_TIMER_EXPIRED = 2046; // 0x7fe

/**
 * Incorrect message semantic.
 * @apiSince REL
 */

public static final int MESSAGE_INCORRECT_SEMANTIC = 95; // 0x5f

/**
 * Unsupported message type.
 * @apiSince REL
 */

public static final int MESSAGE_TYPE_UNSUPPORTED = 97; // 0x61

/**
 * UE is in MIP-only configuration but the MIP configuration fails on call bring up due to
 * incorrect provisioning.
 * @apiSince REL
 */

public static final int MIP_CONFIG_FAILURE = 2050; // 0x802

/**
 * Foreign agent administratively prohibited MIP (Mobile IP) registration.
 * @apiSince REL
 */

public static final int MIP_FA_ADMIN_PROHIBITED = 2001; // 0x7d1

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of delivery style was not
 * supported.
 * @apiSince REL
 */

public static final int MIP_FA_DELIVERY_STYLE_NOT_SUPPORTED = 2012; // 0x7dc

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of requested encapsulation was
 * unavailable.
 * @apiSince REL
 */

public static final int MIP_FA_ENCAPSULATION_UNAVAILABLE = 2008; // 0x7d8

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of home agent authentication
 * failure.
 * @apiSince REL
 */

public static final int MIP_FA_HOME_AGENT_AUTHENTICATION_FAILURE = 2004; // 0x7d4

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of insufficient resources.
 * @apiSince REL
 */

public static final int MIP_FA_INSUFFICIENT_RESOURCES = 2002; // 0x7d2

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of malformed reply.
 * @apiSince REL
 */

public static final int MIP_FA_MALFORMED_REPLY = 2007; // 0x7d7

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of malformed request.
 * @apiSince REL
 */

public static final int MIP_FA_MALFORMED_REQUEST = 2006; // 0x7d6

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of missing challenge.
 * @apiSince REL
 */

public static final int MIP_FA_MISSING_CHALLENGE = 2017; // 0x7e1

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of missing Home Address.
 * @apiSince REL
 */

public static final int MIP_FA_MISSING_HOME_ADDRESS = 2015; // 0x7df

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of missing Home Agent.
 * @apiSince REL
 */

public static final int MIP_FA_MISSING_HOME_AGENT = 2014; // 0x7de

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of missing NAI (Network Access
 * Identifier).
 * @apiSince REL
 */

public static final int MIP_FA_MISSING_NAI = 2013; // 0x7dd

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of MN-AAA authenticator was
 * wrong.
 * @apiSince REL
 */

public static final int MIP_FA_MOBILE_NODE_AUTHENTICATION_FAILURE = 2003; // 0x7d3

/**
 * Reason unspecified for foreign agent rejected MIP (Mobile IP) registration.
 * @apiSince REL
 */

public static final int MIP_FA_REASON_UNSPECIFIED = 2000; // 0x7d0

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of requested lifetime was too
 * long.
 * @apiSince REL
 */

public static final int MIP_FA_REQUESTED_LIFETIME_TOO_LONG = 2005; // 0x7d5

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of reverse tunnel was mandatory
 * but not requested by device.
 * @apiSince REL
 */

public static final int MIP_FA_REVERSE_TUNNEL_IS_MANDATORY = 2011; // 0x7db

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of reverse tunnel was
 * unavailable.
 * @apiSince REL
 */

public static final int MIP_FA_REVERSE_TUNNEL_UNAVAILABLE = 2010; // 0x7da

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of stale challenge.
 * @apiSince REL
 */

public static final int MIP_FA_STALE_CHALLENGE = 2018; // 0x7e2

/**
 * Foreign agent rejected MIP (Mobile IP) registration because of unknown challenge.
 * @apiSince REL
 */

public static final int MIP_FA_UNKNOWN_CHALLENGE = 2016; // 0x7e0

/**
 * Foreign agent rejected MIP (Mobile IP) registration of VJ Header Compression was
 * unavailable.
 * @apiSince REL
 */

public static final int MIP_FA_VJ_HEADER_COMPRESSION_UNAVAILABLE = 2009; // 0x7d9

/**
 * Home agent administratively prohibited MIP (Mobile IP) registration.
 * @apiSince REL
 */

public static final int MIP_HA_ADMIN_PROHIBITED = 2020; // 0x7e4

/**
 * Home agent rejected MIP (Mobile IP) registration because of encapsulation unavailable.
 * @apiSince REL
 */

public static final int MIP_HA_ENCAPSULATION_UNAVAILABLE = 2029; // 0x7ed

/**
 * Home agent rejected MIP (Mobile IP) registration because of foreign agent authentication
 * failure.
 * @apiSince REL
 */

public static final int MIP_HA_FOREIGN_AGENT_AUTHENTICATION_FAILURE = 2023; // 0x7e7

/**
 * Home agent rejected MIP (Mobile IP) registration because of insufficient resources.
 * @apiSince REL
 */

public static final int MIP_HA_INSUFFICIENT_RESOURCES = 2021; // 0x7e5

/**
 * Home agent rejected MIP (Mobile IP) registration because of malformed request.
 * @apiSince REL
 */

public static final int MIP_HA_MALFORMED_REQUEST = 2025; // 0x7e9

/**
 * Home agent rejected MIP (Mobile IP) registration because of MN-HA authenticator was
 * wrong.
 * @apiSince REL
 */

public static final int MIP_HA_MOBILE_NODE_AUTHENTICATION_FAILURE = 2022; // 0x7e6

/**
 * Reason unspecified for home agent rejected MIP (Mobile IP) registration.
 * @apiSince REL
 */

public static final int MIP_HA_REASON_UNSPECIFIED = 2019; // 0x7e3

/**
 * Home agent rejected MIP (Mobile IP) registration because of registration id mismatch.
 * @apiSince REL
 */

public static final int MIP_HA_REGISTRATION_ID_MISMATCH = 2024; // 0x7e8

/**
 * Home agent rejected MIP (Mobile IP) registration because of reverse tunnel is mandatory but
 * not requested by device.
 * @apiSince REL
 */

public static final int MIP_HA_REVERSE_TUNNEL_IS_MANDATORY = 2028; // 0x7ec

/**
 * Home agent rejected MIP (Mobile IP) registration because of reverse tunnel was
 * unavailable.
 * @apiSince REL
 */

public static final int MIP_HA_REVERSE_TUNNEL_UNAVAILABLE = 2027; // 0x7eb

/**
 * Home agent rejected MIP (Mobile IP) registration because of unknown home agent address.
 * @apiSince REL
 */

public static final int MIP_HA_UNKNOWN_HOME_AGENT_ADDRESS = 2026; // 0x7ea

/**
 * Missing or unknown APN.
 * @apiSince REL
 */

public static final int MISSING_UNKNOWN_APN = 27; // 0x1b

/**
 * Another application in modem preempts the data call.
 * @apiSince REL
 */

public static final int MODEM_APP_PREEMPTED = 2032; // 0x7f0

/**
 * Modem restart.
 * @apiSince REL
 */

public static final int MODEM_RESTART = 2037; // 0x7f5

/**
 * Mobile switching center is temporarily unreachable.
 * @apiSince REL
 */

public static final int MSC_TEMPORARILY_NOT_REACHABLE = 2180; // 0x884

/**
 * Message and protocol state uncompatible.
 * @apiSince REL
 */

public static final int MSG_AND_PROTOCOL_STATE_UNCOMPATIBLE = 101; // 0x65

/**
 * Message type uncompatible.
 * @apiSince REL
 */

public static final int MSG_TYPE_NONCOMPATIBLE_STATE = 98; // 0x62

/**
 * No matching identity or context could be found in the network.
 * @apiSince REL
 */

public static final int MS_IDENTITY_CANNOT_BE_DERIVED_BY_THE_NETWORK = 2099; // 0x833

/**
 * Multiple PDP call feature is disabled.
 * @apiSince REL
 */

public static final int MULTIPLE_PDP_CALL_NOT_ALLOWED = 2192; // 0x890

/**
 * Multiple connections to a same PDN is not allowed.
 * @apiSince REL
 */

public static final int MULTI_CONN_TO_SAME_PDN_NOT_ALLOWED = 55; // 0x37

/**
 * Non-Access Spectrum layer failure.
 * @apiSince REL
 */

public static final int NAS_LAYER_FAILURE = 2191; // 0x88f

/**
 * Non-access stratum (NAS) request was rejected by the network.
 * @apiSince REL
 */

public static final int NAS_REQUEST_REJECTED_BY_NETWORK = 2167; // 0x877

/**
 * NAS signalling.
 * @apiSince REL
 */

public static final int NAS_SIGNALLING = 14; // 0xe

/**
 * Network Failure.
 * @apiSince REL
 */

public static final int NETWORK_FAILURE = 38; // 0x26

/**
 * Network-initiated detach without reattach.
 * @apiSince REL
 */

public static final int NETWORK_INITIATED_DETACH_NO_AUTO_REATTACH = 2154; // 0x86a

/**
 * Network-initiated detach with reattach.
 * @apiSince REL
 */

public static final int NETWORK_INITIATED_DETACH_WITH_AUTO_REATTACH = 2153; // 0x869

/**
 * Brought down by the network.
 * @apiSince REL
 */

public static final int NETWORK_INITIATED_TERMINATION = 2031; // 0x7ef

/**
 * There is no failure
 * @apiSince REL
 */

public static final int NONE = 0; // 0x0

/**
 * UE is unable to bring up a non-IP data call because the device is not camped on a NB1 cell.
 * @apiSince REL
 */

public static final int NON_IP_NOT_SUPPORTED = 2069; // 0x815

/**
 * Receiving a release from the base station with no reason.
 * @apiSince REL
 */

public static final int NORMAL_RELEASE = 2218; // 0x8aa

/**
 * Device does not have CDMA service.
 * @apiSince REL
 */

public static final int NO_CDMA_SERVICE = 2084; // 0x824

/**
 * There is no co-located HDR.
 * @apiSince REL
 */

public static final int NO_COLLOCATED_HDR = 2225; // 0x8b1

/**
 * No EPS bearer context was activated.
 * @apiSince REL
 */

public static final int NO_EPS_BEARER_CONTEXT_ACTIVATED = 2189; // 0x88d

/**
 * GPRS context is not available.
 * @apiSince REL
 */

public static final int NO_GPRS_CONTEXT = 2094; // 0x82e

/**
 * Device has no hybrid HDR service.
 * @apiSince REL
 */

public static final int NO_HYBRID_HDR_SERVICE = 2209; // 0x8a1

/**
 * MS requests an establishment of the radio access bearers for all active PDP contexts by
 * sending a service request message indicating data to the network, but the SGSN does not have
 * any active PDP context.
 * @apiSince REL
 */

public static final int NO_PDP_CONTEXT_ACTIVATED = 2107; // 0x83b

/**
 * There was no response received from the base station.
 * @apiSince REL
 */

public static final int NO_RESPONSE_FROM_BASE_STATION = 2081; // 0x821

/**
 * Device has no service.
 * @apiSince REL
 */

public static final int NO_SERVICE = 2216; // 0x8a8

/**
 * No service on the gateway.
 * @apiSince REL
 */

public static final int NO_SERVICE_ON_GATEWAY = 2093; // 0x82d

/**
 * The Network Service Access Point Identifier (NSAPI) is in use.
 * @apiSince REL
 */

public static final int NSAPI_IN_USE = 35; // 0x23

/**
 * PDN is attempted to be brought up with NULL APN but NULL APN is not supported.
 * @apiSince REL
 */

public static final int NULL_APN_DISALLOWED = 2061; // 0x80d

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_1 = 4097; // 0x1001

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_10 = 4106; // 0x100a

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_11 = 4107; // 0x100b

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_12 = 4108; // 0x100c

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_13 = 4109; // 0x100d

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_14 = 4110; // 0x100e

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_15 = 4111; // 0x100f

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_2 = 4098; // 0x1002

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_3 = 4099; // 0x1003

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_4 = 4100; // 0x1004

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_5 = 4101; // 0x1005

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_6 = 4102; // 0x1006

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_7 = 4103; // 0x1007

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_8 = 4104; // 0x1008

/** @apiSince REL */

public static final int OEM_DCFAILCAUSE_9 = 4105; // 0x1009

/**
 * Network supports IPv4v6 PDP type only. Non-IP type is not allowed. In LTE mode of operation,
 * this is a PDN throttling cause code, meaning the UE may throttle further requests to the
 * same APN.
 * @apiSince REL
 */

public static final int ONLY_IPV4V6_ALLOWED = 57; // 0x39

/**
 * Packet Data Protocol (PDP) type IPv4 only allowed.
 * @apiSince REL
 */

public static final int ONLY_IPV4_ALLOWED = 50; // 0x32

/**
 * Packet Data Protocol (PDP) type IPv6 only allowed.
 * @apiSince REL
 */

public static final int ONLY_IPV6_ALLOWED = 51; // 0x33

/**
 * Network supports non-IP PDP type only. IPv4, IPv6 and IPv4v6 is not allowed. In LTE mode of
 * operation, this is a PDN throttling cause code, meaning the UE can throttle further requests
 * to the same APN.
 * @apiSince REL
 */

public static final int ONLY_NON_IP_ALLOWED = 58; // 0x3a

/**
 * Single address bearers only allowed.
 * @apiSince REL
 */

public static final int ONLY_SINGLE_BEARER_ALLOWED = 52; // 0x34

/**
 * Operator determined barring. (no retry)
 * @apiSince REL
 */

public static final int OPERATOR_BARRED = 8; // 0x8

/**
 * OTASP commit is in progress.
 * @apiSince REL
 */

public static final int OTASP_COMMIT_IN_PROGRESS = 2208; // 0x8a0

/**
 * PDN connection does not exist.
 * @apiSince REL
 */

public static final int PDN_CONN_DOES_NOT_EXIST = 54; // 0x36

/**
 * PDN inactivity timer expired due to no data transmission in a configurable duration of time.
 * @apiSince REL
 */

public static final int PDN_INACTIVITY_TIMER_EXPIRED = 2051; // 0x803

/**
 * IPV4 PDN is in throttled state due to network providing only IPV6 address during the
 * previous VSNCP bringup (subs_limited_to_v6).
 * @apiSince REL
 */

public static final int PDN_IPV4_CALL_DISALLOWED = 2033; // 0x7f1

/**
 * IPV4 PDN is in throttled state due to previous VSNCP bringup failure(s).
 * @apiSince REL
 */

public static final int PDN_IPV4_CALL_THROTTLED = 2034; // 0x7f2

/**
 * IPV6 PDN is in throttled state due to network providing only IPV4 address during the
 * previous VSNCP bringup (subs_limited_to_v4).
 * @apiSince REL
 */

public static final int PDN_IPV6_CALL_DISALLOWED = 2035; // 0x7f3

/**
 * IPV6 PDN is in throttled state due to previous VSNCP bringup failure(s).
 * @apiSince REL
 */

public static final int PDN_IPV6_CALL_THROTTLED = 2036; // 0x7f4

/**
 * Non-IP PDN is in disallowed state due to the network providing only an IP address.
 * @apiSince REL
 */

public static final int PDN_NON_IP_CALL_DISALLOWED = 2071; // 0x817

/**
 * Non-IP PDN is in throttled state due to previous VSNCP bringup failure(s).
 * @apiSince REL
 */

public static final int PDN_NON_IP_CALL_THROTTLED = 2070; // 0x816

/**
 * SM attempts PDP activation for a maximum of four attempts.
 * @apiSince REL
 */

public static final int PDP_ACTIVATE_MAX_RETRY_FAILED = 2109; // 0x83d

/**
 * PDP context already exists.
 * @apiSince REL
 */

public static final int PDP_DUPLICATE = 2104; // 0x838

/**
 * Expiration of the PDP establish timer with a maximum of five retries.
 * @apiSince REL
 */

public static final int PDP_ESTABLISH_TIMEOUT_EXPIRED = 2161; // 0x871

/**
 * Expiration of the PDP deactivate timer with a maximum of four retries.
 * @apiSince REL
 */

public static final int PDP_INACTIVE_TIMEOUT_EXPIRED = 2163; // 0x873

/**
 * PDP activation failed due to RRC_ABORT or a forbidden PLMN.
 * @apiSince REL
 */

public static final int PDP_LOWERLAYER_ERROR = 2164; // 0x874

/**
 * MO PDP modify collision when the MT PDP is already in progress.
 * @apiSince REL
 */

public static final int PDP_MODIFY_COLLISION = 2165; // 0x875

/**
 * Expiration of the PDP modify timer with a maximum of four retries.
 * @apiSince REL
 */

public static final int PDP_MODIFY_TIMEOUT_EXPIRED = 2162; // 0x872

/**
 * PDP PPP calls are not supported.
 * @apiSince REL
 */

public static final int PDP_PPP_NOT_SUPPORTED = 2038; // 0x7f6

/**
 * Packet Data Protocol (PDP) without active traffic flow template (TFT).
 * @apiSince REL
 */

public static final int PDP_WITHOUT_ACTIVE_TFT = 46; // 0x2e

/**
 * Device is in use (e.g., voice call).
 * @apiSince REL
 */

public static final int PHONE_IN_USE = 2222; // 0x8ae

/**
 * Physical link is in the process of cleanup.
 * @apiSince REL
 */

public static final int PHYSICAL_LINK_CLOSE_IN_PROGRESS = 2040; // 0x7f8

/**
 * UE requests GPRS service, or the network initiates a detach request in a PLMN which does not
 * offer roaming for GPRS services to that MS.
 * @apiSince REL
 */

public static final int PLMN_NOT_ALLOWED = 2101; // 0x835

/**
 * Data call bring up fails in the PPP setup due to an authorization failure.
 * (e.g., authorization is required, but not negotiated with the network during an LCP phase)
 * @apiSince REL
 */

public static final int PPP_AUTH_FAILURE = 2229; // 0x8b5

/**
 * Data call bring up fails in the PPP setup due to a CHAP failure.
 * @apiSince REL
 */

public static final int PPP_CHAP_FAILURE = 2232; // 0x8b8

/**
 * Data call bring up fails in the PPP setup because the PPP is in the process of cleaning the
 * previous PPP session.
 * @apiSince REL
 */

public static final int PPP_CLOSE_IN_PROGRESS = 2233; // 0x8b9

/**
 * Data call bring up fails in the PPP setup due to an option mismatch.
 * @apiSince REL
 */

public static final int PPP_OPTION_MISMATCH = 2230; // 0x8b6

/**
 * Data call bring up fails in the PPP setup due to a PAP failure.
 * @apiSince REL
 */

public static final int PPP_PAP_FAILURE = 2231; // 0x8b7

/**
 * Data call bring up fails in the PPP setup due to a timeout.
 * (e.g., an LCP conf ack was not received from the network)
 * @apiSince REL
 */

public static final int PPP_TIMEOUT = 2228; // 0x8b4

/**
 * Preferred technology has changed, must retry with parameters appropriate for new technology.
 * @apiSince REL
 */

public static final int PREF_RADIO_TECH_CHANGED = -4; // 0xfffffffc

/**
 * APN bearer type in the profile does not match preferred network mode.
 * @apiSince REL
 */

public static final int PROFILE_BEARER_INCOMPATIBLE = 2042; // 0x7fa

/**
 * Protocol errors.
 * @apiSince REL
 */

public static final int PROTOCOL_ERRORS = 111; // 0x6f

/**
 * Quality of service (QoS) is not accepted.
 * @apiSince REL
 */

public static final int QOS_NOT_ACCEPTED = 37; // 0x25

/**
 * Radio access bearer failure.
 * @apiSince REL
 */

public static final int RADIO_ACCESS_BEARER_FAILURE = 2110; // 0x83e

/**
 * Radio access bearer is not established by the lower layers during activation, modification,
 * or deactivation.
 * @apiSince REL
 */

public static final int RADIO_ACCESS_BEARER_SETUP_FAILURE = 2160; // 0x870

/**
 * Data fail due to radio not unavailable.
 * @apiSince REL
 */

public static final int RADIO_NOT_AVAILABLE = 65537; // 0x10001

/**
 * data call was disconnected because radio was resetting, powered off.
 * @apiSince REL
 */

public static final int RADIO_POWER_OFF = -5; // 0xfffffffb

/**
 * Device is in the process of redirecting or handing off to a different target system.
 * @apiSince REL
 */

public static final int REDIRECTION_OR_HANDOFF_IN_PROGRESS = 2220; // 0x8ac

/**
 * Data fail due to registration failure.
 * @apiSince REL
 */

public static final int REGISTRATION_FAIL = -1; // 0xffffffff

/**
 * Regular deactivation.
 * @apiSince REL
 */

public static final int REGULAR_DEACTIVATION = 36; // 0x24

/**
 * The base station rejecting the call.
 * @apiSince REL
 */

public static final int REJECTED_BY_BASE_STATION = 2082; // 0x822

/**
 * Radio resource control (RRC) connection was aborted by the non-access stratum (NAS) after an
 * IRAT to LTE IRAT handover.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_ABORTED_AFTER_HANDOVER = 2173; // 0x87d

/**
 * Radio resource control (RRC) connection was aborted before deactivating the LTE stack after
 * a successful LTE to GSM/EDGE IRAT cell change order procedure.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_ABORTED_AFTER_IRAT_CELL_CHANGE = 2174; // 0x87e

/**
 * Radio resource control (RRC) connection was aborted before deactivating the LTE stack due to
 * a successful LTE to WCDMA/GSM/TD-SCDMA IRAT change.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_ABORTED_DUE_TO_IRAT_CHANGE = 2171; // 0x87b

/**
 * Radio resource control (RRC) connection was aborted in the middle of a LTE to GSM IRAT cell
 * change order procedure.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_ABORTED_DURING_IRAT_CELL_CHANGE = 2175; // 0x87f

/**
 * Connection has been released by the radio resource control (RRC) due to an abort request.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_ABORT_REQUEST = 2151; // 0x867

/**
 * Radio resource control (RRC) connection establishment failed due to access barrred.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_ACCESS_BARRED = 2139; // 0x85b

/**
 * Radio resource control (RRC) connection failure at access stratum.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_ACCESS_STRATUM_FAILURE = 2137; // 0x859

/**
 * Radio resource control (RRC) connection establishment is aborted due to another procedure.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_ANOTHER_PROCEDURE_IN_PROGRESS = 2138; // 0x85a

/**
 * Connection establishment failed as the radio resource control (RRC) is not camped on any
 * cell.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_CELL_NOT_CAMPED = 2144; // 0x860

/**
 * Radio resource control (RRC) connection establishment failed due to cell reselection at
 * access stratum.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_CELL_RESELECTION = 2140; // 0x85c

/**
 * Connection establishment failed due to configuration failure at the radio resource control
 * (RRC).
 * @apiSince REL
 */

public static final int RRC_CONNECTION_CONFIG_FAILURE = 2141; // 0x85d

/**
 * Radio resource control (RRC) connection establishment failure due to an error in the request
 * message.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_INVALID_REQUEST = 2168; // 0x878

/**
 * Connection establishment failed due to a link failure at the radio resource control (RRC).
 * @apiSince REL
 */

public static final int RRC_CONNECTION_LINK_FAILURE = 2143; // 0x85f

/**
 * Normal radio resource control (RRC) connection release.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_NORMAL_RELEASE = 2147; // 0x863

/**
 * UE is out of service during the call register.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_OUT_OF_SERVICE_DURING_CELL_REGISTER = 2150; // 0x866

/**
 * Radio resource control (RRC) connection release failed due to radio link failure conditions.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_RADIO_LINK_FAILURE = 2148; // 0x864

/**
 * Radio resource control (RRC) connection re-establishment failure.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_REESTABLISHMENT_FAILURE = 2149; // 0x865

/**
 * Radio resource control (RRC) connection establishment failed due to the network rejecting
 * the UE connection request.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_REJECT_BY_NETWORK = 2146; // 0x862

/**
 * If the UE has an LTE radio link failure before security is established, the radio resource
 * control (RRC) connection must be released and the UE must return to idle.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_RELEASED_SECURITY_NOT_ACTIVE = 2172; // 0x87c

/**
 * Radio resource control (RRC) connection establishment failure due to the RF was unavailable.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_RF_UNAVAILABLE = 2170; // 0x87a

/**
 * Radio resource control (RRC) connection released due to a system information block read
 * error.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_SYSTEM_INFORMATION_BLOCK_READ_ERROR = 2152; // 0x868

/**
 * Connection establishment failed due to a service interval failure at the radio resource
 * control (RRC).
 * @apiSince REL
 */

public static final int RRC_CONNECTION_SYSTEM_INTERVAL_FAILURE = 2145; // 0x861

/**
 * Radio resource control (RRC) connection could not be established in the time limit.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_TIMER_EXPIRED = 2142; // 0x85e

/**
 * Radio resource control (RRC) connection establishment failure due to a change in the
 * tracking area ID.
 * @apiSince REL
 */

public static final int RRC_CONNECTION_TRACKING_AREA_ID_CHANGED = 2169; // 0x879

/**
 * Radio resource control (RRC) uplink data delivery failed due to a connection release.
 * @apiSince REL
 */

public static final int RRC_UPLINK_CONNECTION_RELEASE = 2134; // 0x856

/**
 * Transmission failure of radio resource control (RRC) uplink data.
 * @apiSince REL
 */

public static final int RRC_UPLINK_DATA_TRANSMISSION_FAILURE = 2132; // 0x854

/**
 * Radio resource control (RRC) uplink data delivery failed due to a handover.
 * @apiSince REL
 */

public static final int RRC_UPLINK_DELIVERY_FAILED_DUE_TO_HANDOVER = 2133; // 0x855

/**
 * Radio resource control (RRC) is not connected but the non-access stratum (NAS) sends an
 * uplink data request.
 * @apiSince REL
 */

public static final int RRC_UPLINK_ERROR_REQUEST_FROM_NAS = 2136; // 0x858

/**
 * Radio resource control (RRC) uplink data delivery failed due to a radio link failure.
 * @apiSince REL
 */

public static final int RRC_UPLINK_RADIO_LINK_FAILURE = 2135; // 0x857

/**
 * RUIM not being present.
 * @apiSince REL
 */

public static final int RUIM_NOT_PRESENT = 2085; // 0x825

/**
 * Unspecified security mode reject.
 * @apiSince REL
 */

public static final int SECURITY_MODE_REJECTED = 2186; // 0x88a

/**
 * Service is not allowed on the requested PLMN.
 * @apiSince REL
 */

public static final int SERVICE_NOT_ALLOWED_ON_PLMN = 2129; // 0x851

/**
 * Requested service option not subscribed.
 * @apiSince REL
 */

public static final int SERVICE_OPTION_NOT_SUBSCRIBED = 33; // 0x21

/**
 * Service option not supported.
 * @apiSince REL
 */

public static final int SERVICE_OPTION_NOT_SUPPORTED = 32; // 0x20

/**
 * Service option temporarily out of order.
 * @apiSince REL
 */

public static final int SERVICE_OPTION_OUT_OF_ORDER = 34; // 0x22

/**
 * Data call drop due to network/modem disconnect.
 * @apiSince REL
 */

public static final int SIGNAL_LOST = -3; // 0xfffffffd

/**
 * Card was refreshed or removed.
 * @apiSince REL
 */

public static final int SIM_CARD_CHANGED = 2043; // 0x7fb

/**
 * Synchronization failure.
 * @apiSince REL
 */

public static final int SYNCHRONIZATION_FAILURE = 2184; // 0x888

/**
 * Test loop-back data call has been successfully brought down.
 * @apiSince REL
 */

public static final int TEST_LOOPBACK_REGULAR_DEACTIVATION = 2196; // 0x894

/**
 * Data call was disconnected by modem because tethered.
 * @apiSince REL
 */

public static final int TETHERED_CALL_ACTIVE = -6; // 0xfffffffa

/**
 * Semantic error in the Traffic flow templates (TFT) operation.
 * @apiSince REL
 */

public static final int TFT_SEMANTIC_ERROR = 41; // 0x29

/**
 * Syntactical error in the Traffic flow templates (TFT) operation.
 * @apiSince REL
 */

public static final int TFT_SYTAX_ERROR = 42; // 0x2a

/**
 * Put device in thermal emergency.
 * @apiSince REL
 */

public static final int THERMAL_EMERGENCY = 2090; // 0x82a

/**
 * Thermal level increases and causes calls to be torn down when normal mode of operation is
 * not allowed.
 * @apiSince REL
 */

public static final int THERMAL_MITIGATION = 2062; // 0x80e

/**
 * Target RAT swap failed.
 * @apiSince REL
 */

public static final int TRAT_SWAP_FAILED = 2048; // 0x800

/**
 * UE performs a detach or disconnect PDN action based on TE requirements.
 * @apiSince REL
 */

public static final int UE_INITIATED_DETACH_OR_DISCONNECT = 128; // 0x80

/**
 * UE is entering power save mode.
 * @apiSince REL
 */

public static final int UE_IS_ENTERING_POWERSAVE_MODE = 2226; // 0x8b2

/**
 * RAT change on the UE.
 * @apiSince REL
 */

public static final int UE_RAT_CHANGE = 2105; // 0x839

/**
 * UE security capabilities mismatch.
 * @apiSince REL
 */

public static final int UE_SECURITY_CAPABILITIES_MISMATCH = 2185; // 0x889

/**
 * UMTS interface is brought down due to handover from UMTS to iWLAN.
 * @apiSince REL
 */

public static final int UMTS_HANDOVER_TO_IWLAN = 2199; // 0x897

/**
 * Universal Mobile Telecommunications System (UMTS) reactivation request.
 * @apiSince REL
 */

public static final int UMTS_REACTIVATION_REQ = 39; // 0x27

/**
 * Unacceptable non-EPS authentication.
 * @apiSince REL
 */

public static final int UNACCEPTABLE_NON_EPS_AUTHENTICATION = 2187; // 0x88b

/**
 * Unknown data failure cause.
 * @apiSince REL
 */

public static final int UNKNOWN = 65536; // 0x10000

/**
 * Unknown info element.
 * @apiSince REL
 */

public static final int UNKNOWN_INFO_ELEMENT = 99; // 0x63

/**
 * Unknown Packet Data Protocol (PDP) address type.
 * @apiSince REL
 */

public static final int UNKNOWN_PDP_ADDRESS_TYPE = 28; // 0x1c

/**
 * Unknown Packet Data Protocol (PDP) context.
 * @apiSince REL
 */

public static final int UNKNOWN_PDP_CONTEXT = 43; // 0x2b

/**
 * RAT on which the data call is attempted/connected is no longer the preferred RAT.
 * @apiSince REL
 */

public static final int UNPREFERRED_RAT = 2039; // 0x7f7

/**
 * P_rev supported by 1 base station is less than 6, which is not supported for a 1X data call.
 * The UE must be in the footprint of BS which has p_rev >= 6 to support this SO33 call.
 * @apiSince REL
 */

public static final int UNSUPPORTED_1X_PREV = 2214; // 0x8a6

/**
 * Unsupported APN in current public land mobile network (PLMN).
 * @apiSince REL
 */

public static final int UNSUPPORTED_APN_IN_CURRENT_PLMN = 66; // 0x42

/**
 * QCI (QoS Class Identifier) indicated in the UE request cannot be supported.
 * @apiSince REL
 */

public static final int UNSUPPORTED_QCI_VALUE = 59; // 0x3b

/**
 * User authentication.
 * @apiSince REL
 */

public static final int USER_AUTHENTICATION = 29; // 0x1d

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of the VSNCP
 * configuration request with the reason of administratively prohibited at the HSGW.
 * @apiSince REL
 */

public static final int VSNCP_ADMINISTRATIVELY_PROHIBITED = 2245; // 0x8c5

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of the VSNCP
 * configuration request because the requested APN is unauthorized.
 * @apiSince REL
 */

public static final int VSNCP_APN_UNATHORIZED = 2238; // 0x8be

/**
 * Data call bring up fails in the VSNCP phase due to a general error. It's used when there is
 * no other specific error code available to report the failure.
 * @apiSince REL
 */

public static final int VSNCP_GEN_ERROR = 2237; // 0x8bd

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of the VSNCP
 * configuration request with the reason of insufficient parameter.
 * @apiSince REL
 */

public static final int VSNCP_INSUFFICIENT_PARAMETERS = 2243; // 0x8c3

/**
 * Data call bring up fails in the VSNCP phase due to the network rejected the VSNCP
 * configuration request due to no PDN gateway address.
 * @apiSince REL
 */

public static final int VSNCP_NO_PDN_GATEWAY_ADDRESS = 2240; // 0x8c0

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of the VSNCP
 * configuration request because the PDN exists for this APN.
 * @apiSince REL
 */

public static final int VSNCP_PDN_EXISTS_FOR_THIS_APN = 2248; // 0x8c8

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of the VSNCP
 * configuration request due to a PDN gateway reject.
 * @apiSince REL
 */

public static final int VSNCP_PDN_GATEWAY_REJECT = 2242; // 0x8c2

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of the VSNCP
 * configuration request because the PDN gateway is unreachable.
 * @apiSince REL
 */

public static final int VSNCP_PDN_GATEWAY_UNREACHABLE = 2241; // 0x8c1

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of PDN ID in use, or
 * all existing PDNs are brought down with this end reason because one of the PDN bring up was
 * rejected by the network with the reason of PDN ID in use.
 * @apiSince REL
 */

public static final int VSNCP_PDN_ID_IN_USE = 2246; // 0x8c6

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of the VSNCP
 * configuration request because the PDN limit has been exceeded.
 * @apiSince REL
 */

public static final int VSNCP_PDN_LIMIT_EXCEEDED = 2239; // 0x8bf

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of the VSNCP
 * configuration request with reconnect to this PDN not allowed, or an active data call is
 * terminated by the network because reconnection to this PDN is not allowed. Upon receiving
 * this error code from the network, the modem infinitely throttles the PDN until the next
 * power cycle.
 * @apiSince REL
 */

public static final int VSNCP_RECONNECT_NOT_ALLOWED = 2249; // 0x8c9

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of the VSNCP
 * configuration request with the reason of resource unavailable.
 * @apiSince REL
 */

public static final int VSNCP_RESOURCE_UNAVAILABLE = 2244; // 0x8c4

/**
 * Data call bring up fails in the VSNCP phase due to a network rejection of the VSNCP
 * configuration request for the reason of subscriber limitation.
 * @apiSince REL
 */

public static final int VSNCP_SUBSCRIBER_LIMITATION = 2247; // 0x8c7

/**
 * Data call bring up fails in the VSNCP phase due to a VSNCP timeout error.
 * @apiSince REL
 */

public static final int VSNCP_TIMEOUT = 2236; // 0x8bc
}

