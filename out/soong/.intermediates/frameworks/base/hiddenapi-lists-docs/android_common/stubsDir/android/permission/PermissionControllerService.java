/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.permission;

import java.util.List;
import java.util.Map;
import android.content.pm.PackageManager;
import android.content.Intent;

/**
 * This service is meant to be implemented by the app controlling permissions.
 *
 * @see PermissionControllerManager
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class PermissionControllerService extends android.app.Service {

public PermissionControllerService() { throw new RuntimeException("Stub!"); }

/**
 * Revoke a set of runtime permissions for various apps.
 *
 * @param requests The permissions to revoke as {@code Map<packageName, List<permission>>}
 * This value must never be {@code null}.
 * @param doDryRun Compute the permissions that would be revoked, but not actually revoke them
 * @param reason Why the permission should be revoked
 * Value is {@link android.permission.PermissionControllerManager#REASON_MALWARE}, or {@link android.permission.PermissionControllerManager#REASON_INSTALLER_POLICY_VIOLATION}
 * @param callerPackageName The package name of the calling app
 * This value must never be {@code null}.
 * @param callback Callback waiting for the actually removed permissions as
 * {@code Map<packageName, List<permission>>}
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onRevokeRuntimePermissions(@android.annotation.NonNull java.util.Map<java.lang.String,java.util.List<java.lang.String>> requests, boolean doDryRun, int reason, @android.annotation.NonNull java.lang.String callerPackageName, @android.annotation.NonNull java.util.function.Consumer<java.util.Map<java.lang.String,java.util.List<java.lang.String>>> callback);

/**
 * Create a backup of the runtime permissions.
 *
 * @param user The user to back up
 * This value must never be {@code null}.
 * @param backup The stream to write the backup to
 * This value must never be {@code null}.
 * @param callback Callback waiting for operation to be complete
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onGetRuntimePermissionsBackup(@android.annotation.NonNull android.os.UserHandle user, @android.annotation.NonNull java.io.OutputStream backup, @android.annotation.NonNull java.lang.Runnable callback);

/**
 * Restore a backup of the runtime permissions.
 *
 * <p>If an app mentioned in the backup is not installed the state should be saved to later
 * be restored via {@link #onRestoreDelayedRuntimePermissionsBackup}.
 *
 * @param user The user to restore
 * This value must never be {@code null}.
 * @param backup The stream to read the backup from
 * This value must never be {@code null}.
 * @param callback Callback waiting for operation to be complete
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onRestoreRuntimePermissionsBackup(@android.annotation.NonNull android.os.UserHandle user, @android.annotation.NonNull java.io.InputStream backup, @android.annotation.NonNull java.lang.Runnable callback);

/**
 * Restore the permission state of an app that was provided in
 * {@link #onRestoreRuntimePermissionsBackup} but could not be restored back then.
 *
 * @param packageName The app to restore
 * This value must never be {@code null}.
 * @param user The user to restore
 * This value must never be {@code null}.
 * @param callback Callback waiting for whether there is still delayed backup left
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onRestoreDelayedRuntimePermissionsBackup(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull android.os.UserHandle user, @android.annotation.NonNull java.util.function.Consumer<java.lang.Boolean> callback);

/**
 * Gets the runtime permissions for an app.
 *
 * @param packageName The package for which to query.
 * This value must never be {@code null}.
 * @param callback Callback waiting for the descriptions of the runtime permissions of the app
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onGetAppPermissions(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull java.util.function.Consumer<java.util.List<android.permission.RuntimePermissionPresentationInfo>> callback);

/**
 * Revokes the permission {@code permissionName} for app {@code packageName}
 *
 * @param packageName The package for which to revoke
 * This value must never be {@code null}.
 * @param permissionName The permission to revoke
 * This value must never be {@code null}.
 * @param callback Callback waiting for operation to be complete
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onRevokeRuntimePermission(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull java.lang.String permissionName, @android.annotation.NonNull java.lang.Runnable callback);

/**
 * Count how many apps have one of a set of permissions.
 *
 * @param permissionNames The permissions the app might have
 * This value must never be {@code null}.
 * @param flags Modify which apps to count. By default all non-system apps that request a
 *              permission are counted
 * Value is either <code>0</code> or a combination of {@link android.permission.PermissionControllerManager#COUNT_ONLY_WHEN_GRANTED}, and {@link android.permission.PermissionControllerManager#COUNT_WHEN_SYSTEM}
 * @param callback Callback waiting for the number of apps that have one of the permissions
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onCountPermissionApps(@android.annotation.NonNull java.util.List<java.lang.String> permissionNames, int flags, @android.annotation.NonNull java.util.function.IntConsumer callback);

/**
 * Count how many apps have used permissions.
 *
 * @param countSystem Also count system apps
 * @param numMillis The number of milliseconds in the past to check for uses
 * @param callback Callback waiting for the descriptions of the users of permissions
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onGetPermissionUsages(boolean countSystem, long numMillis, @android.annotation.NonNull java.util.function.Consumer<java.util.List<android.permission.RuntimePermissionUsageInfo>> callback);

/**
 * Grant or upgrade runtime permissions. The upgrade could be performed
 * based on whether the device upgraded, whether the permission database
 * version is old, or because the permission policy changed.
 *
 * @param callback Callback waiting for operation to be complete
 *
 * This value must never be {@code null}.
 * @see PackageManager#isDeviceUpgrading()
 * @see PermissionManager#getRuntimePermissionsVersion()
 * @see PermissionManager#setRuntimePermissionsVersion(int)
 * @apiSince REL
 */

public abstract void onGrantOrUpgradeDefaultRuntimePermissions(@android.annotation.NonNull java.lang.Runnable callback);

/**
 * Set the runtime permission state from a device admin.
 *
 * @param callerPackageName The package name of the admin requesting the change
 * This value must never be {@code null}.
 * @param packageName Package the permission belongs to
 * This value must never be {@code null}.
 * @param permission Permission to change
 * This value must never be {@code null}.
 * @param grantState State to set the permission into
 * Value is {@link android.app.admin.DevicePolicyManager#PERMISSION_GRANT_STATE_DEFAULT}, {@link android.app.admin.DevicePolicyManager#PERMISSION_GRANT_STATE_GRANTED}, or {@link android.app.admin.DevicePolicyManager#PERMISSION_GRANT_STATE_DENIED}
 * @param callback Callback waiting for whether the state could be set or not
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onSetRuntimePermissionGrantStateByDeviceAdmin(@android.annotation.NonNull java.lang.String callerPackageName, @android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull java.lang.String permission, int grantState, @android.annotation.NonNull java.util.function.Consumer<java.lang.Boolean> callback);

/**
 * {@inheritDoc}

 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public final android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * The {@link Intent} action that must be declared as handled by a service
 * in its manifest for the system to recognize it as a runtime permission
 * presenter service.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.permission.PermissionControllerService";
}

