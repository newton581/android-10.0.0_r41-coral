/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media.audiopolicy;

import java.util.List;
import android.media.AudioAttributes;

/**
 * A class to create the association between different playback attributes
 * (e.g. media, mapping direction) to a single volume control.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AudioVolumeGroup implements android.os.Parcelable {

/**
 * @param name of the volume group
 * @param id of the volume group
 * @param legacyStreamTypes of volume group
 */

AudioVolumeGroup(@android.annotation.NonNull java.lang.String name, int id, @android.annotation.NonNull android.media.AudioAttributes[] audioAttributes, @android.annotation.NonNull int[] legacyStreamTypes) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param o This value must never be {@code null}.
 * @apiSince REL
 */

public boolean equals(@android.annotation.NonNull java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * @return List of {@link AudioAttributes} involved in this {@link AudioVolumeGroup}.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<android.media.AudioAttributes> getAudioAttributes() { throw new RuntimeException("Stub!"); }

/**
 * @return the stream types involved in this {@link AudioVolumeGroup}.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public int[] getLegacyStreamTypes() { throw new RuntimeException("Stub!"); }

/**
 * @return human-readable name of this volume group.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String name() { throw new RuntimeException("Stub!"); }

/**
 * @return the volume group unique identifier id.
 * @apiSince REL
 */

public int getId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param dest This value must never be {@code null}.
 * @apiSince REL
 */

public void writeToParcel(@android.annotation.NonNull android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final android.os.Parcelable.Creator<android.media.audiopolicy.AudioVolumeGroup> CREATOR;
static { CREATOR = null; }

/**
 * Volume group value to use when introspection API fails.
 * @apiSince REL
 */

public static final int DEFAULT_VOLUME_GROUP = -1; // 0xffffffff
}

