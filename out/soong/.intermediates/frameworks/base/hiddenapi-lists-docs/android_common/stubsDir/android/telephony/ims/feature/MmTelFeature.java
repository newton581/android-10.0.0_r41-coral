/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims.feature;

import android.telephony.ims.stub.ImsCallSessionImplBase;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsReasonInfo;
import android.os.Message;
import android.telephony.ims.stub.ImsRegistrationImplBase;
import android.telephony.ims.stub.ImsUtImplBase;
import android.telephony.ims.stub.ImsEcbmImplBase;
import android.telephony.ims.stub.ImsMultiEndpointImplBase;
import android.telecom.TelecomManager;
import android.os.RemoteException;
import android.telephony.ims.stub.ImsSmsImplBase;

/**
 * Base implementation for Voice and SMS (IR-92) and Video (IR-94) IMS support.
 *
 * Any class wishing to use MmTelFeature should extend this class and implement all methods that the
 * service supports.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MmTelFeature extends android.telephony.ims.feature.ImsFeature {

public MmTelFeature() { throw new RuntimeException("Stub!"); }

/**
 * The current capability status that this MmTelFeature has defined is available. This
 * configuration will be used by the platform to figure out which capabilities are CURRENTLY
 * available to be used.
 *
 * Should be a subset of the capabilities that are enabled by the framework in
 * {@link #changeEnabledCapabilities}.
 * @return A copy of the current MmTelFeature capability status.
 * @apiSince REL
 */

public final android.telephony.ims.feature.MmTelFeature.MmTelCapabilities queryCapabilityStatus() { throw new RuntimeException("Stub!"); }

/**
 * Notify the framework that the status of the Capabilities has changed. Even though the
 * MmTelFeature capability may be enabled by the framework, the status may be disabled due to
 * the feature being unavailable from the network.
 * @param c The current capability status of the MmTelFeature. If a capability is disabled, then
 * the status of that capability is disabled. This can happen if the network does not currently
 * support the capability that is enabled. A capability that is disabled by the framework (via
 * {@link #changeEnabledCapabilities}) should also show the status as disabled.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public final void notifyCapabilitiesStatusChanged(@android.annotation.NonNull android.telephony.ims.feature.MmTelFeature.MmTelCapabilities c) { throw new RuntimeException("Stub!"); }

/**
 * Notify the framework of an incoming call.
 * @param c The {@link ImsCallSessionImplBase} of the new incoming call.
 
 * This value must never be {@code null}.
 
 * @param extras This value must never be {@code null}.
 * @apiSince REL
 */

public final void notifyIncomingCall(@android.annotation.NonNull android.telephony.ims.stub.ImsCallSessionImplBase c, @android.annotation.NonNull android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Notify the framework that a call has been implicitly rejected by this MmTelFeature
 * during call setup.
 * @param callProfile The {@link ImsCallProfile} IMS call profile with details.
 *        This can be null if no call information is available for the rejected call.
 * This value must never be {@code null}.
 * @param reason The {@link ImsReasonInfo} call rejection reason.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public final void notifyRejectedCall(@android.annotation.NonNull android.telephony.ims.ImsCallProfile callProfile, @android.annotation.NonNull android.telephony.ims.ImsReasonInfo reason) { throw new RuntimeException("Stub!"); }

/**
 * Notify the framework of a change in the Voice Message count.
 * @link count the new Voice Message count.
 * @apiSince REL
 */

public final void notifyVoiceMessageCountUpdate(int count) { throw new RuntimeException("Stub!"); }

/**
 * Provides the MmTelFeature with the ability to return the framework Capability Configuration
 * for a provided Capability. If the framework calls {@link #changeEnabledCapabilities} and
 * includes a capability A to enable or disable, this method should return the correct enabled
 * status for capability A.
 * @param capability The capability that we are querying the configuration for.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @param radioTech Value is either <code>0</code> or a combination of {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_NONE}, {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_LTE}, and {@link android.telephony.ims.stub.ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}
 * @return true if the capability is enabled, false otherwise.
 * @apiSince REL
 */

public boolean queryCapabilityConfiguration(int capability, int radioTech) { throw new RuntimeException("Stub!"); }

/**
 * The MmTelFeature should override this method to handle the enabling/disabling of
 * MmTel Features, defined in {@link MmTelCapabilities.MmTelCapability}. The framework assumes
 * the {@link CapabilityChangeRequest} was processed successfully. If a subset of capabilities
 * could not be set to their new values,
 * {@link CapabilityCallbackProxy#onChangeCapabilityConfigurationError} must be called
 * individually for each capability whose processing resulted in an error.
 *
 * Enabling/Disabling a capability here indicates that the capability should be registered or
 * deregistered (depending on the capability change) and become available or unavailable to
 * the framework.
 
 * @param request This value must never be {@code null}.
 
 * @param c This value must never be {@code null}.
 * @apiSince REL
 */

public void changeEnabledCapabilities(@android.annotation.NonNull android.telephony.ims.feature.CapabilityChangeRequest request, @android.annotation.NonNull android.telephony.ims.feature.ImsFeature.CapabilityCallbackProxy c) { throw new RuntimeException("Stub!"); }

/**
 * Creates a {@link ImsCallProfile} from the service capabilities & IMS registration state.
 *
 * @param callSessionType a service type that is specified in {@link ImsCallProfile}
 *        {@link ImsCallProfile#SERVICE_TYPE_NONE}
 *        {@link ImsCallProfile#SERVICE_TYPE_NORMAL}
 *        {@link ImsCallProfile#SERVICE_TYPE_EMERGENCY}
 * @param callType a call type that is specified in {@link ImsCallProfile}
 *        {@link ImsCallProfile#CALL_TYPE_VOICE}
 *        {@link ImsCallProfile#CALL_TYPE_VT}
 *        {@link ImsCallProfile#CALL_TYPE_VT_TX}
 *        {@link ImsCallProfile#CALL_TYPE_VT_RX}
 *        {@link ImsCallProfile#CALL_TYPE_VT_NODIR}
 *        {@link ImsCallProfile#CALL_TYPE_VS}
 *        {@link ImsCallProfile#CALL_TYPE_VS_TX}
 *        {@link ImsCallProfile#CALL_TYPE_VS_RX}
 * @return a {@link ImsCallProfile} object
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.telephony.ims.ImsCallProfile createCallProfile(int callSessionType, int callType) { throw new RuntimeException("Stub!"); }

/**
 * Creates an {@link ImsCallSession} with the specified call profile.
 * Use other methods, if applicable, instead of interacting with
 * {@link ImsCallSession} directly.
 *
 * @param profile a call profile to make the call
 
 * This value must never be {@code null}.
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.telephony.ims.stub.ImsCallSessionImplBase createCallSession(@android.annotation.NonNull android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * Called by the framework to determine if the outgoing call, designated by the outgoing
 * {@link String}s, should be processed as an IMS call or CSFB call. If this method's
 * functionality is not overridden, the platform will process every call as IMS as long as the
 * MmTelFeature reports that the {@link MmTelCapabilities#CAPABILITY_TYPE_VOICE} capability is
 * available.
 * @param numbers An array of {@link String}s that will be used for placing the call. There can
 *         be multiple {@link String}s listed in the case when we want to place an outgoing
 *         call as a conference.
 * This value must never be {@code null}.
 * @return a {@link ProcessCallResult} to the framework, which will be used to determine if the
 *        call will be placed over IMS or via CSFB.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature#PROCESS_CALL_IMS}, and {@link android.telephony.ims.feature.MmTelFeature#PROCESS_CALL_CSFB}
 * @apiSince REL
 */

public int shouldProcessCall(@android.annotation.NonNull java.lang.String[] numbers) { throw new RuntimeException("Stub!"); }

/**
 * @return The {@link ImsUtImplBase} Ut interface implementation for the supplementary service
 * configuration.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.stub.ImsUtImplBase getUt() { throw new RuntimeException("Stub!"); }

/**
 * @return The {@link ImsEcbmImplBase} Emergency call-back mode interface for emergency VoLTE
 * calls that support it.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.stub.ImsEcbmImplBase getEcbm() { throw new RuntimeException("Stub!"); }

/**
 * @return The {@link ImsMultiEndpointImplBase} implementation for implementing Dialog event
 * package processing for multi-endpoint.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.stub.ImsMultiEndpointImplBase getMultiEndpoint() { throw new RuntimeException("Stub!"); }

/**
 * Sets the current UI TTY mode for the MmTelFeature.
 * @param mode An integer containing the new UI TTY Mode, can consist of
 *         {@link TelecomManager#TTY_MODE_OFF},
 *         {@link TelecomManager#TTY_MODE_FULL},
 *         {@link TelecomManager#TTY_MODE_HCO},
 *         {@link TelecomManager#TTY_MODE_VCO}
 * @param onCompleteMessage If non-null, this MmTelFeature should call this {@link Message} when
 *         the operation is complete by using the associated {@link android.os.Messenger} in
 *         {@link Message#replyTo}. For example:
 * {@code
 *     // Set UI TTY Mode and other operations...
 *     try {
 *         // Notify framework that the mode was changed.
 *         Messenger uiMessenger = onCompleteMessage.replyTo;
 *         uiMessenger.send(onCompleteMessage);
 *     } catch (RemoteException e) {
 *         // Remote side is dead
 *     }
 * }
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void setUiTtyMode(int mode, @android.annotation.Nullable android.os.Message onCompleteMessage) { throw new RuntimeException("Stub!"); }

/**
 * Must be overridden by IMS Provider to be able to support SMS over IMS. Otherwise a default
 * non-functional implementation is returned.
 *
 * @return an instance of {@link ImsSmsImplBase} which should be implemented by the IMS
 * Provider.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.ims.stub.ImsSmsImplBase getSmsImplementation() { throw new RuntimeException("Stub!"); }

/**
 *{@inheritDoc}
 * @apiSince REL
 */

public void onFeatureRemoved() { throw new RuntimeException("Stub!"); }

/**
 *{@inheritDoc}
 * @apiSince REL
 */

public void onFeatureReady() { throw new RuntimeException("Stub!"); }

/**
 * To be returned by {@link #shouldProcessCall(String[])} when the telephony framework should
 * not process the outgoing call as IMS and should instead use circuit switch.
 * @apiSince REL
 */

public static final int PROCESS_CALL_CSFB = 1; // 0x1

/**
 * To be returned by {@link #shouldProcessCall(String[])} when the ImsService should process the
 * outgoing call as IMS.
 * @apiSince REL
 */

public static final int PROCESS_CALL_IMS = 0; // 0x0
/**
 * Contains the capabilities defined and supported by a MmTelFeature in the form of a Bitmask.
 * The capabilities that are used in MmTelFeature are defined as
 * {@link MmTelCapabilities#CAPABILITY_TYPE_VOICE},
 * {@link MmTelCapabilities#CAPABILITY_TYPE_VIDEO},
 * {@link MmTelCapabilities#CAPABILITY_TYPE_UT}, and
 * {@link MmTelCapabilities#CAPABILITY_TYPE_SMS}.
 *
 * The capabilities of this MmTelFeature will be set by the framework and can be queried with
 * {@link #queryCapabilityStatus()}.
 *
 * This MmTelFeature can then return the status of each of these capabilities (enabled or not)
 * by sending a {@link #notifyCapabilitiesStatusChanged} callback to the framework. The current
 * status can also be queried using {@link #queryCapabilityStatus()}.
 * @see #isCapable(int)
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class MmTelCapabilities extends android.telephony.ims.feature.ImsFeature.Capabilities {

/**
 * Create a new empty {@link MmTelCapabilities} instance.
 * @see #addCapabilities(int)
 * @see #removeCapabilities(int)
 * @apiSince REL
 */

public MmTelCapabilities() { throw new RuntimeException("Stub!"); }

/**
 *@deprecated Use {@link MmTelCapabilities} to construct a new instance instead.
 * @apiSince REL
 */

@Deprecated
public MmTelCapabilities(android.telephony.ims.feature.ImsFeature.Capabilities c) { throw new RuntimeException("Stub!"); }

/**
 * Create a new {link @MmTelCapabilities} instance with the provided capabilities.
 * @param capabilities The capabilities that are supported for MmTel in the form of a
 *                     bitfield.
 * @apiSince REL
 */

public MmTelCapabilities(int capabilities) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param capabilities Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @apiSince REL
 */

public final void addCapabilities(int capabilities) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param capability Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @apiSince REL
 */

public final void removeCapabilities(int capability) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param capabilities Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @apiSince REL
 */

public final boolean isCapable(int capabilities) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * This MmTelFeature supports SMS (IR.92)
 * @apiSince REL
 */

public static final int CAPABILITY_TYPE_SMS = 8; // 0x8

/**
 * This MmTelFeature supports XCAP over Ut for supplementary services. (IR.92)
 * @apiSince REL
 */

public static final int CAPABILITY_TYPE_UT = 4; // 0x4

/**
 * This MmTelFeature supports Video (IR.94)
 * @apiSince REL
 */

public static final int CAPABILITY_TYPE_VIDEO = 2; // 0x2

/**
 * This MmTelFeature supports Voice calling (IR.92)
 * @apiSince REL
 */

public static final int CAPABILITY_TYPE_VOICE = 1; // 0x1
/**
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO}, {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, and {@link android.telephony.ims.feature.MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface MmTelCapability {
}

}

/**
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.MmTelFeature#PROCESS_CALL_IMS}, and {@link android.telephony.ims.feature.MmTelFeature#PROCESS_CALL_CSFB}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface ProcessCallResult {
}

}

