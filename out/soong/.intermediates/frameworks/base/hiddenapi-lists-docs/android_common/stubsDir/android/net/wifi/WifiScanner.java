/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.wifi;

import android.content.Context;
import android.os.Looper;
import android.Manifest;
import android.os.WorkSource;
import android.os.Parcelable;

/**
 * This class provides a way to scan the Wifi universe around the device
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class WifiScanner {

WifiScanner() { throw new RuntimeException("Stub!"); }

/** start wifi scan in background
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param settings specifies various parameters for the scan; for more information look at
 * {@link ScanSettings}
 * @param listener specifies the object to report events to. This object is also treated as a
 *                 key for this scan, and must also be specified to cancel the scan. Multiple
 *                 scans should also not share this object.
 * @apiSince REL
 */

public void startBackgroundScan(android.net.wifi.WifiScanner.ScanSettings settings, android.net.wifi.WifiScanner.ScanListener listener) { throw new RuntimeException("Stub!"); }

/** start wifi scan in background
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param settings specifies various parameters for the scan; for more information look at
 * {@link ScanSettings}
 * @param workSource WorkSource to blame for power usage
 * @param listener specifies the object to report events to. This object is also treated as a
 *                 key for this scan, and must also be specified to cancel the scan. Multiple
 *                 scans should also not share this object.
 * @apiSince REL
 */

public void startBackgroundScan(android.net.wifi.WifiScanner.ScanSettings settings, android.net.wifi.WifiScanner.ScanListener listener, android.os.WorkSource workSource) { throw new RuntimeException("Stub!"); }

/**
 * stop an ongoing wifi scan
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param listener specifies which scan to cancel; must be same object as passed in {@link
 *  #startBackgroundScan}
 * @apiSince REL
 */

public void stopBackgroundScan(android.net.wifi.WifiScanner.ScanListener listener) { throw new RuntimeException("Stub!"); }

/**
 * reports currently available scan results on appropriate listeners
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @return true if all scan results were reported correctly
 * @apiSince REL
 */

public boolean getScanResults() { throw new RuntimeException("Stub!"); }

/**
 * starts a single scan and reports results asynchronously
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param settings specifies various parameters for the scan; for more information look at
 * {@link ScanSettings}
 * @param listener specifies the object to report events to. This object is also treated as a
 *                 key for this scan, and must also be specified to cancel the scan. Multiple
 *                 scans should also not share this object.
 * @apiSince REL
 */

public void startScan(android.net.wifi.WifiScanner.ScanSettings settings, android.net.wifi.WifiScanner.ScanListener listener) { throw new RuntimeException("Stub!"); }

/**
 * starts a single scan and reports results asynchronously
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param settings specifies various parameters for the scan; for more information look at
 * {@link ScanSettings}
 * @param workSource WorkSource to blame for power usage
 * @param listener specifies the object to report events to. This object is also treated as a
 *                 key for this scan, and must also be specified to cancel the scan. Multiple
 *                 scans should also not share this object.
 * @apiSince REL
 */

public void startScan(android.net.wifi.WifiScanner.ScanSettings settings, android.net.wifi.WifiScanner.ScanListener listener, android.os.WorkSource workSource) { throw new RuntimeException("Stub!"); }

/**
 * stops an ongoing single shot scan; only useful after {@link #startScan} if onResults()
 * hasn't been called on the listener, ignored otherwise
 * <br>
 * Requires {@link android.Manifest.permission#LOCATION_HARDWARE}
 * @param listener
 * @apiSince REL
 */

public void stopScan(android.net.wifi.WifiScanner.ScanListener listener) { throw new RuntimeException("Stub!"); }

/** configure WifiChange detection
 * @param rssiSampleSize number of samples used for RSSI averaging
 * @param lostApSampleSize number of samples to confirm an access point's loss
 * @param unchangedSampleSize number of samples to confirm there are no changes
 * @param minApsBreachingThreshold minimum number of access points that need to be
 *                                 out of range to detect WifiChange
 * @param periodInMs indicates period of scan to find changes
 * @param bssidInfos access points to watch
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void configureWifiChange(int rssiSampleSize, int lostApSampleSize, int unchangedSampleSize, int minApsBreachingThreshold, int periodInMs, android.net.wifi.WifiScanner.BssidInfo[] bssidInfos) { throw new RuntimeException("Stub!"); }

/**
 * track changes in wifi environment
 * @param listener object to report events on; this object must be unique and must also be
 *                 provided on {@link #stopTrackingWifiChange}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void startTrackingWifiChange(android.net.wifi.WifiScanner.WifiChangeListener listener) { throw new RuntimeException("Stub!"); }

/**
 * stop tracking changes in wifi environment
 * @param listener object that was provided to report events on {@link
 * #stopTrackingWifiChange}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void stopTrackingWifiChange(android.net.wifi.WifiScanner.WifiChangeListener listener) { throw new RuntimeException("Stub!"); }

/** @hide */

@Deprecated
public void configureWifiChange(android.net.wifi.WifiScanner.WifiChangeSettings settings) { throw new RuntimeException("Stub!"); }

/**
 * set interesting access points to find
 * @param bssidInfos access points of interest
 * @param apLostThreshold number of scans needed to indicate that AP is lost
 * @param listener object provided to report events on; this object must be unique and must
 *                 also be provided on {@link #stopTrackingBssids}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void startTrackingBssids(android.net.wifi.WifiScanner.BssidInfo[] bssidInfos, int apLostThreshold, android.net.wifi.WifiScanner.BssidListener listener) { throw new RuntimeException("Stub!"); }

/**
 * remove tracking of interesting access points
 * @param listener same object provided in {@link #startTrackingBssids}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void stopTrackingBssids(android.net.wifi.WifiScanner.BssidListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Maximum supported scanning period
 * @apiSince REL
 */

public static final int MAX_SCAN_PERIOD_MS = 1024000; // 0xfa000

/**
 * Minimum supported scanning period
 * @apiSince REL
 */

public static final int MIN_SCAN_PERIOD_MS = 1000; // 0x3e8

/**
 * An outstanding request with the same listener hasn't finished yet.
 * @apiSince REL
 */

public static final int REASON_DUPLICATE_REQEUST = -5; // 0xfffffffb

/**
 * Invalid listener
 * @apiSince REL
 */

public static final int REASON_INVALID_LISTENER = -2; // 0xfffffffe

/**
 * Invalid request
 * @apiSince REL
 */

public static final int REASON_INVALID_REQUEST = -3; // 0xfffffffd

/**
 * Invalid request
 * @apiSince REL
 */

public static final int REASON_NOT_AUTHORIZED = -4; // 0xfffffffc

/**
 * No Error
 * @apiSince REL
 */

public static final int REASON_SUCCEEDED = 0; // 0x0

/**
 * Unknown error
 * @apiSince REL
 */

public static final int REASON_UNSPECIFIED = -1; // 0xffffffff

/**
 * reports {@link ScanListener#onResults} when underlying buffers are full
 * this is simply the lack of the {@link #REPORT_EVENT_AFTER_EACH_SCAN} flag
 * @deprecated It is not supported anymore.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int REPORT_EVENT_AFTER_BUFFER_FULL = 0; // 0x0

/**
 * reports {@link ScanListener#onResults} after each scan
 * @apiSince REL
 */

public static final int REPORT_EVENT_AFTER_EACH_SCAN = 1; // 0x1

/**
 * reports {@link ScanListener#onFullResult} whenever each beacon is discovered
 * @apiSince REL
 */

public static final int REPORT_EVENT_FULL_SCAN_RESULT = 2; // 0x2

/**
 * Do not place scans in the chip's scan history buffer
 * @apiSince REL
 */

public static final int REPORT_EVENT_NO_BATCH = 4; // 0x4

/**
 * 2.4 GHz band
 * @apiSince REL
 */

public static final int WIFI_BAND_24_GHZ = 1; // 0x1

/**
 * 5 GHz band excluding DFS channels
 * @apiSince REL
 */

public static final int WIFI_BAND_5_GHZ = 2; // 0x2

/**
 * DFS channels from 5 GHz band only
 * @apiSince REL
 */

public static final int WIFI_BAND_5_GHZ_DFS_ONLY = 4; // 0x4

/**
 * 5 GHz band including DFS channels
 * @apiSince REL
 */

public static final int WIFI_BAND_5_GHZ_WITH_DFS = 6; // 0x6

/**
 * Both 2.4 GHz band and 5 GHz band; no DFS channels
 * @apiSince REL
 */

public static final int WIFI_BAND_BOTH = 3; // 0x3

/**
 * Both 2.4 GHz band and 5 GHz band; with DFS channels
 * @apiSince REL
 */

public static final int WIFI_BAND_BOTH_WITH_DFS = 7; // 0x7

/**
 * no band specified; use channel list instead
 * @apiSince REL
 */

public static final int WIFI_BAND_UNSPECIFIED = 0; // 0x0
/**
 * Generic action callback invocation interface
 *  @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface ActionListener {

/** @apiSince REL */

public void onSuccess();

/** @apiSince REL */

public void onFailure(int reason, java.lang.String description);
}

/**
 * specifies information about an access point of interest
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class BssidInfo {

@Deprecated
public BssidInfo() { throw new RuntimeException("Stub!"); }

/**
 * bssid of the access point; in XX:XX:XX:XX:XX:XX format
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public java.lang.String bssid;

/**
 * channel frequency (in KHz) where you may find this BSSID
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int frequencyHint;

/**
 * high signal threshold; more information at {@link ScanResult#level}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int high;

/**
 * low signal strength threshold; more information at {@link ScanResult#level}
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int low;
}

/**
 * interface to receive hotlist events on; use this on {@link #setHotlist}
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static interface BssidListener extends android.net.wifi.WifiScanner.ActionListener {

/** indicates that access points were found by on going scans
 * @param results list of scan results, one for each access point visible currently
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onFound(android.net.wifi.ScanResult[] results);

/** indicates that access points were missed by on going scans
 * @param results list of scan results, for each access point that is not visible anymore
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onLost(android.net.wifi.ScanResult[] results);
}

/**
 * provides channel specification for scanning
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ChannelSpec {

/**
 * default constructor for channel spec
 * @apiSince REL
 */

public ChannelSpec(int frequency) { throw new RuntimeException("Stub!"); }

/**
 * channel frequency in MHz; for example channel 1 is specified as 2412
 * @apiSince REL
 */

public int frequency;
}

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class HotlistSettings implements android.os.Parcelable {

@Deprecated
public HotlistSettings() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int apLostThreshold;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public android.net.wifi.WifiScanner.BssidInfo[] bssidInfos;
}

/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ParcelableScanData implements android.os.Parcelable {

/** @apiSince REL */

public ParcelableScanData(android.net.wifi.WifiScanner.ScanData[] results) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.net.wifi.WifiScanner.ScanData[] getResults() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.net.wifi.WifiScanner.ScanData[] mResults;
}

/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ParcelableScanResults implements android.os.Parcelable {

/** @apiSince REL */

public ParcelableScanResults(android.net.wifi.ScanResult[] results) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.net.wifi.ScanResult[] getResults() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.net.wifi.ScanResult[] mResults;
}

/**
 * all the information garnered from a single scan
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ScanData implements android.os.Parcelable {

/** @apiSince REL */

public ScanData(int id, int flags, android.net.wifi.ScanResult[] results) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public ScanData(android.net.wifi.WifiScanner.ScanData s) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getId() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getFlags() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.net.wifi.ScanResult[] getResults() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }
}

/**
 * interface to get scan events on; specify this on {@link #startBackgroundScan} or
 * {@link #startScan}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface ScanListener extends android.net.wifi.WifiScanner.ActionListener {

/**
 * Framework co-ordinates scans across multiple apps; so it may not give exactly the
 * same period requested. If period of a scan is changed; it is reported by this event.
 * @apiSince REL
 */

public void onPeriodChanged(int periodInMs);

/**
 * reports results retrieved from background scan and single shot scans
 * @apiSince REL
 */

public void onResults(android.net.wifi.WifiScanner.ScanData[] results);

/**
 * reports full scan result for each access point found in scan
 * @apiSince REL
 */

public void onFullResult(android.net.wifi.ScanResult fullScanResult);
}

/**
 * scan configuration parameters to be sent to {@link #startBackgroundScan}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ScanSettings implements android.os.Parcelable {

public ScanSettings() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * one of the WIFI_BAND values
 * @apiSince REL
 */

public int band;

/**
 * list of channels; used when band is set to WIFI_BAND_UNSPECIFIED
 * @apiSince REL
 */

public android.net.wifi.WifiScanner.ChannelSpec[] channels;

/**
 * This scan request will be hidden from app-ops noting for location information. This
 * should only be used by FLP/NLP module on the device which is using the scan results to
 * compute results for behalf on their clients. FLP/NLP module using this flag should ensure
 * that they note in app-ops the eventual delivery of location information computed using
 * these results to their client .
 * {@hide}
 */

public boolean hideFromAppOps;

/**
 * This scan request may ignore location settings while receiving scans. This should only
 * be used in emergency situations.
 * {@hide}
 */

public boolean ignoreLocationSettings;

/**
 * if maxPeriodInMs is non zero or different than period, then this bucket is
 * a truncated binary exponential backoff bucket and the scan period will grow
 * exponentially as per formula: actual_period(N) = period * (2 ^ (N/stepCount))
 * to maxPeriodInMs
 * @apiSince REL
 */

public int maxPeriodInMs;

/**
 * defines number of scans to cache; use it with REPORT_EVENT_AFTER_BUFFER_FULL
 * to wake up at fixed interval
 * @apiSince REL
 */

public int maxScansToCache;

/**
 * defines number of bssids to cache from each scan
 * @apiSince REL
 */

public int numBssidsPerScan;

/**
 * period of background scan; in millisecond, 0 => single shot scan
 * @apiSince REL
 */

public int periodInMs;

/**
 * must have a valid REPORT_EVENT value
 * @apiSince REL
 */

public int reportEvents;

/**
 * for truncated binary exponential back off bucket, number of scans to perform
 * for a given period
 * @apiSince REL
 */

public int stepCount;
}

/**
 * interface to get wifi change events on; use this on {@link #startTrackingWifiChange}
 * @apiSince REL
 * @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static interface WifiChangeListener extends android.net.wifi.WifiScanner.ActionListener {

/** indicates that changes were detected in wifi environment
 * @param results indicate the access points that exhibited change
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onChanging(android.net.wifi.ScanResult[] results);

/** indicates that no wifi changes are being detected for a while
 * @param results indicate the access points that are bing monitored for change
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void onQuiescence(android.net.wifi.ScanResult[] results);
}

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class WifiChangeSettings implements android.os.Parcelable {

@Deprecated
public WifiChangeSettings() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** Implement the Parcelable interface {@hide} */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public android.net.wifi.WifiScanner.BssidInfo[] bssidInfos;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int lostApSampleSize;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int minApsBreachingThreshold;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int periodInMs;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int rssiSampleSize;

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public int unchangedSampleSize;
}

}

