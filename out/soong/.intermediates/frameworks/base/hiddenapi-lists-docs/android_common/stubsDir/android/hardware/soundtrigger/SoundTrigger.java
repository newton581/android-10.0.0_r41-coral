/**
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.soundtrigger;

import android.os.Handler;

/**
 * The SoundTrigger class provides access via JNI to the native service managing
 * the sound trigger HAL.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class SoundTrigger {

SoundTrigger() { throw new RuntimeException("Stub!"); }

/**
 * Status code used when the operation succeeded
 * @apiSince REL
 */

public static final int STATUS_OK = 0; // 0x0
/**
 *  A RecognitionEvent is provided by the
 *  {@code StatusListener#onRecognition(RecognitionEvent)}
 *  callback upon recognition success or failure.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class RecognitionEvent {

/** @hide */

RecognitionEvent(int status, int soundModelHandle, boolean captureAvailable, int captureSession, int captureDelayMs, int capturePreambleMs, boolean triggerInData, android.media.AudioFormat captureFormat, byte[] data) { throw new RuntimeException("Stub!"); }

/**
 * Check if is possible to capture audio from this utterance buffered by the hardware.
 *
 * @return {@code true} iff a capturing is possible
 * @apiSince REL
 */

public boolean isCaptureAvailable() { throw new RuntimeException("Stub!"); }

/**
 * Get the audio format of either the trigger in event data or to use for capture of the
 * rest of the utterance
 *
 * @return the audio format
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.media.AudioFormat getCaptureFormat() { throw new RuntimeException("Stub!"); }

/**
 * Get Audio session ID to be used when capturing the utterance with an {@link AudioRecord}
 * if {@link #isCaptureAvailable()} is true.
 *
 * @return The id of the capture session
 * @apiSince REL
 */

public int getCaptureSession() { throw new RuntimeException("Stub!"); }

/**
 * Get the opaque data for use by system applications who know about voice engine
 * internals, typically during enrollment.
 *
 * @return The data of the event
 * @apiSince REL
 */

public byte[] getData() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

}

