/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.app;

import android.os.Bundle;

/**
 * Helper class for building an options Bundle that can be used with
 * {@link android.content.Context#sendBroadcast(android.content.Intent)
 * Context.sendBroadcast(Intent)} and related methods.
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class BroadcastOptions {

BroadcastOptions() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static android.app.BroadcastOptions makeBasic() { throw new RuntimeException("Stub!"); }

/**
 * Set a duration for which the system should temporary place an application on the
 * power whitelist when this broadcast is being delivered to it.
 * <br>
 * Requires {@link android.Manifest.permission#CHANGE_DEVICE_IDLE_TEMP_WHITELIST}
 * @param duration The duration in milliseconds; 0 means to not place on whitelist.
 * @apiSince REL
 */

public void setTemporaryAppWhitelistDuration(long duration) { throw new RuntimeException("Stub!"); }

/**
 * Sets whether pending intent can be sent for an application with background restrictions
 * @param dontSendToRestrictedApps if true, pending intent will not be sent for an application
 * with background restrictions. Default value is {@code false}
 * @apiSince REL
 */

public void setDontSendToRestrictedApps(boolean dontSendToRestrictedApps) { throw new RuntimeException("Stub!"); }

/**
 * Sets the process will be able to start activities from background for the duration of
 * the broadcast dispatch. Default value is {@code false}
 
 * <br>
 * Requires android.Manifest.permission.START_ACTIVITIES_FROM_BACKGROUND
 * @apiSince REL
 */

public void setBackgroundActivityStartsAllowed(boolean allowBackgroundActivityStarts) { throw new RuntimeException("Stub!"); }

/**
 * Returns the created options as a Bundle, which can be passed to
 * {@link android.content.Context#sendBroadcast(android.content.Intent)
 * Context.sendBroadcast(Intent)} and related methods.
 * Note that the returned Bundle is still owned by the BroadcastOptions
 * object; you must not modify it, but can supply it to the sendBroadcast
 * methods that take an options Bundle.
 * @apiSince REL
 */

public android.os.Bundle toBundle() { throw new RuntimeException("Stub!"); }
}

