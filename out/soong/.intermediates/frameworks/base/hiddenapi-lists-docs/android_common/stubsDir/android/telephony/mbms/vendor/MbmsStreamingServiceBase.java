/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.mbms.vendor;

import android.content.Intent;
import android.telephony.mbms.MbmsErrors;
import java.util.List;
import android.net.Uri;
import android.os.Binder;

/**
 * Base class for MBMS streaming services. The middleware should return an instance of this
 * object from its {@link android.app.Service#onBind(Intent)} method.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MbmsStreamingServiceBase extends android.os.Binder {

public MbmsStreamingServiceBase() { throw new RuntimeException("Stub!"); }

/**
 * Initialize streaming service for this app and subId, registering the listener.
 *
 * May throw an {@link IllegalArgumentException} or a {@link SecurityException}, which
 * will be intercepted and passed to the app as
 * {@link MbmsErrors.InitializationErrors#ERROR_UNABLE_TO_INITIALIZE}
 *
 * May return any value from {@link MbmsErrors.InitializationErrors}
 * or {@link MbmsErrors#SUCCESS}. Non-successful error codes will be passed to the app via
 * {@link IMbmsStreamingSessionCallback#onError(int, String)}.
 *
 * @param callback The callback to use to communicate with the app.
 * @param subscriptionId The subscription ID to use.
 * @apiSince REL
 */

public int initialize(android.telephony.mbms.MbmsStreamingSessionCallback callback, int subscriptionId) throws android.os.RemoteException { throw new RuntimeException("Stub!"); }

/**
 * Registers serviceClasses of interest with the appName/subId key.
 * Starts async fetching data on streaming services of matching classes to be reported
 * later via {@link IMbmsStreamingSessionCallback#onStreamingServicesUpdated(List)}
 *
 * Note that subsequent calls with the same uid and subId will replace
 * the service class list.
 *
 * May throw an {@link IllegalArgumentException} or an {@link IllegalStateException}
 *
 * @param subscriptionId The subscription id to use.
 * @param serviceClasses The service classes that the app wishes to get info on. The strings
 *                       may contain arbitrary data as negotiated between the app and the
 *                       carrier.
 * @return {@link MbmsErrors#SUCCESS} or any of the errors in
 * {@link MbmsErrors.GeneralErrors}
 * @apiSince REL
 */

public int requestUpdateStreamingServices(int subscriptionId, java.util.List<java.lang.String> serviceClasses) throws android.os.RemoteException { throw new RuntimeException("Stub!"); }

/**
 * Starts streaming on a particular service. This method may perform asynchronous work. When
 * the middleware is ready to send bits to the frontend, it should inform the app via
 * {@link IStreamingServiceCallback#onStreamStateUpdated(int, int)}.
 *
 * May throw an {@link IllegalArgumentException} or an {@link IllegalStateException}
 *
 * @param subscriptionId The subscription id to use.
 * @param serviceId The ID of the streaming service that the app has requested.
 * @param callback The callback object on which the app wishes to receive updates.
 * @return Any error in {@link MbmsErrors.GeneralErrors}
 * @apiSince REL
 */

public int startStreaming(int subscriptionId, java.lang.String serviceId, android.telephony.mbms.StreamingServiceCallback callback) throws android.os.RemoteException { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the streaming URI for a particular service. If the middleware is not yet ready to
 * stream the service, this method may return null.
 *
 * May throw an {@link IllegalArgumentException} or an {@link IllegalStateException}
 *
 * @param subscriptionId The subscription id to use.
 * @param serviceId The ID of the streaming service that the app has requested.
 * @return An opaque {@link Uri} to be passed to a video player that understands the format.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.net.Uri getPlaybackUri(int subscriptionId, java.lang.String serviceId) throws android.os.RemoteException { throw new RuntimeException("Stub!"); }

/**
 * Stop streaming the stream identified by {@code serviceId}. Notification of the resulting
 * stream state change should be reported to the app via
 * {@link IStreamingServiceCallback#onStreamStateUpdated(int, int)}.
 *
 * In addition, the callback provided via
 * {@link #startStreaming(int, String, IStreamingServiceCallback)} should no longer be
 * used after this method has called by the app.
 *
 * May throw an {@link IllegalArgumentException} or an {@link IllegalStateException}
 *
 * @param subscriptionId The subscription id to use.
 * @param serviceId The ID of the streaming service that the app wishes to stop.
 * @apiSince REL
 */

public void stopStreaming(int subscriptionId, java.lang.String serviceId) throws android.os.RemoteException { throw new RuntimeException("Stub!"); }

/**
 * Signals that the app wishes to dispose of the session identified by the
 * {@code subscriptionId} argument and the caller's uid. No notification back to the
 * app is required for this operation, and the corresponding callback provided via
 * {@link #initialize(IMbmsStreamingSessionCallback, int)} should no longer be used
 * after this method has been called by the app.
 *
 * May throw an {@link IllegalStateException}
 *
 * @param subscriptionId The subscription id to use.
 * @apiSince REL
 */

public void dispose(int subscriptionId) throws android.os.RemoteException { throw new RuntimeException("Stub!"); }

/**
 * Indicates that the app identified by the given UID and subscription ID has died.
 * @param uid the UID of the app, as returned by {@link Binder#getCallingUid()}.
 * @param subscriptionId The subscription ID the app is using.
 * @apiSince REL
 */

public void onAppCallbackDied(int uid, int subscriptionId) { throw new RuntimeException("Stub!"); }

/** @hide */

public android.os.IBinder asBinder() { throw new RuntimeException("Stub!"); }

/** @hide */

public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException { throw new RuntimeException("Stub!"); }
}

