/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.hdmi;

import android.content.pm.PackageManager;
import android.content.Context;

/**
 * The {@link HdmiControlManager} class is used to send HDMI control messages
 * to attached CEC devices.
 *
 * <p>Provides various HDMI client instances that represent HDMI-CEC logical devices
 * hosted in the system. {@link #getTvClient()}, for instance will return an
 * {@link HdmiTvClient} object if the system is configured to host one. Android system
 * can host more than one logical CEC devices. If multiple types are configured they
 * all work as if they were independent logical devices running in the system.
 *
 * <br>
 * Requires the PackageManager#FEATURE_HDMI_CEC feature which can be detected using {@link android.content.pm.PackageManager#hasSystemFeature(String) PackageManager.hasSystemFeature(String)}.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class HdmiControlManager {

HdmiControlManager() { throw new RuntimeException("Stub!"); }

/**
 * Gets an object that represents an HDMI-CEC logical device of a specified type.
 *
 * @param type CEC device type
 * @return {@link HdmiClient} instance. {@code null} on failure.
 * See {@link HdmiDeviceInfo#DEVICE_PLAYBACK}
 * See {@link HdmiDeviceInfo#DEVICE_TV}
 * See {@link HdmiDeviceInfo#DEVICE_AUDIO_SYSTEM}
 * @apiSince REL
 */

@android.annotation.Nullable
public android.hardware.hdmi.HdmiClient getClient(int type) { throw new RuntimeException("Stub!"); }

/**
 * Gets an object that represents an HDMI-CEC logical device of type playback on the system.
 *
 * <p>Used to send HDMI control messages to other devices like TV or audio amplifier through
 * HDMI bus. It is also possible to communicate with other logical devices hosted in the same
 * system if the system is configured to host more than one type of HDMI-CEC logical devices.
 *
 * @return {@link HdmiPlaybackClient} instance. {@code null} on failure.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.hardware.hdmi.HdmiPlaybackClient getPlaybackClient() { throw new RuntimeException("Stub!"); }

/**
 * Gets an object that represents an HDMI-CEC logical device of type TV on the system.
 *
 * <p>Used to send HDMI control messages to other devices and manage them through
 * HDMI bus. It is also possible to communicate with other logical devices hosted in the same
 * system if the system is configured to host more than one type of HDMI-CEC logical devices.
 *
 * @return {@link HdmiTvClient} instance. {@code null} on failure.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.hardware.hdmi.HdmiTvClient getTvClient() { throw new RuntimeException("Stub!"); }

/**
 * Gets an object that represents an HDMI-CEC logical device of type switch on the system.
 *
 * <p>Used to send HDMI control messages to other devices (e.g. TVs) through HDMI bus.
 * It is also possible to communicate with other logical devices hosted in the same
 * system if the system is configured to host more than one type of HDMI-CEC logical device.
 *
 * @return {@link HdmiSwitchClient} instance. {@code null} on failure.
 * @hide
 */

@android.annotation.Nullable
public android.hardware.hdmi.HdmiSwitchClient getSwitchClient() { throw new RuntimeException("Stub!"); }

/**
 * Get a snapshot of the real-time status of the devices on the CEC bus.
 *
 * <p>This only applies to devices with switch functionality, which are devices with one
 * or more than one HDMI inputs.
 *
 * @return a list of {@link HdmiDeviceInfo} of the connected CEC devices on the CEC bus. An
 * empty list will be returned if there is none.
 *
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public java.util.List<android.hardware.hdmi.HdmiDeviceInfo> getConnectedDevices() { throw new RuntimeException("Stub!"); }

/**
 * Power off the target device by sending CEC commands. Note that this device can't be the
 * current device itself.
 *
 * <p>The target device info can be obtained by calling {@link #getConnectedDevicesList()}.
 *
 * @param deviceInfo {@link HdmiDeviceInfo} of the device to be powered off.
 *
 * This value must never be {@code null}.
 * @hide
 */

public void powerOffDevice(@android.annotation.NonNull android.hardware.hdmi.HdmiDeviceInfo deviceInfo) { throw new RuntimeException("Stub!"); }

/**
 * Request the target device to be the new Active Source by sending CEC commands. Note that
 * this device can't be the current device itself.
 *
 * <p>The target device info can be obtained by calling {@link #getConnectedDevicesList()}.
 *
 * <p>If the target device responds to the command, the users should see the target device
 * streaming on their TVs.
 *
 * @param deviceInfo HdmiDeviceInfo of the target device
 *
 * This value must never be {@code null}.
 * @hide
 */

public void setActiveSource(@android.annotation.NonNull android.hardware.hdmi.HdmiDeviceInfo deviceInfo) { throw new RuntimeException("Stub!"); }

/**
 * Controls standby mode of the system. It will also try to turn on/off the connected devices if
 * necessary.
 *
 * <br>
 * Requires {@link android.Manifest.permission#HDMI_CEC}
 * @param isStandbyModeOn target status of the system's standby mode
 * @apiSince REL
 */

public void setStandbyMode(boolean isStandbyModeOn) { throw new RuntimeException("Stub!"); }

/**
 * Get the physical address of the device.
 *
 * <p>Physical address needs to be automatically adjusted when devices are phyiscally or
 * electrically added or removed from the device tree. Please see HDMI Specification Version
 * 1.4b 8.7 Physical Address for more details on the address discovery proccess.
 *
 * @hide
 */

public int getPhysicalAddress() { throw new RuntimeException("Stub!"); }

/**
 * Check if the target device is connected to the current device.
 *
 * <p>The API also returns true if the current device is the target.
 *
 * @param targetDevice {@link HdmiDeviceInfo} of the target device.
 * This value must never be {@code null}.
 * @return true if {@code targetDevice} is directly or indirectly
 * connected to the current device.
 *
 * @hide
 */

public boolean isDeviceConnected(@android.annotation.NonNull android.hardware.hdmi.HdmiDeviceInfo targetDevice) { throw new RuntimeException("Stub!"); }

/**
 * Adds a listener to get informed of {@link HdmiHotplugEvent}.
 *
 * <p>To stop getting the notification,
 * use {@link #removeHotplugEventListener(HotplugEventListener)}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#HDMI_CEC}
 * @param listener {@link HotplugEventListener} instance
 * @see HdmiControlManager#removeHotplugEventListener(HotplugEventListener)
 * @apiSince REL
 */

public void addHotplugEventListener(android.hardware.hdmi.HdmiControlManager.HotplugEventListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Removes a listener to stop getting informed of {@link HdmiHotplugEvent}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#HDMI_CEC}
 * @param listener {@link HotplugEventListener} instance to be removed
 * @apiSince REL
 */

public void removeHotplugEventListener(android.hardware.hdmi.HdmiControlManager.HotplugEventListener listener) { throw new RuntimeException("Stub!"); }

/**
 * Broadcast Action: Display OSD message.
 * <p>Send when the service has a message to display on screen for events
 * that need user's attention such as ARC status change.
 * <p>Always contains the extra fields {@link #EXTRA_MESSAGE_ID}.
 * <p>Requires {@link android.Manifest.permission#HDMI_CEC} to receive.
 * @apiSince REL
 */

public static final java.lang.String ACTION_OSD_MESSAGE = "android.hardware.hdmi.action.OSD_MESSAGE";

/**
 * Volume value for mute state.
 * @apiSince REL
 */

public static final int AVR_VOLUME_MUTED = 101; // 0x65

/**
 * Clear timer error - CEC is disabled.
 * @apiSince REL
 */

public static final int CLEAR_TIMER_STATUS_CEC_DISABLE = 162; // 0xa2

/**
 * Clear timer error - check recorder and connection.
 * @apiSince REL
 */

public static final int CLEAR_TIMER_STATUS_CHECK_RECORDER_CONNECTION = 160; // 0xa0

/**
 * Clear timer error - cannot clear timer for selected source.
 * @apiSince REL
 */

public static final int CLEAR_TIMER_STATUS_FAIL_TO_CLEAR_SELECTED_SOURCE = 161; // 0xa1

/**
 * Timer cleared.
 * @apiSince REL
 */

public static final int CLEAR_TIMER_STATUS_TIMER_CLEARED = 128; // 0x80

/**
 * Timer not cleared – no info available.
 * @apiSince REL
 */

public static final int CLEAR_TIMER_STATUS_TIMER_NOT_CLEARED_NO_INFO_AVAILABLE = 2; // 0x2

/**
 * Timer not cleared – no matching.
 * @apiSince REL
 */

public static final int CLEAR_TIMER_STATUS_TIMER_NOT_CLEARED_NO_MATCHING = 1; // 0x1

/**
 * Timer not cleared – recording.
 * @apiSince REL
 */

public static final int CLEAR_TIMER_STATUS_TIMER_NOT_CLEARED_RECORDING = 0; // 0x0

/**
 * The state of HdmiControlService is changed by changing of settings.
 * @apiSince REL
 */

public static final int CONTROL_STATE_CHANGED_REASON_SETTING = 1; // 0x1

/**
 * The HdmiControlService will be disabled to standby.
 * @apiSince REL
 */

public static final int CONTROL_STATE_CHANGED_REASON_STANDBY = 3; // 0x3

/**
 * The HdmiControlService is started.
 * @apiSince REL
 */

public static final int CONTROL_STATE_CHANGED_REASON_START = 0; // 0x0

/**
 * The HdmiControlService is enabled to wake up.
 * @apiSince REL
 */

public static final int CONTROL_STATE_CHANGED_REASON_WAKEUP = 2; // 0x2

/** @apiSince REL */

public static final int DEVICE_EVENT_ADD_DEVICE = 1; // 0x1

/** @apiSince REL */

public static final int DEVICE_EVENT_REMOVE_DEVICE = 2; // 0x2

/** @apiSince REL */

public static final int DEVICE_EVENT_UPDATE_DEVICE = 3; // 0x3

/**
 * Used as an extra field in the intent {@link #ACTION_OSD_MESSAGE}. Contains the extra value
 * of the message.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_MESSAGE_EXTRA_PARAM1 = "android.hardware.hdmi.extra.MESSAGE_EXTRA_PARAM1";

/**
 * Used as an extra field in the intent {@link #ACTION_OSD_MESSAGE}. Contains the ID of
 * the message to display on screen.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_MESSAGE_ID = "android.hardware.hdmi.extra.MESSAGE_ID";

/**
 * No recording – already recording
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_ALREADY_RECORDING = 18; // 0x12

/**
 * CEC is disabled.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_CEC_DISABLED = 51; // 0x33

/**
 * No recording. Please check recorder and connection.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_CHECK_RECORDER_CONNECTION = 49; // 0x31

/**
 * No recording – Not allowed to copy source. Source is “copy never”.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_DISALLOW_TO_COPY = 13; // 0xd

/**
 * No recording – No further copies allowed
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_DISALLOW_TO_FUTHER_COPIES = 14; // 0xe

/**
 * Cannot record currently displayed source.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_FAIL_TO_RECORD_DISPLAYED_SCREEN = 50; // 0x32

/**
 * No recording – invalid External Physical Address
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_INVALID_EXTERNAL_PHYSICAL_ADDRESS = 10; // 0xa

/**
 * No recording – invalid External plug number
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_INVALID_EXTERNAL_PLUG_NUMBER = 9; // 0x9

/**
 * No recording – media problem
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_MEDIA_PROBLEM = 21; // 0x15

/**
 * No recording – media protected
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_MEDIA_PROTECTED = 19; // 0x13

/**
 * No recording – not enough space available
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_NOT_ENOUGH_SPACE = 22; // 0x16

/**
 * No recording – No media
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_NO_MEDIA = 16; // 0x10

/**
 * No Recording – No or Insufficient CA Entitlements”
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_NO_OR_INSUFFICIENT_CA_ENTITLEMENTS = 12; // 0xc

/**
 * No recording – no source signal
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_NO_SOURCE_SIGNAL = 20; // 0x14

/**
 * No recording – other reason
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_OTHER_REASON = 31; // 0x1f

/**
 * No recording – Parental Lock On
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_PARENT_LOCK_ON = 23; // 0x17

/**
 * No recording – playing
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_PLAYING = 17; // 0x11

/**
 * No recording. Previous recording request in progress.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_PREVIOUS_RECORDING_IN_PROGRESS = 48; // 0x30

/**
 * Recording has already terminated
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_RECORDING_ALREADY_TERMINATED = 27; // 0x1b

/**
 * Recording Analogue Service. Indicates the status of a recording.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_RECORDING_ANALOGUE_SERVICE = 3; // 0x3

/**
 * Recording currently selected source. Indicates the status of a recording.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_RECORDING_CURRENTLY_SELECTED_SOURCE = 1; // 0x1

/**
 * Recording Digital Service. Indicates the status of a recording.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_RECORDING_DIGITAL_SERVICE = 2; // 0x2

/**
 * Recording External input. Indicates the status of a recording.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_RECORDING_EXTERNAL_INPUT = 4; // 0x4

/**
 * Recording terminated normally
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_RECORDING_TERMINATED_NORMALLY = 26; // 0x1a

/**
 * No recording – unable to record Analogue Service. No suitable tuner.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_UNABLE_ANALOGUE_SERVICE = 6; // 0x6

/**
 * No recording – unable to record Digital Service. No suitable tuner.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_UNABLE_DIGITAL_SERVICE = 5; // 0x5

/**
 * No recording – unable to select required service. as suitable tuner, but the requested
 * parameters are invalid or out of range for that tuner.
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_UNABLE_SELECTED_SERVICE = 7; // 0x7

/**
 * No recording – CA system not supported
 * @apiSince REL
 */

public static final int ONE_TOUCH_RECORD_UNSUPPORTED_CA = 11; // 0xb

/**
 * Message that ARC enabled device is connected to invalid port (non-ARC port).
 * @apiSince REL
 */

public static final int OSD_MESSAGE_ARC_CONNECTED_INVALID_PORT = 1; // 0x1

/**
 * Message used by TV to receive volume status from Audio Receiver. It should check volume value
 * that is retrieved from extra value with the key {@link #EXTRA_MESSAGE_EXTRA_PARAM1}. If the
 * value is in range of [0,100], it is current volume of Audio Receiver. And there is another
 * value, {@link #AVR_VOLUME_MUTED}, which is used to inform volume mute.
 * @apiSince REL
 */

public static final int OSD_MESSAGE_AVR_VOLUME_CHANGED = 2; // 0x2

/** @apiSince REL */

public static final int POWER_STATUS_ON = 0; // 0x0

/** @apiSince REL */

public static final int POWER_STATUS_STANDBY = 1; // 0x1

/** @apiSince REL */

public static final int POWER_STATUS_TRANSIENT_TO_ON = 2; // 0x2

/** @apiSince REL */

public static final int POWER_STATUS_TRANSIENT_TO_STANDBY = 3; // 0x3

/** @apiSince REL */

public static final int POWER_STATUS_UNKNOWN = -1; // 0xffffffff

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int RESULT_ALREADY_IN_PROGRESS = 4; // 0x4

/** @apiSince REL */

public static final int RESULT_COMMUNICATION_FAILED = 7; // 0x7

/** @apiSince REL */

public static final int RESULT_EXCEPTION = 5; // 0x5

/** @apiSince REL */

public static final int RESULT_INCORRECT_MODE = 6; // 0x6

/**
 * Source device that the application is using is not available.
 * @apiSince REL
 */

public static final int RESULT_SOURCE_NOT_AVAILABLE = 2; // 0x2

/**
 * Control operation is successfully handled by the framework.
 * @apiSince REL
 */

public static final int RESULT_SUCCESS = 0; // 0x0

/**
 * Target device that the application is controlling is not available.
 * @apiSince REL
 */

public static final int RESULT_TARGET_NOT_AVAILABLE = 3; // 0x3

/** @apiSince REL */

public static final int RESULT_TIMEOUT = 1; // 0x1

/**
 * CEC is disabled.
 * @apiSince REL
 */

public static final int TIMER_RECORDING_RESULT_EXTRA_CEC_DISABLED = 3; // 0x3

/**
 * No timer recording - check recorder and connection.
 * @apiSince REL
 */

public static final int TIMER_RECORDING_RESULT_EXTRA_CHECK_RECORDER_CONNECTION = 1; // 0x1

/**
 * No timer recording - cannot record selected source.
 * @apiSince REL
 */

public static final int TIMER_RECORDING_RESULT_EXTRA_FAIL_TO_RECORD_SELECTED_SOURCE = 2; // 0x2

/**
 * No extra error.
 * @apiSince REL
 */

public static final int TIMER_RECORDING_RESULT_EXTRA_NO_ERROR = 0; // 0x0

/**
 * Timer recording type for analogue service source.
 * @apiSince REL
 */

public static final int TIMER_RECORDING_TYPE_ANALOGUE = 2; // 0x2

/**
 * Timer recording type for digital service source.
 * @apiSince REL
 */

public static final int TIMER_RECORDING_TYPE_DIGITAL = 1; // 0x1

/**
 * Timer recording type for external source.
 * @apiSince REL
 */

public static final int TIMER_RECORDING_TYPE_EXTERNAL = 3; // 0x3

/**
 * [Timer Status Data/Media Info] - Media not present.
 * @apiSince REL
 */

public static final int TIMER_STATUS_MEDIA_INFO_NOT_PRESENT = 2; // 0x2

/**
 * [Timer Status Data/Media Info] - Media present and not protected.
 * @apiSince REL
 */

public static final int TIMER_STATUS_MEDIA_INFO_PRESENT_NOT_PROTECTED = 0; // 0x0

/**
 * [Timer Status Data/Media Info] - Media present, but protected.
 * @apiSince REL
 */

public static final int TIMER_STATUS_MEDIA_INFO_PRESENT_PROTECTED = 1; // 0x1

/**
 * [Timer Status Data/Not Programmed Error Info] - CA system not supported.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_CA_NOT_SUPPORTED = 6; // 0x6

/**
 * [Timer Status Data/Not Programmed Error Info] - Clock Failure.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_CLOCK_FAILURE = 10; // 0xa

/**
 * [Timer Status Data/Not Programmed Error Info] - Date out of range.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_DATE_OUT_OF_RANGE = 2; // 0x2

/**
 * [Timer Status Data/Not Programmed Error Info] - Duplicate: already programmed.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_DUPLICATED = 14; // 0xe

/**
 * [Timer Status Data/Not Programmed Error Info] - Invalid External Physical Address.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_INVALID_EXTERNAL_PHYSICAL_NUMBER = 5; // 0x5

/**
 * [Timer Status Data/Not Programmed Error Info] - Invalid External Plug Number.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_INVALID_EXTERNAL_PLUG_NUMBER = 4; // 0x4

/**
 * [Timer Status Data/Not Programmed Error Info] - Recording Sequence error.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_INVALID_SEQUENCE = 3; // 0x3

/**
 * [Timer Status Data/Not Programmed Error Info] - No or insufficient CA Entitlements.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_NO_CA_ENTITLEMENTS = 7; // 0x7

/**
 * [Timer Status Data/Not Programmed Error Info] - No free timer available.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_NO_FREE_TIME = 1; // 0x1

/**
 * [Timer Status Data/Not Programmed Error Info] - Parental Lock On.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_PARENTAL_LOCK_ON = 9; // 0x9

/**
 * [Timer Status Data/Not Programmed Error Info] - Does not support resolution.
 * @apiSince REL
 */

public static final int TIMER_STATUS_NOT_PROGRAMMED_UNSUPPORTED_RESOLUTION = 8; // 0x8

/**
 * [Timer Status Data/Programmed Info] - Enough space available for recording.
 * @apiSince REL
 */

public static final int TIMER_STATUS_PROGRAMMED_INFO_ENOUGH_SPACE = 8; // 0x8

/**
 * [Timer Status Data/Programmed Info] - Might not enough space available for recording.
 * @apiSince REL
 */

public static final int TIMER_STATUS_PROGRAMMED_INFO_MIGHT_NOT_ENOUGH_SPACE = 11; // 0xb

/**
 * [Timer Status Data/Programmed Info] - Not enough space available for recording.
 * @apiSince REL
 */

public static final int TIMER_STATUS_PROGRAMMED_INFO_NOT_ENOUGH_SPACE = 9; // 0x9

/**
 * [Timer Status Data/Programmed Info] - No media info available.
 * @apiSince REL
 */

public static final int TIMER_STATUS_PROGRAMMED_INFO_NO_MEDIA_INFO = 10; // 0xa
/**
 * Value is {@link android.hardware.hdmi.HdmiControlManager#RESULT_SUCCESS}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_TIMEOUT}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_SOURCE_NOT_AVAILABLE}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_TARGET_NOT_AVAILABLE}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_ALREADY_IN_PROGRESS}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_EXCEPTION}, {@link android.hardware.hdmi.HdmiControlManager#RESULT_INCORRECT_MODE}, or {@link android.hardware.hdmi.HdmiControlManager#RESULT_COMMUNICATION_FAILED}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.CLASS)
public static @interface ControlCallbackResult {
}

/**
 * Listener used to get hotplug event from HDMI port.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface HotplugEventListener {

/** @apiSince REL */

public void onReceived(android.hardware.hdmi.HdmiHotplugEvent event);
}

/**
 * Listener used to get vendor-specific commands.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface VendorCommandListener {

/**
 * Called when a vendor command is received.
 *
 * @param srcAddress source logical address
 * @param destAddress destination logical address
 * @param params vendor-specific parameters
 * @param hasVendorId {@code true} if the command is &lt;Vendor Command
 *        With ID&gt;. The first 3 bytes of params is vendor id.
 * @apiSince REL
 */

public void onReceived(int srcAddress, int destAddress, byte[] params, boolean hasVendorId);

/**
 * The callback is called:
 * <ul>
 *     <li> before HdmiControlService is disabled.
 *     <li> after HdmiControlService is enabled and the local address is assigned.
 * </ul>
 * The client shouldn't hold the thread too long since this is a blocking call.
 *
 * @param enabled {@code true} if HdmiControlService is enabled.
 * @param reason the reason code why the state of HdmiControlService is changed.
 * @see #CONTROL_STATE_CHANGED_REASON_START
 * @see #CONTROL_STATE_CHANGED_REASON_SETTING
 * @see #CONTROL_STATE_CHANGED_REASON_WAKEUP
 * @see #CONTROL_STATE_CHANGED_REASON_STANDBY
 * @apiSince REL
 */

public void onControlStateChanged(boolean enabled, int reason);
}

}

