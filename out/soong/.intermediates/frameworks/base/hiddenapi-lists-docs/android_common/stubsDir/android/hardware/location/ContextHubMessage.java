/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.location;


/**
 * @deprecated Use {@link android.hardware.location.NanoAppMessage} instead to send messages with
 *             {@link android.hardware.location.ContextHubClient#sendMessageToNanoApp(
 *             NanoAppMessage)} and receive messages with
 *             {@link android.hardware.location.ContextHubClientCallback#onMessageFromNanoApp(
 *             ContextHubClient, NanoAppMessage)}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class ContextHubMessage implements android.os.Parcelable {

/**
 * Constructor for a context hub message
 *
 * @param msgType - message type
 * @param version - version
 * @param data    - message buffer
 * @apiSince REL
 */

@Deprecated
public ContextHubMessage(int msgType, int version, byte[] data) { throw new RuntimeException("Stub!"); }

/**
 * Get the message type
 *
 * @return int - message type
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getMsgType() { throw new RuntimeException("Stub!"); }

/**
 * get message version
 *
 * @return int - message version
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getVersion() { throw new RuntimeException("Stub!"); }

/**
 * get message data
 *
 * @return byte[] - message data buffer
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public byte[] getData() { throw new RuntimeException("Stub!"); }

/**
 * set message type
 *
 * @param msgType - message type
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setMsgType(int msgType) { throw new RuntimeException("Stub!"); }

/**
 * Set message version
 *
 * @param version - message version
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setVersion(int version) { throw new RuntimeException("Stub!"); }

/**
 * set message data
 *
 * @param data - message buffer
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setMsgData(byte[] data) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated @android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.location.ContextHubMessage> CREATOR;
static { CREATOR = null; }
}

