/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.om;


/**
 * Immutable overlay information about a package. All PackageInfos that
 * represent an overlay package will have a corresponding OverlayInfo.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class OverlayInfo implements android.os.Parcelable {

/** @hide */

OverlayInfo(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/**
 * Returns package name of the current overlay.
 * @hide

 * @return This value will never be {@code null}.
 */

@android.annotation.NonNull
public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the target package name of the current overlay.
 * @hide

 * @return This value will never be {@code null}.
 */

@android.annotation.NonNull
public java.lang.String getTargetPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Returns the category of the current overlay.
 * @hide\

 * @return This value may be {@code null}.
 */

@android.annotation.Nullable
public java.lang.String getCategory() { throw new RuntimeException("Stub!"); }

/**
 * Returns user handle for which this overlay applies to.
 * @hide
 */

public int getUserId() { throw new RuntimeException("Stub!"); }

/**
 * Returns name of the target overlayable declaration.
 * @hide

 * @return This value may be {@code null}.
 */

@android.annotation.Nullable
public java.lang.String getTargetOverlayableName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return true if this overlay is enabled, i.e. should be used to overlay
 * the resources in the target package.
 *
 * Disabled overlay packages are installed but are currently not in use.
 *
 * @return true if the overlay is enabled, else false.
 * @hide
 */

public boolean isEnabled() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.content.om.OverlayInfo> CREATOR;
static { CREATOR = null; }
}

