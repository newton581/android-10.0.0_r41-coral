/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.printservice.recommendation;

import android.printservice.PrintService;
import java.net.InetAddress;
import java.util.List;
import android.os.Parcel;

/**
 * A recommendation to install a {@link PrintService print service}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class RecommendationInfo implements android.os.Parcelable {

/**
 * Create a new recommendation.
 *
 * @param packageName                  Package name of the print service
 * This value must never be {@code null}.
 * @param name                         Display name of the print service
 * This value must never be {@code null}.
 * @param discoveredPrinters           The {@link InetAddress addresses} of the discovered
 *                                     printers. Cannot be null or empty.
 * This value must never be {@code null}.
 * @param recommendsMultiVendorService If the service detects printer from multiple vendor
 * @apiSince REL
 */

public RecommendationInfo(@android.annotation.NonNull java.lang.CharSequence packageName, @android.annotation.NonNull java.lang.CharSequence name, @android.annotation.NonNull java.util.List<java.net.InetAddress> discoveredPrinters, boolean recommendsMultiVendorService) { throw new RuntimeException("Stub!"); }

/**
 * Create a new recommendation.
 *
 * @param packageName                  Package name of the print service
 * This value must never be {@code null}.
 * @param name                         Display name of the print service
 * This value must never be {@code null}.
 * @param numDiscoveredPrinters        Number of printers the print service would discover if
 *                                     installed
 * Value is 0 or greater
 * @param recommendsMultiVendorService If the service detects printer from multiple vendor
 *
 * @deprecated Use {@link RecommendationInfo(String, String, List<InetAddress>, boolean)}
 *             instead
 * @apiSince REL
 */

@Deprecated
public RecommendationInfo(@android.annotation.NonNull java.lang.CharSequence packageName, @android.annotation.NonNull java.lang.CharSequence name, int numDiscoveredPrinters, boolean recommendsMultiVendorService) { throw new RuntimeException("Stub!"); }

/**
 * @return The package name the recommendations recommends.
 * @apiSince REL
 */

public java.lang.CharSequence getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * @return Whether the recommended print service detects printers of more than one vendor.
 * @apiSince REL
 */

public boolean recommendsMultiVendorService() { throw new RuntimeException("Stub!"); }

/**
 * @return The {@link InetAddress address} of the printers the print service would detect.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<java.net.InetAddress> getDiscoveredPrinters() { throw new RuntimeException("Stub!"); }

/**
 * @return The number of printer the print service would detect.
 * @apiSince REL
 */

public int getNumDiscoveredPrinters() { throw new RuntimeException("Stub!"); }

/**
 * @return The name of the recommended print service.
 * @apiSince REL
 */

public java.lang.CharSequence getName() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Utility class used to create new print service recommendation objects from parcels.
 *
 * @see #RecommendationInfo(Parcel)
 * @apiSince REL
 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.printservice.recommendation.RecommendationInfo> CREATOR;
static { CREATOR = null; }
}

