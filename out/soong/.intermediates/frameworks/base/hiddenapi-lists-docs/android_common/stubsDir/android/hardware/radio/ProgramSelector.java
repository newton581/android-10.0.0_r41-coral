/**
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.radio;


/**
 * A set of identifiers necessary to tune to a given station.
 *
 * This can hold various identifiers, like
 * - AM/FM frequency
 * - HD Radio subchannel
 * - DAB channel info
 *
 * The primary ID uniquely identifies a station and can be used for equality
 * check. The secondary IDs are supplementary and can speed up tuning process,
 * but the primary ID is sufficient (ie. after a full band scan).
 *
 * Two selectors with different secondary IDs, but the same primary ID are
 * considered equal. In particular, secondary IDs vector may get updated for
 * an entry on the program list (ie. when a better frequency for a given
 * station is found).
 *
 * The primaryId of a given programType MUST be of a specific type:
 * - AM, FM: RDS_PI if the station broadcasts RDS, AMFM_FREQUENCY otherwise;
 * - AM_HD, FM_HD: HD_STATION_ID_EXT;
 * - DAB: DAB_SIDECC;
 * - DRMO: DRMO_SERVICE_ID;
 * - SXM: SXM_SERVICE_ID;
 * - VENDOR: VENDOR_PRIMARY.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ProgramSelector implements android.os.Parcelable {

/**
 * Constructor for ProgramSelector.
 *
 * It's not desired to modify selector objects, so all its fields are initialized at creation.
 *
 * Identifier lists must not contain any nulls, but can itself be null to be interpreted
 * as empty list at object creation.
 *
 * @param programType type of a radio technology.
 * Value is {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_INVALID}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_AM}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_FM}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_AM_HD}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_FM_HD}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_DAB}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_DRMO}, or {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_SXM}
 * Value is between PROGRAM_TYPE_VENDOR_START and PROGRAM_TYPE_VENDOR_END inclusive
 * @param primaryId primary program identifier.
 * This value must never be {@code null}.
 * @param secondaryIds list of secondary program identifiers.
 * This value may be {@code null}.
 * @param vendorIds list of vendor-specific program identifiers.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public ProgramSelector(int programType, @android.annotation.NonNull android.hardware.radio.ProgramSelector.Identifier primaryId, @android.annotation.Nullable android.hardware.radio.ProgramSelector.Identifier[] secondaryIds, @android.annotation.Nullable long[] vendorIds) { throw new RuntimeException("Stub!"); }

/**
 * Type of a radio technology.
 *
 * @return program type.
 * Value is {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_INVALID}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_AM}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_FM}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_AM_HD}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_FM_HD}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_DAB}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_DRMO}, or {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_SXM}
 * Value is between PROGRAM_TYPE_VENDOR_START and PROGRAM_TYPE_VENDOR_END inclusive
 * @deprecated use {@link getPrimaryId} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getProgramType() { throw new RuntimeException("Stub!"); }

/**
 * Primary program identifier uniquely identifies a station and is used to
 * determine equality between two ProgramSelectors.
 *
 * @return primary identifier.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.radio.ProgramSelector.Identifier getPrimaryId() { throw new RuntimeException("Stub!"); }

/**
 * Secondary program identifier is not required for tuning, but may make it
 * faster or more reliable.
 *
 * @return secondary identifier list, must not be modified.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.radio.ProgramSelector.Identifier[] getSecondaryIds() { throw new RuntimeException("Stub!"); }

/**
 * Looks up an identifier of a given type (either primary or secondary).
 *
 * If there are multiple identifiers if a given type, then first in order (where primary id is
 * before any secondary) is selected.
 *
 * @param type type of identifier.
 * Value is {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_INVALID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_AMFM_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_RDS_PI}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_ID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_SUBCHANNEL}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_NAME}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SIDECC}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_ENSEMBLE}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SCID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_SERVICE_ID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_MODULATION}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_SERVICE_ID}, or {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_CHANNEL}
 * Value is between IDENTIFIER_TYPE_VENDOR_START and IDENTIFIER_TYPE_VENDOR_END inclusive
 * @return identifier value, if found.
 * @throws IllegalArgumentException, if not found.
 * @apiSince REL
 */

public long getFirstId(int type) { throw new RuntimeException("Stub!"); }

/**
 * Looks up all identifier of a given type (either primary or secondary).
 *
 * Some identifiers may be provided multiple times, for example
 * IDENTIFIER_TYPE_AMFM_FREQUENCY for FM Alternate Frequencies.
 *
 * @param type type of identifier.
 * Value is {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_INVALID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_AMFM_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_RDS_PI}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_ID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_SUBCHANNEL}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_NAME}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SIDECC}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_ENSEMBLE}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SCID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_SERVICE_ID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_MODULATION}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_SERVICE_ID}, or {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_CHANNEL}
 * Value is between IDENTIFIER_TYPE_VENDOR_START and IDENTIFIER_TYPE_VENDOR_END inclusive
 * @return a list of identifiers, generated on each call. May be modified.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.radio.ProgramSelector.Identifier[] getAllIds(int type) { throw new RuntimeException("Stub!"); }

/**
 * Vendor identifiers are passed as-is to the HAL implementation,
 * preserving elements order.
 *
 * @return an array of vendor identifiers, must not be modified.
 * This value will never be {@code null}.
 * @deprecated for HAL 1.x compatibility;
 *             HAL 2.x uses standard primary/secondary lists for vendor IDs
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
@android.annotation.NonNull
public long[] getVendorIds() { throw new RuntimeException("Stub!"); }

/**
 * Creates an equivalent ProgramSelector with a given secondary identifier preferred.
 *
 * Used to point to a specific physical identifier for technologies that may broadcast the same
 * program on different channels. For example, with a DAB program broadcasted over multiple
 * ensembles, the radio hardware may select the one with the strongest signal. The UI may select
 * preferred ensemble though, so the radio hardware may try to use it in the first place.
 *
 * This is a best-effort hint for the tuner, not a guaranteed behavior.
 *
 * Setting the given secondary identifier as preferred means filtering out other secondary
 * identifiers of its type and adding it to the list.
 *
 * @param preferred preferred secondary identifier
 * This value must never be {@code null}.
 * @return a new ProgramSelector with a given secondary identifier preferred
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.radio.ProgramSelector withSecondaryPreferred(@android.annotation.NonNull android.hardware.radio.ProgramSelector.Identifier preferred) { throw new RuntimeException("Stub!"); }

/**
 * Builds new ProgramSelector for AM/FM frequency.
 *
 * @param band the band.
 * Value is {@link android.hardware.radio.RadioManager#BAND_INVALID}, {@link android.hardware.radio.RadioManager#BAND_AM}, {@link android.hardware.radio.RadioManager#BAND_FM}, {@link android.hardware.radio.RadioManager#BAND_AM_HD}, or {@link android.hardware.radio.RadioManager#BAND_FM_HD}
 * @param frequencyKhz the frequency in kHz.
 * @return new ProgramSelector object representing given frequency.
 * This value will never be {@code null}.
 * @throws IllegalArgumentException if provided frequency is out of bounds.
 * @apiSince REL
 */

@android.annotation.NonNull
public static android.hardware.radio.ProgramSelector createAmFmSelector(int band, int frequencyKhz) { throw new RuntimeException("Stub!"); }

/**
 * Builds new ProgramSelector for AM/FM frequency.
 *
 * This method variant supports HD Radio subchannels, but it's undesirable to
 * select them manually. Instead, the value should be retrieved from program list.
 *
 * @param band the band.
 * Value is {@link android.hardware.radio.RadioManager#BAND_INVALID}, {@link android.hardware.radio.RadioManager#BAND_AM}, {@link android.hardware.radio.RadioManager#BAND_FM}, {@link android.hardware.radio.RadioManager#BAND_AM_HD}, or {@link android.hardware.radio.RadioManager#BAND_FM_HD}
 * @param frequencyKhz the frequency in kHz.
 * @param subChannel 1-based HD Radio subchannel.
 * @return new ProgramSelector object representing given frequency.
 * This value will never be {@code null}.
 * @throws IllegalArgumentException if provided frequency is out of bounds,
 *         or tried setting a subchannel for analog AM/FM.
 * @apiSince REL
 */

@android.annotation.NonNull
public static android.hardware.radio.ProgramSelector createAmFmSelector(int band, int frequencyKhz, int subChannel) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.ProgramSelector> CREATOR;
static { CREATOR = null; }

/**
 * kHz
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_AMFM_FREQUENCY = 1; // 0x1

/**
 * 16bit
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_DAB_ENSEMBLE = 6; // 0x6

/**
 * kHz
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_DAB_FREQUENCY = 8; // 0x8

/**
 * 12bit
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_DAB_SCID = 7; // 0x7

/**
 * @see {@link IDENTIFIER_TYPE_DAB_SID_EXT}
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_DAB_SIDECC = 5; // 0x5

/**
 * 28bit compound primary identifier for Digital Audio Broadcasting.
 *
 * Consists of (from the LSB):
 * - 16bit: SId;
 * - 8bit: ECC code;
 * - 4bit: SCIdS.
 *
 * SCIdS (Service Component Identifier within the Service) value
 * of 0 represents the main service, while 1 and above represents
 * secondary services.
 *
 * The remaining bits should be set to zeros when writing on the chip side
 * and ignored when read.
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_DAB_SID_EXT = 5; // 0x5

/**
 * kHz
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_DRMO_FREQUENCY = 10; // 0xa

/**
 * 1: AM, 2:FM
 * @deprecated use {@link IDENTIFIER_TYPE_DRMO_FREQUENCY} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int IDENTIFIER_TYPE_DRMO_MODULATION = 11; // 0xb

/**
 * 24bit
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_DRMO_SERVICE_ID = 9; // 0x9

/**
 * 64bit compound primary identifier for HD Radio.
 *
 * Consists of (from the LSB):
 * - 32bit: Station ID number;
 * - 4bit: HD_SUBCHANNEL;
 * - 18bit: AMFM_FREQUENCY.
 * The remaining bits should be set to zeros when writing on the chip side
 * and ignored when read.
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_HD_STATION_ID_EXT = 3; // 0x3

/**
 * 64bit additional identifier for HD Radio.
 *
 * Due to Station ID abuse, some HD_STATION_ID_EXT identifiers may be not
 * globally unique. To provide a best-effort solution, a short version of
 * station name may be carried as additional identifier and may be used
 * by the tuner hardware to double-check tuning.
 *
 * The name is limited to the first 8 A-Z0-9 characters (lowercase letters
 * must be converted to uppercase). Encoded in little-endian ASCII:
 * the first character of the name is the LSB.
 *
 * For example: "Abc" is encoded as 0x434241.
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_HD_STATION_NAME = 10004; // 0x2714

/**
 * HD Radio subchannel - a value of range 0-7.
 *
 * The subchannel index is 0-based (where 0 is MPS and 1..7 are SPS),
 * as opposed to HD Radio standard (where it's 1-based).
 *
 * @deprecated use IDENTIFIER_TYPE_HD_STATION_ID_EXT instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int IDENTIFIER_TYPE_HD_SUBCHANNEL = 4; // 0x4

/** @apiSince REL */

public static final int IDENTIFIER_TYPE_INVALID = 0; // 0x0

/**
 * 16bit
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_RDS_PI = 2; // 0x2

/**
 * 0-999 range
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_SXM_CHANNEL = 13; // 0xd

/**
 * 32bit
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_SXM_SERVICE_ID = 12; // 0xc

/**
 * @see {@link IDENTIFIER_TYPE_VENDOR_START}
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_VENDOR_END = 1999; // 0x7cf

/**
 * @deprecated use {@link IDENTIFIER_TYPE_VENDOR_END} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int IDENTIFIER_TYPE_VENDOR_PRIMARY_END = 1999; // 0x7cf

/**
 * @deprecated use {@link IDENTIFIER_TYPE_VENDOR_START} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int IDENTIFIER_TYPE_VENDOR_PRIMARY_START = 1000; // 0x3e8

/**
 * Primary identifier for vendor-specific radio technology.
 * The value format is determined by a vendor.
 *
 * It must not be used in any other programType than corresponding VENDOR
 * type between VENDOR_START and VENDOR_END (eg. identifier type 1015 must
 * not be used in any program type other than 1015).
 * @apiSince REL
 */

public static final int IDENTIFIER_TYPE_VENDOR_START = 1000; // 0x3e8

/** Analogue AM radio (with or without RDS).
 * @deprecated use {@link ProgramIdentifier} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PROGRAM_TYPE_AM = 1; // 0x1

/** AM HD Radio.
 * @deprecated use {@link ProgramIdentifier} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PROGRAM_TYPE_AM_HD = 3; // 0x3

/** Digital audio broadcasting.
 * @deprecated use {@link ProgramIdentifier} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PROGRAM_TYPE_DAB = 5; // 0x5

/** Digital Radio Mondiale.
 * @deprecated use {@link ProgramIdentifier} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PROGRAM_TYPE_DRMO = 6; // 0x6

/** analogue FM radio (with or without RDS).
 * @deprecated use {@link ProgramIdentifier} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PROGRAM_TYPE_FM = 2; // 0x2

/** FM HD Radio.
 * @deprecated use {@link ProgramIdentifier} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PROGRAM_TYPE_FM_HD = 4; // 0x4

/** Invalid program type.
 * @deprecated use {@link ProgramIdentifier} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PROGRAM_TYPE_INVALID = 0; // 0x0

/** SiriusXM Satellite Radio.
 * @deprecated use {@link ProgramIdentifier} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PROGRAM_TYPE_SXM = 7; // 0x7

/**
 * @deprecated use {@link ProgramIdentifier} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PROGRAM_TYPE_VENDOR_END = 1999; // 0x7cf

/** Vendor-specific, not synced across devices.
 * @deprecated use {@link ProgramIdentifier} instead
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated public static final int PROGRAM_TYPE_VENDOR_START = 1000; // 0x3e8
/**
 * A single program identifier component, eg. frequency or channel ID.
 *
 * The long value field holds the value in format described in comments for
 * IdentifierType constants.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Identifier implements android.os.Parcelable {

/**
 * @param type Value is {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_INVALID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_AMFM_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_RDS_PI}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_ID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_SUBCHANNEL}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_NAME}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SIDECC}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_ENSEMBLE}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SCID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_SERVICE_ID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_MODULATION}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_SERVICE_ID}, or {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_CHANNEL}
 
 * Value is between IDENTIFIER_TYPE_VENDOR_START and IDENTIFIER_TYPE_VENDOR_END inclusive
 * @apiSince REL
 */

public Identifier(int type, long value) { throw new RuntimeException("Stub!"); }

/**
 * Type of an identifier.
 *
 * @return type of an identifier.
 
 * Value is {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_INVALID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_AMFM_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_RDS_PI}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_ID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_SUBCHANNEL}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_NAME}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SIDECC}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_ENSEMBLE}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SCID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_SERVICE_ID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_MODULATION}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_SERVICE_ID}, or {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_CHANNEL}
 
 * Value is between IDENTIFIER_TYPE_VENDOR_START and IDENTIFIER_TYPE_VENDOR_END inclusive
 * @apiSince REL
 */

public int getType() { throw new RuntimeException("Stub!"); }

/**
 * Value of an identifier.
 *
 * Its meaning depends on identifier type, ie. for IDENTIFIER_TYPE_AMFM_FREQUENCY type,
 * the value is a frequency in kHz.
 *
 * The range of a value depends on its type; it does not always require the whole long
 * range. Casting to necessary type (ie. int) without range checking is correct in front-end
 * code - any range violations are either errors in the framework or in the
 * HAL implementation. For example, IDENTIFIER_TYPE_AMFM_FREQUENCY always fits in int,
 * as Integer.MAX_VALUE would mean 2.1THz.
 *
 * @return value of an identifier.
 * @apiSince REL
 */

public long getValue() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.ProgramSelector.Identifier> CREATOR;
static { CREATOR = null; }
}

/**
 * Value is {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_INVALID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_AMFM_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_RDS_PI}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_ID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_SUBCHANNEL}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_HD_STATION_NAME}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SID_EXT}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SIDECC}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_ENSEMBLE}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_SCID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DAB_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_SERVICE_ID}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_FREQUENCY}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_DRMO_MODULATION}, {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_SERVICE_ID}, or {@link android.hardware.radio.ProgramSelector#IDENTIFIER_TYPE_SXM_CHANNEL}
 
 * <br>
 * Value is between IDENTIFIER_TYPE_VENDOR_START and IDENTIFIER_TYPE_VENDOR_END inclusive
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface IdentifierType {
}

/**
 *
 * Value is {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_INVALID}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_AM}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_FM}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_AM_HD}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_FM_HD}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_DAB}, {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_DRMO}, or {@link android.hardware.radio.ProgramSelector#PROGRAM_TYPE_SXM}
 * <br>
 * Value is between PROGRAM_TYPE_VENDOR_START and PROGRAM_TYPE_VENDOR_END inclusive
 * @deprecated use {@link ProgramIdentifier} instead @apiSince REL
 @deprecatedSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface ProgramType {
}

}

