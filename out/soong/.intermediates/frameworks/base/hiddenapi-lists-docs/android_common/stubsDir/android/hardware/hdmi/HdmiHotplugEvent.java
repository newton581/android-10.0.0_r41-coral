/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.hdmi;

import android.os.Parcelable;
import android.os.Parcel;

/**
 * A class that describes the HDMI port hotplug event.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class HdmiHotplugEvent implements android.os.Parcelable {

/**
 * Constructor.
 *
 * <p>Marked as hidden so only system can create the instance.
 *
 * @hide
 */

HdmiHotplugEvent(int port, boolean connected) { throw new RuntimeException("Stub!"); }

/**
 * Returns the port number for which the event occurred.
 *
 * @return port number
 * @apiSince REL
 */

public int getPort() { throw new RuntimeException("Stub!"); }

/**
 * Returns the connection status associated with this event
 *
 * @return true if the device gets connected; otherwise false
 * @apiSince REL
 */

public boolean isConnected() { throw new RuntimeException("Stub!"); }

/**
 * Describes the kinds of special objects contained in this Parcelable's
 * marshalled representation.
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Flattens this object in to a Parcel.
 *
 * @param dest The Parcel in which the object should be written.
 * @param flags Additional flags about how the object should be written.
 *        May be 0 or {@link Parcelable#PARCELABLE_WRITE_RETURN_VALUE}.
 * @apiSince REL
 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.hdmi.HdmiHotplugEvent> CREATOR;
static { CREATOR = null; }
}

