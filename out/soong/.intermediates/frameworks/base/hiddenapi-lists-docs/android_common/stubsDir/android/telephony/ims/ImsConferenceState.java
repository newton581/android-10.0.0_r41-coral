/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims;

import android.telecom.Connection;

/**
 * Provides the conference information (defined in RFC 4575) for IMS conference call.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ImsConferenceState implements android.os.Parcelable {

/** @hide */

ImsConferenceState() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Translates an {@code ImsConferenceState} status type to a telecom connection state.
 *
 * @param status The status type.
 * @return The corresponding {@link android.telecom.Connection} state.
 * @apiSince REL
 */

public static int getConnectionStateForStatus(java.lang.String status) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.ims.ImsConferenceState> CREATOR;
static { CREATOR = null; }

/** @apiSince REL */

public static final java.lang.String DISPLAY_TEXT = "display-text";

/** @apiSince REL */

public static final java.lang.String ENDPOINT = "endpoint";

/**
 * conference-info : SIP status code (integer)
 * @apiSince REL
 */

public static final java.lang.String SIP_STATUS_CODE = "sipstatuscode";

/** @apiSince REL */

public static final java.lang.String STATUS = "status";

/** @apiSince REL */

public static final java.lang.String STATUS_ALERTING = "alerting";

/** @apiSince REL */

public static final java.lang.String STATUS_CONNECTED = "connected";

/** @apiSince REL */

public static final java.lang.String STATUS_CONNECT_FAIL = "connect-fail";

/** @apiSince REL */

public static final java.lang.String STATUS_DIALING_IN = "dialing-in";

/** @apiSince REL */

public static final java.lang.String STATUS_DIALING_OUT = "dialing-out";

/** @apiSince REL */

public static final java.lang.String STATUS_DISCONNECTED = "disconnected";

/** @apiSince REL */

public static final java.lang.String STATUS_DISCONNECTING = "disconnecting";

/** @apiSince REL */

public static final java.lang.String STATUS_MUTED_VIA_FOCUS = "muted-via-focus";

/** @apiSince REL */

public static final java.lang.String STATUS_ON_HOLD = "on-hold";

/**
 * status-type (String) :
 * "pending" : Endpoint is not yet in the session, but it is anticipated that he/she will
 *      join in the near future.
 * "dialing-out" : Focus has dialed out to connect the endpoint to the conference,
 *      but the endpoint is not yet in the roster (probably being authenticated).
 * "dialing-in" : Endpoint is dialing into the conference, not yet in the roster
 *      (probably being authenticated).
 * "alerting" : PSTN alerting or SIP 180 Ringing was returned for the outbound call;
 *      endpoint is being alerted.
 * "on-hold" : Active signaling dialog exists between an endpoint and a focus,
 *      but endpoint is "on-hold" for this conference, i.e., he/she is neither "hearing"
 *      the conference mix nor is his/her media being mixed in the conference.
 * "connected" : Endpoint is a participant in the conference. Depending on the media policies,
 *      he/she can send and receive media to and from other participants.
 * "disconnecting" : Focus is in the process of disconnecting the endpoint
 *      (e.g. in SIP a DISCONNECT or BYE was sent to the endpoint).
 * "disconnected" : Endpoint is not a participant in the conference, and no active dialog
 *      exists between the endpoint and the focus.
 * "muted-via-focus" : Active signaling dialog exists beween an endpoint and a focus and
 *      the endpoint can "listen" to the conference, but the endpoint's media is not being
 *      mixed into the conference.
 * "connect-fail" : Endpoint fails to join the conference by rejecting the conference call.
 * @apiSince REL
 */

public static final java.lang.String STATUS_PENDING = "pending";

/** @apiSince REL */

public static final java.lang.String STATUS_SEND_ONLY = "sendonly";

/** @apiSince REL */

public static final java.lang.String STATUS_SEND_RECV = "sendrecv";

/**
 * conference-info : user
 * @apiSince REL
 */

public static final java.lang.String USER = "user";

/** @apiSince REL */

public final java.util.HashMap<java.lang.String,android.os.Bundle> mParticipants;
{ mParticipants = null; }
}

