/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.oemlock;


/**
 * Interface for managing the OEM lock on the device.
 *
 * This will only be available if the device implements OEM lock protection.
 *
 * Multiple actors have an opinion on whether the device can be OEM unlocked and they must all be in
 * agreement for unlock to be possible.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class OemLockManager {

OemLockManager() { throw new RuntimeException("Stub!"); }

/**
 * Returns a vendor specific name for the OEM lock.
 *
 * This value is used to identify the security protocol used by locks.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_CARRIER_OEM_UNLOCK_STATE}
 * @return The name of the OEM lock or {@code null} if failed to get the name.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.String getLockName() { throw new RuntimeException("Stub!"); }

/**
 * Sets whether the carrier has allowed this device to be OEM unlocked.
 *
 * Depending on the implementation, the validity of the request might need to be proved. This
 * can be acheived by passing a signature that the system will use to verify the request is
 * legitimate.
 *
 * All actors involved must agree for OEM unlock to be possible.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_CARRIER_OEM_UNLOCK_STATE}
 * @param allowed Whether the device should be allowed to be unlocked.
 * @param signature Optional proof of request validity, {@code null} for none.
 * This value may be {@code null}.
 * @throws IllegalArgumentException if a signature is required but was not provided.
 * @throws SecurityException if the wrong signature was provided.
 *
 * @see #isOemUnlockAllowedByCarrier()
 * @apiSince REL
 */

public void setOemUnlockAllowedByCarrier(boolean allowed, @android.annotation.Nullable byte[] signature) { throw new RuntimeException("Stub!"); }

/**
 * Returns whether the carrier has allowed this device to be OEM unlocked.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_CARRIER_OEM_UNLOCK_STATE}
 * @return Whether OEM unlock is allowed by the carrier, or true if no OEM lock is present.
 *
 * @see #setOemUnlockAllowedByCarrier(boolean, byte[])
 * @apiSince REL
 */

public boolean isOemUnlockAllowedByCarrier() { throw new RuntimeException("Stub!"); }

/**
 * Sets whether the user has allowed this device to be unlocked.
 *
 * All actors involved must agree for OEM unlock to be possible.
 *
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_USER_OEM_UNLOCK_STATE}
 * @param allowed Whether the device should be allowed to be unlocked.
 * @throws SecurityException if the user is not allowed to unlock the device.
 *
 * @see #isOemUnlockAllowedByUser()
 * @apiSince REL
 */

public void setOemUnlockAllowedByUser(boolean allowed) { throw new RuntimeException("Stub!"); }

/**
 * Returns whether, or not, the user has allowed this device to be OEM unlocked.
 * <br>
 * Requires {@link android.Manifest.permission#MANAGE_USER_OEM_UNLOCK_STATE}
 * @return Whether OEM unlock is allowed by the user, or true if no OEM lock is present.
 *
 * @see #setOemUnlockAllowedByUser(boolean)
 * @apiSince REL
 */

public boolean isOemUnlockAllowedByUser() { throw new RuntimeException("Stub!"); }
}

