/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.hdmi;

import android.os.Parcelable;
import android.os.Parcel;

/**
 * A class to encapsulate HDMI port information. Contains the capability of the ports such as
 * HDMI-CEC, MHL, ARC(Audio Return Channel), and physical address assigned to each port.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class HdmiPortInfo implements android.os.Parcelable {

/**
 * Constructor.
 *
 * @param id identifier assigned to each port. 1 for HDMI port 1
 * @param type HDMI port input/output type
 * @param address physical address of the port
 * @param cec {@code true} if HDMI-CEC is supported on the port
 * @param mhl {@code true} if MHL is supported on the port
 * @param arc {@code true} if audio return channel is supported on the port
 * @apiSince REL
 */

public HdmiPortInfo(int id, int type, int address, boolean cec, boolean mhl, boolean arc) { throw new RuntimeException("Stub!"); }

/**
 * Returns the port id.
 *
 * @return port id
 * @apiSince REL
 */

public int getId() { throw new RuntimeException("Stub!"); }

/**
 * Returns the port type.
 *
 * @return port type
 * @apiSince REL
 */

public int getType() { throw new RuntimeException("Stub!"); }

/**
 * Returns the port address.
 *
 * @return port address
 * @apiSince REL
 */

public int getAddress() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the port supports HDMI-CEC signaling.
 *
 * @return {@code true} if the port supports HDMI-CEC signaling.
 * @apiSince REL
 */

public boolean isCecSupported() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the port supports MHL signaling.
 *
 * @return {@code true} if the port supports MHL signaling.
 * @apiSince REL
 */

public boolean isMhlSupported() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the port supports audio return channel.
 *
 * @return {@code true} if the port supports audio return channel
 * @apiSince REL
 */

public boolean isArcSupported() { throw new RuntimeException("Stub!"); }

/**
 * Describes the kinds of special objects contained in this Parcelable's
 * marshalled representation.
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Serializes this object into a {@link Parcel}.
 *
 * @param dest The Parcel in which the object should be written.
 * @param flags Additional flags about how the object should be written.
 *        May be 0 or {@link Parcelable#PARCELABLE_WRITE_RETURN_VALUE}.
 * @apiSince REL
 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * A helper class to deserialize {@link HdmiPortInfo} for a parcel.
 * @apiSince REL
 */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.hdmi.HdmiPortInfo> CREATOR;
static { CREATOR = null; }

/**
 * HDMI port type: Input
 * @apiSince REL
 */

public static final int PORT_INPUT = 0; // 0x0

/**
 * HDMI port type: Output
 * @apiSince REL
 */

public static final int PORT_OUTPUT = 1; // 0x1
}

