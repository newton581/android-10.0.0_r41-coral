/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;

import android.content.Context;
import android.net.Uri;

/**
 * A VibrationEffect describes a haptic effect to be performed by a {@link Vibrator}.
 *
 * These effects may be any number of things, from single shot vibrations to complex waveforms.
 * @apiSince 26
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class VibrationEffect implements android.os.Parcelable {

/** @hide to prevent subclassing from outside of the framework */

VibrationEffect() { throw new RuntimeException("Stub!"); }

/**
 * Create a one shot vibration.
 *
 * One shot vibrations will vibrate constantly for the specified period of time at the
 * specified amplitude, and then stop.
 *
 * @param milliseconds The number of milliseconds to vibrate. This must be a positive number.
 * @param amplitude The strength of the vibration. This must be a value between 1 and 255, or
 * {@link #DEFAULT_AMPLITUDE}.
 *
 * @return The desired effect.
 * @apiSince 26
 */

public static android.os.VibrationEffect createOneShot(long milliseconds, int amplitude) { throw new RuntimeException("Stub!"); }

/**
 * Create a waveform vibration.
 *
 * Waveform vibrations are a potentially repeating series of timing and amplitude pairs. For
 * each pair, the value in the amplitude array determines the strength of the vibration and the
 * value in the timing array determines how long it vibrates for. An amplitude of 0 implies no
 * vibration (i.e. off), and any pairs with a timing value of 0 will be ignored.
 * <p>
 * The amplitude array of the generated waveform will be the same size as the given
 * timing array with alternating values of 0 (i.e. off) and {@link #DEFAULT_AMPLITUDE},
 * starting with 0. Therefore the first timing value will be the period to wait before turning
 * the vibrator on, the second value will be how long to vibrate at {@link #DEFAULT_AMPLITUDE}
 * strength, etc.
 * </p><p>
 * To cause the pattern to repeat, pass the index into the timings array at which to start the
 * repetition, or -1 to disable repeating.
 * </p>
 *
 * @param timings The pattern of alternating on-off timings, starting with off. Timing values
 *                of 0 will cause the timing / amplitude pair to be ignored.
 * @param repeat The index into the timings array at which to repeat, or -1 if you you don't
 *               want to repeat.
 *
 * @return The desired effect.
 * @apiSince 26
 */

public static android.os.VibrationEffect createWaveform(long[] timings, int repeat) { throw new RuntimeException("Stub!"); }

/**
 * Create a waveform vibration.
 *
 * Waveform vibrations are a potentially repeating series of timing and amplitude pairs. For
 * each pair, the value in the amplitude array determines the strength of the vibration and the
 * value in the timing array determines how long it vibrates for. An amplitude of 0 implies no
 * vibration (i.e. off), and any pairs with a timing value of 0 will be ignored.
 * </p><p>
 * To cause the pattern to repeat, pass the index into the timings array at which to start the
 * repetition, or -1 to disable repeating.
 * </p>
 *
 * @param timings The timing values of the timing / amplitude pairs. Timing values of 0
 *                will cause the pair to be ignored.
 * @param amplitudes The amplitude values of the timing / amplitude pairs. Amplitude values
 *                   must be between 0 and 255, or equal to {@link #DEFAULT_AMPLITUDE}. An
 *                   amplitude value of 0 implies the motor is off.
 * @param repeat The index into the timings array at which to repeat, or -1 if you you don't
 *               want to repeat.
 *
 * @return The desired effect.
 * @apiSince 26
 */

public static android.os.VibrationEffect createWaveform(long[] timings, int[] amplitudes, int repeat) { throw new RuntimeException("Stub!"); }

/**
 * Create a predefined vibration effect.
 *
 * Predefined effects are a set of common vibration effects that should be identical, regardless
 * of the app they come from, in order to provide a cohesive experience for users across
 * the entire device. They also may be custom tailored to the device hardware in order to
 * provide a better experience than you could otherwise build using the generic building
 * blocks.
 *
 * This will fallback to a generic pattern if one exists and there does not exist a
 * hardware-specific implementation of the effect.
 *
 * @param effectId The ID of the effect to perform:
 *                 {@link #EFFECT_CLICK}, {@link #EFFECT_DOUBLE_CLICK}, {@link #EFFECT_TICK}
 *
 * Value is {@link android.os.VibrationEffect#EFFECT_TICK}, {@link android.os.VibrationEffect#EFFECT_CLICK}, {@link android.os.VibrationEffect#EFFECT_HEAVY_CLICK}, or {@link android.os.VibrationEffect#EFFECT_DOUBLE_CLICK}
 * @return The desired effect.
 
 * This value will never be {@code null}.
 * @apiSince 29
 */

@android.annotation.NonNull
public static android.os.VibrationEffect createPredefined(int effectId) { throw new RuntimeException("Stub!"); }

/**
 * Get a predefined vibration effect.
 *
 * Predefined effects are a set of common vibration effects that should be identical, regardless
 * of the app they come from, in order to provide a cohesive experience for users across
 * the entire device. They also may be custom tailored to the device hardware in order to
 * provide a better experience than you could otherwise build using the generic building
 * blocks.
 *
 * This will fallback to a generic pattern if one exists and there does not exist a
 * hardware-specific implementation of the effect.
 *
 * @param effectId The ID of the effect to perform:
 *                 {@link #EFFECT_CLICK}, {@link #EFFECT_DOUBLE_CLICK}, {@link #EFFECT_TICK}
 *
 * @return The desired effect.
 * @hide
 */

public static android.os.VibrationEffect get(int effectId) { throw new RuntimeException("Stub!"); }

/**
 * Get a predefined vibration effect.
 *
 * Predefined effects are a set of common vibration effects that should be identical, regardless
 * of the app they come from, in order to provide a cohesive experience for users across
 * the entire device. They also may be custom tailored to the device hardware in order to
 * provide a better experience than you could otherwise build using the generic building
 * blocks.
 *
 * Some effects you may only want to play if there's a hardware specific implementation because
 * they may, for example, be too disruptive to the user without tuning. The {@code fallback}
 * parameter allows you to decide whether you want to fallback to the generic implementation or
 * only play if there's a tuned, hardware specific one available.
 *
 * @param effectId The ID of the effect to perform:
 *                 {@link #EFFECT_CLICK}, {@link #EFFECT_DOUBLE_CLICK}, {@link #EFFECT_TICK}
 * @param fallback Whether to fallback to a generic pattern if a hardware specific
 *                 implementation doesn't exist.
 *
 * @return The desired effect.
 * @hide
 */

public static android.os.VibrationEffect get(int effectId, boolean fallback) { throw new RuntimeException("Stub!"); }

/**
 * Get a predefined vibration effect associated with a given URI.
 *
 * Predefined effects are a set of common vibration effects that should be identical, regardless
 * of the app they come from, in order to provide a cohesive experience for users across
 * the entire device. They also may be custom tailored to the device hardware in order to
 * provide a better experience than you could otherwise build using the generic building
 * blocks.
 *
 * @param uri The URI associated with the haptic effect.
 * @param context The context used to get the URI to haptic effect association.
 *
 * @return The desired effect, or {@code null} if there's no associated effect.
 *
 * @hide
 */

@android.annotation.Nullable
public static android.os.VibrationEffect get(android.net.Uri uri, android.content.Context context) { throw new RuntimeException("Stub!"); }

/** @apiSince 26 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * Gets the estimated duration of the vibration in milliseconds.
 *
 * For effects without a defined end (e.g. a Waveform with a non-negative repeat index), this
 * returns Long.MAX_VALUE. For effects with an unknown duration (e.g. Prebaked effects where
 * the length is device and potentially run-time dependent), this returns -1.
 *
 * @hide
 */

public abstract long getDuration();

/**
 * Scale the amplitude with the given constraints.
 *
 * This assumes that the previous value was in the range [0, MAX_AMPLITUDE]
 * @hide
 */

protected static int scale(int amplitude, float gamma, int maxAmplitude) { throw new RuntimeException("Stub!"); }

/** @apiSince 26 */

@androidx.annotation.RecentlyNonNull public static final android.os.Parcelable.Creator<android.os.VibrationEffect> CREATOR;
static { CREATOR = null; }

/**
 * The default vibration strength of the device.
 * @apiSince 26
 */

public static final int DEFAULT_AMPLITUDE = -1; // 0xffffffff

/**
 * A click effect. Use this effect as a baseline, as it's the most common type of click effect.
 *
 * @see #get(int)
 * @apiSince 29
 */

public static final int EFFECT_CLICK = 0; // 0x0

/**
 * A double click effect.
 *
 * @see #get(int)
 * @apiSince 29
 */

public static final int EFFECT_DOUBLE_CLICK = 1; // 0x1

/**
 * A heavy click effect. This effect is stronger than {@link #EFFECT_CLICK}.
 * @see #get(int)
 * @apiSince 29
 */

public static final int EFFECT_HEAVY_CLICK = 5; // 0x5

/**
 * A pop effect.
 * @see #get(int)
 * @hide
 */

public static final int EFFECT_POP = 4; // 0x4

/** {@hide} */

public static final int EFFECT_STRENGTH_LIGHT = 0; // 0x0

/** {@hide} */

public static final int EFFECT_STRENGTH_MEDIUM = 1; // 0x1

/** {@hide} */

public static final int EFFECT_STRENGTH_STRONG = 2; // 0x2

/**
 * A texture effect meant to replicate soft ticks.
 *
 * Unlike normal effects, texture effects are meant to be called repeatedly, generally in
 * response to some motion, in order to replicate the feeling of some texture underneath the
 * user's fingers.
 *
 * @see #get(int)
 * @hide
 */

public static final int EFFECT_TEXTURE_TICK = 21; // 0x15

/**
 * A thud effect.
 * @see #get(int)
 * @hide
 */

public static final int EFFECT_THUD = 3; // 0x3

/**
 * A tick effect. This effect is less strong compared to {@link #EFFECT_CLICK}.
 * @see #get(int)
 * @apiSince 29
 */

public static final int EFFECT_TICK = 2; // 0x2

/**
 * Ringtone patterns. They may correspond with the device's ringtone audio, or may just be a
 * pattern that can be played as a ringtone with any audio, depending on the device.
 *
 * @see #get(Uri, Context)
 * @hide
 */

public static final int[] RINGTONES;
static { RINGTONES = new int[0]; }
/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class OneShot extends android.os.VibrationEffect implements android.os.Parcelable {

/** @apiSince REL */

public OneShot(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public OneShot(long milliseconds, int amplitude) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long getDuration() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getAmplitude() { throw new RuntimeException("Stub!"); }

/**
 * Scale the amplitude of this effect.
 *
 * @param gamma the gamma adjustment to apply
 * @param maxAmplitude the new maximum amplitude of the effect, must be between 0 and
 *         MAX_AMPLITUDE
 * @throws IllegalArgumentException if maxAmplitude less than 0 or more than MAX_AMPLITUDE
 *
 * @return A {@link OneShot} effect with the same timing but scaled amplitude.
 * @apiSince REL
 */

public android.os.VibrationEffect.OneShot scale(float gamma, int maxAmplitude) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void validate() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.VibrationEffect.OneShot> CREATOR;
static { CREATOR = null; }
}

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Prebaked extends android.os.VibrationEffect implements android.os.Parcelable {

/** @apiSince REL */

public Prebaked(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public Prebaked(int effectId, boolean fallback) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getId() { throw new RuntimeException("Stub!"); }

/**
 * Whether the effect should fall back to a generic pattern if there's no hardware specific
 * implementation of it.
 * @apiSince REL
 */

public boolean shouldFallback() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long getDuration() { throw new RuntimeException("Stub!"); }

/**
 * Set the effect strength of the prebaked effect.
 * @apiSince REL
 */

public void setEffectStrength(int strength) { throw new RuntimeException("Stub!"); }

/**
 * Set the effect strength.
 * @apiSince REL
 */

public int getEffectStrength() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void validate() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.VibrationEffect.Prebaked> CREATOR;
static { CREATOR = null; }
}

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Waveform extends android.os.VibrationEffect implements android.os.Parcelable {

/** @apiSince REL */

public Waveform(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public Waveform(long[] timings, int[] amplitudes, int repeat) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long[] getTimings() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int[] getAmplitudes() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getRepeatIndex() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public long getDuration() { throw new RuntimeException("Stub!"); }

/**
 * Scale the Waveform with the given gamma and new max amplitude.
 *
 * @param gamma the gamma adjustment to apply
 * @param maxAmplitude the new maximum amplitude of the effect, must be between 0 and
 *         MAX_AMPLITUDE
 * @throws IllegalArgumentException if maxAmplitude less than 0 or more than MAX_AMPLITUDE
 *
 * @return A {@link Waveform} effect with the same timings and repeat index
 *         but scaled amplitude.
 * @apiSince REL
 */

public android.os.VibrationEffect.Waveform scale(float gamma, int maxAmplitude) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void validate() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.VibrationEffect.Waveform> CREATOR;
static { CREATOR = null; }
}

}

