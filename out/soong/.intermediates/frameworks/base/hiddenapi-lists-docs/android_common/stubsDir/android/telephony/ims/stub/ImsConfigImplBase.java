/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims.stub;


/**
 * Controls the modification of IMS specific configurations. For more information on the supported
 * IMS configuration constants, see {@link ImsConfig}.
 *
 * The inner class {@link ImsConfigStub} implements methods of IImsConfig AIDL interface.
 * The IImsConfig AIDL interface is called by ImsConfig, which may exist in many other processes.
 * ImsConfigImpl access to the configuration parameters may be arbitrarily slow, especially in
 * during initialization, or times when a lot of configuration parameters are being set/get
 * (such as during boot up or SIM card change). By providing a cache in ImsConfigStub, we can speed
 * up access to these configuration parameters, so a query to the ImsConfigImpl does not have to be
 * performed every time.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ImsConfigImplBase {

/** @apiSince REL */

public ImsConfigImplBase() { throw new RuntimeException("Stub!"); }

/**
 * Updates provisioning value and notifies the framework of the change.
 * Doesn't call {@link #setConfig(int,int)} and assumes the result succeeded.
 * This should only be used when the IMS implementer implicitly changed provisioned values.
 *
 * @param item an integer key.
 * @param value in Integer format.
 * @apiSince REL
 */

public final void notifyProvisionedValueChanged(int item, int value) { throw new RuntimeException("Stub!"); }

/**
 * Updates provisioning value and notifies the framework of the change.
 * Doesn't call {@link #setConfig(int,String)} and assumes the result succeeded.
 * This should only be used when the IMS implementer implicitly changed provisioned values.
 *
 * @param item an integer key.
 * @param value in String format.
 * @apiSince REL
 */

public final void notifyProvisionedValueChanged(int item, java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the configuration value for this ImsService.
 *
 * @param item an integer key.
 * @param value an integer containing the configuration value.
 * @return the result of setting the configuration value.
 
 * Value is {@link android.telephony.ims.stub.ImsConfigImplBase#CONFIG_RESULT_SUCCESS}, or {@link android.telephony.ims.stub.ImsConfigImplBase#CONFIG_RESULT_FAILED}
 * @apiSince REL
 */

public int setConfig(int item, int value) { throw new RuntimeException("Stub!"); }

/**
 * Sets the configuration value for this ImsService.
 *
 * @param item an integer key.
 * @param value a String containing the new configuration value.
 * @return Result of setting the configuration value.
 
 * Value is {@link android.telephony.ims.stub.ImsConfigImplBase#CONFIG_RESULT_SUCCESS}, or {@link android.telephony.ims.stub.ImsConfigImplBase#CONFIG_RESULT_FAILED}
 * @apiSince REL
 */

public int setConfig(int item, java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the currently stored value configuration value from the ImsService for {@code item}.
 *
 * @param item an integer key.
 * @return configuration value, stored in integer format or {@link #CONFIG_RESULT_UNKNOWN} if
 * unavailable.
 * @apiSince REL
 */

public int getConfigInt(int item) { throw new RuntimeException("Stub!"); }

/**
 * Gets the currently stored value configuration value from the ImsService for {@code item}.
 *
 * @param item an integer key.
 * @return configuration value, stored in String format or {@code null} if unavailable.
 * @apiSince REL
 */

public java.lang.String getConfigString(int item) { throw new RuntimeException("Stub!"); }

/**
 * Setting the configuration value failed.
 * @apiSince REL
 */

public static final int CONFIG_RESULT_FAILED = 1; // 0x1

/**
 * Setting the configuration value completed.
 * @apiSince REL
 */

public static final int CONFIG_RESULT_SUCCESS = 0; // 0x0

/**
 * The configuration requested resulted in an unknown result. This may happen if the
 * IMS configurations are unavailable.
 * @apiSince REL
 */

public static final int CONFIG_RESULT_UNKNOWN = -1; // 0xffffffff
}

