/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims;

import android.os.Parcelable;
import android.net.Uri;

/**
 * Parcelable object to handle MultiEndpoint Dialog Event Package Information.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ImsExternalCallState implements android.os.Parcelable {

/**
 * Create a new ImsExternalCallState instance to contain Multiendpoint Dialog information.
 * @param callId The unique ID of the call, which will be used to identify this external
 *               connection.
 * This value must never be {@code null}.
 * @param address A {@link Uri} containing the remote address of this external connection.
 * This value must never be {@code null}.
 * @param localAddress A {@link Uri} containing the local address information.
 * This value may be {@code null}.
 * @param isPullable A flag determining if this external connection can be pulled to the current
 *         device.
 * @param callState The state of the external call.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsExternalCallState#CALL_STATE_CONFIRMED}, and {@link android.telephony.ims.ImsExternalCallState#CALL_STATE_TERMINATED}
 * @param callType The type of external call.
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsCallProfile#CALL_TYPE_VOICE}, {@link android.telephony.ims.ImsCallProfile#CALL_TYPE_VT_TX}, {@link android.telephony.ims.ImsCallProfile#CALL_TYPE_VT_RX}, and {@link android.telephony.ims.ImsCallProfile#CALL_TYPE_VT}
 * @param isCallheld A flag determining if the external connection is currently held.
 * @apiSince REL
 */

public ImsExternalCallState(@android.annotation.NonNull java.lang.String callId, @android.annotation.NonNull android.net.Uri address, @android.annotation.Nullable android.net.Uri localAddress, boolean isPullable, int callState, int callType, boolean isCallheld) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getCallId() { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.net.Uri getAddress() { throw new RuntimeException("Stub!"); }

/**
 * @return A {@link Uri} containing the local address from the Multiendpoint Dialog Information.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.net.Uri getLocalAddress() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isCallPullable() { throw new RuntimeException("Stub!"); }

/**
 * @return Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsExternalCallState#CALL_STATE_CONFIRMED}, and {@link android.telephony.ims.ImsExternalCallState#CALL_STATE_TERMINATED}
 * @apiSince REL
 */

public int getCallState() { throw new RuntimeException("Stub!"); }

/**
 * @return Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsCallProfile#CALL_TYPE_VOICE}, {@link android.telephony.ims.ImsCallProfile#CALL_TYPE_VT_TX}, {@link android.telephony.ims.ImsCallProfile#CALL_TYPE_VT_RX}, and {@link android.telephony.ims.ImsCallProfile#CALL_TYPE_VT}
 * @apiSince REL
 */

public int getCallType() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean isCallHeld() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * The external call is in the confirmed dialog state.
 * @apiSince REL
 */

public static final int CALL_STATE_CONFIRMED = 1; // 0x1

/**
 * The external call is in the terminated dialog state.
 * @apiSince REL
 */

public static final int CALL_STATE_TERMINATED = 2; // 0x2

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.ims.ImsExternalCallState> CREATOR;
static { CREATOR = null; }
}

