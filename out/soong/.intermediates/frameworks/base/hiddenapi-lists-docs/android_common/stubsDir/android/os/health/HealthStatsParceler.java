/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os.health;


/**
 * Class to allow sending the HealthStats through aidl generated glue.
 *
 * The alternative would be to send a HealthStats object, which would
 * require constructing one, and then immediately flattening it. This
 * saves that step at the cost of doing the extra flattening when
 * accessed in the same process as the writer.
 *
 * The HealthStatsWriter passed in the constructor is retained, so don't
 * reuse them.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HealthStatsParceler implements android.os.Parcelable {

/** @apiSince REL */

public HealthStatsParceler(android.os.health.HealthStatsWriter writer) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public HealthStatsParceler(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public android.os.health.HealthStats getHealthStats() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.os.health.HealthStatsParceler> CREATOR;
static { CREATOR = null; }
}

