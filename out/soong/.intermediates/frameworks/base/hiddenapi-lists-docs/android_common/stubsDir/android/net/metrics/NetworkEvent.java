/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.net.metrics;


/**
 * {@hide}
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class NetworkEvent implements android.net.metrics.IpConnectivityLog.Event {

/**
 * @param eventType Value is {@link android.net.metrics.NetworkEvent#NETWORK_CONNECTED}, {@link android.net.metrics.NetworkEvent#NETWORK_VALIDATED}, {@link android.net.metrics.NetworkEvent#NETWORK_VALIDATION_FAILED}, {@link android.net.metrics.NetworkEvent#NETWORK_CAPTIVE_PORTAL_FOUND}, {@link android.net.metrics.NetworkEvent#NETWORK_LINGER}, {@link android.net.metrics.NetworkEvent#NETWORK_UNLINGER}, {@link android.net.metrics.NetworkEvent#NETWORK_DISCONNECTED}, {@link android.net.metrics.NetworkEvent#NETWORK_FIRST_VALIDATION_SUCCESS}, {@link android.net.metrics.NetworkEvent#NETWORK_REVALIDATION_SUCCESS}, {@link android.net.metrics.NetworkEvent#NETWORK_FIRST_VALIDATION_PORTAL_FOUND}, {@link android.net.metrics.NetworkEvent#NETWORK_REVALIDATION_PORTAL_FOUND}, {@link android.net.metrics.NetworkEvent#NETWORK_CONSECUTIVE_DNS_TIMEOUT_FOUND}, or {@link android.net.metrics.NetworkEvent#NETWORK_PARTIAL_CONNECTIVITY}
 * @apiSince REL
 */

public NetworkEvent(int eventType, long durationMs) { throw new RuntimeException("Stub!"); }

/**
 * @param eventType Value is {@link android.net.metrics.NetworkEvent#NETWORK_CONNECTED}, {@link android.net.metrics.NetworkEvent#NETWORK_VALIDATED}, {@link android.net.metrics.NetworkEvent#NETWORK_VALIDATION_FAILED}, {@link android.net.metrics.NetworkEvent#NETWORK_CAPTIVE_PORTAL_FOUND}, {@link android.net.metrics.NetworkEvent#NETWORK_LINGER}, {@link android.net.metrics.NetworkEvent#NETWORK_UNLINGER}, {@link android.net.metrics.NetworkEvent#NETWORK_DISCONNECTED}, {@link android.net.metrics.NetworkEvent#NETWORK_FIRST_VALIDATION_SUCCESS}, {@link android.net.metrics.NetworkEvent#NETWORK_REVALIDATION_SUCCESS}, {@link android.net.metrics.NetworkEvent#NETWORK_FIRST_VALIDATION_PORTAL_FOUND}, {@link android.net.metrics.NetworkEvent#NETWORK_REVALIDATION_PORTAL_FOUND}, {@link android.net.metrics.NetworkEvent#NETWORK_CONSECUTIVE_DNS_TIMEOUT_FOUND}, or {@link android.net.metrics.NetworkEvent#NETWORK_PARTIAL_CONNECTIVITY}
 * @apiSince REL
 */

public NetworkEvent(int eventType) { throw new RuntimeException("Stub!"); }

/** @hide */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @hide */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int NETWORK_CAPTIVE_PORTAL_FOUND = 4; // 0x4

/** @apiSince REL */

public static final int NETWORK_CONNECTED = 1; // 0x1

/** @apiSince REL */

public static final int NETWORK_CONSECUTIVE_DNS_TIMEOUT_FOUND = 12; // 0xc

/** @apiSince REL */

public static final int NETWORK_DISCONNECTED = 7; // 0x7

/** @apiSince REL */

public static final int NETWORK_FIRST_VALIDATION_PORTAL_FOUND = 10; // 0xa

/** @apiSince REL */

public static final int NETWORK_FIRST_VALIDATION_SUCCESS = 8; // 0x8

/** @apiSince REL */

public static final int NETWORK_LINGER = 5; // 0x5

/** @apiSince REL */

public static final int NETWORK_PARTIAL_CONNECTIVITY = 13; // 0xd

/** @apiSince REL */

public static final int NETWORK_REVALIDATION_PORTAL_FOUND = 11; // 0xb

/** @apiSince REL */

public static final int NETWORK_REVALIDATION_SUCCESS = 9; // 0x9

/** @apiSince REL */

public static final int NETWORK_UNLINGER = 6; // 0x6

/** @apiSince REL */

public static final int NETWORK_VALIDATED = 2; // 0x2

/** @apiSince REL */

public static final int NETWORK_VALIDATION_FAILED = 3; // 0x3
}

