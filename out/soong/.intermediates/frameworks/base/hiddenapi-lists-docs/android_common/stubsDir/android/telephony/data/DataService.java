/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony.data;

import android.telephony.AccessNetworkConstants;
import android.net.LinkProperties;
import java.util.List;

/**
 * Base class of data service. Services that extend DataService must register the service in
 * their AndroidManifest to be detected by the framework. They must be protected by the permission
 * "android.permission.BIND_TELEPHONY_DATA_SERVICE". The data service definition in the manifest
 * must follow the following format:
 * ...
 * <service android:name=".xxxDataService"
 *     android:permission="android.permission.BIND_TELEPHONY_DATA_SERVICE" >
 *     <intent-filter>
 *         <action android:name="android.telephony.data.DataService" />
 *     </intent-filter>
 * </service>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class DataService extends android.app.Service {

/**
 * Default constructor.
 * @apiSince REL
 */

public DataService() { throw new RuntimeException("Stub!"); }

/**
 * Create the instance of {@link DataServiceProvider}. Data service provider must override
 * this method to facilitate the creation of {@link DataServiceProvider} instances. The system
 * will call this method after binding the data service for each active SIM slot id.
 *
 * @param slotIndex SIM slot id the data service associated with.
 * @return Data service object. Null if failed to create the provider (e.g. invalid slot index)
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public abstract android.telephony.data.DataService.DataServiceProvider onCreateDataServiceProvider(int slotIndex);

/** @apiSince REL */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean onUnbind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onDestroy() { throw new RuntimeException("Stub!"); }

/**
 * The reason of the data request is IWLAN handover
 * @apiSince REL
 */

public static final int REQUEST_REASON_HANDOVER = 3; // 0x3

/**
 * The reason of the data request is normal
 * @apiSince REL
 */

public static final int REQUEST_REASON_NORMAL = 1; // 0x1

/**
 * The reason of the data request is device shutdown
 * @apiSince REL
 */

public static final int REQUEST_REASON_SHUTDOWN = 2; // 0x2

/**
 * The reason of the data request is unknown
 * @apiSince REL
 */

public static final int REQUEST_REASON_UNKNOWN = 0; // 0x0

/** @apiSince REL */

public static final java.lang.String SERVICE_INTERFACE = "android.telephony.data.DataService";
/**
 * The abstract class of the actual data service implementation. The data service provider
 * must extend this class to support data connection. Note that each instance of data service
 * provider is associated with one physical SIM slot.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class DataServiceProvider implements java.lang.AutoCloseable {

/**
 * Constructor
 * @param slotIndex SIM slot index the data service provider associated with.
 */

public DataServiceProvider(int slotIndex) { throw new RuntimeException("Stub!"); }

/**
 * @return SIM slot index the data service provider associated with.
 * @apiSince REL
 */

public final int getSlotIndex() { throw new RuntimeException("Stub!"); }

/**
 * Setup a data connection. The data service provider must implement this method to support
 * establishing a packet data connection. When completed or error, the service must invoke
 * the provided callback to notify the platform.
 *
 * @param accessNetworkType Access network type that the data call will be established on.
 *        Must be one of {@link AccessNetworkConstants.AccessNetworkType}.
 * @param dataProfile Data profile used for data call setup. See {@link DataProfile}
 * This value must never be {@code null}.
 * @param isRoaming True if the device is data roaming.
 * @param allowRoaming True if data roaming is allowed by the user.
 * @param reason The reason for data setup. Must be {@link #REQUEST_REASON_NORMAL} or
 *        {@link #REQUEST_REASON_HANDOVER}.
 * Value is {@link android.telephony.data.DataService#REQUEST_REASON_UNKNOWN}, {@link android.telephony.data.DataService#REQUEST_REASON_NORMAL}, or {@link android.telephony.data.DataService#REQUEST_REASON_HANDOVER}
 * @param linkProperties If {@code reason} is {@link #REQUEST_REASON_HANDOVER}, this is the
 *        link properties of the existing data connection, otherwise null.
 * This value may be {@code null}.
 * @param callback The result callback for this request.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void setupDataCall(int accessNetworkType, @android.annotation.NonNull android.telephony.data.DataProfile dataProfile, boolean isRoaming, boolean allowRoaming, int reason, @android.annotation.Nullable android.net.LinkProperties linkProperties, @android.annotation.NonNull android.telephony.data.DataServiceCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Deactivate a data connection. The data service provider must implement this method to
 * support data connection tear down. When completed or error, the service must invoke the
 * provided callback to notify the platform.
 *
 * @param cid Call id returned in the callback of {@link DataServiceProvider#setupDataCall(
 *        int, DataProfile, boolean, boolean, int, LinkProperties, DataServiceCallback)}.
 * @param reason The reason for data deactivation. Must be {@link #REQUEST_REASON_NORMAL},
 *        {@link #REQUEST_REASON_SHUTDOWN} or {@link #REQUEST_REASON_HANDOVER}.
 * Value is {@link android.telephony.data.DataService#REQUEST_REASON_UNKNOWN}, {@link android.telephony.data.DataService#REQUEST_REASON_NORMAL}, {@link android.telephony.data.DataService#REQUEST_REASON_SHUTDOWN}, or {@link android.telephony.data.DataService#REQUEST_REASON_HANDOVER}
 * @param callback The result callback for this request. Null if the client does not care
 *        about the result.
 *
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void deactivateDataCall(int cid, int reason, @android.annotation.Nullable android.telephony.data.DataServiceCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Set an APN to initial attach network.
 *
 * @param dataProfile Data profile used for data call setup. See {@link DataProfile}.
 * This value must never be {@code null}.
 * @param isRoaming True if the device is data roaming.
 * @param callback The result callback for this request.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void setInitialAttachApn(@android.annotation.NonNull android.telephony.data.DataProfile dataProfile, boolean isRoaming, @android.annotation.NonNull android.telephony.data.DataServiceCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Send current carrier's data profiles to the data service for data call setup. This is
 * only for CDMA carrier that can change the profile through OTA. The data service should
 * always uses the latest data profile sent by the framework.
 *
 * @param dps A list of data profiles.
 * This value must never be {@code null}.
 * @param isRoaming True if the device is data roaming.
 * @param callback The result callback for this request.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void setDataProfile(@android.annotation.NonNull java.util.List<android.telephony.data.DataProfile> dps, boolean isRoaming, @android.annotation.NonNull android.telephony.data.DataServiceCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Get the active data call list.
 *
 * @param callback The result callback for this request.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void requestDataCallList(@android.annotation.NonNull android.telephony.data.DataServiceCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Notify the system that current data call list changed. Data service must invoke this
 * method whenever there is any data call status changed.
 *
 * @param dataCallList List of the current active data call.
 * @apiSince REL
 */

public final void notifyDataCallListChanged(java.util.List<android.telephony.data.DataCallResponse> dataCallList) { throw new RuntimeException("Stub!"); }

/**
 * Called when the instance of data service is destroyed (e.g. got unbind or binder died)
 * or when the data service provider is removed. The extended class should implement this
 * method to perform cleanup works.
 * @apiSince REL
 */

public abstract void close();
}

}

