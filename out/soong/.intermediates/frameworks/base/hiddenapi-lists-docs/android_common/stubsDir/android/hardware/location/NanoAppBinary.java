/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.location;


/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class NanoAppBinary implements android.os.Parcelable {

/** @apiSince REL */

public NanoAppBinary(byte[] appBinary) { throw new RuntimeException("Stub!"); }

/**
 * @return the app binary byte array
 * @apiSince REL
 */

public byte[] getBinary() { throw new RuntimeException("Stub!"); }

/**
 * @return the app binary byte array without the leading header
 *
 * @throws IndexOutOfBoundsException if the nanoapp binary size is smaller than the header size
 * @throws NullPointerException if the nanoapp binary is null
 * @apiSince REL
 */

public byte[] getBinaryNoHeader() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if the header is valid, {@code false} otherwise
 * @apiSince REL
 */

public boolean hasValidHeader() { throw new RuntimeException("Stub!"); }

/**
 * @return the header version
 * @apiSince REL
 */

public int getHeaderVersion() { throw new RuntimeException("Stub!"); }

/**
 * @return the app ID parsed from the nanoapp header
 * @apiSince REL
 */

public long getNanoAppId() { throw new RuntimeException("Stub!"); }

/**
 * @return the app version parsed from the nanoapp header
 * @apiSince REL
 */

public int getNanoAppVersion() { throw new RuntimeException("Stub!"); }

/**
 * @return the compile target hub type parsed from the nanoapp header
 * @apiSince REL
 */

public long getHwHubType() { throw new RuntimeException("Stub!"); }

/**
 * @return the target CHRE API major version parsed from the nanoapp header
 * @apiSince REL
 */

public byte getTargetChreApiMajorVersion() { throw new RuntimeException("Stub!"); }

/**
 * @return the target CHRE API minor version parsed from the nanoapp header
 * @apiSince REL
 */

public byte getTargetChreApiMinorVersion() { throw new RuntimeException("Stub!"); }

/**
 * Returns the flags for the nanoapp as defined in context_hub.h.
 *
 * This method is meant to be used by the Context Hub Service.
 *
 * @return the flags for the nanoapp
 * @apiSince REL
 */

public int getFlags() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if the nanoapp binary is signed, {@code false} otherwise
 * @apiSince REL
 */

public boolean isSigned() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if the nanoapp binary is encrypted, {@code false} otherwise
 * @apiSince REL
 */

public boolean isEncrypted() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.location.NanoAppBinary> CREATOR;
static { CREATOR = null; }
}

