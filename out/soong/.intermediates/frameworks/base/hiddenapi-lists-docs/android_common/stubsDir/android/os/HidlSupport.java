/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;

import java.util.Objects;
import java.util.Arrays;

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HidlSupport {

/** @hide */

HidlSupport() { throw new RuntimeException("Stub!"); }

/**
 * Similar to Objects.deepEquals, but also take care of lists.
 * Two objects of HIDL types are considered equal if:
 * 1. Both null
 * 2. Both non-null, and of the same class, and:
 * 2.1 Both are primitive arrays / enum arrays, elements are equal using == check
 * 2.2 Both are object arrays, elements are checked recursively
 * 2.3 Both are Lists, elements are checked recursively
 * 2.4 (If both are collections other than lists or maps, throw an error)
 * 2.5 lft.equals(rgt) returns true
 * @hide
 */

public static boolean deepEquals(java.lang.Object lft, java.lang.Object rgt) { throw new RuntimeException("Stub!"); }

/**
 * Similar to Arrays.deepHashCode, but also take care of lists.
 * @hide
 */

public static int deepHashCode(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * Test that two interfaces are equal. This is the Java equivalent to C++
 * interfacesEqual function.
 * This essentially calls .equals on the internal binder objects (via Binder()).
 * - If both interfaces are proxies, asBinder() returns a {@link HwRemoteBinder}
 *   object, and they are compared in {@link HwRemoteBinder#equals}.
 * - If both interfaces are stubs, asBinder() returns the object itself. By default,
 *   auto-generated IFoo.Stub does not override equals(), but an implementation can
 *   optionally override it, and {@code interfacesEqual} will use it here.
 * @hide
 */

public static boolean interfacesEqual(android.os.IHwInterface lft, java.lang.Object rgt) { throw new RuntimeException("Stub!"); }

/**
 * Return PID of process only if on a non-user build. For debugging purposes.
 * @hide
 */

public static native int getPidIfSharable();
}

