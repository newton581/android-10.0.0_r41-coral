/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.view.contentcapture;

import android.view.autofill.AutofillId;

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ContentCaptureEvent implements android.os.Parcelable {

/** @hide */

ContentCaptureEvent(int sessionId, int type) { throw new RuntimeException("Stub!"); }

/**
 * Gets the {@link ContentCaptureContext} set calls to
 * {@link ContentCaptureSession#setContentCaptureContext(ContentCaptureContext)}.
 *
 * <p>Only set on {@link #TYPE_CONTEXT_UPDATED} events.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.view.contentcapture.ContentCaptureContext getContentCaptureContext() { throw new RuntimeException("Stub!"); }

/**
 * Gets the type of the event.
 *
 * @return one of {@link #TYPE_VIEW_APPEARED}, {@link #TYPE_VIEW_DISAPPEARED},
 * {@link #TYPE_VIEW_TEXT_CHANGED}, {@link #TYPE_VIEW_TREE_APPEARING},
 * {@link #TYPE_VIEW_TREE_APPEARED}, {@link #TYPE_CONTEXT_UPDATED},
 * {@link #TYPE_SESSION_RESUMED}, or {@link #TYPE_SESSION_PAUSED}.
 
 * Value is {@link android.view.contentcapture.ContentCaptureEvent#TYPE_VIEW_APPEARED}, {@link android.view.contentcapture.ContentCaptureEvent#TYPE_VIEW_DISAPPEARED}, {@link android.view.contentcapture.ContentCaptureEvent#TYPE_VIEW_TEXT_CHANGED}, {@link android.view.contentcapture.ContentCaptureEvent#TYPE_VIEW_TREE_APPEARING}, {@link android.view.contentcapture.ContentCaptureEvent#TYPE_VIEW_TREE_APPEARED}, {@link android.view.contentcapture.ContentCaptureEvent#TYPE_CONTEXT_UPDATED}, {@link android.view.contentcapture.ContentCaptureEvent#TYPE_SESSION_PAUSED}, or {@link android.view.contentcapture.ContentCaptureEvent#TYPE_SESSION_RESUMED}
 * @apiSince REL
 */

public int getType() { throw new RuntimeException("Stub!"); }

/**
 * Gets when the event was generated, in millis since epoch.
 * @apiSince REL
 */

public long getEventTime() { throw new RuntimeException("Stub!"); }

/**
 * Gets the whole metadata of the node associated with the event.
 *
 * <p>Only set on {@link #TYPE_VIEW_APPEARED} events.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.view.contentcapture.ViewNode getViewNode() { throw new RuntimeException("Stub!"); }

/**
 * Gets the {@link AutofillId} of the node associated with the event.
 *
 * <p>Only set on {@link #TYPE_VIEW_DISAPPEARED} (when the event contains just one node - if
 * it contains more than one, this method returns {@code null} and the actual ids should be
 * retrived by {@link #getIds()}) and {@link #TYPE_VIEW_TEXT_CHANGED} events.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.view.autofill.AutofillId getId() { throw new RuntimeException("Stub!"); }

/**
 * Gets the {@link AutofillId AutofillIds} of the nodes associated with the event.
 *
 * <p>Only set on {@link #TYPE_VIEW_DISAPPEARED}, when the event contains more than one node
 * (if it contains just one node, it's returned by {@link #getId()} instead.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.util.List<android.view.autofill.AutofillId> getIds() { throw new RuntimeException("Stub!"); }

/**
 * Gets the current text of the node associated with the event.
 *
 * <p>Only set on {@link #TYPE_VIEW_TEXT_CHANGED} events.
 
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.lang.CharSequence getText() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.view.contentcapture.ContentCaptureEvent> CREATOR;
static { CREATOR = null; }

/**
 * Called after a call to
 * {@link ContentCaptureSession#setContentCaptureContext(ContentCaptureContext)}.
 *
 * <p>The passed context is available through {@link #getContentCaptureContext()}.
 * @apiSince REL
 */

public static final int TYPE_CONTEXT_UPDATED = 6; // 0x6

/**
 * Called after the session is paused, typically after the activity paused and the
 * views disappeared.
 * @apiSince REL
 */

public static final int TYPE_SESSION_PAUSED = 8; // 0x8

/**
 * Called after the session is ready, typically after the activity resumed and the
 * initial views appeared
 * @apiSince REL
 */

public static final int TYPE_SESSION_RESUMED = 7; // 0x7

/**
 * Called when a node has been added to the screen and is visible to the user.
 *
 * <p>The metadata of the node is available through {@link #getViewNode()}.
 * @apiSince REL
 */

public static final int TYPE_VIEW_APPEARED = 1; // 0x1

/**
 * Called when one or more nodes have been removed from the screen and is not visible to the
 * user anymore.
 *
 * <p>To get the id(s), first call {@link #getIds()} - if it returns {@code null}, then call
 * {@link #getId()}.
 * @apiSince REL
 */

public static final int TYPE_VIEW_DISAPPEARED = 2; // 0x2

/**
 * Called when the text of a node has been changed.
 *
 * <p>The id of the node is available through {@link #getId()}, and the new text is
 * available through {@link #getText()}.
 * @apiSince REL
 */

public static final int TYPE_VIEW_TEXT_CHANGED = 3; // 0x3

/**
 * Called after events (such as {@link #TYPE_VIEW_APPEARED} and/or
 * {@link #TYPE_VIEW_DISAPPEARED}) representing a view hierarchy were sent.
 *
 * <p><b>NOTE</b>: there is no guarantee this event will be sent. For example, it's not sent
 * if the initial view hierarchy doesn't initially have any view that's important for content
 * capture.
 * @apiSince REL
 */

public static final int TYPE_VIEW_TREE_APPEARED = 5; // 0x5

/**
 * Called before events (such as {@link #TYPE_VIEW_APPEARED} and/or
 * {@link #TYPE_VIEW_DISAPPEARED}) representing a view hierarchy are sent.
 *
 * <p><b>NOTE</b>: there is no guarantee this event will be sent. For example, it's not sent
 * if the initial view hierarchy doesn't initially have any view that's important for content
 * capture.
 * @apiSince REL
 */

public static final int TYPE_VIEW_TREE_APPEARING = 4; // 0x4
}

