/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.pm;

import android.os.PersistableBundle;
import java.util.Locale;

/**
 * A container to describe the dialog to be shown when the user tries to launch a suspended
 * application.
 * The suspending app can customize the dialog's following attributes:
 * <ul>
 * <li>The dialog icon, by providing a resource id.
 * <li>The title text, by providing a resource id.
 * <li>The text of the dialog's body, by providing a resource id or a string.
 * <li>The text on the neutral button which starts the
 * {@link android.content.Intent#ACTION_SHOW_SUSPENDED_APP_DETAILS SHOW_SUSPENDED_APP_DETAILS}
 * activity, by providing a resource id.
 * </ul>
 * System defaults are used whenever any of these are not provided, or any of the provided resource
 * ids cannot be resolved at the time of displaying the dialog.
 *
 * @hide
 * @see PackageManager#setPackagesSuspended(String[], boolean, PersistableBundle, PersistableBundle,
 * SuspendDialogInfo)
 * @see Builder
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class SuspendDialogInfo implements android.os.Parcelable {

SuspendDialogInfo(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int parcelableFlags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.content.pm.SuspendDialogInfo> CREATOR;
static { CREATOR = null; }
/**
 * Builder to build a {@link SuspendDialogInfo} object.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Set the resource id of the icon to be used. If not provided, no icon will be shown.
 *
 * @param resId The resource id of the icon.
 * @return this builder object.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.content.pm.SuspendDialogInfo.Builder setIcon(int resId) { throw new RuntimeException("Stub!"); }

/**
 * Set the resource id of the title text to be displayed. If this is not provided, the
 * system will use a default title.
 *
 * @param resId The resource id of the title.
 * @return this builder object.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.content.pm.SuspendDialogInfo.Builder setTitle(int resId) { throw new RuntimeException("Stub!"); }

/**
 * Set the text to show in the body of the dialog. Ignored if a resource id is set via
 * {@link #setMessage(int)}.
 * <p>
 * The system will use {@link String#format(Locale, String, Object...) String.format} to
 * insert the suspended app name into the message, so an example format string could be
 * {@code "The app %1$s is currently suspended"}. This is optional - if the string passed in
 * {@code message} does not accept an argument, it will be used as is.
 *
 * @param message The dialog message.
 * This value must never be {@code null}.
 * @return this builder object.
 * This value will never be {@code null}.
 * @see #setMessage(int)
 * @apiSince REL
 */

@android.annotation.NonNull
public android.content.pm.SuspendDialogInfo.Builder setMessage(@android.annotation.NonNull java.lang.String message) { throw new RuntimeException("Stub!"); }

/**
 * Set the resource id of the dialog message to be shown. If no dialog message is provided
 * via either this method or {@link #setMessage(String)}, the system will use a
 * default message.
 * <p>
 * The system will use {@link android.content.res.Resources#getString(int, Object...)
 * getString} to insert the suspended app name into the message, so an example format string
 * could be {@code "The app %1$s is currently suspended"}. This is optional - if the string
 * referred to by {@code resId} does not accept an argument, it will be used as is.
 *
 * @param resId The resource id of the dialog message.
 * @return this builder object.
 * This value will never be {@code null}.
 * @see #setMessage(String)
 * @apiSince REL
 */

@android.annotation.NonNull
public android.content.pm.SuspendDialogInfo.Builder setMessage(int resId) { throw new RuntimeException("Stub!"); }

/**
 * Set the resource id of text to be shown on the neutral button. Tapping this button starts
 * the {@link android.content.Intent#ACTION_SHOW_SUSPENDED_APP_DETAILS} activity. If this is
 * not provided, the system will use a default text.
 *
 * @param resId The resource id of the button text
 * @return this builder object.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.content.pm.SuspendDialogInfo.Builder setNeutralButtonText(int resId) { throw new RuntimeException("Stub!"); }

/**
 * Build the final object based on given inputs.
 *
 * @return The {@link SuspendDialogInfo} object built using this builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.content.pm.SuspendDialogInfo build() { throw new RuntimeException("Stub!"); }
}

}

