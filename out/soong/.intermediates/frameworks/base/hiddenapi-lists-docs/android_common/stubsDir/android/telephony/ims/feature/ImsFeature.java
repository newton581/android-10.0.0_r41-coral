/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims.feature;

import java.util.Set;
import android.telephony.ims.stub.ImsRegistrationImplBase;

/**
 * Base class for all IMS features that are supported by the framework. Use a concrete subclass
 * of {@link ImsFeature}, such as {@link MmTelFeature} or {@link RcsFeature}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ImsFeature {

public ImsFeature() { throw new RuntimeException("Stub!"); }

/**
 * Set the state of the ImsFeature. The state is used as a signal to the framework to start or
 * stop communication, depending on the state sent.
 * @param state The ImsFeature's state, defined as {@link #STATE_UNAVAILABLE},
 * {@link #STATE_INITIALIZING}, or {@link #STATE_READY}.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.ImsFeature#STATE_UNAVAILABLE}, {@link android.telephony.ims.feature.ImsFeature#STATE_INITIALIZING}, and {@link android.telephony.ims.feature.ImsFeature#STATE_READY}
 * @apiSince REL
 */

public final void setFeatureState(int state) { throw new RuntimeException("Stub!"); }

/**
 * Features should override this method to receive Capability preference change requests from
 * the framework using the provided {@link CapabilityChangeRequest}. If any of the capabilities
 * in the {@link CapabilityChangeRequest} are not able to be completed due to an error,
 * {@link CapabilityCallbackProxy#onChangeCapabilityConfigurationError} should be called for
 * each failed capability.
 *
 * @param request A {@link CapabilityChangeRequest} containing requested capabilities to
 *     enable/disable.
 * @param c A {@link CapabilityCallbackProxy}, which will be used to call back to the framework
 * setting a subset of these capabilities fail, using
 * {@link CapabilityCallbackProxy#onChangeCapabilityConfigurationError}.
 * @apiSince REL
 */

public abstract void changeEnabledCapabilities(android.telephony.ims.feature.CapabilityChangeRequest request, android.telephony.ims.feature.ImsFeature.CapabilityCallbackProxy c);

/**
 * Called when the framework is removing this feature and it needs to be cleaned up.
 * @apiSince REL
 */

public abstract void onFeatureRemoved();

/**
 * Called when the feature has been initialized and communication with the framework is set up.
 * Any attempt by this feature to access the framework before this method is called will return
 * with an {@link IllegalStateException}.
 * The IMS provider should use this method to trigger registration for this feature on the IMS
 * network, if needed.
 * @apiSince REL
 */

public abstract void onFeatureReady();

/**
 * The capability was unable to be changed.
 * @apiSince REL
 */

public static final int CAPABILITY_ERROR_GENERIC = -1; // 0xffffffff

/**
 * The capability was able to be changed.
 * @apiSince REL
 */

public static final int CAPABILITY_SUCCESS = 0; // 0x0

/**
 * This feature supports emergency calling over MMTEL. If defined, the framework will try to
 * place an emergency call over IMS first. If it is not defined, the framework will only use
 * CSFB for emergency calling.
 * @apiSince REL
 */

public static final int FEATURE_EMERGENCY_MMTEL = 0; // 0x0

/**
 * This feature supports the MMTEL feature.
 * @apiSince REL
 */

public static final int FEATURE_MMTEL = 1; // 0x1

/**
 * This feature supports the RCS feature.
 * @apiSince REL
 */

public static final int FEATURE_RCS = 2; // 0x2

/**
 * This {@link ImsFeature} state is initializing and should not be communicated with.
 * @apiSince REL
 */

public static final int STATE_INITIALIZING = 1; // 0x1

/**
 * This {@link ImsFeature} is ready for communication.
 * @apiSince REL
 */

public static final int STATE_READY = 2; // 0x2

/**
 * This {@link ImsFeature}'s state is unavailable and should not be communicated with.
 * @apiSince REL
 */

public static final int STATE_UNAVAILABLE = 0; // 0x0
/**
 * Contains the capabilities defined and supported by an ImsFeature in the form of a bit mask.
 * @hide
 * @deprecated Use {@link MmTelFeature.MmTelCapabilities} instead.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public static class Capabilities {

/**
 * @hide
 */

Capabilities() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated protected int mCapabilities = 0; // 0x0
}

/**
 * Used by the ImsFeature to call back to the CapabilityCallback that the framework has
 * provided.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
protected static class CapabilityCallbackProxy {

CapabilityCallbackProxy() { throw new RuntimeException("Stub!"); }

/**
 * This method notifies the provided framework callback that the request to change the
 * indicated capability has failed and has not changed.
 *
 * @param capability The Capability that will be notified to the framework, defined as
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VOICE},
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_VIDEO},
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_UT}, or
 * {@link MmTelFeature.MmTelCapabilities#CAPABILITY_TYPE_SMS}.
 * @param radioTech The radio tech that this capability failed for, defined as
 * {@link ImsRegistrationImplBase#REGISTRATION_TECH_LTE} or
 * {@link ImsRegistrationImplBase#REGISTRATION_TECH_IWLAN}.
 * @param reason The reason this capability was unable to be changed, defined as
 * {@link #CAPABILITY_ERROR_GENERIC} or {@link #CAPABILITY_SUCCESS}.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.feature.ImsFeature#CAPABILITY_ERROR_GENERIC}, and {@link android.telephony.ims.feature.ImsFeature#CAPABILITY_SUCCESS}
 * @apiSince REL
 */

public void onChangeCapabilityConfigurationError(int capability, int radioTech, int reason) { throw new RuntimeException("Stub!"); }
}

}

