/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.provider;


/**
 * Describe the contract for an Indexable data.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class SearchIndexablesContract {

public SearchIndexablesContract() { throw new RuntimeException("Stub!"); }

/**
 * Non indexable data keys columns indices.
 * @apiSince REL
 */

public static final int COLUMN_INDEX_NON_INDEXABLE_KEYS_KEY_VALUE = 0; // 0x0

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_CLASS_NAME = 7; // 0x7

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_ENTRIES = 4; // 0x4

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_ICON_RESID = 8; // 0x8

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_INTENT_ACTION = 9; // 0x9

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_INTENT_TARGET_CLASS = 11; // 0xb

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_INTENT_TARGET_PACKAGE = 10; // 0xa

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_KEY = 12; // 0xc

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_KEYWORDS = 5; // 0x5

/**
 * Indexable raw data columns indices.
 * @apiSince REL
 */

public static final int COLUMN_INDEX_RAW_RANK = 0; // 0x0

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_SCREEN_TITLE = 6; // 0x6

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_SUMMARY_OFF = 3; // 0x3

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_SUMMARY_ON = 2; // 0x2

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_TITLE = 1; // 0x1

/** @apiSince REL */

public static final int COLUMN_INDEX_RAW_USER_ID = 13; // 0xd

/** @apiSince REL */

public static final int COLUMN_INDEX_XML_RES_CLASS_NAME = 2; // 0x2

/** @apiSince REL */

public static final int COLUMN_INDEX_XML_RES_ICON_RESID = 3; // 0x3

/** @apiSince REL */

public static final int COLUMN_INDEX_XML_RES_INTENT_ACTION = 4; // 0x4

/** @apiSince REL */

public static final int COLUMN_INDEX_XML_RES_INTENT_TARGET_CLASS = 6; // 0x6

/** @apiSince REL */

public static final int COLUMN_INDEX_XML_RES_INTENT_TARGET_PACKAGE = 5; // 0x5

/**
 * Indexable xml resources columns indices.
 * @apiSince REL
 */

public static final int COLUMN_INDEX_XML_RES_RANK = 0; // 0x0

/** @apiSince REL */

public static final int COLUMN_INDEX_XML_RES_RESID = 1; // 0x1

/**
 * Indexable raw data names.
 * @apiSince REL
 */

public static final java.lang.String INDEXABLES_RAW = "indexables_raw";

/**
 * Indexable raw data columns.
 * @apiSince REL
 */

public static final java.lang.String[] INDEXABLES_RAW_COLUMNS;
static { INDEXABLES_RAW_COLUMNS = new java.lang.String[0]; }

/**
 * ContentProvider path for indexable raw data.
 * @apiSince REL
 */

public static final java.lang.String INDEXABLES_RAW_PATH = "settings/indexables_raw";

/**
 * Indexable reference names.
 * @apiSince REL
 */

public static final java.lang.String INDEXABLES_XML_RES = "indexables_xml_res";

/**
 * Indexable xml resources columns.
 * @apiSince REL
 */

public static final java.lang.String[] INDEXABLES_XML_RES_COLUMNS;
static { INDEXABLES_XML_RES_COLUMNS = new java.lang.String[0]; }

/**
 * ContentProvider path for indexable xml resources.
 * @apiSince REL
 */

public static final java.lang.String INDEXABLES_XML_RES_PATH = "settings/indexables_xml_res";

/**
 * Non indexable data keys.
 * @apiSince REL
 */

public static final java.lang.String NON_INDEXABLES_KEYS = "non_indexables_key";

/**
 * Indexable raw data columns.
 * @apiSince REL
 */

public static final java.lang.String[] NON_INDEXABLES_KEYS_COLUMNS;
static { NON_INDEXABLES_KEYS_COLUMNS = new java.lang.String[0]; }

/**
 * ContentProvider path for non indexable data keys.
 * @apiSince REL
 */

public static final java.lang.String NON_INDEXABLES_KEYS_PATH = "settings/non_indexables_key";

/**
 * Intent action used to identify {@link SearchIndexablesProvider}
 * instances. This is used in the {@code <intent-filter>} of a {@code <provider>}.
 * @apiSince REL
 */

public static final java.lang.String PROVIDER_INTERFACE = "android.content.action.SEARCH_INDEXABLES_PROVIDER";

/**
 * Last path segment for Preference Key, Slice Uri pair.
 * <p>
 *     The (Key, Slice Uri) pairs are a mapping between the primary key of the search result and
 *     a Uri for a Slice that represents the same data. Thus, an app can specify a list of Uris
 *     for Slices that replace regular intent-based search results with inline content.
 * </p>
 * @apiSince REL
 */

public static final java.lang.String SLICE_URI_PAIRS = "slice_uri_pairs";

/**
 * Cursor schema for SliceUriPairs.
 * @apiSince REL
 */

@android.annotation.NonNull public static final java.lang.String[] SLICE_URI_PAIRS_COLUMNS;
static { SLICE_URI_PAIRS_COLUMNS = new java.lang.String[0]; }

/**
 * ContentProvider path for Slice Uri pairs.
 * @apiSince REL
 */

public static final java.lang.String SLICE_URI_PAIRS_PATH = "settings/slice_uri_pairs";
/**
 * The base columns.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class BaseColumns {

BaseColumns() { throw new RuntimeException("Stub!"); }

/**
 * Class name associated with the data (usually a Fragment class name).
 * @apiSince REL
 */

public static final java.lang.String COLUMN_CLASS_NAME = "className";

/**
 * Icon resource ID for the data.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_ICON_RESID = "iconResId";

/**
 * Intent action associated with the data.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_INTENT_ACTION = "intentAction";

/**
 * Intent target class associated with the data.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_INTENT_TARGET_CLASS = "intentTargetClass";

/**
 * Intent target package associated with the data.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_INTENT_TARGET_PACKAGE = "intentTargetPackage";

/**
 * Rank of the data. This is an integer used for ranking the search results. This is
 * application specific.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_RANK = "rank";
}

/**
 * Constants related to a {@link SearchIndexableResource} and {@link SearchIndexableData}.
 *
 * This is a description of a data (thru its unique key) that cannot be indexed.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class NonIndexableKey extends android.provider.SearchIndexablesContract.BaseColumns {

NonIndexableKey() { throw new RuntimeException("Stub!"); }

/**
 * Key for the non indexable data.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_KEY_VALUE = "key";

/** @apiSince REL */

public static final java.lang.String MIME_TYPE = "vnd.android.cursor.dir/non_indexables_key";
}

/**
 * Constants related to a {@link SearchIndexableData}.
 *
 * This is the raw data that is stored into an Index. This is related to
 * {@link android.preference.Preference} and its attributes like
 * {@link android.preference.Preference#getTitle()},
 * {@link android.preference.Preference#getSummary()}, etc.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class RawData extends android.provider.SearchIndexablesContract.BaseColumns {

RawData() { throw new RuntimeException("Stub!"); }

/**
 * Entries associated with the raw data (when the data can have several values).
 * @apiSince REL
 */

public static final java.lang.String COLUMN_ENTRIES = "entries";

/**
 * Key associated with the raw data. The key needs to be unique.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_KEY = "key";

/**
 * Keywords' raw data.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_KEYWORDS = "keywords";

/**
 * Fragment or Activity title associated with the raw data.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_SCREEN_TITLE = "screenTitle";

/**
 * Summary's raw data when the data is "OFF".
 * @apiSince REL
 */

public static final java.lang.String COLUMN_SUMMARY_OFF = "summaryOff";

/**
 * Summary's raw data when the data is "ON".
 * @apiSince REL
 */

public static final java.lang.String COLUMN_SUMMARY_ON = "summaryOn";

/**
 * Title's raw data.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_TITLE = "title";

/**
 * UserId associated with the raw data.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_USER_ID = "user_id";

/** @apiSince REL */

public static final java.lang.String MIME_TYPE = "vnd.android.cursor.dir/indexables_raw";
}

/**
 * Columns for the SliceUri and Preference Key pairs.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SliceUriPairColumns {

SliceUriPairColumns() { throw new RuntimeException("Stub!"); }

/**
 * The preference key for the Setting.
 * @apiSince REL
 */

public static final java.lang.String KEY = "key";

/**
 * The Slice Uri corresponding to the Setting key.
 * @apiSince REL
 */

public static final java.lang.String SLICE_URI = "slice_uri";
}

/**
 * Constants related to a {@link SearchIndexableResource}.
 *
 * This is a description of
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class XmlResource extends android.provider.SearchIndexablesContract.BaseColumns {

XmlResource() { throw new RuntimeException("Stub!"); }

/**
 * XML resource ID for the {@link android.preference.PreferenceScreen} to load and index.
 * @apiSince REL
 */

public static final java.lang.String COLUMN_XML_RESID = "xmlResId";

/** @apiSince REL */

public static final java.lang.String MIME_TYPE = "vnd.android.cursor.dir/indexables_xml_res";
}

}

