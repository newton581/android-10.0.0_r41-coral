/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims;

import android.telecom.Connection;

/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ImsVideoCallProvider {

/** @apiSince REL */

public ImsVideoCallProvider() { throw new RuntimeException("Stub!"); }

/**
 * @see Connection.VideoProvider#onSetCamera
 * @apiSince REL
 */

public abstract void onSetCamera(java.lang.String cameraId);

/**
 * Similar to {@link #onSetCamera(String)}, except includes the UID of the calling process which
 * the IMS service uses when opening the camera.  This ensures camera permissions are verified
 * by the camera service.
 *
 * @param cameraId The id of the camera to be opened.
 * @param uid The uid of the caller, used when opening the camera for permission verification.
 * @see Connection.VideoProvider#onSetCamera
 * @apiSince REL
 */

public void onSetCamera(java.lang.String cameraId, int uid) { throw new RuntimeException("Stub!"); }

/**
 * @see Connection.VideoProvider#onSetPreviewSurface
 * @apiSince REL
 */

public abstract void onSetPreviewSurface(android.view.Surface surface);

/**
 * @see Connection.VideoProvider#onSetDisplaySurface
 * @apiSince REL
 */

public abstract void onSetDisplaySurface(android.view.Surface surface);

/**
 * @see Connection.VideoProvider#onSetDeviceOrientation
 * @apiSince REL
 */

public abstract void onSetDeviceOrientation(int rotation);

/**
 * @see Connection.VideoProvider#onSetZoom
 * @apiSince REL
 */

public abstract void onSetZoom(float value);

/**
 * @see Connection.VideoProvider#onSendSessionModifyRequest
 * @apiSince REL
 */

public abstract void onSendSessionModifyRequest(android.telecom.VideoProfile fromProfile, android.telecom.VideoProfile toProfile);

/**
 * @see Connection.VideoProvider#onSendSessionModifyResponse
 * @apiSince REL
 */

public abstract void onSendSessionModifyResponse(android.telecom.VideoProfile responseProfile);

/**
 * @see Connection.VideoProvider#onRequestCameraCapabilities
 * @apiSince REL
 */

public abstract void onRequestCameraCapabilities();

/**
 * @see Connection.VideoProvider#onRequestCallDataUsage
 * @apiSince REL
 */

public abstract void onRequestCallDataUsage();

/**
 * @see Connection.VideoProvider#onSetPauseImage
 * @apiSince REL
 */

public abstract void onSetPauseImage(android.net.Uri uri);

/**
 * @see Connection.VideoProvider#receiveSessionModifyRequest
 * @apiSince REL
 */

public void receiveSessionModifyRequest(android.telecom.VideoProfile VideoProfile) { throw new RuntimeException("Stub!"); }

/**
 * @see Connection.VideoProvider#receiveSessionModifyResponse
 * @apiSince REL
 */

public void receiveSessionModifyResponse(int status, android.telecom.VideoProfile requestedProfile, android.telecom.VideoProfile responseProfile) { throw new RuntimeException("Stub!"); }

/**
 * @see Connection.VideoProvider#handleCallSessionEvent
 * @apiSince REL
 */

public void handleCallSessionEvent(int event) { throw new RuntimeException("Stub!"); }

/**
 * @see Connection.VideoProvider#changePeerDimensions
 * @apiSince REL
 */

public void changePeerDimensions(int width, int height) { throw new RuntimeException("Stub!"); }

/**
 * @see Connection.VideoProvider#changeCallDataUsage
 * @apiSince REL
 */

public void changeCallDataUsage(long dataUsage) { throw new RuntimeException("Stub!"); }

/**
 * @see Connection.VideoProvider#changeCameraCapabilities
 * @apiSince REL
 */

public void changeCameraCapabilities(android.telecom.VideoProfile.CameraCapabilities CameraCapabilities) { throw new RuntimeException("Stub!"); }

/**
 * @see Connection.VideoProvider#changeVideoQuality
 * @apiSince REL
 */

public void changeVideoQuality(int videoQuality) { throw new RuntimeException("Stub!"); }
}

