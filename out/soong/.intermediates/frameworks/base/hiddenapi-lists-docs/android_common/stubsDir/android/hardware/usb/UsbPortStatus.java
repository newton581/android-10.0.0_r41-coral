/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.usb;


/**
 * Describes the status of a USB port.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class UsbPortStatus implements android.os.Parcelable {

/** @hide */

UsbPortStatus(int currentMode, int currentPowerRole, int currentDataRole, int supportedRoleCombinations, int contaminantProtectionStatus, int contaminantDetectionStatus) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if there is anything connected to the port.
 *
 * @return {@code true} iff there is anything connected to the port.
 * @apiSince REL
 */

public boolean isConnected() { throw new RuntimeException("Stub!"); }

/**
 * Gets the current mode of the port.
 *
 * @return The current mode: {@link #MODE_DFP}, {@link #MODE_UFP},
 * {@link #MODE_AUDIO_ACCESSORY}, {@link #MODE_DEBUG_ACCESSORY}, or {@link {@link #MODE_NONE} if
 * nothing is connected.
 
 * Value is either <code>0</code> or a combination of {@link android.hardware.usb.UsbPortStatus#MODE_NONE}, {@link android.hardware.usb.UsbPortStatus#MODE_DFP}, {@link android.hardware.usb.UsbPortStatus#MODE_UFP}, {@link android.hardware.usb.UsbPortStatus#MODE_AUDIO_ACCESSORY}, and {@link android.hardware.usb.UsbPortStatus#MODE_DEBUG_ACCESSORY}
 * @apiSince REL
 */

public int getCurrentMode() { throw new RuntimeException("Stub!"); }

/**
 * Gets the current power role of the port.
 *
 * @return The current power role: {@link #POWER_ROLE_SOURCE}, {@link #POWER_ROLE_SINK}, or
 * {@link #POWER_ROLE_NONE} if nothing is connected.
 
 * Value is {@link android.hardware.usb.UsbPortStatus#POWER_ROLE_NONE}, {@link android.hardware.usb.UsbPortStatus#POWER_ROLE_SOURCE}, or {@link android.hardware.usb.UsbPortStatus#POWER_ROLE_SINK}
 * @apiSince REL
 */

public int getCurrentPowerRole() { throw new RuntimeException("Stub!"); }

/**
 * Gets the current data role of the port.
 *
 * @return The current data role: {@link #DATA_ROLE_HOST}, {@link #DATA_ROLE_DEVICE}, or
 * {@link #DATA_ROLE_NONE} if nothing is connected.
 
 * Value is {@link android.hardware.usb.UsbPortStatus#DATA_ROLE_NONE}, {@link android.hardware.usb.UsbPortStatus#DATA_ROLE_HOST}, or {@link android.hardware.usb.UsbPortStatus#DATA_ROLE_DEVICE}
 * @apiSince REL
 */

public int getCurrentDataRole() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if the specified power and data role combination is supported
 * given what is currently connected to the port.
 *
 * @param powerRole The power role to check: {@link #POWER_ROLE_SOURCE}  or
 *                  {@link #POWER_ROLE_SINK}, or {@link #POWER_ROLE_NONE} if no power role.
 * Value is {@link android.hardware.usb.UsbPortStatus#POWER_ROLE_NONE}, {@link android.hardware.usb.UsbPortStatus#POWER_ROLE_SOURCE}, or {@link android.hardware.usb.UsbPortStatus#POWER_ROLE_SINK}
 * @param dataRole  The data role to check: either {@link #DATA_ROLE_HOST} or
 *                  {@link #DATA_ROLE_DEVICE}, or {@link #DATA_ROLE_NONE} if no data role.
 
 * Value is {@link android.hardware.usb.UsbPortStatus#DATA_ROLE_NONE}, {@link android.hardware.usb.UsbPortStatus#DATA_ROLE_HOST}, or {@link android.hardware.usb.UsbPortStatus#DATA_ROLE_DEVICE}
 * @apiSince REL
 */

public boolean isRoleCombinationSupported(int powerRole, int dataRole) { throw new RuntimeException("Stub!"); }

/**
 * Get the supported role combinations.
 * @apiSince REL
 */

public int getSupportedRoleCombinations() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.usb.UsbPortStatus> CREATOR;
static { CREATOR = null; }

/**
 * Data role: This USB port can act as a device (offer data services).
 * @apiSince REL
 */

public static final int DATA_ROLE_DEVICE = 2; // 0x2

/**
 * Data role: This USB port can act as a host (access data services).
 * @apiSince REL
 */

public static final int DATA_ROLE_HOST = 1; // 0x1

/**
 * Power role: This USB port does not have a data role.
 * @apiSince REL
 */

public static final int DATA_ROLE_NONE = 0; // 0x0

/**
 * This USB port can support USB Type-C Audio accessory.
 * @apiSince REL
 */

public static final int MODE_AUDIO_ACCESSORY = 4; // 0x4

/**
 * This USB port can support USB Type-C debug accessory.
 * @apiSince REL
 */

public static final int MODE_DEBUG_ACCESSORY = 8; // 0x8

/**
 * This USB port can act as a downstream facing port (host).
 *
 * <p> Implies that the port supports the {@link #POWER_ROLE_SOURCE} and
 * {@link #DATA_ROLE_HOST} combination of roles (and possibly others as well).
 * @apiSince REL
 */

public static final int MODE_DFP = 2; // 0x2

/**
 * There is currently nothing connected to this USB port.
 * @apiSince REL
 */

public static final int MODE_NONE = 0; // 0x0

/**
 * This USB port can act as an upstream facing port (device).
 *
 * <p> Implies that the port supports the {@link #POWER_ROLE_SINK} and
 * {@link #DATA_ROLE_DEVICE} combination of roles (and possibly others as well).
 * @apiSince REL
 */

public static final int MODE_UFP = 1; // 0x1

/**
 * Power role: This USB port does not have a power role.
 * @apiSince REL
 */

public static final int POWER_ROLE_NONE = 0; // 0x0

/**
 * Power role: This USB port can act as a sink (receive power).
 * @apiSince REL
 */

public static final int POWER_ROLE_SINK = 2; // 0x2

/**
 * Power role: This USB port can act as a source (provide power).
 * @apiSince REL
 */

public static final int POWER_ROLE_SOURCE = 1; // 0x1
}

