/**
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.radio;


/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class Announcement implements android.os.Parcelable {

Announcement(@android.annotation.NonNull android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.radio.ProgramSelector getSelector() { throw new RuntimeException("Stub!"); }

/**
 * @return Value is {@link android.hardware.radio.Announcement#TYPE_EMERGENCY}, {@link android.hardware.radio.Announcement#TYPE_WARNING}, {@link android.hardware.radio.Announcement#TYPE_TRAFFIC}, {@link android.hardware.radio.Announcement#TYPE_WEATHER}, {@link android.hardware.radio.Announcement#TYPE_NEWS}, {@link android.hardware.radio.Announcement#TYPE_EVENT}, {@link android.hardware.radio.Announcement#TYPE_SPORT}, or {@link android.hardware.radio.Announcement#TYPE_MISC}
 * @apiSince REL
 */

public int getType() { throw new RuntimeException("Stub!"); }

/**
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.Map<java.lang.String,java.lang.String> getVendorInfo() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.radio.Announcement> CREATOR;
static { CREATOR = null; }

/**
 * DAB alarm, RDS emergency program type (PTY 31).
 * @apiSince REL
 */

public static final int TYPE_EMERGENCY = 1; // 0x1

/**
 * DAB event, special event.
 * @apiSince REL
 */

public static final int TYPE_EVENT = 6; // 0x6

/**
 * All others.
 * @apiSince REL
 */

public static final int TYPE_MISC = 8; // 0x8

/**
 * News.
 * @apiSince REL
 */

public static final int TYPE_NEWS = 5; // 0x5

/**
 * DAB sport report, RDS sports.
 * @apiSince REL
 */

public static final int TYPE_SPORT = 7; // 0x7

/**
 * DAB road traffic, RDS TA, HD Radio transportation.
 * @apiSince REL
 */

public static final int TYPE_TRAFFIC = 3; // 0x3

/**
 * DAB warning.
 * @apiSince REL
 */

public static final int TYPE_WARNING = 2; // 0x2

/**
 * Weather.
 * @apiSince REL
 */

public static final int TYPE_WEATHER = 4; // 0x4
/**
 * Listener of announcement list events.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnListUpdatedListener {

/**
 * An event called whenever a list of active announcements change.
 *
 * The entire list is sent each time a new announcement appears or any ends broadcasting.
 *
 * @param activeAnnouncements a full list of active announcements
 * @apiSince REL
 */

public void onListUpdated(java.util.Collection<android.hardware.radio.Announcement> activeAnnouncements);
}

}

