/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.location;


/**
 * A class that represents an event for each change in the state of a monitoring system.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class GeofenceHardwareMonitorEvent implements android.os.Parcelable {

/** @apiSince REL */

public GeofenceHardwareMonitorEvent(int monitoringType, int monitoringStatus, int sourceTechnologies, android.location.Location location) { throw new RuntimeException("Stub!"); }

/**
 * Returns the type of the monitoring system that has a change on its state.
 * @apiSince REL
 */

public int getMonitoringType() { throw new RuntimeException("Stub!"); }

/**
 * Returns the new status associated with the monitoring system.
 * @apiSince REL
 */

public int getMonitoringStatus() { throw new RuntimeException("Stub!"); }

/**
 * Returns the source technologies that the status is associated to.
 * @apiSince REL
 */

public int getSourceTechnologies() { throw new RuntimeException("Stub!"); }

/**
 * Returns the last known location according to the monitoring system.
 * @apiSince REL
 */

public android.location.Location getLocation() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.location.GeofenceHardwareMonitorEvent> CREATOR;
static { CREATOR = null; }
}

