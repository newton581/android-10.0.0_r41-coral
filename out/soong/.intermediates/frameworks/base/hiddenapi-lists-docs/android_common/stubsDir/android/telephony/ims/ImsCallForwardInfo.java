/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims;


/**
 * Provides the call forward information for the supplementary service configuration.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ImsCallForwardInfo implements android.os.Parcelable {

/**
 * IMS Call Forward Information.
 
 * @param reason Value is {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_UNCONDITIONAL}, {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_BUSY}, {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_NO_REPLY}, {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_NOT_REACHABLE}, {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_ALL}, {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_ALL_CONDITIONAL}, or {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_NOT_LOGGED_IN}
 
 * @param status Value is {@link android.telephony.ims.ImsCallForwardInfo#STATUS_NOT_ACTIVE}, or {@link android.telephony.ims.ImsCallForwardInfo#STATUS_ACTIVE}
 
 * @param toA Value is {@link android.telephony.ims.ImsCallForwardInfo#TYPE_OF_ADDRESS_INTERNATIONAL}, or {@link android.telephony.ims.ImsCallForwardInfo#TYPE_OF_ADDRESS_UNKNOWN}
 
 * @param serviceClass Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_NONE}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_VOICE}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_FAX}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_SMS}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_CIRCUIT_SYNC}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_CIRCUIT_ASYNC}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_PACKET_ACCESS}, and {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_PAD}
 
 * @param number This value must never be {@code null}.
 * @apiSince REL
 */

public ImsCallForwardInfo(int reason, int status, int toA, int serviceClass, @android.annotation.NonNull java.lang.String number, int replyTimerSec) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * @return the condition of call forwarding for the service classes specified.
 
 * Value is {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_UNCONDITIONAL}, {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_BUSY}, {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_NO_REPLY}, {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_NOT_REACHABLE}, {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_ALL}, {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_ALL_CONDITIONAL}, or {@link android.telephony.ims.ImsCallForwardInfo#CDIV_CF_REASON_NOT_LOGGED_IN}
 * @apiSince REL
 */

public int getCondition() { throw new RuntimeException("Stub!"); }

/**
 * @return The call forwarding status.
 
 * Value is {@link android.telephony.ims.ImsCallForwardInfo#STATUS_NOT_ACTIVE}, or {@link android.telephony.ims.ImsCallForwardInfo#STATUS_ACTIVE}
 * @apiSince REL
 */

public int getStatus() { throw new RuntimeException("Stub!"); }

/**
 * @return the type of address (ToA) for the number.
 * Value is {@link android.telephony.ims.ImsCallForwardInfo#TYPE_OF_ADDRESS_INTERNATIONAL}, or {@link android.telephony.ims.ImsCallForwardInfo#TYPE_OF_ADDRESS_UNKNOWN}
 * @see #getNumber()
 * @apiSince REL
 */

public int getToA() { throw new RuntimeException("Stub!"); }

/**
 * @return a bitfield containing the service classes that are enabled for call forwarding.
 
 * Value is either <code>0</code> or a combination of {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_NONE}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_VOICE}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_FAX}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_SMS}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_CIRCUIT_SYNC}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_CIRCUIT_ASYNC}, {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_PACKET_ACCESS}, and {@link android.telephony.ims.ImsSsData#SERVICE_CLASS_DATA_PAD}
 * @apiSince REL
 */

public int getServiceClass() { throw new RuntimeException("Stub!"); }

/**
 * @return the call forwarding number associated with call forwarding, with a number type
 * defined by {@link #getToA()}.
 * @apiSince REL
 */

public java.lang.String getNumber() { throw new RuntimeException("Stub!"); }

/**
 * @return the number in seconds to wait before the call is forwarded for call forwarding
 * condition {@link #CDIV_CF_REASON_NO_REPLY}
 * @apiSince REL
 */

public int getTimeSeconds() { throw new RuntimeException("Stub!"); }

/**
 * CDIV (Communication Diversion, 3GPP TS 24.604) call forwarding reason for setting all call
 * forwarding reasons simultaneously (i.e. unconditional, busy, no reply, and not reachable).
 * @apiSince REL
 */

public static final int CDIV_CF_REASON_ALL = 4; // 0x4

/**
 * CDIV (Communication Diversion, 3GPP TS 24.604) call forwarding reason for setting all
 * conditional call forwarding reasons simultaneously (i.e. if busy, if no reply, and if not
 * reachable).
 * @apiSince REL
 */

public static final int CDIV_CF_REASON_ALL_CONDITIONAL = 5; // 0x5

/**
 * CDIV (Communication Diversion, 3GPP TS 24.604) call forwarding reason for call forwarding
 * when the user is busy.
 * @apiSince REL
 */

public static final int CDIV_CF_REASON_BUSY = 1; // 0x1

/**
 * CDIV (Communication Diversion) IMS only call forwarding reason for call forwarding when the
 * user is not logged in.
 * @apiSince REL
 */

public static final int CDIV_CF_REASON_NOT_LOGGED_IN = 6; // 0x6

/**
 * CDIV (Communication Diversion, 3GPP TS 24.604) call forwarding reason for call forwarding
 * when the user is not reachable.
 * @apiSince REL
 */

public static final int CDIV_CF_REASON_NOT_REACHABLE = 3; // 0x3

/**
 * CDIV (Communication Diversion, 3GPP TS 24.604) call forwarding reason for call forwarding
 * when there is no reply from the user.
 * @apiSince REL
 */

public static final int CDIV_CF_REASON_NO_REPLY = 2; // 0x2

/**
 * CDIV (Communication Diversion, 3GPP TS 24.604) call forwarding reason for unconditional call
 * forwarding. See TC 27.007
 * @apiSince REL
 */

public static final int CDIV_CF_REASON_UNCONDITIONAL = 0; // 0x0

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.ims.ImsCallForwardInfo> CREATOR;
static { CREATOR = null; }

/**
 * Call forwarding is active for one or more service classes.
 * @apiSince REL
 */

public static final int STATUS_ACTIVE = 1; // 0x1

/**
 * Call forwarding is not active for any service class.
 * @apiSince REL
 */

public static final int STATUS_NOT_ACTIVE = 0; // 0x0

/**
 * The address defined in {@link #getNumber()} is in E.164 international format, which includes
 * international access code "+".
 *
 * See TS 27.007, section 7.11 for more information.
 * @apiSince REL
 */

public static final int TYPE_OF_ADDRESS_INTERNATIONAL = 145; // 0x91

/**
 * The address defined in {@link #getNumber()} is in an unknown format.
 *
 * See TS 27.007, section 7.11 for more information.
 * @apiSince REL
 */

public static final int TYPE_OF_ADDRESS_UNKNOWN = 129; // 0x81
}

