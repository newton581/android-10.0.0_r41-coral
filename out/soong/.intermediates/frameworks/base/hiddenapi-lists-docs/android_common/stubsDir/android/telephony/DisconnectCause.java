/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;


/**
 * Describes the cause of a disconnected call. Those disconnect causes can be converted into a more
 * generic {@link android.telecom.DisconnectCause} object.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class DisconnectCause {

/** Private constructor to avoid class instantiation. */

DisconnectCause() { throw new RuntimeException("Stub!"); }

/**
 * Indicates that a new outgoing call cannot be placed because there is already an outgoing
 * call dialing out.
 * @apiSince REL
 */

public static final int ALREADY_DIALING = 72; // 0x48

/**
 * The call was terminated because it was answered on another device.
 * @apiSince REL
 */

public static final int ANSWERED_ELSEWHERE = 52; // 0x34

/**
 * Outgoing call to busy line
 * @apiSince REL
 */

public static final int BUSY = 4; // 0x4

/**
 * Indicates that a new outgoing call cannot be placed because calling has been disabled using
 * the ro.telephony.disable-call system property.
 * @apiSince REL
 */

public static final int CALLING_DISABLED = 74; // 0x4a

/**
 * Call was blocked by call barring
 * @apiSince REL
 */

public static final int CALL_BARRED = 20; // 0x14

/**
 * The call was terminated because it was pulled to another device.
 * @apiSince REL
 */

public static final int CALL_PULLED = 51; // 0x33

/**
 * Indicates that a new outgoing call cannot be placed while there is a ringing call.
 * @apiSince REL
 */

public static final int CANT_CALL_WHILE_RINGING = 73; // 0x49

/**
 * Access Blocked by CDMA network
 * @apiSince REL
 */

public static final int CDMA_ACCESS_BLOCKED = 35; // 0x23

/**
 * Unable to obtain access to the CDMA system
 * @apiSince REL
 */

public static final int CDMA_ACCESS_FAILURE = 32; // 0x20

/**
 * The call was terminated because CDMA phone service and roaming have already been activated.
 * @apiSince REL
 */

public static final int CDMA_ALREADY_ACTIVATED = 49; // 0x31

/**
 * Drop call
 * @apiSince REL
 */

public static final int CDMA_DROP = 27; // 0x1b

/**
 * INTERCEPT order received, MS state idle entered
 * @apiSince REL
 */

public static final int CDMA_INTERCEPT = 28; // 0x1c

/**
 * MS is locked until next power cycle
 * @apiSince REL
 */

public static final int CDMA_LOCKED_UNTIL_POWER_CYCLE = 26; // 0x1a

/**
 * Not an emergency call
 * @apiSince REL
 */

public static final int CDMA_NOT_EMERGENCY = 34; // 0x22

/**
 * Not a preempted call
 * @apiSince REL
 */

public static final int CDMA_PREEMPTED = 33; // 0x21

/**
 * MS has been redirected, call is cancelled
 * @apiSince REL
 */

public static final int CDMA_REORDER = 29; // 0x1d

/**
 * Requested service is rejected, retry delay is set
 * @apiSince REL
 */

public static final int CDMA_RETRY_ORDER = 31; // 0x1f

/**
 * Service option rejection
 * @apiSince REL
 */

public static final int CDMA_SO_REJECT = 30; // 0x1e

/**
 * Outgoing call to congested network
 * @apiSince REL
 */

public static final int CONGESTION = 5; // 0x5

/**
 * Call was blocked by restricted all voice access
 * @apiSince REL
 */

public static final int CS_RESTRICTED = 22; // 0x16

/**
 * Call was blocked by restricted emergency voice access
 * @apiSince REL
 */

public static final int CS_RESTRICTED_EMERGENCY = 24; // 0x18

/**
 * Call was blocked by restricted normal voice access
 * @apiSince REL
 */

public static final int CS_RESTRICTED_NORMAL = 23; // 0x17

/**
 * The call was terminated because cellular data has been disabled.
 * Used when in a video call and the user disables cellular data via the settings.
 * @apiSince REL
 */

public static final int DATA_DISABLED = 54; // 0x36

/**
 * The call was terminated because the data policy has disabled cellular data.
 * Used when in a video call and the user has exceeded the device data limit.
 * @apiSince REL
 */

public static final int DATA_LIMIT_REACHED = 55; // 0x37

/**
 * The call being placed was detected as a call forwarding number and was being dialed while
 * roaming on a carrier that does not allow this.
 * @apiSince REL
 */

public static final int DIALED_CALL_FORWARDING_WHILE_ROAMING = 57; // 0x39

/**
 * Our initial phone number was actually an MMI sequence.
 * @apiSince REL
 */

public static final int DIALED_MMI = 39; // 0x27

/**
 * A call was not dialed because the device's battery is too low.
 * @apiSince REL
 */

public static final int DIAL_LOW_BATTERY = 62; // 0x3e

/**
 * Stk Call Control modified DIAL request to DIAL with modified data.
 * @apiSince REL
 */

public static final int DIAL_MODIFIED_TO_DIAL = 48; // 0x30

/**
 * Stk Call Control modified DIAL request to video DIAL request.
 * @apiSince REL
 */

public static final int DIAL_MODIFIED_TO_DIAL_VIDEO = 66; // 0x42

/**
 * Stk Call Control modified DIAL request to SS request.
 * @apiSince REL
 */

public static final int DIAL_MODIFIED_TO_SS = 47; // 0x2f

/**
 * Stk Call Control modified DIAL request to USSD request.
 * @apiSince REL
 */

public static final int DIAL_MODIFIED_TO_USSD = 46; // 0x2e

/**
 * Stk Call Control modified Video DIAL request to DIAL request.
 * @apiSince REL
 */

public static final int DIAL_VIDEO_MODIFIED_TO_DIAL = 69; // 0x45

/**
 * Stk Call Control modified Video DIAL request to Video DIAL request.
 * @apiSince REL
 */

public static final int DIAL_VIDEO_MODIFIED_TO_DIAL_VIDEO = 70; // 0x46

/**
 * Stk Call Control modified Video DIAL request to SS request.
 * @apiSince REL
 */

public static final int DIAL_VIDEO_MODIFIED_TO_SS = 67; // 0x43

/**
 * Stk Call Control modified Video DIAL request to USSD request.
 * @apiSince REL
 */

public static final int DIAL_VIDEO_MODIFIED_TO_USSD = 68; // 0x44

/**
 * Emergency call failed with a permanent fail cause and should not be redialed on this
 * slot.
 * @apiSince REL
 */

public static final int EMERGENCY_PERM_FAILURE = 64; // 0x40

/**
 * Emergency call failed with a temporary fail cause and can be redialed on this slot.
 * @apiSince REL
 */

public static final int EMERGENCY_TEMP_FAILURE = 63; // 0x3f

/**
 * Unknown error or not specified
 * @apiSince REL
 */

public static final int ERROR_UNSPECIFIED = 36; // 0x24

/**
 * Call was blocked by fixed dial number
 * @apiSince REL
 */

public static final int FDN_BLOCKED = 21; // 0x15

/**
 * No ICC, ICC locked, or other ICC error
 * @apiSince REL
 */

public static final int ICC_ERROR = 19; // 0x13

/**
 * The network does not accept the emergency call request because IMEI was used as
 * identification and this cability is not supported by the network.
 * @apiSince REL
 */

public static final int IMEI_NOT_ACCEPTED = 58; // 0x3a

/**
 * The call has failed because of access class barring.
 * @apiSince REL
 */

public static final int IMS_ACCESS_BLOCKED = 60; // 0x3c

/**
 * The call, which was an IMS call, disconnected because it merged with another call.
 * @apiSince REL
 */

public static final int IMS_MERGED_SUCCESSFULLY = 45; // 0x2d

/**
 * The network has reported that an alternative emergency number has been dialed, but the user
 * must exit airplane mode to place the call.
 * @apiSince REL
 */

public static final int IMS_SIP_ALTERNATE_EMERGENCY_CALL = 71; // 0x47

/**
 * An incoming call that was missed and never answered
 * @apiSince REL
 */

public static final int INCOMING_MISSED = 1; // 0x1

/**
 * An incoming call that was rejected
 * @apiSince REL
 */

public static final int INCOMING_REJECTED = 16; // 0x10

/**
 * Invalid credentials
 * @apiSince REL
 */

public static final int INVALID_CREDENTIALS = 10; // 0xa

/**
 * Invalid dial string
 * @apiSince REL
 */

public static final int INVALID_NUMBER = 7; // 0x7

/**
 * GSM or CDMA ACM limit exceeded
 * @apiSince REL
 */

public static final int LIMIT_EXCEEDED = 15; // 0xf

/**
 * Normal; Local hangup
 * @apiSince REL
 */

public static final int LOCAL = 3; // 0x3

/**
 * Client went out of network range
 * @apiSince REL
 */

public static final int LOST_SIGNAL = 14; // 0xe

/**
 * The call has ended (mid-call) because the device's battery is too low.
 * @apiSince REL
 */

public static final int LOW_BATTERY = 61; // 0x3d

/**
 * The call was terminated because the maximum allowable number of calls has been reached.
 * @apiSince REL
 */

public static final int MAXIMUM_NUMBER_OF_CALLS_REACHED = 53; // 0x35

/**
 * Not presently used
 * @apiSince REL
 */

public static final int MMI = 6; // 0x6

/**
 * Normal; Remote hangup
 * @apiSince REL
 */

public static final int NORMAL = 2; // 0x2

/**
 * This cause is used to report a normal event only when no other cause in the normal class
 * applies.
 * @apiSince REL
 */

public static final int NORMAL_UNSPECIFIED = 65; // 0x41

/**
 * Has not yet disconnected
 * @apiSince REL
 */

public static final int NOT_DISCONNECTED = 0; // 0x0

/**
 * The disconnect cause is not valid (Not received a disconnect cause)
 * @apiSince REL
 */

public static final int NOT_VALID = -1; // 0xffffffff

/**
 * The supplied CALL Intent didn't contain a valid phone number.
 * @apiSince REL
 */

public static final int NO_PHONE_NUMBER_SUPPLIED = 38; // 0x26

/**
 * Cannot reach the peer
 * @apiSince REL
 */

public static final int NUMBER_UNREACHABLE = 8; // 0x8

/**
 * Indicates that a new outgoing call cannot be placed because OTASP provisioning is currently
 * in process.
 * @apiSince REL
 */

public static final int OTASP_PROVISIONING_IN_PROCESS = 76; // 0x4c

/**
 * The outgoing call was canceled by the {@link android.telecom.ConnectionService}.
 * @apiSince REL
 */

public static final int OUTGOING_CANCELED = 44; // 0x2c

/**
 * The outgoing call failed with an unknown cause.
 * @apiSince REL
 */

public static final int OUTGOING_FAILURE = 43; // 0x2b

/**
 * Calling from out of network is not allowed
 * @apiSince REL
 */

public static final int OUT_OF_NETWORK = 11; // 0xb

/**
 * Out of service
 * @apiSince REL
 */

public static final int OUT_OF_SERVICE = 18; // 0x12

/**
 * Radio is turned off explicitly
 * @apiSince REL
 */

public static final int POWER_OFF = 17; // 0x11

/**
 * Server error
 * @apiSince REL
 */

public static final int SERVER_ERROR = 12; // 0xc

/**
 * Cannot reach the server
 * @apiSince REL
 */

public static final int SERVER_UNREACHABLE = 9; // 0x9

/**
 * Client timed out
 * @apiSince REL
 */

public static final int TIMED_OUT = 13; // 0xd

/**
 * Indicates that a new outgoing call cannot be placed because there is currently an ongoing
 * foreground and background call.
 * @apiSince REL
 */

public static final int TOO_MANY_ONGOING_CALLS = 75; // 0x4b

/**
 * Unassigned number
 * @apiSince REL
 */

public static final int UNOBTAINABLE_NUMBER = 25; // 0x19

/**
 * The call was terminated because it is not possible to place a video call while TTY is
 * enabled.
 * @apiSince REL
 */

public static final int VIDEO_CALL_NOT_ALLOWED_WHILE_TTY_ENABLED = 50; // 0x32

/**
 * We tried to call a voicemail: URI but the device has no voicemail number configured.
 * @apiSince REL
 */

public static final int VOICEMAIL_NUMBER_MISSING = 40; // 0x28

/**
 * A call over WIFI was disconnected because the WIFI signal was lost or became too degraded to
 * continue the call.
 * @apiSince REL
 */

public static final int WIFI_LOST = 59; // 0x3b
}

