/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.service.contentsuggestions;

import android.app.contentsuggestions.ContentSuggestionsManager;

/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ContentSuggestionsService extends android.app.Service {

public ContentSuggestionsService() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * If you override this method you <em>must</em> call through to the
 * superclass implementation.
 * @apiSince REL
 */

public void onCreate() { throw new RuntimeException("Stub!"); }

/** @hide */

public final android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Called by the system to provide the snapshot for the task associated with the given
 * {@param taskId}.
 
 * @param contextImage This value may be {@code null}.
 
 * @param extras This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onProcessContextImage(int taskId, @android.annotation.Nullable android.graphics.Bitmap contextImage, @android.annotation.NonNull android.os.Bundle extras);

/**
 * Content selections have been request through {@link ContentSuggestionsManager}, implementer
 * should reply on the callback with selections.
 
 * @param request This value must never be {@code null}.
 
 * @param callback This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onSuggestContentSelections(@android.annotation.NonNull android.app.contentsuggestions.SelectionsRequest request, @android.annotation.NonNull android.app.contentsuggestions.ContentSuggestionsManager.SelectionsCallback callback);

/**
 * Content classifications have been request through {@link ContentSuggestionsManager},
 * implementer should reply on the callback with classifications.
 
 * @param request This value must never be {@code null}.
 
 * @param callback This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onClassifyContentSelections(@android.annotation.NonNull android.app.contentsuggestions.ClassificationsRequest request, @android.annotation.NonNull android.app.contentsuggestions.ContentSuggestionsManager.ClassificationsCallback callback);

/**
 * User interactions have been reported through {@link ContentSuggestionsManager}, implementer
 * should handle those interactions.
 
 * @param requestId This value must never be {@code null}.
 
 * @param interaction This value must never be {@code null}.
 * @apiSince REL
 */

public abstract void onNotifyInteraction(@android.annotation.NonNull java.lang.String requestId, @android.annotation.NonNull android.os.Bundle interaction);

/**
 * The action for the intent used to define the content suggestions service.
 *
 * <p>To be supported, the service must also require the
 * * {@link android.Manifest.permission#BIND_CONTENT_SUGGESTIONS_SERVICE} permission so
 * * that other applications can not abuse it.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.service.contentsuggestions.ContentSuggestionsService";
}

