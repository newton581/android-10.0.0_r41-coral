/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os.health;


/**
 * Constants and stuff for the android.os.health package.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HealthKeys {

public HealthKeys() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int BASE_PACKAGE = 40000; // 0x9c40

/** @apiSince REL */

public static final int BASE_PID = 20000; // 0x4e20

/** @apiSince REL */

public static final int BASE_PROCESS = 30000; // 0x7530

/** @apiSince REL */

public static final int BASE_SERVICE = 50000; // 0xc350

/** @apiSince REL */

public static final int BASE_UID = 10000; // 0x2710

/** @apiSince REL */

public static final int TYPE_COUNT = 5; // 0x5

/** @apiSince REL */

public static final int TYPE_MEASUREMENT = 1; // 0x1

/** @apiSince REL */

public static final int TYPE_MEASUREMENTS = 4; // 0x4

/** @apiSince REL */

public static final int TYPE_STATS = 2; // 0x2

/** @apiSince REL */

public static final int TYPE_TIMER = 0; // 0x0

/** @apiSince REL */

public static final int TYPE_TIMERS = 3; // 0x3

/**
 * No valid key will ever be 0.
 * @apiSince REL
 */

public static final int UNKNOWN_KEY = 0; // 0x0
/**
 * Annotation to mark public static final int fields that are to be used
 * as field keys in HealthStats.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({java.lang.annotation.ElementType.FIELD})
public static @interface Constant {

/**
 * One of the TYPE_* constants above.
 * @apiSince REL
 */

public int type();
}

/**
 * Class to gather the constants defined in a class full of constants and
 * build the key indices used by HealthStatsWriter and HealthStats.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Constants {

/**
 * Pass in a class to gather the public static final int fields that are
 * tagged with the @Constant annotation.
 * @apiSince REL
 */

public Constants(java.lang.Class clazz) { throw new RuntimeException("Stub!"); }

/**
 * Get a string representation of this class. Useful for debugging. It will be the
 * simple name of the class passed in the constructor.
 * @apiSince REL
 */

public java.lang.String getDataType() { throw new RuntimeException("Stub!"); }

/**
 * Return how many keys there are for the given field type.
 *
 * @see TYPE_TIMER
 * @see TYPE_MEASUREMENT
 * @see TYPE_TIMERS
 * @see TYPE_MEASUREMENTS
 * @see TYPE_STATS
 * @apiSince REL
 */

public int getSize(int type) { throw new RuntimeException("Stub!"); }

/**
 * Return the index for the given type and key combination in the array of field
 * keys or values.
 *
 * @see TYPE_TIMER
 * @see TYPE_MEASUREMENT
 * @see TYPE_TIMERS
 * @see TYPE_MEASUREMENTS
 * @see TYPE_STATS
 * @apiSince REL
 */

public int getIndex(int type, int key) { throw new RuntimeException("Stub!"); }

/**
 * Get the array of keys for the given field type.
 * @apiSince REL
 */

public int[] getKeys(int type) { throw new RuntimeException("Stub!"); }
}

}

