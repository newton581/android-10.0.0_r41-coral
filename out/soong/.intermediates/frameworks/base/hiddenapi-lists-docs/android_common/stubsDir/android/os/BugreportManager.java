/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;

import android.content.Context;
import java.util.concurrent.Executor;

/**
 * Class that provides a privileged API to capture and consume bugreports.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class BugreportManager {

BugreportManager() { throw new RuntimeException("Stub!"); }

/**
 * Starts a bugreport.
 *
 * <p>This starts a bugreport in the background. However the call itself can take several
 * seconds to return in the worst case. {@code callback} will receive progress and status
 * updates.
 *
 * <p>The bugreport artifacts will be copied over to the given file descriptors only if the
 * user consents to sharing with the calling app.
 *
 * <p>{@link BugreportManager} takes ownership of {@code bugreportFd} and {@code screenshotFd}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#DUMP}
 * @param bugreportFd file to write the bugreport. This should be opened in write-only,
 *     append mode.
 * This value must never be {@code null}.
 * @param screenshotFd file to write the screenshot, if necessary. This should be opened
 *     in write-only, append mode.
 * This value may be {@code null}.
 * @param params options that specify what kind of a bugreport should be taken
 * This value must never be {@code null}.
 * @param callback callback for progress and status updates
 
 * This value must never be {@code null}.
 * @param executor This value must never be {@code null}.
 
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @apiSince REL
 */

public void startBugreport(@android.annotation.NonNull android.os.ParcelFileDescriptor bugreportFd, @android.annotation.Nullable android.os.ParcelFileDescriptor screenshotFd, @android.annotation.NonNull android.os.BugreportParams params, @android.annotation.NonNull java.util.concurrent.Executor executor, @android.annotation.NonNull android.os.BugreportManager.BugreportCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Requires {@link android.Manifest.permission#DUMP}
 * @apiSince REL
 */

public void cancelBugreport() { throw new RuntimeException("Stub!"); }
/**
 * An interface describing the callback for bugreport progress and status.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class BugreportCallback {

public BugreportCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called when there is a progress update.
 * @param progress the progress in [0.0, 100.0]
 
 * Value is between 0f and 100f inclusive
 * @apiSince REL
 */

public void onProgress(float progress) { throw new RuntimeException("Stub!"); }

/**
 * Called when taking bugreport resulted in an error.
 *
 * <p>If {@code BUGREPORT_ERROR_USER_DENIED_CONSENT} is passed, then the user did not
 * consent to sharing the bugreport with the calling app.
 *
 * <p>If {@code BUGREPORT_ERROR_USER_CONSENT_TIMED_OUT} is passed, then the consent timed
 * out, but the bugreport could be available in the internal directory of dumpstate for
 * manual retrieval.
 *
 * <p> If {@code BUGREPORT_ERROR_ANOTHER_REPORT_IN_PROGRESS} is passed, then the
 * caller should try later, as only one bugreport can be in progress at a time.
 
 * @param errorCode Value is {@link android.os.BugreportManager.BugreportCallback#BUGREPORT_ERROR_INVALID_INPUT}, {@link android.os.BugreportManager.BugreportCallback#BUGREPORT_ERROR_RUNTIME}, {@link android.os.BugreportManager.BugreportCallback#BUGREPORT_ERROR_USER_DENIED_CONSENT}, {@link android.os.BugreportManager.BugreportCallback#BUGREPORT_ERROR_USER_CONSENT_TIMED_OUT}, or {@link android.os.BugreportManager.BugreportCallback#BUGREPORT_ERROR_ANOTHER_REPORT_IN_PROGRESS}
 * @apiSince REL
 */

public void onError(int errorCode) { throw new RuntimeException("Stub!"); }

/**
 * Called when taking bugreport finishes successfully.
 * @apiSince REL
 */

public void onFinished() { throw new RuntimeException("Stub!"); }

/**
 * There is currently a bugreport running. The caller should try again later.
 * @apiSince REL
 */

public static final int BUGREPORT_ERROR_ANOTHER_REPORT_IN_PROGRESS = 5; // 0x5

/**
 * The input options were invalid
 * @apiSince REL
 */

public static final int BUGREPORT_ERROR_INVALID_INPUT = 1; // 0x1

/**
 * A runtime error occured
 * @apiSince REL
 */

public static final int BUGREPORT_ERROR_RUNTIME = 2; // 0x2

/**
 * The request to get user consent timed out.
 * @apiSince REL
 */

public static final int BUGREPORT_ERROR_USER_CONSENT_TIMED_OUT = 4; // 0x4

/**
 * User denied consent to share the bugreport
 * @apiSince REL
 */

public static final int BUGREPORT_ERROR_USER_DENIED_CONSENT = 3; // 0x3
}

}

