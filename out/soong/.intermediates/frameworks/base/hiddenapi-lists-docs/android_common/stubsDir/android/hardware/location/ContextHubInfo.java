/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.location;


/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ContextHubInfo implements android.os.Parcelable {

/** @apiSince REL */

public ContextHubInfo() { throw new RuntimeException("Stub!"); }

/**
 * returns the maximum number of bytes that can be sent per message to the hub
 *
 * @return int - maximum bytes that can be transmitted in a
 *         single packet
 * @apiSince REL
 */

public int getMaxPacketLengthBytes() { throw new RuntimeException("Stub!"); }

/**
 * get the context hub unique identifer
 *
 * @return int - unique system wide identifier
 * @apiSince REL
 */

public int getId() { throw new RuntimeException("Stub!"); }

/**
 * get a string as a hub name
 *
 * @return String - a name for the hub
 * @apiSince REL
 */

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

/**
 * get a string as the vendor name
 *
 * @return String - a name for the vendor
 * @apiSince REL
 */

public java.lang.String getVendor() { throw new RuntimeException("Stub!"); }

/**
 * get tool chain string
 *
 * @return String - description of the tool chain
 * @apiSince REL
 */

public java.lang.String getToolchain() { throw new RuntimeException("Stub!"); }

/**
 * get platform version
 *
 * @return int - platform version number
 * @apiSince REL
 */

public int getPlatformVersion() { throw new RuntimeException("Stub!"); }

/**
 * get static platform version number
 *
 * @return int - platform version number
 * @apiSince REL
 */

public int getStaticSwVersion() { throw new RuntimeException("Stub!"); }

/**
 * get the tool chain version
 *
 * @return int - the tool chain version
 * @apiSince REL
 */

public int getToolchainVersion() { throw new RuntimeException("Stub!"); }

/**
 * get the peak processing mips the hub can support
 *
 * @return float - peak MIPS that this hub can deliver
 * @apiSince REL
 */

public float getPeakMips() { throw new RuntimeException("Stub!"); }

/**
 * get the stopped power draw in milliwatts
 * This assumes that the hub enter a stopped state - which is
 * different from the sleep state. Latencies on exiting the
 * sleep state are typically higher and expect to be in multiple
 * milliseconds.
 *
 * @return float - power draw by the hub in stopped state
 * @apiSince REL
 */

public float getStoppedPowerDrawMw() { throw new RuntimeException("Stub!"); }

/**
 * get the power draw of the hub in sleep mode. This assumes
 * that the hub supports a sleep mode in which the power draw is
 * lower than the power consumed when the hub is actively
 * processing. As a guideline, assume that the hub should be
 * able to enter sleep mode if it knows reliably on completion
 * of some task that the next interrupt/scheduled work item is
 * at least 250 milliseconds later.
 *
 * @return float - sleep power draw in milli watts
 * @apiSince REL
 */

public float getSleepPowerDrawMw() { throw new RuntimeException("Stub!"); }

/**
 * get the peak powe draw of the hub. This is the power consumed
 * by the hub at maximum load.
 *
 * @return float - peak power draw
 * @apiSince REL
 */

public float getPeakPowerDrawMw() { throw new RuntimeException("Stub!"); }

/**
 * get the sensors supported by this hub
 *
 * @return int[] - all the supported sensors on this hub
 *
 * @see ContextHubManager
 * @apiSince REL
 */

public int[] getSupportedSensors() { throw new RuntimeException("Stub!"); }

/**
 * get the various memory regions on this hub
 *
 * @return MemoryRegion[] - all the memory regions on this hub
 *
 * @see MemoryRegion
 * @apiSince REL
 */

public android.hardware.location.MemoryRegion[] getMemoryRegions() { throw new RuntimeException("Stub!"); }

/**
 * @return the CHRE platform ID as defined in chre/version.h
 * @apiSince REL
 */

public long getChrePlatformId() { throw new RuntimeException("Stub!"); }

/**
 * @return the CHRE API's major version as defined in chre/version.h
 * @apiSince REL
 */

public byte getChreApiMajorVersion() { throw new RuntimeException("Stub!"); }

/**
 * @return the CHRE API's minor version as defined in chre/version.h
 * @apiSince REL
 */

public byte getChreApiMinorVersion() { throw new RuntimeException("Stub!"); }

/**
 * @return the CHRE patch version as defined in chre/version.h
 * @apiSince REL
 */

public short getChrePatchVersion() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param object This value may be {@code null}.
 * @apiSince REL
 */

public boolean equals(@android.annotation.Nullable java.lang.Object object) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.location.ContextHubInfo> CREATOR;
static { CREATOR = null; }
}

