/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.util.proto;


/**
 * A stream of bytes containing a read pointer and a write pointer,
 * backed by a set of fixed-size buffers.  There are write functions for the
 * primitive types stored by protocol buffers, but none of the logic
 * for tags, inner objects, or any of that.
 *
 * Terminology:
 *      *Pos:       Position in the whole data set (as if it were a single buffer).
 *      *Index:     Position within a buffer.
 *      *BufIndex:  Index of a buffer within the mBuffers list
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class EncodedBuffer {

/** @apiSince REL */

public EncodedBuffer() { throw new RuntimeException("Stub!"); }

/**
 * Construct an EncodedBuffer object.
 *
 * @param chunkSize The size of the buffers to use.  If chunkSize &lt;= 0, a default
 *                  size will be used instead.
 * @apiSince REL
 */

public EncodedBuffer(int chunkSize) { throw new RuntimeException("Stub!"); }

/**
 * Rewind the read and write pointers, and record how much data was last written.
 * @apiSince REL
 */

public void startEditing() { throw new RuntimeException("Stub!"); }

/**
 * Rewind the read pointer. Don't touch the write pointer.
 * @apiSince REL
 */

public void rewindRead() { throw new RuntimeException("Stub!"); }

/**
 * Only valid after startEditing. Returns -1 before that.
 * @apiSince REL
 */

public int getReadableSize() { throw new RuntimeException("Stub!"); }

/**
 * Returns the buffer size
 * @return the buffer size
 * @apiSince REL
 */

public int getSize() { throw new RuntimeException("Stub!"); }

/**
 * Only valid after startEditing.
 * @apiSince REL
 */

public int getReadPos() { throw new RuntimeException("Stub!"); }

/**
 * Skip over _amount_ bytes.
 * @apiSince REL
 */

public void skipRead(int amount) { throw new RuntimeException("Stub!"); }

/**
 * Read one byte from the stream and advance the read pointer.
 *
 * @throws IndexOutOfBoundsException if the read point is past the end of
 * the buffer or past the read limit previously set by startEditing().
 * @apiSince REL
 */

public byte readRawByte() { throw new RuntimeException("Stub!"); }

/**
 * Read an unsigned varint. The value will be returend in a java signed long.
 * @apiSince REL
 */

public long readRawUnsigned() { throw new RuntimeException("Stub!"); }

/**
 * Read 32 little endian bits from the stream.
 * @apiSince REL
 */

public int readRawFixed32() { throw new RuntimeException("Stub!"); }

/**
 * Write a single byte to the stream.
 * @apiSince REL
 */

public void writeRawByte(byte val) { throw new RuntimeException("Stub!"); }

/**
 * Return how many bytes a 32 bit unsigned varint will take when written to the stream.
 * @apiSince REL
 */

public static int getRawVarint32Size(int val) { throw new RuntimeException("Stub!"); }

/**
 * Write an unsigned varint to the stream. A signed value would need to take 10 bytes.
 *
 * @param val treated as unsigned.
 * @apiSince REL
 */

public void writeRawVarint32(int val) { throw new RuntimeException("Stub!"); }

/**
 * Return how many bytes a 32 bit signed zig zag value will take when written to the stream.
 * @apiSince REL
 */

public static int getRawZigZag32Size(int val) { throw new RuntimeException("Stub!"); }

/**
 *  Write a zig-zag encoded value.
 *
 *  @param val treated as signed
 * @apiSince REL
 */

public void writeRawZigZag32(int val) { throw new RuntimeException("Stub!"); }

/**
 * Return how many bytes a 64 bit varint will take when written to the stream.
 * @apiSince REL
 */

public static int getRawVarint64Size(long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a 64 bit varint to the stream.
 * @apiSince REL
 */

public void writeRawVarint64(long val) { throw new RuntimeException("Stub!"); }

/**
 * Return how many bytes a signed 64 bit zig zag value will take when written to the stream.
 * @apiSince REL
 */

public static int getRawZigZag64Size(long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a 64 bit signed zig zag value to the stream.
 * @apiSince REL
 */

public void writeRawZigZag64(long val) { throw new RuntimeException("Stub!"); }

/**
 * Write 4 little endian bytes to the stream.
 * @apiSince REL
 */

public void writeRawFixed32(int val) { throw new RuntimeException("Stub!"); }

/**
 * Write 8 little endian bytes to the stream.
 * @apiSince REL
 */

public void writeRawFixed64(long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a buffer to the stream. Writes nothing if val is null or zero-length.
 * @apiSince REL
 */

public void writeRawBuffer(byte[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write part of an array of bytes.
 * @apiSince REL
 */

public void writeRawBuffer(byte[] val, int offset, int length) { throw new RuntimeException("Stub!"); }

/**
 * Copies data _size_ bytes of data within this buffer from _srcOffset_
 * to the current write position. Like memmov but handles the chunked buffer.
 * @apiSince REL
 */

public void writeFromThisBuffer(int srcOffset, int size) { throw new RuntimeException("Stub!"); }

/**
 * Returns the index into the virtual array of the write pointer.
 * @apiSince REL
 */

public int getWritePos() { throw new RuntimeException("Stub!"); }

/**
 * Resets the write pointer to a virtual location as returned by getWritePos.
 * @apiSince REL
 */

public void rewindWriteTo(int writePos) { throw new RuntimeException("Stub!"); }

/**
 * Read a 32 bit value from the stream.
 *
 * Doesn't touch or affect mWritePos.
 * @apiSince REL
 */

public int getRawFixed32At(int pos) { throw new RuntimeException("Stub!"); }

/**
 * Overwrite a 32 bit value in the stream.
 *
 * Doesn't touch or affect mWritePos.
 * @apiSince REL
 */

public void editRawFixed32(int pos, int val) { throw new RuntimeException("Stub!"); }

/**
 * Get a copy of the first _size_ bytes of data. This is not range
 * checked, and if the bounds are outside what has been written you will
 * get garbage and if it is outside the buffers that have been allocated,
 * you will get an exception.
 * @apiSince REL
 */

public byte[] getBytes(int size) { throw new RuntimeException("Stub!"); }

/**
 * Get the number of chunks allocated.
 * @apiSince REL
 */

public int getChunkCount() { throw new RuntimeException("Stub!"); }

/**
 * Get the write position inside the current write chunk.
 * @apiSince REL
 */

public int getWriteIndex() { throw new RuntimeException("Stub!"); }

/**
 * Get the index of the current write chunk in the list of chunks.
 * @apiSince REL
 */

public int getWriteBufIndex() { throw new RuntimeException("Stub!"); }

/**
 * Return debugging information about this EncodedBuffer object.
 * @apiSince REL
 */

public java.lang.String getDebugString() { throw new RuntimeException("Stub!"); }

/**
 * Print the internal buffer chunks.
 * @apiSince REL
 */

public void dumpBuffers(java.lang.String tag) { throw new RuntimeException("Stub!"); }

/**
 * Print the internal buffer chunks.
 * @apiSince REL
 */

public static void dumpByteString(java.lang.String tag, java.lang.String prefix, byte[] buf) { throw new RuntimeException("Stub!"); }
}

