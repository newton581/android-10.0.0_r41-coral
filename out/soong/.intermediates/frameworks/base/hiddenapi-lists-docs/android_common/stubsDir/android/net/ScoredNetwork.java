/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.net;


/**
 * A network identifier along with a score for the quality of that network.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ScoredNetwork implements android.os.Parcelable {

/**
 * Construct a new {@link ScoredNetwork}.
 *
 * @param networkKey the {@link NetworkKey} uniquely identifying this network.
 * @param rssiCurve the {@link RssiCurve} representing the scores for this network based on the
 *     RSSI. This field is optional, and may be skipped to represent a network which the scorer
 *     has opted not to score at this time. Passing a null value here is strongly preferred to
 *     not returning any {@link ScoredNetwork} for a given {@link NetworkKey} because it
 *     indicates to the system not to request scores for this network in the future, although
 *     the scorer may choose to issue an out-of-band update at any time.
 * @apiSince REL
 */

public ScoredNetwork(android.net.NetworkKey networkKey, android.net.RssiCurve rssiCurve) { throw new RuntimeException("Stub!"); }

/**
 * Construct a new {@link ScoredNetwork}.
 *
 * @param networkKey the {@link NetworkKey} uniquely identifying this network.
 * @param rssiCurve the {@link RssiCurve} representing the scores for this network based on the
 *     RSSI. This field is optional, and may be skipped to represent a network which the scorer
 *     has opted not to score at this time. Passing a null value here is strongly preferred to
 *     not returning any {@link ScoredNetwork} for a given {@link NetworkKey} because it
 *     indicates to the system not to request scores for this network in the future, although
 *     the scorer may choose to issue an out-of-band update at any time.
 * @param meteredHint A boolean value indicating whether or not the network is believed to be
 *     metered.
 * @apiSince REL
 */

public ScoredNetwork(android.net.NetworkKey networkKey, android.net.RssiCurve rssiCurve, boolean meteredHint) { throw new RuntimeException("Stub!"); }

/**
 * Construct a new {@link ScoredNetwork}.
 *
 * @param networkKey the {@link NetworkKey} uniquely identifying this network
 * @param rssiCurve the {@link RssiCurve} representing the scores for this network based on the
 *     RSSI. This field is optional, and may be skipped to represent a network which the scorer
 *     has opted not to score at this time. Passing a null value here is strongly preferred to
 *     not returning any {@link ScoredNetwork} for a given {@link NetworkKey} because it
 *     indicates to the system not to request scores for this network in the future, although
 *     the scorer may choose to issue an out-of-band update at any time.
 * @param meteredHint a boolean value indicating whether or not the network is believed to be
 *                    metered
 * @param attributes optional provider specific attributes
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public ScoredNetwork(android.net.NetworkKey networkKey, android.net.RssiCurve rssiCurve, boolean meteredHint, @android.annotation.Nullable android.os.Bundle attributes) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Return the {@link NetworkBadging.Badging} enum for this network for the given RSSI, derived from the
 * badging curve.
 *
 * <p>If no badging curve is present, {@link #BADGE_NONE} will be returned.
 *
 * @param rssi The rssi level for which the badge should be calculated
 
 * @return Value is android.net.NetworkBadging.BADGING_NONE, android.net.NetworkBadging.BADGING_SD, android.net.NetworkBadging.BADGING_HD, or android.net.NetworkBadging.BADGING_4K
 * @apiSince REL
 */

public int calculateBadge(int rssi) { throw new RuntimeException("Stub!"); }

/**
 * Key used with the {@link #attributes} bundle to define the badging curve.
 *
 * <p>The badging curve is a {@link RssiCurve} used to map different RSSI values to {@link
 * NetworkBadging.Badging} enums.
 * @apiSince REL
 */

public static final java.lang.String ATTRIBUTES_KEY_BADGING_CURVE = "android.net.attributes.key.BADGING_CURVE";

/**
 * Extra used with {@link #attributes} to specify whether the
 * network is believed to have a captive portal.
 * <p>
 * This data may be used, for example, to display a visual indicator
 * in a network selection list.
 * <p>
 * Note that the this extra conveys the possible presence of a
 * captive portal, not its state or the user's ability to open
 * the portal.
 * <p>
 * If no value is associated with this key then it's unknown.
 * @apiSince REL
 */

public static final java.lang.String ATTRIBUTES_KEY_HAS_CAPTIVE_PORTAL = "android.net.attributes.key.HAS_CAPTIVE_PORTAL";

/**
 * Key used with the {@link #attributes} bundle to define the rankingScoreOffset int value.
 *
 * <p>The rankingScoreOffset is used when calculating the ranking score used to rank networks
 * against one another. See {@link #calculateRankingScore} for more information.
 * @apiSince REL
 */

public static final java.lang.String ATTRIBUTES_KEY_RANKING_SCORE_OFFSET = "android.net.attributes.key.RANKING_SCORE_OFFSET";

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.net.ScoredNetwork> CREATOR;
static { CREATOR = null; }

/**
 * An additional collection of optional attributes set by
 * the Network Recommendation Provider.
 *
 * @see #ATTRIBUTES_KEY_HAS_CAPTIVE_PORTAL
 * @see #ATTRIBUTES_KEY_RANKING_SCORE_OFFSET
 * @apiSince REL
 */

@android.annotation.Nullable public final android.os.Bundle attributes;
{ attributes = null; }

/**
 * A boolean value that indicates whether or not the network is believed to be metered.
 *
 * <p>A network can be classified as metered if the user would be
 * sensitive to heavy data usage on that connection due to monetary costs,
 * data limitations or battery/performance issues. A typical example would
 * be a wifi connection where the user would be charged for usage.
 * @apiSince REL
 */

public final boolean meteredHint;
{ meteredHint = false; }

/**
 * A {@link NetworkKey} uniquely identifying this network.
 * @apiSince REL
 */

public final android.net.NetworkKey networkKey;
{ networkKey = null; }

/**
 * The {@link RssiCurve} representing the scores for this network based on the RSSI.
 *
 * <p>This field is optional and may be set to null to indicate that no score is available for
 * this network at this time. Such networks, along with networks for which the scorer has not
 * responded, are always prioritized below scored networks, regardless of the score.
 * @apiSince REL
 */

public final android.net.RssiCurve rssiCurve;
{ rssiCurve = null; }
}

