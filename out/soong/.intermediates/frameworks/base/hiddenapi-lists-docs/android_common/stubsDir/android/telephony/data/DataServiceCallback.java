/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony.data;

import android.telephony.data.DataService.DataServiceProvider;
import android.net.LinkProperties;
import java.util.List;

/**
 * Data service callback, which is for bound data service to invoke for solicited and unsolicited
 * response. The caller is responsible to create a callback object for each single asynchronous
 * request.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DataServiceCallback {

DataServiceCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called to indicate result for the request {@link DataServiceProvider#setupDataCall(int,
 * DataProfile, boolean, boolean, int, LinkProperties, DataServiceCallback)} .
 *
 * @param result The result code. Must be one of the {@link ResultCode}.
 * Value is {@link android.telephony.data.DataServiceCallback#RESULT_SUCCESS}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_UNSUPPORTED}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_INVALID_ARG}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_BUSY}, or {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_ILLEGAL_STATE}
 * @param response Setup data call response.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void onSetupDataCallComplete(int result, @android.annotation.Nullable android.telephony.data.DataCallResponse response) { throw new RuntimeException("Stub!"); }

/**
 * Called to indicate result for the request {@link DataServiceProvider#deactivateDataCall(int,
 * int, DataServiceCallback)}
 *
 * @param result The result code. Must be one of the {@link ResultCode}.
 
 * Value is {@link android.telephony.data.DataServiceCallback#RESULT_SUCCESS}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_UNSUPPORTED}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_INVALID_ARG}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_BUSY}, or {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_ILLEGAL_STATE}
 * @apiSince REL
 */

public void onDeactivateDataCallComplete(int result) { throw new RuntimeException("Stub!"); }

/**
 * Called to indicate result for the request {@link DataServiceProvider#setInitialAttachApn(
 * DataProfile, boolean, DataServiceCallback)}.
 *
 * @param result The result code. Must be one of the {@link ResultCode}.
 
 * Value is {@link android.telephony.data.DataServiceCallback#RESULT_SUCCESS}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_UNSUPPORTED}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_INVALID_ARG}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_BUSY}, or {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_ILLEGAL_STATE}
 * @apiSince REL
 */

public void onSetInitialAttachApnComplete(int result) { throw new RuntimeException("Stub!"); }

/**
 * Called to indicate result for the request {@link DataServiceProvider#setDataProfile(List,
 * boolean, DataServiceCallback)}.
 *
 * @param result The result code. Must be one of the {@link ResultCode}.
 
 * Value is {@link android.telephony.data.DataServiceCallback#RESULT_SUCCESS}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_UNSUPPORTED}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_INVALID_ARG}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_BUSY}, or {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_ILLEGAL_STATE}
 * @apiSince REL
 */

public void onSetDataProfileComplete(int result) { throw new RuntimeException("Stub!"); }

/**
 * Called to indicate result for the request {@link DataServiceProvider#requestDataCallList(
 * DataServiceCallback)}.
 *
 * @param result The result code. Must be one of the {@link ResultCode}.
 * Value is {@link android.telephony.data.DataServiceCallback#RESULT_SUCCESS}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_UNSUPPORTED}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_INVALID_ARG}, {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_BUSY}, or {@link android.telephony.data.DataServiceCallback#RESULT_ERROR_ILLEGAL_STATE}
 * @param dataCallList List of the current active data connection. If no data call is presented,
 * set it to an empty list.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onRequestDataCallListComplete(int result, @android.annotation.NonNull java.util.List<android.telephony.data.DataCallResponse> dataCallList) { throw new RuntimeException("Stub!"); }

/**
 * Called to indicate that data connection list changed. If no data call is presented, set it to
 * an empty list.
 *
 * @param dataCallList List of the current active data connection.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onDataCallListChanged(@android.annotation.NonNull java.util.List<android.telephony.data.DataCallResponse> dataCallList) { throw new RuntimeException("Stub!"); }

/**
 * Service is busy
 * @apiSince REL
 */

public static final int RESULT_ERROR_BUSY = 3; // 0x3

/**
 * Request sent in illegal state
 * @apiSince REL
 */

public static final int RESULT_ERROR_ILLEGAL_STATE = 4; // 0x4

/**
 * Request contains invalid arguments
 * @apiSince REL
 */

public static final int RESULT_ERROR_INVALID_ARG = 2; // 0x2

/**
 * Request is not support
 * @apiSince REL
 */

public static final int RESULT_ERROR_UNSUPPORTED = 1; // 0x1

/**
 * Request is completed successfully
 * @apiSince REL
 */

public static final int RESULT_SUCCESS = 0; // 0x0
}

