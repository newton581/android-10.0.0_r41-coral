/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.pm.permission;


/**
 * This class contains information about how a runtime permission
 * is to be presented in the UI. A single runtime permission
 * presented to the user may correspond to multiple platform defined
 * permissions, e.g. the location permission may control both the
 * coarse and fine platform permissions.
 *
 * @hide
 *
 * @deprecated Not used anymore. Use {@link android.permission.RuntimePermissionPresentationInfo}
 * instead
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public final class RuntimePermissionPresentationInfo implements android.os.Parcelable {

/**
 * Creates a new instance.
 *
 * @param label The permission label.
 * @param granted Whether the permission is granted.
 * @param standard Whether this is a platform-defined permission.
 * @apiSince REL
 */

@Deprecated
public RuntimePermissionPresentationInfo(java.lang.CharSequence label, boolean granted, boolean standard) { throw new RuntimeException("Stub!"); }

/**
 * @return Whether the permission is granted.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean isGranted() { throw new RuntimeException("Stub!"); }

/**
 * @return Whether the permission is platform-defined.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public boolean isStandard() { throw new RuntimeException("Stub!"); }

/**
 * Gets the permission label.
 *
 * @return The label.
 
 * This value will never be {@code null}.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
@android.annotation.NonNull
public java.lang.CharSequence getLabel() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated @android.annotation.NonNull public static final android.os.Parcelable.Creator<android.content.pm.permission.RuntimePermissionPresentationInfo> CREATOR;
static { CREATOR = null; }
}

