/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.location;


/**
 * This class handles geofences managed by various hardware subsystems. It contains
 * the public APIs that is needed to accomplish the task.
 *
 * <p>The APIs should not be called directly by the app developers. A higher level api
 * which abstracts the hardware should be used instead. All the checks are done by the higher
 * level public API. Any needed locking should be handled by the higher level API.
 *
 * <p> There are 3 states associated with a Geofence: Inside, Outside, Unknown.
 * There are 3 transitions: {@link #GEOFENCE_ENTERED}, {@link #GEOFENCE_EXITED},
 * {@link #GEOFENCE_UNCERTAIN}. The APIs only expose the transitions.
 *
 * <p> Inside state: The hardware subsystem is reasonably confident that the user is inside
 * the geofence. Outside state: The hardware subsystem is reasonably confident that the user
 * is outside the geofence Unknown state: Unknown state can be interpreted as a state in which the
 * monitoring subsystem isn't confident enough that the user is either inside or
 * outside the Geofence. If the accuracy does not improve for a sufficient period of time,
 * the {@link #GEOFENCE_UNCERTAIN} transition would be triggered. If the accuracy improves later,
 * an appropriate transition would be triggered. The "reasonably confident" parameter
 * depends on the hardware system and the positioning algorithms used.
 * For instance, {@link #MONITORING_TYPE_GPS_HARDWARE} uses 95% as a confidence level.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class GeofenceHardware {

GeofenceHardware() { throw new RuntimeException("Stub!"); }

/**
 * Returns all the hardware geofence monitoring systems which are supported
 *
 * <p> Call {@link #getStatusOfMonitoringType(int)} to know the current state
 * of a monitoring system.
 *
 * <p> Requires {@link android.Manifest.permission#LOCATION_HARDWARE} permission to access
 * geofencing in hardware.
 *
 * @return An array of all the monitoring types.
 *         An array of length 0 is returned in case of errors.
 * @apiSince 18
 */

public int[] getMonitoringTypes() { throw new RuntimeException("Stub!"); }

/**
 * Returns current status of a hardware geofence monitoring system.
 *
 * <p>Status can be one of {@link #MONITOR_CURRENTLY_AVAILABLE},
 * {@link #MONITOR_CURRENTLY_UNAVAILABLE} or {@link #MONITOR_UNSUPPORTED}
 *
 * <p> Some supported hardware monitoring systems might not be available
 * for monitoring geofences in certain scenarios. For example, when a user
 * enters a building, the GPS hardware subsystem might not be able monitor
 * geofences and will change from {@link #MONITOR_CURRENTLY_AVAILABLE} to
 * {@link #MONITOR_CURRENTLY_UNAVAILABLE}.
 *
 * @param monitoringType
 * @return Current status of the monitoring type.
 * @apiSince 18
 */

public int getStatusOfMonitoringType(int monitoringType) { throw new RuntimeException("Stub!"); }

/**
 * Creates a circular geofence which is monitored by subsystems in the hardware.
 *
 * <p> When the device detects that is has entered, exited or is uncertain
 * about the area specified by the geofence, the given callback will be called.
 *
 * <p> If this call returns true, it means that the geofence has been sent to the hardware.
 * {@link GeofenceHardwareCallback#onGeofenceAdd} will be called with the result of the
 * add call from the hardware. The {@link GeofenceHardwareCallback#onGeofenceAdd} will be
 * called with the following parameters when a transition event occurs.
 * <ul>
 * <li> The geofence Id
 * <li> The location object indicating the last known location.
 * <li> The transition associated with the geofence. One of
 *      {@link #GEOFENCE_ENTERED}, {@link #GEOFENCE_EXITED}, {@link #GEOFENCE_UNCERTAIN}
 * <li> The timestamp when the geofence transition occured.
 * <li> The monitoring type ({@link #MONITORING_TYPE_GPS_HARDWARE} is one such example)
 *      that was used.
 * </ul>
 *
 * <p> The geofence will be monitored by the subsystem specified by monitoring_type parameter.
 * The application does not need to hold a wakelock when the monitoring
 * is being done by the underlying hardware subsystem. If the same geofence Id is being
 * monitored by two different monitoring systems, the same id can be used for both calls, as
 * long as the same callback object is used.
 *
 * <p> Requires {@link android.Manifest.permission#ACCESS_FINE_LOCATION} permission when
 * {@link #MONITORING_TYPE_GPS_HARDWARE} is used.
 *
 * <p> Requires {@link android.Manifest.permission#LOCATION_HARDWARE} permission to access
 * geofencing in hardware.
 *
 * <p>This API should not be called directly by the app developers. A higher level api
 * which abstracts the hardware should be used instead. All the checks are done by the higher
 * level public API. Any needed locking should be handled by the higher level API.
 *
 * <p> Create a geofence request object using the methods in {@link GeofenceHardwareRequest} to
 * set all the characteristics of the geofence. Use the created GeofenceHardwareRequest object
 * in this call.
 *
 * @param geofenceId The id associated with the geofence.
 * @param monitoringType The type of the hardware subsystem that should be used
 *        to monitor the geofence.
 * @param geofenceRequest The {@link GeofenceHardwareRequest} object associated with the
 *        geofence.
 * @param callback {@link GeofenceHardwareCallback} that will be use to notify the
 *        transition.
 * @return true when the geofence is successfully sent to the hardware for addition.
 * @throws IllegalArgumentException when the geofence request type is not supported.
 * @apiSince 18
 */

public boolean addGeofence(int geofenceId, int monitoringType, android.hardware.location.GeofenceHardwareRequest geofenceRequest, android.hardware.location.GeofenceHardwareCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Removes a geofence added by {@link #addGeofence} call.
 *
 * <p> If this call returns true, it means that the geofence has been sent to the hardware.
 * {@link GeofenceHardwareCallback#onGeofenceRemove} will be called with the result of the
 * remove call from the hardware.
 *
 * <p> Requires {@link android.Manifest.permission#ACCESS_FINE_LOCATION} permission when
 * {@link #MONITORING_TYPE_GPS_HARDWARE} is used.
 *
 * <p> Requires {@link android.Manifest.permission#LOCATION_HARDWARE} permission to access
 * geofencing in hardware.
 *
 * <p>This API should not be called directly by the app developers. A higher level api
 * which abstracts the hardware should be used instead. All the checks are done by the higher
 * level public API. Any needed locking should be handled by the higher level API.
 *
 * @param geofenceId The id of the geofence.
 * @param monitoringType The type of the hardware subsystem that should be used
 *        to monitor the geofence.
 * @return true when the geofence is successfully sent to the hardware for removal.                     .
 * @apiSince 18
 */

public boolean removeGeofence(int geofenceId, int monitoringType) { throw new RuntimeException("Stub!"); }

/**
 * Pauses the monitoring of a geofence added by {@link #addGeofence} call.
 *
 * <p> If this call returns true, it means that the geofence has been sent to the hardware.
 * {@link GeofenceHardwareCallback#onGeofencePause} will be called with the result of the
 * pause call from the hardware.
 *
 * <p> Requires {@link android.Manifest.permission#ACCESS_FINE_LOCATION} permission when
 * {@link #MONITORING_TYPE_GPS_HARDWARE} is used.
 *
 * <p> Requires {@link android.Manifest.permission#LOCATION_HARDWARE} permission to access
 * geofencing in hardware.
 *
 * <p>This API should not be called directly by the app developers. A higher level api
 * which abstracts the hardware should be used instead. All the checks are done by the higher
 * level public API. Any needed locking should be handled by the higher level API.
 *
 * @param geofenceId The id of the geofence.
 * @param monitoringType The type of the hardware subsystem that should be used
 *        to monitor the geofence.
 * @return true when the geofence is successfully sent to the hardware for pausing.
 * @apiSince 18
 */

public boolean pauseGeofence(int geofenceId, int monitoringType) { throw new RuntimeException("Stub!"); }

/**
 * Resumes the monitoring of a geofence added by {@link #pauseGeofence} call.
 *
 * <p> If this call returns true, it means that the geofence has been sent to the hardware.
 * {@link GeofenceHardwareCallback#onGeofenceResume} will be called with the result of the
 * resume call from the hardware.
 *
 * <p> Requires {@link android.Manifest.permission#ACCESS_FINE_LOCATION} permission when
 * {@link #MONITORING_TYPE_GPS_HARDWARE} is used.
 *
 * <p> Requires {@link android.Manifest.permission#LOCATION_HARDWARE} permission to access
 * geofencing in hardware.
 *
 * <p>This API should not be called directly by the app developers. A higher level api
 * which abstracts the hardware should be used instead. All the checks are done by the higher
 * level public API. Any needed locking should be handled by the higher level API.
 *
 * @param geofenceId The id of the geofence.
 * @param monitoringType The type of the hardware subsystem that should be used
 *        to monitor the geofence.
 * @param monitorTransition Bitwise OR of {@link #GEOFENCE_ENTERED},
 *        {@link #GEOFENCE_EXITED}, {@link #GEOFENCE_UNCERTAIN}
 * @return true when the geofence is successfully sent to the hardware for resumption.
 * @apiSince 18
 */

public boolean resumeGeofence(int geofenceId, int monitoringType, int monitorTransition) { throw new RuntimeException("Stub!"); }

/**
 * Register the callback to be notified when the state of a hardware geofence
 * monitoring system changes. For instance, it can change from
 * {@link #MONITOR_CURRENTLY_AVAILABLE} to {@link #MONITOR_CURRENTLY_UNAVAILABLE}
 *
 * <p> Requires {@link android.Manifest.permission#ACCESS_FINE_LOCATION} permission when
 * {@link #MONITORING_TYPE_GPS_HARDWARE} is used.
 *
 * <p> Requires {@link android.Manifest.permission#LOCATION_HARDWARE} permission to access
 * geofencing in hardware.
 *
 * <p>This API should not be called directly by the app developers. A higher level api
 * which abstracts the hardware should be used instead. All the checks are done by the higher
 * level public API. Any needed locking should be handled by the higher level API.
 *
 * <p> The same callback object can be used to be informed of geofence transitions
 * and state changes of the underlying hardware subsystem.
 *
 * @param monitoringType Type of the monitor
 * @param callback Callback that will be called.
 * @return true on success
 * @apiSince 18
 */

public boolean registerForMonitorStateChangeCallback(int monitoringType, android.hardware.location.GeofenceHardwareMonitorCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Unregister the callback that was used with {@link #registerForMonitorStateChangeCallback}
 * to notify when the state of the hardware geofence monitoring system changes.
 *
 * <p> Requires {@link android.Manifest.permission#ACCESS_FINE_LOCATION} permission when
 * {@link #MONITORING_TYPE_GPS_HARDWARE} is used.
 *
 * <p> Requires {@link android.Manifest.permission#LOCATION_HARDWARE} permission to access
 * geofencing in hardware.
 *
 * <p>This API should not be called directly by the app developers. A higher level api
 * which abstracts the hardware should be used instead. All the checks are done by the higher
 * level public API. Any needed locking should be handled by the higher level API.
 *
 * @param monitoringType Type of the monitor
 * @param callback Callback that will be called.
 * @return true on success
 * @apiSince 18
 */

public boolean unregisterForMonitorStateChangeCallback(int monitoringType, android.hardware.location.GeofenceHardwareMonitorCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * The constant to indicate that the user has entered the geofence.
 * @apiSince 18
 */

public static final int GEOFENCE_ENTERED = 1; // 0x1

/**
 * The constant used to indicate that the geofence id already exists.
 * @apiSince 18
 */

public static final int GEOFENCE_ERROR_ID_EXISTS = 2; // 0x2

/**
 * The constant used to indicate that the geofence id is unknown.
 * @apiSince 18
 */

public static final int GEOFENCE_ERROR_ID_UNKNOWN = 3; // 0x3

/**
 * The constant used to indicate that the operation failed due to insufficient memory.
 * @apiSince REL
 */

public static final int GEOFENCE_ERROR_INSUFFICIENT_MEMORY = 6; // 0x6

/**
 * The constant used to indicate that the transition requested for the geofence is invalid.
 * @apiSince 18
 */

public static final int GEOFENCE_ERROR_INVALID_TRANSITION = 4; // 0x4

/**
 * The constant used to indicate that too many geofences have been registered.
 * @apiSince 18
 */

public static final int GEOFENCE_ERROR_TOO_MANY_GEOFENCES = 1; // 0x1

/**
 * The constant to indicate that the user has exited the geofence.
 * @apiSince 18
 */

public static final int GEOFENCE_EXITED = 2; // 0x2

/**
 * The constant used to indicate that the geofence operation has failed.
 * @apiSince 18
 */

public static final int GEOFENCE_FAILURE = 5; // 0x5

/**
 * The constant used to indicate success of the particular geofence call
 * @apiSince 18
 */

public static final int GEOFENCE_SUCCESS = 0; // 0x0

/**
 * The constant to indicate that the user is uncertain with respect to a
 * geofence.
 * @apiSince 18
 */

public static final int GEOFENCE_UNCERTAIN = 4; // 0x4

/**
 * Constant for geofence monitoring done by the Fused hardware.
 * @apiSince REL
 */

public static final int MONITORING_TYPE_FUSED_HARDWARE = 1; // 0x1

/**
 * Constant for geofence monitoring done by the GPS hardware.
 * @apiSince 18
 */

public static final int MONITORING_TYPE_GPS_HARDWARE = 0; // 0x0

/**
 * Constant to indicate that the monitoring system is currently
 * available for monitoring geofences.
 * @apiSince 18
 */

public static final int MONITOR_CURRENTLY_AVAILABLE = 0; // 0x0

/**
 * Constant to indicate that the monitoring system is currently
 * unavailable for monitoring geofences.
 * @apiSince 18
 */

public static final int MONITOR_CURRENTLY_UNAVAILABLE = 1; // 0x1

/**
 * Constant to indicate that the monitoring system is unsupported
 * for hardware geofence monitoring.
 * @apiSince 18
 */

public static final int MONITOR_UNSUPPORTED = 2; // 0x2

/**
 * The constant used to indicate that the monitoring system supports Bluetooth.
 * @apiSince REL
 */

public static final int SOURCE_TECHNOLOGY_BLUETOOTH = 16; // 0x10

/**
 * The constant used to indicate that the monitoring system supports Cell.
 * @apiSince REL
 */

public static final int SOURCE_TECHNOLOGY_CELL = 8; // 0x8

/**
 * The constant used to indicate that the monitoring system supports GNSS.
 * @apiSince REL
 */

public static final int SOURCE_TECHNOLOGY_GNSS = 1; // 0x1

/**
 * The constant used to indicate that the monitoring system supports Sensors.
 * @apiSince REL
 */

public static final int SOURCE_TECHNOLOGY_SENSORS = 4; // 0x4

/**
 * The constant used to indicate that the monitoring system supports WiFi.
 * @apiSince REL
 */

public static final int SOURCE_TECHNOLOGY_WIFI = 2; // 0x2
}

