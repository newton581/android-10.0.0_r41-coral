/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims;

import android.telephony.ims.stub.ImsCallSessionImplBase;

/**
 * Listener interface for notifying the Framework's {@link ImsCallSession} for updates to an ongoing
 * IMS call.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ImsCallSessionListener {

ImsCallSessionListener() { throw new RuntimeException("Stub!"); }

/**
 * A request has been sent out to initiate a new IMS call session and a 1xx response has been
 * received from the network.
 * @apiSince REL
 */

public void callSessionProgressing(android.telephony.ims.ImsStreamMediaProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session has been initiated.
 *
 * @param profile the associated {@link ImsCallProfile}.
 * @apiSince REL
 */

public void callSessionInitiated(android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session establishment has failed.
 *
 * @param reasonInfo {@link ImsReasonInfo} detailing the reason of the IMS call session
 * establishment failure.
 * @apiSince REL
 */

public void callSessionInitiatedFailed(android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session has been terminated.
 *
 * @param reasonInfo {@link ImsReasonInfo} detailing the reason of the session termination.
 * @apiSince REL
 */

public void callSessionTerminated(android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session has started the process of holding the call. If it fails,
 * {@link #callSessionHoldFailed(ImsReasonInfo)} should be called.
 *
 * If the IMS call session is resumed, call {@link #callSessionResumed(ImsCallProfile)}.
 *
 * @param profile The associated {@link ImsCallProfile} of the call session that has been put
 * on hold.
 * @apiSince REL
 */

public void callSessionHeld(android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session has failed to be held.
 *
 * @param reasonInfo {@link ImsReasonInfo} detailing the reason of the session hold failure.
 * @apiSince REL
 */

public void callSessionHoldFailed(android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * This IMS Call session has been put on hold by the remote party.
 *
 * @param profile The {@link ImsCallProfile} associated with this IMS call session.
 * @apiSince REL
 */

public void callSessionHoldReceived(android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session has started the process of resuming the call. If the process of resuming
 * the call fails, call {@link #callSessionResumeFailed(ImsReasonInfo)}.
 *
 * @param profile The {@link ImsCallProfile} associated with this IMS call session.
 * @apiSince REL
 */

public void callSessionResumed(android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session resume has failed.
 *
 * @param reasonInfo {@link ImsReasonInfo} containing the detailed reason of the session resume
 * failure.
 * @apiSince REL
 */

public void callSessionResumeFailed(android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * The remote party has resumed this IMS call session.
 *
 * @param profile {@link ImsCallProfile} associated with the IMS call session.
 * @apiSince REL
 */

public void callSessionResumeReceived(android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session merge has been started.  At this point, the {@code newSession}
 * represents the IMS call session which represents the new merged conference and has been
 * initiated to the IMS conference server.
 *
 * @param newSession the {@link ImsCallSessionImplBase} that represents the merged active & held
 * sessions.
 * @param profile The {@link ImsCallProfile} associated with this IMS call session.
 * @apiSince REL
 */

public void callSessionMergeStarted(android.telephony.ims.stub.ImsCallSessionImplBase newSession, android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The session merge is successful and the merged {@link ImsCallSession} is active.
 *
 * @param newSession the new {@link ImsCallSessionImplBase}
 *                  that represents the conference IMS call
 * session.
 * @apiSince REL
 */

public void callSessionMergeComplete(android.telephony.ims.stub.ImsCallSessionImplBase newSession) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session merge has failed.
 *
 * @param reasonInfo {@link ImsReasonInfo} contining the reason for the call merge failure.
 * @apiSince REL
 */

public void callSessionMergeFailed(android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session profile has been updated. Does not include holding or resuming a call.
 *
 * @param profile The {@link ImsCallProfile} associated with the updated IMS call session.
 * @apiSince REL
 */

public void callSessionUpdated(android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session profile update has failed.
 *
 * @param reasonInfo {@link ImsReasonInfo} containing a reason for the session update failure.
 * @apiSince REL
 */

public void callSessionUpdateFailed(android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session profile has received an update from the remote user.
 *
 * @param profile The new {@link ImsCallProfile} associated with the update.
 * @apiSince REL
 */

public void callSessionUpdateReceived(android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * Called when the session has been extended to a conference session.
 *
 * If the conference extension fails, call
 * {@link #callSessionConferenceExtendFailed(ImsReasonInfo)}.
 *
 * @param newSession the session object that is extended to the conference from the active
 * IMS Call session.
 * @param profile The {@link ImsCallProfile} associated with the IMS call session.
 * @apiSince REL
 */

public void callSessionConferenceExtended(android.telephony.ims.stub.ImsCallSessionImplBase newSession, android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The previous conference extension has failed.
 *
 * @param reasonInfo {@link ImsReasonInfo} containing the detailed reason of the conference
 * extension failure.
 * @apiSince REL
 */

public void callSessionConferenceExtendFailed(android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * A conference extension has been received received from the remote party.
 *
 * @param newSession An {@link ImsCallSessionImplBase}
 *                   representing the extended IMS call session.
 * @param profile The {@link ImsCallProfile} associated with the new IMS call session.
 * @apiSince REL
 */

public void callSessionConferenceExtendReceived(android.telephony.ims.stub.ImsCallSessionImplBase newSession, android.telephony.ims.ImsCallProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The request to invite participants to the conference has been delivered to the conference
 * server.
 * @apiSince REL
 */

public void callSessionInviteParticipantsRequestDelivered() { throw new RuntimeException("Stub!"); }

/**
 * The previous request to invite participants to the conference (see
 * {@link #callSessionInviteParticipantsRequestDelivered()}) has failed.
 *
 * @param reasonInfo {@link ImsReasonInfo} detailing the reason forthe conference invitation
 * failure.
 * @apiSince REL
 */

public void callSessionInviteParticipantsRequestFailed(android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * The request to remove participants from the conference has been delivered to the conference
 * server.
 * @apiSince REL
 */

public void callSessionRemoveParticipantsRequestDelivered() { throw new RuntimeException("Stub!"); }

/**
 * The previous request to remove participants from the conference (see
 * {@link #callSessionRemoveParticipantsRequestDelivered()}) has failed.
 *
 * @param reasonInfo {@link ImsReasonInfo} detailing the reason for the conference removal
 * failure.
 * @apiSince REL
 */

public void callSessionRemoveParticipantsRequestFailed(android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session's conference state has changed.
 *
 * @param state The new {@link ImsConferenceState} associated with the conference.
 * @apiSince REL
 */

public void callSessionConferenceStateUpdated(android.telephony.ims.ImsConferenceState state) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session has received a Ussd message.
 *
 * @param mode The mode of the USSD message, either
 * {@link ImsCallSessionImplBase#USSD_MODE_NOTIFY} or
 * {@link ImsCallSessionImplBase#USSD_MODE_REQUEST}.
 * @param ussdMessage The USSD message.
 * @apiSince REL
 */

public void callSessionUssdMessageReceived(int mode, java.lang.String ussdMessage) { throw new RuntimeException("Stub!"); }

/**
 * An {@link ImsCallSession} may potentially handover from one radio
 * technology to another.
 *
 * @param srcAccessTech The source radio access technology; one of the access technology
 * constants defined in {@link android.telephony.ServiceState}. For example
 * {@link android.telephony.ServiceState#RIL_RADIO_TECHNOLOGY_LTE}.
 * @param targetAccessTech The target radio access technology; one of the access technology
 * constants defined in {@link android.telephony.ServiceState}. For example
 * {@link android.telephony.ServiceState#RIL_RADIO_TECHNOLOGY_LTE}.
 * @apiSince REL
 */

public void callSessionMayHandover(int srcAccessTech, int targetAccessTech) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session's access technology has changed.
 *
 * @param srcAccessTech original access technology, defined in
 * {@link android.telephony.ServiceState}.
 * @param targetAccessTech new access technology, defined in
 * {@link android.telephony.ServiceState}.
 * @param reasonInfo The {@link ImsReasonInfo} associated with this handover.
 * @apiSince REL
 */

public void callSessionHandover(int srcAccessTech, int targetAccessTech, android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * The IMS call session's access technology change has failed..
 *
 * @param srcAccessTech original access technology
 * @param targetAccessTech new access technology
 * @param reasonInfo An {@link ImsReasonInfo} detailing the reason for the failure.
 * @apiSince REL
 */

public void callSessionHandoverFailed(int srcAccessTech, int targetAccessTech, android.telephony.ims.ImsReasonInfo reasonInfo) { throw new RuntimeException("Stub!"); }

/**
 * The TTY mode has been changed by the remote party.
 *
 * @param mode one of the following: -
 *             {@link com.android.internal.telephony.Phone#TTY_MODE_OFF} -
 *             {@link com.android.internal.telephony.Phone#TTY_MODE_FULL} -
 *             {@link com.android.internal.telephony.Phone#TTY_MODE_HCO} -
 *             {@link com.android.internal.telephony.Phone#TTY_MODE_VCO}
 * @apiSince REL
 */

public void callSessionTtyModeReceived(int mode) { throw new RuntimeException("Stub!"); }

/**
 * The multiparty state has been changed for this {@code ImsCallSession}.
 *
 * @param isMultiParty {@code true} if the session became multiparty, {@code false} otherwise.
 * @apiSince REL
 */

public void callSessionMultipartyStateChanged(boolean isMultiParty) { throw new RuntimeException("Stub!"); }

/**
 * Supplementary service information has been received for the current IMS call session.
 *
 * @param suppSrvNotification The {@link ImsSuppServiceNotification} containing the change.
 * @apiSince REL
 */

public void callSessionSuppServiceReceived(android.telephony.ims.ImsSuppServiceNotification suppSrvNotification) { throw new RuntimeException("Stub!"); }

/**
 * An RTT modify request has been received from the remote party.
 *
 * @param callProfile An {@link ImsCallProfile} with the updated attributes
 * @apiSince REL
 */

public void callSessionRttModifyRequestReceived(android.telephony.ims.ImsCallProfile callProfile) { throw new RuntimeException("Stub!"); }

/**
 * An RTT modify response has been received.
 *
 * @param status the received response for RTT modify request.
 * @apiSince REL
 */

public void callSessionRttModifyResponseReceived(int status) { throw new RuntimeException("Stub!"); }

/**
 * An RTT message has been received from the remote party.
 *
 * @param rttMessage The RTT message that has been received.
 * @apiSince REL
 */

public void callSessionRttMessageReceived(java.lang.String rttMessage) { throw new RuntimeException("Stub!"); }

/**
 * While in call, there has been a change in RTT audio indicator.
 *
 * @param profile updated ImsStreamMediaProfile
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void callSessionRttAudioIndicatorChanged(@android.annotation.NonNull android.telephony.ims.ImsStreamMediaProfile profile) { throw new RuntimeException("Stub!"); }

/**
 * The call quality has changed.
 *
 * @param callQuality The new call quality
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void callQualityChanged(@android.annotation.NonNull android.telephony.CallQuality callQuality) { throw new RuntimeException("Stub!"); }
}

