/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.webkit;


/**
 * Top level factory, used creating all the main WebView implementation classes.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class WebViewFactory {

public WebViewFactory() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static android.content.pm.PackageInfo getLoadedPackageInfo() { throw new RuntimeException("Stub!"); }

/**
 * Load the native library for the given package name if that package
 * name is the same as the one providing the webview.
 * @apiSince REL
 */

public static int loadWebViewNativeLibraryFromPackage(java.lang.String packageName, java.lang.ClassLoader clazzLoader) { throw new RuntimeException("Stub!"); }

/**
 * Perform any WebView loading preparations that must happen in the zygote.
 * Currently, this means allocating address space to load the real JNI library later.
 * @apiSince REL
 */

public static void prepareWebViewInZygote() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final int LIBLOAD_ADDRESS_SPACE_NOT_RESERVED = 2; // 0x2

/** @apiSince REL */

public static final int LIBLOAD_FAILED_JNI_CALL = 7; // 0x7

/** @apiSince REL */

public static final int LIBLOAD_FAILED_LISTING_WEBVIEW_PACKAGES = 4; // 0x4

/** @apiSince REL */

public static final int LIBLOAD_FAILED_TO_FIND_NAMESPACE = 10; // 0xa

/** @apiSince REL */

public static final int LIBLOAD_FAILED_TO_LOAD_LIBRARY = 6; // 0x6

/** @apiSince REL */

public static final int LIBLOAD_FAILED_TO_OPEN_RELRO_FILE = 5; // 0x5

/** @apiSince REL */

public static final int LIBLOAD_FAILED_WAITING_FOR_RELRO = 3; // 0x3

/** @apiSince REL */

public static final int LIBLOAD_FAILED_WAITING_FOR_WEBVIEW_REASON_UNKNOWN = 8; // 0x8

/** @apiSince REL */

public static final int LIBLOAD_SUCCESS = 0; // 0x0

/** @apiSince REL */

public static final int LIBLOAD_WRONG_PACKAGE_NAME = 1; // 0x1
}

