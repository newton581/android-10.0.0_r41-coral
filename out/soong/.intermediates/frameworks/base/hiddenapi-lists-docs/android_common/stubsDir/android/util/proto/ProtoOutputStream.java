/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.util.proto;

import java.io.OutputStream;
import java.io.FileDescriptor;

/**
 * Class to write to a protobuf stream.
 *
 * Each write method takes an ID code from the protoc generated classes
 * and the value to write.  To make a nested object, call #start
 * and then #end when you are done.
 *
 * The ID codes have type information embedded into them, so if you call
 * the incorrect function you will get an IllegalArgumentException.
 *
 * To retrieve the encoded protobuf stream, call getBytes().
 *
 * TODO: Add a constructor that takes an OutputStream and write to that
 * stream as the top-level objects are finished.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ProtoOutputStream extends android.util.proto.ProtoStream {

/**
 * Construct a ProtoOutputStream with the default chunk size.
 * @apiSince REL
 */

public ProtoOutputStream() { throw new RuntimeException("Stub!"); }

/**
 * Construct a ProtoOutputStream with the given chunk size.
 * @apiSince REL
 */

public ProtoOutputStream(int chunkSize) { throw new RuntimeException("Stub!"); }

/**
 * Construct a ProtoOutputStream that sits on top of an OutputStream.
 * @more
 * The {@link #flush() flush()} method must be called when done writing
 * to flush any remanining data, althought data *may* be written at intermediate
 * points within the writing as well.
 * @apiSince REL
 */

public ProtoOutputStream(java.io.OutputStream stream) { throw new RuntimeException("Stub!"); }

/**
 * Construct a ProtoOutputStream that sits on top of a FileDescriptor.
 * @more
 * The {@link #flush() flush()} method must be called when done writing
 * to flush any remanining data, althought data *may* be written at intermediate
 * points within the writing as well.
 * @apiSince REL
 */

public ProtoOutputStream(java.io.FileDescriptor fd) { throw new RuntimeException("Stub!"); }

/**
 * Returns the uncompressed buffer size
 * @return the uncompressed buffer size
 * @apiSince REL
 */

public int getRawSize() { throw new RuntimeException("Stub!"); }

/**
 * Write a value for the given fieldId.
 *
 * Will automatically convert for the following field types, and
 * throw an exception for others: double, float, int32, int64, uint32, uint64,
 * sint32, sint64, fixed32, fixed64, sfixed32, sfixed64, bool, enum.
 *
 * @param fieldId The field identifier constant from the generated class.
 * @param val The value.
 * @apiSince REL
 */

public void write(long fieldId, double val) { throw new RuntimeException("Stub!"); }

/**
 * Write a value for the given fieldId.
 *
 * Will automatically convert for the following field types, and
 * throw an exception for others: double, float, int32, int64, uint32, uint64,
 * sint32, sint64, fixed32, fixed64, sfixed32, sfixed64, bool, enum.
 *
 * @param fieldId The field identifier constant from the generated class.
 * @param val The value.
 * @apiSince REL
 */

public void write(long fieldId, float val) { throw new RuntimeException("Stub!"); }

/**
 * Write a value for the given fieldId.
 *
 * Will automatically convert for the following field types, and
 * throw an exception for others: double, float, int32, int64, uint32, uint64,
 * sint32, sint64, fixed32, fixed64, sfixed32, sfixed64, bool, enum.
 *
 * @param fieldId The field identifier constant from the generated class.
 * @param val The value.
 * @apiSince REL
 */

public void write(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a value for the given fieldId.
 *
 * Will automatically convert for the following field types, and
 * throw an exception for others: double, float, int32, int64, uint32, uint64,
 * sint32, sint64, fixed32, fixed64, sfixed32, sfixed64, bool, enum.
 *
 * @param fieldId The field identifier constant from the generated class.
 * @param val The value.
 * @apiSince REL
 */

public void write(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a boolean value for the given fieldId.
 *
 * If the field is not a bool field, an exception will be thrown.
 *
 * @param fieldId The field identifier constant from the generated class.
 * @param val The value.
 * @apiSince REL
 */

public void write(long fieldId, boolean val) { throw new RuntimeException("Stub!"); }

/**
 * Write a string value for the given fieldId.
 *
 * If the field is not a string field, an exception will be thrown.
 *
 * @param fieldId The field identifier constant from the generated class.
 * @param val The value.
 * @apiSince REL
 */

public void write(long fieldId, java.lang.String val) { throw new RuntimeException("Stub!"); }

/**
 * Write a byte[] value for the given fieldId.
 *
 * If the field is not a bytes or object field, an exception will be thrown.
 *
 * @param fieldId The field identifier constant from the generated class.
 * @param val The value.
 * @apiSince REL
 */

public void write(long fieldId, byte[] val) { throw new RuntimeException("Stub!"); }

/**
 * Start a sub object.
 * @apiSince REL
 */

public long start(long fieldId) { throw new RuntimeException("Stub!"); }

/**
 * End the object started by start() that returned token.
 * @apiSince REL
 */

public void end(long token) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "double" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeDouble(long fieldId, double val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "double" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedDouble(long fieldId, double val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "double" type field values.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedDouble(long fieldId, double[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "float" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeFloat(long fieldId, float val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "float" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedFloat(long fieldId, float val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "float" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedFloat(long fieldId, float[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "int32" type field value.
 *
 * Note that these are stored in memory as signed values and written as unsigned
 * varints, which if negative, are 10 bytes long. If you know the data is likely
 * to be negative, use "sint32".
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeInt32(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "int32" type field value.
 *
 * Note that these are stored in memory as signed values and written as unsigned
 * varints, which if negative, are 10 bytes long. If you know the data is likely
 * to be negative, use "sint32".
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedInt32(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "int32" type field value.
 *
 * Note that these are stored in memory as signed values and written as unsigned
 * varints, which if negative, are 10 bytes long. If you know the data is likely
 * to be negative, use "sint32".
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedInt32(long fieldId, int[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "int64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeInt64(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "int64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedInt64(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "int64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedInt64(long fieldId, long[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "uint32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeUInt32(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "uint32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedUInt32(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "uint32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedUInt32(long fieldId, int[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "uint64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeUInt64(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "uint64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedUInt64(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "uint64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedUInt64(long fieldId, long[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "sint32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeSInt32(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "sint32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedSInt32(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "sint32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedSInt32(long fieldId, int[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "sint64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeSInt64(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "sint64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedSInt64(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "sint64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedSInt64(long fieldId, long[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "fixed32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeFixed32(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "fixed32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedFixed32(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "fixed32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedFixed32(long fieldId, int[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "fixed64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeFixed64(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "fixed64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedFixed64(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "fixed64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedFixed64(long fieldId, long[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "sfixed32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeSFixed32(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "sfixed32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedSFixed32(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "sfixed32" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedSFixed32(long fieldId, int[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "sfixed64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeSFixed64(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "sfixed64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedSFixed64(long fieldId, long val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "sfixed64" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedSFixed64(long fieldId, long[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "bool" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeBool(long fieldId, boolean val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "bool" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedBool(long fieldId, boolean val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto "bool" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedBool(long fieldId, boolean[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "string" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeString(long fieldId, java.lang.String val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "string" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedString(long fieldId, java.lang.String val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto "bytes" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeBytes(long fieldId, byte[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto "bytes" type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedBytes(long fieldId, byte[] val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single proto enum type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeEnum(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a single repeated proto enum type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedEnum(long fieldId, int val) { throw new RuntimeException("Stub!"); }

/**
 * Write a list of packed proto enum type field value.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writePackedEnum(long fieldId, int[] val) { throw new RuntimeException("Stub!"); }

/**
 * Start a child object.
 *
 * Returns a token which should be passed to endObject.  Calls to endObject must be
 * nested properly.
 *
 * @deprecated Use #start() instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public long startObject(long fieldId) { throw new RuntimeException("Stub!"); }

/**
 * End a child object. Pass in the token from the correspoinding startObject call.
 *
 * @deprecated Use #end() instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void endObject(long token) { throw new RuntimeException("Stub!"); }

/**
 * Start a repeated child object.
 *
 * Returns a token which should be passed to endObject.  Calls to endObject must be
 * nested properly.
 *
 * @deprecated Use #start() instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public long startRepeatedObject(long fieldId) { throw new RuntimeException("Stub!"); }

/**
 * End a child object. Pass in the token from the correspoinding startRepeatedObject call.
 *
 * @deprecated Use #end() instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void endRepeatedObject(long token) { throw new RuntimeException("Stub!"); }

/**
 * Write an object that has already been flattend.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeObject(long fieldId, byte[] value) { throw new RuntimeException("Stub!"); }

/**
 * Write an object that has already been flattend.
 *
 * @deprecated Use #write instead.
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeRepeatedObject(long fieldId, byte[] value) { throw new RuntimeException("Stub!"); }

/**
 * Combine a fieldId (the field keys in the proto file) and the field flags.
 * Mostly useful for testing because the generated code contains the fieldId
 * constants.
 * @apiSince REL
 */

public static long makeFieldId(int id, long fieldFlags) { throw new RuntimeException("Stub!"); }

/**
 * Validates that the fieldId providied is of the type and count from expectedType.
 *
 * The type must match exactly to pass this check.
 *
 * The count must match according to this truth table to pass the check:
 *
 *                  expectedFlags
 *                  UNKNOWN     SINGLE      REPEATED    PACKED
 *    fieldId
 *    UNKNOWN       true        false       false       false
 *    SINGLE        x           true        false       false
 *    REPEATED      x           false       true        false
 *    PACKED        x           false       true        true
 *
 * @throws IllegalArgumentException if it is not.
 *
 * @return The raw ID of that field.
 * @apiSince REL
 */

public static int checkFieldId(long fieldId, long expectedFlags) { throw new RuntimeException("Stub!"); }

/**
 * Write a field tage to the stream.
 * @apiSince REL
 */

public void writeTag(int id, int wireType) { throw new RuntimeException("Stub!"); }

/**
 * Finish the encoding of the data, and return a byte[] with
 * the protobuf formatted data.
 *
 * After this call, do not call any of the write* functions. The
 * behavior is undefined.
 * @apiSince REL
 */

public byte[] getBytes() { throw new RuntimeException("Stub!"); }

/**
 * Write remaining data to the output stream.  If there is no output stream,
 * this function does nothing. Any currently open objects (i.e. ones that
 * have not had endObject called for them will not be written).  Whether this
 * writes objects that are closed if there are remaining open objects is
 * undefined (current implementation does not write it, future ones will).
 * For now, can either call getBytes() or flush(), but not both.
 * @apiSince REL
 */

public void flush() { throw new RuntimeException("Stub!"); }

/**
 * Dump debugging data about the buffers with the given log tag.
 * @apiSince REL
 */

public void dump(java.lang.String tag) { throw new RuntimeException("Stub!"); }
}

