/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed urnder the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.net;


/**
 * A class allowing apps handling the {@link ConnectivityManager#ACTION_CAPTIVE_PORTAL_SIGN_IN}
 * activity to indicate to the system different outcomes of captive portal sign in.  This class is
 * passed as an extra named {@link ConnectivityManager#EXTRA_CAPTIVE_PORTAL} with the
 * {@code ACTION_CAPTIVE_PORTAL_SIGN_IN} activity.
 * @apiSince 23
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class CaptivePortal implements android.os.Parcelable {

/** @hide */

CaptivePortal(@android.annotation.NonNull android.os.IBinder binder) { throw new RuntimeException("Stub!"); }

/** @apiSince 23 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince 23 */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Indicate to the system that the captive portal has been
 * dismissed.  In response the framework will re-evaluate the network's
 * connectivity and might take further action thereafter.
 * @apiSince 23
 */

public void reportCaptivePortalDismissed() { throw new RuntimeException("Stub!"); }

/**
 * Indicate to the system that the user does not want to pursue signing in to the
 * captive portal and the system should continue to prefer other networks
 * without captive portals for use as the default active data network.  The
 * system will not retest the network for a captive portal so as to avoid
 * disturbing the user with further sign in to network notifications.
 * @apiSince 23
 */

public void ignoreNetwork() { throw new RuntimeException("Stub!"); }

/**
 * Indicate to the system the user wants to use this network as is, even though
 * the captive portal is still in place.  The system will treat the network
 * as if it did not have a captive portal when selecting the network to use
 * as the default active data network. This may result in this network
 * becoming the default active data network, which could disrupt network
 * connectivity for apps because the captive portal is still in place.
 * @hide
 */

public void useNetwork() { throw new RuntimeException("Stub!"); }

/**
 * Log a captive portal login event.
 * @param eventId one of the CAPTIVE_PORTAL_LOGIN_* constants in metrics_constants.proto.
 * @param packageName captive portal application package name.
 * This value must never be {@code null}.
 * @hide
 */

public void logEvent(int eventId, @android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Response code from the captive portal application, indicating that the portal was dismissed
 * and the network should be re-validated.
 * @see ICaptivePortal#appResponse(int)
 * @see android.net.INetworkMonitor#notifyCaptivePortalAppFinished(int)
 * @hide
 */

public static final int APP_RETURN_DISMISSED = 0; // 0x0

/**
 * Response code from the captive portal application, indicating that the user did not login and
 * does not want to use the captive portal network.
 * @see ICaptivePortal#appResponse(int)
 * @see android.net.INetworkMonitor#notifyCaptivePortalAppFinished(int)
 * @hide
 */

public static final int APP_RETURN_UNWANTED = 1; // 0x1

/**
 * Response code from the captive portal application, indicating that the user does not wish to
 * login but wants to use the captive portal network as-is.
 * @see ICaptivePortal#appResponse(int)
 * @see android.net.INetworkMonitor#notifyCaptivePortalAppFinished(int)
 * @hide
 */

public static final int APP_RETURN_WANTED_AS_IS = 2; // 0x2

/** @apiSince 23 */

@androidx.annotation.RecentlyNonNull public static final android.os.Parcelable.Creator<android.net.CaptivePortal> CREATOR;
static { CREATOR = null; }
}

