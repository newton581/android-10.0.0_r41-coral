/*
 * Copyright (c) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony.ims;

import android.content.pm.PackageManager;

/**
 * This class defines an IMS-related exception that has been thrown while interacting with a
 * device or carrier provided ImsService implementation.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ImsException extends java.lang.Exception {

/**
 * A new {@link ImsException} with an unspecified {@link ImsErrorCode} code.
 * @param message an optional message to detail the error condition more specifically.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public ImsException(@android.annotation.Nullable java.lang.String message) { throw new RuntimeException("Stub!"); }

/**
 * A new {@link ImsException} that includes an {@link ImsErrorCode} error code.
 * @param message an optional message to detail the error condition more specifically.
 
 * This value may be {@code null}.
 
 * @param code Value is {@link android.telephony.ims.ImsException#CODE_ERROR_UNSPECIFIED}, {@link android.telephony.ims.ImsException#CODE_ERROR_SERVICE_UNAVAILABLE}, or {@link android.telephony.ims.ImsException#CODE_ERROR_UNSUPPORTED_OPERATION}
 * @apiSince REL
 */

public ImsException(@android.annotation.Nullable java.lang.String message, int code) { throw new RuntimeException("Stub!"); }

/**
 * A new {@link ImsException} that includes an {@link ImsErrorCode} error code and a
 * {@link Throwable} that contains the original error that was thrown to lead to this Exception.
 * @param message an optional message to detail the error condition more specifically.
 * This value may be {@code null}.
 * @param cause the {@link Throwable} that caused this {@link ImsException} to be created.
 
 * This value may be {@code null}.
 * @param code Value is {@link android.telephony.ims.ImsException#CODE_ERROR_UNSPECIFIED}, {@link android.telephony.ims.ImsException#CODE_ERROR_SERVICE_UNAVAILABLE}, or {@link android.telephony.ims.ImsException#CODE_ERROR_UNSUPPORTED_OPERATION}
 * @apiSince REL
 */

public ImsException(@android.annotation.Nullable java.lang.String message, int code, @android.annotation.Nullable java.lang.Throwable cause) { throw new RuntimeException("Stub!"); }

/**
 * @return the IMS Error code that is associated with this {@link ImsException}.
 
 * Value is {@link android.telephony.ims.ImsException#CODE_ERROR_UNSPECIFIED}, {@link android.telephony.ims.ImsException#CODE_ERROR_SERVICE_UNAVAILABLE}, or {@link android.telephony.ims.ImsException#CODE_ERROR_UNSUPPORTED_OPERATION}
 * @apiSince REL
 */

public int getCode() { throw new RuntimeException("Stub!"); }

/**
 * The operation has failed because there is no {@link ImsService} available to service it. This
 * may be due to an {@link ImsService} crash or other illegal state.
 * <p>
 * This is a temporary error and the operation may be retried until the connection to the
 * {@link ImsService} is restored.
 * @apiSince REL
 */

public static final int CODE_ERROR_SERVICE_UNAVAILABLE = 1; // 0x1

/**
 * The operation has failed due to an unknown or unspecified error.
 * @apiSince REL
 */

public static final int CODE_ERROR_UNSPECIFIED = 0; // 0x0

/**
 * This device or carrier configuration does not support IMS for this subscription.
 * <p>
 * This is a permanent configuration error and there should be no retry. Usually this is
 * because {@link PackageManager#FEATURE_TELEPHONY_IMS} is not available
 * or the device has no ImsService implementation to service this request.
 * @apiSince REL
 */

public static final int CODE_ERROR_UNSUPPORTED_OPERATION = 2; // 0x2
}

