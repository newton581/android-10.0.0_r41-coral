/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.location;


/**
 * A container of supported GNSS chipset capabilities.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class GnssCapabilities {

GnssCapabilities(long gnssCapabilities) { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if GNSS chipset supports low power mode, {@code false} otherwise.
 * @apiSince REL
 */

public boolean hasLowPowerMode() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if GNSS chipset supports blacklisting satellites, {@code false}
 * otherwise.
 * @apiSince REL
 */

public boolean hasSatelliteBlacklist() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if GNSS chipset supports geofencing, {@code false} otherwise.
 * @apiSince REL
 */

public boolean hasGeofencing() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if GNSS chipset supports measurements, {@code false} otherwise.
 * @apiSince REL
 */

public boolean hasMeasurements() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if GNSS chipset supports navigation messages, {@code false} otherwise.
 * @apiSince REL
 */

public boolean hasNavMessages() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if GNSS chipset supports measurement corrections, {@code false}
 * otherwise.
 * @apiSince REL
 */

public boolean hasMeasurementCorrections() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if GNSS chipset supports line-of-sight satellite identification
 * measurement corrections, {@code false} otherwise.
 * @apiSince REL
 */

public boolean hasMeasurementCorrectionsLosSats() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if GNSS chipset supports per satellite excess-path-length measurement
 * corrections, {@code false} otherwise.
 * @apiSince REL
 */

public boolean hasMeasurementCorrectionsExcessPathLength() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if GNSS chipset supports reflecting planes measurement corrections,
 * {@code false} otherwise.
 * @apiSince REL
 */

public boolean hasMeasurementCorrectionsReflectingPane() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

