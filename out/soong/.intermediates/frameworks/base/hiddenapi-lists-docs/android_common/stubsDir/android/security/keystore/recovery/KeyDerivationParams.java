/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.security.keystore.recovery;


/**
 * Collection of parameters which define a key derivation function.
 * Currently only supports salted SHA-256.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class KeyDerivationParams implements android.os.Parcelable {

/**
 * @hide
 */

KeyDerivationParams(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Creates instance of the class to to derive keys using salted SHA256 hash.
 *
 * <p>The salted SHA256 hash is computed over the concatenation of four byte strings, salt_len +
 * salt + key_material_len + key_material, where salt_len and key_material_len are 4-byte, and
 * denote the number of bytes for salt and key_material, respectively.
 
 * @param salt This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static android.security.keystore.recovery.KeyDerivationParams createSha256Params(@android.annotation.NonNull byte[] salt) { throw new RuntimeException("Stub!"); }

/**
 * Creates instance of the class to to derive keys using the password hashing algorithm SCRYPT.
 *
 * <p>We expose only one tuning parameter of SCRYPT, which is the memory cost parameter (i.e. N
 * in <a href="https://www.tarsnap.com/scrypt/scrypt.pdf">the SCRYPT paper</a>). Regular/default
 * values are used for the other parameters, to keep the overall running time low. Specifically,
 * the parallelization parameter p is 1, the block size parameter r is 8, and the hashing output
 * length is 32-byte.
 
 * @param salt This value must never be {@code null}.
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public static android.security.keystore.recovery.KeyDerivationParams createScryptParams(@android.annotation.NonNull byte[] salt, int memoryDifficulty) { throw new RuntimeException("Stub!"); }

/**
 * Gets algorithm.
 
 * @return Value is {@link android.security.keystore.recovery.KeyDerivationParams#ALGORITHM_SHA256}, or {@link android.security.keystore.recovery.KeyDerivationParams#ALGORITHM_SCRYPT}
 * @apiSince REL
 */

public int getAlgorithm() { throw new RuntimeException("Stub!"); }

/**
 * Gets salt.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public byte[] getSalt() { throw new RuntimeException("Stub!"); }

/**
 * Gets the memory difficulty parameter for the hashing algorithm.
 *
 * <p>The effect of this parameter depends on the algorithm in use. For example, please see
 * {@link #createScryptParams(byte[], int)} for choosing the parameter for SCRYPT.
 *
 * <p>If the specific algorithm does not support such a memory difficulty parameter, its value
 * should be -1.
 * @apiSince REL
 */

public int getMemoryDifficulty() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * SCRYPT.
 * @apiSince REL
 */

public static final int ALGORITHM_SCRYPT = 2; // 0x2

/**
 * Salted SHA256.
 * @apiSince REL
 */

public static final int ALGORITHM_SHA256 = 1; // 0x1

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.security.keystore.recovery.KeyDerivationParams> CREATOR;
static { CREATOR = null; }
}

