/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony.data;

import android.telephony.AccessNetworkConstants.AccessNetworkType;
import java.util.List;

/**
 * Base class of the qualified networks service, which is a vendor service providing up-to-date
 * qualified network information to the frameworks for data handover control. A qualified network
 * is defined as an access network that is ready for bringing up data connection for given APN
 * types.
 *
 * Services that extend QualifiedNetworksService must register the service in their AndroidManifest
 * to be detected by the framework. They must be protected by the permission
 * "android.permission.BIND_TELEPHONY_DATA_SERVICE". The qualified networks service definition in
 * the manifest must follow the following format:
 * ...
 * <service android:name=".xxxQualifiedNetworksService"
 *     android:permission="android.permission.BIND_TELEPHONY_DATA_SERVICE" >
 *     <intent-filter>
 *         <action android:name="android.telephony.data.QualifiedNetworksService" />
 *     </intent-filter>
 * </service>
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class QualifiedNetworksService extends android.app.Service {

/**
 * Default constructor.
 * @apiSince REL
 */

public QualifiedNetworksService() { throw new RuntimeException("Stub!"); }

/**
 * Create the instance of {@link NetworkAvailabilityProvider}. Vendor qualified network service
 * must override this method to facilitate the creation of {@link NetworkAvailabilityProvider}
 * instances. The system will call this method after binding the qualified networks service for
 * each active SIM slot index.
 *
 * @param slotIndex SIM slot index the qualified networks service associated with.
 * @return Qualified networks service instance
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public abstract android.telephony.data.QualifiedNetworksService.NetworkAvailabilityProvider onCreateNetworkAvailabilityProvider(int slotIndex);

/** @hide */

public android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/** @hide */

public boolean onUnbind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/** @hide */

public void onDestroy() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public static final java.lang.String QUALIFIED_NETWORKS_SERVICE_INTERFACE = "android.telephony.data.QualifiedNetworksService";
/**
 * The abstract class of the network availability provider implementation. The vendor qualified
 * network service must extend this class to report the available networks for data
 * connection setup. Note that each instance of network availability provider is associated with
 * one physical SIM slot.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class NetworkAvailabilityProvider implements java.lang.AutoCloseable {

/**
 * Constructor
 * @param slotIndex SIM slot index the network availability provider associated with.
 */

public NetworkAvailabilityProvider(int slotIndex) { throw new RuntimeException("Stub!"); }

/**
 * @return SIM slot index the network availability provider associated with.
 * @apiSince REL
 */

public final int getSlotIndex() { throw new RuntimeException("Stub!"); }

/**
 * Update the qualified networks list. Network availability provider must invoke this method
 * whenever the qualified networks changes. If this method is never invoked for certain
 * APN types, then frameworks will always use the default (i.e. cellular) data and network
 * service.
 *
 * @param apnTypes APN types of the qualified networks. This must be a bitmask combination
 * of {@link ApnSetting.ApnType}.
 * Value is either <code>0</code> or a combination of {@link android.telephony.data.ApnSetting#TYPE_DEFAULT}, {@link android.telephony.data.ApnSetting#TYPE_MMS}, {@link android.telephony.data.ApnSetting#TYPE_SUPL}, {@link android.telephony.data.ApnSetting#TYPE_DUN}, {@link android.telephony.data.ApnSetting#TYPE_HIPRI}, {@link android.telephony.data.ApnSetting#TYPE_FOTA}, {@link android.telephony.data.ApnSetting#TYPE_IMS}, {@link android.telephony.data.ApnSetting#TYPE_CBS}, {@link android.telephony.data.ApnSetting#TYPE_IA}, {@link android.telephony.data.ApnSetting#TYPE_EMERGENCY}, and {@link android.telephony.data.ApnSetting#TYPE_MCX}
 * @param qualifiedNetworkTypes List of network types which are qualified for data
 * connection setup for {@link @apnType} in the preferred order. Each element in the list
 * is a {@link AccessNetworkType}. An empty list indicates no networks are qualified
 * for data setup.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public final void updateQualifiedNetworkTypes(int apnTypes, @android.annotation.NonNull java.util.List<java.lang.Integer> qualifiedNetworkTypes) { throw new RuntimeException("Stub!"); }

/**
 * Called when the qualified networks provider is removed. The extended class should
 * implement this method to perform cleanup works.
 * @apiSince REL
 */

public abstract void close();
}

}

