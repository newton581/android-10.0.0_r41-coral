/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.os;


/**
 * Access to the service that keeps track of device idleness and drives low power mode based on
 * that.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DeviceIdleManager {

DeviceIdleManager() { throw new RuntimeException("Stub!"); }

/**
 * @return package names the system has white-listed to opt out of power save restrictions,
 * except for device idle mode.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String[] getSystemPowerWhitelistExceptIdle() { throw new RuntimeException("Stub!"); }

/**
 * @return package names the system has white-listed to opt out of power save restrictions for
 * all modes.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public java.lang.String[] getSystemPowerWhitelist() { throw new RuntimeException("Stub!"); }
}

