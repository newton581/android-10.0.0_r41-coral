/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.euicc;


/**
 * Result of a {@link EuiccService#onGetEuiccProfileInfoList} operation.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class GetEuiccProfileInfoListResult implements android.os.Parcelable {

/**
 * Construct a new {@link GetEuiccProfileInfoListResult}.
 *
 * @param result Result of the operation. May be one of the predefined {@code RESULT_} constants
 *     in EuiccService or any implementation-specific code starting with
 *     {@link EuiccService#RESULT_FIRST_USER}.
 * @param profiles the list of profiles. Should only be provided if the result is
 *     {@link EuiccService#RESULT_OK}.
 * This value may be {@code null}.
 * @param isRemovable whether the eUICC in this slot is removable. If true, the profiles
 *     returned here will only be considered accessible as long as this eUICC is present.
 *     Otherwise, they will remain accessible until the next time a response with isRemovable
 *     set to false is returned.
 * @apiSince REL
 */

public GetEuiccProfileInfoListResult(int result, @android.annotation.Nullable android.service.euicc.EuiccProfileInfo[] profiles, boolean isRemovable) { throw new RuntimeException("Stub!"); }

/**
 * Gets the result of the operation.
 *
 * <p>May be one of the predefined {@code RESULT_} constants in EuiccService or any
 * implementation-specific code starting with {@link EuiccService#RESULT_FIRST_USER}.
 * @apiSince REL
 */

public int getResult() { throw new RuntimeException("Stub!"); }

/**
 * Gets the profile list (only upon success).
 * @return This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public java.util.List<android.service.euicc.EuiccProfileInfo> getProfiles() { throw new RuntimeException("Stub!"); }

/**
 * Gets whether the eUICC is removable.
 * @apiSince REL
 */

public boolean getIsRemovable() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.service.euicc.GetEuiccProfileInfoListResult> CREATOR;
static { CREATOR = null; }
}

