/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.mbms;

import android.net.Uri;
import android.telephony.mbms.vendor.VendorUtils;
import android.content.ContentResolver;

/**
 * Wrapper for a pair of {@link Uri}s that describe a temp file used by the middleware to
 * download files via cell-broadcast.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class UriPathPair implements android.os.Parcelable {

/** @hide */

UriPathPair(android.os.Parcel in) { throw new RuntimeException("Stub!"); }

/**
 * Returns the file-path {@link Uri}. This has scheme {@code file} and points to the actual
 * location on disk where the temp file resides. Use this when sending {@link Uri}s back to the
 * app in the intents in {@link VendorUtils}.
 * @return A {@code file} {@link Uri}.
 * @apiSince REL
 */

public android.net.Uri getFilePathUri() { throw new RuntimeException("Stub!"); }

/**
 * Returns the content {@link Uri} that may be used with
 * {@link ContentResolver#openFileDescriptor(Uri, String)} to obtain a
 * {@link android.os.ParcelFileDescriptor} to a temp file to write to. This {@link Uri} will
 * expire if the middleware process dies.
 * @return A {@code content} {@link Uri}
 * @apiSince REL
 */

public android.net.Uri getContentUri() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.mbms.UriPathPair> CREATOR;
static { CREATOR = null; }
}

