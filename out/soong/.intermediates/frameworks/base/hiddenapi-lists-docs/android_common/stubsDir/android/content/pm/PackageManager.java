/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.content.pm;

import android.content.Context;
import android.content.Intent;
import android.os.UserHandle;
import android.Manifest;
import java.util.List;
import android.content.ComponentName;
import android.graphics.drawable.Drawable;
import android.graphics.Rect;
import android.content.res.Resources;
import android.content.IntentFilter;
import java.util.Set;
import android.content.IntentSender;
import android.app.usage.StorageStatsManager;
import java.util.Locale;
import android.os.PersistableBundle;
import android.os.Bundle;
import android.content.pm.dex.ArtManager;
import android.os.UserManager;
import android.os.storage.StorageManager;
import android.app.ActivityManager;
import android.net.wifi.WifiManager;
import android.app.admin.DevicePolicyManager;

/**
 * Class for retrieving various kinds of information related to the application
 * packages that are currently installed on the device.
 *
 * You can find this class through {@link Context#getPackageManager}.
 * @apiSince 1
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class PackageManager {

public PackageManager() { throw new RuntimeException("Stub!"); }

/**
 * Retrieve overall information about an application package that is
 * installed on the system.
 *
 * @param packageName The full name (i.e. com.google.apps.contacts) of the
 *            desired package.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_ACTIVITIES}, {@link android.content.pm.PackageManager#GET_CONFIGURATIONS}, {@link android.content.pm.PackageManager#GET_GIDS}, {@link android.content.pm.PackageManager#GET_INSTRUMENTATION}, {@link android.content.pm.PackageManager#GET_INTENT_FILTERS}, {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_PERMISSIONS}, {@link android.content.pm.PackageManager#GET_PROVIDERS}, {@link android.content.pm.PackageManager#GET_RECEIVERS}, {@link android.content.pm.PackageManager#GET_SERVICES}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#GET_SIGNATURES}, {@link android.content.pm.PackageManager#GET_SIGNING_CERTIFICATES}, {@link android.content.pm.PackageManager#GET_URI_PERMISSION_PATTERNS}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_FACTORY_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#MATCH_APEX}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @return A PackageInfo object containing information about the package. If
 *         flag {@code MATCH_UNINSTALLED_PACKAGES} is set and if the package
 *         is not found in the list of installed applications, the package
 *         information is retrieved from the list of uninstalled
 *         applications (which includes installed applications as well as
 *         applications with data directory i.e. applications which had been
 *         deleted with {@code DONT_DELETE_DATA} flag set).
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 1
 */

public abstract android.content.pm.PackageInfo getPackageInfo(@androidx.annotation.RecentlyNonNull java.lang.String packageName, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve overall information about an application package that is
 * installed on the system. This method can be used for retrieving
 * information about packages for which multiple versions can be installed
 * at the time. Currently only packages hosting static shared libraries can
 * have multiple installed versions. The method can also be used to get info
 * for a package that has a single version installed by passing
 * {@link #VERSION_CODE_HIGHEST} in the {@link VersionedPackage}
 * constructor.
 *
 * @param versionedPackage The versioned package for which to query.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_ACTIVITIES}, {@link android.content.pm.PackageManager#GET_CONFIGURATIONS}, {@link android.content.pm.PackageManager#GET_GIDS}, {@link android.content.pm.PackageManager#GET_INSTRUMENTATION}, {@link android.content.pm.PackageManager#GET_INTENT_FILTERS}, {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_PERMISSIONS}, {@link android.content.pm.PackageManager#GET_PROVIDERS}, {@link android.content.pm.PackageManager#GET_RECEIVERS}, {@link android.content.pm.PackageManager#GET_SERVICES}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#GET_SIGNATURES}, {@link android.content.pm.PackageManager#GET_SIGNING_CERTIFICATES}, {@link android.content.pm.PackageManager#GET_URI_PERMISSION_PATTERNS}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_FACTORY_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#MATCH_APEX}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @return A PackageInfo object containing information about the package. If
 *         flag {@code MATCH_UNINSTALLED_PACKAGES} is set and if the package
 *         is not found in the list of installed applications, the package
 *         information is retrieved from the list of uninstalled
 *         applications (which includes installed applications as well as
 *         applications with data directory i.e. applications which had been
 *         deleted with {@code DONT_DELETE_DATA} flag set).
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 26
 */

public abstract android.content.pm.PackageInfo getPackageInfo(@androidx.annotation.RecentlyNonNull android.content.pm.VersionedPackage versionedPackage, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Map from the current package names in use on the device to whatever
 * the current canonical name of that package is.
 * @param packageNames Array of current names to be mapped.
 * This value must never be {@code null}.
 * @return Returns an array of the same size as the original, containing
 * the canonical name for each package.
 * @apiSince 8
 */

public abstract java.lang.String[] currentToCanonicalPackageNames(@androidx.annotation.RecentlyNonNull java.lang.String[] packageNames);

/**
 * Map from a packages canonical name to the current name in use on the device.
 * @param packageNames Array of new names to be mapped.
 * This value must never be {@code null}.
 * @return Returns an array of the same size as the original, containing
 * the current name for each package.
 * @apiSince 8
 */

public abstract java.lang.String[] canonicalToCurrentPackageNames(@androidx.annotation.RecentlyNonNull java.lang.String[] packageNames);

/**
 * Returns a "good" intent to launch a front-door activity in a package.
 * This is used, for example, to implement an "open" button when browsing
 * through packages.  The current implementation looks first for a main
 * activity in the category {@link Intent#CATEGORY_INFO}, and next for a
 * main activity in the category {@link Intent#CATEGORY_LAUNCHER}. Returns
 * <code>null</code> if neither are found.
 *
 * @param packageName The name of the package to inspect.
 *
 * This value must never be {@code null}.
 * @return A fully-qualified {@link Intent} that can be used to launch the
 * main activity in the package. Returns <code>null</code> if the package
 * does not contain such an activity, or if <em>packageName</em> is not
 * recognized.
 * @apiSince 3
 */

@android.annotation.Nullable
public abstract android.content.Intent getLaunchIntentForPackage(@android.annotation.NonNull java.lang.String packageName);

/**
 * Return a "good" intent to launch a front-door Leanback activity in a
 * package, for use for example to implement an "open" button when browsing
 * through packages. The current implementation will look for a main
 * activity in the category {@link Intent#CATEGORY_LEANBACK_LAUNCHER}, or
 * return null if no main leanback activities are found.
 *
 * @param packageName The name of the package to inspect.
 * This value must never be {@code null}.
 * @return Returns either a fully-qualified Intent that can be used to launch
 *         the main Leanback activity in the package, or null if the package
 *         does not contain such an activity.
 * @apiSince 21
 */

@android.annotation.Nullable
public abstract android.content.Intent getLeanbackLaunchIntentForPackage(@android.annotation.NonNull java.lang.String packageName);

/**
 * Return an array of all of the POSIX secondary group IDs that have been
 * assigned to the given package.
 * <p>
 * Note that the same package may have different GIDs under different
 * {@link UserHandle} on the same device.
 *
 * @param packageName The full name (i.e. com.google.apps.contacts) of the
 *            desired package.
 * This value must never be {@code null}.
 * @return Returns an int array of the assigned GIDs, or null if there are
 *         none.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 1
 */

public abstract int[] getPackageGids(@android.annotation.NonNull java.lang.String packageName) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Return an array of all of the POSIX secondary group IDs that have been
 * assigned to the given package.
 * <p>
 * Note that the same package may have different GIDs under different
 * {@link UserHandle} on the same device.
 *
 * @param packageName The full name (i.e. com.google.apps.contacts) of the
 *            desired package.
 * This value must never be {@code null}.
 * @param flags Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_ACTIVITIES}, {@link android.content.pm.PackageManager#GET_CONFIGURATIONS}, {@link android.content.pm.PackageManager#GET_GIDS}, {@link android.content.pm.PackageManager#GET_INSTRUMENTATION}, {@link android.content.pm.PackageManager#GET_INTENT_FILTERS}, {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_PERMISSIONS}, {@link android.content.pm.PackageManager#GET_PROVIDERS}, {@link android.content.pm.PackageManager#GET_RECEIVERS}, {@link android.content.pm.PackageManager#GET_SERVICES}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#GET_SIGNATURES}, {@link android.content.pm.PackageManager#GET_SIGNING_CERTIFICATES}, {@link android.content.pm.PackageManager#GET_URI_PERMISSION_PATTERNS}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_FACTORY_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#MATCH_APEX}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @return Returns an int array of the assigned gids, or null if there are
 *         none.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 24
 */

public abstract int[] getPackageGids(@androidx.annotation.RecentlyNonNull java.lang.String packageName, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Return the UID associated with the given package name.
 * <p>
 * Note that the same package will have different UIDs under different
 * {@link UserHandle} on the same device.
 *
 * @param packageName The full name (i.e. com.google.apps.contacts) of the
 *            desired package.
 * This value must never be {@code null}.
 * @param flags Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_ACTIVITIES}, {@link android.content.pm.PackageManager#GET_CONFIGURATIONS}, {@link android.content.pm.PackageManager#GET_GIDS}, {@link android.content.pm.PackageManager#GET_INSTRUMENTATION}, {@link android.content.pm.PackageManager#GET_INTENT_FILTERS}, {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_PERMISSIONS}, {@link android.content.pm.PackageManager#GET_PROVIDERS}, {@link android.content.pm.PackageManager#GET_RECEIVERS}, {@link android.content.pm.PackageManager#GET_SERVICES}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#GET_SIGNATURES}, {@link android.content.pm.PackageManager#GET_SIGNING_CERTIFICATES}, {@link android.content.pm.PackageManager#GET_URI_PERMISSION_PATTERNS}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_FACTORY_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#MATCH_APEX}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @return Returns an integer UID who owns the given package name.
 * @throws NameNotFoundException if a package with the given name can not be
 *             found on the system.
 * @apiSince 24
 */

public abstract int getPackageUid(@androidx.annotation.RecentlyNonNull java.lang.String packageName, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve all of the information we know about a particular permission.
 *
 * @param permissionName The fully qualified name (i.e. com.google.permission.LOGIN)
 *            of the permission you are interested in.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or {@link android.content.pm.PackageManager#GET_META_DATA}
 * @return Returns a {@link PermissionInfo} containing information about the
 *         permission.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 1
 */

public abstract android.content.pm.PermissionInfo getPermissionInfo(@androidx.annotation.RecentlyNonNull java.lang.String permissionName, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Query for all of the permissions associated with a particular group.
 *
 * @param permissionGroup The fully qualified name (i.e. com.google.permission.LOGIN)
 *            of the permission group you are interested in. Use null to
 *            find all of the permissions not associated with a group.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or {@link android.content.pm.PackageManager#GET_META_DATA}
 * @return Returns a list of {@link PermissionInfo} containing information
 *         about all of the permissions in the given group.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.PermissionInfo> queryPermissionsByGroup(@androidx.annotation.RecentlyNonNull java.lang.String permissionGroup, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Returns true if some permissions are individually controlled.
 *
 * <p>The user usually grants and revokes permission-groups. If this option is set some
 * dangerous system permissions can be revoked/granted by the user separately from their group.
 *
 * @hide
 */

public abstract boolean arePermissionsIndividuallyControlled();

/**
 * Retrieve all of the information we know about a particular group of
 * permissions.
 *
 * @param permissionName The fully qualified name (i.e.
 *            com.google.permission_group.APPS) of the permission you are
 *            interested in.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or {@link android.content.pm.PackageManager#GET_META_DATA}
 * @return Returns a {@link PermissionGroupInfo} containing information
 *         about the permission.
 * This value will never be {@code null}.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.pm.PermissionGroupInfo getPermissionGroupInfo(@androidx.annotation.RecentlyNonNull java.lang.String permissionName, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve all of the known permission groups in the system.
 *
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or {@link android.content.pm.PackageManager#GET_META_DATA}
 * @return Returns a list of {@link PermissionGroupInfo} containing
 *         information about all of the known permission groups.
 
 * This value will never be {@code null}.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.PermissionGroupInfo> getAllPermissionGroups(int flags);

/**
 * Retrieve all of the information we know about a particular
 * package/application.
 *
 * @param packageName The full name (i.e. com.google.apps.contacts) of an
 *            application.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, android.content.pm.PackageManager.MATCH_STATIC_SHARED_LIBRARIES, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @return An {@link ApplicationInfo} containing information about the
 *         package. If flag {@code MATCH_UNINSTALLED_PACKAGES} is set and if
 *         the package is not found in the list of installed applications,
 *         the application information is retrieved from the list of
 *         uninstalled applications (which includes installed applications
 *         as well as applications with data directory i.e. applications
 *         which had been deleted with {@code DONT_DELETE_DATA} flag set).
 * This value will never be {@code null}.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.pm.ApplicationInfo getApplicationInfo(@androidx.annotation.RecentlyNonNull java.lang.String packageName, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve all of the information we know about a particular
 * package/application, for a specific user.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS}
 * @param packageName The full name (i.e. com.google.apps.contacts) of an
 *            application.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, android.content.pm.PackageManager.MATCH_STATIC_SHARED_LIBRARIES, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @param user This value must never be {@code null}.
 * @return An {@link ApplicationInfo} containing information about the
 *         package. If flag {@code MATCH_UNINSTALLED_PACKAGES} is set and if
 *         the package is not found in the list of installed applications,
 *         the application information is retrieved from the list of
 *         uninstalled applications (which includes installed applications
 *         as well as applications with data directory i.e. applications
 *         which had been deleted with {@code DONT_DELETE_DATA} flag set).
 * This value will never be {@code null}.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @hide
 */

@android.annotation.NonNull
public android.content.pm.ApplicationInfo getApplicationInfoAsUser(@android.annotation.NonNull java.lang.String packageName, int flags, @android.annotation.NonNull android.os.UserHandle user) throws android.content.pm.PackageManager.NameNotFoundException { throw new RuntimeException("Stub!"); }

/**
 * Retrieve all of the information we know about a particular activity
 * class.
 *
 * @param component The full component name (i.e.
 *            com.google.apps.contacts/com.google.apps.contacts.
 *            ContactsList) of an Activity class.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, android.content.pm.PackageManager.MATCH_STATIC_SHARED_LIBRARIES, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return An {@link ActivityInfo} containing information about the
 *         activity.
 * This value will never be {@code null}.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.pm.ActivityInfo getActivityInfo(@androidx.annotation.RecentlyNonNull android.content.ComponentName component, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve all of the information we know about a particular receiver
 * class.
 *
 * @param component The full component name (i.e.
 *            com.google.apps.calendar/com.google.apps.calendar.
 *            CalendarAlarm) of a Receiver class.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, android.content.pm.PackageManager.MATCH_STATIC_SHARED_LIBRARIES, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return An {@link ActivityInfo} containing information about the
 *         receiver.
 * This value will never be {@code null}.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.pm.ActivityInfo getReceiverInfo(@androidx.annotation.RecentlyNonNull android.content.ComponentName component, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve all of the information we know about a particular service class.
 *
 * @param component The full component name (i.e.
 *            com.google.apps.media/com.google.apps.media.
 *            BackgroundPlayback) of a Service class.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, android.content.pm.PackageManager.MATCH_STATIC_SHARED_LIBRARIES, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return A {@link ServiceInfo} object containing information about the
 *         service.
 * This value will never be {@code null}.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.pm.ServiceInfo getServiceInfo(@androidx.annotation.RecentlyNonNull android.content.ComponentName component, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve all of the information we know about a particular content
 * provider class.
 *
 * @param component The full component name (i.e.
 *            com.google.providers.media/com.google.providers.media.
 *            MediaProvider) of a ContentProvider class.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, android.content.pm.PackageManager.MATCH_STATIC_SHARED_LIBRARIES, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return A {@link ProviderInfo} object containing information about the
 *         provider.
 * This value will never be {@code null}.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 9
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.pm.ProviderInfo getProviderInfo(@androidx.annotation.RecentlyNonNull android.content.ComponentName component, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve information for a particular module.
 *
 * @param packageName The name of the module.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or {@link android.content.pm.PackageManager#MATCH_ALL}
 * @return A {@link ModuleInfo} object containing information about the
 *         module.
 * This value will never be {@code null}.
 * @throws NameNotFoundException if a module with the given name cannot be
 *             found on the system.
 * @apiSince 29
 */

@android.annotation.NonNull
public android.content.pm.ModuleInfo getModuleInfo(@android.annotation.NonNull java.lang.String packageName, int flags) throws android.content.pm.PackageManager.NameNotFoundException { throw new RuntimeException("Stub!"); }

/**
 * Return a List of all modules that are installed.
 *
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or {@link android.content.pm.PackageManager#MATCH_ALL}
 * @return A {@link List} of {@link ModuleInfo} objects, one for each installed
 *         module, containing information about the module. In the unlikely case
 *         there are no installed modules, an empty list is returned.
 
 * This value will never be {@code null}.
 * @apiSince 29
 */

@android.annotation.NonNull
public java.util.List<android.content.pm.ModuleInfo> getInstalledModules(int flags) { throw new RuntimeException("Stub!"); }

/**
 * Return a List of all packages that are installed for the current user.
 *
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_ACTIVITIES}, {@link android.content.pm.PackageManager#GET_CONFIGURATIONS}, {@link android.content.pm.PackageManager#GET_GIDS}, {@link android.content.pm.PackageManager#GET_INSTRUMENTATION}, {@link android.content.pm.PackageManager#GET_INTENT_FILTERS}, {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_PERMISSIONS}, {@link android.content.pm.PackageManager#GET_PROVIDERS}, {@link android.content.pm.PackageManager#GET_RECEIVERS}, {@link android.content.pm.PackageManager#GET_SERVICES}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#GET_SIGNATURES}, {@link android.content.pm.PackageManager#GET_SIGNING_CERTIFICATES}, {@link android.content.pm.PackageManager#GET_URI_PERMISSION_PATTERNS}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_FACTORY_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#MATCH_APEX}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @return A List of PackageInfo objects, one for each installed package,
 *         containing information about the package. In the unlikely case
 *         there are no installed packages, an empty list is returned. If
 *         flag {@code MATCH_UNINSTALLED_PACKAGES} is set, the package
 *         information is retrieved from the list of uninstalled
 *         applications (which includes installed applications as well as
 *         applications with data directory i.e. applications which had been
 *         deleted with {@code DONT_DELETE_DATA} flag set).
 
 * This value will never be {@code null}.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.PackageInfo> getInstalledPackages(int flags);

/**
 * Return a List of all installed packages that are currently holding any of
 * the given permissions.
 *
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_ACTIVITIES}, {@link android.content.pm.PackageManager#GET_CONFIGURATIONS}, {@link android.content.pm.PackageManager#GET_GIDS}, {@link android.content.pm.PackageManager#GET_INSTRUMENTATION}, {@link android.content.pm.PackageManager#GET_INTENT_FILTERS}, {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_PERMISSIONS}, {@link android.content.pm.PackageManager#GET_PROVIDERS}, {@link android.content.pm.PackageManager#GET_RECEIVERS}, {@link android.content.pm.PackageManager#GET_SERVICES}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#GET_SIGNATURES}, {@link android.content.pm.PackageManager#GET_SIGNING_CERTIFICATES}, {@link android.content.pm.PackageManager#GET_URI_PERMISSION_PATTERNS}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_FACTORY_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#MATCH_APEX}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @param permissions This value must never be {@code null}.
 * @return A List of PackageInfo objects, one for each installed package
 *         that holds any of the permissions that were provided, containing
 *         information about the package. If no installed packages hold any
 *         of the permissions, an empty list is returned. If flag
 *         {@code MATCH_UNINSTALLED_PACKAGES} is set, the package
 *         information is retrieved from the list of uninstalled
 *         applications (which includes installed applications as well as
 *         applications with data directory i.e. applications which had been
 *         deleted with {@code DONT_DELETE_DATA} flag set).
 
 * This value will never be {@code null}.
 * @apiSince 18
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.PackageInfo> getPackagesHoldingPermissions(@androidx.annotation.RecentlyNonNull java.lang.String[] permissions, int flags);

/**
 * Return a List of all packages that are installed on the device, for a
 * specific user.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS_FULL}
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_ACTIVITIES}, {@link android.content.pm.PackageManager#GET_CONFIGURATIONS}, {@link android.content.pm.PackageManager#GET_GIDS}, {@link android.content.pm.PackageManager#GET_INSTRUMENTATION}, {@link android.content.pm.PackageManager#GET_INTENT_FILTERS}, {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_PERMISSIONS}, {@link android.content.pm.PackageManager#GET_PROVIDERS}, {@link android.content.pm.PackageManager#GET_RECEIVERS}, {@link android.content.pm.PackageManager#GET_SERVICES}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#GET_SIGNATURES}, {@link android.content.pm.PackageManager#GET_SIGNING_CERTIFICATES}, {@link android.content.pm.PackageManager#GET_URI_PERMISSION_PATTERNS}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_FACTORY_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#MATCH_APEX}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @param userId The user for whom the installed packages are to be listed
 * @return A List of PackageInfo objects, one for each installed package,
 *         containing information about the package. In the unlikely case
 *         there are no installed packages, an empty list is returned. If
 *         flag {@code MATCH_UNINSTALLED_PACKAGES} is set, the package
 *         information is retrieved from the list of uninstalled
 *         applications (which includes installed applications as well as
 *         applications with data directory i.e. applications which had been
 *         deleted with {@code DONT_DELETE_DATA} flag set).
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public abstract java.util.List<android.content.pm.PackageInfo> getInstalledPackagesAsUser(int flags, int userId);

/**
 * Check whether a particular package has been granted a particular
 * permission.
 *
 * @param permissionName The name of the permission you are checking for.
 * This value must never be {@code null}.
 * @param packageName The name of the package you are checking against.
 *
 * This value must never be {@code null}.
 * @return If the package has the permission, PERMISSION_GRANTED is
 * returned.  If it does not have the permission, PERMISSION_DENIED
 * is returned.
 *
 * Value is {@link android.content.pm.PackageManager#PERMISSION_GRANTED}, or {@link android.content.pm.PackageManager#PERMISSION_DENIED}
 * @see #PERMISSION_GRANTED
 * @see #PERMISSION_DENIED
 * @apiSince 1
 */

public abstract int checkPermission(@androidx.annotation.RecentlyNonNull java.lang.String permissionName, @androidx.annotation.RecentlyNonNull java.lang.String packageName);

/**
 * Checks whether a particular permissions has been revoked for a
 * package by policy. Typically the device owner or the profile owner
 * may apply such a policy. The user cannot grant policy revoked
 * permissions, hence the only way for an app to get such a permission
 * is by a policy change.
 *
 * @param permissionName The name of the permission you are checking for.
 * This value must never be {@code null}.
 * @param packageName The name of the package you are checking against.
 *
 * This value must never be {@code null}.
 * @return Whether the permission is restricted by policy.
 * @apiSince 23
 */

public abstract boolean isPermissionRevokedByPolicy(@android.annotation.NonNull java.lang.String permissionName, @android.annotation.NonNull java.lang.String packageName);

/**
 * Gets the package name of the component controlling runtime permissions.
 *
 * @return The package name.
 *
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public abstract java.lang.String getPermissionControllerPackageName();

/**
 * Add a new dynamic permission to the system.  For this to work, your
 * package must have defined a permission tree through the
 * {@link android.R.styleable#AndroidManifestPermissionTree
 * &lt;permission-tree&gt;} tag in its manifest.  A package can only add
 * permissions to trees that were defined by either its own package or
 * another with the same user id; a permission is in a tree if it
 * matches the name of the permission tree + ".": for example,
 * "com.foo.bar" is a member of the permission tree "com.foo".
 *
 * <p>It is good to make your permission tree name descriptive, because you
 * are taking possession of that entire set of permission names.  Thus, it
 * must be under a domain you control, with a suffix that will not match
 * any normal permissions that may be declared in any applications that
 * are part of that domain.
 *
 * <p>New permissions must be added before
 * any .apks are installed that use those permissions.  Permissions you
 * add through this method are remembered across reboots of the device.
 * If the given permission already exists, the info you supply here
 * will be used to update it.
 *
 * @param info Description of the permission to be added.
 *
 * This value must never be {@code null}.
 * @return Returns true if a new permission was created, false if an
 * existing one was updated.
 *
 * @throws SecurityException if you are not allowed to add the
 * given permission name.
 *
 * @see #removePermission(String)
 * @apiSince 1
 */

public abstract boolean addPermission(@androidx.annotation.RecentlyNonNull android.content.pm.PermissionInfo info);

/**
 * Like {@link #addPermission(PermissionInfo)} but asynchronously
 * persists the package manager state after returning from the call,
 * allowing it to return quicker and batch a series of adds at the
 * expense of no guarantee the added permission will be retained if
 * the device is rebooted before it is written.
 
 * @param info This value must never be {@code null}.
 * @apiSince 8
 */

public abstract boolean addPermissionAsync(@androidx.annotation.RecentlyNonNull android.content.pm.PermissionInfo info);

/**
 * Removes a permission that was previously added with
 * {@link #addPermission(PermissionInfo)}.  The same ownership rules apply
 * -- you are only allowed to remove permissions that you are allowed
 * to add.
 *
 * @param permissionName The name of the permission to remove.
 *
 * This value must never be {@code null}.
 * @throws SecurityException if you are not allowed to remove the
 * given permission name.
 *
 * @see #addPermission(PermissionInfo)
 * @apiSince 1
 */

public abstract void removePermission(@androidx.annotation.RecentlyNonNull java.lang.String permissionName);

/**
 * Grant a runtime permission to an application which the application does not
 * already have. The permission must have been requested by the application.
 * If the application is not allowed to hold the permission, a {@link
 * java.lang.SecurityException} is thrown. If the package or permission is
 * invalid, a {@link java.lang.IllegalArgumentException} is thrown.
 * <p>
 * <strong>Note: </strong>Using this API requires holding
 * android.permission.GRANT_RUNTIME_PERMISSIONS and if the user id is
 * not the current user android.permission.INTERACT_ACROSS_USERS_FULL.
 * </p>
 *
 * <br>
 * Requires {@link android.Manifest.permission#GRANT_RUNTIME_PERMISSIONS}
 * @param packageName The package to which to grant the permission.
 * This value must never be {@code null}.
 * @param permissionName The permission name to grant.
 * This value must never be {@code null}.
 * @param user The user for which to grant the permission.
 *
 * This value must never be {@code null}.
 * @see #revokeRuntimePermission(String, String, android.os.UserHandle)
 *
 * @hide
 */

public abstract void grantRuntimePermission(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull java.lang.String permissionName, @android.annotation.NonNull android.os.UserHandle user);

/**
 * Revoke a runtime permission that was previously granted by {@link
 * #grantRuntimePermission(String, String, android.os.UserHandle)}. The
 * permission must have been requested by and granted to the application.
 * If the application is not allowed to hold the permission, a {@link
 * java.lang.SecurityException} is thrown. If the package or permission is
 * invalid, a {@link java.lang.IllegalArgumentException} is thrown.
 * <p>
 * <strong>Note: </strong>Using this API requires holding
 * android.permission.REVOKE_RUNTIME_PERMISSIONS and if the user id is
 * not the current user android.permission.INTERACT_ACROSS_USERS_FULL.
 * </p>
 *
 * <br>
 * Requires {@link android.Manifest.permission#REVOKE_RUNTIME_PERMISSIONS}
 * @param packageName The package from which to revoke the permission.
 * This value must never be {@code null}.
 * @param permissionName The permission name to revoke.
 * This value must never be {@code null}.
 * @param user The user for which to revoke the permission.
 *
 * This value must never be {@code null}.
 * @see #grantRuntimePermission(String, String, android.os.UserHandle)
 *
 * @hide
 */

public abstract void revokeRuntimePermission(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull java.lang.String permissionName, @android.annotation.NonNull android.os.UserHandle user);

/**
 * Gets the state flags associated with a permission.
 *
 * <br>
 * Requires {@link android.Manifest.permission#GRANT_RUNTIME_PERMISSIONS} or {@link android.Manifest.permission#REVOKE_RUNTIME_PERMISSIONS} or {@link android.Manifest.permission#GET_RUNTIME_PERMISSIONS}
 * @param permissionName The permission for which to get the flags.
 * This value must never be {@code null}.
 * @param packageName The package name for which to get the flags.
 * This value must never be {@code null}.
 * @param user The user for which to get permission flags.
 * This value must never be {@code null}.
 * @return The permission flags.
 *
 * Value is {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SET}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_POLICY_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_REVOKE_ON_UPGRADE}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_SYSTEM_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_GRANTED_BY_DEFAULT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SENSITIVE_WHEN_GRANTED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SENSITIVE_WHEN_DENIED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_UPGRADE_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_SYSTEM_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_INSTALLER_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_APPLY_RESTRICTION}, or {@link android.content.pm.PackageManager#FLAG_PERMISSION_GRANTED_BY_ROLE}
 * @hide
 */

public abstract int getPermissionFlags(@android.annotation.NonNull java.lang.String permissionName, @android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull android.os.UserHandle user);

/**
 * Updates the flags associated with a permission by replacing the flags in
 * the specified mask with the provided flag values.
 *
 * <br>
 * Requires {@link android.Manifest.permission#GRANT_RUNTIME_PERMISSIONS} or {@link android.Manifest.permission#REVOKE_RUNTIME_PERMISSIONS}
 * @param permissionName The permission for which to update the flags.
 * This value must never be {@code null}.
 * @param packageName The package name for which to update the flags.
 * This value must never be {@code null}.
 * @param flagMask The flags which to replace.
 * Value is {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SET}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_POLICY_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_REVOKE_ON_UPGRADE}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_SYSTEM_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_GRANTED_BY_DEFAULT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SENSITIVE_WHEN_GRANTED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SENSITIVE_WHEN_DENIED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_UPGRADE_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_SYSTEM_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_INSTALLER_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_APPLY_RESTRICTION}, or {@link android.content.pm.PackageManager#FLAG_PERMISSION_GRANTED_BY_ROLE}
 * @param flagValues The flags with which to replace.
 * Value is {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SET}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_POLICY_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_REVOKE_ON_UPGRADE}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_SYSTEM_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_GRANTED_BY_DEFAULT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SENSITIVE_WHEN_GRANTED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SENSITIVE_WHEN_DENIED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_UPGRADE_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_SYSTEM_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_INSTALLER_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_APPLY_RESTRICTION}, or {@link android.content.pm.PackageManager#FLAG_PERMISSION_GRANTED_BY_ROLE}
 * @param user The user for which to update the permission flags.
 *
 * This value must never be {@code null}.
 * @hide
 */

public abstract void updatePermissionFlags(@android.annotation.NonNull java.lang.String permissionName, @android.annotation.NonNull java.lang.String packageName, int flagMask, int flagValues, @android.annotation.NonNull android.os.UserHandle user);

/**
 * Gets the restricted permissions that have been whitelisted and the app
 * is allowed to have them granted in their full form.
 *
 * <p> Permissions can be hard restricted which means that the app cannot hold
 * them or soft restricted where the app can hold the permission but in a weaker
 * form. Whether a permission is {@link PermissionInfo#FLAG_HARD_RESTRICTED hard
 * restricted} or {@link PermissionInfo#FLAG_SOFT_RESTRICTED soft restricted}
 * depends on the permission declaration. Whitelisting a hard restricted permission
 * allows for the to hold that permission and whitelisting a soft restricted
 * permission allows the app to hold the permission in its full, unrestricted form.
 *
 * <p><ol>There are three whitelists:
 *
 * <li>one for cases where the system permission policy whitelists a permission
 * This list corresponds to the{@link #FLAG_PERMISSION_WHITELIST_SYSTEM} flag.
 * Can only be accessed by pre-installed holders of a dedicated permission.
 *
 * <li>one for cases where the system whitelists the permission when upgrading
 * from an OS version in which the permission was not restricted to an OS version
 * in which the permission is restricted. This list corresponds to the {@link
 * #FLAG_PERMISSION_WHITELIST_UPGRADE} flag. Can be accessed by pre-installed
 * holders of a dedicated permission or the installer on record.
 *
 * <li>one for cases where the installer of the package whitelists a permission.
 * This list corresponds to the {@link #FLAG_PERMISSION_WHITELIST_INSTALLER} flag.
 * Can be accessed by pre-installed holders of a dedicated permission or the
 * installer on record.
 *
 * @param packageName The app for which to get whitelisted permissions.
 * This value must never be {@code null}.
 * @param whitelistFlag The flag to determine which whitelist to query. Only one flag
 * can be passed.s
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#FLAG_PERMISSION_WHITELIST_SYSTEM}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_WHITELIST_INSTALLER}, and {@link android.content.pm.PackageManager#FLAG_PERMISSION_WHITELIST_UPGRADE}
 * @return The whitelisted permissions that are on any of the whitelists you query for.
 *
 * This value will never be {@code null}.
 * @see #addWhitelistedRestrictedPermission(String, String, int)
 * @see #removeWhitelistedRestrictedPermission(String, String, int)
 * @see #FLAG_PERMISSION_WHITELIST_SYSTEM
 * @see #FLAG_PERMISSION_WHITELIST_UPGRADE
 * @see #FLAG_PERMISSION_WHITELIST_INSTALLER
 *
 * @throws SecurityException if you try to access a whitelist that you have no access to.
 * @apiSince 29
 */

@android.annotation.NonNull
public java.util.Set<java.lang.String> getWhitelistedRestrictedPermissions(@android.annotation.NonNull java.lang.String packageName, int whitelistFlag) { throw new RuntimeException("Stub!"); }

/**
 * Adds a whitelisted restricted permission for an app.
 *
 * <p> Permissions can be hard restricted which means that the app cannot hold
 * them or soft restricted where the app can hold the permission but in a weaker
 * form. Whether a permission is {@link PermissionInfo#FLAG_HARD_RESTRICTED hard
 * restricted} or {@link PermissionInfo#FLAG_SOFT_RESTRICTED soft restricted}
 * depends on the permission declaration. Whitelisting a hard restricted permission
 * allows for the to hold that permission and whitelisting a soft restricted
 * permission allows the app to hold the permission in its full, unrestricted form.
 *
 * <p><ol>There are three whitelists:
 *
 * <li>one for cases where the system permission policy whitelists a permission
 * This list corresponds to the {@link #FLAG_PERMISSION_WHITELIST_SYSTEM} flag.
 * Can only be modified by pre-installed holders of a dedicated permission.
 *
 * <li>one for cases where the system whitelists the permission when upgrading
 * from an OS version in which the permission was not restricted to an OS version
 * in which the permission is restricted. This list corresponds to the {@link
 * #FLAG_PERMISSION_WHITELIST_UPGRADE} flag. Can be modified by pre-installed
 * holders of a dedicated permission. The installer on record can only remove
 * permissions from this whitelist.
 *
 * <li>one for cases where the installer of the package whitelists a permission.
 * This list corresponds to the {@link #FLAG_PERMISSION_WHITELIST_INSTALLER} flag.
 * Can be modified by pre-installed holders of a dedicated permission or the installer
 * on record.
 *
 * <p>You need to specify the whitelists for which to set the whitelisted permissions
 * which will clear the previous whitelisted permissions and replace them with the
 * provided ones.
 *
 * @param packageName The app for which to get whitelisted permissions.
 * This value must never be {@code null}.
 * @param permission The whitelisted permission to add.
 * This value must never be {@code null}.
 * @param whitelistFlags The whitelists to which to add. Passing multiple flags
 * updates all specified whitelists.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#FLAG_PERMISSION_WHITELIST_SYSTEM}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_WHITELIST_INSTALLER}, and {@link android.content.pm.PackageManager#FLAG_PERMISSION_WHITELIST_UPGRADE}
 * @return Whether the permission was added to the whitelist.
 *
 * @see #getWhitelistedRestrictedPermissions(String, int)
 * @see #removeWhitelistedRestrictedPermission(String, String, int)
 * @see #FLAG_PERMISSION_WHITELIST_SYSTEM
 * @see #FLAG_PERMISSION_WHITELIST_UPGRADE
 * @see #FLAG_PERMISSION_WHITELIST_INSTALLER
 *
 * @throws SecurityException if you try to modify a whitelist that you have no access to.
 * @apiSince 29
 */

public boolean addWhitelistedRestrictedPermission(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull java.lang.String permission, int whitelistFlags) { throw new RuntimeException("Stub!"); }

/**
 * Removes a whitelisted restricted permission for an app.
 *
 * <p> Permissions can be hard restricted which means that the app cannot hold
 * them or soft restricted where the app can hold the permission but in a weaker
 * form. Whether a permission is {@link PermissionInfo#FLAG_HARD_RESTRICTED hard
 * restricted} or {@link PermissionInfo#FLAG_SOFT_RESTRICTED soft restricted}
 * depends on the permission declaration. Whitelisting a hard restricted permission
 * allows for the to hold that permission and whitelisting a soft restricted
 * permission allows the app to hold the permission in its full, unrestricted form.
 *
 * <p><ol>There are three whitelists:
 *
 * <li>one for cases where the system permission policy whitelists a permission
 * This list corresponds to the {@link #FLAG_PERMISSION_WHITELIST_SYSTEM} flag.
 * Can only be modified by pre-installed holders of a dedicated permission.
 *
 * <li>one for cases where the system whitelists the permission when upgrading
 * from an OS version in which the permission was not restricted to an OS version
 * in which the permission is restricted. This list corresponds to the {@link
 * #FLAG_PERMISSION_WHITELIST_UPGRADE} flag. Can be modified by pre-installed
 * holders of a dedicated permission. The installer on record can only remove
 * permissions from this whitelist.
 *
 * <li>one for cases where the installer of the package whitelists a permission.
 * This list corresponds to the {@link #FLAG_PERMISSION_WHITELIST_INSTALLER} flag.
 * Can be modified by pre-installed holders of a dedicated permission or the installer
 * on record.
 *
 * <p>You need to specify the whitelists for which to set the whitelisted permissions
 * which will clear the previous whitelisted permissions and replace them with the
 * provided ones.
 *
 * @param packageName The app for which to get whitelisted permissions.
 * This value must never be {@code null}.
 * @param permission The whitelisted permission to remove.
 * This value must never be {@code null}.
 * @param whitelistFlags The whitelists from which to remove. Passing multiple flags
 * updates all specified whitelists.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#FLAG_PERMISSION_WHITELIST_SYSTEM}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_WHITELIST_INSTALLER}, and {@link android.content.pm.PackageManager#FLAG_PERMISSION_WHITELIST_UPGRADE}
 * @return Whether the permission was removed from the whitelist.
 *
 * @see #getWhitelistedRestrictedPermissions(String, int)
 * @see #addWhitelistedRestrictedPermission(String, String, int)
 * @see #FLAG_PERMISSION_WHITELIST_SYSTEM
 * @see #FLAG_PERMISSION_WHITELIST_UPGRADE
 * @see #FLAG_PERMISSION_WHITELIST_INSTALLER
 *
 * @throws SecurityException if you try to modify a whitelist that you have no access to.
 * @apiSince 29
 */

public boolean removeWhitelistedRestrictedPermission(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull java.lang.String permission, int whitelistFlags) { throw new RuntimeException("Stub!"); }

/**
 * Compare the signatures of two packages to determine if the same
 * signature appears in both of them.  If they do contain the same
 * signature, then they are allowed special privileges when working
 * with each other: they can share the same user-id, run instrumentation
 * against each other, etc.
 *
 * @param packageName1 First package name whose signature will be compared.
 * This value must never be {@code null}.
 * @param packageName2 Second package name whose signature will be compared.
 *
 * This value must never be {@code null}.
 * @return Returns an integer indicating whether all signatures on the
 * two packages match. The value is >= 0 ({@link #SIGNATURE_MATCH}) if
 * all signatures match or < 0 if there is not a match ({@link
 * #SIGNATURE_NO_MATCH} or {@link #SIGNATURE_UNKNOWN_PACKAGE}).
 *
 * Value is {@link android.content.pm.PackageManager#SIGNATURE_MATCH}, {@link android.content.pm.PackageManager#SIGNATURE_NEITHER_SIGNED}, {@link android.content.pm.PackageManager#SIGNATURE_FIRST_NOT_SIGNED}, {@link android.content.pm.PackageManager#SIGNATURE_SECOND_NOT_SIGNED}, {@link android.content.pm.PackageManager#SIGNATURE_NO_MATCH}, or {@link android.content.pm.PackageManager#SIGNATURE_UNKNOWN_PACKAGE}
 * @see #checkSignatures(int, int)
 * @apiSince 1
 */

public abstract int checkSignatures(@androidx.annotation.RecentlyNonNull java.lang.String packageName1, @androidx.annotation.RecentlyNonNull java.lang.String packageName2);

/**
 * Like {@link #checkSignatures(String, String)}, but takes UIDs of
 * the two packages to be checked.  This can be useful, for example,
 * when doing the check in an IPC, where the UID is the only identity
 * available.  It is functionally identical to determining the package
 * associated with the UIDs and checking their signatures.
 *
 * @param uid1 First UID whose signature will be compared.
 * @param uid2 Second UID whose signature will be compared.
 *
 * @return Returns an integer indicating whether all signatures on the
 * two packages match. The value is >= 0 ({@link #SIGNATURE_MATCH}) if
 * all signatures match or < 0 if there is not a match ({@link
 * #SIGNATURE_NO_MATCH} or {@link #SIGNATURE_UNKNOWN_PACKAGE}).
 *
 * Value is {@link android.content.pm.PackageManager#SIGNATURE_MATCH}, {@link android.content.pm.PackageManager#SIGNATURE_NEITHER_SIGNED}, {@link android.content.pm.PackageManager#SIGNATURE_FIRST_NOT_SIGNED}, {@link android.content.pm.PackageManager#SIGNATURE_SECOND_NOT_SIGNED}, {@link android.content.pm.PackageManager#SIGNATURE_NO_MATCH}, or {@link android.content.pm.PackageManager#SIGNATURE_UNKNOWN_PACKAGE}
 * @see #checkSignatures(String, String)
 * @apiSince 5
 */

public abstract int checkSignatures(int uid1, int uid2);

/**
 * Retrieve the names of all packages that are associated with a particular
 * user id.  In most cases, this will be a single package name, the package
 * that has been assigned that user id.  Where there are multiple packages
 * sharing the same user id through the "sharedUserId" mechanism, all
 * packages with that id will be returned.
 *
 * @param uid The user id for which you would like to retrieve the
 * associated packages.
 *
 * @return Returns an array of one or more packages assigned to the user
 * id, or null if there are no known packages with the given id.
 * @apiSince 1
 */

@android.annotation.Nullable
public abstract java.lang.String[] getPackagesForUid(int uid);

/**
 * Retrieve the official name associated with a uid. This name is
 * guaranteed to never change, though it is possible for the underlying
 * uid to be changed.  That is, if you are storing information about
 * uids in persistent storage, you should use the string returned
 * by this function instead of the raw uid.
 *
 * @param uid The uid for which you would like to retrieve a name.
 * @return Returns a unique name for the given uid, or null if the
 * uid is not currently assigned.
 * @apiSince 1
 */

@android.annotation.Nullable
public abstract java.lang.String getNameForUid(int uid);

/**
 * Retrieves the official names associated with each given uid.
 * @see #getNameForUid(int)
 *
 * @hide

 * @return This value may be {@code null}.
 */

@android.annotation.Nullable
public abstract java.lang.String[] getNamesForUids(int[] uids);

/**
 * Return a List of all application packages that are installed for the
 * current user. If flag GET_UNINSTALLED_PACKAGES has been set, a list of all
 * applications including those deleted with {@code DONT_DELETE_DATA}
 * (partially installed apps with data directory) will be returned.
 *
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, android.content.pm.PackageManager.MATCH_STATIC_SHARED_LIBRARIES, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @return A List of ApplicationInfo objects, one for each installed
 *         application. In the unlikely case there are no installed
 *         packages, an empty list is returned. If flag
 *         {@code MATCH_UNINSTALLED_PACKAGES} is set, the application
 *         information is retrieved from the list of uninstalled
 *         applications (which includes installed applications as well as
 *         applications with data directory i.e. applications which had been
 *         deleted with {@code DONT_DELETE_DATA} flag set).
 
 * This value will never be {@code null}.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.ApplicationInfo> getInstalledApplications(int flags);

/**
 * Return a List of all application packages that are installed on the
 * device, for a specific user. If flag GET_UNINSTALLED_PACKAGES has been
 * set, a list of all applications including those deleted with
 * {@code DONT_DELETE_DATA} (partially installed apps with data directory)
 * will be returned.
 *
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, android.content.pm.PackageManager.MATCH_STATIC_SHARED_LIBRARIES, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @param userId The user for whom the installed applications are to be
 *            listed
 * @return A List of ApplicationInfo objects, one for each installed
 *         application. In the unlikely case there are no installed
 *         packages, an empty list is returned. If flag
 *         {@code MATCH_UNINSTALLED_PACKAGES} is set, the application
 *         information is retrieved from the list of uninstalled
 *         applications (which includes installed applications as well as
 *         applications with data directory i.e. applications which had been
 *         deleted with {@code DONT_DELETE_DATA} flag set).
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public abstract java.util.List<android.content.pm.ApplicationInfo> getInstalledApplicationsAsUser(int flags, int userId);

/**
 * Gets the instant applications the user recently used.
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_INSTANT_APPS}
 * @return The instant app list.
 *
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public abstract java.util.List<android.content.pm.InstantAppInfo> getInstantApps();

/**
 * Gets the icon for an instant application.
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_INSTANT_APPS}
 * @param packageName The app package name.
 *
 * @hide

 * @return This value may be {@code null}.
 */

@android.annotation.Nullable
public abstract android.graphics.drawable.Drawable getInstantAppIcon(java.lang.String packageName);

/**
 * Gets whether this application is an instant app.
 *
 * @return Whether caller is an instant app.
 *
 * @see #isInstantApp(String)
 * @see #updateInstantAppCookie(byte[])
 * @see #getInstantAppCookie()
 * @see #getInstantAppCookieMaxBytes()
 * @apiSince 26
 */

public abstract boolean isInstantApp();

/**
 * Gets whether the given package is an instant app.
 *
 * @param packageName The package to check
 * This value must never be {@code null}.
 * @return Whether the given package is an instant app.
 *
 * @see #isInstantApp()
 * @see #updateInstantAppCookie(byte[])
 * @see #getInstantAppCookie()
 * @see #getInstantAppCookieMaxBytes()
 * @see #clearInstantAppCookie()
 * @apiSince 26
 */

public abstract boolean isInstantApp(@androidx.annotation.RecentlyNonNull java.lang.String packageName);

/**
 * Gets the maximum size in bytes of the cookie data an instant app
 * can store on the device.
 *
 * @return The max cookie size in bytes.
 *
 * @see #isInstantApp()
 * @see #isInstantApp(String)
 * @see #updateInstantAppCookie(byte[])
 * @see #getInstantAppCookie()
 * @see #clearInstantAppCookie()
 * @apiSince 26
 */

public abstract int getInstantAppCookieMaxBytes();

/**
 * Gets the instant application cookie for this app. Non
 * instant apps and apps that were instant but were upgraded
 * to normal apps can still access this API. For instant apps
 * this cookie is cached for some time after uninstall while for
 * normal apps the cookie is deleted after the app is uninstalled.
 * The cookie is always present while the app is installed.
 *
 * @return The cookie.
 *
 * This value will never be {@code null}.
 * @see #isInstantApp()
 * @see #isInstantApp(String)
 * @see #updateInstantAppCookie(byte[])
 * @see #getInstantAppCookieMaxBytes()
 * @see #clearInstantAppCookie()
 * @apiSince 26
 */

@android.annotation.NonNull
public abstract byte[] getInstantAppCookie();

/**
 * Clears the instant application cookie for the calling app.
 *
 * @see #isInstantApp()
 * @see #isInstantApp(String)
 * @see #getInstantAppCookieMaxBytes()
 * @see #getInstantAppCookie()
 * @see #clearInstantAppCookie()
 * @apiSince 26
 */

public abstract void clearInstantAppCookie();

/**
 * Updates the instant application cookie for the calling app. Non
 * instant apps and apps that were instant but were upgraded
 * to normal apps can still access this API. For instant apps
 * this cookie is cached for some time after uninstall while for
 * normal apps the cookie is deleted after the app is uninstalled.
 * The cookie is always present while the app is installed. The
 * cookie size is limited by {@link #getInstantAppCookieMaxBytes()}.
 * Passing <code>null</code> or an empty array clears the cookie.
 * </p>
 *
 * @param cookie The cookie data.
 *
 * This value may be {@code null}.
 * @see #isInstantApp()
 * @see #isInstantApp(String)
 * @see #getInstantAppCookieMaxBytes()
 * @see #getInstantAppCookie()
 * @see #clearInstantAppCookie()
 *
 * @throws IllegalArgumentException if the array exceeds max cookie size.
 * @apiSince 26
 */

public abstract void updateInstantAppCookie(@android.annotation.Nullable byte[] cookie);

/**
 * Get a list of shared libraries that are available on the
 * system.
 *
 * @return An array of shared library names that are
 * available on the system, or null if none are installed.
 *
 * @apiSince 3
 */

@androidx.annotation.RecentlyNullable
public abstract java.lang.String[] getSystemSharedLibraryNames();

/**
 * Get a list of shared libraries on the device.
 *
 * @param flags To filter the libraries to return.
 * Value is either <code>0</code> or a combination of android.content.pm.PackageManager.INSTALL_REPLACE_EXISTING, android.content.pm.PackageManager.INSTALL_ALLOW_TEST, android.content.pm.PackageManager.INSTALL_INTERNAL, android.content.pm.PackageManager.INSTALL_FROM_ADB, android.content.pm.PackageManager.INSTALL_ALL_USERS, android.content.pm.PackageManager.INSTALL_REQUEST_DOWNGRADE, android.content.pm.PackageManager.INSTALL_GRANT_RUNTIME_PERMISSIONS, android.content.pm.PackageManager.INSTALL_ALL_WHITELIST_RESTRICTED_PERMISSIONS, android.content.pm.PackageManager.INSTALL_FORCE_VOLUME_UUID, android.content.pm.PackageManager.INSTALL_FORCE_PERMISSION_PROMPT, android.content.pm.PackageManager.INSTALL_INSTANT_APP, android.content.pm.PackageManager.INSTALL_DONT_KILL_APP, android.content.pm.PackageManager.INSTALL_FULL_APP, android.content.pm.PackageManager.INSTALL_ALLOCATE_AGGRESSIVE, android.content.pm.PackageManager.INSTALL_VIRTUAL_PRELOAD, android.content.pm.PackageManager.INSTALL_APEX, android.content.pm.PackageManager.INSTALL_ENABLE_ROLLBACK, android.content.pm.PackageManager.INSTALL_ALLOW_DOWNGRADE, android.content.pm.PackageManager.INSTALL_STAGED, and android.content.pm.PackageManager.INSTALL_DRY_RUN
 * @return The shared library list.
 *
 * This value will never be {@code null}.
 * @see #MATCH_UNINSTALLED_PACKAGES
 * @apiSince 26
 */

@android.annotation.NonNull
public abstract java.util.List<android.content.pm.SharedLibraryInfo> getSharedLibraries(int flags);

/**
 * Get the list of shared libraries declared by a package.
 *
 * <br>
 * Requires {@link android.Manifest.permission#ACCESS_SHARED_LIBRARIES}
 * @param packageName the package name to query
 * This value must never be {@code null}.
 * @param flags the flags to filter packages
 * Value is either <code>0</code> or a combination of android.content.pm.PackageManager.INSTALL_REPLACE_EXISTING, android.content.pm.PackageManager.INSTALL_ALLOW_TEST, android.content.pm.PackageManager.INSTALL_INTERNAL, android.content.pm.PackageManager.INSTALL_FROM_ADB, android.content.pm.PackageManager.INSTALL_ALL_USERS, android.content.pm.PackageManager.INSTALL_REQUEST_DOWNGRADE, android.content.pm.PackageManager.INSTALL_GRANT_RUNTIME_PERMISSIONS, android.content.pm.PackageManager.INSTALL_ALL_WHITELIST_RESTRICTED_PERMISSIONS, android.content.pm.PackageManager.INSTALL_FORCE_VOLUME_UUID, android.content.pm.PackageManager.INSTALL_FORCE_PERMISSION_PROMPT, android.content.pm.PackageManager.INSTALL_INSTANT_APP, android.content.pm.PackageManager.INSTALL_DONT_KILL_APP, android.content.pm.PackageManager.INSTALL_FULL_APP, android.content.pm.PackageManager.INSTALL_ALLOCATE_AGGRESSIVE, android.content.pm.PackageManager.INSTALL_VIRTUAL_PRELOAD, android.content.pm.PackageManager.INSTALL_APEX, android.content.pm.PackageManager.INSTALL_ENABLE_ROLLBACK, android.content.pm.PackageManager.INSTALL_ALLOW_DOWNGRADE, android.content.pm.PackageManager.INSTALL_STAGED, and android.content.pm.PackageManager.INSTALL_DRY_RUN
 * @return the shared library list
 *
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public java.util.List<android.content.pm.SharedLibraryInfo> getDeclaredSharedLibraries(@android.annotation.NonNull java.lang.String packageName, int flags) { throw new RuntimeException("Stub!"); }

/**
 * Get the name of the package hosting the services shared library.
 *
 * @return The library host package.
 *
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public abstract java.lang.String getServicesSystemSharedLibraryPackageName();

/**
 * Get the name of the package hosting the shared components shared library.
 *
 * @return The library host package.
 *
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public abstract java.lang.String getSharedSystemSharedLibraryPackageName();

/**
 * Returns the names of the packages that have been changed
 * [eg. added, removed or updated] since the given sequence
 * number.
 * <p>If no packages have been changed, returns <code>null</code>.
 * <p>The sequence number starts at <code>0</code> and is
 * reset every boot.
 * @param sequenceNumber The first sequence number for which to retrieve package changes.
 * Value is 0 or greater
 * @see android.provider.Settings.Global#BOOT_COUNT
 * @apiSince 26
 */

@android.annotation.Nullable
public abstract android.content.pm.ChangedPackages getChangedPackages(int sequenceNumber);

/**
 * Get a list of features that are available on the
 * system.
 *
 * @return An array of FeatureInfo classes describing the features
 * that are available on the system, or null if there are none(!!).
 * @apiSince 5
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.pm.FeatureInfo[] getSystemAvailableFeatures();

/**
 * Check whether the given feature name is one of the available features as
 * returned by {@link #getSystemAvailableFeatures()}. This tests for the
 * presence of <em>any</em> version of the given feature name; use
 * {@link #hasSystemFeature(String, int)} to check for a minimum version.
 *
 * @param featureName This value must never be {@code null}.
 * @return Returns true if the devices supports the feature, else false.
 * @apiSince 5
 */

public abstract boolean hasSystemFeature(@androidx.annotation.RecentlyNonNull java.lang.String featureName);

/**
 * Check whether the given feature name and version is one of the available
 * features as returned by {@link #getSystemAvailableFeatures()}. Since
 * features are defined to always be backwards compatible, this returns true
 * if the available feature version is greater than or equal to the
 * requested version.
 *
 * @param featureName This value must never be {@code null}.
 * @return Returns true if the devices supports the feature, else false.
 * @apiSince 24
 */

public abstract boolean hasSystemFeature(@androidx.annotation.RecentlyNonNull java.lang.String featureName, int version);

/**
 * Determine the best action to perform for a given Intent. This is how
 * {@link Intent#resolveActivity} finds an activity if a class has not been
 * explicitly specified.
 * <p>
 * <em>Note:</em> if using an implicit Intent (without an explicit
 * ComponentName specified), be sure to consider whether to set the
 * {@link #MATCH_DEFAULT_ONLY} only flag. You need to do so to resolve the
 * activity in the same way that
 * {@link android.content.Context#startActivity(Intent)} and
 * {@link android.content.Intent#resolveActivity(PackageManager)
 * Intent.resolveActivity(PackageManager)} do.
 * </p>
 *
 * @param intent An intent containing all of the desired specification
 *            (action, data, type, category, and/or component).
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned. The
 *            most important is {@link #MATCH_DEFAULT_ONLY}, to limit the
 *            resolution to only those activities that support the
 *            {@link android.content.Intent#CATEGORY_DEFAULT}.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return Returns a ResolveInfo object containing the final activity intent
 *         that was determined to be the best action. Returns null if no
 *         matching activity was found. If multiple matching activities are
 *         found and there is no default set, returns a ResolveInfo object
 *         containing something else, such as the activity resolver.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNullable
public abstract android.content.pm.ResolveInfo resolveActivity(@androidx.annotation.RecentlyNonNull android.content.Intent intent, int flags);

/**
 * Retrieve all activities that can be performed for the given intent.
 *
 * @param intent The desired intent as per resolveActivity().
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned. The
 *            most important is {@link #MATCH_DEFAULT_ONLY}, to limit the
 *            resolution to only those activities that support the
 *            {@link android.content.Intent#CATEGORY_DEFAULT}. Or, set
 *            {@link #MATCH_ALL} to prevent any filtering of the results.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return Returns a List of ResolveInfo objects containing one entry for
 *         each matching activity, ordered from best to worst. In other
 *         words, the first item is what would be returned by
 *         {@link #resolveActivity}. If there are no matching activities, an
 *         empty list is returned.
 
 * This value will never be {@code null}.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.ResolveInfo> queryIntentActivities(@androidx.annotation.RecentlyNonNull android.content.Intent intent, int flags);

/**
 * Retrieve all activities that can be performed for the given intent, for a
 * specific user.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS}
 * @param intent The desired intent as per resolveActivity().
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned. The
 *            most important is {@link #MATCH_DEFAULT_ONLY}, to limit the
 *            resolution to only those activities that support the
 *            {@link android.content.Intent#CATEGORY_DEFAULT}. Or, set
 *            {@link #MATCH_ALL} to prevent any filtering of the results.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @param user The user being queried.
 * This value must never be {@code null}.
 * @return Returns a List of ResolveInfo objects containing one entry for
 *         each matching activity, ordered from best to worst. In other
 *         words, the first item is what would be returned by
 *         {@link #resolveActivity}. If there are no matching activities, an
 *         empty list is returned.
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public java.util.List<android.content.pm.ResolveInfo> queryIntentActivitiesAsUser(@android.annotation.NonNull android.content.Intent intent, int flags, @android.annotation.NonNull android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve a set of activities that should be presented to the user as
 * similar options. This is like {@link #queryIntentActivities}, except it
 * also allows you to supply a list of more explicit Intents that you would
 * like to resolve to particular options, and takes care of returning the
 * final ResolveInfo list in a reasonable order, with no duplicates, based
 * on those inputs.
 *
 * @param caller The class name of the activity that is making the request.
 *            This activity will never appear in the output list. Can be
 *            null.
 * This value may be {@code null}.
 * @param specifics An array of Intents that should be resolved to the first
 *            specific results. Can be null.
 * This value may be {@code null}.
 * @param intent The desired intent as per resolveActivity().
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned. The
 *            most important is {@link #MATCH_DEFAULT_ONLY}, to limit the
 *            resolution to only those activities that support the
 *            {@link android.content.Intent#CATEGORY_DEFAULT}.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return Returns a List of ResolveInfo objects containing one entry for
 *         each matching activity. The list is ordered first by all of the
 *         intents resolved in <var>specifics</var> and then any additional
 *         activities that can handle <var>intent</var> but did not get
 *         included by one of the <var>specifics</var> intents. If there are
 *         no matching activities, an empty list is returned.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.ResolveInfo> queryIntentActivityOptions(@android.annotation.Nullable android.content.ComponentName caller, @android.annotation.Nullable android.content.Intent[] specifics, @androidx.annotation.RecentlyNonNull android.content.Intent intent, int flags);

/**
 * Retrieve all receivers that can handle a broadcast of the given intent.
 *
 * @param intent The desired intent as per resolveActivity().
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return Returns a List of ResolveInfo objects containing one entry for
 *         each matching receiver, ordered from best to worst. If there are
 *         no matching receivers, an empty list or null is returned.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.ResolveInfo> queryBroadcastReceivers(@androidx.annotation.RecentlyNonNull android.content.Intent intent, int flags);

/**
 * Retrieve all receivers that can handle a broadcast of the given intent,
 * for a specific user.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS}
 * @param intent The desired intent as per resolveActivity().
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @param userHandle UserHandle of the user being queried.
 * @return Returns a List of ResolveInfo objects containing one entry for
 *         each matching receiver, ordered from best to worst. If there are
 *         no matching receivers, an empty list or null is returned.
 * @hide
 */

@android.annotation.NonNull
public java.util.List<android.content.pm.ResolveInfo> queryBroadcastReceiversAsUser(@android.annotation.NonNull android.content.Intent intent, int flags, android.os.UserHandle userHandle) { throw new RuntimeException("Stub!"); }

/**
 * Determine the best service to handle for a given Intent.
 *
 * @param intent An intent containing all of the desired specification
 *            (action, data, type, category, and/or component).
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return Returns a ResolveInfo object containing the final service intent
 *         that was determined to be the best action. Returns null if no
 *         matching service was found.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNullable
public abstract android.content.pm.ResolveInfo resolveService(@androidx.annotation.RecentlyNonNull android.content.Intent intent, int flags);

/**
 * Retrieve all services that can match the given intent.
 *
 * @param intent The desired intent as per resolveService().
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return Returns a List of ResolveInfo objects containing one entry for
 *         each matching service, ordered from best to worst. In other
 *         words, the first item is what would be returned by
 *         {@link #resolveService}. If there are no matching services, an
 *         empty list or null is returned.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.ResolveInfo> queryIntentServices(@androidx.annotation.RecentlyNonNull android.content.Intent intent, int flags);

/**
 * Retrieve all services that can match the given intent for a given user.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS}
 * @param intent The desired intent as per resolveService().
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @param user The user being queried.
 * This value must never be {@code null}.
 * @return Returns a List of ResolveInfo objects containing one entry for
 *         each matching service, ordered from best to worst. In other
 *         words, the first item is what would be returned by
 *         {@link #resolveService}. If there are no matching services, an
 *         empty list or null is returned.
 * @hide
 */

@android.annotation.NonNull
public java.util.List<android.content.pm.ResolveInfo> queryIntentServicesAsUser(@android.annotation.NonNull android.content.Intent intent, int flags, @android.annotation.NonNull android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve all providers that can match the given intent.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS}
 * @param intent An intent containing all of the desired specification
 *            (action, data, type, category, and/or component).
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @param user The user being queried.
 * This value must never be {@code null}.
 * @return Returns a List of ResolveInfo objects containing one entry for
 *         each matching provider, ordered from best to worst. If there are
 *         no matching services, an empty list or null is returned.
 * @hide
 */

@android.annotation.NonNull
public java.util.List<android.content.pm.ResolveInfo> queryIntentContentProvidersAsUser(@android.annotation.NonNull android.content.Intent intent, int flags, @android.annotation.NonNull android.os.UserHandle user) { throw new RuntimeException("Stub!"); }

/**
 * Retrieve all providers that can match the given intent.
 *
 * @param intent An intent containing all of the desired specification
 *            (action, data, type, category, and/or component).
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_RESOLVED_FILTER}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return Returns a List of ResolveInfo objects containing one entry for
 *         each matching provider, ordered from best to worst. If there are
 *         no matching services, an empty list or null is returned.
 * @apiSince 19
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.ResolveInfo> queryIntentContentProviders(@androidx.annotation.RecentlyNonNull android.content.Intent intent, int flags);

/**
 * Find a single content provider by its authority.
 * <p>
 * Example:<p>
 * <pre>
 * Uri uri = Uri.parse("content://com.example.app.provider/table1");
 * ProviderInfo info = packageManager.resolveContentProvider(uri.getAuthority(), flags);
 * </pre>
 *
 * @param authority The authority of the provider to find.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, android.content.pm.PackageManager.MATCH_STATIC_SHARED_LIBRARIES, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return A {@link ProviderInfo} object containing information about the
 *         provider. If a provider was not found, returns null.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNullable
public abstract android.content.pm.ProviderInfo resolveContentProvider(@androidx.annotation.RecentlyNonNull java.lang.String authority, int flags);

/**
 * Retrieve content provider information.
 * <p>
 * <em>Note: unlike most other methods, an empty result set is indicated
 * by a null return instead of an empty list.</em>
 *
 * @param processName If non-null, limits the returned providers to only
 *            those that are hosted by the given process. If null, all
 *            content providers are returned.
 * This value may be {@code null}.
 * @param uid If <var>processName</var> is non-null, this is the required
 *            uid owning the requested content providers.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#MATCH_ALL}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_DEFAULT_ONLY}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AUTO}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_AWARE}, {@link android.content.pm.PackageManager#MATCH_DIRECT_BOOT_UNAWARE}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_INSTANT}, android.content.pm.PackageManager.MATCH_STATIC_SHARED_LIBRARIES, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, and {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}
 * @return A list of {@link ProviderInfo} objects containing one entry for
 *         each provider either matching <var>processName</var> or, if
 *         <var>processName</var> is null, all known content providers.
 *         <em>If there are no matching providers, null is returned.</em>
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.ProviderInfo> queryContentProviders(@androidx.annotation.RecentlyNullable java.lang.String processName, int uid, int flags);

/**
 * Retrieve all of the information we know about a particular
 * instrumentation class.
 *
 * @param className The full name (i.e.
 *            com.google.apps.contacts.InstrumentList) of an Instrumentation
 *            class.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or {@link android.content.pm.PackageManager#GET_META_DATA}
 * @return An {@link InstrumentationInfo} object containing information
 *         about the instrumentation.
 * This value will never be {@code null}.
 * @throws NameNotFoundException if a package with the given name cannot be
 *             found on the system.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.pm.InstrumentationInfo getInstrumentationInfo(@androidx.annotation.RecentlyNonNull android.content.ComponentName className, int flags) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve information about available instrumentation code. May be used to
 * retrieve either all instrumentation code, or only the code targeting a
 * particular package.
 *
 * @param targetPackage If null, all instrumentation is returned; only the
 *            instrumentation targeting this package name is returned.
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or {@link android.content.pm.PackageManager#GET_META_DATA}
 * @return A list of {@link InstrumentationInfo} objects containing one
 *         entry for each matching instrumentation. If there are no
 *         instrumentation available, returns an empty list.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.InstrumentationInfo> queryInstrumentation(@androidx.annotation.RecentlyNonNull java.lang.String targetPackage, int flags);

/**
 * Retrieve an image from a package.  This is a low-level API used by
 * the various package manager info structures (such as
 * {@link ComponentInfo} to implement retrieval of their associated
 * icon.
 *
 * @param packageName The name of the package that this icon is coming from.
 * Cannot be null.
 * This value must never be {@code null}.
 * @param resid The resource identifier of the desired image.  Cannot be 0.
 * @param appInfo Overall information about <var>packageName</var>.  This
 * may be null, in which case the application information will be retrieved
 * for you if needed; if you already have this information around, it can
 * be much more efficient to supply it here.
 *
 * This value may be {@code null}.
 * @return Returns a Drawable holding the requested image.  Returns null if
 * an image could not be found for any reason.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNullable
public abstract android.graphics.drawable.Drawable getDrawable(@androidx.annotation.RecentlyNonNull java.lang.String packageName, int resid, @androidx.annotation.RecentlyNullable android.content.pm.ApplicationInfo appInfo);

/**
 * Retrieve the icon associated with an activity.  Given the full name of
 * an activity, retrieves the information about it and calls
 * {@link ComponentInfo#loadIcon ComponentInfo.loadIcon()} to return its icon.
 * If the activity cannot be found, NameNotFoundException is thrown.
 *
 * @param activityName Name of the activity whose icon is to be retrieved.
 *
 * This value must never be {@code null}.
 * @return Returns the image of the icon, or the default activity icon if
 * it could not be found.  Does not return null.
 * @throws NameNotFoundException Thrown if the resources for the given
 * activity could not be loaded.
 *
 * @see #getActivityIcon(Intent)
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.graphics.drawable.Drawable getActivityIcon(@androidx.annotation.RecentlyNonNull android.content.ComponentName activityName) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve the icon associated with an Intent.  If intent.getClassName() is
 * set, this simply returns the result of
 * getActivityIcon(intent.getClassName()).  Otherwise it resolves the intent's
 * component and returns the icon associated with the resolved component.
 * If intent.getClassName() cannot be found or the Intent cannot be resolved
 * to a component, NameNotFoundException is thrown.
 *
 * @param intent The intent for which you would like to retrieve an icon.
 *
 * This value must never be {@code null}.
 * @return Returns the image of the icon, or the default activity icon if
 * it could not be found.  Does not return null.
 * @throws NameNotFoundException Thrown if the resources for application
 * matching the given intent could not be loaded.
 *
 * @see #getActivityIcon(ComponentName)
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.graphics.drawable.Drawable getActivityIcon(@androidx.annotation.RecentlyNonNull android.content.Intent intent) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve the banner associated with an activity. Given the full name of
 * an activity, retrieves the information about it and calls
 * {@link ComponentInfo#loadIcon ComponentInfo.loadIcon()} to return its
 * banner. If the activity cannot be found, NameNotFoundException is thrown.
 *
 * @param activityName Name of the activity whose banner is to be retrieved.
 * This value must never be {@code null}.
 * @return Returns the image of the banner, or null if the activity has no
 *         banner specified.
 * @throws NameNotFoundException Thrown if the resources for the given
 *             activity could not be loaded.
 * @see #getActivityBanner(Intent)
 * @apiSince 20
 */

@androidx.annotation.RecentlyNullable
public abstract android.graphics.drawable.Drawable getActivityBanner(@androidx.annotation.RecentlyNonNull android.content.ComponentName activityName) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve the banner associated with an Intent. If intent.getClassName()
 * is set, this simply returns the result of
 * getActivityBanner(intent.getClassName()). Otherwise it resolves the
 * intent's component and returns the banner associated with the resolved
 * component. If intent.getClassName() cannot be found or the Intent cannot
 * be resolved to a component, NameNotFoundException is thrown.
 *
 * @param intent The intent for which you would like to retrieve a banner.
 * This value must never be {@code null}.
 * @return Returns the image of the banner, or null if the activity has no
 *         banner specified.
 * @throws NameNotFoundException Thrown if the resources for application
 *             matching the given intent could not be loaded.
 * @see #getActivityBanner(ComponentName)
 * @apiSince 20
 */

@androidx.annotation.RecentlyNullable
public abstract android.graphics.drawable.Drawable getActivityBanner(@androidx.annotation.RecentlyNonNull android.content.Intent intent) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Return the generic icon for an activity that is used when no specific
 * icon is defined.
 *
 * @return Drawable Image of the icon.
 
 * This value will never be {@code null}.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.graphics.drawable.Drawable getDefaultActivityIcon();

/**
 * Retrieve the icon associated with an application.  If it has not defined
 * an icon, the default app icon is returned.  Does not return null.
 *
 * @param info Information about application being queried.
 *
 * This value must never be {@code null}.
 * @return Returns the image of the icon, or the default application icon
 * if it could not be found.
 *
 * @see #getApplicationIcon(String)
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.graphics.drawable.Drawable getApplicationIcon(@androidx.annotation.RecentlyNonNull android.content.pm.ApplicationInfo info);

/**
 * Retrieve the icon associated with an application.  Given the name of the
 * application's package, retrieves the information about it and calls
 * getApplicationIcon() to return its icon. If the application cannot be
 * found, NameNotFoundException is thrown.
 *
 * @param packageName Name of the package whose application icon is to be
 *                    retrieved.
 *
 * This value must never be {@code null}.
 * @return Returns the image of the icon, or the default application icon
 * if it could not be found.  Does not return null.
 * @throws NameNotFoundException Thrown if the resources for the given
 * application could not be loaded.
 *
 * @see #getApplicationIcon(ApplicationInfo)
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.graphics.drawable.Drawable getApplicationIcon(@androidx.annotation.RecentlyNonNull java.lang.String packageName) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve the banner associated with an application.
 *
 * @param info Information about application being queried.
 * This value must never be {@code null}.
 * @return Returns the image of the banner or null if the application has no
 *         banner specified.
 * @see #getApplicationBanner(String)
 * @apiSince 20
 */

@androidx.annotation.RecentlyNullable
public abstract android.graphics.drawable.Drawable getApplicationBanner(@androidx.annotation.RecentlyNonNull android.content.pm.ApplicationInfo info);

/**
 * Retrieve the banner associated with an application. Given the name of the
 * application's package, retrieves the information about it and calls
 * getApplicationIcon() to return its banner. If the application cannot be
 * found, NameNotFoundException is thrown.
 *
 * @param packageName Name of the package whose application banner is to be
 *            retrieved.
 * This value must never be {@code null}.
 * @return Returns the image of the banner or null if the application has no
 *         banner specified.
 * @throws NameNotFoundException Thrown if the resources for the given
 *             application could not be loaded.
 * @see #getApplicationBanner(ApplicationInfo)
 * @apiSince 20
 */

@androidx.annotation.RecentlyNullable
public abstract android.graphics.drawable.Drawable getApplicationBanner(@androidx.annotation.RecentlyNonNull java.lang.String packageName) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve the logo associated with an activity. Given the full name of an
 * activity, retrieves the information about it and calls
 * {@link ComponentInfo#loadLogo ComponentInfo.loadLogo()} to return its
 * logo. If the activity cannot be found, NameNotFoundException is thrown.
 *
 * @param activityName Name of the activity whose logo is to be retrieved.
 * This value must never be {@code null}.
 * @return Returns the image of the logo or null if the activity has no logo
 *         specified.
 * @throws NameNotFoundException Thrown if the resources for the given
 *             activity could not be loaded.
 * @see #getActivityLogo(Intent)
 * @apiSince 9
 */

@androidx.annotation.RecentlyNullable
public abstract android.graphics.drawable.Drawable getActivityLogo(@androidx.annotation.RecentlyNonNull android.content.ComponentName activityName) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve the logo associated with an Intent.  If intent.getClassName() is
 * set, this simply returns the result of
 * getActivityLogo(intent.getClassName()).  Otherwise it resolves the intent's
 * component and returns the logo associated with the resolved component.
 * If intent.getClassName() cannot be found or the Intent cannot be resolved
 * to a component, NameNotFoundException is thrown.
 *
 * @param intent The intent for which you would like to retrieve a logo.
 *
 * This value must never be {@code null}.
 * @return Returns the image of the logo, or null if the activity has no
 * logo specified.
 *
 * @throws NameNotFoundException Thrown if the resources for application
 * matching the given intent could not be loaded.
 *
 * @see #getActivityLogo(ComponentName)
 * @apiSince 9
 */

@androidx.annotation.RecentlyNullable
public abstract android.graphics.drawable.Drawable getActivityLogo(@androidx.annotation.RecentlyNonNull android.content.Intent intent) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve the logo associated with an application.  If it has not specified
 * a logo, this method returns null.
 *
 * @param info Information about application being queried.
 *
 * This value must never be {@code null}.
 * @return Returns the image of the logo, or null if no logo is specified
 * by the application.
 *
 * @see #getApplicationLogo(String)
 * @apiSince 9
 */

@androidx.annotation.RecentlyNullable
public abstract android.graphics.drawable.Drawable getApplicationLogo(@androidx.annotation.RecentlyNonNull android.content.pm.ApplicationInfo info);

/**
 * Retrieve the logo associated with an application.  Given the name of the
 * application's package, retrieves the information about it and calls
 * getApplicationLogo() to return its logo. If the application cannot be
 * found, NameNotFoundException is thrown.
 *
 * @param packageName Name of the package whose application logo is to be
 *                    retrieved.
 *
 * This value must never be {@code null}.
 * @return Returns the image of the logo, or null if no application logo
 * has been specified.
 *
 * @throws NameNotFoundException Thrown if the resources for the given
 * application could not be loaded.
 *
 * @see #getApplicationLogo(ApplicationInfo)
 * @apiSince 9
 */

@androidx.annotation.RecentlyNullable
public abstract android.graphics.drawable.Drawable getApplicationLogo(@androidx.annotation.RecentlyNonNull java.lang.String packageName) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * If the target user is a managed profile, then this returns a badged copy of the given icon
 * to be able to distinguish it from the original icon. For badging an arbitrary drawable use
 * {@link #getUserBadgedDrawableForDensity(
 * android.graphics.drawable.Drawable, UserHandle, android.graphics.Rect, int)}.
 * <p>
 * If the original drawable is a BitmapDrawable and the backing bitmap is
 * mutable as per {@link android.graphics.Bitmap#isMutable()}, the badging
 * is performed in place and the original drawable is returned.
 * </p>
 *
 * @param drawable The drawable to badge.
 * This value must never be {@code null}.
 * @param user The target user.
 * This value must never be {@code null}.
 * @return A drawable that combines the original icon and a badge as
 *         determined by the system.
 
 * This value will never be {@code null}.
 * @apiSince 21
 */

@androidx.annotation.RecentlyNonNull
public abstract android.graphics.drawable.Drawable getUserBadgedIcon(@androidx.annotation.RecentlyNonNull android.graphics.drawable.Drawable drawable, @androidx.annotation.RecentlyNonNull android.os.UserHandle user);

/**
 * If the target user is a managed profile of the calling user or the caller
 * is itself a managed profile, then this returns a badged copy of the given
 * drawable allowing the user to distinguish it from the original drawable.
 * The caller can specify the location in the bounds of the drawable to be
 * badged where the badge should be applied as well as the density of the
 * badge to be used.
 * <p>
 * If the original drawable is a BitmapDrawable and the backing bitmap is
 * mutable as per {@link android.graphics.Bitmap#isMutable()}, the badging
 * is performed in place and the original drawable is returned.
 * </p>
 *
 * @param drawable The drawable to badge.
 * This value must never be {@code null}.
 * @param user The target user.
 * This value must never be {@code null}.
 * @param badgeLocation Where in the bounds of the badged drawable to place
 *         the badge. If it's {@code null}, the badge is applied on top of the entire
 *         drawable being badged.
 * This value may be {@code null}.
 * @param badgeDensity The optional desired density for the badge as per
 *         {@link android.util.DisplayMetrics#densityDpi}. If it's not positive,
 *         the density of the display is used.
 * @return A drawable that combines the original drawable and a badge as
 *         determined by the system.
 * @apiSince 21
 */

@androidx.annotation.RecentlyNonNull
public abstract android.graphics.drawable.Drawable getUserBadgedDrawableForDensity(@androidx.annotation.RecentlyNonNull android.graphics.drawable.Drawable drawable, @androidx.annotation.RecentlyNonNull android.os.UserHandle user, @androidx.annotation.RecentlyNullable android.graphics.Rect badgeLocation, int badgeDensity);

/**
 * If the target user is a managed profile of the calling user or the caller
 * is itself a managed profile, then this returns a copy of the label with
 * badging for accessibility services like talkback. E.g. passing in "Email"
 * and it might return "Work Email" for Email in the work profile.
 *
 * @param label The label to change.
 * This value must never be {@code null}.
 * @param user The target user.
 * This value must never be {@code null}.
 * @return A label that combines the original label and a badge as
 *         determined by the system.
 
 * This value will never be {@code null}.
 * @apiSince 21
 */

@androidx.annotation.RecentlyNonNull
public abstract java.lang.CharSequence getUserBadgedLabel(@androidx.annotation.RecentlyNonNull java.lang.CharSequence label, @androidx.annotation.RecentlyNonNull android.os.UserHandle user);

/**
 * Retrieve text from a package.  This is a low-level API used by
 * the various package manager info structures (such as
 * {@link ComponentInfo} to implement retrieval of their associated
 * labels and other text.
 *
 * @param packageName The name of the package that this text is coming from.
 * Cannot be null.
 * This value must never be {@code null}.
 * @param resid The resource identifier of the desired text.  Cannot be 0.
 * @param appInfo Overall information about <var>packageName</var>.  This
 * may be null, in which case the application information will be retrieved
 * for you if needed; if you already have this information around, it can
 * be much more efficient to supply it here.
 *
 * This value may be {@code null}.
 * @return Returns a CharSequence holding the requested text.  Returns null
 * if the text could not be found for any reason.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNullable
public abstract java.lang.CharSequence getText(@androidx.annotation.RecentlyNonNull java.lang.String packageName, int resid, @androidx.annotation.RecentlyNullable android.content.pm.ApplicationInfo appInfo);

/**
 * Retrieve an XML file from a package.  This is a low-level API used to
 * retrieve XML meta data.
 *
 * @param packageName The name of the package that this xml is coming from.
 * Cannot be null.
 * This value must never be {@code null}.
 * @param resid The resource identifier of the desired xml.  Cannot be 0.
 * @param appInfo Overall information about <var>packageName</var>.  This
 * may be null, in which case the application information will be retrieved
 * for you if needed; if you already have this information around, it can
 * be much more efficient to supply it here.
 *
 * This value may be {@code null}.
 * @return Returns an XmlPullParser allowing you to parse out the XML
 * data.  Returns null if the xml resource could not be found for any
 * reason.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNullable
public abstract android.content.res.XmlResourceParser getXml(@androidx.annotation.RecentlyNonNull java.lang.String packageName, int resid, @androidx.annotation.RecentlyNullable android.content.pm.ApplicationInfo appInfo);

/**
 * Return the label to use for this application.
 *
 * @return Returns the label associated with this application, or null if
 * it could not be found for any reason.
 * @param info The application to get the label of.
 
 * This value must never be {@code null}.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract java.lang.CharSequence getApplicationLabel(@androidx.annotation.RecentlyNonNull android.content.pm.ApplicationInfo info);

/**
 * Retrieve the resources associated with an activity.  Given the full
 * name of an activity, retrieves the information about it and calls
 * getResources() to return its application's resources.  If the activity
 * cannot be found, NameNotFoundException is thrown.
 *
 * @param activityName Name of the activity whose resources are to be
 *                     retrieved.
 *
 * This value must never be {@code null}.
 * @return Returns the application's Resources.
 * This value will never be {@code null}.
 * @throws NameNotFoundException Thrown if the resources for the given
 * application could not be loaded.
 *
 * @see #getResourcesForApplication(ApplicationInfo)
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.res.Resources getResourcesForActivity(@androidx.annotation.RecentlyNonNull android.content.ComponentName activityName) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve the resources for an application.  Throws NameNotFoundException
 * if the package is no longer installed.
 *
 * @param app Information about the desired application.
 *
 * This value must never be {@code null}.
 * @return Returns the application's Resources.
 * This value will never be {@code null}.
 * @throws NameNotFoundException Thrown if the resources for the given
 * application could not be loaded (most likely because it was uninstalled).
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.res.Resources getResourcesForApplication(@androidx.annotation.RecentlyNonNull android.content.pm.ApplicationInfo app) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve the resources associated with an application.  Given the full
 * package name of an application, retrieves the information about it and
 * calls getResources() to return its application's resources.  If the
 * appPackageName cannot be found, NameNotFoundException is thrown.
 *
 * @param packageName Package name of the application whose resources
 *                       are to be retrieved.
 *
 * This value must never be {@code null}.
 * @return Returns the application's Resources.
 * This value will never be {@code null}.
 * @throws NameNotFoundException Thrown if the resources for the given
 * application could not be loaded.
 *
 * @see #getResourcesForApplication(ApplicationInfo)
 * @apiSince 1
 */

@androidx.annotation.RecentlyNonNull
public abstract android.content.res.Resources getResourcesForApplication(@androidx.annotation.RecentlyNonNull java.lang.String packageName) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Retrieve overall information about an application package defined in a
 * package archive file
 *
 * @param archiveFilePath The path to the archive file
 * This value must never be {@code null}.
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_ACTIVITIES}, {@link android.content.pm.PackageManager#GET_CONFIGURATIONS}, {@link android.content.pm.PackageManager#GET_GIDS}, {@link android.content.pm.PackageManager#GET_INSTRUMENTATION}, {@link android.content.pm.PackageManager#GET_INTENT_FILTERS}, {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_PERMISSIONS}, {@link android.content.pm.PackageManager#GET_PROVIDERS}, {@link android.content.pm.PackageManager#GET_RECEIVERS}, {@link android.content.pm.PackageManager#GET_SERVICES}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#GET_SIGNATURES}, {@link android.content.pm.PackageManager#GET_SIGNING_CERTIFICATES}, {@link android.content.pm.PackageManager#GET_URI_PERMISSION_PATTERNS}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_FACTORY_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#MATCH_APEX}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @return A PackageInfo object containing information about the package
 *         archive. If the package could not be parsed, returns null.
 * @apiSince 1
 */

@androidx.annotation.RecentlyNullable
public android.content.pm.PackageInfo getPackageArchiveInfo(@androidx.annotation.RecentlyNonNull java.lang.String archiveFilePath, int flags) { throw new RuntimeException("Stub!"); }

/**
 * If there is already an application with the given package name installed
 * on the system for other users, also install it for the calling user.
 * @hide
 *
 * @deprecated use {@link PackageInstaller#installExistingPackage()} instead.

 * @param packageName This value must never be {@code null}.
 */

@Deprecated
public abstract int installExistingPackage(@android.annotation.NonNull java.lang.String packageName) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * If there is already an application with the given package name installed
 * on the system for other users, also install it for the calling user.
 * @hide
 *
 * @deprecated use {@link PackageInstaller#installExistingPackage()} instead.
 
 * @param packageName This value must never be {@code null}.

 * @param installReason Value is {@link android.content.pm.PackageManager#INSTALL_REASON_UNKNOWN}, {@link android.content.pm.PackageManager#INSTALL_REASON_POLICY}, {@link android.content.pm.PackageManager#INSTALL_REASON_DEVICE_RESTORE}, {@link android.content.pm.PackageManager#INSTALL_REASON_DEVICE_SETUP}, or {@link android.content.pm.PackageManager#INSTALL_REASON_USER}
 */

@Deprecated
public abstract int installExistingPackage(@android.annotation.NonNull java.lang.String packageName, int installReason) throws android.content.pm.PackageManager.NameNotFoundException;

/**
 * Allows a package listening to the
 * {@link Intent#ACTION_PACKAGE_NEEDS_VERIFICATION package verification
 * broadcast} to respond to the package manager. The response must include
 * the {@code verificationCode} which is one of
 * {@link PackageManager#VERIFICATION_ALLOW} or
 * {@link PackageManager#VERIFICATION_REJECT}.
 *
 * @param id pending package identifier as passed via the
 *            {@link PackageManager#EXTRA_VERIFICATION_ID} Intent extra.
 * @param verificationCode either {@link PackageManager#VERIFICATION_ALLOW}
 *            or {@link PackageManager#VERIFICATION_REJECT}.
 * @throws SecurityException if the caller does not have the
 *            PACKAGE_VERIFICATION_AGENT permission.
 * @apiSince 14
 */

public abstract void verifyPendingInstall(int id, int verificationCode);

/**
 * Allows a package listening to the
 * {@link Intent#ACTION_PACKAGE_NEEDS_VERIFICATION package verification
 * broadcast} to extend the default timeout for a response and declare what
 * action to perform after the timeout occurs. The response must include
 * the {@code verificationCodeAtTimeout} which is one of
 * {@link PackageManager#VERIFICATION_ALLOW} or
 * {@link PackageManager#VERIFICATION_REJECT}.
 *
 * This method may only be called once per package id. Additional calls
 * will have no effect.
 *
 * @param id pending package identifier as passed via the
 *            {@link PackageManager#EXTRA_VERIFICATION_ID} Intent extra.
 * @param verificationCodeAtTimeout either
 *            {@link PackageManager#VERIFICATION_ALLOW} or
 *            {@link PackageManager#VERIFICATION_REJECT}. If
 *            {@code verificationCodeAtTimeout} is neither
 *            {@link PackageManager#VERIFICATION_ALLOW} or
 *            {@link PackageManager#VERIFICATION_REJECT}, then
 *            {@code verificationCodeAtTimeout} will default to
 *            {@link PackageManager#VERIFICATION_REJECT}.
 * @param millisecondsToDelay the amount of time requested for the timeout.
 *            Must be positive and less than
 *            {@link PackageManager#MAXIMUM_VERIFICATION_TIMEOUT}. If
 *            {@code millisecondsToDelay} is out of bounds,
 *            {@code millisecondsToDelay} will be set to the closest in
 *            bounds value; namely, 0 or
 *            {@link PackageManager#MAXIMUM_VERIFICATION_TIMEOUT}.
 * @throws SecurityException if the caller does not have the
 *            PACKAGE_VERIFICATION_AGENT permission.
 * @apiSince 17
 */

public abstract void extendVerificationTimeout(int id, int verificationCodeAtTimeout, long millisecondsToDelay);

/**
 * Allows a package listening to the
 * {@link Intent#ACTION_INTENT_FILTER_NEEDS_VERIFICATION} intent filter verification
 * broadcast to respond to the package manager. The response must include
 * the {@code verificationCode} which is one of
 * {@link PackageManager#INTENT_FILTER_VERIFICATION_SUCCESS} or
 * {@link PackageManager#INTENT_FILTER_VERIFICATION_FAILURE}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTENT_FILTER_VERIFICATION_AGENT}
 * @param verificationId pending package identifier as passed via the
 *            {@link PackageManager#EXTRA_VERIFICATION_ID} Intent extra.
 * @param verificationCode either {@link PackageManager#INTENT_FILTER_VERIFICATION_SUCCESS}
 *            or {@link PackageManager#INTENT_FILTER_VERIFICATION_FAILURE}.
 * @param failedDomains a list of failed domains if the verificationCode is
 *            {@link PackageManager#INTENT_FILTER_VERIFICATION_FAILURE}, otherwise null;
 * This value must never be {@code null}.
 * @throws SecurityException if the caller does not have the
 *            INTENT_FILTER_VERIFICATION_AGENT permission.
 *
 * @hide
 */

public abstract void verifyIntentFilter(int verificationId, int verificationCode, @android.annotation.NonNull java.util.List<java.lang.String> failedDomains);

/**
 * Get the status of a Domain Verification Result for an IntentFilter. This is
 * related to the {@link android.content.IntentFilter#setAutoVerify(boolean)} and
 * {@link android.content.IntentFilter#getAutoVerify()}
 *
 * This is used by the ResolverActivity to change the status depending on what the User select
 * in the Disambiguation Dialog and also used by the Settings App for changing the default App
 * for a domain.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS_FULL}
 * @param packageName The package name of the Activity associated with the IntentFilter.
 * This value must never be {@code null}.
 * @param userId The user id.
 *
 * @return The status to set to. This can be
 *              {@link #INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_ASK} or
 *              {@link #INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_ALWAYS} or
 *              {@link #INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_NEVER} or
 *              {@link #INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_UNDEFINED}
 *
 * @hide
 */

public abstract int getIntentVerificationStatusAsUser(@android.annotation.NonNull java.lang.String packageName, int userId);

/**
 * Allow to change the status of a Intent Verification status for all IntentFilter of an App.
 * This is related to the {@link android.content.IntentFilter#setAutoVerify(boolean)} and
 * {@link android.content.IntentFilter#getAutoVerify()}
 *
 * This is used by the ResolverActivity to change the status depending on what the User select
 * in the Disambiguation Dialog and also used by the Settings App for changing the default App
 * for a domain.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SET_PREFERRED_APPLICATIONS}
 * @param packageName The package name of the Activity associated with the IntentFilter.
 * This value must never be {@code null}.
 * @param status The status to set to. This can be
 *              {@link #INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_ASK} or
 *              {@link #INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_ALWAYS} or
 *              {@link #INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_NEVER}
 * @param userId The user id.
 *
 * @return true if the status has been set. False otherwise.
 *
 * @hide
 */

public abstract boolean updateIntentVerificationStatusAsUser(@android.annotation.NonNull java.lang.String packageName, int status, int userId);

/**
 * Get the list of IntentFilterVerificationInfo for a specific package and User.
 *
 * @param packageName the package name. When this parameter is set to a non null value,
 *                    the results will be filtered by the package name provided.
 *                    Otherwise, there will be no filtering and it will return a list
 *                    corresponding for all packages
 *
 * This value must never be {@code null}.
 * @return a list of IntentFilterVerificationInfo for a specific package.
 *
 * @hide
 */

@android.annotation.NonNull
public abstract java.util.List<android.content.pm.IntentFilterVerificationInfo> getIntentFilterVerifications(@android.annotation.NonNull java.lang.String packageName);

/**
 * Get the list of IntentFilter for a specific package.
 *
 * @param packageName the package name. This parameter is set to a non null value,
 *                    the list will contain all the IntentFilter for that package.
 *                    Otherwise, the list will be empty.
 *
 * This value must never be {@code null}.
 * @return a list of IntentFilter for a specific package.
 *
 * @hide
 */

@android.annotation.NonNull
public abstract java.util.List<android.content.IntentFilter> getAllIntentFilters(@android.annotation.NonNull java.lang.String packageName);

/**
 * Get the default Browser package name for a specific user.
 *
 * <br>
 * Requires {@link android.Manifest.permission#INTERACT_ACROSS_USERS_FULL}
 * @param userId The user id.
 *
 * @return the package name of the default Browser for the specified user. If the user id passed
 *         is -1 (all users) it will return a null value.
 *
 * @hide
 */

@android.annotation.Nullable
public abstract java.lang.String getDefaultBrowserPackageNameAsUser(int userId);

/**
 * Set the default Browser package name for a specific user.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SET_PREFERRED_APPLICATIONS} and {@link android.Manifest.permission#INTERACT_ACROSS_USERS_FULL}
 * @param packageName The package name of the default Browser.
 * This value may be {@code null}.
 * @param userId The user id.
 *
 * @return true if the default Browser for the specified user has been set,
 *         otherwise return false. If the user id passed is -1 (all users) this call will not
 *         do anything and just return false.
 *
 * @hide
 */

public abstract boolean setDefaultBrowserPackageNameAsUser(@android.annotation.Nullable java.lang.String packageName, int userId);

/**
 * Change the installer associated with a given package.  There are limitations
 * on how the installer package can be changed; in particular:
 * <ul>
 * <li> A SecurityException will be thrown if <var>installerPackageName</var>
 * is not signed with the same certificate as the calling application.
 * <li> A SecurityException will be thrown if <var>targetPackage</var> already
 * has an installer package, and that installer package is not signed with
 * the same certificate as the calling application.
 * </ul>
 *
 * @param targetPackage The installed package whose installer will be changed.
 * This value must never be {@code null}.
 * @param installerPackageName The package name of the new installer.  May be
 * null to clear the association.
 
 * This value may be {@code null}.
 * @apiSince 11
 */

public abstract void setInstallerPackageName(@androidx.annotation.RecentlyNonNull java.lang.String targetPackage, @androidx.annotation.RecentlyNullable java.lang.String installerPackageName);

/**
 *
 * Requires {@link android.Manifest.permission#INSTALL_PACKAGES}
 * @hide
 * @param packageName This value must never be {@code null}.
 */

public abstract void setUpdateAvailable(@android.annotation.NonNull java.lang.String packageName, boolean updateAvaialble);

/**
 * Retrieve the package name of the application that installed a package. This identifies
 * which market the package came from.
 *
 * @param packageName The name of the package to query
 * This value must never be {@code null}.
 * @throws IllegalArgumentException if the given package name is not installed
 
 * @return This value may be {@code null}.
 * @apiSince 5
 */

@androidx.annotation.RecentlyNullable
public abstract java.lang.String getInstallerPackageName(@androidx.annotation.RecentlyNonNull java.lang.String packageName);

/**
 * @deprecated This function no longer does anything. It is the platform's
 * responsibility to assign preferred activities and this cannot be modified
 * directly. To determine the activities resolved by the platform, use
 * {@link #resolveActivity} or {@link #queryIntentActivities}. To configure
 * an app to be responsible for a particular role and to check current role
 * holders, see {@link android.app.role.RoleManager}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 1
 * @deprecatedSince 15
 */

@Deprecated
public abstract void addPackageToPreferred(@androidx.annotation.RecentlyNonNull java.lang.String packageName);

/**
 * @deprecated This function no longer does anything. It is the platform's
 * responsibility to assign preferred activities and this cannot be modified
 * directly. To determine the activities resolved by the platform, use
 * {@link #resolveActivity} or {@link #queryIntentActivities}. To configure
 * an app to be responsible for a particular role and to check current role
 * holders, see {@link android.app.role.RoleManager}.
 
 * @param packageName This value must never be {@code null}.
 * @apiSince 1
 * @deprecatedSince 15
 */

@Deprecated
public abstract void removePackageFromPreferred(@androidx.annotation.RecentlyNonNull java.lang.String packageName);

/**
 * Retrieve the list of all currently configured preferred packages. The
 * first package on the list is the most preferred, the last is the least
 * preferred.
 *
 * @param flags Additional option flags to modify the data returned.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#GET_ACTIVITIES}, {@link android.content.pm.PackageManager#GET_CONFIGURATIONS}, {@link android.content.pm.PackageManager#GET_GIDS}, {@link android.content.pm.PackageManager#GET_INSTRUMENTATION}, {@link android.content.pm.PackageManager#GET_INTENT_FILTERS}, {@link android.content.pm.PackageManager#GET_META_DATA}, {@link android.content.pm.PackageManager#GET_PERMISSIONS}, {@link android.content.pm.PackageManager#GET_PROVIDERS}, {@link android.content.pm.PackageManager#GET_RECEIVERS}, {@link android.content.pm.PackageManager#GET_SERVICES}, {@link android.content.pm.PackageManager#GET_SHARED_LIBRARY_FILES}, {@link android.content.pm.PackageManager#GET_SIGNATURES}, {@link android.content.pm.PackageManager#GET_SIGNING_CERTIFICATES}, {@link android.content.pm.PackageManager#GET_URI_PERMISSION_PATTERNS}, {@link android.content.pm.PackageManager#MATCH_UNINSTALLED_PACKAGES}, {@link android.content.pm.PackageManager#MATCH_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#MATCH_SYSTEM_ONLY}, {@link android.content.pm.PackageManager#MATCH_FACTORY_ONLY}, android.content.pm.PackageManager.MATCH_DEBUG_TRIAGED_MISSING, {@link android.content.pm.PackageManager#MATCH_INSTANT}, {@link android.content.pm.PackageManager#MATCH_APEX}, {@link android.content.pm.PackageManager#GET_DISABLED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_DISABLED_UNTIL_USED_COMPONENTS}, {@link android.content.pm.PackageManager#GET_UNINSTALLED_PACKAGES}, and android.content.pm.PackageManager.MATCH_HIDDEN_UNTIL_INSTALLED_COMPONENTS
 * @return A List of PackageInfo objects, one for each preferred
 *         application, in order of preference.
 *
 * This value will never be {@code null}.
 * @deprecated This function no longer does anything. It is the platform's
 * responsibility to assign preferred activities and this cannot be modified
 * directly. To determine the activities resolved by the platform, use
 * {@link #resolveActivity} or {@link #queryIntentActivities}. To configure
 * an app to be responsible for a particular role and to check current role
 * holders, see {@link android.app.role.RoleManager}.
 * @apiSince 1
 * @deprecatedSince 29
 */

@Deprecated
@androidx.annotation.RecentlyNonNull
public abstract java.util.List<android.content.pm.PackageInfo> getPreferredPackages(int flags);

/**
 * Add a new preferred activity mapping to the system.  This will be used
 * to automatically select the given activity component when
 * {@link Context#startActivity(Intent) Context.startActivity()} finds
 * multiple matching activities and also matches the given filter.
 *
 * @param filter The set of intents under which this activity will be
 * made preferred.
 * This value must never be {@code null}.
 * @param match The IntentFilter match category that this preference
 * applies to.
 * @param set The set of activities that the user was picking from when
 * this preference was made.
 * This value may be {@code null}.
 * @param activity The component name of the activity that is to be
 * preferred.
 *
 * This value must never be {@code null}.
 * @deprecated This function no longer does anything. It is the platform's
 * responsibility to assign preferred activities and this cannot be modified
 * directly. To determine the activities resolved by the platform, use
 * {@link #resolveActivity} or {@link #queryIntentActivities}. To configure
 * an app to be responsible for a particular role and to check current role
 * holders, see {@link android.app.role.RoleManager}.
 * @apiSince 1
 * @deprecatedSince 15
 */

@Deprecated
public abstract void addPreferredActivity(@androidx.annotation.RecentlyNonNull android.content.IntentFilter filter, int match, @androidx.annotation.RecentlyNullable android.content.ComponentName[] set, @androidx.annotation.RecentlyNonNull android.content.ComponentName activity);

/**
 * Replaces an existing preferred activity mapping to the system, and if that were not present
 * adds a new preferred activity.  This will be used to automatically select the given activity
 * component when {@link Context#startActivity(Intent) Context.startActivity()} finds multiple
 * matching activities and also matches the given filter.
 *
 * @param filter The set of intents under which this activity will be made preferred.
 * This value must never be {@code null}.
 * @param match The IntentFilter match category that this preference applies to. Should be a
 *              combination of {@link IntentFilter#MATCH_CATEGORY_MASK} and
 *              {@link IntentFilter#MATCH_ADJUSTMENT_MASK}).
 * @param set The set of activities that the user was picking from when this preference was
 *            made.
 * This value must never be {@code null}.
 * @param activity The component name of the activity that is to be preferred.
 *
 * This value must never be {@code null}.
 * @hide
 */

public void replacePreferredActivity(@android.annotation.NonNull android.content.IntentFilter filter, int match, @android.annotation.NonNull java.util.List<android.content.ComponentName> set, @android.annotation.NonNull android.content.ComponentName activity) { throw new RuntimeException("Stub!"); }

/**
 * Remove all preferred activity mappings, previously added with
 * {@link #addPreferredActivity}, from the
 * system whose activities are implemented in the given package name.
 * An application can only clear its own package(s).
 *
 * @param packageName The name of the package whose preferred activity
 * mappings are to be removed.
 *
 * This value must never be {@code null}.
 * @deprecated This function no longer does anything. It is the platform's
 * responsibility to assign preferred activities and this cannot be modified
 * directly. To determine the activities resolved by the platform, use
 * {@link #resolveActivity} or {@link #queryIntentActivities}. To configure
 * an app to be responsible for a particular role and to check current role
 * holders, see {@link android.app.role.RoleManager}.
 * @apiSince 1
 * @deprecatedSince 29
 */

@Deprecated
public abstract void clearPackagePreferredActivities(@androidx.annotation.RecentlyNonNull java.lang.String packageName);

/**
 * Retrieve all preferred activities, previously added with
 * {@link #addPreferredActivity}, that are
 * currently registered with the system.
 *
 * @param outFilters A required list in which to place the filters of all of the
 * preferred activities.
 * This value must never be {@code null}.
 * @param outActivities A required list in which to place the component names of
 * all of the preferred activities.
 * This value must never be {@code null}.
 * @param packageName An optional package in which you would like to limit
 * the list.  If null, all activities will be returned; if non-null, only
 * those activities in the given package are returned.
 *
 * This value may be {@code null}.
 * @return Returns the total number of registered preferred activities
 * (the number of distinct IntentFilter records, not the number of unique
 * activity components) that were found.
 *
 * @deprecated This function no longer does anything. It is the platform's
 * responsibility to assign preferred activities and this cannot be modified
 * directly. To determine the activities resolved by the platform, use
 * {@link #resolveActivity} or {@link #queryIntentActivities}. To configure
 * an app to be responsible for a particular role and to check current role
 * holders, see {@link android.app.role.RoleManager}.
 * @apiSince 1
 * @deprecatedSince 29
 */

@Deprecated
public abstract int getPreferredActivities(@android.annotation.NonNull java.util.List<android.content.IntentFilter> outFilters, @android.annotation.NonNull java.util.List<android.content.ComponentName> outActivities, @androidx.annotation.RecentlyNullable java.lang.String packageName);

/**
 * Set the enabled setting for a package component (activity, receiver, service, provider).
 * This setting will override any enabled state which may have been set by the component in its
 * manifest.
 *
 * @param componentName The component to enable
 * This value must never be {@code null}.
 * @param newState The new enabled state for the component.
 * Value is {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DEFAULT}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_ENABLED}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED_USER}, or {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED_UNTIL_USED}
 * @param flags Optional behavior flags.
 
 * Value is either <code>0</code> or {@link android.content.pm.PackageManager#DONT_KILL_APP}
 * @apiSince 1
 */

public abstract void setComponentEnabledSetting(@android.annotation.NonNull android.content.ComponentName componentName, int newState, int flags);

/**
 * Return the enabled setting for a package component (activity,
 * receiver, service, provider).  This returns the last value set by
 * {@link #setComponentEnabledSetting(ComponentName, int, int)}; in most
 * cases this value will be {@link #COMPONENT_ENABLED_STATE_DEFAULT} since
 * the value originally specified in the manifest has not been modified.
 *
 * @param componentName The component to retrieve.
 * This value must never be {@code null}.
 * @return Returns the current enabled state for the component.
 
 * Value is {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DEFAULT}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_ENABLED}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED_USER}, or {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED_UNTIL_USED}
 * @apiSince 1
 */

public abstract int getComponentEnabledSetting(@androidx.annotation.RecentlyNonNull android.content.ComponentName componentName);

/**
 * Set whether a synthetic app details activity will be generated if the app has no enabled
 * launcher activity. Disabling this allows the app to have no launcher icon.
 *
 * @param packageName The package name of the app
 * This value must never be {@code null}.
 * @param enabled The new enabled state for the synthetic app details activity.
 *
 * @hide
 */

public void setSyntheticAppDetailsActivityEnabled(@android.annotation.NonNull java.lang.String packageName, boolean enabled) { throw new RuntimeException("Stub!"); }

/**
 * Return whether a synthetic app details activity will be generated if the app has no enabled
 * launcher activity.
 *
 * @param packageName The package name of the app
 * This value must never be {@code null}.
 * @return Returns the enabled state for the synthetic app details activity.
 *
 *
 * @apiSince 29
 */

public boolean getSyntheticAppDetailsActivityEnabled(@android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Set the enabled setting for an application
 * This setting will override any enabled state which may have been set by the application in
 * its manifest.  It also overrides the enabled state set in the manifest for any of the
 * application's components.  It does not override any enabled state set by
 * {@link #setComponentEnabledSetting} for any of the application's components.
 *
 * @param packageName The package name of the application to enable
 * This value must never be {@code null}.
 * @param newState The new enabled state for the application.
 * Value is {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DEFAULT}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_ENABLED}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED_USER}, or {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED_UNTIL_USED}
 * @param flags Optional behavior flags.
 
 * Value is either <code>0</code> or {@link android.content.pm.PackageManager#DONT_KILL_APP}
 * @apiSince 1
 */

public abstract void setApplicationEnabledSetting(@android.annotation.NonNull java.lang.String packageName, int newState, int flags);

/**
 * Return the enabled setting for an application. This returns
 * the last value set by
 * {@link #setApplicationEnabledSetting(String, int, int)}; in most
 * cases this value will be {@link #COMPONENT_ENABLED_STATE_DEFAULT} since
 * the value originally specified in the manifest has not been modified.
 *
 * @param packageName The package name of the application to retrieve.
 * This value must never be {@code null}.
 * @return Returns the current enabled state for the application.
 * Value is {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DEFAULT}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_ENABLED}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED}, {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED_USER}, or {@link android.content.pm.PackageManager#COMPONENT_ENABLED_STATE_DISABLED_UNTIL_USED}
 * @throws IllegalArgumentException if the named package does not exist.
 * @apiSince 1
 */

public abstract int getApplicationEnabledSetting(@androidx.annotation.RecentlyNonNull java.lang.String packageName);

/**
 * Return whether the device has been booted into safe mode.
 * @apiSince 3
 */

public abstract boolean isSafeMode();

/**
 * Adds a listener for permission changes for installed packages.
 *
 * <br>
 * Requires android.Manifest.permission.OBSERVE_GRANT_REVOKE_PERMISSIONS
 * @param listener The listener to add.
 *
 * This value must never be {@code null}.
 * @hide
 */

public abstract void addOnPermissionsChangeListener(@android.annotation.NonNull android.content.pm.PackageManager.OnPermissionsChangedListener listener);

/**
 * Remvoes a listener for permission changes for installed packages.
 *
 * <br>
 * Requires android.Manifest.permission.OBSERVE_GRANT_REVOKE_PERMISSIONS
 * @param listener The listener to remove.
 *
 * This value must never be {@code null}.
 * @hide
 */

public abstract void removeOnPermissionsChangeListener(@android.annotation.NonNull android.content.pm.PackageManager.OnPermissionsChangedListener listener);

/**
 * Mark or unmark the given packages as distracting to the user.
 * These packages can have certain restrictions set that should discourage the user to launch
 * them often. For example, notifications from such an app can be hidden, or the app can be
 * removed from launcher suggestions, so the user is able to restrict their use of these apps.
 *
 * <p>The caller must hold {@link android.Manifest.permission#SUSPEND_APPS} to use this API.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SUSPEND_APPS}
 * @param packages Packages to mark as distracting.
 * This value must never be {@code null}.
 * @param restrictionFlags Any combination of restrictions to impose on the given packages.
 *                         {@link #RESTRICTION_NONE} can be used to clear any existing
 *                         restrictions.
 * Value is either <code>0</code> or a combination of {@link android.content.pm.PackageManager#RESTRICTION_NONE}, {@link android.content.pm.PackageManager#RESTRICTION_HIDE_FROM_SUGGESTIONS}, and {@link android.content.pm.PackageManager#RESTRICTION_HIDE_NOTIFICATIONS}
 * @return A list of packages that could not have the {@code restrictionFlags} set. The system
 * may prevent restricting critical packages to preserve normal device function.
 *
 * This value will never be {@code null}.
 * @hide
 * @see #RESTRICTION_NONE
 * @see #RESTRICTION_HIDE_FROM_SUGGESTIONS
 * @see #RESTRICTION_HIDE_NOTIFICATIONS
 */

@android.annotation.NonNull
public java.lang.String[] setDistractingPackageRestrictions(@android.annotation.NonNull java.lang.String[] packages, int restrictionFlags) { throw new RuntimeException("Stub!"); }

/**
 * Puts the package in a suspended state, where attempts at starting activities are denied.
 *
 * <p>It doesn't remove the data or the actual package file. The application's notifications
 * will be hidden, any of its started activities will be stopped and it will not be able to
 * show toasts or system alert windows or ring the device.
 *
 * <p>When the user tries to launch a suspended app, a system dialog with the given
 * {@code dialogMessage} will be shown instead. Since the message is supplied to the system as
 * a {@link String}, the caller needs to take care of localization as needed.
 * The dialog message can optionally contain a placeholder for the name of the suspended app.
 * The system uses {@link String#format(Locale, String, Object...) String.format} to insert the
 * app name into the message, so an example format string could be {@code "The app %1$s is
 * currently suspended"}. This makes it easier for callers to provide a single message which
 * works for all the packages being suspended in a single call.
 *
 * <p>The package must already be installed. If the package is uninstalled while suspended
 * the package will no longer be suspended. </p>
 *
 * <p>Optionally, the suspending app can provide extra information in the form of
 * {@link PersistableBundle} objects to be shared with the apps being suspended and the
 * launcher to support customization that they might need to handle the suspended state.
 *
 * <p>The caller must hold {@link Manifest.permission#SUSPEND_APPS} to use this API.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SUSPEND_APPS}
 * @param packageNames The names of the packages to set the suspended status.
 * This value may be {@code null}.
 * @param suspended If set to {@code true}, the packages will be suspended, if set to
 * {@code false}, the packages will be unsuspended.
 * @param appExtras An optional {@link PersistableBundle} that the suspending app can provide
 *                  which will be shared with the apps being suspended. Ignored if
 *                  {@code suspended} is false.
 * This value may be {@code null}.
 * @param launcherExtras An optional {@link PersistableBundle} that the suspending app can
 *                       provide which will be shared with the launcher. Ignored if
 *                       {@code suspended} is false.
 * This value may be {@code null}.
 * @param dialogMessage The message to be displayed to the user, when they try to launch a
 *                      suspended app.
 *
 * This value may be {@code null}.
 * @return an array of package names for which the suspended status could not be set as
 * requested in this method. Returns {@code null} if {@code packageNames} was {@code null}.
 *
 * @deprecated use {@link #setPackagesSuspended(String[], boolean, PersistableBundle,
 * PersistableBundle, android.content.pm.SuspendDialogInfo)} instead.
 *
 * @hide
 */

@Deprecated
@android.annotation.Nullable
public java.lang.String[] setPackagesSuspended(@android.annotation.Nullable java.lang.String[] packageNames, boolean suspended, @android.annotation.Nullable android.os.PersistableBundle appExtras, @android.annotation.Nullable android.os.PersistableBundle launcherExtras, @android.annotation.Nullable java.lang.String dialogMessage) { throw new RuntimeException("Stub!"); }

/**
 * Puts the given packages in a suspended state, where attempts at starting activities are
 * denied.
 *
 * <p>The suspended application's notifications and all of its windows will be hidden, any
 * of its started activities will be stopped and it won't be able to ring the device.
 * It doesn't remove the data or the actual package file.
 *
 * <p>When the user tries to launch a suspended app, a system dialog alerting them that the app
 * is suspended will be shown instead.
 * The caller can optionally customize the dialog by passing a {@link SuspendDialogInfo} object
 * to this API. This dialog will have a button that starts the
 * {@link Intent#ACTION_SHOW_SUSPENDED_APP_DETAILS} intent if the suspending app declares an
 * activity which handles this action.
 *
 * <p>The packages being suspended must already be installed. If a package is uninstalled, it
 * will no longer be suspended.
 *
 * <p>Optionally, the suspending app can provide extra information in the form of
 * {@link PersistableBundle} objects to be shared with the apps being suspended and the
 * launcher to support customization that they might need to handle the suspended state.
 *
 * <p>The caller must hold {@link Manifest.permission#SUSPEND_APPS} to use this API.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SUSPEND_APPS}
 * @param packageNames The names of the packages to set the suspended status.
 * This value may be {@code null}.
 * @param suspended If set to {@code true}, the packages will be suspended, if set to
 * {@code false}, the packages will be unsuspended.
 * @param appExtras An optional {@link PersistableBundle} that the suspending app can provide
 *                  which will be shared with the apps being suspended. Ignored if
 *                  {@code suspended} is false.
 * This value may be {@code null}.
 * @param launcherExtras An optional {@link PersistableBundle} that the suspending app can
 *                       provide which will be shared with the launcher. Ignored if
 *                       {@code suspended} is false.
 * This value may be {@code null}.
 * @param dialogInfo An optional {@link SuspendDialogInfo} object describing the dialog that
 *                   should be shown to the user when they try to launch a suspended app.
 *                   Ignored if {@code suspended} is false.
 *
 * This value may be {@code null}.
 * @return an array of package names for which the suspended status could not be set as
 * requested in this method. Returns {@code null} if {@code packageNames} was {@code null}.
 *
 * @see #isPackageSuspended
 * @see SuspendDialogInfo
 * @see SuspendDialogInfo.Builder
 * @see Intent#ACTION_SHOW_SUSPENDED_APP_DETAILS
 *
 * @hide
 */

@android.annotation.Nullable
public java.lang.String[] setPackagesSuspended(@android.annotation.Nullable java.lang.String[] packageNames, boolean suspended, @android.annotation.Nullable android.os.PersistableBundle appExtras, @android.annotation.Nullable android.os.PersistableBundle launcherExtras, @android.annotation.Nullable android.content.pm.SuspendDialogInfo dialogInfo) { throw new RuntimeException("Stub!"); }

/**
 * Returns any packages in a given set of packages that cannot be suspended via a call to {@link
 * #setPackagesSuspended(String[], boolean, PersistableBundle, PersistableBundle,
 * SuspendDialogInfo) setPackagesSuspended}. The platform prevents suspending certain critical
 * packages to keep the device in a functioning state, e.g. the default dialer.
 * Apps need to hold {@link Manifest.permission#SUSPEND_APPS SUSPEND_APPS} to call this API.
 *
 * <p>
 * Note that this set of critical packages can change with time, so even though a package name
 * was not returned by this call, it does not guarantee that a subsequent call to
 * {@link #setPackagesSuspended(String[], boolean, PersistableBundle, PersistableBundle,
 * SuspendDialogInfo) setPackagesSuspended} for that package will succeed, especially if
 * significant time elapsed between the two calls.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SUSPEND_APPS}
 * @param packageNames The packages to check.
 * This value must never be {@code null}.
 * @return A list of packages that can not be currently suspended by the system.
 * This value will never be {@code null}.
 * @hide
 */

@android.annotation.NonNull
public java.lang.String[] getUnsuspendablePackages(@android.annotation.NonNull java.lang.String[] packageNames) { throw new RuntimeException("Stub!"); }

/**
 * Query if an app is currently suspended.
 *
 * @param packageName This value must never be {@code null}.
 * @return {@code true} if the given package is suspended, {@code false} otherwise
 * @throws NameNotFoundException if the package could not be found.
 *
 * @see #isPackageSuspended()
 * @apiSince 29
 */

public boolean isPackageSuspended(@android.annotation.NonNull java.lang.String packageName) throws android.content.pm.PackageManager.NameNotFoundException { throw new RuntimeException("Stub!"); }

/**
 * Apps can query this to know if they have been suspended. A system app with the permission
 * {@code android.permission.SUSPEND_APPS} can put any app on the device into a suspended state.
 *
 * <p>While in this state, the application's notifications will be hidden, any of its started
 * activities will be stopped and it will not be able to show toasts or dialogs or play audio.
 * When the user tries to launch a suspended app, the system will, instead, show a
 * dialog to the user informing them that they cannot use this app while it is suspended.
 *
 * <p>When an app is put into this state, the broadcast action
 * {@link Intent#ACTION_MY_PACKAGE_SUSPENDED} will be delivered to any of its broadcast
 * receivers that included this action in their intent-filters, <em>including manifest
 * receivers.</em> Similarly, a broadcast action {@link Intent#ACTION_MY_PACKAGE_UNSUSPENDED}
 * is delivered when a previously suspended app is taken out of this state. Apps are expected to
 * use these to gracefully deal with transitions to and from this state.
 *
 * @return {@code true} if the calling package has been suspended, {@code false} otherwise.
 *
 * @see #getSuspendedPackageAppExtras()
 * @see Intent#ACTION_MY_PACKAGE_SUSPENDED
 * @see Intent#ACTION_MY_PACKAGE_UNSUSPENDED
 * @apiSince 28
 */

public boolean isPackageSuspended() { throw new RuntimeException("Stub!"); }

/**
 * Returns a {@link Bundle} of extras that was meant to be sent to the calling app when it was
 * suspended. An app with the permission {@code android.permission.SUSPEND_APPS} can supply this
 * to the system at the time of suspending an app.
 *
 * <p>This is the same {@link Bundle} that is sent along with the broadcast
 * {@link Intent#ACTION_MY_PACKAGE_SUSPENDED}, whenever the app is suspended. The contents of
 * this {@link Bundle} are a contract between the suspended app and the suspending app.
 *
 * <p>Note: These extras are optional, so if no extras were supplied to the system, this method
 * will return {@code null}, even when the calling app has been suspended.
 *
 * @return A {@link Bundle} containing the extras for the app, or {@code null} if the
 * package is not currently suspended.
 *
 * @see #isPackageSuspended()
 * @see Intent#ACTION_MY_PACKAGE_UNSUSPENDED
 * @see Intent#ACTION_MY_PACKAGE_SUSPENDED
 * @see Intent#EXTRA_SUSPENDED_PACKAGE_EXTRAS
 * @apiSince 28
 */

@android.annotation.Nullable
public android.os.Bundle getSuspendedPackageAppExtras() { throw new RuntimeException("Stub!"); }

/**
 * Provide a hint of what the {@link ApplicationInfo#category} value should
 * be for the given package.
 * <p>
 * This hint can only be set by the app which installed this package, as
 * determined by {@link #getInstallerPackageName(String)}.
 *
 * @param packageName the package to change the category hint for.
 * This value must never be {@code null}.
 * @param categoryHint the category hint to set.
 
 * Value is {@link android.content.pm.ApplicationInfo#CATEGORY_UNDEFINED}, {@link android.content.pm.ApplicationInfo#CATEGORY_GAME}, {@link android.content.pm.ApplicationInfo#CATEGORY_AUDIO}, {@link android.content.pm.ApplicationInfo#CATEGORY_VIDEO}, {@link android.content.pm.ApplicationInfo#CATEGORY_IMAGE}, {@link android.content.pm.ApplicationInfo#CATEGORY_SOCIAL}, {@link android.content.pm.ApplicationInfo#CATEGORY_NEWS}, {@link android.content.pm.ApplicationInfo#CATEGORY_MAPS}, or {@link android.content.pm.ApplicationInfo#CATEGORY_PRODUCTIVITY}
 * @apiSince 26
 */

public abstract void setApplicationCategoryHint(@android.annotation.NonNull java.lang.String packageName, int categoryHint);

/**
 * Returns true if the device is upgrading, such as first boot after OTA.
 * @apiSince 29
 */

public boolean isDeviceUpgrading() { throw new RuntimeException("Stub!"); }

/**
 * Return interface that offers the ability to install, upgrade, and remove
 * applications on the device.
 
 * @return This value will never be {@code null}.
 * @apiSince 21
 */

@android.annotation.NonNull
public abstract android.content.pm.PackageInstaller getPackageInstaller();

/**
 * Return the install reason that was recorded when a package was first
 * installed for a specific user. Requesting the install reason for another
 * user will require the permission INTERACT_ACROSS_USERS_FULL.
 *
 * @param packageName The package for which to retrieve the install reason
 * This value must never be {@code null}.
 * @param user The user for whom to retrieve the install reason
 * This value must never be {@code null}.
 * @return The install reason. If the package is not installed for the given
 *         user, {@code INSTALL_REASON_UNKNOWN} is returned.
 * Value is {@link android.content.pm.PackageManager#INSTALL_REASON_UNKNOWN}, {@link android.content.pm.PackageManager#INSTALL_REASON_POLICY}, {@link android.content.pm.PackageManager#INSTALL_REASON_DEVICE_RESTORE}, {@link android.content.pm.PackageManager#INSTALL_REASON_DEVICE_SETUP}, or {@link android.content.pm.PackageManager#INSTALL_REASON_USER}
 * @hide
 */

public abstract int getInstallReason(@android.annotation.NonNull java.lang.String packageName, @android.annotation.NonNull android.os.UserHandle user);

/**
 * Checks whether the calling package is allowed to request package installs through package
 * installer. Apps are encouraged to call this API before launching the package installer via
 * intent {@link android.content.Intent#ACTION_INSTALL_PACKAGE}. Starting from Android O, the
 * user can explicitly choose what external sources they trust to install apps on the device.
 * If this API returns false, the install request will be blocked by the package installer and
 * a dialog will be shown to the user with an option to launch settings to change their
 * preference. An application must target Android O or higher and declare permission
 * {@link android.Manifest.permission#REQUEST_INSTALL_PACKAGES} in order to use this API.
 *
 * @return true if the calling package is trusted by the user to request install packages on
 * the device, false otherwise.
 * @see android.content.Intent#ACTION_INSTALL_PACKAGE
 * @see android.provider.Settings#ACTION_MANAGE_UNKNOWN_APP_SOURCES
 * @apiSince 26
 */

public abstract boolean canRequestPackageInstalls();

/**
 * Return the {@link ComponentName} of the activity providing Settings for the Instant App
 * resolver.
 *
 * @see {@link android.content.Intent#ACTION_INSTANT_APP_RESOLVER_SETTINGS}
 * @hide

 * @return This value may be {@code null}.
 */

@android.annotation.Nullable
public abstract android.content.ComponentName getInstantAppResolverSettingsComponent();

/**
 * Return the {@link ComponentName} of the activity responsible for installing instant
 * applications.
 *
 * @see {@link android.content.Intent#ACTION_INSTALL_INSTANT_APP_PACKAGE}
 * @hide

 * @return This value may be {@code null}.
 */

@android.annotation.Nullable
public abstract android.content.ComponentName getInstantAppInstallerComponent();

/**
 * Register an application dex module with the package manager.
 * The package manager will keep track of the given module for future optimizations.
 *
 * Dex module optimizations will disable the classpath checking at runtime. The client bares
 * the responsibility to ensure that the static assumptions on classes in the optimized code
 * hold at runtime (e.g. there's no duplicate classes in the classpath).
 *
 * Note that the package manager already keeps track of dex modules loaded with
 * {@link dalvik.system.DexClassLoader} and {@link dalvik.system.PathClassLoader}.
 * This can be called for an eager registration.
 *
 * The call might take a while and the results will be posted on the main thread, using
 * the given callback.
 *
 * If the module is intended to be shared with other apps, make sure that the file
 * permissions allow for it.
 * If at registration time the permissions allow for others to read it, the module would
 * be marked as a shared module which might undergo a different optimization strategy.
 * (usually shared modules will generated larger optimizations artifacts,
 * taking more disk space).
 *
 * @param dexModulePath the absolute path of the dex module.
 * This value must never be {@code null}.
 * @param callback if not null, {@link DexModuleRegisterCallback#onDexModuleRegistered} will
 *                 be called once the registration finishes.
 *
 * This value may be {@code null}.
 * @hide
 */

public abstract void registerDexModule(@android.annotation.NonNull java.lang.String dexModulePath, @android.annotation.Nullable android.content.pm.PackageManager.DexModuleRegisterCallback callback);

/**
 * Returns the {@link ArtManager} associated with this package manager.
 *
 * @hide

 * @return This value will never be {@code null}.
 */

@android.annotation.NonNull
public android.content.pm.dex.ArtManager getArtManager() { throw new RuntimeException("Stub!"); }

/**
 * Sets or clears the harmful app warning details for the given app.
 *
 * When set, any attempt to launch an activity in this package will be intercepted and a
 * warning dialog will be shown to the user instead, with the given warning. The user
 * will have the option to proceed with the activity launch, or to uninstall the application.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SET_HARMFUL_APP_WARNINGS}
 * @param packageName The full name of the package to warn on.
 * This value must never be {@code null}.
 * @param warning A warning string to display to the user describing the threat posed by the
 *                application, or null to clear the warning.
 *
 * This value may be {@code null}.
 * @hide
 */

public void setHarmfulAppWarning(@android.annotation.NonNull java.lang.String packageName, @android.annotation.Nullable java.lang.CharSequence warning) { throw new RuntimeException("Stub!"); }

/**
 * Returns the harmful app warning string for the given app, or null if there is none set.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SET_HARMFUL_APP_WARNINGS}
 * @param packageName The full name of the desired package.
 *
 * This value must never be {@code null}.
 * @hide
 */

@android.annotation.Nullable
public java.lang.CharSequence getHarmfulAppWarning(@android.annotation.NonNull java.lang.String packageName) { throw new RuntimeException("Stub!"); }

/**
 * Searches the set of signing certificates by which the given package has proven to have been
 * signed.  This should be used instead of {@code getPackageInfo} with {@code GET_SIGNATURES}
 * since it takes into account the possibility of signing certificate rotation, except in the
 * case of packages that are signed by multiple certificates, for which signing certificate
 * rotation is not supported.  This method is analogous to using {@code getPackageInfo} with
 * {@code GET_SIGNING_CERTIFICATES} and then searching through the resulting {@code
 * signingInfo} field to see if the desired certificate is present.
 *
 * @param packageName package whose signing certificates to check
 * This value must never be {@code null}.
 * @param certificate signing certificate for which to search
 * This value must never be {@code null}.
 * @param type representation of the {@code certificate}
 * Value is {@link android.content.pm.PackageManager#CERT_INPUT_RAW_X509}, or {@link android.content.pm.PackageManager#CERT_INPUT_SHA256}
 * @return true if this package was or is signed by exactly the certificate {@code certificate}
 * @apiSince 28
 */

public boolean hasSigningCertificate(@androidx.annotation.RecentlyNonNull java.lang.String packageName, @androidx.annotation.RecentlyNonNull byte[] certificate, int type) { throw new RuntimeException("Stub!"); }

/**
 * Searches the set of signing certificates by which the package(s) for the given uid has proven
 * to have been signed.  For multiple packages sharing the same uid, this will return the
 * signing certificates found in the signing history of the "newest" package, where "newest"
 * indicates the package with the newest signing certificate in the shared uid group.  This
 * method should be used instead of {@code getPackageInfo} with {@code GET_SIGNATURES}
 * since it takes into account the possibility of signing certificate rotation, except in the
 * case of packages that are signed by multiple certificates, for which signing certificate
 * rotation is not supported. This method is analogous to using {@code getPackagesForUid}
 * followed by {@code getPackageInfo} with {@code GET_SIGNING_CERTIFICATES}, selecting the
 * {@code PackageInfo} of the newest-signed bpackage , and finally searching through the
 * resulting {@code signingInfo} field to see if the desired certificate is there.
 *
 * @param uid uid whose signing certificates to check
 * @param certificate signing certificate for which to search
 * This value must never be {@code null}.
 * @param type representation of the {@code certificate}
 * Value is {@link android.content.pm.PackageManager#CERT_INPUT_RAW_X509}, or {@link android.content.pm.PackageManager#CERT_INPUT_SHA256}
 * @return true if this package was or is signed by exactly the certificate {@code certificate}
 * @apiSince 28
 */

public boolean hasSigningCertificate(int uid, @androidx.annotation.RecentlyNonNull byte[] certificate, int type) { throw new RuntimeException("Stub!"); }

/**
 * @return the wellbeing app package name, or null if it's not defined by the OEM.
 *
 * @hide
 */

@android.annotation.Nullable
public java.lang.String getWellbeingPackageName() { throw new RuntimeException("Stub!"); }

/**
 * @return the incident report approver app package name, or null if it's not defined
 * by the OEM.
 *
 * @hide
 */

@android.annotation.Nullable
public java.lang.String getIncidentReportApproverPackageName() { throw new RuntimeException("Stub!"); }

/**
 * Notify to the rest of the system that a new device configuration has
 * been prepared and that it is time to refresh caches.
 *
 * @see android.content.Intent#ACTION_DEVICE_CUSTOMIZATION_READY
 *
 * @hide
 */

public void sendDeviceCustomizationReadyBroadcast() { throw new RuntimeException("Stub!"); }

/**
 * The action used to request that the user approve a permission request
 * from the application.
 *
 * @hide
 */

public static final java.lang.String ACTION_REQUEST_PERMISSIONS = "android.content.pm.action.REQUEST_PERMISSIONS";

/**
 * Certificate input bytes: the input bytes represent an encoded X.509 Certificate which could
 * be generated using an {@code CertificateFactory}
 * @apiSince 28
 */

public static final int CERT_INPUT_RAW_X509 = 0; // 0x0

/**
 * Certificate input bytes: the input bytes represent the SHA256 output of an encoded X.509
 * Certificate.
 * @apiSince 28
 */

public static final int CERT_INPUT_SHA256 = 1; // 0x1

/**
 * Flag for {@link #setApplicationEnabledSetting(String, int, int)} and
 * {@link #setComponentEnabledSetting(ComponentName, int, int)}: This
 * component or application is in its default enabled state (as specified in
 * its manifest).
 * <p>
 * Explicitly setting the component state to this value restores it's
 * enabled state to whatever is set in the manifest.
 * @apiSince 1
 */

public static final int COMPONENT_ENABLED_STATE_DEFAULT = 0; // 0x0

/**
 * Flag for {@link #setApplicationEnabledSetting(String, int, int)}
 * and {@link #setComponentEnabledSetting(ComponentName, int, int)}: This
 * component or application has been explicitly disabled, regardless of
 * what it has specified in its manifest.
 * @apiSince 1
 */

public static final int COMPONENT_ENABLED_STATE_DISABLED = 2; // 0x2

/**
 * Flag for {@link #setApplicationEnabledSetting(String, int, int)} only: This
 * application should be considered, until the point where the user actually
 * wants to use it.  This means that it will not normally show up to the user
 * (such as in the launcher), but various parts of the user interface can
 * use {@link #GET_DISABLED_UNTIL_USED_COMPONENTS} to still see it and allow
 * the user to select it (as for example an IME, device admin, etc).  Such code,
 * once the user has selected the app, should at that point also make it enabled.
 * This option currently <strong>can not</strong> be used with
 * {@link #setComponentEnabledSetting(ComponentName, int, int)}.
 * @apiSince 18
 */

public static final int COMPONENT_ENABLED_STATE_DISABLED_UNTIL_USED = 4; // 0x4

/**
 * Flag for {@link #setApplicationEnabledSetting(String, int, int)} only: The
 * user has explicitly disabled the application, regardless of what it has
 * specified in its manifest.  Because this is due to the user's request,
 * they may re-enable it if desired through the appropriate system UI.  This
 * option currently <strong>cannot</strong> be used with
 * {@link #setComponentEnabledSetting(ComponentName, int, int)}.
 * @apiSince 14
 */

public static final int COMPONENT_ENABLED_STATE_DISABLED_USER = 3; // 0x3

/**
 * Flag for {@link #setApplicationEnabledSetting(String, int, int)}
 * and {@link #setComponentEnabledSetting(ComponentName, int, int)}: This
 * component or application has been explictily enabled, regardless of
 * what it has specified in its manifest.
 * @apiSince 1
 */

public static final int COMPONENT_ENABLED_STATE_ENABLED = 1; // 0x1

/**
 * Flag parameter for
 * {@link #setComponentEnabledSetting(android.content.ComponentName, int, int)} to indicate
 * that you don't want to kill the app containing the component.  Be careful when you set this
 * since changing component states can make the containing application's behavior unpredictable.
 * @apiSince 1
 */

public static final int DONT_KILL_APP = 1; // 0x1

/**
 * The names of the requested permissions.
 * <p>
 * <strong>Type:</strong> String[]
 * </p>
 *
 * @hide
 */

public static final java.lang.String EXTRA_REQUEST_PERMISSIONS_NAMES = "android.content.pm.extra.REQUEST_PERMISSIONS_NAMES";

/**
 * The results from the permissions request.
 * <p>
 * <strong>Type:</strong> int[] of #PermissionResult
 * </p>
 *
 * @hide
 */

public static final java.lang.String EXTRA_REQUEST_PERMISSIONS_RESULTS = "android.content.pm.extra.REQUEST_PERMISSIONS_RESULTS";

/**
 * Extra field name for the ID of a package pending verification. Passed to
 * a package verifier and is used to call back to
 * {@link PackageManager#verifyPendingInstall(int, int)}
 * @apiSince 14
 */

public static final java.lang.String EXTRA_VERIFICATION_ID = "android.content.pm.extra.VERIFICATION_ID";

/**
 * Extra field name for the result of a verification, either
 * {@link #VERIFICATION_ALLOW}, or {@link #VERIFICATION_REJECT}.
 * Passed to package verifiers after a package is verified.
 * @apiSince 17
 */

public static final java.lang.String EXTRA_VERIFICATION_RESULT = "android.content.pm.extra.VERIFICATION_RESULT";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device supports running activities on secondary displays.
 * @apiSince 26
 */

public static final java.lang.String FEATURE_ACTIVITIES_ON_SECONDARY_DISPLAYS = "android.software.activities_on_secondary_displays";

/** {@hide} */

public static final java.lang.String FEATURE_ADOPTABLE_STORAGE = "android.software.adoptable_storage";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports app widgets.
 * @apiSince 18
 */

public static final java.lang.String FEATURE_APP_WIDGETS = "android.software.app_widgets";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}: The device's
 * audio pipeline is low-latency, more suitable for audio applications sensitive to delays or
 * lag in sound input or output.
 * @apiSince 9
 */

public static final java.lang.String FEATURE_AUDIO_LOW_LATENCY = "android.hardware.audio.low_latency";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes at least one form of audio
 * output, as defined in the Android Compatibility Definition Document (CDD)
 * <a href="https://source.android.com/compatibility/android-cdd#7_8_audio">section 7.8 Audio</a>.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_AUDIO_OUTPUT = "android.hardware.audio.output";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device has professional audio level of functionality and performance.
 * @apiSince 23
 */

public static final java.lang.String FEATURE_AUDIO_PRO = "android.hardware.audio.pro";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device supports autofill of user credentials, addresses, credit cards, etc
 * via integration with {@link android.service.autofill.AutofillService autofill
 * providers}.
 * @apiSince 26
 */

public static final java.lang.String FEATURE_AUTOFILL = "android.software.autofill";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: This is a device dedicated to showing UI
 * on a vehicle headunit. A headunit here is defined to be inside a
 * vehicle that may or may not be moving. A headunit uses either a
 * primary display in the center console and/or additional displays in
 * the instrument cluster or elsewhere in the vehicle. Headunit display(s)
 * have limited size and resolution. The user will likely be focused on
 * driving so limiting driver distraction is a primary concern. User input
 * can be a variety of hard buttons, touch, rotary controllers and even mouse-
 * like interfaces.
 * @apiSince 23
 */

public static final java.lang.String FEATURE_AUTOMOTIVE = "android.hardware.type.automotive";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device can perform backup and restore operations on installed applications.
 * @apiSince 20
 */

public static final java.lang.String FEATURE_BACKUP = "android.software.backup";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device is capable of communicating with
 * other devices via Bluetooth.
 * @apiSince 8
 */

public static final java.lang.String FEATURE_BLUETOOTH = "android.hardware.bluetooth";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device is capable of communicating with
 * other devices via Bluetooth Low Energy radio.
 * @apiSince 18
 */

public static final java.lang.String FEATURE_BLUETOOTH_LE = "android.hardware.bluetooth_le";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes broadcast radio tuner.
 * @hide
 */

public static final java.lang.String FEATURE_BROADCAST_RADIO = "android.hardware.broadcastradio";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has a camera facing away
 * from the screen.
 * @apiSince 7
 */

public static final java.lang.String FEATURE_CAMERA = "android.hardware.camera";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has at least one camera pointing in
 * some direction, or can support an external camera being connected to it.
 * @apiSince 17
 */

public static final java.lang.String FEATURE_CAMERA_ANY = "android.hardware.camera.any";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}: At least one
 * of the cameras on the device supports the
 * {@link android.hardware.camera2.CameraMetadata#REQUEST_AVAILABLE_CAPABILITIES_MOTION_TRACKING
 * MOTION_TRACKING} capability level.
 * @apiSince 28
 */

public static final java.lang.String FEATURE_CAMERA_AR = "android.hardware.camera.ar";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device's camera supports auto-focus.
 * @apiSince 7
 */

public static final java.lang.String FEATURE_CAMERA_AUTOFOCUS = "android.hardware.camera.autofocus";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}: At least one
 * of the cameras on the device supports the
 * {@link android.hardware.camera2.CameraMetadata#REQUEST_AVAILABLE_CAPABILITIES_MANUAL_POST_PROCESSING manual post-processing}
 * capability level.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_CAMERA_CAPABILITY_MANUAL_POST_PROCESSING = "android.hardware.camera.capability.manual_post_processing";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}: At least one
 * of the cameras on the device supports the
 * {@link android.hardware.camera2.CameraMetadata#REQUEST_AVAILABLE_CAPABILITIES_MANUAL_SENSOR manual sensor}
 * capability level.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_CAMERA_CAPABILITY_MANUAL_SENSOR = "android.hardware.camera.capability.manual_sensor";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}: At least one
 * of the cameras on the device supports the
 * {@link android.hardware.camera2.CameraMetadata#REQUEST_AVAILABLE_CAPABILITIES_RAW RAW}
 * capability level.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_CAMERA_CAPABILITY_RAW = "android.hardware.camera.capability.raw";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device can support having an external camera connected to it.
 * The external camera may not always be connected or available to applications to use.
 * @apiSince 20
 */

public static final java.lang.String FEATURE_CAMERA_EXTERNAL = "android.hardware.camera.external";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device's camera supports flash.
 * @apiSince 7
 */

public static final java.lang.String FEATURE_CAMERA_FLASH = "android.hardware.camera.flash";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has a front facing camera.
 * @apiSince 9
 */

public static final java.lang.String FEATURE_CAMERA_FRONT = "android.hardware.camera.front";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}: At least one
 * of the cameras on the device supports the
 * {@link android.hardware.camera2.CameraCharacteristics#INFO_SUPPORTED_HARDWARE_LEVEL full hardware}
 * capability level.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_CAMERA_LEVEL_FULL = "android.hardware.camera.level.full";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports the
 * {@link android.R.attr#cantSaveState} API.
 * @apiSince 28
 */

public static final java.lang.String FEATURE_CANT_SAVE_STATE = "android.software.cant_save_state";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device supports {@link android.companion.CompanionDeviceManager#associate associating}
 * with devices via {@link android.companion.CompanionDeviceManager}.
 * @apiSince 26
 */

public static final java.lang.String FEATURE_COMPANION_DEVICE_SETUP = "android.software.companion_device_setup";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The Connection Service API is enabled on the device.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_CONNECTION_SERVICE = "android.software.connectionservice";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device is capable of communicating with
 * consumer IR devices.
 * @apiSince 19
 */

public static final java.lang.String FEATURE_CONSUMER_IR = "android.hardware.consumerir";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports device policy enforcement via device admins.
 * @apiSince 19
 */

public static final java.lang.String FEATURE_DEVICE_ADMIN = "android.software.device_admin";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: This is a device for IoT and may not have an UI. An embedded
 * device is defined as a full stack Android device with or without a display and no
 * user-installable apps.
 * @apiSince 26
 */

public static final java.lang.String FEATURE_EMBEDDED = "android.hardware.type.embedded";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: This device supports ethernet.
 * @apiSince 24
 */

public static final java.lang.String FEATURE_ETHERNET = "android.hardware.ethernet";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has biometric hardware to perform face authentication.
 * @apiSince 29
 */

public static final java.lang.String FEATURE_FACE = "android.hardware.biometrics.face";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device does not have a touch screen, but
 * does support touch emulation for basic events. For instance, the
 * device might use a mouse or remote control to drive a cursor, and
 * emulate basic touch pointer events like down, up, drag, etc. All
 * devices that support android.hardware.touchscreen or a sub-feature are
 * presumed to also support faketouch.
 * @apiSince 11
 */

public static final java.lang.String FEATURE_FAKETOUCH = "android.hardware.faketouch";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device does not have a touch screen, but
 * does support touch emulation for basic events that supports distinct
 * tracking of two or more fingers.  This is an extension of
 * {@link #FEATURE_FAKETOUCH} for input devices with this capability.  Note
 * that unlike a distinct multitouch screen as defined by
 * {@link #FEATURE_TOUCHSCREEN_MULTITOUCH_DISTINCT}, these kinds of input
 * devices will not actually provide full two-finger gestures since the
 * input is being transformed to cursor movement on the screen.  That is,
 * single finger gestures will move a cursor; two-finger swipes will
 * result in single-finger touch events; other two-finger gestures will
 * result in the corresponding two-finger touch event.
 * @apiSince 13
 */

public static final java.lang.String FEATURE_FAKETOUCH_MULTITOUCH_DISTINCT = "android.hardware.faketouch.multitouch.distinct";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device does not have a touch screen, but
 * does support touch emulation for basic events that supports tracking
 * a hand of fingers (5 or more fingers) fully independently.
 * This is an extension of
 * {@link #FEATURE_FAKETOUCH} for input devices with this capability.  Note
 * that unlike a multitouch screen as defined by
 * {@link #FEATURE_TOUCHSCREEN_MULTITOUCH_JAZZHAND}, not all two finger
 * gestures can be detected due to the limitations described for
 * {@link #FEATURE_FAKETOUCH_MULTITOUCH_DISTINCT}.
 * @apiSince 13
 */

public static final java.lang.String FEATURE_FAKETOUCH_MULTITOUCH_JAZZHAND = "android.hardware.faketouch.multitouch.jazzhand";

/** {@hide} */

public static final java.lang.String FEATURE_FILE_BASED_ENCRYPTION = "android.software.file_based_encryption";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has biometric hardware to detect a fingerprint.
 * @apiSince 23
 */

public static final java.lang.String FEATURE_FINGERPRINT = "android.hardware.fingerprint";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports freeform window management.
 * Windows have title bars and can be moved and resized.
 * @apiSince 24
 */

public static final java.lang.String FEATURE_FREEFORM_WINDOW_MANAGEMENT = "android.software.freeform_window_management";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device has all of the inputs necessary to be considered a compatible game controller, or
 * includes a compatible game controller in the box.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_GAMEPAD = "android.hardware.gamepad";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports high fidelity sensor processing
 * capabilities.
 * @apiSince 23
 */

public static final java.lang.String FEATURE_HIFI_SENSORS = "android.hardware.sensor.hifi_sensors";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports a home screen that is replaceable
 * by third party applications.
 * @apiSince 18
 */

public static final java.lang.String FEATURE_HOME_SCREEN = "android.software.home_screen";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports adding new input methods implemented
 * with the {@link android.inputmethodservice.InputMethodService} API.
 * @apiSince 18
 */

public static final java.lang.String FEATURE_INPUT_METHODS = "android.software.input_methods";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}: The device has
 * the requisite kernel support for multinetworking-capable IPsec tunnels.
 *
 * <p>This feature implies that the device supports XFRM Interfaces (CONFIG_XFRM_INTERFACE), or
 * VTIs with kernel patches allowing updates of output/set mark via UPDSA.
 * @apiSince 29
 */

public static final java.lang.String FEATURE_IPSEC_TUNNELS = "android.software.ipsec_tunnels";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has biometric hardware to perform iris authentication.
 * @apiSince 29
 */

public static final java.lang.String FEATURE_IRIS = "android.hardware.biometrics.iris";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports leanback UI. This is
 * typically used in a living room television experience, but is a software
 * feature unlike {@link #FEATURE_TELEVISION}. Devices running with this
 * feature will use resources associated with the "television" UI mode.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_LEANBACK = "android.software.leanback";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports only leanback UI. Only
 * applications designed for this experience should be run, though this is
 * not enforced by the system.
 * @apiSince 26
 */

public static final java.lang.String FEATURE_LEANBACK_ONLY = "android.software.leanback_only";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports live TV and can display
 * contents from TV inputs implemented with the
 * {@link android.media.tv.TvInputService} API.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_LIVE_TV = "android.software.live_tv";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports live wallpapers.
 * @apiSince 7
 */

public static final java.lang.String FEATURE_LIVE_WALLPAPER = "android.software.live_wallpaper";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports one or more methods of
 * reporting current location.
 * @apiSince 8
 */

public static final java.lang.String FEATURE_LOCATION = "android.hardware.location";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has a Global Positioning System
 * receiver and can report precise location.
 * @apiSince 8
 */

public static final java.lang.String FEATURE_LOCATION_GPS = "android.hardware.location.gps";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device can report location with coarse
 * accuracy using a network-based geolocation system.
 * @apiSince 8
 */

public static final java.lang.String FEATURE_LOCATION_NETWORK = "android.hardware.location.network";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device supports creating secondary users and managed profiles via
 * {@link DevicePolicyManager}.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_MANAGED_USERS = "android.software.managed_users";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device can record audio via a
 * microphone.
 * @apiSince 8
 */

public static final java.lang.String FEATURE_MICROPHONE = "android.hardware.microphone";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device has a full implementation of the android.media.midi.* APIs.
 * @apiSince 23
 */

public static final java.lang.String FEATURE_MIDI = "android.software.midi";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device can communicate using Near-Field
 * Communications (NFC).
 * @apiSince 9
 */

public static final java.lang.String FEATURE_NFC = "android.hardware.nfc";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The Beam API is enabled on the device.
 * @apiSince 29
 */

public static final java.lang.String FEATURE_NFC_BEAM = "android.sofware.nfc.beam";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports host-
 * based NFC card emulation.
 * @apiSince 19
 */

public static final java.lang.String FEATURE_NFC_HOST_CARD_EMULATION = "android.hardware.nfc.hce";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports host-
 * based NFC-F card emulation.
 * @apiSince 24
 */

public static final java.lang.String FEATURE_NFC_HOST_CARD_EMULATION_NFCF = "android.hardware.nfc.hcef";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports eSE-
 * based NFC card emulation.
 * @apiSince 29
 */

public static final java.lang.String FEATURE_NFC_OFF_HOST_CARD_EMULATION_ESE = "android.hardware.nfc.ese";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports uicc-
 * based NFC card emulation.
 * @apiSince 29
 */

public static final java.lang.String FEATURE_NFC_OFF_HOST_CARD_EMULATION_UICC = "android.hardware.nfc.uicc";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports the OpenGL ES
 * <a href="http://www.khronos.org/registry/gles/extensions/ANDROID/ANDROID_extension_pack_es31a.txt">
 * Android Extension Pack</a>.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_OPENGLES_EXTENSION_PACK = "android.hardware.opengles.aep";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: This is a device dedicated to be primarily used
 * with keyboard, mouse or touchpad. This includes traditional desktop
 * computers, laptops and variants such as convertibles or detachables.
 * Due to the larger screen, the device will most likely use the
 * {@link #FEATURE_FREEFORM_WINDOW_MANAGEMENT} feature as well.
 * @apiSince 27
 */

public static final java.lang.String FEATURE_PC = "android.hardware.type.pc";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device supports picture-in-picture multi-window mode.
 * @apiSince 24
 */

public static final java.lang.String FEATURE_PICTURE_IN_PICTURE = "android.software.picture_in_picture";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device supports printing.
 * @apiSince 20
 */

public static final java.lang.String FEATURE_PRINTING = "android.software.print";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device's
 * {@link ActivityManager#isLowRamDevice() ActivityManager.isLowRamDevice()} method returns
 * true.
 * @apiSince 27
 */

public static final java.lang.String FEATURE_RAM_LOW = "android.hardware.ram.low";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device's
 * {@link ActivityManager#isLowRamDevice() ActivityManager.isLowRamDevice()} method returns
 * false.
 * @apiSince 27
 */

public static final java.lang.String FEATURE_RAM_NORMAL = "android.hardware.ram.normal";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports landscape orientation
 * screens.  For backwards compatibility, you can assume that if neither
 * this nor {@link #FEATURE_SCREEN_PORTRAIT} is set then the device supports
 * both portrait and landscape.
 * @apiSince 13
 */

public static final java.lang.String FEATURE_SCREEN_LANDSCAPE = "android.hardware.screen.landscape";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports portrait orientation
 * screens.  For backwards compatibility, you can assume that if neither
 * this nor {@link #FEATURE_SCREEN_LANDSCAPE} is set then the device supports
 * both portrait and landscape.
 * @apiSince 13
 */

public static final java.lang.String FEATURE_SCREEN_PORTRAIT = "android.hardware.screen.portrait";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device supports secure removal of users. When a user is deleted the data associated
 * with that user is securely deleted and no longer available.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_SECURELY_REMOVES_USERS = "android.software.securely_removes_users";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has a secure implementation of keyguard, meaning the
 * device supports PIN, pattern and password as defined in Android CDD
 * @apiSince 29
 */

public static final java.lang.String FEATURE_SECURE_LOCK_SCREEN = "android.software.secure_lock_screen";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes an accelerometer.
 * @apiSince 8
 */

public static final java.lang.String FEATURE_SENSOR_ACCELEROMETER = "android.hardware.sensor.accelerometer";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes an ambient temperature sensor.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_SENSOR_AMBIENT_TEMPERATURE = "android.hardware.sensor.ambient_temperature";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes a barometer (air
 * pressure sensor.)
 * @apiSince 9
 */

public static final java.lang.String FEATURE_SENSOR_BAROMETER = "android.hardware.sensor.barometer";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes a magnetometer (compass).
 * @apiSince 8
 */

public static final java.lang.String FEATURE_SENSOR_COMPASS = "android.hardware.sensor.compass";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes a gyroscope.
 * @apiSince 9
 */

public static final java.lang.String FEATURE_SENSOR_GYROSCOPE = "android.hardware.sensor.gyroscope";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes a heart rate monitor.
 * @apiSince 20
 */

public static final java.lang.String FEATURE_SENSOR_HEART_RATE = "android.hardware.sensor.heartrate";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The heart rate sensor on this device is an Electrocardiogram.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_SENSOR_HEART_RATE_ECG = "android.hardware.sensor.heartrate.ecg";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes a light sensor.
 * @apiSince 7
 */

public static final java.lang.String FEATURE_SENSOR_LIGHT = "android.hardware.sensor.light";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes a proximity sensor.
 * @apiSince 7
 */

public static final java.lang.String FEATURE_SENSOR_PROXIMITY = "android.hardware.sensor.proximity";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes a relative humidity sensor.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_SENSOR_RELATIVE_HUMIDITY = "android.hardware.sensor.relative_humidity";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes a hardware step counter.
 * @apiSince 19
 */

public static final java.lang.String FEATURE_SENSOR_STEP_COUNTER = "android.hardware.sensor.stepcounter";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device includes a hardware step detector.
 * @apiSince 19
 */

public static final java.lang.String FEATURE_SENSOR_STEP_DETECTOR = "android.hardware.sensor.stepdetector";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The SIP API is enabled on the device.
 * @apiSince 9
 */

public static final java.lang.String FEATURE_SIP = "android.software.sip";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports SIP-based VOIP.
 * @apiSince 9
 */

public static final java.lang.String FEATURE_SIP_VOIP = "android.software.sip.voip";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device has a StrongBox hardware-backed Keystore.
 * @apiSince 28
 */

public static final java.lang.String FEATURE_STRONGBOX_KEYSTORE = "android.hardware.strongbox_keystore";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has a telephony radio with data
 * communication support.
 * @apiSince 7
 */

public static final java.lang.String FEATURE_TELEPHONY = "android.hardware.telephony";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device supports telephony carrier restriction mechanism.
 *
 * <p>Devices declaring this feature must have an implementation of the
 * {@link android.telephony.TelephonyManager#getAllowedCarriers} and
 * {@link android.telephony.TelephonyManager#setAllowedCarriers}.
 * @hide
 */

public static final java.lang.String FEATURE_TELEPHONY_CARRIERLOCK = "android.hardware.telephony.carrierlock";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has a CDMA telephony stack.
 * @apiSince 7
 */

public static final java.lang.String FEATURE_TELEPHONY_CDMA = "android.hardware.telephony.cdma";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}: The device
 * supports embedded subscriptions on eUICCs.
 * @apiSince 28
 */

public static final java.lang.String FEATURE_TELEPHONY_EUICC = "android.hardware.telephony.euicc";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device has a GSM telephony stack.
 * @apiSince 7
 */

public static final java.lang.String FEATURE_TELEPHONY_GSM = "android.hardware.telephony.gsm";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}: The device
 * supports attaching to IMS implementations using the ImsService API in telephony.
 * @apiSince 29
 */

public static final java.lang.String FEATURE_TELEPHONY_IMS = "android.hardware.telephony.ims";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}: The device
 * supports cell-broadcast reception using the MBMS APIs.
 * @apiSince 28
 */

public static final java.lang.String FEATURE_TELEPHONY_MBMS = "android.hardware.telephony.mbms";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: This is a device dedicated to showing UI
 * on a television.  Television here is defined to be a typical living
 * room television experience: displayed on a big screen, where the user
 * is sitting far away from it, and the dominant form of input will be
 * something like a DPAD, not through touch or mouse.
 * @deprecated use {@link #FEATURE_LEANBACK} instead.
 * @apiSince 16
 * @deprecatedSince 21
 */

@Deprecated public static final java.lang.String FEATURE_TELEVISION = "android.hardware.type.television";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device's display has a touch screen.
 * @apiSince 8
 */

public static final java.lang.String FEATURE_TOUCHSCREEN = "android.hardware.touchscreen";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device's touch screen supports
 * multitouch sufficient for basic two-finger gesture detection.
 * @apiSince 7
 */

public static final java.lang.String FEATURE_TOUCHSCREEN_MULTITOUCH = "android.hardware.touchscreen.multitouch";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device's touch screen is capable of
 * tracking two or more fingers fully independently.
 * @apiSince 8
 */

public static final java.lang.String FEATURE_TOUCHSCREEN_MULTITOUCH_DISTINCT = "android.hardware.touchscreen.multitouch.distinct";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device's touch screen is capable of
 * tracking a full hand of fingers fully independently -- that is, 5 or
 * more simultaneous independent pointers.
 * @apiSince 9
 */

public static final java.lang.String FEATURE_TOUCHSCREEN_MULTITOUCH_JAZZHAND = "android.hardware.touchscreen.multitouch.jazzhand";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports connecting to USB accessories.
 * @apiSince 12
 */

public static final java.lang.String FEATURE_USB_ACCESSORY = "android.hardware.usb.accessory";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports connecting to USB devices
 * as the USB host.
 * @apiSince 12
 */

public static final java.lang.String FEATURE_USB_HOST = "android.hardware.usb.host";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device supports verified boot.
 * @apiSince 21
 */

public static final java.lang.String FEATURE_VERIFIED_BOOT = "android.software.verified_boot";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device implements headtracking suitable for a VR device.
 * @apiSince 26
 */

public static final java.lang.String FEATURE_VR_HEADTRACKING = "android.hardware.vr.headtracking";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device implements an optimized mode for virtual reality (VR) applications that handles
 * stereoscopic rendering of notifications, and disables most monocular system UI components
 * while a VR application has user focus.
 * Devices declaring this feature must include an application implementing a
 * {@link android.service.vr.VrListenerService} that can be targeted by VR applications via
 * {@link android.app.Activity#setVrModeEnabled}.
 * @deprecated use {@link #FEATURE_VR_MODE_HIGH_PERFORMANCE} instead.
 * @apiSince 24
 * @deprecatedSince 28
 */

@Deprecated public static final java.lang.String FEATURE_VR_MODE = "android.software.vr.mode";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device implements an optimized mode for virtual reality (VR) applications that handles
 * stereoscopic rendering of notifications, disables most monocular system UI components
 * while a VR application has user focus and meets extra CDD requirements to provide a
 * high-quality VR experience.
 * Devices declaring this feature must include an application implementing a
 * {@link android.service.vr.VrListenerService} that can be targeted by VR applications via
 * {@link android.app.Activity#setVrModeEnabled}.
 * and must meet CDD requirements to provide a high-quality VR experience.
 * @apiSince 24
 */

public static final java.lang.String FEATURE_VR_MODE_HIGH_PERFORMANCE = "android.hardware.vr.high_performance";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature(String, int)}: If this feature is supported, the Vulkan
 * implementation on this device is hardware accelerated, and the Vulkan native API will
 * enumerate at least one {@code VkPhysicalDevice}, and the feature version will indicate what
 * level of optional compute features that device supports beyond the Vulkan 1.0 requirements.
 * <p>
 * Compute level 0 indicates:
 * <ul>
 * <li>The {@code VK_KHR_variable_pointers} extension and
 *     {@code VkPhysicalDeviceVariablePointerFeaturesKHR::variablePointers} feature are
      supported.</li>
 * <li>{@code VkPhysicalDeviceLimits::maxPerStageDescriptorStorageBuffers} is at least 16.</li>
 * </ul>
 * @apiSince 26
 */

public static final java.lang.String FEATURE_VULKAN_HARDWARE_COMPUTE = "android.hardware.vulkan.compute";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature(String, int)}: If this feature is supported, the Vulkan
 * implementation on this device is hardware accelerated, and the Vulkan native API will
 * enumerate at least one {@code VkPhysicalDevice}, and the feature version will indicate what
 * level of optional hardware features limits it supports.
 * <p>
 * Level 0 includes the base Vulkan requirements as well as:
 * <ul><li>{@code VkPhysicalDeviceFeatures::textureCompressionETC2}</li></ul>
 * <p>
 * Level 1 additionally includes:
 * <ul>
 * <li>{@code VkPhysicalDeviceFeatures::fullDrawIndexUint32}</li>
 * <li>{@code VkPhysicalDeviceFeatures::imageCubeArray}</li>
 * <li>{@code VkPhysicalDeviceFeatures::independentBlend}</li>
 * <li>{@code VkPhysicalDeviceFeatures::geometryShader}</li>
 * <li>{@code VkPhysicalDeviceFeatures::tessellationShader}</li>
 * <li>{@code VkPhysicalDeviceFeatures::sampleRateShading}</li>
 * <li>{@code VkPhysicalDeviceFeatures::textureCompressionASTC_LDR}</li>
 * <li>{@code VkPhysicalDeviceFeatures::fragmentStoresAndAtomics}</li>
 * <li>{@code VkPhysicalDeviceFeatures::shaderImageGatherExtended}</li>
 * <li>{@code VkPhysicalDeviceFeatures::shaderUniformBufferArrayDynamicIndexing}</li>
 * <li>{@code VkPhysicalDeviceFeatures::shaderSampledImageArrayDynamicIndexing}</li>
 * </ul>
 * @apiSince 24
 */

public static final java.lang.String FEATURE_VULKAN_HARDWARE_LEVEL = "android.hardware.vulkan.level";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature(String, int)}: If this feature is supported, the Vulkan
 * implementation on this device is hardware accelerated, and the feature version will indicate
 * the highest {@code VkPhysicalDeviceProperties::apiVersion} supported by the physical devices
 * that support the hardware level indicated by {@link #FEATURE_VULKAN_HARDWARE_LEVEL}. The
 * feature version uses the same encoding as Vulkan version numbers:
 * <ul>
 * <li>Major version number in bits 31-22</li>
 * <li>Minor version number in bits 21-12</li>
 * <li>Patch version number in bits 11-0</li>
 * </ul>
 * A version of 1.1.0 or higher also indicates:
 * <ul>
 * <li>The {@code VK_ANDROID_external_memory_android_hardware_buffer} extension is
 *     supported.</li>
 * <li>{@code SYNC_FD} external semaphore and fence handles are supported.</li>
 * <li>{@code VkPhysicalDeviceSamplerYcbcrConversionFeatures::samplerYcbcrConversion} is
 *     supported.</li>
 * </ul>
 * @apiSince 24
 */

public static final java.lang.String FEATURE_VULKAN_HARDWARE_VERSION = "android.hardware.vulkan.version";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: This is a device dedicated to showing UI
 * on a watch. A watch here is defined to be a device worn on the body, perhaps on
 * the wrist. The user is very close when interacting with the device.
 * @apiSince 20
 */

public static final java.lang.String FEATURE_WATCH = "android.hardware.type.watch";

/**
 * Feature for {@link #getSystemAvailableFeatures} and {@link #hasSystemFeature}:
 * The device has a full implementation of the android.webkit.* APIs. Devices
 * lacking this feature will not have a functioning WebView implementation.
 * @apiSince 20
 */

public static final java.lang.String FEATURE_WEBVIEW = "android.software.webview";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports WiFi (802.11) networking.
 * @apiSince 8
 */

public static final java.lang.String FEATURE_WIFI = "android.hardware.wifi";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports Wi-Fi Aware.
 * @apiSince 26
 */

public static final java.lang.String FEATURE_WIFI_AWARE = "android.hardware.wifi.aware";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports Wi-Fi Direct networking.
 * @apiSince 14
 */

public static final java.lang.String FEATURE_WIFI_DIRECT = "android.hardware.wifi.direct";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports Wi-Fi Passpoint and all
 * Passpoint related APIs in {@link WifiManager} are supported. Refer to
 * {@link WifiManager#addOrUpdatePasspointConfiguration} for more info.
 * @apiSince 27
 */

public static final java.lang.String FEATURE_WIFI_PASSPOINT = "android.hardware.wifi.passpoint";

/**
 * Feature for {@link #getSystemAvailableFeatures} and
 * {@link #hasSystemFeature}: The device supports Wi-Fi RTT (IEEE 802.11mc).
 * @apiSince 28
 */

public static final java.lang.String FEATURE_WIFI_RTT = "android.hardware.wifi.rtt";

/**
 * Permission flag: The permission is disabled but may be granted. If
 * disabled the data protected by the permission should be protected
 * by a no-op (empty list, default error, etc) instead of crashing the
 * client.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_APPLY_RESTRICTION = 16384; // 0x4000

/**
 * Permission flag: The permission is granted by default because it
 * enables app functionality that is expected to work out-of-the-box
 * for providing a smooth user experience. For example, the phone app
 * is expected to have the phone permission.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_GRANTED_BY_DEFAULT = 32; // 0x20

/**
 * Permission flag: The permission is granted because the application holds a role.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_GRANTED_BY_ROLE = 32768; // 0x8000

/**
 * Permission flag: The permission is set in its current state
 * by device policy and neither apps nor the user can change
 * its state.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_POLICY_FIXED = 4; // 0x4

/**
 * Permission flag: The permission is restricted but the app is exempt
 * from the restriction and is allowed to hold this permission in its
 * full form and the exemption is provided by the installer on record.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_RESTRICTION_INSTALLER_EXEMPT = 2048; // 0x800

/**
 * Permission flag: The permission is restricted but the app is exempt
 * from the restriction and is allowed to hold this permission in its
 * full form and the exemption is provided by the system due to its
 * permission policy.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_RESTRICTION_SYSTEM_EXEMPT = 4096; // 0x1000

/**
 * Permission flag: The permission is restricted but the app is exempt
 * from the restriction and is allowed to hold this permission and the
 * exemption is provided by the system when upgrading from an OS version
 * where the permission was not restricted to an OS version where the
 * permission is restricted.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_RESTRICTION_UPGRADE_EXEMPT = 8192; // 0x2000

/**
 * Permission flag: The permission has to be reviewed before any of
 * the app components can run.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_REVIEW_REQUIRED = 64; // 0x40

/**
 * Permission flag: The permission is set in a granted state but
 * access to resources it guards is restricted by other means to
 * enable revoking a permission on legacy apps that do not support
 * runtime permissions. If this permission is upgraded to runtime
 * because the app was updated to support runtime permissions, the
 * the permission will be revoked in the upgrade process.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_REVOKE_ON_UPGRADE = 8; // 0x8

/**
 * Permission flag: The permission has not been explicitly requested by
 * the app but has been added automatically by the system. Revoke once
 * the app does explicitly request it.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_REVOKE_WHEN_REQUESTED = 128; // 0x80

/**
 * Permission flag: The permission is set in its current state
 * because the app is a component that is a part of the system.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_SYSTEM_FIXED = 16; // 0x10

/**
 * Permission flag: The permission is set in its current state
 * by the user and it is fixed, i.e. apps can no longer request
 * this permission.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_USER_FIXED = 2; // 0x2

/**
 * Permission flag: The permission's usage should be made highly visible to the user
 * when denied.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_USER_SENSITIVE_WHEN_DENIED = 512; // 0x200

/**
 * Permission flag: The permission's usage should be made highly visible to the user
 * when granted.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_USER_SENSITIVE_WHEN_GRANTED = 256; // 0x100

/**
 * Permission flag: The permission is set in its current state
 * by the user and apps can still request it at runtime.
 *
 * @hide
 */

public static final int FLAG_PERMISSION_USER_SET = 1; // 0x1

/**
 * Permission whitelist flag: permissions whitelisted by the installer.
 * Permissions can also be whitelisted by the system or on upgrade.
 * @apiSince 29
 */

public static final int FLAG_PERMISSION_WHITELIST_INSTALLER = 2; // 0x2

/**
 * Permission whitelist flag: permissions whitelisted by the system.
 * Permissions can also be whitelisted by the installer or on upgrade.
 * @apiSince 29
 */

public static final int FLAG_PERMISSION_WHITELIST_SYSTEM = 1; // 0x1

/**
 * Permission whitelist flag: permissions whitelisted by the system
 * when upgrading from an OS version where the permission was not
 * restricted to an OS version where the permission is restricted.
 * Permissions can also be whitelisted by the installer or the system.
 * @apiSince 29
 */

public static final int FLAG_PERMISSION_WHITELIST_UPGRADE = 4; // 0x4

/**
 * {@link PackageInfo} flag: return information about
 * activities in the package in {@link PackageInfo#activities}.
 * @apiSince 1
 */

public static final int GET_ACTIVITIES = 1; // 0x1

/**
 * {@link PackageInfo} flag: return information about
 * hardware preferences in
 * {@link PackageInfo#configPreferences PackageInfo.configPreferences},
 * and requested features in {@link PackageInfo#reqFeatures} and
 * {@link PackageInfo#featureGroups}.
 * @apiSince 3
 */

public static final int GET_CONFIGURATIONS = 16384; // 0x4000

/**
 * @deprecated replaced with {@link #MATCH_DISABLED_COMPONENTS}
 * @apiSince 1
 * @deprecatedSince 24
 */

@Deprecated public static final int GET_DISABLED_COMPONENTS = 512; // 0x200

/**
 * @deprecated replaced with {@link #MATCH_DISABLED_UNTIL_USED_COMPONENTS}.
 * @apiSince 18
 * @deprecatedSince 24
 */

@Deprecated public static final int GET_DISABLED_UNTIL_USED_COMPONENTS = 32768; // 0x8000

/**
 * {@link PackageInfo} flag: return the
 * {@link PackageInfo#gids group ids} that are associated with an
 * application.
 * This applies for any API returning a PackageInfo class, either
 * directly or nested inside of another.
 * @apiSince 1
 */

public static final int GET_GIDS = 256; // 0x100

/**
 * {@link PackageInfo} flag: return information about
 * instrumentation in the package in
 * {@link PackageInfo#instrumentation}.
 * @apiSince 1
 */

public static final int GET_INSTRUMENTATION = 16; // 0x10

/**
 * {@link PackageInfo} flag: return information about the
 * intent filters supported by the activity.
 * @apiSince 1
 */

public static final int GET_INTENT_FILTERS = 32; // 0x20

/**
 * {@link ComponentInfo} flag: return the {@link ComponentInfo#metaData}
 * data {@link android.os.Bundle}s that are associated with a component.
 * This applies for any API returning a ComponentInfo subclass.
 * @apiSince 1
 */

public static final int GET_META_DATA = 128; // 0x80

/**
 * {@link PackageInfo} flag: return information about
 * permissions in the package in
 * {@link PackageInfo#permissions}.
 * @apiSince 1
 */

public static final int GET_PERMISSIONS = 4096; // 0x1000

/**
 * {@link PackageInfo} flag: return information about
 * content providers in the package in
 * {@link PackageInfo#providers}.
 * @apiSince 1
 */

public static final int GET_PROVIDERS = 8; // 0x8

/**
 * {@link PackageInfo} flag: return information about
 * intent receivers in the package in
 * {@link PackageInfo#receivers}.
 * @apiSince 1
 */

public static final int GET_RECEIVERS = 2; // 0x2

/**
 * {@link ResolveInfo} flag: return the IntentFilter that
 * was matched for a particular ResolveInfo in
 * {@link ResolveInfo#filter}.
 * @apiSince 1
 */

public static final int GET_RESOLVED_FILTER = 64; // 0x40

/**
 * {@link PackageInfo} flag: return information about
 * services in the package in {@link PackageInfo#services}.
 * @apiSince 1
 */

public static final int GET_SERVICES = 4; // 0x4

/**
 * {@link ApplicationInfo} flag: return the
 * {@link ApplicationInfo#sharedLibraryFiles paths to the shared libraries}
 * that are associated with an application.
 * This applies for any API returning an ApplicationInfo class, either
 * directly or nested inside of another.
 * @apiSince 1
 */

public static final int GET_SHARED_LIBRARY_FILES = 1024; // 0x400

/**
 * {@link PackageInfo} flag: return information about the
 * signatures included in the package.
 *
 * @deprecated use {@code GET_SIGNING_CERTIFICATES} instead
 * @apiSince 1
 * @deprecatedSince 28
 */

@Deprecated public static final int GET_SIGNATURES = 64; // 0x40

/**
 * {@link PackageInfo} flag: return the signing certificates associated with
 * this package.  Each entry is a signing certificate that the package
 * has proven it is authorized to use, usually a past signing certificate from
 * which it has rotated.
 * @apiSince 28
 */

public static final int GET_SIGNING_CERTIFICATES = 134217728; // 0x8000000

/**
 * @deprecated replaced with {@link #MATCH_UNINSTALLED_PACKAGES}
 * @apiSince 3
 * @deprecatedSince 24
 */

@Deprecated public static final int GET_UNINSTALLED_PACKAGES = 8192; // 0x2000

/**
 * {@link ProviderInfo} flag: return the
 * {@link ProviderInfo#uriPermissionPatterns URI permission patterns}
 * that are associated with a content provider.
 * This applies for any API returning a ProviderInfo class, either
 * directly or nested inside of another.
 * @apiSince 1
 */

public static final int GET_URI_PERMISSION_PATTERNS = 2048; // 0x800

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the package is already installed.
 *
 * @hide
 */

public static final int INSTALL_FAILED_ALREADY_EXISTS = -1; // 0xffffffff

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package failed because it contains a content provider with the same authority as a
 * provider already installed in the system.
 *
 * @hide
 */

public static final int INSTALL_FAILED_CONFLICTING_PROVIDER = -13; // 0xfffffff3

/**
 * Installation return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if a secure container mount point couldn't be
 * accessed on external media.
 *
 * @hide
 */

public static final int INSTALL_FAILED_CONTAINER_ERROR = -18; // 0xffffffee

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the package being installed contains native code, but none that is compatible with the
 * device's CPU_ABI.
 *
 * @hide
 */

public static final int INSTALL_FAILED_CPU_ABI_INCOMPATIBLE = -16; // 0xfffffff0

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package failed while optimizing and validating its dex files, either because there
 * was not enough storage or the validation failed.
 *
 * @hide
 */

public static final int INSTALL_FAILED_DEXOPT = -11; // 0xfffffff5

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if a package is already installed with the same name.
 *
 * @hide
 */

public static final int INSTALL_FAILED_DUPLICATE_PACKAGE = -5; // 0xfffffffb

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the package manager service found that the device didn't have enough storage space to
 * install the app.
 *
 * @hide
 */

public static final int INSTALL_FAILED_INSUFFICIENT_STORAGE = -4; // 0xfffffffc

/**
 * Installation failed return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the system failed to install the package
 * because of system issues.
 *
 * @hide
 */

public static final int INSTALL_FAILED_INTERNAL_ERROR = -110; // 0xffffff92

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the package archive file is invalid.
 *
 * @hide
 */

public static final int INSTALL_FAILED_INVALID_APK = -2; // 0xfffffffe

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package couldn't be installed in the specified install location.
 *
 * @hide
 */

public static final int INSTALL_FAILED_INVALID_INSTALL_LOCATION = -19; // 0xffffffed

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the URI passed in is invalid.
 *
 * @hide
 */

public static final int INSTALL_FAILED_INVALID_URI = -3; // 0xfffffffd

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package couldn't be installed in the specified install location because the media
 * is not available.
 *
 * @hide
 */

public static final int INSTALL_FAILED_MEDIA_UNAVAILABLE = -20; // 0xffffffec

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package uses a feature that is not available.
 *
 * @hide
 */

public static final int INSTALL_FAILED_MISSING_FEATURE = -17; // 0xffffffef

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package uses a shared library that is not available.
 *
 * @hide
 */

public static final int INSTALL_FAILED_MISSING_SHARED_LIBRARY = -9; // 0xfffffff7

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package failed because the current SDK version is newer than that required by the
 * package.
 *
 * @hide
 */

public static final int INSTALL_FAILED_NEWER_SDK = -14; // 0xfffffff2

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the requested shared user does not exist.
 *
 * @hide
 */

public static final int INSTALL_FAILED_NO_SHARED_USER = -6; // 0xfffffffa

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package failed because the current SDK version is older than that required by the
 * package.
 *
 * @hide
 */

public static final int INSTALL_FAILED_OLDER_SDK = -12; // 0xfffffff4

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the package changed from what the calling program expected.
 *
 * @hide
 */

public static final int INSTALL_FAILED_PACKAGE_CHANGED = -23; // 0xffffffe9

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the old package has target SDK high enough to support runtime permission and the new
 * package has target SDK low enough to not support runtime permissions.
 *
 * @hide
 */

public static final int INSTALL_FAILED_PERMISSION_MODEL_DOWNGRADE = -26; // 0xffffffe6

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package uses a shared library that is not available.
 *
 * @hide
 */

public static final int INSTALL_FAILED_REPLACE_COULDNT_DELETE = -10; // 0xfffffff6

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package attempts to downgrade the target sandbox version of the app.
 *
 * @hide
 */

public static final int INSTALL_FAILED_SANDBOX_VERSION_DOWNGRADE = -27; // 0xffffffe5

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package is requested a shared user which is already installed on the device and
 * does not have matching signature.
 *
 * @hide
 */

public static final int INSTALL_FAILED_SHARED_USER_INCOMPATIBLE = -8; // 0xfffffff8

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package failed because it has specified that it is a test-only package and the
 * caller has not supplied the {@link #INSTALL_ALLOW_TEST} flag.
 *
 * @hide
 */

public static final int INSTALL_FAILED_TEST_ONLY = -15; // 0xfffffff1

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if a previously installed package of the same name has a different signature than the new
 * package (and the old package's data was not removed).
 *
 * @hide
 */

public static final int INSTALL_FAILED_UPDATE_INCOMPATIBLE = -7; // 0xfffffff9

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package couldn't be installed because the verification did not succeed.
 *
 * @hide
 */

public static final int INSTALL_FAILED_VERIFICATION_FAILURE = -22; // 0xffffffea

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * if the new package couldn't be installed because the verification timed out.
 *
 * @hide
 */

public static final int INSTALL_FAILED_VERIFICATION_TIMEOUT = -21; // 0xffffffeb

/**
 * Installation parse return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the parser was unable to retrieve the
 * AndroidManifest.xml file.
 *
 * @hide
 */

public static final int INSTALL_PARSE_FAILED_BAD_MANIFEST = -101; // 0xffffff9b

/**
 * Installation parse return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the parser encountered a bad or missing
 * package name in the manifest.
 *
 * @hide
 */

public static final int INSTALL_PARSE_FAILED_BAD_PACKAGE_NAME = -106; // 0xffffff96

/**
 * Installation parse return code: tthis is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the parser encountered a bad shared user id
 * name in the manifest.
 *
 * @hide
 */

public static final int INSTALL_PARSE_FAILED_BAD_SHARED_USER_ID = -107; // 0xffffff95

/**
 * Installation parse return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the parser encountered a
 * CertificateEncodingException in one of the files in the .apk.
 *
 * @hide
 */

public static final int INSTALL_PARSE_FAILED_CERTIFICATE_ENCODING = -105; // 0xffffff97

/**
 * Installation parse return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the parser found inconsistent certificates on
 * the files in the .apk.
 *
 * @hide
 */

public static final int INSTALL_PARSE_FAILED_INCONSISTENT_CERTIFICATES = -104; // 0xffffff98

/**
 * Installation parse return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the parser did not find any actionable tags
 * (instrumentation or application) in the manifest.
 *
 * @hide
 */

public static final int INSTALL_PARSE_FAILED_MANIFEST_EMPTY = -109; // 0xffffff93

/**
 * Installation parse return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the parser encountered some structural
 * problem in the manifest.
 *
 * @hide
 */

public static final int INSTALL_PARSE_FAILED_MANIFEST_MALFORMED = -108; // 0xffffff94

/**
 * Installation parse return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the parser was given a path that is not a
 * file, or does not end with the expected '.apk' extension.
 *
 * @hide
 */

public static final int INSTALL_PARSE_FAILED_NOT_APK = -100; // 0xffffff9c

/**
 * Installation parse return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the parser did not find any certificates in
 * the .apk.
 *
 * @hide
 */

public static final int INSTALL_PARSE_FAILED_NO_CERTIFICATES = -103; // 0xffffff99

/**
 * Installation parse return code: this is passed in the
 * {@link PackageInstaller#EXTRA_LEGACY_STATUS} if the parser encountered an unexpected
 * exception.
 *
 * @hide
 */

public static final int INSTALL_PARSE_FAILED_UNEXPECTED_EXCEPTION = -102; // 0xffffff9a

/**
 * Code indicating that this package was installed as part of restoring from another device.
 * @apiSince 26
 */

public static final int INSTALL_REASON_DEVICE_RESTORE = 2; // 0x2

/**
 * Code indicating that this package was installed as part of device setup.
 * @apiSince 26
 */

public static final int INSTALL_REASON_DEVICE_SETUP = 3; // 0x3

/**
 * Code indicating that this package was installed due to enterprise policy.
 * @apiSince 26
 */

public static final int INSTALL_REASON_POLICY = 1; // 0x1

/**
 * Code indicating that the reason for installing this package is unknown.
 * @apiSince 26
 */

public static final int INSTALL_REASON_UNKNOWN = 0; // 0x0

/**
 * Code indicating that the package installation was initiated by the user.
 * @apiSince 26
 */

public static final int INSTALL_REASON_USER = 4; // 0x4

/**
 * Installation return code: this is passed in the {@link PackageInstaller#EXTRA_LEGACY_STATUS}
 * on success.
 *
 * @hide
 */

public static final int INSTALL_SUCCEEDED = 1; // 0x1

/**
 * Used as the {@code status} argument for
 * {@link #updateIntentVerificationStatusAsUser} to indicate that the User
 * will never be prompted the Intent Disambiguation Dialog if there are two
 * or more resolution of the Intent. The default App for the domain(s)
 * specified in the IntentFilter will also ALWAYS be used.
 *
 * @hide
 */

public static final int INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_ALWAYS = 2; // 0x2

/**
 * Used as the {@code status} argument for
 * {@link #updateIntentVerificationStatusAsUser} to indicate that this app
 * should always be considered as an ambiguous candidate for handling the
 * matching Intent even if there are other candidate apps in the "always"
 * state. Put another way: if there are any 'always ask' apps in a set of
 * more than one candidate app, then a disambiguation is *always* presented
 * even if there is another candidate app with the 'always' state.
 *
 * @hide
 */

public static final int INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_ALWAYS_ASK = 4; // 0x4

/**
 * Used as the {@code status} argument for
 * {@link #updateIntentVerificationStatusAsUser} to indicate that the User
 * will always be prompted the Intent Disambiguation Dialog if there are two
 * or more Intent resolved for the IntentFilter's domain(s).
 *
 * @hide
 */

public static final int INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_ASK = 1; // 0x1

/**
 * Used as the {@code status} argument for
 * {@link #updateIntentVerificationStatusAsUser} to indicate that the User
 * may be prompted the Intent Disambiguation Dialog if there are two or more
 * Intent resolved. The default App for the domain(s) specified in the
 * IntentFilter will also NEVER be presented to the User.
 *
 * @hide
 */

public static final int INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_NEVER = 3; // 0x3

/**
 * Internal status code to indicate that an IntentFilter verification result is not specified.
 *
 * @hide
 */

public static final int INTENT_FILTER_DOMAIN_VERIFICATION_STATUS_UNDEFINED = 0; // 0x0

/**
 * Used as the {@code verificationCode} argument for
 * {@link PackageManager#verifyIntentFilter} to indicate that the calling
 * IntentFilter Verifier confirms that the IntentFilter is NOT verified.
 *
 * @hide
 */

public static final int INTENT_FILTER_VERIFICATION_FAILURE = -1; // 0xffffffff

/**
 * Used as the {@code verificationCode} argument for
 * {@link PackageManager#verifyIntentFilter} to indicate that the calling
 * IntentFilter Verifier confirms that the IntentFilter is verified.
 *
 * @hide
 */

public static final int INTENT_FILTER_VERIFICATION_SUCCESS = 1; // 0x1

/**
 * Mask for all permission flags.
 *
 * @hide
 *
 * @deprecated Don't use - does not capture all flags.
 */

@Deprecated public static final int MASK_PERMISSION_FLAGS = 255; // 0xff

/**
 * Querying flag: if set and if the platform is doing any filtering of the
 * results, then the filtering will not happen. This is a synonym for saying
 * that all results should be returned.
 * <p>
 * <em>This flag should be used with extreme care.</em>
 * @apiSince 23
 */

public static final int MATCH_ALL = 131072; // 0x20000

/**
 * Allows querying of packages installed for any user, not just the specific one. This flag
 * is only meant for use by apps that have INTERACT_ACROSS_USERS permission.
 * @hide
 */

public static final int MATCH_ANY_USER = 4194304; // 0x400000

/**
 * {@link PackageInfo} flag: include APEX packages that are currently
 * installed. In APEX terminology, this corresponds to packages that are
 * currently active, i.e. mounted and available to other processes of the OS.
 * In particular, this flag alone will not match APEX files that are staged
 * for activation at next reboot.
 * @apiSince 29
 */

public static final int MATCH_APEX = 1073741824; // 0x40000000

/**
 * Resolution and querying flag: if set, only filters that support the
 * {@link android.content.Intent#CATEGORY_DEFAULT} will be considered for
 * matching.  This is a synonym for including the CATEGORY_DEFAULT in your
 * supplied Intent.
 * @apiSince 1
 */

public static final int MATCH_DEFAULT_ONLY = 65536; // 0x10000

/**
 * Querying flag: automatically match components based on their Direct Boot
 * awareness and the current user state.
 * <p>
 * Since the default behavior is to automatically apply the current user
 * state, this is effectively a sentinel value that doesn't change the
 * output of any queries based on its presence or absence.
 * <p>
 * Instead, this value can be useful in conjunction with
 * {@link android.os.StrictMode.VmPolicy.Builder#detectImplicitDirectBoot()}
 * to detect when a caller is relying on implicit automatic matching,
 * instead of confirming the explicit behavior they want, using a
 * combination of these flags:
 * <ul>
 * <li>{@link #MATCH_DIRECT_BOOT_AWARE}
 * <li>{@link #MATCH_DIRECT_BOOT_UNAWARE}
 * <li>{@link #MATCH_DIRECT_BOOT_AUTO}
 * </ul>
 * @apiSince 29
 */

public static final int MATCH_DIRECT_BOOT_AUTO = 268435456; // 0x10000000

/**
 * Querying flag: match components which are direct boot <em>aware</em> in
 * the returned info, regardless of the current user state.
 * <p>
 * When neither {@link #MATCH_DIRECT_BOOT_AWARE} nor
 * {@link #MATCH_DIRECT_BOOT_UNAWARE} are specified, the default behavior is
 * to match only runnable components based on the user state. For example,
 * when a user is started but credentials have not been presented yet, the
 * user is running "locked" and only {@link #MATCH_DIRECT_BOOT_AWARE}
 * components are returned. Once the user credentials have been presented,
 * the user is running "unlocked" and both {@link #MATCH_DIRECT_BOOT_AWARE}
 * and {@link #MATCH_DIRECT_BOOT_UNAWARE} components are returned.
 *
 * @see UserManager#isUserUnlocked()
 * @apiSince 24
 */

public static final int MATCH_DIRECT_BOOT_AWARE = 524288; // 0x80000

/**
 * Querying flag: match components which are direct boot <em>unaware</em> in
 * the returned info, regardless of the current user state.
 * <p>
 * When neither {@link #MATCH_DIRECT_BOOT_AWARE} nor
 * {@link #MATCH_DIRECT_BOOT_UNAWARE} are specified, the default behavior is
 * to match only runnable components based on the user state. For example,
 * when a user is started but credentials have not been presented yet, the
 * user is running "locked" and only {@link #MATCH_DIRECT_BOOT_AWARE}
 * components are returned. Once the user credentials have been presented,
 * the user is running "unlocked" and both {@link #MATCH_DIRECT_BOOT_AWARE}
 * and {@link #MATCH_DIRECT_BOOT_UNAWARE} components are returned.
 *
 * @see UserManager#isUserUnlocked()
 * @apiSince 24
 */

public static final int MATCH_DIRECT_BOOT_UNAWARE = 262144; // 0x40000

/**
 * {@link PackageInfo} flag: include disabled components in the returned info.
 * @apiSince 24
 */

public static final int MATCH_DISABLED_COMPONENTS = 512; // 0x200

/**
 * {@link PackageInfo} flag: include disabled components which are in
 * that state only because of {@link #COMPONENT_ENABLED_STATE_DISABLED_UNTIL_USED}
 * in the returned info.  Note that if you set this flag, applications
 * that are in this disabled state will be reported as enabled.
 * @apiSince 24
 */

public static final int MATCH_DISABLED_UNTIL_USED_COMPONENTS = 32768; // 0x8000

/**
 * Internal {@link PackageInfo} flag: include only components on the system image.
 * This will not return information on any unbundled update to system components.
 * @hide
 */

public static final int MATCH_FACTORY_ONLY = 2097152; // 0x200000

/**
 * Internal {@link PackageInfo} flag: include components that are part of an
 * instant app. By default, instant app components are not matched.
 * @hide
 */

public static final int MATCH_INSTANT = 8388608; // 0x800000

/**
 * Combination of MATCH_ANY_USER and MATCH_UNINSTALLED_PACKAGES to mean any known
 * package.
 * @hide
 */

public static final int MATCH_KNOWN_PACKAGES = 4202496; // 0x402000

/**
 * Querying flag: include only components from applications that are marked
 * with {@link ApplicationInfo#FLAG_SYSTEM}.
 * @apiSince 24
 */

public static final int MATCH_SYSTEM_ONLY = 1048576; // 0x100000

/**
 * Flag parameter to retrieve some information about all applications (even
 * uninstalled ones) which have data directories. This state could have
 * resulted if applications have been deleted with flag
 * {@code DONT_DELETE_DATA} with a possibility of being replaced or
 * reinstalled in future.
 * <p>
 * Note: this flag may cause less information about currently installed
 * applications to be returned.
 * @apiSince 24
 */

public static final int MATCH_UNINSTALLED_PACKAGES = 8192; // 0x2000

/**
 * Can be used as the {@code millisecondsToDelay} argument for
 * {@link PackageManager#extendVerificationTimeout}. This is the
 * maximum time {@code PackageManager} waits for the verification
 * agent to return (in milliseconds).
 * @apiSince 17
 */

public static final long MAXIMUM_VERIFICATION_TIMEOUT = 3600000L; // 0x36ee80L

/**
 * Permission check result: this is returned by {@link #checkPermission}
 * if the permission has not been granted to the given package.
 * @apiSince 1
 */

public static final int PERMISSION_DENIED = -1; // 0xffffffff

/**
 * Permission check result: this is returned by {@link #checkPermission}
 * if the permission has been granted to the given package.
 * @apiSince 1
 */

public static final int PERMISSION_GRANTED = 0; // 0x0

/**
 * Flag to denote that a package should be hidden from any suggestions to the user.
 * @hide
 * @see #setDistractingPackageRestrictions(String[], int)
 */

public static final int RESTRICTION_HIDE_FROM_SUGGESTIONS = 1; // 0x1

/**
 * Flag to denote that a package's notifications should be hidden.
 * @hide
 * @see #setDistractingPackageRestrictions(String[], int)
 */

public static final int RESTRICTION_HIDE_NOTIFICATIONS = 2; // 0x2

/**
 * Flag to denote no restrictions. This should be used to clear any restrictions that may have
 * been previously set for the package.
 * @hide
 * @see #setDistractingPackageRestrictions(String[], int)
 */

public static final int RESTRICTION_NONE = 0; // 0x0

/**
 * Signature check result: this is returned by {@link #checkSignatures}
 * if the first package is not signed but the second is.
 * @apiSince 1
 */

public static final int SIGNATURE_FIRST_NOT_SIGNED = -1; // 0xffffffff

/**
 * Signature check result: this is returned by {@link #checkSignatures}
 * if all signatures on the two packages match.
 * @apiSince 1
 */

public static final int SIGNATURE_MATCH = 0; // 0x0

/**
 * Signature check result: this is returned by {@link #checkSignatures}
 * if neither of the two packages is signed.
 * @apiSince 1
 */

public static final int SIGNATURE_NEITHER_SIGNED = 1; // 0x1

/**
 * Signature check result: this is returned by {@link #checkSignatures}
 * if not all signatures on both packages match.
 * @apiSince 1
 */

public static final int SIGNATURE_NO_MATCH = -3; // 0xfffffffd

/**
 * Signature check result: this is returned by {@link #checkSignatures}
 * if the second package is not signed but the first is.
 * @apiSince 1
 */

public static final int SIGNATURE_SECOND_NOT_SIGNED = -2; // 0xfffffffe

/**
 * Signature check result: this is returned by {@link #checkSignatures}
 * if either of the packages are not valid.
 * @apiSince 1
 */

public static final int SIGNATURE_UNKNOWN_PACKAGE = -4; // 0xfffffffc

/**
 * This is a library that contains components apps can invoke. For
 * example, a services for apps to bind to, or standard chooser UI,
 * etc. This library is versioned and backwards compatible. Clients
 * should check its version via {@link android.ext.services.Version
 * #getVersionCode()} and avoid calling APIs added in later versions.
 *
 * @hide
 */

public static final java.lang.String SYSTEM_SHARED_LIBRARY_SERVICES = "android.ext.services";

/**
 * This is a library that contains components apps can dynamically
 * load. For example, new widgets, helper classes, etc. This library
 * is versioned and backwards compatible. Clients should check its
 * version via {@link android.ext.shared.Version#getVersionCode()}
 * and avoid calling APIs added in later versions.
 *
 * @hide
 */

public static final java.lang.String SYSTEM_SHARED_LIBRARY_SHARED = "android.ext.shared";

/**
 * Used as the {@code verificationCode} argument for
 * {@link PackageManager#verifyPendingInstall} to indicate that the calling
 * package verifier allows the installation to proceed.
 * @apiSince 14
 */

public static final int VERIFICATION_ALLOW = 1; // 0x1

/**
 * Used as the {@code verificationCode} argument for
 * {@link PackageManager#verifyPendingInstall} to indicate the calling
 * package verifier does not vote to allow the installation to proceed.
 * @apiSince 14
 */

public static final int VERIFICATION_REJECT = -1; // 0xffffffff

/**
 * Constant for specifying the highest installed package version code.
 * @apiSince 26
 */

public static final int VERSION_CODE_HIGHEST = -1; // 0xffffffff
/**
 * Callback use to notify the callers of module registration that the operation
 * has finished.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class DexModuleRegisterCallback {

public DexModuleRegisterCallback() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public abstract void onDexModuleRegistered(java.lang.String dexModulePath, boolean success, java.lang.String message);
}

/**
 * This exception is thrown when a given package, application, or component
 * name cannot be found.
 * @apiSince 1
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class NameNotFoundException extends android.util.AndroidException {

/** @apiSince 1 */

public NameNotFoundException() { throw new RuntimeException("Stub!"); }

/** @apiSince 1 */

public NameNotFoundException(java.lang.String name) { throw new RuntimeException("Stub!"); }
}

/**
 * Listener for changes in permissions granted to a UID.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface OnPermissionsChangedListener {

/**
 * Called when the permissions for a UID change.
 * @param uid The UID with a change.
 * @apiSince REL
 */

public void onPermissionsChanged(int uid);
}

/**
 * Permission flags set when granting or revoking a permission.
 *
 * <br>
 * Value is {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SET}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_POLICY_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_REVOKE_ON_UPGRADE}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_SYSTEM_FIXED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_GRANTED_BY_DEFAULT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SENSITIVE_WHEN_GRANTED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_USER_SENSITIVE_WHEN_DENIED}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_UPGRADE_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_SYSTEM_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_RESTRICTION_INSTALLER_EXEMPT}, {@link android.content.pm.PackageManager#FLAG_PERMISSION_APPLY_RESTRICTION}, or {@link android.content.pm.PackageManager#FLAG_PERMISSION_GRANTED_BY_ROLE}
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface PermissionFlags {
}

}

