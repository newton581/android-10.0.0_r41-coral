
 /**
 * Provides utility classes to export protocol buffers from the system.
 *
 * {@hide}
 */

package android.util.proto;
