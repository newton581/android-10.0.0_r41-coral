/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;

import android.os.Parcelable;

/**
 * Contains information about a call's attributes as passed up from the HAL. If there are multiple
 * ongoing calls, the CallAttributes will pertain to the call in the foreground.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CallAttributes implements android.os.Parcelable {

/**
 * @param state This value must never be {@code null}.
 
 * @param networkType Value is {@link android.telephony.TelephonyManager#NETWORK_TYPE_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IDEN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IWLAN}, android.telephony.TelephonyManager.NETWORK_TYPE_LTE_CA, or {@link android.telephony.TelephonyManager#NETWORK_TYPE_NR}
 
 * @param callQuality This value must never be {@code null}.
 * @apiSince REL
 */

public CallAttributes(@android.annotation.NonNull android.telephony.PreciseCallState state, int networkType, @android.annotation.NonNull android.telephony.CallQuality callQuality) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Returns the {@link PreciseCallState} of the call.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.PreciseCallState getPreciseCallState() { throw new RuntimeException("Stub!"); }

/**
 * Returns the {@link TelephonyManager#NetworkType} of the call.
 *
 * @see TelephonyManager#NETWORK_TYPE_UNKNOWN
 * @see TelephonyManager#NETWORK_TYPE_GPRS
 * @see TelephonyManager#NETWORK_TYPE_EDGE
 * @see TelephonyManager#NETWORK_TYPE_UMTS
 * @see TelephonyManager#NETWORK_TYPE_CDMA
 * @see TelephonyManager#NETWORK_TYPE_EVDO_0
 * @see TelephonyManager#NETWORK_TYPE_EVDO_A
 * @see TelephonyManager#NETWORK_TYPE_1xRTT
 * @see TelephonyManager#NETWORK_TYPE_HSDPA
 * @see TelephonyManager#NETWORK_TYPE_HSUPA
 * @see TelephonyManager#NETWORK_TYPE_HSPA
 * @see TelephonyManager#NETWORK_TYPE_IDEN
 * @see TelephonyManager#NETWORK_TYPE_EVDO_B
 * @see TelephonyManager#NETWORK_TYPE_LTE
 * @see TelephonyManager#NETWORK_TYPE_EHRPD
 * @see TelephonyManager#NETWORK_TYPE_HSPAP
 * @see TelephonyManager#NETWORK_TYPE_GSM
 * @see TelephonyManager#NETWORK_TYPE_TD_SCDMA
 * @see TelephonyManager#NETWORK_TYPE_IWLAN
 * @see TelephonyManager#NETWORK_TYPE_LTE_CA
 * @see TelephonyManager#NETWORK_TYPE_NR
 
 * @return Value is {@link android.telephony.TelephonyManager#NETWORK_TYPE_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IDEN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IWLAN}, android.telephony.TelephonyManager.NETWORK_TYPE_LTE_CA, or {@link android.telephony.TelephonyManager#NETWORK_TYPE_NR}
 * @apiSince REL
 */

public int getNetworkType() { throw new RuntimeException("Stub!"); }

/**
 * Returns the {#link CallQuality} of the call.
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.CallQuality getCallQuality() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/**
 * {@link Parcelable#describeContents}
 
 * @return Value is either <code>0</code> or {@link android.os.Parcelable#CONTENTS_FILE_DESCRIPTOR}
 * @apiSince REL
 */

public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * {@link Parcelable#writeToParcel}
 
 * @param flags Value is either <code>0</code> or a combination of {@link android.os.Parcelable#PARCELABLE_WRITE_RETURN_VALUE}, and android.os.Parcelable.PARCELABLE_ELIDE_DUPLICATES
 * @apiSince REL
 */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.CallAttributes> CREATOR;
static { CREATOR = null; }
}

