/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.webkit;


/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class FindActionModeCallback implements android.view.ActionMode.Callback, android.text.TextWatcher, android.view.View.OnClickListener, android.webkit.WebView.FindListener {

/** @apiSince REL */

public FindActionModeCallback(android.content.Context context) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void finish() { throw new RuntimeException("Stub!"); }

/**
 * Place text in the text field so it can be searched for.  Need to press
 * the find next or find previous button to find all of the matches.
 * @apiSince REL
 */

public void setText(java.lang.String text) { throw new RuntimeException("Stub!"); }

/**
 * Set the WebView to search.
 *
 * @param webView an implementation of WebView
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void setWebView(@android.annotation.NonNull android.webkit.WebView webView) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onFindResultReceived(int activeMatchOrdinal, int numberOfMatches, boolean isDoneCounting) { throw new RuntimeException("Stub!"); }

/**
 * Highlight all the instances of the string from mEditText in mWebView.
 * @apiSince REL
 */

public void findAll() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void showSoftInput() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void updateMatchCount(int matchIndex, int matchCount, boolean isEmptyFind) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onClick(android.view.View v) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean onCreateActionMode(android.view.ActionMode mode, android.view.Menu menu) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onDestroyActionMode(android.view.ActionMode mode) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean onPrepareActionMode(android.view.ActionMode mode, android.view.Menu menu) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean onActionItemClicked(android.view.ActionMode mode, android.view.MenuItem item) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void beforeTextChanged(java.lang.CharSequence s, int start, int count, int after) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onTextChanged(java.lang.CharSequence s, int start, int before, int count) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void afterTextChanged(android.text.Editable s) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int getActionModeGlobalBottom() { throw new RuntimeException("Stub!"); }
/** @apiSince REL */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class NoAction implements android.view.ActionMode.Callback {

public NoAction() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean onCreateActionMode(android.view.ActionMode mode, android.view.Menu menu) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean onPrepareActionMode(android.view.ActionMode mode, android.view.Menu menu) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean onActionItemClicked(android.view.ActionMode mode, android.view.MenuItem item) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void onDestroyActionMode(android.view.ActionMode mode) { throw new RuntimeException("Stub!"); }
}

}

