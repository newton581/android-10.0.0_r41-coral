/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.location;


/**
 * A class containing a GPS satellite Navigation Message.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class GpsNavigationMessage implements android.os.Parcelable {

GpsNavigationMessage() { throw new RuntimeException("Stub!"); }

/**
 * Sets all contents to the values stored in the provided object.
 * @apiSince REL
 */

public void set(android.location.GpsNavigationMessage navigationMessage) { throw new RuntimeException("Stub!"); }

/**
 * Resets all the contents to its original state.
 * @apiSince REL
 */

public void reset() { throw new RuntimeException("Stub!"); }

/**
 * Gets the type of the navigation message contained in the object.
 * @apiSince REL
 */

public byte getType() { throw new RuntimeException("Stub!"); }

/**
 * Sets the type of the navigation message.
 * @apiSince REL
 */

public void setType(byte value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the Pseudo-random number.
 * Range: [1, 32].
 * @apiSince REL
 */

public byte getPrn() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Pseud-random number.
 * @apiSince REL
 */

public void setPrn(byte value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the Message Identifier.
 * It provides an index so the complete Navigation Message can be assembled. i.e. for L1 C/A
 * subframe 4 and 5, this value corresponds to the 'frame id' of the navigation message.
 * Subframe 1, 2, 3 does not contain a 'frame id' and this might be reported as -1.
 * @apiSince REL
 */

public short getMessageId() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Message Identifier.
 * @apiSince REL
 */

public void setMessageId(short value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the Sub-message Identifier.
 * If required by {@link #getType()}, this value contains a sub-index within the current message
 * (or frame) that is being transmitted. i.e. for L1 C/A the sub-message identifier corresponds
 * to the sub-frame Id of the navigation message.
 * @apiSince REL
 */

public short getSubmessageId() { throw new RuntimeException("Stub!"); }

/**
 * Sets the Sub-message identifier.
 * @apiSince REL
 */

public void setSubmessageId(short value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the data associated with the Navigation Message.
 * The bytes (or words) specified using big endian format (MSB first).
 
 * @return This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public byte[] getData() { throw new RuntimeException("Stub!"); }

/**
 * Sets the data associated with the Navigation Message.
 * @apiSince REL
 */

public void setData(byte[] value) { throw new RuntimeException("Stub!"); }

/**
 * Gets the Status of the navigation message contained in the object.
 * @apiSince REL
 */

public short getStatus() { throw new RuntimeException("Stub!"); }

/**
 * Sets the status of the navigation message.
 * @apiSince REL
 */

public void setStatus(short value) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel parcel, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.location.GpsNavigationMessage> CREATOR;
static { CREATOR = null; }

/**
 * The Navigation Message was received without any parity error in its navigation words.
 * @apiSince REL
 */

public static final short STATUS_PARITY_PASSED = 1; // 0x1

/**
 * The Navigation Message was received with words that failed parity check, but the receiver was
 * able to correct those words.
 * @apiSince REL
 */

public static final short STATUS_PARITY_REBUILT = 2; // 0x2

/**
 * The Navigation Message Status is 'unknown'.
 * @apiSince REL
 */

public static final short STATUS_UNKNOWN = 0; // 0x0

/**
 * The Navigation Message is of type CNAV-2.
 * @apiSince REL
 */

public static final byte TYPE_CNAV2 = 4; // 0x4

/**
 * The Navigation Message is of type L1 C/A.
 * @apiSince REL
 */

public static final byte TYPE_L1CA = 1; // 0x1

/**
 * The Navigation Message is of type L1-CNAV.
 * @apiSince REL
 */

public static final byte TYPE_L2CNAV = 2; // 0x2

/**
 * The Navigation Message is of type L5-CNAV.
 * @apiSince REL
 */

public static final byte TYPE_L5CNAV = 3; // 0x3

/**
 * The type of the navigation message is not available or unknown.
 * @apiSince REL
 */

public static final byte TYPE_UNKNOWN = 0; // 0x0
}

