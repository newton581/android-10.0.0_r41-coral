/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.location;


/** A class describing nano apps.
 * A nano app is a piece of executable code that can be
 * downloaded onto a specific architecture. These are targtted
 * for low power compute domains on a device.
 *
 * Nano apps are expected to be used only by bundled apps only
 * at this time.
 *
 * @deprecated Use {@link android.hardware.location.NanoAppBinary} instead to load a nanoapp with
 *             {@link android.hardware.location.ContextHubManager#loadNanoApp(
 *             ContextHubInfo, NanoAppBinary)}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class NanoApp implements android.os.Parcelable {

/**
 * If this version of the constructor is used, the methods
 * {@link #setAppBinary(byte[])} and {@link #setAppId(long)} must be called
 * prior to passing this object to any managers.
 *
 * @see #NanoApp(long, byte[])
 * @apiSince REL
 */

@Deprecated
public NanoApp() { throw new RuntimeException("Stub!"); }

/**
 * Initialize a NanoApp with the given id and binary.
 *
 * While this sets defaults for other fields, users will want to provide
 * other values for those fields in most cases.
 *
 * @see #setPublisher(String)
 * @see #setName(String)
 * @see #setAppVersion(int)
 * @see #setNeededReadMemBytes(int)
 * @see #setNeededWriteMemBytes(int)
 * @see #setNeededExecMemBytes(int)
 * @see #setNeededSensors(int[])
 * @see #setOutputEvents(int[])
 *
 * @deprecated Use NanoApp(long, byte[]) instead
 * @apiSince REL
 */

@Deprecated
public NanoApp(int appId, byte[] appBinary) { throw new RuntimeException("Stub!"); }

/**
 * Initialize a NanoApp with the given id and binary.
 *
 * While this sets defaults for other fields, users will want to provide
 * other values for those fields in most cases.
 *
 * @see #setPublisher(String)
 * @see #setName(String)
 * @see #setAppVersion(int)
 * @see #setNeededReadMemBytes(int)
 * @see #setNeededWriteMemBytes(int)
 * @see #setNeededExecMemBytes(int)
 * @see #setNeededSensors(int[])
 * @see #setOutputEvents(int[])
 * @apiSince REL
 */

@Deprecated
public NanoApp(long appId, byte[] appBinary) { throw new RuntimeException("Stub!"); }

/**
 * Set the publisher name
 *
 * @param publisher name of the publisher of this nano app
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setPublisher(java.lang.String publisher) { throw new RuntimeException("Stub!"); }

/**
 * set the name of the app
 *
 * @param name   name of the app
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * set the app identifier
 *
 * @param appId  app identifier
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setAppId(long appId) { throw new RuntimeException("Stub!"); }

/**
 * Set the app version
 *
 * @param appVersion app version
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setAppVersion(int appVersion) { throw new RuntimeException("Stub!"); }

/**
 * set memory needed as read only
 *
 * @param neededReadMemBytes
 *               read only memory needed in bytes
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setNeededReadMemBytes(int neededReadMemBytes) { throw new RuntimeException("Stub!"); }

/**
 * set writable memory needed in bytes
 *
 * @param neededWriteMemBytes
 *               writable memory needed in bytes
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setNeededWriteMemBytes(int neededWriteMemBytes) { throw new RuntimeException("Stub!"); }

/**
 * set executable memory needed
 *
 * @param neededExecMemBytes
 *               executable memory needed in bytes
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setNeededExecMemBytes(int neededExecMemBytes) { throw new RuntimeException("Stub!"); }

/**
 * set the sensors needed for this app
 *
 * @param neededSensors
 *               needed Sensors
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setNeededSensors(int[] neededSensors) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setOutputEvents(int[] outputEvents) { throw new RuntimeException("Stub!"); }

/**
 * set output events returned by the nano app
 *
 * @param appBinary generated events
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void setAppBinary(byte[] appBinary) { throw new RuntimeException("Stub!"); }

/**
 * get the publisher name
 *
 * @return publisher name
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public java.lang.String getPublisher() { throw new RuntimeException("Stub!"); }

/**
 * get the name of the app
 *
 * @return app name
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public java.lang.String getName() { throw new RuntimeException("Stub!"); }

/**
 * get the identifier of the app
 *
 * @return identifier for this app
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public long getAppId() { throw new RuntimeException("Stub!"); }

/**
 * get the app version
 *
 * @return app version
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getAppVersion() { throw new RuntimeException("Stub!"); }

/**
 * get the ammount of readable memory needed by this app
 *
 * @return readable memory needed in bytes
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getNeededReadMemBytes() { throw new RuntimeException("Stub!"); }

/**
 * get the ammount og writable memory needed in bytes
 *
 * @return writable memory needed in bytes
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getNeededWriteMemBytes() { throw new RuntimeException("Stub!"); }

/**
 * executable memory needed in bytes
 *
 * @return executable memory needed in bytes
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int getNeededExecMemBytes() { throw new RuntimeException("Stub!"); }

/**
 * get the sensors needed by this app
 *
 * @return sensors needed
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int[] getNeededSensors() { throw new RuntimeException("Stub!"); }

/**
 * get the events generated by this app
 *
 * @return generated events
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int[] getOutputEvents() { throw new RuntimeException("Stub!"); }

/**
 * get the binary for this app
 *
 * @return app binary
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public byte[] getAppBinary() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public int describeContents() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated
public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * @apiSince REL
 * @deprecatedSince REL
 */

@Deprecated @android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.location.NanoApp> CREATOR;
static { CREATOR = null; }
}

