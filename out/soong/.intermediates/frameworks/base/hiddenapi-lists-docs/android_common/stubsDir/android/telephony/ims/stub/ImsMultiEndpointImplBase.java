/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.telephony.ims.stub;


/**
 * Base implementation of ImsMultiEndpoint, which implements stub versions of the methods
 * in the IImsMultiEndpoint AIDL. Override the methods that your implementation of
 * ImsMultiEndpoint supports.
 *
 * DO NOT remove or change the existing APIs, only add new ones to this Base implementation or you
 * will break other implementations of ImsMultiEndpoint maintained by other ImsServices.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ImsMultiEndpointImplBase {

public ImsMultiEndpointImplBase() { throw new RuntimeException("Stub!"); }

/**
 * Notifies framework when Dialog Event Package update is received
 *
 * @throws RuntimeException if the connection to the framework is not available.
 * @apiSince REL
 */

public final void onImsExternalCallStateUpdate(java.util.List<android.telephony.ims.ImsExternalCallState> externalCallDialogs) { throw new RuntimeException("Stub!"); }

/**
 * This method should be implemented by the IMS provider. Framework will trigger this to get the
 * latest Dialog Event Package information. Should
 * @apiSince REL
 */

public void requestImsExternalCallStateInfo() { throw new RuntimeException("Stub!"); }
}

