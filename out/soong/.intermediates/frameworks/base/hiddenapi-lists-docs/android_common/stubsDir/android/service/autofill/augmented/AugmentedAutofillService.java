/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill.augmented;

import android.app.Service;
import java.util.List;
import android.view.autofill.AutofillManager;
import java.io.PrintWriter;
import android.content.Intent;

/**
 * A service used to augment the Autofill subsystem by potentially providing autofill data when the
 * "standard" workflow failed (for example, because the standard AutofillService didn't have data).
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class AugmentedAutofillService extends android.app.Service {

public AugmentedAutofillService() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * If you override this method you <em>must</em> call through to the
 * superclass implementation.
 * @apiSince REL
 */

public void onCreate() { throw new RuntimeException("Stub!"); }

/** @hide */

public final android.os.IBinder onBind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean onUnbind(android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * Called when the Android system connects to service.
 *
 * <p>You should generally do initialization here rather than in {@link #onCreate}.
 * @apiSince REL
 */

public void onConnected() { throw new RuntimeException("Stub!"); }

/**
 * Asks the service to handle an "augmented" autofill request.
 *
 * <p>This method is called when the "stantard" autofill service cannot handle a request, which
 * typically occurs when:
 * <ul>
 *   <li>Service does not recognize what should be autofilled.
 *   <li>Service does not have data to fill the request.
 *   <li>Service blacklisted that app (or activity) for autofill.
 *   <li>App disabled itself for autofill.
 * </ul>
 *
 * <p>Differently from the standard autofill workflow, on augmented autofill the service is
 * responsible to generate the autofill UI and request the Android system to autofill the
 * activity when the user taps an action in that UI (through the
 * {@link FillController#autofill(List)} method).
 *
 * <p>The service <b>MUST</b> call {@link
 * FillCallback#onSuccess(android.service.autofill.augmented.FillResponse)} as soon as possible,
 * passing {@code null} when it cannot fulfill the request.
 * @param request the request to handle.
 * This value must never be {@code null}.
 * @param cancellationSignal signal for observing cancellation requests. The system will use
 *     this to notify you that the fill result is no longer needed and you should stop
 *     handling this fill request in order to save resources.
 * This value must never be {@code null}.
 * @param controller object used to interact with the autofill system.
 * This value must never be {@code null}.
 * @param callback object used to notify the result of the request. Service <b>must</b> call
 * {@link FillCallback#onSuccess(android.service.autofill.augmented.FillResponse)}.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onFillRequest(@android.annotation.NonNull android.service.autofill.augmented.FillRequest request, @android.annotation.NonNull android.os.CancellationSignal cancellationSignal, @android.annotation.NonNull android.service.autofill.augmented.FillController controller, @android.annotation.NonNull android.service.autofill.augmented.FillCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Called when the Android system disconnects from the service.
 *
 * <p> At this point this service may no longer be an active {@link AugmentedAutofillService}.
 * It should not make calls on {@link AutofillManager} that requires the caller to be
 * the current service.
 * @apiSince REL
 */

public void onDisconnected() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

protected final void dump(java.io.FileDescriptor fd, java.io.PrintWriter pw, java.lang.String[] args) { throw new RuntimeException("Stub!"); }

/**
 * Implementation specific {@code dump}. The child class can override the method to provide
 * additional information about the Service's state into the dumpsys output.
 *
 * @param pw The PrintWriter to which you should dump your state.  This will be closed for
 * you after you return.
 * This value must never be {@code null}.
 * @param args additional arguments to the dump request.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

protected void dump(@android.annotation.NonNull java.io.PrintWriter pw, @android.annotation.NonNull java.lang.String[] args) { throw new RuntimeException("Stub!"); }

/**
 * The {@link Intent} that must be declared as handled by the service.
 * To be supported, the service must also require the
 * {@link android.Manifest.permission#BIND_AUGMENTED_AUTOFILL_SERVICE} permission so
 * that other applications can not abuse it.
 * @apiSince REL
 */

public static final java.lang.String SERVICE_INTERFACE = "android.service.autofill.augmented.AugmentedAutofillService";
}

