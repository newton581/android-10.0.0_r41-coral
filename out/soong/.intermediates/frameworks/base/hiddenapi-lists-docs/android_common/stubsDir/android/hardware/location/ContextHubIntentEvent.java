/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.location;

import android.content.Intent;
import android.app.PendingIntent;

/**
 * A helper class to retrieve information about a Intent event received for a PendingIntent
 * registered with {@link ContextHubManager.createClient(ContextHubInfo, PendingIntent, long)}.
 * This object can only be created through the factory method
 * {@link ContextHubIntentEvent.fromIntent(Intent)}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ContextHubIntentEvent {

ContextHubIntentEvent(@android.annotation.NonNull android.hardware.location.ContextHubInfo contextHubInfo, int eventType) { throw new RuntimeException("Stub!"); }

/**
 * Creates a ContextHubIntentEvent object from an Intent received through a PendingIntent
 * registered with {@link ContextHubManager.createClient(ContextHubInfo, PendingIntent, long)}.
 *
 * @param intent the Intent object from an Intent event
 * This value must never be {@code null}.
 * @return the ContextHubIntentEvent object describing the event
 *
 * This value will never be {@code null}.
 * @throws IllegalArgumentException if the Intent was not a valid intent
 * @apiSince REL
 */

@android.annotation.NonNull
public static android.hardware.location.ContextHubIntentEvent fromIntent(@android.annotation.NonNull android.content.Intent intent) { throw new RuntimeException("Stub!"); }

/**
 * @return the event type of this Intent event
 
 * Value is {@link android.hardware.location.ContextHubManager#EVENT_NANOAPP_LOADED}, {@link android.hardware.location.ContextHubManager#EVENT_NANOAPP_UNLOADED}, {@link android.hardware.location.ContextHubManager#EVENT_NANOAPP_ENABLED}, {@link android.hardware.location.ContextHubManager#EVENT_NANOAPP_DISABLED}, {@link android.hardware.location.ContextHubManager#EVENT_NANOAPP_ABORTED}, {@link android.hardware.location.ContextHubManager#EVENT_NANOAPP_MESSAGE}, or {@link android.hardware.location.ContextHubManager#EVENT_HUB_RESET}
 * @apiSince REL
 */

public int getEventType() { throw new RuntimeException("Stub!"); }

/**
 * @return the ContextHubInfo object describing the Context Hub this event was for
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.ContextHubInfo getContextHubInfo() { throw new RuntimeException("Stub!"); }

/**
 * @return the ID of the nanoapp this event was for
 *
 * @throws UnsupportedOperationException if the event did not have a nanoapp associated
 * @apiSince REL
 */

public long getNanoAppId() { throw new RuntimeException("Stub!"); }

/**
 * @return the nanoapp's abort code
 *
 * @throws UnsupportedOperationException if this was not a nanoapp abort event
 * @apiSince REL
 */

public int getNanoAppAbortCode() { throw new RuntimeException("Stub!"); }

/**
 * @return the message from a nanoapp
 *
 * This value will never be {@code null}.
 * @throws UnsupportedOperationException if this was not a nanoapp message event
 * @apiSince REL
 */

@android.annotation.NonNull
public android.hardware.location.NanoAppMessage getNanoAppMessage() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param object This value may be {@code null}.
 * @apiSince REL
 */

public boolean equals(@android.annotation.Nullable java.lang.Object object) { throw new RuntimeException("Stub!"); }
}

