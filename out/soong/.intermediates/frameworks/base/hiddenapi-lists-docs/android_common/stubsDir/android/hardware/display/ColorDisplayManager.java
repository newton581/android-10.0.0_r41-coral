/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.hardware.display;

import android.Manifest;
import java.time.LocalTime;

/**
 * Manages the display's color transforms and modes.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ColorDisplayManager {

/**
 * @hide
 */

ColorDisplayManager() { throw new RuntimeException("Stub!"); }

/**
 * Returns the current auto mode value controlling when Night display will be automatically
 * activated. One of {@link #AUTO_MODE_DISABLED}, {@link #AUTO_MODE_CUSTOM_TIME}, or
 * {@link #AUTO_MODE_TWILIGHT}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#CONTROL_DISPLAY_COLOR_TRANSFORMS}
 * @hide

 * @return Value is {@link android.hardware.display.ColorDisplayManager#AUTO_MODE_DISABLED}, {@link android.hardware.display.ColorDisplayManager#AUTO_MODE_CUSTOM_TIME}, or {@link android.hardware.display.ColorDisplayManager#AUTO_MODE_TWILIGHT}
 */

public int getNightDisplayAutoMode() { throw new RuntimeException("Stub!"); }

/**
 * Sets the current auto mode value controlling when Night display will be automatically
 * activated. One of {@link #AUTO_MODE_DISABLED}, {@link #AUTO_MODE_CUSTOM_TIME}, or
 * {@link #AUTO_MODE_TWILIGHT}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#CONTROL_DISPLAY_COLOR_TRANSFORMS}
 * @param autoMode the new auto mode to use
 * Value is {@link android.hardware.display.ColorDisplayManager#AUTO_MODE_DISABLED}, {@link android.hardware.display.ColorDisplayManager#AUTO_MODE_CUSTOM_TIME}, or {@link android.hardware.display.ColorDisplayManager#AUTO_MODE_TWILIGHT}
 * @return {@code true} if new auto mode was set successfully
 *
 * @hide
 */

public boolean setNightDisplayAutoMode(int autoMode) { throw new RuntimeException("Stub!"); }

/**
 * Sets the local time when Night display will be automatically activated when using
 * {@link ColorDisplayManager#AUTO_MODE_CUSTOM_TIME}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#CONTROL_DISPLAY_COLOR_TRANSFORMS}
 * @param startTime the local time to automatically activate Night display
 * This value must never be {@code null}.
 * @return {@code true} if the new custom start time was set successfully
 *
 * @hide
 */

public boolean setNightDisplayCustomStartTime(@android.annotation.NonNull java.time.LocalTime startTime) { throw new RuntimeException("Stub!"); }

/**
 * Sets the local time when Night display will be automatically deactivated when using
 * {@link #AUTO_MODE_CUSTOM_TIME}.
 *
 * <br>
 * Requires {@link android.Manifest.permission#CONTROL_DISPLAY_COLOR_TRANSFORMS}
 * @param endTime the local time to automatically deactivate Night display
 * This value must never be {@code null}.
 * @return {@code true} if the new custom end time was set successfully
 *
 * @hide
 */

public boolean setNightDisplayCustomEndTime(@android.annotation.NonNull java.time.LocalTime endTime) { throw new RuntimeException("Stub!"); }

/**
 * Set the level of color saturation to apply to the display.
 *
 * <br>
 * Requires {@link android.Manifest.permission#CONTROL_DISPLAY_COLOR_TRANSFORMS}
 * @param saturationLevel 0-100 (inclusive), where 100 is full saturation
 * Value is between 0 and 100 inclusive
 * @return whether the saturation level change was applied successfully
 * @hide
 */

public boolean setSaturationLevel(int saturationLevel) { throw new RuntimeException("Stub!"); }

/**
 * Set the level of color saturation to apply to a specific app.
 *
 * <br>
 * Requires {@link android.Manifest.permission#CONTROL_DISPLAY_COLOR_TRANSFORMS}
 * @param packageName the package name of the app whose windows should be desaturated
 * This value must never be {@code null}.
 * @param saturationLevel 0-100 (inclusive), where 100 is full saturation
 * Value is between 0 and 100 inclusive
 * @return whether the saturation level change was applied successfully
 * @hide
 */

public boolean setAppSaturationLevel(@android.annotation.NonNull java.lang.String packageName, int saturationLevel) { throw new RuntimeException("Stub!"); }

/**
 * Returns the available software and hardware color transform capabilities of this device.
 *
 * <br>
 * Requires {@link android.Manifest.permission#CONTROL_DISPLAY_COLOR_TRANSFORMS}
 * @hide

 * @return Value is {@link android.hardware.display.ColorDisplayManager#CAPABILITY_NONE}, {@link android.hardware.display.ColorDisplayManager#CAPABILITY_PROTECTED_CONTENT}, {@link android.hardware.display.ColorDisplayManager#CAPABILITY_HARDWARE_ACCELERATION_GLOBAL}, or {@link android.hardware.display.ColorDisplayManager#CAPABILITY_HARDWARE_ACCELERATION_PER_APP}
 */

public int getTransformCapabilities() { throw new RuntimeException("Stub!"); }

/**
 * Auto mode value to automatically activate Night display at a specific start and end time.
 *
 * @see #setNightDisplayAutoMode(int)
 * @see #setNightDisplayCustomStartTime(LocalTime)
 * @see #setNightDisplayCustomEndTime(LocalTime)
 *
 * @hide
 */

public static final int AUTO_MODE_CUSTOM_TIME = 1; // 0x1

/**
 * Auto mode value to prevent Night display from being automatically activated. It can still
 * be activated manually via {@link #setNightDisplayActivated(boolean)}.
 *
 * @see #setNightDisplayAutoMode(int)
 *
 * @hide
 */

public static final int AUTO_MODE_DISABLED = 0; // 0x0

/**
 * Auto mode value to automatically activate Night display from sunset to sunrise.
 *
 * @see #setNightDisplayAutoMode(int)
 *
 * @hide
 */

public static final int AUTO_MODE_TWILIGHT = 2; // 0x2

/**
 * The device's hardware can efficiently apply transforms to the entire display.
 *
 * @hide
 */

public static final int CAPABILITY_HARDWARE_ACCELERATION_GLOBAL = 2; // 0x2

/**
 * The device's hardware can efficiently apply transforms to a specific Surface (window) so
 * that apps can be transformed independently of one another.
 *
 * @hide
 */

public static final int CAPABILITY_HARDWARE_ACCELERATION_PER_APP = 4; // 0x4

/**
 * The device does not support color transforms.
 *
 * @hide
 */

public static final int CAPABILITY_NONE = 0; // 0x0

/**
 * The device can use GPU composition on protected content (layers whose buffers are protected
 * in the trusted memory zone).
 *
 * @hide
 */

public static final int CAPABILITY_PROTECTED_CONTENT = 1; // 0x1
}

