/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.telephony;

import java.util.List;

/**
 * Description of a mobile network registration info
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class NetworkRegistrationInfo implements android.os.Parcelable {

NetworkRegistrationInfo(android.os.Parcel source) { throw new RuntimeException("Stub!"); }

/**
 * @return The transport type.
 
 * Value is {@link android.telephony.AccessNetworkConstants#TRANSPORT_TYPE_INVALID}, {@link android.telephony.AccessNetworkConstants#TRANSPORT_TYPE_WWAN}, or {@link android.telephony.AccessNetworkConstants#TRANSPORT_TYPE_WLAN}
 * @apiSince REL
 */

public int getTransportType() { throw new RuntimeException("Stub!"); }

/**
 * @return The network domain.
 
 * Value is {@link android.telephony.NetworkRegistrationInfo#DOMAIN_CS}, or {@link android.telephony.NetworkRegistrationInfo#DOMAIN_PS}
 * @apiSince REL
 */

public int getDomain() { throw new RuntimeException("Stub!"); }

/**
 * @return The registration state.
 
 * Value is {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_NOT_REGISTERED_OR_SEARCHING}, {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_HOME}, {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_NOT_REGISTERED_SEARCHING}, {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_DENIED}, {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_UNKNOWN}, or {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_ROAMING}
 * @apiSince REL
 */

public int getRegistrationState() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if registered on roaming network, {@code false} otherwise.
 * @apiSince REL
 */

public boolean isRoaming() { throw new RuntimeException("Stub!"); }

/**
 * @return the current network roaming type.
 
 * Value is {@link android.telephony.ServiceState#ROAMING_TYPE_NOT_ROAMING}, {@link android.telephony.ServiceState#ROAMING_TYPE_UNKNOWN}, {@link android.telephony.ServiceState#ROAMING_TYPE_DOMESTIC}, or {@link android.telephony.ServiceState#ROAMING_TYPE_INTERNATIONAL}
 * @apiSince REL
 */

public int getRoamingType() { throw new RuntimeException("Stub!"); }

/**
 * @return Whether emergency is enabled.
 * @apiSince REL
 */

public boolean isEmergencyEnabled() { throw new RuntimeException("Stub!"); }

/**
 * @return List of available service types.
 
 * This value will never be {@code null}.
 
 * Value is {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_UNKNOWN}, {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_VOICE}, {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_DATA}, {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_SMS}, {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_VIDEO}, or {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_EMERGENCY}
 * @apiSince REL
 */

@android.annotation.NonNull
public java.util.List<java.lang.Integer> getAvailableServices() { throw new RuntimeException("Stub!"); }

/**
 * @return The access network technology {@link NetworkType}.
 
 * Value is {@link android.telephony.TelephonyManager#NETWORK_TYPE_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IDEN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IWLAN}, android.telephony.TelephonyManager.NETWORK_TYPE_LTE_CA, or {@link android.telephony.TelephonyManager#NETWORK_TYPE_NR}
 * @apiSince REL
 */

public int getAccessNetworkTechnology() { throw new RuntimeException("Stub!"); }

/**
 * @return Reason for denial if the registration state is {@link #REGISTRATION_STATE_DENIED}.
 * Depending on {@code accessNetworkTechnology}, the values are defined in 3GPP TS 24.008
 * 10.5.3.6 for UMTS, 3GPP TS 24.301 9.9.3.9 for LTE, and 3GPP2 A.S0001 6.2.2.44 for CDMA
 * @apiSince REL
 */

public int getRejectCause() { throw new RuntimeException("Stub!"); }

/**
 * @return The cell information.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.telephony.CellIdentity getCellIdentity() { throw new RuntimeException("Stub!"); }

/**
 * @return Data registration related info
 
 * This value may be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public android.telephony.DataSpecificRegistrationInfo getDataSpecificInfo() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int hashCode() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel dest, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.telephony.NetworkRegistrationInfo> CREATOR;
static { CREATOR = null; }

/**
 * Circuit switching domain
 * @apiSince REL
 */

public static final int DOMAIN_CS = 1; // 0x1

/**
 * Packet switching domain
 * @apiSince REL
 */

public static final int DOMAIN_PS = 2; // 0x2

/**
 * Registration denied.
 * @apiSince REL
 */

public static final int REGISTRATION_STATE_DENIED = 3; // 0x3

/**
 * Registered on home network.
 * @apiSince REL
 */

public static final int REGISTRATION_STATE_HOME = 1; // 0x1

/**
 * Not registered. The device is not currently searching a new operator to register.
 * @apiSince REL
 */

public static final int REGISTRATION_STATE_NOT_REGISTERED_OR_SEARCHING = 0; // 0x0

/**
 * Not registered. The device is currently searching a new operator to register.
 * @apiSince REL
 */

public static final int REGISTRATION_STATE_NOT_REGISTERED_SEARCHING = 2; // 0x2

/**
 * Registered on roaming network.
 * @apiSince REL
 */

public static final int REGISTRATION_STATE_ROAMING = 5; // 0x5

/**
 * Registration state is unknown.
 * @apiSince REL
 */

public static final int REGISTRATION_STATE_UNKNOWN = 4; // 0x4

/**
 * Data service
 * @apiSince REL
 */

public static final int SERVICE_TYPE_DATA = 2; // 0x2

/**
 * Emergency service
 * @apiSince REL
 */

public static final int SERVICE_TYPE_EMERGENCY = 5; // 0x5

/**
 * SMS service
 * @apiSince REL
 */

public static final int SERVICE_TYPE_SMS = 3; // 0x3

/**
 * Unkown service
 * @apiSince REL
 */

public static final int SERVICE_TYPE_UNKNOWN = 0; // 0x0

/**
 * Video service
 * @apiSince REL
 */

public static final int SERVICE_TYPE_VIDEO = 4; // 0x4

/**
 * Voice service
 * @apiSince REL
 */

public static final int SERVICE_TYPE_VOICE = 1; // 0x1
/**
 * Provides a convenient way to set the fields of a {@link NetworkRegistrationInfo} when
 * creating a new instance.
 *
 * <p>The example below shows how you might create a new {@code NetworkRegistrationInfo}:
 *
 * <pre><code>
 *
 * NetworkRegistrationInfo nri = new NetworkRegistrationInfo.Builder()
 *     .setAccessNetworkTechnology(TelephonyManager.NETWORK_TYPE_LTE)
 *     .setRegistrationState(REGISTRATION_STATE_HOME)
 *     .build();
 * </code></pre>
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Default constructor for Builder.
 * @apiSince REL
 */

public Builder() { throw new RuntimeException("Stub!"); }

/**
 * Set the network domain.
 *
 * @param domain Network domain.
 *
 * Value is {@link android.telephony.NetworkRegistrationInfo#DOMAIN_CS}, or {@link android.telephony.NetworkRegistrationInfo#DOMAIN_PS}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.NetworkRegistrationInfo.Builder setDomain(int domain) { throw new RuntimeException("Stub!"); }

/**
 * Set the transport type.
 *
 * @param transportType Transport type.
 *
 * Value is {@link android.telephony.AccessNetworkConstants#TRANSPORT_TYPE_INVALID}, {@link android.telephony.AccessNetworkConstants#TRANSPORT_TYPE_WWAN}, or {@link android.telephony.AccessNetworkConstants#TRANSPORT_TYPE_WLAN}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.NetworkRegistrationInfo.Builder setTransportType(int transportType) { throw new RuntimeException("Stub!"); }

/**
 * Set the registration state.
 *
 * @param registrationState The registration state.
 *
 * Value is {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_NOT_REGISTERED_OR_SEARCHING}, {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_HOME}, {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_NOT_REGISTERED_SEARCHING}, {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_DENIED}, {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_UNKNOWN}, or {@link android.telephony.NetworkRegistrationInfo#REGISTRATION_STATE_ROAMING}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.NetworkRegistrationInfo.Builder setRegistrationState(int registrationState) { throw new RuntimeException("Stub!"); }

/**
 * Set tne access network technology.
 *
 * @return The same instance of the builder.
 *
 * This value will never be {@code null}.
 * @param accessNetworkTechnology The access network technology
 
 * Value is {@link android.telephony.TelephonyManager#NETWORK_TYPE_UNKNOWN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GPRS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EDGE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_UMTS}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_CDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_0}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_A}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_1xRTT}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSDPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSUPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IDEN}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EVDO_B}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_LTE}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_EHRPD}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_HSPAP}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_GSM}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_TD_SCDMA}, {@link android.telephony.TelephonyManager#NETWORK_TYPE_IWLAN}, android.telephony.TelephonyManager.NETWORK_TYPE_LTE_CA, or {@link android.telephony.TelephonyManager#NETWORK_TYPE_NR}
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.NetworkRegistrationInfo.Builder setAccessNetworkTechnology(int accessNetworkTechnology) { throw new RuntimeException("Stub!"); }

/**
 * Set the network reject cause.
 *
 * @param rejectCause Reason for denial if the registration state is
 * {@link #REGISTRATION_STATE_DENIED}.Depending on {@code accessNetworkTechnology}, the
 * values are defined in 3GPP TS 24.008 10.5.3.6 for UMTS, 3GPP TS 24.301 9.9.3.9 for LTE,
 * and 3GPP2 A.S0001 6.2.2.44 for CDMA. If the reject cause is not supported or unknown, set
 * it to 0.
 *
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.NetworkRegistrationInfo.Builder setRejectCause(int rejectCause) { throw new RuntimeException("Stub!"); }

/**
 * Set emergency only.
 *
 * @param emergencyOnly True if this network registration is for emergency use only.
 *
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.NetworkRegistrationInfo.Builder setEmergencyOnly(boolean emergencyOnly) { throw new RuntimeException("Stub!"); }

/**
 * Set the available services.
 *
 * @param availableServices Available services.
 *
 * This value must never be {@code null}.
 * Value is {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_UNKNOWN}, {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_VOICE}, {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_DATA}, {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_SMS}, {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_VIDEO}, or {@link android.telephony.NetworkRegistrationInfo#SERVICE_TYPE_EMERGENCY}
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.NetworkRegistrationInfo.Builder setAvailableServices(@android.annotation.NonNull java.util.List<java.lang.Integer> availableServices) { throw new RuntimeException("Stub!"); }

/**
 * Set the cell identity.
 *
 * @param cellIdentity The cell identity.
 *
 * This value may be {@code null}.
 * @return The same instance of the builder.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.NetworkRegistrationInfo.Builder setCellIdentity(@android.annotation.Nullable android.telephony.CellIdentity cellIdentity) { throw new RuntimeException("Stub!"); }

/**
 * Build the NetworkRegistrationInfo.
 *
 * @return the NetworkRegistrationInfo object.
 
 * This value will never be {@code null}.
 * @apiSince REL
 */

@android.annotation.NonNull
public android.telephony.NetworkRegistrationInfo build() { throw new RuntimeException("Stub!"); }
}

}

