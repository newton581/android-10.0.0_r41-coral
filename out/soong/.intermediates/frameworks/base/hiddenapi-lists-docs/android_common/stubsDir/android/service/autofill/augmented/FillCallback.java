/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.service.autofill.augmented;


/**
 * Callback used to indicate at {@link FillRequest} has been fulfilled.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class FillCallback {

FillCallback() { throw new RuntimeException("Stub!"); }

/**
 * Sets the response associated with the request.
 *
 * @param response response associated with the request, or {@code null} if the service
 * could not provide autofill for the request.
 
 * This value may be {@code null}.
 * @apiSince REL
 */

public void onSuccess(@android.annotation.Nullable android.service.autofill.augmented.FillResponse response) { throw new RuntimeException("Stub!"); }
}

