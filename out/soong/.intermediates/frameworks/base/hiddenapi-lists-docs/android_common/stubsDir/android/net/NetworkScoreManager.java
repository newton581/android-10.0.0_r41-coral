/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package android.net;

import android.Manifest.permission;

/**
 * Class that manages communication between network subsystems and a network scorer.
 *
 * <p>A network scorer is any application which:
 * <ul>
 * <li>Is granted the {@link permission#SCORE_NETWORKS} permission.
 * <li>Is granted the {@link permission#ACCESS_COARSE_LOCATION} permission.
 * <li>Include a Service for the {@link #ACTION_RECOMMEND_NETWORKS} action
 *     protected by the {@link permission#BIND_NETWORK_RECOMMENDATION_SERVICE}
 *     permission.
 * </ul>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class NetworkScoreManager {

NetworkScoreManager() { throw new RuntimeException("Stub!"); }

/**
 * Obtain the package name of the current active network scorer.
 *
 * <p>At any time, only one scorer application will receive {@link #ACTION_SCORE_NETWORKS}
 * broadcasts and be allowed to call {@link #updateScores}. Applications may use this method to
 * determine the current scorer and offer the user the ability to select a different scorer via
 * the {@link #ACTION_CHANGE_ACTIVE} intent.
 * <br>
 * Requires {@link android.Manifest.permission#SCORE_NETWORKS} or android.Manifest.permission.REQUEST_NETWORK_SCORES
 * @return the full package name of the current active scorer, or null if there is no active
 *         scorer.
 * @throws SecurityException if the caller doesn't hold either {@link permission#SCORE_NETWORKS}
 *                           or {@link permission#REQUEST_NETWORK_SCORES} permissions.
 * @apiSince REL
 */

public java.lang.String getActiveScorerPackage() { throw new RuntimeException("Stub!"); }

/**
 * Update network scores.
 *
 * <p>This may be called at any time to re-score active networks. Scores will generally be
 * updated quickly, but if this method is called too frequently, the scores may be held and
 * applied at a later time.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SCORE_NETWORKS}
 * @param networks the networks which have been scored by the scorer.
 * @return whether the update was successful.
 * @throws SecurityException if the caller is not the active scorer.
 * @apiSince REL
 */

public boolean updateScores(android.net.ScoredNetwork[] networks) throws java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/**
 * Clear network scores.
 *
 * <p>Should be called when all scores need to be invalidated, i.e. because the scoring
 * algorithm has changed and old scores can no longer be compared to future scores.
 *
 * <p>Note that scores will be cleared automatically when the active scorer changes, as scores
 * from one scorer cannot be compared to those from another scorer.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SCORE_NETWORKS} or android.Manifest.permission.REQUEST_NETWORK_SCORES
 * @return whether the clear was successful.
 * @throws SecurityException if the caller is not the active scorer or if the caller doesn't
 *                           hold the {@link permission#REQUEST_NETWORK_SCORES} permission.
 * @apiSince REL
 */

public boolean clearScores() throws java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/**
 * Set the active scorer to a new package and clear existing scores.
 *
 * <p>Should never be called directly without obtaining user consent. This can be done by using
 * the {@link #ACTION_CHANGE_ACTIVE} broadcast, or using a custom configuration activity.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SCORE_NETWORKS} or android.Manifest.permission.REQUEST_NETWORK_SCORES
 * @return true if the operation succeeded, or false if the new package is not a valid scorer.
 * @throws SecurityException if the caller doesn't hold either {@link permission#SCORE_NETWORKS}
 *                           or {@link permission#REQUEST_NETWORK_SCORES} permissions.
 * @hide
 */

public boolean setActiveScorer(java.lang.String packageName) throws java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/**
 * Turn off network scoring.
 *
 * <p>May only be called by the current scorer app, or the system.
 *
 * <br>
 * Requires {@link android.Manifest.permission#SCORE_NETWORKS} or android.Manifest.permission.REQUEST_NETWORK_SCORES
 * @throws SecurityException if the caller is not the active scorer or if the caller doesn't
 *                           hold the {@link permission#REQUEST_NETWORK_SCORES} permission.
 * @apiSince REL
 */

public void disableScoring() throws java.lang.SecurityException { throw new RuntimeException("Stub!"); }

/**
 * Activity action: ask the user to change the active network scorer. This will show a dialog
 * that asks the user whether they want to replace the current active scorer with the one
 * specified in {@link #EXTRA_PACKAGE_NAME}. The activity will finish with RESULT_OK if the
 * active scorer was changed or RESULT_CANCELED if it failed for any reason.
 * @apiSince REL
 */

public static final java.lang.String ACTION_CHANGE_ACTIVE = "android.net.scoring.CHANGE_ACTIVE";

/**
 * Activity action: launch an activity for configuring a provider for the feature that connects
 * and secures open wifi networks available before enabling it. Applications that enable this
 * feature must provide an activity for this action. The framework will launch this activity
 * which must return RESULT_OK if the feature should be enabled.
 * @apiSince REL
 */

public static final java.lang.String ACTION_CUSTOM_ENABLE = "android.net.scoring.CUSTOM_ENABLE";

/**
 * Service action: Used to discover and bind to a network recommendation provider.
 * Implementations should return {@link NetworkRecommendationProvider#getBinder()} from
 * their <code>onBind()</code> method.
 * @apiSince REL
 */

public static final java.lang.String ACTION_RECOMMEND_NETWORKS = "android.net.action.RECOMMEND_NETWORKS";

/**
 * Broadcast action: the active scorer has been changed. Scorer apps may listen to this to
 * perform initialization once selected as the active scorer, or clean up unneeded resources
 * if another scorer has been selected. This is an explicit broadcast only sent to the
 * previous scorer and new scorer. Note that it is unnecessary to clear existing scores as
 * this is handled by the system.
 *
 * <p>The new scorer will be specified in {@link #EXTRA_NEW_SCORER}.
 *
 * <p class="note">This is a protected intent that can only be sent by the system.
 * @apiSince REL
 */

public static final java.lang.String ACTION_SCORER_CHANGED = "android.net.scoring.SCORER_CHANGED";

/**
 * Broadcast action: new network scores are being requested. This intent will only be delivered
 * to the current active scorer app. That app is responsible for scoring the networks and
 * calling {@link #updateScores} when complete. The networks to score are specified in
 * {@link #EXTRA_NETWORKS_TO_SCORE}, and will generally consist of all networks which have been
 * configured by the user as well as any open networks.
 *
 * <p class="note">This is a protected intent that can only be sent by the system.
 * @apiSince REL
 */

public static final java.lang.String ACTION_SCORE_NETWORKS = "android.net.scoring.SCORE_NETWORKS";

/**
 * Extra used with {@link #ACTION_SCORE_NETWORKS} to specify the networks to be scored, as an
 * array of {@link NetworkKey}s. Can be obtained with
 * {@link android.content.Intent#getParcelableArrayExtra(String)}}.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_NETWORKS_TO_SCORE = "networksToScore";

/**
 * Extra used with {@link #ACTION_SCORER_CHANGED} to specify the newly selected scorer's package
 * name. Will be null if scoring was disabled. Can be obtained with
 * {@link android.content.Intent#getStringExtra(String)}.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_NEW_SCORER = "newScorer";

/**
 * Extra used with {@link #ACTION_CHANGE_ACTIVE} to specify the new scorer package. Set with
 * {@link android.content.Intent#putExtra(String, String)}.
 * @apiSince REL
 */

public static final java.lang.String EXTRA_PACKAGE_NAME = "packageName";
}

