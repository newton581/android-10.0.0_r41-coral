/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.hardware.location;


/**
 * A class describing the nanoapp state information resulting from a query to a Context Hub.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class NanoAppState implements android.os.Parcelable {

/** @apiSince REL */

public NanoAppState(long nanoAppId, int appVersion, boolean enabled) { throw new RuntimeException("Stub!"); }

/**
 * @return the NanoAppInfo for this app
 * @apiSince REL
 */

public long getNanoAppId() { throw new RuntimeException("Stub!"); }

/**
 * @return the app version
 * @apiSince REL
 */

public long getNanoAppVersion() { throw new RuntimeException("Stub!"); }

/**
 * @return {@code true} if the app is enabled at the Context Hub, {@code false} otherwise
 * @apiSince REL
 */

public boolean isEnabled() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public int describeContents() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

public void writeToParcel(android.os.Parcel out, int flags) { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

@android.annotation.NonNull public static final android.os.Parcelable.Creator<android.hardware.location.NanoAppState> CREATOR;
static { CREATOR = null; }
}

