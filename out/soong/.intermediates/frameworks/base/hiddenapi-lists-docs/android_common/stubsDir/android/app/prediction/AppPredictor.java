/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.app.prediction;

import android.content.Context;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * Class that represents an App Prediction client.
 *
 * <p>
 * Usage: <pre> {@code
 *
 * class MyActivity {
 *    private AppPredictor mClient
 *
 *    void onCreate() {
 *         mClient = new AppPredictor(...)
 *         mClient.registerPredictionUpdates(...)
 *    }
 *
 *    void onStart() {
 *        mClient.requestPredictionUpdate()
 *    }
 *
 *    void onClick(...) {
 *        mClient.notifyAppTargetEvent(...)
 *    }
 *
 *    void onDestroy() {
 *        mClient.unregisterPredictionUpdates()
 *        mClient.close()
 *    }
 *
 * }</pre>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class AppPredictor {

/**
 * Creates a new Prediction client.
 * <p>
 * The caller should call {@link AppPredictor#destroy()} to dispose the client once it
 * no longer used.
 *
 * @param context The {@link Context} of the user of this {@link AppPredictor}.
 * @param predictionContext The prediction context.
 */

AppPredictor(@android.annotation.NonNull android.content.Context context, @android.annotation.NonNull android.app.prediction.AppPredictionContext predictionContext) { throw new RuntimeException("Stub!"); }

/**
 * Notifies the prediction service of an app target event.
 *
 * @param event The {@link AppTargetEvent} that represents the app target event.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void notifyAppTargetEvent(@android.annotation.NonNull android.app.prediction.AppTargetEvent event) { throw new RuntimeException("Stub!"); }

/**
 * Notifies the prediction service when the targets in a launch location are shown to the user.
 *
 * @param launchLocation The launch location where the targets are shown to the user.
 * This value must never be {@code null}.
 * @param targetIds List of {@link AppTargetId}s that are shown to the user.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void notifyLaunchLocationShown(@android.annotation.NonNull java.lang.String launchLocation, @android.annotation.NonNull java.util.List<android.app.prediction.AppTargetId> targetIds) { throw new RuntimeException("Stub!"); }

/**
 * Requests the prediction service provide continuous updates of App predictions via the
 * provided callback, until the given callback is unregistered.
 *
 * @see Callback#onTargetsAvailable(List).
 *
 * @param callbackExecutor The callback executor to use when calling the callback.
 * This value must never be {@code null}.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The Callback to be called when updates of App predictions are available.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void registerPredictionUpdates(@android.annotation.NonNull java.util.concurrent.Executor callbackExecutor, @android.annotation.NonNull android.app.prediction.AppPredictor.Callback callback) { throw new RuntimeException("Stub!"); }

/**
 * Requests the prediction service to stop providing continuous updates to the provided
 * callback until the callback is re-registered.
 *
 * @see {@link AppPredictor#registerPredictionUpdates(Executor, Callback)}.
 *
 * @param callback The callback to be unregistered.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void unregisterPredictionUpdates(@android.annotation.NonNull android.app.prediction.AppPredictor.Callback callback) { throw new RuntimeException("Stub!"); }

/**
 * Requests the prediction service to dispatch a new set of App predictions via the provided
 * callback.
 *
 * @see Callback#onTargetsAvailable(List).
 * @apiSince REL
 */

public void requestPredictionUpdate() { throw new RuntimeException("Stub!"); }

/**
 * Returns a new list of AppTargets sorted based on prediction rank or {@code null} if the
 * ranker is not available.
 *
 * @param targets List of app targets to be sorted.
 * This value must never be {@code null}.
 * @param callbackExecutor The callback executor to use when calling the callback.
 * This value must never be {@code null}.
 * @param callback The callback to return the sorted list of app targets.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

@android.annotation.Nullable
public void sortTargets(@android.annotation.NonNull java.util.List<android.app.prediction.AppTarget> targets, @android.annotation.NonNull java.util.concurrent.Executor callbackExecutor, @android.annotation.NonNull java.util.function.Consumer<java.util.List<android.app.prediction.AppTarget>> callback) { throw new RuntimeException("Stub!"); }

/**
 * Destroys the client and unregisters the callback. Any method on this class after this call
 * with throw {@link IllegalStateException}.
 * @apiSince REL
 */

public void destroy() { throw new RuntimeException("Stub!"); }

/** @apiSince REL */

protected void finalize() throws java.lang.Throwable { throw new RuntimeException("Stub!"); }

/**
 * Returns the id of this prediction session.
 *
 * @hide
 */

public android.app.prediction.AppPredictionSessionId getSessionId() { throw new RuntimeException("Stub!"); }
/**
 * Callback for receiving prediction updates.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface Callback {

/**
 * Called when a new set of predicted app targets are available.
 * @param targets Sorted list of predicted targets.
 
 * This value must never be {@code null}.
 * @apiSince REL
 */

public void onTargetsAvailable(@android.annotation.NonNull java.util.List<android.app.prediction.AppTarget> targets);
}

}

