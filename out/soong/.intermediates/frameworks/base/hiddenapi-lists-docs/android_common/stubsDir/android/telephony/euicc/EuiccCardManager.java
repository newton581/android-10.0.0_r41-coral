/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.telephony.euicc;

import android.content.Context;
import java.util.concurrent.Executor;
import android.service.euicc.EuiccProfileInfo;

/**
 * EuiccCardManager is the application interface to an eSIM card.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class EuiccCardManager {

/** @hide */

EuiccCardManager(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Requests all the profiles on eUicc.
 *
 * @param cardId The Id of the eUICC.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to get the result code and all the profiles.
 * @apiSince REL
 */

public void requestAllProfiles(java.lang.String cardId, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<android.service.euicc.EuiccProfileInfo[]> callback) { throw new RuntimeException("Stub!"); }

/**
 * Requests the profile of the given iccid.
 *
 * @param cardId The Id of the eUICC.
 * @param iccid The iccid of the profile.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to get the result code and profile.
 * @apiSince REL
 */

public void requestProfile(java.lang.String cardId, java.lang.String iccid, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<android.service.euicc.EuiccProfileInfo> callback) { throw new RuntimeException("Stub!"); }

/**
 * Disables the profile of the given iccid.
 *
 * @param cardId The Id of the eUICC.
 * @param iccid The iccid of the profile.
 * @param refresh Whether sending the REFRESH command to modem.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to get the result code.
 * @apiSince REL
 */

public void disableProfile(java.lang.String cardId, java.lang.String iccid, boolean refresh, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<java.lang.Void> callback) { throw new RuntimeException("Stub!"); }

/**
 * Switches from the current profile to another profile. The current profile will be disabled
 * and the specified profile will be enabled.
 *
 * @param cardId The Id of the eUICC.
 * @param iccid The iccid of the profile to switch to.
 * @param refresh Whether sending the REFRESH command to modem.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to get the result code and the EuiccProfileInfo enabled.
 * @apiSince REL
 */

public void switchToProfile(java.lang.String cardId, java.lang.String iccid, boolean refresh, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<android.service.euicc.EuiccProfileInfo> callback) { throw new RuntimeException("Stub!"); }

/**
 * Sets the nickname of the profile of the given iccid.
 *
 * @param cardId The Id of the eUICC.
 * @param iccid The iccid of the profile.
 * @param nickname The nickname of the profile.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to get the result code.
 * @apiSince REL
 */

public void setNickname(java.lang.String cardId, java.lang.String iccid, java.lang.String nickname, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<java.lang.Void> callback) { throw new RuntimeException("Stub!"); }

/**
 * Deletes the profile of the given iccid from eUICC.
 *
 * @param cardId The Id of the eUICC.
 * @param iccid The iccid of the profile.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to get the result code.
 * @apiSince REL
 */

public void deleteProfile(java.lang.String cardId, java.lang.String iccid, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<java.lang.Void> callback) { throw new RuntimeException("Stub!"); }

/**
 * Resets the eUICC memory.
 *
 * @param cardId The Id of the eUICC.
 * @param options Bits of the options of resetting which parts of the eUICC memory. See
 *     EuiccCard for details.
 * Value is either <code>0</code> or a combination of {@link android.telephony.euicc.EuiccCardManager#RESET_OPTION_DELETE_OPERATIONAL_PROFILES}, {@link android.telephony.euicc.EuiccCardManager#RESET_OPTION_DELETE_FIELD_LOADED_TEST_PROFILES}, and {@link android.telephony.euicc.EuiccCardManager#RESET_OPTION_RESET_DEFAULT_SMDP_ADDRESS}
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to get the result code.
 * @apiSince REL
 */

public void resetMemory(java.lang.String cardId, int options, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<java.lang.Void> callback) { throw new RuntimeException("Stub!"); }

/**
 * Requests the default SM-DP+ address from eUICC.
 *
 * @param cardId The Id of the eUICC.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to get the result code and the default SM-DP+ address.
 * @apiSince REL
 */

public void requestDefaultSmdpAddress(java.lang.String cardId, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<java.lang.String> callback) { throw new RuntimeException("Stub!"); }

/**
 * Requests the SM-DS address from eUICC.
 *
 * @param cardId The Id of the eUICC.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to get the result code and the SM-DS address.
 * @apiSince REL
 */

public void requestSmdsAddress(java.lang.String cardId, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<java.lang.String> callback) { throw new RuntimeException("Stub!"); }

/**
 * Sets the default SM-DP+ address of eUICC.
 *
 * @param cardId The Id of the eUICC.
 * @param defaultSmdpAddress The default SM-DP+ address to set.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback The callback to get the result code.
 * @apiSince REL
 */

public void setDefaultSmdpAddress(java.lang.String cardId, java.lang.String defaultSmdpAddress, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<java.lang.Void> callback) { throw new RuntimeException("Stub!"); }

/**
 * Requests Rules Authorisation Table.
 *
 * @param cardId The Id of the eUICC.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and the rule authorisation table.
 * @apiSince REL
 */

public void requestRulesAuthTable(java.lang.String cardId, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<android.telephony.euicc.EuiccRulesAuthTable> callback) { throw new RuntimeException("Stub!"); }

/**
 * Requests the eUICC challenge for new profile downloading.
 *
 * @param cardId The Id of the eUICC.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and the challenge.
 * @apiSince REL
 */

public void requestEuiccChallenge(java.lang.String cardId, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<byte[]> callback) { throw new RuntimeException("Stub!"); }

/**
 * Requests the eUICC info1 defined in GSMA RSP v2.0+ for new profile downloading.
 *
 * @param cardId The Id of the eUICC.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and the info1.
 * @apiSince REL
 */

public void requestEuiccInfo1(java.lang.String cardId, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<byte[]> callback) { throw new RuntimeException("Stub!"); }

/**
 * Gets the eUICC info2 defined in GSMA RSP v2.0+ for new profile downloading.
 *
 * @param cardId The Id of the eUICC.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and the info2.
 * @apiSince REL
 */

public void requestEuiccInfo2(java.lang.String cardId, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<byte[]> callback) { throw new RuntimeException("Stub!"); }

/**
 * Authenticates the SM-DP+ server by the eUICC.
 *
 * @param cardId The Id of the eUICC.
 * @param matchingId the activation code token defined in GSMA RSP v2.0+ or empty when it is not
 *     required.
 * @param serverSigned1 ASN.1 data in byte array signed and returned by the SM-DP+ server.
 * @param serverSignature1 ASN.1 data in byte array indicating a SM-DP+ signature which is
 *     returned by SM-DP+ server.
 * @param euiccCiPkIdToBeUsed ASN.1 data in byte array indicating CI Public Key Identifier to be
 *     used by the eUICC for signature which is returned by SM-DP+ server. This is defined in
 *     GSMA RSP v2.0+.
 * @param serverCertificate ASN.1 data in byte array indicating SM-DP+ Certificate returned by
 *     SM-DP+ server.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and a byte array which represents a
 *     {@code AuthenticateServerResponse} defined in GSMA RSP v2.0+.
 * @apiSince REL
 */

public void authenticateServer(java.lang.String cardId, java.lang.String matchingId, byte[] serverSigned1, byte[] serverSignature1, byte[] euiccCiPkIdToBeUsed, byte[] serverCertificate, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<byte[]> callback) { throw new RuntimeException("Stub!"); }

/**
 * Prepares the profile download request sent to SM-DP+.
 *
 * @param cardId The Id of the eUICC.
 * @param hashCc the hash of confirmation code. It can be null if there is no confirmation code
 *     required.
 * This value may be {@code null}.
 * @param smdpSigned2 ASN.1 data in byte array indicating the data to be signed by the SM-DP+
 *     returned by SM-DP+ server.
 * @param smdpSignature2 ASN.1 data in byte array indicating the SM-DP+ signature returned by
 *     SM-DP+ server.
 * @param smdpCertificate ASN.1 data in byte array indicating the SM-DP+ Certificate returned
 *     by SM-DP+ server.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and a byte array which represents a
 *     {@code PrepareDownloadResponse} defined in GSMA RSP v2.0+
 * @apiSince REL
 */

public void prepareDownload(java.lang.String cardId, @android.annotation.Nullable byte[] hashCc, byte[] smdpSigned2, byte[] smdpSignature2, byte[] smdpCertificate, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<byte[]> callback) { throw new RuntimeException("Stub!"); }

/**
 * Loads a downloaded bound profile package onto the eUICC.
 *
 * @param cardId The Id of the eUICC.
 * @param boundProfilePackage the Bound Profile Package data returned by SM-DP+ server.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and a byte array which represents a
 *     {@code LoadBoundProfilePackageResponse} defined in GSMA RSP v2.0+.
 * @apiSince REL
 */

public void loadBoundProfilePackage(java.lang.String cardId, byte[] boundProfilePackage, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<byte[]> callback) { throw new RuntimeException("Stub!"); }

/**
 * Cancels the current profile download session.
 *
 * @param cardId The Id of the eUICC.
 * @param transactionId the transaction ID returned by SM-DP+ server.
 * @param reason the cancel reason.
 * Value is {@link android.telephony.euicc.EuiccCardManager#CANCEL_REASON_END_USER_REJECTED}, {@link android.telephony.euicc.EuiccCardManager#CANCEL_REASON_POSTPONED}, {@link android.telephony.euicc.EuiccCardManager#CANCEL_REASON_TIMEOUT}, or {@link android.telephony.euicc.EuiccCardManager#CANCEL_REASON_PPR_NOT_ALLOWED}
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and an byte[] which represents a
 *     {@code CancelSessionResponse} defined in GSMA RSP v2.0+.
 * @apiSince REL
 */

public void cancelSession(java.lang.String cardId, byte[] transactionId, int reason, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<byte[]> callback) { throw new RuntimeException("Stub!"); }

/**
 * Lists all notifications of the given {@code events}.
 *
 * @param cardId The Id of the eUICC.
 * @param events bits of the event types ({@link EuiccNotification.Event}) to list.
 * Value is either <code>0</code> or a combination of {@link android.telephony.euicc.EuiccNotification#EVENT_INSTALL}, {@link android.telephony.euicc.EuiccNotification#EVENT_ENABLE}, {@link android.telephony.euicc.EuiccNotification#EVENT_DISABLE}, and {@link android.telephony.euicc.EuiccNotification#EVENT_DELETE}
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and the list of notifications.
 * @apiSince REL
 */

public void listNotifications(java.lang.String cardId, int events, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<android.telephony.euicc.EuiccNotification[]> callback) { throw new RuntimeException("Stub!"); }

/**
 * Retrieves contents of all notification of the given {@code events}.
 *
 * @param cardId The Id of the eUICC.
 * @param events bits of the event types ({@link EuiccNotification.Event}) to list.
 * Value is either <code>0</code> or a combination of {@link android.telephony.euicc.EuiccNotification#EVENT_INSTALL}, {@link android.telephony.euicc.EuiccNotification#EVENT_ENABLE}, {@link android.telephony.euicc.EuiccNotification#EVENT_DISABLE}, and {@link android.telephony.euicc.EuiccNotification#EVENT_DELETE}
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and the list of notifications.
 * @apiSince REL
 */

public void retrieveNotificationList(java.lang.String cardId, int events, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<android.telephony.euicc.EuiccNotification[]> callback) { throw new RuntimeException("Stub!"); }

/**
 * Retrieves the content of a notification of the given {@code seqNumber}.
 *
 * @param cardId The Id of the eUICC.
 * @param seqNumber the sequence number of the notification.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code and the notification.
 * @apiSince REL
 */

public void retrieveNotification(java.lang.String cardId, int seqNumber, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<android.telephony.euicc.EuiccNotification> callback) { throw new RuntimeException("Stub!"); }

/**
 * Removes a notification from eUICC.
 *
 * @param cardId The Id of the eUICC.
 * @param seqNumber the sequence number of the notification.
 * @param executor The executor through which the callback should be invoked.
 * Callback and listener events are dispatched through this
 * {@link java.util.concurrent.Executor Executor}, providing an easy way to control which thread is
 * used. To dispatch events through the main thread of your
 * application, you can use {@link android.content.Context#getMainExecutor() Context#getMainExecutor()}. To
 * dispatch events through a shared thread pool, you can use
 * {@link android.os.AsyncTask#THREAD_POOL_EXECUTOR AsyncTask#THREAD_POOL_EXECUTOR}.
 * @param callback the callback to get the result code.
 * @apiSince REL
 */

public void removeNotificationFromList(java.lang.String cardId, int seqNumber, java.util.concurrent.Executor executor, android.telephony.euicc.EuiccCardManager.ResultCallback<java.lang.Void> callback) { throw new RuntimeException("Stub!"); }

/**
 * The end user has rejected the download. The profile will be put into the error state and
 * cannot be downloaded again without the operator's change.
 * @apiSince REL
 */

public static final int CANCEL_REASON_END_USER_REJECTED = 0; // 0x0

/**
 * The download has been postponed and can be restarted later.
 * @apiSince REL
 */

public static final int CANCEL_REASON_POSTPONED = 1; // 0x1

/**
 * The profile to be downloaded cannot be installed due to its policy rule is not allowed by
 * the RAT (Rules Authorisation Table) on the eUICC or by other installed profiles. The
 * download can be restarted later.
 * @apiSince REL
 */

public static final int CANCEL_REASON_PPR_NOT_ALLOWED = 3; // 0x3

/**
 * The download has been timed out and can be restarted later.
 * @apiSince REL
 */

public static final int CANCEL_REASON_TIMEOUT = 2; // 0x2

/**
 * Deletes all field-loaded testing profiles.
 * @apiSince REL
 */

public static final int RESET_OPTION_DELETE_FIELD_LOADED_TEST_PROFILES = 2; // 0x2

/**
 * Deletes all operational profiles.
 * @apiSince REL
 */

public static final int RESET_OPTION_DELETE_OPERATIONAL_PROFILES = 1; // 0x1

/**
 * Resets the default SM-DP+ address.
 * @apiSince REL
 */

public static final int RESET_OPTION_RESET_DEFAULT_SMDP_ADDRESS = 4; // 0x4

/**
 * Result code indicating the caller is not the active LPA.
 * @apiSince REL
 */

public static final int RESULT_CALLER_NOT_ALLOWED = -3; // 0xfffffffd

/**
 * Result code when the eUICC card with the given card Id is not found.
 * @apiSince REL
 */

public static final int RESULT_EUICC_NOT_FOUND = -2; // 0xfffffffe

/**
 * Result code of execution with no error.
 * @apiSince REL
 */

public static final int RESULT_OK = 0; // 0x0

/**
 * Result code of an unknown error.
 * @apiSince REL
 */

public static final int RESULT_UNKNOWN_ERROR = -1; // 0xffffffff
/**
 * Reason for canceling a profile download session
 * <br>
 * Value is {@link android.telephony.euicc.EuiccCardManager#CANCEL_REASON_END_USER_REJECTED}, {@link android.telephony.euicc.EuiccCardManager#CANCEL_REASON_POSTPONED}, {@link android.telephony.euicc.EuiccCardManager#CANCEL_REASON_TIMEOUT}, or {@link android.telephony.euicc.EuiccCardManager#CANCEL_REASON_PPR_NOT_ALLOWED}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface CancelReason {
}

/**
 * Options for resetting eUICC memory
 * <br>
 * Value is either <code>0</code> or a combination of {@link android.telephony.euicc.EuiccCardManager#RESET_OPTION_DELETE_OPERATIONAL_PROFILES}, {@link android.telephony.euicc.EuiccCardManager#RESET_OPTION_DELETE_FIELD_LOADED_TEST_PROFILES}, and {@link android.telephony.euicc.EuiccCardManager#RESET_OPTION_RESET_DEFAULT_SMDP_ADDRESS}
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.SOURCE)
public static @interface ResetOption {
}

/**
 * Callback to receive the result of an eUICC card API.
 *
 * @param <T> Type of the result.
 * @apiSince REL
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface ResultCallback<T> {

/**
 * This method will be called when an eUICC card API call is completed.
 *
 * @param resultCode This can be {@link #RESULT_OK} or other positive values returned by the
 *     eUICC.
 * @param result The result object. It can be null if the {@code resultCode} is not
 *     {@link #RESULT_OK}.
 * @apiSince REL
 */

public void onComplete(int resultCode, T result);
}

}

