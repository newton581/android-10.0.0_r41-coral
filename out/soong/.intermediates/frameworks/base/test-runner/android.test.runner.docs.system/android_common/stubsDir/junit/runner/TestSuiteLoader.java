
package junit.runner;


/**
 * An interface to define how a test suite should be loaded.
 *
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface TestSuiteLoader {

public java.lang.Class load(java.lang.String suiteClassName) throws java.lang.ClassNotFoundException;

public java.lang.Class reload(java.lang.Class aClass) throws java.lang.ClassNotFoundException;
}

