/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.future.usb;


/**
 * A class representing a USB accessory.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class UsbAccessory {

UsbAccessory(android.hardware.usb.UsbAccessory accessory) { throw new RuntimeException("Stub!"); }

/**
 * Returns the manufacturer of the accessory.
 *
 * @return the accessory manufacturer
 */

public java.lang.String getManufacturer() { throw new RuntimeException("Stub!"); }

/**
 * Returns the model name of the accessory.
 *
 * @return the accessory model
 */

public java.lang.String getModel() { throw new RuntimeException("Stub!"); }

/**
 * Returns a user visible description of the accessory.
 *
 * @return the accessory description
 */

public java.lang.String getDescription() { throw new RuntimeException("Stub!"); }

/**
 * Returns the version of the accessory.
 *
 * @return the accessory version
 */

public java.lang.String getVersion() { throw new RuntimeException("Stub!"); }

/**
 * Returns the URI for the accessory.
 * This is an optional URI that might show information about the accessory
 * or provide the option to download an application for the accessory
 *
 * @return the accessory URI
 */

public java.lang.String getUri() { throw new RuntimeException("Stub!"); }

/**
 * Returns the unique serial number for the accessory.
 * This is an optional serial number that can be used to differentiate
 * between individual accessories of the same model and manufacturer
 *
 * @return the unique serial number
 */

public java.lang.String getSerial() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

