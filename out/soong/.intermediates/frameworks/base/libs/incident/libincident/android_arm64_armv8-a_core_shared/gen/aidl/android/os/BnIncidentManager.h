#ifndef AIDL_GENERATED_ANDROID_OS_BN_INCIDENT_MANAGER_H_
#define AIDL_GENERATED_ANDROID_OS_BN_INCIDENT_MANAGER_H_

#include <binder/IInterface.h>
#include <android/os/IIncidentManager.h>

namespace android {

namespace os {

class BnIncidentManager : public ::android::BnInterface<IIncidentManager> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnIncidentManager

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_BN_INCIDENT_MANAGER_H_
