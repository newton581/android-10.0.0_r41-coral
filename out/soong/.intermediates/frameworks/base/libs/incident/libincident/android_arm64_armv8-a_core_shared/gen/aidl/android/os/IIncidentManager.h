#ifndef AIDL_GENERATED_ANDROID_OS_I_INCIDENT_MANAGER_H_
#define AIDL_GENERATED_ANDROID_OS_I_INCIDENT_MANAGER_H_

#include <android-base/unique_fd.h>
#include <android/os/IIncidentReportStatusListener.h>
#include <android/os/IncidentManager.h>
#include <android/os/IncidentReportArgs.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <utils/String16.h>
#include <utils/StrongPointer.h>
#include <vector>

namespace android {

namespace os {

class IIncidentManager : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(IncidentManager)
  virtual ::android::binder::Status reportIncident(const ::android::os::IncidentReportArgs& args) = 0;
  virtual ::android::binder::Status reportIncidentToStream(const ::android::os::IncidentReportArgs& args, const ::android::sp<::android::os::IIncidentReportStatusListener>& listener, const ::android::base::unique_fd& stream) = 0;
  virtual ::android::binder::Status systemRunning() = 0;
  virtual ::android::binder::Status getIncidentReportList(const ::android::String16& pkg, const ::android::String16& cls, ::std::vector<::android::String16>* _aidl_return) = 0;
  virtual ::android::binder::Status getIncidentReport(const ::android::String16& pkg, const ::android::String16& cls, const ::android::String16& id, ::android::os::IncidentManager::IncidentReport* _aidl_return) = 0;
  virtual ::android::binder::Status deleteIncidentReports(const ::android::String16& pkg, const ::android::String16& cls, const ::android::String16& id) = 0;
  virtual ::android::binder::Status deleteAllIncidentReports(const ::android::String16& pkg) = 0;
};  // class IIncidentManager

class IIncidentManagerDefault : public IIncidentManager {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status reportIncident(const ::android::os::IncidentReportArgs& args) override;
  ::android::binder::Status reportIncidentToStream(const ::android::os::IncidentReportArgs& args, const ::android::sp<::android::os::IIncidentReportStatusListener>& listener, const ::android::base::unique_fd& stream) override;
  ::android::binder::Status systemRunning() override;
  ::android::binder::Status getIncidentReportList(const ::android::String16& pkg, const ::android::String16& cls, ::std::vector<::android::String16>* _aidl_return) override;
  ::android::binder::Status getIncidentReport(const ::android::String16& pkg, const ::android::String16& cls, const ::android::String16& id, ::android::os::IncidentManager::IncidentReport* _aidl_return) override;
  ::android::binder::Status deleteIncidentReports(const ::android::String16& pkg, const ::android::String16& cls, const ::android::String16& id) override;
  ::android::binder::Status deleteAllIncidentReports(const ::android::String16& pkg) override;

};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_INCIDENT_MANAGER_H_
