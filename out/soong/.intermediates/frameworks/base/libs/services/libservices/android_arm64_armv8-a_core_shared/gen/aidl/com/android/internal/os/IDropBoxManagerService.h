#ifndef AIDL_GENERATED_COM_ANDROID_INTERNAL_OS_I_DROP_BOX_MANAGER_SERVICE_H_
#define AIDL_GENERATED_COM_ANDROID_INTERNAL_OS_I_DROP_BOX_MANAGER_SERVICE_H_

#include <android/os/DropBoxManager.h>
#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <utils/String16.h>
#include <utils/StrongPointer.h>

namespace com {

namespace android {

namespace internal {

namespace os {

class IDropBoxManagerService : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(DropBoxManagerService)
  virtual ::android::binder::Status add(const ::android::os::DropBoxManager::Entry& entry) = 0;
  virtual ::android::binder::Status isTagEnabled(const ::android::String16& tag, bool* _aidl_return) = 0;
  virtual ::android::binder::Status getNextEntry(const ::android::String16& tag, int64_t millis, const ::android::String16& packageName, ::android::os::DropBoxManager::Entry* _aidl_return) = 0;
};  // class IDropBoxManagerService

class IDropBoxManagerServiceDefault : public IDropBoxManagerService {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status add(const ::android::os::DropBoxManager::Entry& entry) override;
  ::android::binder::Status isTagEnabled(const ::android::String16& tag, bool* _aidl_return) override;
  ::android::binder::Status getNextEntry(const ::android::String16& tag, int64_t millis, const ::android::String16& packageName, ::android::os::DropBoxManager::Entry* _aidl_return) override;

};

}  // namespace os

}  // namespace internal

}  // namespace android

}  // namespace com

#endif  // AIDL_GENERATED_COM_ANDROID_INTERNAL_OS_I_DROP_BOX_MANAGER_SERVICE_H_
