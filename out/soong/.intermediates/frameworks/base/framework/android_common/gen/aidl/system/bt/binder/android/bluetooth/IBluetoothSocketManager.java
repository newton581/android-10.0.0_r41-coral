/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.bluetooth;
/**
 * API for Bluetooth Sockets service.
 *
 * {@hide}
 */
public interface IBluetoothSocketManager extends android.os.IInterface
{
  /** Default implementation for IBluetoothSocketManager. */
  public static class Default implements android.bluetooth.IBluetoothSocketManager
  {
    @Override public android.os.ParcelFileDescriptor connectSocket(android.bluetooth.BluetoothDevice device, int type, android.os.ParcelUuid uuid, int port, int flag) throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.os.ParcelFileDescriptor createSocketChannel(int type, java.lang.String serviceName, android.os.ParcelUuid uuid, int port, int flag) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void requestMaximumTxDataLength(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.bluetooth.IBluetoothSocketManager
  {
    private static final java.lang.String DESCRIPTOR = "android.bluetooth.IBluetoothSocketManager";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.bluetooth.IBluetoothSocketManager interface,
     * generating a proxy if needed.
     */
    public static android.bluetooth.IBluetoothSocketManager asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.bluetooth.IBluetoothSocketManager))) {
        return ((android.bluetooth.IBluetoothSocketManager)iin);
      }
      return new android.bluetooth.IBluetoothSocketManager.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_connectSocket:
        {
          return "connectSocket";
        }
        case TRANSACTION_createSocketChannel:
        {
          return "createSocketChannel";
        }
        case TRANSACTION_requestMaximumTxDataLength:
        {
          return "requestMaximumTxDataLength";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_connectSocket:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          android.os.ParcelUuid _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.os.ParcelUuid.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          int _arg3;
          _arg3 = data.readInt();
          int _arg4;
          _arg4 = data.readInt();
          android.os.ParcelFileDescriptor _result = this.connectSocket(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_createSocketChannel:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.os.ParcelUuid _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.os.ParcelUuid.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          int _arg3;
          _arg3 = data.readInt();
          int _arg4;
          _arg4 = data.readInt();
          android.os.ParcelFileDescriptor _result = this.createSocketChannel(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_requestMaximumTxDataLength:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothDevice _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothDevice.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.requestMaximumTxDataLength(_arg0);
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.bluetooth.IBluetoothSocketManager
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public android.os.ParcelFileDescriptor connectSocket(android.bluetooth.BluetoothDevice device, int type, android.os.ParcelUuid uuid, int port, int flag) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.ParcelFileDescriptor _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(type);
          if ((uuid!=null)) {
            _data.writeInt(1);
            uuid.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(port);
          _data.writeInt(flag);
          boolean _status = mRemote.transact(Stub.TRANSACTION_connectSocket, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().connectSocket(device, type, uuid, port, flag);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.ParcelFileDescriptor.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.os.ParcelFileDescriptor createSocketChannel(int type, java.lang.String serviceName, android.os.ParcelUuid uuid, int port, int flag) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.ParcelFileDescriptor _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(type);
          _data.writeString(serviceName);
          if ((uuid!=null)) {
            _data.writeInt(1);
            uuid.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(port);
          _data.writeInt(flag);
          boolean _status = mRemote.transact(Stub.TRANSACTION_createSocketChannel, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().createSocketChannel(type, serviceName, uuid, port, flag);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.ParcelFileDescriptor.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void requestMaximumTxDataLength(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((device!=null)) {
            _data.writeInt(1);
            device.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_requestMaximumTxDataLength, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().requestMaximumTxDataLength(device);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static android.bluetooth.IBluetoothSocketManager sDefaultImpl;
    }
    static final int TRANSACTION_connectSocket = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_createSocketChannel = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_requestMaximumTxDataLength = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    public static boolean setDefaultImpl(android.bluetooth.IBluetoothSocketManager impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.bluetooth.IBluetoothSocketManager getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public android.os.ParcelFileDescriptor connectSocket(android.bluetooth.BluetoothDevice device, int type, android.os.ParcelUuid uuid, int port, int flag) throws android.os.RemoteException;
  public android.os.ParcelFileDescriptor createSocketChannel(int type, java.lang.String serviceName, android.os.ParcelUuid uuid, int port, int flag) throws android.os.RemoteException;
  public void requestMaximumTxDataLength(android.bluetooth.BluetoothDevice device) throws android.os.RemoteException;
}
