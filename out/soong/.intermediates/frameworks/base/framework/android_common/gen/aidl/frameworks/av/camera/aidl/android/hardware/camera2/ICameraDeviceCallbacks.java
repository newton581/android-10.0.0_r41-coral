/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.hardware.camera2;
/** @hide */
public interface ICameraDeviceCallbacks extends android.os.IInterface
{
  /** Default implementation for ICameraDeviceCallbacks. */
  public static class Default implements android.hardware.camera2.ICameraDeviceCallbacks
  {
    @Override public void onDeviceError(int errorCode, android.hardware.camera2.impl.CaptureResultExtras resultExtras) throws android.os.RemoteException
    {
    }
    @Override public void onDeviceIdle() throws android.os.RemoteException
    {
    }
    @Override public void onCaptureStarted(android.hardware.camera2.impl.CaptureResultExtras resultExtras, long timestamp) throws android.os.RemoteException
    {
    }
    @Override public void onResultReceived(android.hardware.camera2.impl.CameraMetadataNative result, android.hardware.camera2.impl.CaptureResultExtras resultExtras, android.hardware.camera2.impl.PhysicalCaptureResultInfo[] physicalCaptureResultInfos) throws android.os.RemoteException
    {
    }
    @Override public void onPrepared(int streamId) throws android.os.RemoteException
    {
    }
    /**
         * Repeating request encountered an error and was stopped.
         *
         * @param lastFrameNumber Frame number of the last frame of the streaming request.
         * @param repeatingRequestId the ID of the repeating request being stopped
         */
    @Override public void onRepeatingRequestError(long lastFrameNumber, int repeatingRequestId) throws android.os.RemoteException
    {
    }
    @Override public void onRequestQueueEmpty() throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.hardware.camera2.ICameraDeviceCallbacks
  {
    private static final java.lang.String DESCRIPTOR = "android.hardware.camera2.ICameraDeviceCallbacks";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.hardware.camera2.ICameraDeviceCallbacks interface,
     * generating a proxy if needed.
     */
    public static android.hardware.camera2.ICameraDeviceCallbacks asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.hardware.camera2.ICameraDeviceCallbacks))) {
        return ((android.hardware.camera2.ICameraDeviceCallbacks)iin);
      }
      return new android.hardware.camera2.ICameraDeviceCallbacks.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onDeviceError:
        {
          return "onDeviceError";
        }
        case TRANSACTION_onDeviceIdle:
        {
          return "onDeviceIdle";
        }
        case TRANSACTION_onCaptureStarted:
        {
          return "onCaptureStarted";
        }
        case TRANSACTION_onResultReceived:
        {
          return "onResultReceived";
        }
        case TRANSACTION_onPrepared:
        {
          return "onPrepared";
        }
        case TRANSACTION_onRepeatingRequestError:
        {
          return "onRepeatingRequestError";
        }
        case TRANSACTION_onRequestQueueEmpty:
        {
          return "onRequestQueueEmpty";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onDeviceError:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.hardware.camera2.impl.CaptureResultExtras _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.hardware.camera2.impl.CaptureResultExtras.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onDeviceError(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onDeviceIdle:
        {
          data.enforceInterface(descriptor);
          this.onDeviceIdle();
          return true;
        }
        case TRANSACTION_onCaptureStarted:
        {
          data.enforceInterface(descriptor);
          android.hardware.camera2.impl.CaptureResultExtras _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.hardware.camera2.impl.CaptureResultExtras.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          long _arg1;
          _arg1 = data.readLong();
          this.onCaptureStarted(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onResultReceived:
        {
          data.enforceInterface(descriptor);
          android.hardware.camera2.impl.CameraMetadataNative _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.hardware.camera2.impl.CameraMetadataNative.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.hardware.camera2.impl.CaptureResultExtras _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.hardware.camera2.impl.CaptureResultExtras.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          android.hardware.camera2.impl.PhysicalCaptureResultInfo[] _arg2;
          _arg2 = data.createTypedArray(android.hardware.camera2.impl.PhysicalCaptureResultInfo.CREATOR);
          this.onResultReceived(_arg0, _arg1, _arg2);
          return true;
        }
        case TRANSACTION_onPrepared:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.onPrepared(_arg0);
          return true;
        }
        case TRANSACTION_onRepeatingRequestError:
        {
          data.enforceInterface(descriptor);
          long _arg0;
          _arg0 = data.readLong();
          int _arg1;
          _arg1 = data.readInt();
          this.onRepeatingRequestError(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onRequestQueueEmpty:
        {
          data.enforceInterface(descriptor);
          this.onRequestQueueEmpty();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.hardware.camera2.ICameraDeviceCallbacks
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onDeviceError(int errorCode, android.hardware.camera2.impl.CaptureResultExtras resultExtras) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(errorCode);
          if ((resultExtras!=null)) {
            _data.writeInt(1);
            resultExtras.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onDeviceError, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onDeviceError(errorCode, resultExtras);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onDeviceIdle() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onDeviceIdle, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onDeviceIdle();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onCaptureStarted(android.hardware.camera2.impl.CaptureResultExtras resultExtras, long timestamp) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((resultExtras!=null)) {
            _data.writeInt(1);
            resultExtras.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeLong(timestamp);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onCaptureStarted, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onCaptureStarted(resultExtras, timestamp);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onResultReceived(android.hardware.camera2.impl.CameraMetadataNative result, android.hardware.camera2.impl.CaptureResultExtras resultExtras, android.hardware.camera2.impl.PhysicalCaptureResultInfo[] physicalCaptureResultInfos) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((result!=null)) {
            _data.writeInt(1);
            result.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((resultExtras!=null)) {
            _data.writeInt(1);
            resultExtras.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeTypedArray(physicalCaptureResultInfos, 0);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onResultReceived, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onResultReceived(result, resultExtras, physicalCaptureResultInfos);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onPrepared(int streamId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(streamId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onPrepared, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onPrepared(streamId);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Repeating request encountered an error and was stopped.
           *
           * @param lastFrameNumber Frame number of the last frame of the streaming request.
           * @param repeatingRequestId the ID of the repeating request being stopped
           */
      @Override public void onRepeatingRequestError(long lastFrameNumber, int repeatingRequestId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeLong(lastFrameNumber);
          _data.writeInt(repeatingRequestId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onRepeatingRequestError, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onRepeatingRequestError(lastFrameNumber, repeatingRequestId);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onRequestQueueEmpty() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onRequestQueueEmpty, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onRequestQueueEmpty();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.hardware.camera2.ICameraDeviceCallbacks sDefaultImpl;
    }
    static final int TRANSACTION_onDeviceError = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onDeviceIdle = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_onCaptureStarted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_onResultReceived = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_onPrepared = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_onRepeatingRequestError = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_onRequestQueueEmpty = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    public static boolean setDefaultImpl(android.hardware.camera2.ICameraDeviceCallbacks impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.hardware.camera2.ICameraDeviceCallbacks getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int ERROR_CAMERA_INVALID_ERROR = -1;
  public static final int ERROR_CAMERA_DISCONNECTED = 0;
  public static final int ERROR_CAMERA_DEVICE = 1;
  public static final int ERROR_CAMERA_SERVICE = 2;
  public static final int ERROR_CAMERA_REQUEST = 3;
  public static final int ERROR_CAMERA_RESULT = 4;
  public static final int ERROR_CAMERA_BUFFER = 5;
  public static final int ERROR_CAMERA_DISABLED = 6;
  public void onDeviceError(int errorCode, android.hardware.camera2.impl.CaptureResultExtras resultExtras) throws android.os.RemoteException;
  public void onDeviceIdle() throws android.os.RemoteException;
  public void onCaptureStarted(android.hardware.camera2.impl.CaptureResultExtras resultExtras, long timestamp) throws android.os.RemoteException;
  public void onResultReceived(android.hardware.camera2.impl.CameraMetadataNative result, android.hardware.camera2.impl.CaptureResultExtras resultExtras, android.hardware.camera2.impl.PhysicalCaptureResultInfo[] physicalCaptureResultInfos) throws android.os.RemoteException;
  public void onPrepared(int streamId) throws android.os.RemoteException;
  /**
       * Repeating request encountered an error and was stopped.
       *
       * @param lastFrameNumber Frame number of the last frame of the streaming request.
       * @param repeatingRequestId the ID of the repeating request being stopped
       */
  public void onRepeatingRequestError(long lastFrameNumber, int repeatingRequestId) throws android.os.RemoteException;
  public void onRequestQueueEmpty() throws android.os.RemoteException;
}
