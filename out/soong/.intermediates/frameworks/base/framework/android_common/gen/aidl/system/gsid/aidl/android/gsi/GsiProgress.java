/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.gsi;
/** {@hide} */
public class GsiProgress implements android.os.Parcelable
{
  /* String containing which step of the installation is in progress. */
  public java.lang.String step;
  /* Status code (see constants in IGsiService.aidl) */
  public int status;
  /* Number of bytes processed */
  public long bytes_processed;
  /* Total number of bytes to be processed */
  public long total_bytes;
  public static final android.os.Parcelable.Creator<GsiProgress> CREATOR = new android.os.Parcelable.Creator<GsiProgress>() {
    @Override
    public GsiProgress createFromParcel(android.os.Parcel _aidl_source) {
      GsiProgress _aidl_out = new GsiProgress();
      _aidl_out.readFromParcel(_aidl_source);
      return _aidl_out;
    }
    @Override
    public GsiProgress[] newArray(int _aidl_size) {
      return new GsiProgress[_aidl_size];
    }
  };
  @Override public final void writeToParcel(android.os.Parcel _aidl_parcel, int _aidl_flag)
  {
    int _aidl_start_pos = _aidl_parcel.dataPosition();
    _aidl_parcel.writeInt(0);
    _aidl_parcel.writeString(step);
    _aidl_parcel.writeInt(status);
    _aidl_parcel.writeLong(bytes_processed);
    _aidl_parcel.writeLong(total_bytes);
    int _aidl_end_pos = _aidl_parcel.dataPosition();
    _aidl_parcel.setDataPosition(_aidl_start_pos);
    _aidl_parcel.writeInt(_aidl_end_pos - _aidl_start_pos);
    _aidl_parcel.setDataPosition(_aidl_end_pos);
  }
  public final void readFromParcel(android.os.Parcel _aidl_parcel)
  {
    int _aidl_start_pos = _aidl_parcel.dataPosition();
    int _aidl_parcelable_size = _aidl_parcel.readInt();
    if (_aidl_parcelable_size < 0) return;
    try {
      step = _aidl_parcel.readString();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      status = _aidl_parcel.readInt();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      bytes_processed = _aidl_parcel.readLong();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      total_bytes = _aidl_parcel.readLong();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
    } finally {
      _aidl_parcel.setDataPosition(_aidl_start_pos + _aidl_parcelable_size);
    }
  }
  @Override public int describeContents()
  {
    return 0;
  }
}
