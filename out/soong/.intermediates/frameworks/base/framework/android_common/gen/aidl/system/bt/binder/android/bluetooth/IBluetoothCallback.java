/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.bluetooth;
/**
 * System private API for Bluetooth service callbacks.
 *
 * {@hide}
 */
public interface IBluetoothCallback extends android.os.IInterface
{
  /** Default implementation for IBluetoothCallback. */
  public static class Default implements android.bluetooth.IBluetoothCallback
  {
    //void onRfcommChannelFound(int channel);

    @Override public void onBluetoothStateChange(int prevState, int newState) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.bluetooth.IBluetoothCallback
  {
    private static final java.lang.String DESCRIPTOR = "android.bluetooth.IBluetoothCallback";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.bluetooth.IBluetoothCallback interface,
     * generating a proxy if needed.
     */
    public static android.bluetooth.IBluetoothCallback asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.bluetooth.IBluetoothCallback))) {
        return ((android.bluetooth.IBluetoothCallback)iin);
      }
      return new android.bluetooth.IBluetoothCallback.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onBluetoothStateChange:
        {
          return "onBluetoothStateChange";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onBluetoothStateChange:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.onBluetoothStateChange(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.bluetooth.IBluetoothCallback
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      //void onRfcommChannelFound(int channel);

      @Override public void onBluetoothStateChange(int prevState, int newState) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(prevState);
          _data.writeInt(newState);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onBluetoothStateChange, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onBluetoothStateChange(prevState, newState);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      public static android.bluetooth.IBluetoothCallback sDefaultImpl;
    }
    static final int TRANSACTION_onBluetoothStateChange = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    public static boolean setDefaultImpl(android.bluetooth.IBluetoothCallback impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.bluetooth.IBluetoothCallback getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  //void onRfcommChannelFound(int channel);

  public void onBluetoothStateChange(int prevState, int newState) throws android.os.RemoteException;
}
