/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.gsi;
/** {@hide} */
public class GsiInstallParams implements android.os.Parcelable
{
  /**
       * The directory to install GSI images under. This must be either an empty
       * string (which will use the default /data/gsi), "/data/gsi", or a mount
       * under /mnt/media_rw. It may end in a trailing slash.
       */
  public java.lang.String installDir;
  /* The size of the on-disk GSI image. */
  public long gsiSize;
  /* The desired size of the userdata partition. */
  public long userdataSize;
  /* If false, a userdata image is only created if one does not already
       * exist. If the size is zero, a default size of 8GiB is used. If there is
       * an existing image smaller than the desired size, it may be resized
       * automatically.
       */
  public boolean wipeUserdata;
  public static final android.os.Parcelable.Creator<GsiInstallParams> CREATOR = new android.os.Parcelable.Creator<GsiInstallParams>() {
    @Override
    public GsiInstallParams createFromParcel(android.os.Parcel _aidl_source) {
      GsiInstallParams _aidl_out = new GsiInstallParams();
      _aidl_out.readFromParcel(_aidl_source);
      return _aidl_out;
    }
    @Override
    public GsiInstallParams[] newArray(int _aidl_size) {
      return new GsiInstallParams[_aidl_size];
    }
  };
  @Override public final void writeToParcel(android.os.Parcel _aidl_parcel, int _aidl_flag)
  {
    int _aidl_start_pos = _aidl_parcel.dataPosition();
    _aidl_parcel.writeInt(0);
    _aidl_parcel.writeString(installDir);
    _aidl_parcel.writeLong(gsiSize);
    _aidl_parcel.writeLong(userdataSize);
    _aidl_parcel.writeInt(((wipeUserdata)?(1):(0)));
    int _aidl_end_pos = _aidl_parcel.dataPosition();
    _aidl_parcel.setDataPosition(_aidl_start_pos);
    _aidl_parcel.writeInt(_aidl_end_pos - _aidl_start_pos);
    _aidl_parcel.setDataPosition(_aidl_end_pos);
  }
  public final void readFromParcel(android.os.Parcel _aidl_parcel)
  {
    int _aidl_start_pos = _aidl_parcel.dataPosition();
    int _aidl_parcelable_size = _aidl_parcel.readInt();
    if (_aidl_parcelable_size < 0) return;
    try {
      installDir = _aidl_parcel.readString();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      gsiSize = _aidl_parcel.readLong();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      userdataSize = _aidl_parcel.readLong();
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
      wipeUserdata = (0!=_aidl_parcel.readInt());
      if (_aidl_parcel.dataPosition() - _aidl_start_pos >= _aidl_parcelable_size) return;
    } finally {
      _aidl_parcel.setDataPosition(_aidl_start_pos + _aidl_parcelable_size);
    }
  }
  @Override public int describeContents()
  {
    return 0;
  }
}
