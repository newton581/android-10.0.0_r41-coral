/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.security;
/**
 * Caller is required to ensure that {@link KeyStore#unlock
 * KeyStore.unlock} was successful.
 *
 * @hide
 */
public interface IKeyChainService extends android.os.IInterface
{
  /** Default implementation for IKeyChainService. */
  public static class Default implements android.security.IKeyChainService
  {
    // APIs used by KeyChain

    @Override public java.lang.String requestPrivateKey(java.lang.String alias) throws android.os.RemoteException
    {
      return null;
    }
    @Override public byte[] getCertificate(java.lang.String alias) throws android.os.RemoteException
    {
      return null;
    }
    @Override public byte[] getCaCertificates(java.lang.String alias) throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean isUserSelectable(java.lang.String alias) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void setUserSelectable(java.lang.String alias, boolean isUserSelectable) throws android.os.RemoteException
    {
    }
    @Override public int generateKeyPair(java.lang.String algorithm, android.security.keystore.ParcelableKeyGenParameterSpec spec) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int attestKey(java.lang.String alias, byte[] challenge, int[] idAttestationFlags, android.security.keymaster.KeymasterCertificateChain chain) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean setKeyPairCertificate(java.lang.String alias, byte[] userCert, byte[] certChain) throws android.os.RemoteException
    {
      return false;
    }
    // APIs used by CertInstaller and DevicePolicyManager

    @Override public java.lang.String installCaCertificate(byte[] caCertificate) throws android.os.RemoteException
    {
      return null;
    }
    // APIs used by DevicePolicyManager

    @Override public boolean installKeyPair(byte[] privateKey, byte[] userCert, byte[] certChain, java.lang.String alias) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean removeKeyPair(java.lang.String alias) throws android.os.RemoteException
    {
      return false;
    }
    // APIs used by Settings

    @Override public boolean deleteCaCertificate(java.lang.String alias) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean reset() throws android.os.RemoteException
    {
      return false;
    }
    @Override public android.content.pm.StringParceledListSlice getUserCaAliases() throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.content.pm.StringParceledListSlice getSystemCaAliases() throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean containsCaAlias(java.lang.String alias) throws android.os.RemoteException
    {
      return false;
    }
    @Override public byte[] getEncodedCaCertificate(java.lang.String alias, boolean includeDeletedSystem) throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.util.List<java.lang.String> getCaCertificateChainAliases(java.lang.String rootAlias, boolean includeDeletedSystem) throws android.os.RemoteException
    {
      return null;
    }
    // APIs used by KeyChainActivity

    @Override public void setGrant(int uid, java.lang.String alias, boolean value) throws android.os.RemoteException
    {
    }
    @Override public boolean hasGrant(int uid, java.lang.String alias) throws android.os.RemoteException
    {
      return false;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.security.IKeyChainService
  {
    private static final java.lang.String DESCRIPTOR = "android.security.IKeyChainService";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.security.IKeyChainService interface,
     * generating a proxy if needed.
     */
    public static android.security.IKeyChainService asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.security.IKeyChainService))) {
        return ((android.security.IKeyChainService)iin);
      }
      return new android.security.IKeyChainService.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_requestPrivateKey:
        {
          return "requestPrivateKey";
        }
        case TRANSACTION_getCertificate:
        {
          return "getCertificate";
        }
        case TRANSACTION_getCaCertificates:
        {
          return "getCaCertificates";
        }
        case TRANSACTION_isUserSelectable:
        {
          return "isUserSelectable";
        }
        case TRANSACTION_setUserSelectable:
        {
          return "setUserSelectable";
        }
        case TRANSACTION_generateKeyPair:
        {
          return "generateKeyPair";
        }
        case TRANSACTION_attestKey:
        {
          return "attestKey";
        }
        case TRANSACTION_setKeyPairCertificate:
        {
          return "setKeyPairCertificate";
        }
        case TRANSACTION_installCaCertificate:
        {
          return "installCaCertificate";
        }
        case TRANSACTION_installKeyPair:
        {
          return "installKeyPair";
        }
        case TRANSACTION_removeKeyPair:
        {
          return "removeKeyPair";
        }
        case TRANSACTION_deleteCaCertificate:
        {
          return "deleteCaCertificate";
        }
        case TRANSACTION_reset:
        {
          return "reset";
        }
        case TRANSACTION_getUserCaAliases:
        {
          return "getUserCaAliases";
        }
        case TRANSACTION_getSystemCaAliases:
        {
          return "getSystemCaAliases";
        }
        case TRANSACTION_containsCaAlias:
        {
          return "containsCaAlias";
        }
        case TRANSACTION_getEncodedCaCertificate:
        {
          return "getEncodedCaCertificate";
        }
        case TRANSACTION_getCaCertificateChainAliases:
        {
          return "getCaCertificateChainAliases";
        }
        case TRANSACTION_setGrant:
        {
          return "setGrant";
        }
        case TRANSACTION_hasGrant:
        {
          return "hasGrant";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_requestPrivateKey:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _result = this.requestPrivateKey(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_getCertificate:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          byte[] _result = this.getCertificate(_arg0);
          reply.writeNoException();
          reply.writeByteArray(_result);
          return true;
        }
        case TRANSACTION_getCaCertificates:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          byte[] _result = this.getCaCertificates(_arg0);
          reply.writeNoException();
          reply.writeByteArray(_result);
          return true;
        }
        case TRANSACTION_isUserSelectable:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.isUserSelectable(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setUserSelectable:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.setUserSelectable(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_generateKeyPair:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          android.security.keystore.ParcelableKeyGenParameterSpec _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.security.keystore.ParcelableKeyGenParameterSpec.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          int _result = this.generateKeyPair(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_attestKey:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          byte[] _arg1;
          _arg1 = data.createByteArray();
          int[] _arg2;
          _arg2 = data.createIntArray();
          android.security.keymaster.KeymasterCertificateChain _arg3;
          _arg3 = new android.security.keymaster.KeymasterCertificateChain();
          int _result = this.attestKey(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(_result);
          if ((_arg3!=null)) {
            reply.writeInt(1);
            _arg3.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_setKeyPairCertificate:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          byte[] _arg1;
          _arg1 = data.createByteArray();
          byte[] _arg2;
          _arg2 = data.createByteArray();
          boolean _result = this.setKeyPairCertificate(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_installCaCertificate:
        {
          data.enforceInterface(descriptor);
          byte[] _arg0;
          _arg0 = data.createByteArray();
          java.lang.String _result = this.installCaCertificate(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_installKeyPair:
        {
          data.enforceInterface(descriptor);
          byte[] _arg0;
          _arg0 = data.createByteArray();
          byte[] _arg1;
          _arg1 = data.createByteArray();
          byte[] _arg2;
          _arg2 = data.createByteArray();
          java.lang.String _arg3;
          _arg3 = data.readString();
          boolean _result = this.installKeyPair(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_removeKeyPair:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.removeKeyPair(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_deleteCaCertificate:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.deleteCaCertificate(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_reset:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.reset();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getUserCaAliases:
        {
          data.enforceInterface(descriptor);
          android.content.pm.StringParceledListSlice _result = this.getUserCaAliases();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getSystemCaAliases:
        {
          data.enforceInterface(descriptor);
          android.content.pm.StringParceledListSlice _result = this.getSystemCaAliases();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_containsCaAlias:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.containsCaAlias(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getEncodedCaCertificate:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          byte[] _result = this.getEncodedCaCertificate(_arg0, _arg1);
          reply.writeNoException();
          reply.writeByteArray(_result);
          return true;
        }
        case TRANSACTION_getCaCertificateChainAliases:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          java.util.List<java.lang.String> _result = this.getCaCertificateChainAliases(_arg0, _arg1);
          reply.writeNoException();
          reply.writeStringList(_result);
          return true;
        }
        case TRANSACTION_setGrant:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _arg2;
          _arg2 = (0!=data.readInt());
          this.setGrant(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_hasGrant:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          boolean _result = this.hasGrant(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.security.IKeyChainService
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      // APIs used by KeyChain

      @Override public java.lang.String requestPrivateKey(java.lang.String alias) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          boolean _status = mRemote.transact(Stub.TRANSACTION_requestPrivateKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().requestPrivateKey(alias);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public byte[] getCertificate(java.lang.String alias) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        byte[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCertificate, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCertificate(alias);
          }
          _reply.readException();
          _result = _reply.createByteArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public byte[] getCaCertificates(java.lang.String alias) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        byte[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCaCertificates, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCaCertificates(alias);
          }
          _reply.readException();
          _result = _reply.createByteArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isUserSelectable(java.lang.String alias) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isUserSelectable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isUserSelectable(alias);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void setUserSelectable(java.lang.String alias, boolean isUserSelectable) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          _data.writeInt(((isUserSelectable)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setUserSelectable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setUserSelectable(alias, isUserSelectable);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public int generateKeyPair(java.lang.String algorithm, android.security.keystore.ParcelableKeyGenParameterSpec spec) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(algorithm);
          if ((spec!=null)) {
            _data.writeInt(1);
            spec.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_generateKeyPair, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().generateKeyPair(algorithm, spec);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int attestKey(java.lang.String alias, byte[] challenge, int[] idAttestationFlags, android.security.keymaster.KeymasterCertificateChain chain) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          _data.writeByteArray(challenge);
          _data.writeIntArray(idAttestationFlags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_attestKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().attestKey(alias, challenge, idAttestationFlags, chain);
          }
          _reply.readException();
          _result = _reply.readInt();
          if ((0!=_reply.readInt())) {
            chain.readFromParcel(_reply);
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean setKeyPairCertificate(java.lang.String alias, byte[] userCert, byte[] certChain) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          _data.writeByteArray(userCert);
          _data.writeByteArray(certChain);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setKeyPairCertificate, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setKeyPairCertificate(alias, userCert, certChain);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // APIs used by CertInstaller and DevicePolicyManager

      @Override public java.lang.String installCaCertificate(byte[] caCertificate) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeByteArray(caCertificate);
          boolean _status = mRemote.transact(Stub.TRANSACTION_installCaCertificate, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().installCaCertificate(caCertificate);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // APIs used by DevicePolicyManager

      @Override public boolean installKeyPair(byte[] privateKey, byte[] userCert, byte[] certChain, java.lang.String alias) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeByteArray(privateKey);
          _data.writeByteArray(userCert);
          _data.writeByteArray(certChain);
          _data.writeString(alias);
          boolean _status = mRemote.transact(Stub.TRANSACTION_installKeyPair, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().installKeyPair(privateKey, userCert, certChain, alias);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean removeKeyPair(java.lang.String alias) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          boolean _status = mRemote.transact(Stub.TRANSACTION_removeKeyPair, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().removeKeyPair(alias);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // APIs used by Settings

      @Override public boolean deleteCaCertificate(java.lang.String alias) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          boolean _status = mRemote.transact(Stub.TRANSACTION_deleteCaCertificate, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().deleteCaCertificate(alias);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean reset() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reset, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().reset();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.content.pm.StringParceledListSlice getUserCaAliases() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.pm.StringParceledListSlice _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getUserCaAliases, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getUserCaAliases();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.pm.StringParceledListSlice.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.content.pm.StringParceledListSlice getSystemCaAliases() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.content.pm.StringParceledListSlice _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getSystemCaAliases, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getSystemCaAliases();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.content.pm.StringParceledListSlice.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean containsCaAlias(java.lang.String alias) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          boolean _status = mRemote.transact(Stub.TRANSACTION_containsCaAlias, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().containsCaAlias(alias);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public byte[] getEncodedCaCertificate(java.lang.String alias, boolean includeDeletedSystem) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        byte[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(alias);
          _data.writeInt(((includeDeletedSystem)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getEncodedCaCertificate, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getEncodedCaCertificate(alias, includeDeletedSystem);
          }
          _reply.readException();
          _result = _reply.createByteArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.util.List<java.lang.String> getCaCertificateChainAliases(java.lang.String rootAlias, boolean includeDeletedSystem) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.util.List<java.lang.String> _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(rootAlias);
          _data.writeInt(((includeDeletedSystem)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCaCertificateChainAliases, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCaCertificateChainAliases(rootAlias, includeDeletedSystem);
          }
          _reply.readException();
          _result = _reply.createStringArrayList();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // APIs used by KeyChainActivity

      @Override public void setGrant(int uid, java.lang.String alias, boolean value) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeString(alias);
          _data.writeInt(((value)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setGrant, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setGrant(uid, alias, value);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean hasGrant(int uid, java.lang.String alias) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeString(alias);
          boolean _status = mRemote.transact(Stub.TRANSACTION_hasGrant, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().hasGrant(uid, alias);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static android.security.IKeyChainService sDefaultImpl;
    }
    static final int TRANSACTION_requestPrivateKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_getCertificate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_getCaCertificates = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_isUserSelectable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_setUserSelectable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_generateKeyPair = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_attestKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_setKeyPairCertificate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_installCaCertificate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_installKeyPair = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_removeKeyPair = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_deleteCaCertificate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_reset = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_getUserCaAliases = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_getSystemCaAliases = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_containsCaAlias = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    static final int TRANSACTION_getEncodedCaCertificate = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
    static final int TRANSACTION_getCaCertificateChainAliases = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
    static final int TRANSACTION_setGrant = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
    static final int TRANSACTION_hasGrant = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
    public static boolean setDefaultImpl(android.security.IKeyChainService impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.security.IKeyChainService getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  // APIs used by KeyChain

  @android.annotation.UnsupportedAppUsage
  public java.lang.String requestPrivateKey(java.lang.String alias) throws android.os.RemoteException;
  public byte[] getCertificate(java.lang.String alias) throws android.os.RemoteException;
  public byte[] getCaCertificates(java.lang.String alias) throws android.os.RemoteException;
  public boolean isUserSelectable(java.lang.String alias) throws android.os.RemoteException;
  public void setUserSelectable(java.lang.String alias, boolean isUserSelectable) throws android.os.RemoteException;
  public int generateKeyPair(java.lang.String algorithm, android.security.keystore.ParcelableKeyGenParameterSpec spec) throws android.os.RemoteException;
  public int attestKey(java.lang.String alias, byte[] challenge, int[] idAttestationFlags, android.security.keymaster.KeymasterCertificateChain chain) throws android.os.RemoteException;
  public boolean setKeyPairCertificate(java.lang.String alias, byte[] userCert, byte[] certChain) throws android.os.RemoteException;
  // APIs used by CertInstaller and DevicePolicyManager

  public java.lang.String installCaCertificate(byte[] caCertificate) throws android.os.RemoteException;
  // APIs used by DevicePolicyManager

  public boolean installKeyPair(byte[] privateKey, byte[] userCert, byte[] certChain, java.lang.String alias) throws android.os.RemoteException;
  public boolean removeKeyPair(java.lang.String alias) throws android.os.RemoteException;
  // APIs used by Settings

  public boolean deleteCaCertificate(java.lang.String alias) throws android.os.RemoteException;
  public boolean reset() throws android.os.RemoteException;
  public android.content.pm.StringParceledListSlice getUserCaAliases() throws android.os.RemoteException;
  public android.content.pm.StringParceledListSlice getSystemCaAliases() throws android.os.RemoteException;
  public boolean containsCaAlias(java.lang.String alias) throws android.os.RemoteException;
  public byte[] getEncodedCaCertificate(java.lang.String alias, boolean includeDeletedSystem) throws android.os.RemoteException;
  public java.util.List<java.lang.String> getCaCertificateChainAliases(java.lang.String rootAlias, boolean includeDeletedSystem) throws android.os.RemoteException;
  // APIs used by KeyChainActivity

  public void setGrant(int uid, java.lang.String alias, boolean value) throws android.os.RemoteException;
  public boolean hasGrant(int uid, java.lang.String alias) throws android.os.RemoteException;
}
