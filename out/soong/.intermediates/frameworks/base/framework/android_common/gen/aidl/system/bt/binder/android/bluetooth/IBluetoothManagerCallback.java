/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.bluetooth;
/**
 * API for Communication between BluetoothAdapter and BluetoothManager
 *
 * {@hide}
 */
public interface IBluetoothManagerCallback extends android.os.IInterface
{
  /** Default implementation for IBluetoothManagerCallback. */
  public static class Default implements android.bluetooth.IBluetoothManagerCallback
  {
    @Override public void onBluetoothServiceUp(android.bluetooth.IBluetooth bluetoothService) throws android.os.RemoteException
    {
    }
    @Override public void onBluetoothServiceDown() throws android.os.RemoteException
    {
    }
    @Override public void onBrEdrDown() throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.bluetooth.IBluetoothManagerCallback
  {
    private static final java.lang.String DESCRIPTOR = "android.bluetooth.IBluetoothManagerCallback";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.bluetooth.IBluetoothManagerCallback interface,
     * generating a proxy if needed.
     */
    public static android.bluetooth.IBluetoothManagerCallback asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.bluetooth.IBluetoothManagerCallback))) {
        return ((android.bluetooth.IBluetoothManagerCallback)iin);
      }
      return new android.bluetooth.IBluetoothManagerCallback.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onBluetoothServiceUp:
        {
          return "onBluetoothServiceUp";
        }
        case TRANSACTION_onBluetoothServiceDown:
        {
          return "onBluetoothServiceDown";
        }
        case TRANSACTION_onBrEdrDown:
        {
          return "onBrEdrDown";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onBluetoothServiceUp:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.IBluetooth _arg0;
          _arg0 = android.bluetooth.IBluetooth.Stub.asInterface(data.readStrongBinder());
          this.onBluetoothServiceUp(_arg0);
          return true;
        }
        case TRANSACTION_onBluetoothServiceDown:
        {
          data.enforceInterface(descriptor);
          this.onBluetoothServiceDown();
          return true;
        }
        case TRANSACTION_onBrEdrDown:
        {
          data.enforceInterface(descriptor);
          this.onBrEdrDown();
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.bluetooth.IBluetoothManagerCallback
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onBluetoothServiceUp(android.bluetooth.IBluetooth bluetoothService) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((bluetoothService!=null))?(bluetoothService.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_onBluetoothServiceUp, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onBluetoothServiceUp(bluetoothService);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onBluetoothServiceDown() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onBluetoothServiceDown, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onBluetoothServiceDown();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onBrEdrDown() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onBrEdrDown, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onBrEdrDown();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.bluetooth.IBluetoothManagerCallback sDefaultImpl;
    }
    static final int TRANSACTION_onBluetoothServiceUp = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onBluetoothServiceDown = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_onBrEdrDown = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    public static boolean setDefaultImpl(android.bluetooth.IBluetoothManagerCallback impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.bluetooth.IBluetoothManagerCallback getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public void onBluetoothServiceUp(android.bluetooth.IBluetooth bluetoothService) throws android.os.RemoteException;
  public void onBluetoothServiceDown() throws android.os.RemoteException;
  public void onBrEdrDown() throws android.os.RemoteException;
}
