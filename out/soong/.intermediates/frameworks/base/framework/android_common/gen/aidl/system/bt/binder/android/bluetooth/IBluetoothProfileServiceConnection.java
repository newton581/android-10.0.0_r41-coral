/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.bluetooth;
/**
 * Callback for bluetooth profile connections.
 *
 * {@hide}
 */
public interface IBluetoothProfileServiceConnection extends android.os.IInterface
{
  /** Default implementation for IBluetoothProfileServiceConnection. */
  public static class Default implements android.bluetooth.IBluetoothProfileServiceConnection
  {
    @Override public void onServiceConnected(android.content.ComponentName comp, android.os.IBinder service) throws android.os.RemoteException
    {
    }
    @Override public void onServiceDisconnected(android.content.ComponentName comp) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.bluetooth.IBluetoothProfileServiceConnection
  {
    private static final java.lang.String DESCRIPTOR = "android.bluetooth.IBluetoothProfileServiceConnection";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.bluetooth.IBluetoothProfileServiceConnection interface,
     * generating a proxy if needed.
     */
    public static android.bluetooth.IBluetoothProfileServiceConnection asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.bluetooth.IBluetoothProfileServiceConnection))) {
        return ((android.bluetooth.IBluetoothProfileServiceConnection)iin);
      }
      return new android.bluetooth.IBluetoothProfileServiceConnection.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onServiceConnected:
        {
          return "onServiceConnected";
        }
        case TRANSACTION_onServiceDisconnected:
        {
          return "onServiceDisconnected";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onServiceConnected:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.IBinder _arg1;
          _arg1 = data.readStrongBinder();
          this.onServiceConnected(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onServiceDisconnected:
        {
          data.enforceInterface(descriptor);
          android.content.ComponentName _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.content.ComponentName.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.onServiceDisconnected(_arg0);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.bluetooth.IBluetoothProfileServiceConnection
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onServiceConnected(android.content.ComponentName comp, android.os.IBinder service) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((comp!=null)) {
            _data.writeInt(1);
            comp.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeStrongBinder(service);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onServiceConnected, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onServiceConnected(comp, service);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onServiceDisconnected(android.content.ComponentName comp) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((comp!=null)) {
            _data.writeInt(1);
            comp.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onServiceDisconnected, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onServiceDisconnected(comp);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.bluetooth.IBluetoothProfileServiceConnection sDefaultImpl;
    }
    static final int TRANSACTION_onServiceConnected = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onServiceDisconnected = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    public static boolean setDefaultImpl(android.bluetooth.IBluetoothProfileServiceConnection impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.bluetooth.IBluetoothProfileServiceConnection getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public void onServiceConnected(android.content.ComponentName comp, android.os.IBinder service) throws android.os.RemoteException;
  public void onServiceDisconnected(android.content.ComponentName comp) throws android.os.RemoteException;
}
