/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os;
/** {@hide} */
public interface IVoldListener extends android.os.IInterface
{
  /** Default implementation for IVoldListener. */
  public static class Default implements android.os.IVoldListener
  {
    @Override public void onDiskCreated(java.lang.String diskId, int flags) throws android.os.RemoteException
    {
    }
    @Override public void onDiskScanned(java.lang.String diskId) throws android.os.RemoteException
    {
    }
    @Override public void onDiskMetadataChanged(java.lang.String diskId, long sizeBytes, java.lang.String label, java.lang.String sysPath) throws android.os.RemoteException
    {
    }
    @Override public void onDiskDestroyed(java.lang.String diskId) throws android.os.RemoteException
    {
    }
    @Override public void onVolumeCreated(java.lang.String volId, int type, java.lang.String diskId, java.lang.String partGuid) throws android.os.RemoteException
    {
    }
    @Override public void onVolumeStateChanged(java.lang.String volId, int state) throws android.os.RemoteException
    {
    }
    @Override public void onVolumeMetadataChanged(java.lang.String volId, java.lang.String fsType, java.lang.String fsUuid, java.lang.String fsLabel) throws android.os.RemoteException
    {
    }
    @Override public void onVolumePathChanged(java.lang.String volId, java.lang.String path) throws android.os.RemoteException
    {
    }
    @Override public void onVolumeInternalPathChanged(java.lang.String volId, java.lang.String internalPath) throws android.os.RemoteException
    {
    }
    @Override public void onVolumeDestroyed(java.lang.String volId) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.IVoldListener
  {
    private static final java.lang.String DESCRIPTOR = "android.os.IVoldListener";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.IVoldListener interface,
     * generating a proxy if needed.
     */
    public static android.os.IVoldListener asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.IVoldListener))) {
        return ((android.os.IVoldListener)iin);
      }
      return new android.os.IVoldListener.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onDiskCreated:
        {
          return "onDiskCreated";
        }
        case TRANSACTION_onDiskScanned:
        {
          return "onDiskScanned";
        }
        case TRANSACTION_onDiskMetadataChanged:
        {
          return "onDiskMetadataChanged";
        }
        case TRANSACTION_onDiskDestroyed:
        {
          return "onDiskDestroyed";
        }
        case TRANSACTION_onVolumeCreated:
        {
          return "onVolumeCreated";
        }
        case TRANSACTION_onVolumeStateChanged:
        {
          return "onVolumeStateChanged";
        }
        case TRANSACTION_onVolumeMetadataChanged:
        {
          return "onVolumeMetadataChanged";
        }
        case TRANSACTION_onVolumePathChanged:
        {
          return "onVolumePathChanged";
        }
        case TRANSACTION_onVolumeInternalPathChanged:
        {
          return "onVolumeInternalPathChanged";
        }
        case TRANSACTION_onVolumeDestroyed:
        {
          return "onVolumeDestroyed";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onDiskCreated:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          this.onDiskCreated(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onDiskScanned:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.onDiskScanned(_arg0);
          return true;
        }
        case TRANSACTION_onDiskMetadataChanged:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          long _arg1;
          _arg1 = data.readLong();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          this.onDiskMetadataChanged(_arg0, _arg1, _arg2, _arg3);
          return true;
        }
        case TRANSACTION_onDiskDestroyed:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.onDiskDestroyed(_arg0);
          return true;
        }
        case TRANSACTION_onVolumeCreated:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          this.onVolumeCreated(_arg0, _arg1, _arg2, _arg3);
          return true;
        }
        case TRANSACTION_onVolumeStateChanged:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          this.onVolumeStateChanged(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onVolumeMetadataChanged:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          this.onVolumeMetadataChanged(_arg0, _arg1, _arg2, _arg3);
          return true;
        }
        case TRANSACTION_onVolumePathChanged:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.onVolumePathChanged(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onVolumeInternalPathChanged:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.onVolumeInternalPathChanged(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onVolumeDestroyed:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.onVolumeDestroyed(_arg0);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.IVoldListener
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onDiskCreated(java.lang.String diskId, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(diskId);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onDiskCreated, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onDiskCreated(diskId, flags);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onDiskScanned(java.lang.String diskId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(diskId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onDiskScanned, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onDiskScanned(diskId);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onDiskMetadataChanged(java.lang.String diskId, long sizeBytes, java.lang.String label, java.lang.String sysPath) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(diskId);
          _data.writeLong(sizeBytes);
          _data.writeString(label);
          _data.writeString(sysPath);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onDiskMetadataChanged, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onDiskMetadataChanged(diskId, sizeBytes, label, sysPath);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onDiskDestroyed(java.lang.String diskId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(diskId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onDiskDestroyed, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onDiskDestroyed(diskId);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onVolumeCreated(java.lang.String volId, int type, java.lang.String diskId, java.lang.String partGuid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          _data.writeInt(type);
          _data.writeString(diskId);
          _data.writeString(partGuid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onVolumeCreated, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onVolumeCreated(volId, type, diskId, partGuid);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onVolumeStateChanged(java.lang.String volId, int state) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          _data.writeInt(state);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onVolumeStateChanged, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onVolumeStateChanged(volId, state);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onVolumeMetadataChanged(java.lang.String volId, java.lang.String fsType, java.lang.String fsUuid, java.lang.String fsLabel) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          _data.writeString(fsType);
          _data.writeString(fsUuid);
          _data.writeString(fsLabel);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onVolumeMetadataChanged, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onVolumeMetadataChanged(volId, fsType, fsUuid, fsLabel);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onVolumePathChanged(java.lang.String volId, java.lang.String path) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          _data.writeString(path);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onVolumePathChanged, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onVolumePathChanged(volId, path);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onVolumeInternalPathChanged(java.lang.String volId, java.lang.String internalPath) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          _data.writeString(internalPath);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onVolumeInternalPathChanged, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onVolumeInternalPathChanged(volId, internalPath);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onVolumeDestroyed(java.lang.String volId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(volId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onVolumeDestroyed, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onVolumeDestroyed(volId);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.os.IVoldListener sDefaultImpl;
    }
    static final int TRANSACTION_onDiskCreated = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onDiskScanned = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_onDiskMetadataChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_onDiskDestroyed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_onVolumeCreated = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_onVolumeStateChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_onVolumeMetadataChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_onVolumePathChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_onVolumeInternalPathChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_onVolumeDestroyed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    public static boolean setDefaultImpl(android.os.IVoldListener impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.IVoldListener getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public void onDiskCreated(java.lang.String diskId, int flags) throws android.os.RemoteException;
  public void onDiskScanned(java.lang.String diskId) throws android.os.RemoteException;
  public void onDiskMetadataChanged(java.lang.String diskId, long sizeBytes, java.lang.String label, java.lang.String sysPath) throws android.os.RemoteException;
  public void onDiskDestroyed(java.lang.String diskId) throws android.os.RemoteException;
  public void onVolumeCreated(java.lang.String volId, int type, java.lang.String diskId, java.lang.String partGuid) throws android.os.RemoteException;
  public void onVolumeStateChanged(java.lang.String volId, int state) throws android.os.RemoteException;
  public void onVolumeMetadataChanged(java.lang.String volId, java.lang.String fsType, java.lang.String fsUuid, java.lang.String fsLabel) throws android.os.RemoteException;
  public void onVolumePathChanged(java.lang.String volId, java.lang.String path) throws android.os.RemoteException;
  public void onVolumeInternalPathChanged(java.lang.String volId, java.lang.String internalPath) throws android.os.RemoteException;
  public void onVolumeDestroyed(java.lang.String volId) throws android.os.RemoteException;
}
