/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.hardware;
/** @hide */
public interface ICameraServiceListener extends android.os.IInterface
{
  /** Default implementation for ICameraServiceListener. */
  public static class Default implements android.hardware.ICameraServiceListener
  {
    @Override public void onStatusChanged(int status, java.lang.String cameraId) throws android.os.RemoteException
    {
    }
    @Override public void onTorchStatusChanged(int status, java.lang.String cameraId) throws android.os.RemoteException
    {
    }
    /**
         * Notify registered clients about camera access priority changes.
         * Clients which were previously unable to open a certain camera device
         * can retry after receiving this callback.
         */
    @Override public void onCameraAccessPrioritiesChanged() throws android.os.RemoteException
    {
    }
    /**
         * Notify registered clients about cameras being opened/closed.
         * Only clients with android.permission.CAMERA_OPEN_CLOSE_LISTENER permission
         * will receive such callbacks.
         */
    @Override public void onCameraOpened(java.lang.String cameraId, java.lang.String clientPackageId) throws android.os.RemoteException
    {
    }
    @Override public void onCameraClosed(java.lang.String cameraId) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.hardware.ICameraServiceListener
  {
    private static final java.lang.String DESCRIPTOR = "android.hardware.ICameraServiceListener";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.hardware.ICameraServiceListener interface,
     * generating a proxy if needed.
     */
    public static android.hardware.ICameraServiceListener asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.hardware.ICameraServiceListener))) {
        return ((android.hardware.ICameraServiceListener)iin);
      }
      return new android.hardware.ICameraServiceListener.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_onStatusChanged:
        {
          return "onStatusChanged";
        }
        case TRANSACTION_onTorchStatusChanged:
        {
          return "onTorchStatusChanged";
        }
        case TRANSACTION_onCameraAccessPrioritiesChanged:
        {
          return "onCameraAccessPrioritiesChanged";
        }
        case TRANSACTION_onCameraOpened:
        {
          return "onCameraOpened";
        }
        case TRANSACTION_onCameraClosed:
        {
          return "onCameraClosed";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onStatusChanged:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.onStatusChanged(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onTorchStatusChanged:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.onTorchStatusChanged(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onCameraAccessPrioritiesChanged:
        {
          data.enforceInterface(descriptor);
          this.onCameraAccessPrioritiesChanged();
          return true;
        }
        case TRANSACTION_onCameraOpened:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.onCameraOpened(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onCameraClosed:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          this.onCameraClosed(_arg0);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.hardware.ICameraServiceListener
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onStatusChanged(int status, java.lang.String cameraId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(status);
          _data.writeString(cameraId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onStatusChanged, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onStatusChanged(status, cameraId);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onTorchStatusChanged(int status, java.lang.String cameraId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(status);
          _data.writeString(cameraId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onTorchStatusChanged, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onTorchStatusChanged(status, cameraId);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Notify registered clients about camera access priority changes.
           * Clients which were previously unable to open a certain camera device
           * can retry after receiving this callback.
           */
      @Override public void onCameraAccessPrioritiesChanged() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onCameraAccessPrioritiesChanged, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onCameraAccessPrioritiesChanged();
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /**
           * Notify registered clients about cameras being opened/closed.
           * Only clients with android.permission.CAMERA_OPEN_CLOSE_LISTENER permission
           * will receive such callbacks.
           */
      @Override public void onCameraOpened(java.lang.String cameraId, java.lang.String clientPackageId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(cameraId);
          _data.writeString(clientPackageId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onCameraOpened, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onCameraOpened(cameraId, clientPackageId);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onCameraClosed(java.lang.String cameraId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(cameraId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onCameraClosed, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onCameraClosed(cameraId);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.hardware.ICameraServiceListener sDefaultImpl;
    }
    static final int TRANSACTION_onStatusChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onTorchStatusChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_onCameraAccessPrioritiesChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_onCameraOpened = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_onCameraClosed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    public static boolean setDefaultImpl(android.hardware.ICameraServiceListener impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.hardware.ICameraServiceListener getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int STATUS_NOT_PRESENT = 0;
  public static final int STATUS_PRESENT = 1;
  public static final int STATUS_ENUMERATING = 2;
  public static final int STATUS_NOT_AVAILABLE = -2;
  public static final int STATUS_UNKNOWN = -1;
  public static final int TORCH_STATUS_NOT_AVAILABLE = 0;
  public static final int TORCH_STATUS_AVAILABLE_OFF = 1;
  public static final int TORCH_STATUS_AVAILABLE_ON = 2;
  public static final int TORCH_STATUS_UNKNOWN = -1;
  public void onStatusChanged(int status, java.lang.String cameraId) throws android.os.RemoteException;
  public void onTorchStatusChanged(int status, java.lang.String cameraId) throws android.os.RemoteException;
  /**
       * Notify registered clients about camera access priority changes.
       * Clients which were previously unable to open a certain camera device
       * can retry after receiving this callback.
       */
  public void onCameraAccessPrioritiesChanged() throws android.os.RemoteException;
  /**
       * Notify registered clients about cameras being opened/closed.
       * Only clients with android.permission.CAMERA_OPEN_CLOSE_LISTENER permission
       * will receive such callbacks.
       */
  public void onCameraOpened(java.lang.String cameraId, java.lang.String clientPackageId) throws android.os.RemoteException;
  public void onCameraClosed(java.lang.String cameraId) throws android.os.RemoteException;
}
