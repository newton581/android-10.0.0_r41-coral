/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package com.android.internal.app;
public interface IBatteryStats extends android.os.IInterface
{
  /** Default implementation for IBatteryStats. */
  public static class Default implements com.android.internal.app.IBatteryStats
  {
    // These first methods are also called by native code, so must
    // be kept in sync with frameworks/native/libs/binder/include/binder/IBatteryStats.h

    @Override public void noteStartSensor(int uid, int sensor) throws android.os.RemoteException
    {
    }
    @Override public void noteStopSensor(int uid, int sensor) throws android.os.RemoteException
    {
    }
    @Override public void noteStartVideo(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteStopVideo(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteStartAudio(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteStopAudio(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteResetVideo() throws android.os.RemoteException
    {
    }
    @Override public void noteResetAudio() throws android.os.RemoteException
    {
    }
    @Override public void noteFlashlightOn(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteFlashlightOff(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteStartCamera(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteStopCamera(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteResetCamera() throws android.os.RemoteException
    {
    }
    @Override public void noteResetFlashlight() throws android.os.RemoteException
    {
    }
    // Remaining methods are only used in Java.

    @Override public byte[] getStatistics() throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.os.ParcelFileDescriptor getStatisticsStream() throws android.os.RemoteException
    {
      return null;
    }
    // Return true if we see the battery as currently charging.

    @Override public boolean isCharging() throws android.os.RemoteException
    {
      return false;
    }
    // Return the computed amount of time remaining on battery, in milliseconds.
    // Returns -1 if nothing could be computed.

    @Override public long computeBatteryTimeRemaining() throws android.os.RemoteException
    {
      return 0L;
    }
    // Return the computed amount of time remaining to fully charge, in milliseconds.
    // Returns -1 if nothing could be computed.

    @Override public long computeChargeTimeRemaining() throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public void noteEvent(int code, java.lang.String name, int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteSyncStart(java.lang.String name, int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteSyncFinish(java.lang.String name, int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteJobStart(java.lang.String name, int uid, int standbyBucket, int jobid) throws android.os.RemoteException
    {
    }
    @Override public void noteJobFinish(java.lang.String name, int uid, int stopReason, int standbyBucket, int jobid) throws android.os.RemoteException
    {
    }
    @Override public void noteStartWakelock(int uid, int pid, java.lang.String name, java.lang.String historyName, int type, boolean unimportantForLogging) throws android.os.RemoteException
    {
    }
    @Override public void noteStopWakelock(int uid, int pid, java.lang.String name, java.lang.String historyName, int type) throws android.os.RemoteException
    {
    }
    @Override public void noteStartWakelockFromSource(android.os.WorkSource ws, int pid, java.lang.String name, java.lang.String historyName, int type, boolean unimportantForLogging) throws android.os.RemoteException
    {
    }
    @Override public void noteChangeWakelockFromSource(android.os.WorkSource ws, int pid, java.lang.String name, java.lang.String histyoryName, int type, android.os.WorkSource newWs, int newPid, java.lang.String newName, java.lang.String newHistoryName, int newType, boolean newUnimportantForLogging) throws android.os.RemoteException
    {
    }
    @Override public void noteStopWakelockFromSource(android.os.WorkSource ws, int pid, java.lang.String name, java.lang.String historyName, int type) throws android.os.RemoteException
    {
    }
    @Override public void noteLongPartialWakelockStart(java.lang.String name, java.lang.String historyName, int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteLongPartialWakelockStartFromSource(java.lang.String name, java.lang.String historyName, android.os.WorkSource workSource) throws android.os.RemoteException
    {
    }
    @Override public void noteLongPartialWakelockFinish(java.lang.String name, java.lang.String historyName, int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteLongPartialWakelockFinishFromSource(java.lang.String name, java.lang.String historyName, android.os.WorkSource workSource) throws android.os.RemoteException
    {
    }
    @Override public void noteVibratorOn(int uid, long durationMillis) throws android.os.RemoteException
    {
    }
    @Override public void noteVibratorOff(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteGpsChanged(android.os.WorkSource oldSource, android.os.WorkSource newSource) throws android.os.RemoteException
    {
    }
    @Override public void noteGpsSignalQuality(int signalLevel) throws android.os.RemoteException
    {
    }
    @Override public void noteScreenState(int state) throws android.os.RemoteException
    {
    }
    @Override public void noteScreenBrightness(int brightness) throws android.os.RemoteException
    {
    }
    @Override public void noteUserActivity(int uid, int event) throws android.os.RemoteException
    {
    }
    @Override public void noteWakeUp(java.lang.String reason, int reasonUid) throws android.os.RemoteException
    {
    }
    @Override public void noteInteractive(boolean interactive) throws android.os.RemoteException
    {
    }
    @Override public void noteConnectivityChanged(int type, java.lang.String extra) throws android.os.RemoteException
    {
    }
    @Override public void noteMobileRadioPowerState(int powerState, long timestampNs, int uid) throws android.os.RemoteException
    {
    }
    @Override public void notePhoneOn() throws android.os.RemoteException
    {
    }
    @Override public void notePhoneOff() throws android.os.RemoteException
    {
    }
    @Override public void notePhoneSignalStrength(android.telephony.SignalStrength signalStrength) throws android.os.RemoteException
    {
    }
    @Override public void notePhoneDataConnectionState(int dataType, boolean hasData, int serviceType) throws android.os.RemoteException
    {
    }
    @Override public void notePhoneState(int phoneState) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiOn() throws android.os.RemoteException
    {
    }
    @Override public void noteWifiOff() throws android.os.RemoteException
    {
    }
    @Override public void noteWifiRunning(android.os.WorkSource ws) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiRunningChanged(android.os.WorkSource oldWs, android.os.WorkSource newWs) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiStopped(android.os.WorkSource ws) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiState(int wifiState, java.lang.String accessPoint) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiSupplicantStateChanged(int supplState, boolean failedAuth) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiRssiChanged(int newRssi) throws android.os.RemoteException
    {
    }
    @Override public void noteFullWifiLockAcquired(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteFullWifiLockReleased(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiScanStarted(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiScanStopped(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiMulticastEnabled(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiMulticastDisabled(int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteFullWifiLockAcquiredFromSource(android.os.WorkSource ws) throws android.os.RemoteException
    {
    }
    @Override public void noteFullWifiLockReleasedFromSource(android.os.WorkSource ws) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiScanStartedFromSource(android.os.WorkSource ws) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiScanStoppedFromSource(android.os.WorkSource ws) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiBatchedScanStartedFromSource(android.os.WorkSource ws, int csph) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiBatchedScanStoppedFromSource(android.os.WorkSource ws) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiRadioPowerState(int powerState, long timestampNs, int uid) throws android.os.RemoteException
    {
    }
    @Override public void noteNetworkInterfaceType(java.lang.String iface, int type) throws android.os.RemoteException
    {
    }
    @Override public void noteNetworkStatsEnabled() throws android.os.RemoteException
    {
    }
    @Override public void noteDeviceIdleMode(int mode, java.lang.String activeReason, int activeUid) throws android.os.RemoteException
    {
    }
    @Override public void setBatteryState(int status, int health, int plugType, int level, int temp, int volt, int chargeUAh, int chargeFullUAh) throws android.os.RemoteException
    {
    }
    @Override public long getAwakeTimeBattery() throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public long getAwakeTimePlugged() throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public void noteBleScanStarted(android.os.WorkSource ws, boolean isUnoptimized) throws android.os.RemoteException
    {
    }
    @Override public void noteBleScanStopped(android.os.WorkSource ws, boolean isUnoptimized) throws android.os.RemoteException
    {
    }
    @Override public void noteResetBleScan() throws android.os.RemoteException
    {
    }
    @Override public void noteBleScanResults(android.os.WorkSource ws, int numNewResults) throws android.os.RemoteException
    {
    }
    /** {@hide} */
    @Override public android.os.connectivity.CellularBatteryStats getCellularBatteryStats() throws android.os.RemoteException
    {
      return null;
    }
    /** {@hide} */
    @Override public android.os.connectivity.WifiBatteryStats getWifiBatteryStats() throws android.os.RemoteException
    {
      return null;
    }
    /** {@hide} */
    @Override public android.os.connectivity.GpsBatteryStats getGpsBatteryStats() throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.os.health.HealthStatsParceler takeUidSnapshot(int uid) throws android.os.RemoteException
    {
      return null;
    }
    @Override public android.os.health.HealthStatsParceler[] takeUidSnapshots(int[] uid) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void noteBluetoothControllerActivity(android.bluetooth.BluetoothActivityEnergyInfo info) throws android.os.RemoteException
    {
    }
    @Override public void noteModemControllerActivity(android.telephony.ModemActivityInfo info) throws android.os.RemoteException
    {
    }
    @Override public void noteWifiControllerActivity(android.net.wifi.WifiActivityEnergyInfo info) throws android.os.RemoteException
    {
    }
    /** {@hide} */
    @Override public boolean setChargingStateUpdateDelayMillis(int delay) throws android.os.RemoteException
    {
      return false;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements com.android.internal.app.IBatteryStats
  {
    private static final java.lang.String DESCRIPTOR = "com.android.internal.app.IBatteryStats";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an com.android.internal.app.IBatteryStats interface,
     * generating a proxy if needed.
     */
    public static com.android.internal.app.IBatteryStats asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof com.android.internal.app.IBatteryStats))) {
        return ((com.android.internal.app.IBatteryStats)iin);
      }
      return new com.android.internal.app.IBatteryStats.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_noteStartSensor:
        {
          return "noteStartSensor";
        }
        case TRANSACTION_noteStopSensor:
        {
          return "noteStopSensor";
        }
        case TRANSACTION_noteStartVideo:
        {
          return "noteStartVideo";
        }
        case TRANSACTION_noteStopVideo:
        {
          return "noteStopVideo";
        }
        case TRANSACTION_noteStartAudio:
        {
          return "noteStartAudio";
        }
        case TRANSACTION_noteStopAudio:
        {
          return "noteStopAudio";
        }
        case TRANSACTION_noteResetVideo:
        {
          return "noteResetVideo";
        }
        case TRANSACTION_noteResetAudio:
        {
          return "noteResetAudio";
        }
        case TRANSACTION_noteFlashlightOn:
        {
          return "noteFlashlightOn";
        }
        case TRANSACTION_noteFlashlightOff:
        {
          return "noteFlashlightOff";
        }
        case TRANSACTION_noteStartCamera:
        {
          return "noteStartCamera";
        }
        case TRANSACTION_noteStopCamera:
        {
          return "noteStopCamera";
        }
        case TRANSACTION_noteResetCamera:
        {
          return "noteResetCamera";
        }
        case TRANSACTION_noteResetFlashlight:
        {
          return "noteResetFlashlight";
        }
        case TRANSACTION_getStatistics:
        {
          return "getStatistics";
        }
        case TRANSACTION_getStatisticsStream:
        {
          return "getStatisticsStream";
        }
        case TRANSACTION_isCharging:
        {
          return "isCharging";
        }
        case TRANSACTION_computeBatteryTimeRemaining:
        {
          return "computeBatteryTimeRemaining";
        }
        case TRANSACTION_computeChargeTimeRemaining:
        {
          return "computeChargeTimeRemaining";
        }
        case TRANSACTION_noteEvent:
        {
          return "noteEvent";
        }
        case TRANSACTION_noteSyncStart:
        {
          return "noteSyncStart";
        }
        case TRANSACTION_noteSyncFinish:
        {
          return "noteSyncFinish";
        }
        case TRANSACTION_noteJobStart:
        {
          return "noteJobStart";
        }
        case TRANSACTION_noteJobFinish:
        {
          return "noteJobFinish";
        }
        case TRANSACTION_noteStartWakelock:
        {
          return "noteStartWakelock";
        }
        case TRANSACTION_noteStopWakelock:
        {
          return "noteStopWakelock";
        }
        case TRANSACTION_noteStartWakelockFromSource:
        {
          return "noteStartWakelockFromSource";
        }
        case TRANSACTION_noteChangeWakelockFromSource:
        {
          return "noteChangeWakelockFromSource";
        }
        case TRANSACTION_noteStopWakelockFromSource:
        {
          return "noteStopWakelockFromSource";
        }
        case TRANSACTION_noteLongPartialWakelockStart:
        {
          return "noteLongPartialWakelockStart";
        }
        case TRANSACTION_noteLongPartialWakelockStartFromSource:
        {
          return "noteLongPartialWakelockStartFromSource";
        }
        case TRANSACTION_noteLongPartialWakelockFinish:
        {
          return "noteLongPartialWakelockFinish";
        }
        case TRANSACTION_noteLongPartialWakelockFinishFromSource:
        {
          return "noteLongPartialWakelockFinishFromSource";
        }
        case TRANSACTION_noteVibratorOn:
        {
          return "noteVibratorOn";
        }
        case TRANSACTION_noteVibratorOff:
        {
          return "noteVibratorOff";
        }
        case TRANSACTION_noteGpsChanged:
        {
          return "noteGpsChanged";
        }
        case TRANSACTION_noteGpsSignalQuality:
        {
          return "noteGpsSignalQuality";
        }
        case TRANSACTION_noteScreenState:
        {
          return "noteScreenState";
        }
        case TRANSACTION_noteScreenBrightness:
        {
          return "noteScreenBrightness";
        }
        case TRANSACTION_noteUserActivity:
        {
          return "noteUserActivity";
        }
        case TRANSACTION_noteWakeUp:
        {
          return "noteWakeUp";
        }
        case TRANSACTION_noteInteractive:
        {
          return "noteInteractive";
        }
        case TRANSACTION_noteConnectivityChanged:
        {
          return "noteConnectivityChanged";
        }
        case TRANSACTION_noteMobileRadioPowerState:
        {
          return "noteMobileRadioPowerState";
        }
        case TRANSACTION_notePhoneOn:
        {
          return "notePhoneOn";
        }
        case TRANSACTION_notePhoneOff:
        {
          return "notePhoneOff";
        }
        case TRANSACTION_notePhoneSignalStrength:
        {
          return "notePhoneSignalStrength";
        }
        case TRANSACTION_notePhoneDataConnectionState:
        {
          return "notePhoneDataConnectionState";
        }
        case TRANSACTION_notePhoneState:
        {
          return "notePhoneState";
        }
        case TRANSACTION_noteWifiOn:
        {
          return "noteWifiOn";
        }
        case TRANSACTION_noteWifiOff:
        {
          return "noteWifiOff";
        }
        case TRANSACTION_noteWifiRunning:
        {
          return "noteWifiRunning";
        }
        case TRANSACTION_noteWifiRunningChanged:
        {
          return "noteWifiRunningChanged";
        }
        case TRANSACTION_noteWifiStopped:
        {
          return "noteWifiStopped";
        }
        case TRANSACTION_noteWifiState:
        {
          return "noteWifiState";
        }
        case TRANSACTION_noteWifiSupplicantStateChanged:
        {
          return "noteWifiSupplicantStateChanged";
        }
        case TRANSACTION_noteWifiRssiChanged:
        {
          return "noteWifiRssiChanged";
        }
        case TRANSACTION_noteFullWifiLockAcquired:
        {
          return "noteFullWifiLockAcquired";
        }
        case TRANSACTION_noteFullWifiLockReleased:
        {
          return "noteFullWifiLockReleased";
        }
        case TRANSACTION_noteWifiScanStarted:
        {
          return "noteWifiScanStarted";
        }
        case TRANSACTION_noteWifiScanStopped:
        {
          return "noteWifiScanStopped";
        }
        case TRANSACTION_noteWifiMulticastEnabled:
        {
          return "noteWifiMulticastEnabled";
        }
        case TRANSACTION_noteWifiMulticastDisabled:
        {
          return "noteWifiMulticastDisabled";
        }
        case TRANSACTION_noteFullWifiLockAcquiredFromSource:
        {
          return "noteFullWifiLockAcquiredFromSource";
        }
        case TRANSACTION_noteFullWifiLockReleasedFromSource:
        {
          return "noteFullWifiLockReleasedFromSource";
        }
        case TRANSACTION_noteWifiScanStartedFromSource:
        {
          return "noteWifiScanStartedFromSource";
        }
        case TRANSACTION_noteWifiScanStoppedFromSource:
        {
          return "noteWifiScanStoppedFromSource";
        }
        case TRANSACTION_noteWifiBatchedScanStartedFromSource:
        {
          return "noteWifiBatchedScanStartedFromSource";
        }
        case TRANSACTION_noteWifiBatchedScanStoppedFromSource:
        {
          return "noteWifiBatchedScanStoppedFromSource";
        }
        case TRANSACTION_noteWifiRadioPowerState:
        {
          return "noteWifiRadioPowerState";
        }
        case TRANSACTION_noteNetworkInterfaceType:
        {
          return "noteNetworkInterfaceType";
        }
        case TRANSACTION_noteNetworkStatsEnabled:
        {
          return "noteNetworkStatsEnabled";
        }
        case TRANSACTION_noteDeviceIdleMode:
        {
          return "noteDeviceIdleMode";
        }
        case TRANSACTION_setBatteryState:
        {
          return "setBatteryState";
        }
        case TRANSACTION_getAwakeTimeBattery:
        {
          return "getAwakeTimeBattery";
        }
        case TRANSACTION_getAwakeTimePlugged:
        {
          return "getAwakeTimePlugged";
        }
        case TRANSACTION_noteBleScanStarted:
        {
          return "noteBleScanStarted";
        }
        case TRANSACTION_noteBleScanStopped:
        {
          return "noteBleScanStopped";
        }
        case TRANSACTION_noteResetBleScan:
        {
          return "noteResetBleScan";
        }
        case TRANSACTION_noteBleScanResults:
        {
          return "noteBleScanResults";
        }
        case TRANSACTION_getCellularBatteryStats:
        {
          return "getCellularBatteryStats";
        }
        case TRANSACTION_getWifiBatteryStats:
        {
          return "getWifiBatteryStats";
        }
        case TRANSACTION_getGpsBatteryStats:
        {
          return "getGpsBatteryStats";
        }
        case TRANSACTION_takeUidSnapshot:
        {
          return "takeUidSnapshot";
        }
        case TRANSACTION_takeUidSnapshots:
        {
          return "takeUidSnapshots";
        }
        case TRANSACTION_noteBluetoothControllerActivity:
        {
          return "noteBluetoothControllerActivity";
        }
        case TRANSACTION_noteModemControllerActivity:
        {
          return "noteModemControllerActivity";
        }
        case TRANSACTION_noteWifiControllerActivity:
        {
          return "noteWifiControllerActivity";
        }
        case TRANSACTION_setChargingStateUpdateDelayMillis:
        {
          return "setChargingStateUpdateDelayMillis";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_noteStartSensor:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.noteStartSensor(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStopSensor:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.noteStopSensor(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStartVideo:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteStartVideo(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStopVideo:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteStopVideo(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStartAudio:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteStartAudio(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStopAudio:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteStopAudio(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteResetVideo:
        {
          data.enforceInterface(descriptor);
          this.noteResetVideo();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteResetAudio:
        {
          data.enforceInterface(descriptor);
          this.noteResetAudio();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteFlashlightOn:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteFlashlightOn(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteFlashlightOff:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteFlashlightOff(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStartCamera:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteStartCamera(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStopCamera:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteStopCamera(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteResetCamera:
        {
          data.enforceInterface(descriptor);
          this.noteResetCamera();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteResetFlashlight:
        {
          data.enforceInterface(descriptor);
          this.noteResetFlashlight();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getStatistics:
        {
          data.enforceInterface(descriptor);
          byte[] _result = this.getStatistics();
          reply.writeNoException();
          reply.writeByteArray(_result);
          return true;
        }
        case TRANSACTION_getStatisticsStream:
        {
          data.enforceInterface(descriptor);
          android.os.ParcelFileDescriptor _result = this.getStatisticsStream();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_isCharging:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isCharging();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_computeBatteryTimeRemaining:
        {
          data.enforceInterface(descriptor);
          long _result = this.computeBatteryTimeRemaining();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_computeChargeTimeRemaining:
        {
          data.enforceInterface(descriptor);
          long _result = this.computeChargeTimeRemaining();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_noteEvent:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          this.noteEvent(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteSyncStart:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          this.noteSyncStart(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteSyncFinish:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          this.noteSyncFinish(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteJobStart:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          this.noteJobStart(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteJobFinish:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          int _arg4;
          _arg4 = data.readInt();
          this.noteJobFinish(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStartWakelock:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          boolean _arg5;
          _arg5 = (0!=data.readInt());
          this.noteStartWakelock(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStopWakelock:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          this.noteStopWakelock(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStartWakelockFromSource:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          boolean _arg5;
          _arg5 = (0!=data.readInt());
          this.noteStartWakelockFromSource(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteChangeWakelockFromSource:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          android.os.WorkSource _arg5;
          if ((0!=data.readInt())) {
            _arg5 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg5 = null;
          }
          int _arg6;
          _arg6 = data.readInt();
          java.lang.String _arg7;
          _arg7 = data.readString();
          java.lang.String _arg8;
          _arg8 = data.readString();
          int _arg9;
          _arg9 = data.readInt();
          boolean _arg10;
          _arg10 = (0!=data.readInt());
          this.noteChangeWakelockFromSource(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg7, _arg8, _arg9, _arg10);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteStopWakelockFromSource:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _arg2;
          _arg2 = data.readString();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          this.noteStopWakelockFromSource(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteLongPartialWakelockStart:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          this.noteLongPartialWakelockStart(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteLongPartialWakelockStartFromSource:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.os.WorkSource _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          this.noteLongPartialWakelockStartFromSource(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteLongPartialWakelockFinish:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          this.noteLongPartialWakelockFinish(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteLongPartialWakelockFinishFromSource:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.os.WorkSource _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          this.noteLongPartialWakelockFinishFromSource(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteVibratorOn:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          long _arg1;
          _arg1 = data.readLong();
          this.noteVibratorOn(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteVibratorOff:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteVibratorOff(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteGpsChanged:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.WorkSource _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.noteGpsChanged(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteGpsSignalQuality:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteGpsSignalQuality(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteScreenState:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteScreenState(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteScreenBrightness:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteScreenBrightness(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteUserActivity:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          this.noteUserActivity(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWakeUp:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          this.noteWakeUp(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteInteractive:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          this.noteInteractive(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteConnectivityChanged:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.noteConnectivityChanged(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteMobileRadioPowerState:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          long _arg1;
          _arg1 = data.readLong();
          int _arg2;
          _arg2 = data.readInt();
          this.noteMobileRadioPowerState(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_notePhoneOn:
        {
          data.enforceInterface(descriptor);
          this.notePhoneOn();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_notePhoneOff:
        {
          data.enforceInterface(descriptor);
          this.notePhoneOff();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_notePhoneSignalStrength:
        {
          data.enforceInterface(descriptor);
          android.telephony.SignalStrength _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.telephony.SignalStrength.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.notePhoneSignalStrength(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_notePhoneDataConnectionState:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          int _arg2;
          _arg2 = data.readInt();
          this.notePhoneDataConnectionState(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_notePhoneState:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.notePhoneState(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiOn:
        {
          data.enforceInterface(descriptor);
          this.noteWifiOn();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiOff:
        {
          data.enforceInterface(descriptor);
          this.noteWifiOff();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiRunning:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.noteWifiRunning(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiRunningChanged:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          android.os.WorkSource _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.noteWifiRunningChanged(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiStopped:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.noteWifiStopped(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiState:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          this.noteWifiState(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiSupplicantStateChanged:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.noteWifiSupplicantStateChanged(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiRssiChanged:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteWifiRssiChanged(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteFullWifiLockAcquired:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteFullWifiLockAcquired(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteFullWifiLockReleased:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteFullWifiLockReleased(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiScanStarted:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteWifiScanStarted(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiScanStopped:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteWifiScanStopped(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiMulticastEnabled:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteWifiMulticastEnabled(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiMulticastDisabled:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.noteWifiMulticastDisabled(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteFullWifiLockAcquiredFromSource:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.noteFullWifiLockAcquiredFromSource(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteFullWifiLockReleasedFromSource:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.noteFullWifiLockReleasedFromSource(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiScanStartedFromSource:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.noteWifiScanStartedFromSource(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiScanStoppedFromSource:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.noteWifiScanStoppedFromSource(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiBatchedScanStartedFromSource:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          this.noteWifiBatchedScanStartedFromSource(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiBatchedScanStoppedFromSource:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.noteWifiBatchedScanStoppedFromSource(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteWifiRadioPowerState:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          long _arg1;
          _arg1 = data.readLong();
          int _arg2;
          _arg2 = data.readInt();
          this.noteWifiRadioPowerState(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteNetworkInterfaceType:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          this.noteNetworkInterfaceType(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteNetworkStatsEnabled:
        {
          data.enforceInterface(descriptor);
          this.noteNetworkStatsEnabled();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteDeviceIdleMode:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          this.noteDeviceIdleMode(_arg0, _arg1, _arg2);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_setBatteryState:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          int _arg4;
          _arg4 = data.readInt();
          int _arg5;
          _arg5 = data.readInt();
          int _arg6;
          _arg6 = data.readInt();
          int _arg7;
          _arg7 = data.readInt();
          this.setBatteryState(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg7);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getAwakeTimeBattery:
        {
          data.enforceInterface(descriptor);
          long _result = this.getAwakeTimeBattery();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_getAwakeTimePlugged:
        {
          data.enforceInterface(descriptor);
          long _result = this.getAwakeTimePlugged();
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_noteBleScanStarted:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.noteBleScanStarted(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteBleScanStopped:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          this.noteBleScanStopped(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteResetBleScan:
        {
          data.enforceInterface(descriptor);
          this.noteResetBleScan();
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_noteBleScanResults:
        {
          data.enforceInterface(descriptor);
          android.os.WorkSource _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.os.WorkSource.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          int _arg1;
          _arg1 = data.readInt();
          this.noteBleScanResults(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getCellularBatteryStats:
        {
          data.enforceInterface(descriptor);
          android.os.connectivity.CellularBatteryStats _result = this.getCellularBatteryStats();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getWifiBatteryStats:
        {
          data.enforceInterface(descriptor);
          android.os.connectivity.WifiBatteryStats _result = this.getWifiBatteryStats();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_getGpsBatteryStats:
        {
          data.enforceInterface(descriptor);
          android.os.connectivity.GpsBatteryStats _result = this.getGpsBatteryStats();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_takeUidSnapshot:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.os.health.HealthStatsParceler _result = this.takeUidSnapshot(_arg0);
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_takeUidSnapshots:
        {
          data.enforceInterface(descriptor);
          int[] _arg0;
          _arg0 = data.createIntArray();
          android.os.health.HealthStatsParceler[] _result = this.takeUidSnapshots(_arg0);
          reply.writeNoException();
          reply.writeTypedArray(_result, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          return true;
        }
        case TRANSACTION_noteBluetoothControllerActivity:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.BluetoothActivityEnergyInfo _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.bluetooth.BluetoothActivityEnergyInfo.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.noteBluetoothControllerActivity(_arg0);
          return true;
        }
        case TRANSACTION_noteModemControllerActivity:
        {
          data.enforceInterface(descriptor);
          android.telephony.ModemActivityInfo _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.telephony.ModemActivityInfo.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.noteModemControllerActivity(_arg0);
          return true;
        }
        case TRANSACTION_noteWifiControllerActivity:
        {
          data.enforceInterface(descriptor);
          android.net.wifi.WifiActivityEnergyInfo _arg0;
          if ((0!=data.readInt())) {
            _arg0 = android.net.wifi.WifiActivityEnergyInfo.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          this.noteWifiControllerActivity(_arg0);
          return true;
        }
        case TRANSACTION_setChargingStateUpdateDelayMillis:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          boolean _result = this.setChargingStateUpdateDelayMillis(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements com.android.internal.app.IBatteryStats
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      // These first methods are also called by native code, so must
      // be kept in sync with frameworks/native/libs/binder/include/binder/IBatteryStats.h

      @Override public void noteStartSensor(int uid, int sensor) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeInt(sensor);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStartSensor, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStartSensor(uid, sensor);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStopSensor(int uid, int sensor) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeInt(sensor);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStopSensor, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStopSensor(uid, sensor);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStartVideo(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStartVideo, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStartVideo(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStopVideo(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStopVideo, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStopVideo(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStartAudio(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStartAudio, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStartAudio(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStopAudio(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStopAudio, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStopAudio(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteResetVideo() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteResetVideo, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteResetVideo();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteResetAudio() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteResetAudio, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteResetAudio();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteFlashlightOn(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteFlashlightOn, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteFlashlightOn(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteFlashlightOff(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteFlashlightOff, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteFlashlightOff(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStartCamera(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStartCamera, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStartCamera(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStopCamera(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStopCamera, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStopCamera(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteResetCamera() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteResetCamera, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteResetCamera();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteResetFlashlight() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteResetFlashlight, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteResetFlashlight();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      // Remaining methods are only used in Java.

      @Override public byte[] getStatistics() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        byte[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getStatistics, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getStatistics();
          }
          _reply.readException();
          _result = _reply.createByteArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.os.ParcelFileDescriptor getStatisticsStream() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.ParcelFileDescriptor _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getStatisticsStream, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getStatisticsStream();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.ParcelFileDescriptor.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // Return true if we see the battery as currently charging.

      @Override public boolean isCharging() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isCharging, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isCharging();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // Return the computed amount of time remaining on battery, in milliseconds.
      // Returns -1 if nothing could be computed.

      @Override public long computeBatteryTimeRemaining() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_computeBatteryTimeRemaining, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().computeBatteryTimeRemaining();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // Return the computed amount of time remaining to fully charge, in milliseconds.
      // Returns -1 if nothing could be computed.

      @Override public long computeChargeTimeRemaining() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_computeChargeTimeRemaining, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().computeChargeTimeRemaining();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void noteEvent(int code, java.lang.String name, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(code);
          _data.writeString(name);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteEvent, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteEvent(code, name, uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteSyncStart(java.lang.String name, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteSyncStart, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteSyncStart(name, uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteSyncFinish(java.lang.String name, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteSyncFinish, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteSyncFinish(name, uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteJobStart(java.lang.String name, int uid, int standbyBucket, int jobid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(uid);
          _data.writeInt(standbyBucket);
          _data.writeInt(jobid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteJobStart, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteJobStart(name, uid, standbyBucket, jobid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteJobFinish(java.lang.String name, int uid, int stopReason, int standbyBucket, int jobid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(uid);
          _data.writeInt(stopReason);
          _data.writeInt(standbyBucket);
          _data.writeInt(jobid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteJobFinish, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteJobFinish(name, uid, stopReason, standbyBucket, jobid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStartWakelock(int uid, int pid, java.lang.String name, java.lang.String historyName, int type, boolean unimportantForLogging) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeInt(pid);
          _data.writeString(name);
          _data.writeString(historyName);
          _data.writeInt(type);
          _data.writeInt(((unimportantForLogging)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStartWakelock, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStartWakelock(uid, pid, name, historyName, type, unimportantForLogging);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStopWakelock(int uid, int pid, java.lang.String name, java.lang.String historyName, int type) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeInt(pid);
          _data.writeString(name);
          _data.writeString(historyName);
          _data.writeInt(type);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStopWakelock, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStopWakelock(uid, pid, name, historyName, type);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStartWakelockFromSource(android.os.WorkSource ws, int pid, java.lang.String name, java.lang.String historyName, int type, boolean unimportantForLogging) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(pid);
          _data.writeString(name);
          _data.writeString(historyName);
          _data.writeInt(type);
          _data.writeInt(((unimportantForLogging)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStartWakelockFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStartWakelockFromSource(ws, pid, name, historyName, type, unimportantForLogging);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteChangeWakelockFromSource(android.os.WorkSource ws, int pid, java.lang.String name, java.lang.String histyoryName, int type, android.os.WorkSource newWs, int newPid, java.lang.String newName, java.lang.String newHistoryName, int newType, boolean newUnimportantForLogging) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(pid);
          _data.writeString(name);
          _data.writeString(histyoryName);
          _data.writeInt(type);
          if ((newWs!=null)) {
            _data.writeInt(1);
            newWs.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(newPid);
          _data.writeString(newName);
          _data.writeString(newHistoryName);
          _data.writeInt(newType);
          _data.writeInt(((newUnimportantForLogging)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteChangeWakelockFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteChangeWakelockFromSource(ws, pid, name, histyoryName, type, newWs, newPid, newName, newHistoryName, newType, newUnimportantForLogging);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteStopWakelockFromSource(android.os.WorkSource ws, int pid, java.lang.String name, java.lang.String historyName, int type) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(pid);
          _data.writeString(name);
          _data.writeString(historyName);
          _data.writeInt(type);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteStopWakelockFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteStopWakelockFromSource(ws, pid, name, historyName, type);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteLongPartialWakelockStart(java.lang.String name, java.lang.String historyName, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeString(historyName);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteLongPartialWakelockStart, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteLongPartialWakelockStart(name, historyName, uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteLongPartialWakelockStartFromSource(java.lang.String name, java.lang.String historyName, android.os.WorkSource workSource) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeString(historyName);
          if ((workSource!=null)) {
            _data.writeInt(1);
            workSource.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteLongPartialWakelockStartFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteLongPartialWakelockStartFromSource(name, historyName, workSource);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteLongPartialWakelockFinish(java.lang.String name, java.lang.String historyName, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeString(historyName);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteLongPartialWakelockFinish, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteLongPartialWakelockFinish(name, historyName, uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteLongPartialWakelockFinishFromSource(java.lang.String name, java.lang.String historyName, android.os.WorkSource workSource) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeString(historyName);
          if ((workSource!=null)) {
            _data.writeInt(1);
            workSource.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteLongPartialWakelockFinishFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteLongPartialWakelockFinishFromSource(name, historyName, workSource);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteVibratorOn(int uid, long durationMillis) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeLong(durationMillis);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteVibratorOn, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteVibratorOn(uid, durationMillis);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteVibratorOff(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteVibratorOff, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteVibratorOff(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteGpsChanged(android.os.WorkSource oldSource, android.os.WorkSource newSource) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((oldSource!=null)) {
            _data.writeInt(1);
            oldSource.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((newSource!=null)) {
            _data.writeInt(1);
            newSource.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteGpsChanged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteGpsChanged(oldSource, newSource);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteGpsSignalQuality(int signalLevel) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(signalLevel);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteGpsSignalQuality, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteGpsSignalQuality(signalLevel);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteScreenState(int state) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(state);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteScreenState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteScreenState(state);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteScreenBrightness(int brightness) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(brightness);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteScreenBrightness, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteScreenBrightness(brightness);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteUserActivity(int uid, int event) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          _data.writeInt(event);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteUserActivity, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteUserActivity(uid, event);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWakeUp(java.lang.String reason, int reasonUid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(reason);
          _data.writeInt(reasonUid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWakeUp, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWakeUp(reason, reasonUid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteInteractive(boolean interactive) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((interactive)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteInteractive, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteInteractive(interactive);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteConnectivityChanged(int type, java.lang.String extra) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(type);
          _data.writeString(extra);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteConnectivityChanged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteConnectivityChanged(type, extra);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteMobileRadioPowerState(int powerState, long timestampNs, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(powerState);
          _data.writeLong(timestampNs);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteMobileRadioPowerState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteMobileRadioPowerState(powerState, timestampNs, uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void notePhoneOn() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_notePhoneOn, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().notePhoneOn();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void notePhoneOff() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_notePhoneOff, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().notePhoneOff();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void notePhoneSignalStrength(android.telephony.SignalStrength signalStrength) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((signalStrength!=null)) {
            _data.writeInt(1);
            signalStrength.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_notePhoneSignalStrength, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().notePhoneSignalStrength(signalStrength);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void notePhoneDataConnectionState(int dataType, boolean hasData, int serviceType) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(dataType);
          _data.writeInt(((hasData)?(1):(0)));
          _data.writeInt(serviceType);
          boolean _status = mRemote.transact(Stub.TRANSACTION_notePhoneDataConnectionState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().notePhoneDataConnectionState(dataType, hasData, serviceType);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void notePhoneState(int phoneState) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(phoneState);
          boolean _status = mRemote.transact(Stub.TRANSACTION_notePhoneState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().notePhoneState(phoneState);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiOn() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiOn, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiOn();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiOff() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiOff, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiOff();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiRunning(android.os.WorkSource ws) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiRunning, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiRunning(ws);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiRunningChanged(android.os.WorkSource oldWs, android.os.WorkSource newWs) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((oldWs!=null)) {
            _data.writeInt(1);
            oldWs.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((newWs!=null)) {
            _data.writeInt(1);
            newWs.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiRunningChanged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiRunningChanged(oldWs, newWs);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiStopped(android.os.WorkSource ws) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiStopped, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiStopped(ws);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiState(int wifiState, java.lang.String accessPoint) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(wifiState);
          _data.writeString(accessPoint);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiState(wifiState, accessPoint);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiSupplicantStateChanged(int supplState, boolean failedAuth) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(supplState);
          _data.writeInt(((failedAuth)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiSupplicantStateChanged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiSupplicantStateChanged(supplState, failedAuth);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiRssiChanged(int newRssi) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(newRssi);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiRssiChanged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiRssiChanged(newRssi);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteFullWifiLockAcquired(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteFullWifiLockAcquired, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteFullWifiLockAcquired(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteFullWifiLockReleased(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteFullWifiLockReleased, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteFullWifiLockReleased(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiScanStarted(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiScanStarted, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiScanStarted(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiScanStopped(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiScanStopped, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiScanStopped(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiMulticastEnabled(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiMulticastEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiMulticastEnabled(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiMulticastDisabled(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiMulticastDisabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiMulticastDisabled(uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteFullWifiLockAcquiredFromSource(android.os.WorkSource ws) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteFullWifiLockAcquiredFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteFullWifiLockAcquiredFromSource(ws);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteFullWifiLockReleasedFromSource(android.os.WorkSource ws) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteFullWifiLockReleasedFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteFullWifiLockReleasedFromSource(ws);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiScanStartedFromSource(android.os.WorkSource ws) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiScanStartedFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiScanStartedFromSource(ws);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiScanStoppedFromSource(android.os.WorkSource ws) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiScanStoppedFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiScanStoppedFromSource(ws);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiBatchedScanStartedFromSource(android.os.WorkSource ws, int csph) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(csph);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiBatchedScanStartedFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiBatchedScanStartedFromSource(ws, csph);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiBatchedScanStoppedFromSource(android.os.WorkSource ws) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiBatchedScanStoppedFromSource, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiBatchedScanStoppedFromSource(ws);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteWifiRadioPowerState(int powerState, long timestampNs, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(powerState);
          _data.writeLong(timestampNs);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiRadioPowerState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiRadioPowerState(powerState, timestampNs, uid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteNetworkInterfaceType(java.lang.String iface, int type) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(iface);
          _data.writeInt(type);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteNetworkInterfaceType, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteNetworkInterfaceType(iface, type);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteNetworkStatsEnabled() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteNetworkStatsEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteNetworkStatsEnabled();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteDeviceIdleMode(int mode, java.lang.String activeReason, int activeUid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(mode);
          _data.writeString(activeReason);
          _data.writeInt(activeUid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteDeviceIdleMode, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteDeviceIdleMode(mode, activeReason, activeUid);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void setBatteryState(int status, int health, int plugType, int level, int temp, int volt, int chargeUAh, int chargeFullUAh) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(status);
          _data.writeInt(health);
          _data.writeInt(plugType);
          _data.writeInt(level);
          _data.writeInt(temp);
          _data.writeInt(volt);
          _data.writeInt(chargeUAh);
          _data.writeInt(chargeFullUAh);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setBatteryState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setBatteryState(status, health, plugType, level, temp, volt, chargeUAh, chargeFullUAh);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public long getAwakeTimeBattery() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAwakeTimeBattery, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAwakeTimeBattery();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long getAwakeTimePlugged() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAwakeTimePlugged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAwakeTimePlugged();
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void noteBleScanStarted(android.os.WorkSource ws, boolean isUnoptimized) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((isUnoptimized)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteBleScanStarted, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteBleScanStarted(ws, isUnoptimized);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteBleScanStopped(android.os.WorkSource ws, boolean isUnoptimized) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(((isUnoptimized)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteBleScanStopped, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteBleScanStopped(ws, isUnoptimized);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteResetBleScan() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteResetBleScan, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteResetBleScan();
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void noteBleScanResults(android.os.WorkSource ws, int numNewResults) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((ws!=null)) {
            _data.writeInt(1);
            ws.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(numNewResults);
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteBleScanResults, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteBleScanResults(ws, numNewResults);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      /** {@hide} */
      @Override public android.os.connectivity.CellularBatteryStats getCellularBatteryStats() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.connectivity.CellularBatteryStats _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getCellularBatteryStats, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getCellularBatteryStats();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.connectivity.CellularBatteryStats.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /** {@hide} */
      @Override public android.os.connectivity.WifiBatteryStats getWifiBatteryStats() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.connectivity.WifiBatteryStats _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getWifiBatteryStats, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getWifiBatteryStats();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.connectivity.WifiBatteryStats.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /** {@hide} */
      @Override public android.os.connectivity.GpsBatteryStats getGpsBatteryStats() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.connectivity.GpsBatteryStats _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getGpsBatteryStats, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getGpsBatteryStats();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.connectivity.GpsBatteryStats.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.os.health.HealthStatsParceler takeUidSnapshot(int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.health.HealthStatsParceler _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_takeUidSnapshot, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().takeUidSnapshot(uid);
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.os.health.HealthStatsParceler.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.os.health.HealthStatsParceler[] takeUidSnapshots(int[] uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.os.health.HealthStatsParceler[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeIntArray(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_takeUidSnapshots, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().takeUidSnapshots(uid);
          }
          _reply.readException();
          _result = _reply.createTypedArray(android.os.health.HealthStatsParceler.CREATOR);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void noteBluetoothControllerActivity(android.bluetooth.BluetoothActivityEnergyInfo info) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((info!=null)) {
            _data.writeInt(1);
            info.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteBluetoothControllerActivity, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteBluetoothControllerActivity(info);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void noteModemControllerActivity(android.telephony.ModemActivityInfo info) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((info!=null)) {
            _data.writeInt(1);
            info.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteModemControllerActivity, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteModemControllerActivity(info);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void noteWifiControllerActivity(android.net.wifi.WifiActivityEnergyInfo info) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((info!=null)) {
            _data.writeInt(1);
            info.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_noteWifiControllerActivity, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().noteWifiControllerActivity(info);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      /** {@hide} */
      @Override public boolean setChargingStateUpdateDelayMillis(int delay) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(delay);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setChargingStateUpdateDelayMillis, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setChargingStateUpdateDelayMillis(delay);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static com.android.internal.app.IBatteryStats sDefaultImpl;
    }
    static final int TRANSACTION_noteStartSensor = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_noteStopSensor = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_noteStartVideo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_noteStopVideo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_noteStartAudio = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_noteStopAudio = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_noteResetVideo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_noteResetAudio = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_noteFlashlightOn = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_noteFlashlightOff = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_noteStartCamera = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_noteStopCamera = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_noteResetCamera = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_noteResetFlashlight = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_getStatistics = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_getStatisticsStream = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    static final int TRANSACTION_isCharging = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
    static final int TRANSACTION_computeBatteryTimeRemaining = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
    static final int TRANSACTION_computeChargeTimeRemaining = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
    static final int TRANSACTION_noteEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
    static final int TRANSACTION_noteSyncStart = (android.os.IBinder.FIRST_CALL_TRANSACTION + 20);
    static final int TRANSACTION_noteSyncFinish = (android.os.IBinder.FIRST_CALL_TRANSACTION + 21);
    static final int TRANSACTION_noteJobStart = (android.os.IBinder.FIRST_CALL_TRANSACTION + 22);
    static final int TRANSACTION_noteJobFinish = (android.os.IBinder.FIRST_CALL_TRANSACTION + 23);
    static final int TRANSACTION_noteStartWakelock = (android.os.IBinder.FIRST_CALL_TRANSACTION + 24);
    static final int TRANSACTION_noteStopWakelock = (android.os.IBinder.FIRST_CALL_TRANSACTION + 25);
    static final int TRANSACTION_noteStartWakelockFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 26);
    static final int TRANSACTION_noteChangeWakelockFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 27);
    static final int TRANSACTION_noteStopWakelockFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 28);
    static final int TRANSACTION_noteLongPartialWakelockStart = (android.os.IBinder.FIRST_CALL_TRANSACTION + 29);
    static final int TRANSACTION_noteLongPartialWakelockStartFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 30);
    static final int TRANSACTION_noteLongPartialWakelockFinish = (android.os.IBinder.FIRST_CALL_TRANSACTION + 31);
    static final int TRANSACTION_noteLongPartialWakelockFinishFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 32);
    static final int TRANSACTION_noteVibratorOn = (android.os.IBinder.FIRST_CALL_TRANSACTION + 33);
    static final int TRANSACTION_noteVibratorOff = (android.os.IBinder.FIRST_CALL_TRANSACTION + 34);
    static final int TRANSACTION_noteGpsChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 35);
    static final int TRANSACTION_noteGpsSignalQuality = (android.os.IBinder.FIRST_CALL_TRANSACTION + 36);
    static final int TRANSACTION_noteScreenState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 37);
    static final int TRANSACTION_noteScreenBrightness = (android.os.IBinder.FIRST_CALL_TRANSACTION + 38);
    static final int TRANSACTION_noteUserActivity = (android.os.IBinder.FIRST_CALL_TRANSACTION + 39);
    static final int TRANSACTION_noteWakeUp = (android.os.IBinder.FIRST_CALL_TRANSACTION + 40);
    static final int TRANSACTION_noteInteractive = (android.os.IBinder.FIRST_CALL_TRANSACTION + 41);
    static final int TRANSACTION_noteConnectivityChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 42);
    static final int TRANSACTION_noteMobileRadioPowerState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 43);
    static final int TRANSACTION_notePhoneOn = (android.os.IBinder.FIRST_CALL_TRANSACTION + 44);
    static final int TRANSACTION_notePhoneOff = (android.os.IBinder.FIRST_CALL_TRANSACTION + 45);
    static final int TRANSACTION_notePhoneSignalStrength = (android.os.IBinder.FIRST_CALL_TRANSACTION + 46);
    static final int TRANSACTION_notePhoneDataConnectionState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 47);
    static final int TRANSACTION_notePhoneState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 48);
    static final int TRANSACTION_noteWifiOn = (android.os.IBinder.FIRST_CALL_TRANSACTION + 49);
    static final int TRANSACTION_noteWifiOff = (android.os.IBinder.FIRST_CALL_TRANSACTION + 50);
    static final int TRANSACTION_noteWifiRunning = (android.os.IBinder.FIRST_CALL_TRANSACTION + 51);
    static final int TRANSACTION_noteWifiRunningChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 52);
    static final int TRANSACTION_noteWifiStopped = (android.os.IBinder.FIRST_CALL_TRANSACTION + 53);
    static final int TRANSACTION_noteWifiState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 54);
    static final int TRANSACTION_noteWifiSupplicantStateChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 55);
    static final int TRANSACTION_noteWifiRssiChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 56);
    static final int TRANSACTION_noteFullWifiLockAcquired = (android.os.IBinder.FIRST_CALL_TRANSACTION + 57);
    static final int TRANSACTION_noteFullWifiLockReleased = (android.os.IBinder.FIRST_CALL_TRANSACTION + 58);
    static final int TRANSACTION_noteWifiScanStarted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 59);
    static final int TRANSACTION_noteWifiScanStopped = (android.os.IBinder.FIRST_CALL_TRANSACTION + 60);
    static final int TRANSACTION_noteWifiMulticastEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 61);
    static final int TRANSACTION_noteWifiMulticastDisabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 62);
    static final int TRANSACTION_noteFullWifiLockAcquiredFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 63);
    static final int TRANSACTION_noteFullWifiLockReleasedFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 64);
    static final int TRANSACTION_noteWifiScanStartedFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 65);
    static final int TRANSACTION_noteWifiScanStoppedFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 66);
    static final int TRANSACTION_noteWifiBatchedScanStartedFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 67);
    static final int TRANSACTION_noteWifiBatchedScanStoppedFromSource = (android.os.IBinder.FIRST_CALL_TRANSACTION + 68);
    static final int TRANSACTION_noteWifiRadioPowerState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 69);
    static final int TRANSACTION_noteNetworkInterfaceType = (android.os.IBinder.FIRST_CALL_TRANSACTION + 70);
    static final int TRANSACTION_noteNetworkStatsEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 71);
    static final int TRANSACTION_noteDeviceIdleMode = (android.os.IBinder.FIRST_CALL_TRANSACTION + 72);
    static final int TRANSACTION_setBatteryState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 73);
    static final int TRANSACTION_getAwakeTimeBattery = (android.os.IBinder.FIRST_CALL_TRANSACTION + 74);
    static final int TRANSACTION_getAwakeTimePlugged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 75);
    static final int TRANSACTION_noteBleScanStarted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 76);
    static final int TRANSACTION_noteBleScanStopped = (android.os.IBinder.FIRST_CALL_TRANSACTION + 77);
    static final int TRANSACTION_noteResetBleScan = (android.os.IBinder.FIRST_CALL_TRANSACTION + 78);
    static final int TRANSACTION_noteBleScanResults = (android.os.IBinder.FIRST_CALL_TRANSACTION + 79);
    static final int TRANSACTION_getCellularBatteryStats = (android.os.IBinder.FIRST_CALL_TRANSACTION + 80);
    static final int TRANSACTION_getWifiBatteryStats = (android.os.IBinder.FIRST_CALL_TRANSACTION + 81);
    static final int TRANSACTION_getGpsBatteryStats = (android.os.IBinder.FIRST_CALL_TRANSACTION + 82);
    static final int TRANSACTION_takeUidSnapshot = (android.os.IBinder.FIRST_CALL_TRANSACTION + 83);
    static final int TRANSACTION_takeUidSnapshots = (android.os.IBinder.FIRST_CALL_TRANSACTION + 84);
    static final int TRANSACTION_noteBluetoothControllerActivity = (android.os.IBinder.FIRST_CALL_TRANSACTION + 85);
    static final int TRANSACTION_noteModemControllerActivity = (android.os.IBinder.FIRST_CALL_TRANSACTION + 86);
    static final int TRANSACTION_noteWifiControllerActivity = (android.os.IBinder.FIRST_CALL_TRANSACTION + 87);
    static final int TRANSACTION_setChargingStateUpdateDelayMillis = (android.os.IBinder.FIRST_CALL_TRANSACTION + 88);
    public static boolean setDefaultImpl(com.android.internal.app.IBatteryStats impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static com.android.internal.app.IBatteryStats getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  // These first methods are also called by native code, so must
  // be kept in sync with frameworks/native/libs/binder/include/binder/IBatteryStats.h

  public void noteStartSensor(int uid, int sensor) throws android.os.RemoteException;
  public void noteStopSensor(int uid, int sensor) throws android.os.RemoteException;
  public void noteStartVideo(int uid) throws android.os.RemoteException;
  public void noteStopVideo(int uid) throws android.os.RemoteException;
  public void noteStartAudio(int uid) throws android.os.RemoteException;
  public void noteStopAudio(int uid) throws android.os.RemoteException;
  public void noteResetVideo() throws android.os.RemoteException;
  public void noteResetAudio() throws android.os.RemoteException;
  public void noteFlashlightOn(int uid) throws android.os.RemoteException;
  public void noteFlashlightOff(int uid) throws android.os.RemoteException;
  public void noteStartCamera(int uid) throws android.os.RemoteException;
  public void noteStopCamera(int uid) throws android.os.RemoteException;
  public void noteResetCamera() throws android.os.RemoteException;
  public void noteResetFlashlight() throws android.os.RemoteException;
  // Remaining methods are only used in Java.

  @android.annotation.UnsupportedAppUsage
  public byte[] getStatistics() throws android.os.RemoteException;
  public android.os.ParcelFileDescriptor getStatisticsStream() throws android.os.RemoteException;
  // Return true if we see the battery as currently charging.

  @android.annotation.UnsupportedAppUsage
  public boolean isCharging() throws android.os.RemoteException;
  // Return the computed amount of time remaining on battery, in milliseconds.
  // Returns -1 if nothing could be computed.

  public long computeBatteryTimeRemaining() throws android.os.RemoteException;
  // Return the computed amount of time remaining to fully charge, in milliseconds.
  // Returns -1 if nothing could be computed.

  @android.annotation.UnsupportedAppUsage
  public long computeChargeTimeRemaining() throws android.os.RemoteException;
  public void noteEvent(int code, java.lang.String name, int uid) throws android.os.RemoteException;
  public void noteSyncStart(java.lang.String name, int uid) throws android.os.RemoteException;
  public void noteSyncFinish(java.lang.String name, int uid) throws android.os.RemoteException;
  public void noteJobStart(java.lang.String name, int uid, int standbyBucket, int jobid) throws android.os.RemoteException;
  public void noteJobFinish(java.lang.String name, int uid, int stopReason, int standbyBucket, int jobid) throws android.os.RemoteException;
  public void noteStartWakelock(int uid, int pid, java.lang.String name, java.lang.String historyName, int type, boolean unimportantForLogging) throws android.os.RemoteException;
  public void noteStopWakelock(int uid, int pid, java.lang.String name, java.lang.String historyName, int type) throws android.os.RemoteException;
  public void noteStartWakelockFromSource(android.os.WorkSource ws, int pid, java.lang.String name, java.lang.String historyName, int type, boolean unimportantForLogging) throws android.os.RemoteException;
  public void noteChangeWakelockFromSource(android.os.WorkSource ws, int pid, java.lang.String name, java.lang.String histyoryName, int type, android.os.WorkSource newWs, int newPid, java.lang.String newName, java.lang.String newHistoryName, int newType, boolean newUnimportantForLogging) throws android.os.RemoteException;
  public void noteStopWakelockFromSource(android.os.WorkSource ws, int pid, java.lang.String name, java.lang.String historyName, int type) throws android.os.RemoteException;
  public void noteLongPartialWakelockStart(java.lang.String name, java.lang.String historyName, int uid) throws android.os.RemoteException;
  public void noteLongPartialWakelockStartFromSource(java.lang.String name, java.lang.String historyName, android.os.WorkSource workSource) throws android.os.RemoteException;
  public void noteLongPartialWakelockFinish(java.lang.String name, java.lang.String historyName, int uid) throws android.os.RemoteException;
  public void noteLongPartialWakelockFinishFromSource(java.lang.String name, java.lang.String historyName, android.os.WorkSource workSource) throws android.os.RemoteException;
  public void noteVibratorOn(int uid, long durationMillis) throws android.os.RemoteException;
  public void noteVibratorOff(int uid) throws android.os.RemoteException;
  public void noteGpsChanged(android.os.WorkSource oldSource, android.os.WorkSource newSource) throws android.os.RemoteException;
  public void noteGpsSignalQuality(int signalLevel) throws android.os.RemoteException;
  public void noteScreenState(int state) throws android.os.RemoteException;
  public void noteScreenBrightness(int brightness) throws android.os.RemoteException;
  public void noteUserActivity(int uid, int event) throws android.os.RemoteException;
  public void noteWakeUp(java.lang.String reason, int reasonUid) throws android.os.RemoteException;
  public void noteInteractive(boolean interactive) throws android.os.RemoteException;
  public void noteConnectivityChanged(int type, java.lang.String extra) throws android.os.RemoteException;
  public void noteMobileRadioPowerState(int powerState, long timestampNs, int uid) throws android.os.RemoteException;
  public void notePhoneOn() throws android.os.RemoteException;
  public void notePhoneOff() throws android.os.RemoteException;
  public void notePhoneSignalStrength(android.telephony.SignalStrength signalStrength) throws android.os.RemoteException;
  public void notePhoneDataConnectionState(int dataType, boolean hasData, int serviceType) throws android.os.RemoteException;
  public void notePhoneState(int phoneState) throws android.os.RemoteException;
  public void noteWifiOn() throws android.os.RemoteException;
  public void noteWifiOff() throws android.os.RemoteException;
  public void noteWifiRunning(android.os.WorkSource ws) throws android.os.RemoteException;
  public void noteWifiRunningChanged(android.os.WorkSource oldWs, android.os.WorkSource newWs) throws android.os.RemoteException;
  public void noteWifiStopped(android.os.WorkSource ws) throws android.os.RemoteException;
  public void noteWifiState(int wifiState, java.lang.String accessPoint) throws android.os.RemoteException;
  public void noteWifiSupplicantStateChanged(int supplState, boolean failedAuth) throws android.os.RemoteException;
  public void noteWifiRssiChanged(int newRssi) throws android.os.RemoteException;
  public void noteFullWifiLockAcquired(int uid) throws android.os.RemoteException;
  public void noteFullWifiLockReleased(int uid) throws android.os.RemoteException;
  public void noteWifiScanStarted(int uid) throws android.os.RemoteException;
  public void noteWifiScanStopped(int uid) throws android.os.RemoteException;
  public void noteWifiMulticastEnabled(int uid) throws android.os.RemoteException;
  public void noteWifiMulticastDisabled(int uid) throws android.os.RemoteException;
  public void noteFullWifiLockAcquiredFromSource(android.os.WorkSource ws) throws android.os.RemoteException;
  public void noteFullWifiLockReleasedFromSource(android.os.WorkSource ws) throws android.os.RemoteException;
  public void noteWifiScanStartedFromSource(android.os.WorkSource ws) throws android.os.RemoteException;
  public void noteWifiScanStoppedFromSource(android.os.WorkSource ws) throws android.os.RemoteException;
  public void noteWifiBatchedScanStartedFromSource(android.os.WorkSource ws, int csph) throws android.os.RemoteException;
  public void noteWifiBatchedScanStoppedFromSource(android.os.WorkSource ws) throws android.os.RemoteException;
  public void noteWifiRadioPowerState(int powerState, long timestampNs, int uid) throws android.os.RemoteException;
  public void noteNetworkInterfaceType(java.lang.String iface, int type) throws android.os.RemoteException;
  public void noteNetworkStatsEnabled() throws android.os.RemoteException;
  public void noteDeviceIdleMode(int mode, java.lang.String activeReason, int activeUid) throws android.os.RemoteException;
  public void setBatteryState(int status, int health, int plugType, int level, int temp, int volt, int chargeUAh, int chargeFullUAh) throws android.os.RemoteException;
  @android.annotation.UnsupportedAppUsage
  public long getAwakeTimeBattery() throws android.os.RemoteException;
  public long getAwakeTimePlugged() throws android.os.RemoteException;
  public void noteBleScanStarted(android.os.WorkSource ws, boolean isUnoptimized) throws android.os.RemoteException;
  public void noteBleScanStopped(android.os.WorkSource ws, boolean isUnoptimized) throws android.os.RemoteException;
  public void noteResetBleScan() throws android.os.RemoteException;
  public void noteBleScanResults(android.os.WorkSource ws, int numNewResults) throws android.os.RemoteException;
  /** {@hide} */
  public android.os.connectivity.CellularBatteryStats getCellularBatteryStats() throws android.os.RemoteException;
  /** {@hide} */
  public android.os.connectivity.WifiBatteryStats getWifiBatteryStats() throws android.os.RemoteException;
  /** {@hide} */
  public android.os.connectivity.GpsBatteryStats getGpsBatteryStats() throws android.os.RemoteException;
  public android.os.health.HealthStatsParceler takeUidSnapshot(int uid) throws android.os.RemoteException;
  public android.os.health.HealthStatsParceler[] takeUidSnapshots(int[] uid) throws android.os.RemoteException;
  public void noteBluetoothControllerActivity(android.bluetooth.BluetoothActivityEnergyInfo info) throws android.os.RemoteException;
  public void noteModemControllerActivity(android.telephony.ModemActivityInfo info) throws android.os.RemoteException;
  public void noteWifiControllerActivity(android.net.wifi.WifiActivityEnergyInfo info) throws android.os.RemoteException;
  /** {@hide} */
  public boolean setChargingStateUpdateDelayMillis(int delay) throws android.os.RemoteException;
}
