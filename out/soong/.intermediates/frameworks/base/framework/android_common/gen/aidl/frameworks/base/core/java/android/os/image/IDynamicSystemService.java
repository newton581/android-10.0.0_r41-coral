/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.os.image;
/** {@hide} */
public interface IDynamicSystemService extends android.os.IInterface
{
  /** Default implementation for IDynamicSystemService. */
  public static class Default implements android.os.image.IDynamicSystemService
  {
    /**
         * Start DynamicSystem installation. This call may take 60~90 seconds. The caller
         * may use another thread to call the getStartProgress() to get the progress.
         *
         * @param systemSize system size in bytes
         * @param userdataSize userdata size in bytes
         * @return true if the call succeeds
         */
    @Override public boolean startInstallation(long systemSize, long userdataSize) throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Query the progress of the current installation operation. This can be called while
         * the installation is in progress.
         *
         * @return GsiProgress
         */
    @Override public android.gsi.GsiProgress getInstallationProgress() throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Abort the installation process. Note this method must be called in a thread other
         * than the one calling the startInstallation method as the startInstallation
         * method will not return until it is finished.
         *
         * @return true if the call succeeds
         */
    @Override public boolean abort() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * @return true if the device is running an DynamicAnroid image
         */
    @Override public boolean isInUse() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * @return true if the device has an DynamicSystem image installed
         */
    @Override public boolean isInstalled() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * @return true if the device has an DynamicSystem image enabled
         */
    @Override public boolean isEnabled() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Remove DynamicSystem installation if present
         *
         * @return true if the call succeeds
         */
    @Override public boolean remove() throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Enable or disable DynamicSystem.
         *
         * @return true if the call succeeds
         */
    @Override public boolean setEnable(boolean enable) throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Write a chunk of the DynamicSystem system image
         *
         * @return true if the call succeeds
         */
    @Override public boolean write(byte[] buf) throws android.os.RemoteException
    {
      return false;
    }
    /**
         * Finish write and make device to boot into the it after reboot.
         *
         * @return true if the call succeeds
         */
    @Override public boolean commit() throws android.os.RemoteException
    {
      return false;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.os.image.IDynamicSystemService
  {
    private static final java.lang.String DESCRIPTOR = "android.os.image.IDynamicSystemService";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.os.image.IDynamicSystemService interface,
     * generating a proxy if needed.
     */
    public static android.os.image.IDynamicSystemService asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.os.image.IDynamicSystemService))) {
        return ((android.os.image.IDynamicSystemService)iin);
      }
      return new android.os.image.IDynamicSystemService.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_startInstallation:
        {
          return "startInstallation";
        }
        case TRANSACTION_getInstallationProgress:
        {
          return "getInstallationProgress";
        }
        case TRANSACTION_abort:
        {
          return "abort";
        }
        case TRANSACTION_isInUse:
        {
          return "isInUse";
        }
        case TRANSACTION_isInstalled:
        {
          return "isInstalled";
        }
        case TRANSACTION_isEnabled:
        {
          return "isEnabled";
        }
        case TRANSACTION_remove:
        {
          return "remove";
        }
        case TRANSACTION_setEnable:
        {
          return "setEnable";
        }
        case TRANSACTION_write:
        {
          return "write";
        }
        case TRANSACTION_commit:
        {
          return "commit";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_startInstallation:
        {
          data.enforceInterface(descriptor);
          long _arg0;
          _arg0 = data.readLong();
          long _arg1;
          _arg1 = data.readLong();
          boolean _result = this.startInstallation(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getInstallationProgress:
        {
          data.enforceInterface(descriptor);
          android.gsi.GsiProgress _result = this.getInstallationProgress();
          reply.writeNoException();
          if ((_result!=null)) {
            reply.writeInt(1);
            _result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
          }
          else {
            reply.writeInt(0);
          }
          return true;
        }
        case TRANSACTION_abort:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.abort();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isInUse:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isInUse();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isInstalled:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isInstalled();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isEnabled:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isEnabled();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_remove:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.remove();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_setEnable:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          boolean _result = this.setEnable(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_write:
        {
          data.enforceInterface(descriptor);
          byte[] _arg0;
          _arg0 = data.createByteArray();
          boolean _result = this.write(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_commit:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.commit();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.os.image.IDynamicSystemService
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Start DynamicSystem installation. This call may take 60~90 seconds. The caller
           * may use another thread to call the getStartProgress() to get the progress.
           *
           * @param systemSize system size in bytes
           * @param userdataSize userdata size in bytes
           * @return true if the call succeeds
           */
      @Override public boolean startInstallation(long systemSize, long userdataSize) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeLong(systemSize);
          _data.writeLong(userdataSize);
          boolean _status = mRemote.transact(Stub.TRANSACTION_startInstallation, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().startInstallation(systemSize, userdataSize);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Query the progress of the current installation operation. This can be called while
           * the installation is in progress.
           *
           * @return GsiProgress
           */
      @Override public android.gsi.GsiProgress getInstallationProgress() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.gsi.GsiProgress _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getInstallationProgress, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getInstallationProgress();
          }
          _reply.readException();
          if ((0!=_reply.readInt())) {
            _result = android.gsi.GsiProgress.CREATOR.createFromParcel(_reply);
          }
          else {
            _result = null;
          }
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Abort the installation process. Note this method must be called in a thread other
           * than the one calling the startInstallation method as the startInstallation
           * method will not return until it is finished.
           *
           * @return true if the call succeeds
           */
      @Override public boolean abort() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_abort, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().abort();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * @return true if the device is running an DynamicAnroid image
           */
      @Override public boolean isInUse() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isInUse, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isInUse();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * @return true if the device has an DynamicSystem image installed
           */
      @Override public boolean isInstalled() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isInstalled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isInstalled();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * @return true if the device has an DynamicSystem image enabled
           */
      @Override public boolean isEnabled() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isEnabled();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Remove DynamicSystem installation if present
           *
           * @return true if the call succeeds
           */
      @Override public boolean remove() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_remove, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().remove();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Enable or disable DynamicSystem.
           *
           * @return true if the call succeeds
           */
      @Override public boolean setEnable(boolean enable) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((enable)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setEnable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setEnable(enable);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Write a chunk of the DynamicSystem system image
           *
           * @return true if the call succeeds
           */
      @Override public boolean write(byte[] buf) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeByteArray(buf);
          boolean _status = mRemote.transact(Stub.TRANSACTION_write, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().write(buf);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Finish write and make device to boot into the it after reboot.
           *
           * @return true if the call succeeds
           */
      @Override public boolean commit() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_commit, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().commit();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static android.os.image.IDynamicSystemService sDefaultImpl;
    }
    static final int TRANSACTION_startInstallation = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_getInstallationProgress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_abort = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_isInUse = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_isInstalled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_isEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_remove = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_setEnable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_write = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_commit = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    public static boolean setDefaultImpl(android.os.image.IDynamicSystemService impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.os.image.IDynamicSystemService getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /**
       * Start DynamicSystem installation. This call may take 60~90 seconds. The caller
       * may use another thread to call the getStartProgress() to get the progress.
       *
       * @param systemSize system size in bytes
       * @param userdataSize userdata size in bytes
       * @return true if the call succeeds
       */
  public boolean startInstallation(long systemSize, long userdataSize) throws android.os.RemoteException;
  /**
       * Query the progress of the current installation operation. This can be called while
       * the installation is in progress.
       *
       * @return GsiProgress
       */
  public android.gsi.GsiProgress getInstallationProgress() throws android.os.RemoteException;
  /**
       * Abort the installation process. Note this method must be called in a thread other
       * than the one calling the startInstallation method as the startInstallation
       * method will not return until it is finished.
       *
       * @return true if the call succeeds
       */
  public boolean abort() throws android.os.RemoteException;
  /**
       * @return true if the device is running an DynamicAnroid image
       */
  public boolean isInUse() throws android.os.RemoteException;
  /**
       * @return true if the device has an DynamicSystem image installed
       */
  public boolean isInstalled() throws android.os.RemoteException;
  /**
       * @return true if the device has an DynamicSystem image enabled
       */
  public boolean isEnabled() throws android.os.RemoteException;
  /**
       * Remove DynamicSystem installation if present
       *
       * @return true if the call succeeds
       */
  public boolean remove() throws android.os.RemoteException;
  /**
       * Enable or disable DynamicSystem.
       *
       * @return true if the call succeeds
       */
  public boolean setEnable(boolean enable) throws android.os.RemoteException;
  /**
       * Write a chunk of the DynamicSystem system image
       *
       * @return true if the call succeeds
       */
  public boolean write(byte[] buf) throws android.os.RemoteException;
  /**
       * Finish write and make device to boot into the it after reboot.
       *
       * @return true if the call succeeds
       */
  public boolean commit() throws android.os.RemoteException;
}
