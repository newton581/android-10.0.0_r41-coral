/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.security.keystore;
/**
 * @hide
 */
public interface IKeystoreService extends android.os.IInterface
{
  /** Default implementation for IKeystoreService. */
  public static class Default implements android.security.keystore.IKeystoreService
  {
    @Override public int getState(int userId) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public byte[] get(java.lang.String name, int uid) throws android.os.RemoteException
    {
      return null;
    }
    @Override public int insert(java.lang.String name, byte[] item, int uid, int flags) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int del(java.lang.String name, int uid) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int exist(java.lang.String name, int uid) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public java.lang.String[] list(java.lang.String namePrefix, int uid) throws android.os.RemoteException
    {
      return null;
    }
    @Override public int reset() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int onUserPasswordChanged(int userId, java.lang.String newPassword) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int lock(int userId) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int unlock(int userId, java.lang.String userPassword) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int isEmpty(int userId) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public java.lang.String grant(java.lang.String name, int granteeUid) throws android.os.RemoteException
    {
      return null;
    }
    @Override public int ungrant(java.lang.String name, int granteeUid) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public long getmtime(java.lang.String name, int uid) throws android.os.RemoteException
    {
      return 0L;
    }
    @Override public int is_hardware_backed(java.lang.String string) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int clear_uid(long uid) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int addRngEntropy(android.security.keystore.IKeystoreResponseCallback cb, byte[] data, int flags) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int generateKey(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String alias, android.security.keymaster.KeymasterArguments arguments, byte[] entropy, int uid, int flags) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int getKeyCharacteristics(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String alias, android.security.keymaster.KeymasterBlob clientId, android.security.keymaster.KeymasterBlob appData, int uid) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int importKey(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String alias, android.security.keymaster.KeymasterArguments arguments, int format, byte[] keyData, int uid, int flags) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int exportKey(android.security.keystore.IKeystoreExportKeyCallback cb, java.lang.String alias, int format, android.security.keymaster.KeymasterBlob clientId, android.security.keymaster.KeymasterBlob appData, int uid) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int begin(android.security.keystore.IKeystoreOperationResultCallback cb, android.os.IBinder appToken, java.lang.String alias, int purpose, boolean pruneable, android.security.keymaster.KeymasterArguments params, byte[] entropy, int uid) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int update(android.security.keystore.IKeystoreOperationResultCallback cb, android.os.IBinder token, android.security.keymaster.KeymasterArguments params, byte[] input) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int finish(android.security.keystore.IKeystoreOperationResultCallback cb, android.os.IBinder token, android.security.keymaster.KeymasterArguments params, byte[] signature, byte[] entropy) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int abort(android.security.keystore.IKeystoreResponseCallback cb, android.os.IBinder token) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int addAuthToken(byte[] authToken) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int onUserAdded(int userId, int parentId) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int onUserRemoved(int userId) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int attestKey(android.security.keystore.IKeystoreCertificateChainCallback cb, java.lang.String alias, android.security.keymaster.KeymasterArguments params) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int attestDeviceIds(android.security.keystore.IKeystoreCertificateChainCallback cb, android.security.keymaster.KeymasterArguments params) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int onDeviceOffBody() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int importWrappedKey(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String wrappedKeyAlias, byte[] wrappedKey, java.lang.String wrappingKeyAlias, byte[] maskingKey, android.security.keymaster.KeymasterArguments arguments, long rootSid, long fingerprintSid) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int presentConfirmationPrompt(android.os.IBinder listener, java.lang.String promptText, byte[] extraData, java.lang.String locale, int uiOptionsAsFlags) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int cancelConfirmationPrompt(android.os.IBinder listener) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean isConfirmationPromptSupported() throws android.os.RemoteException
    {
      return false;
    }
    @Override public int onKeyguardVisibilityChanged(boolean isShowing, int userId) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public int listUidsOfAuthBoundKeys(java.util.List<java.lang.String> uids) throws android.os.RemoteException
    {
      return 0;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.security.keystore.IKeystoreService
  {
    private static final java.lang.String DESCRIPTOR = "android.security.keystore.IKeystoreService";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.security.keystore.IKeystoreService interface,
     * generating a proxy if needed.
     */
    public static android.security.keystore.IKeystoreService asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.security.keystore.IKeystoreService))) {
        return ((android.security.keystore.IKeystoreService)iin);
      }
      return new android.security.keystore.IKeystoreService.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_getState:
        {
          return "getState";
        }
        case TRANSACTION_get:
        {
          return "get";
        }
        case TRANSACTION_insert:
        {
          return "insert";
        }
        case TRANSACTION_del:
        {
          return "del";
        }
        case TRANSACTION_exist:
        {
          return "exist";
        }
        case TRANSACTION_list:
        {
          return "list";
        }
        case TRANSACTION_reset:
        {
          return "reset";
        }
        case TRANSACTION_onUserPasswordChanged:
        {
          return "onUserPasswordChanged";
        }
        case TRANSACTION_lock:
        {
          return "lock";
        }
        case TRANSACTION_unlock:
        {
          return "unlock";
        }
        case TRANSACTION_isEmpty:
        {
          return "isEmpty";
        }
        case TRANSACTION_grant:
        {
          return "grant";
        }
        case TRANSACTION_ungrant:
        {
          return "ungrant";
        }
        case TRANSACTION_getmtime:
        {
          return "getmtime";
        }
        case TRANSACTION_is_hardware_backed:
        {
          return "is_hardware_backed";
        }
        case TRANSACTION_clear_uid:
        {
          return "clear_uid";
        }
        case TRANSACTION_addRngEntropy:
        {
          return "addRngEntropy";
        }
        case TRANSACTION_generateKey:
        {
          return "generateKey";
        }
        case TRANSACTION_getKeyCharacteristics:
        {
          return "getKeyCharacteristics";
        }
        case TRANSACTION_importKey:
        {
          return "importKey";
        }
        case TRANSACTION_exportKey:
        {
          return "exportKey";
        }
        case TRANSACTION_begin:
        {
          return "begin";
        }
        case TRANSACTION_update:
        {
          return "update";
        }
        case TRANSACTION_finish:
        {
          return "finish";
        }
        case TRANSACTION_abort:
        {
          return "abort";
        }
        case TRANSACTION_addAuthToken:
        {
          return "addAuthToken";
        }
        case TRANSACTION_onUserAdded:
        {
          return "onUserAdded";
        }
        case TRANSACTION_onUserRemoved:
        {
          return "onUserRemoved";
        }
        case TRANSACTION_attestKey:
        {
          return "attestKey";
        }
        case TRANSACTION_attestDeviceIds:
        {
          return "attestDeviceIds";
        }
        case TRANSACTION_onDeviceOffBody:
        {
          return "onDeviceOffBody";
        }
        case TRANSACTION_importWrappedKey:
        {
          return "importWrappedKey";
        }
        case TRANSACTION_presentConfirmationPrompt:
        {
          return "presentConfirmationPrompt";
        }
        case TRANSACTION_cancelConfirmationPrompt:
        {
          return "cancelConfirmationPrompt";
        }
        case TRANSACTION_isConfirmationPromptSupported:
        {
          return "isConfirmationPromptSupported";
        }
        case TRANSACTION_onKeyguardVisibilityChanged:
        {
          return "onKeyguardVisibilityChanged";
        }
        case TRANSACTION_listUidsOfAuthBoundKeys:
        {
          return "listUidsOfAuthBoundKeys";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_getState:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _result = this.getState(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_get:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          byte[] _result = this.get(_arg0, _arg1);
          reply.writeNoException();
          reply.writeByteArray(_result);
          return true;
        }
        case TRANSACTION_insert:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          byte[] _arg1;
          _arg1 = data.createByteArray();
          int _arg2;
          _arg2 = data.readInt();
          int _arg3;
          _arg3 = data.readInt();
          int _result = this.insert(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_del:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _result = this.del(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_exist:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _result = this.exist(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_list:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String[] _result = this.list(_arg0, _arg1);
          reply.writeNoException();
          reply.writeStringArray(_result);
          return true;
        }
        case TRANSACTION_reset:
        {
          data.enforceInterface(descriptor);
          int _result = this.reset();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_onUserPasswordChanged:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _result = this.onUserPasswordChanged(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_lock:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _result = this.lock(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_unlock:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _result = this.unlock(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_isEmpty:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _result = this.isEmpty(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_grant:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          java.lang.String _result = this.grant(_arg0, _arg1);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_ungrant:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          int _result = this.ungrant(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getmtime:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _arg1;
          _arg1 = data.readInt();
          long _result = this.getmtime(_arg0, _arg1);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_is_hardware_backed:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _result = this.is_hardware_backed(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_clear_uid:
        {
          data.enforceInterface(descriptor);
          long _arg0;
          _arg0 = data.readLong();
          int _result = this.clear_uid(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_addRngEntropy:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreResponseCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreResponseCallback.Stub.asInterface(data.readStrongBinder());
          byte[] _arg1;
          _arg1 = data.createByteArray();
          int _arg2;
          _arg2 = data.readInt();
          int _result = this.addRngEntropy(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_generateKey:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreKeyCharacteristicsCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreKeyCharacteristicsCallback.Stub.asInterface(data.readStrongBinder());
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.security.keymaster.KeymasterArguments _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.security.keymaster.KeymasterArguments.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          byte[] _arg3;
          _arg3 = data.createByteArray();
          int _arg4;
          _arg4 = data.readInt();
          int _arg5;
          _arg5 = data.readInt();
          int _result = this.generateKey(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getKeyCharacteristics:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreKeyCharacteristicsCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreKeyCharacteristicsCallback.Stub.asInterface(data.readStrongBinder());
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.security.keymaster.KeymasterBlob _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.security.keymaster.KeymasterBlob.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          android.security.keymaster.KeymasterBlob _arg3;
          if ((0!=data.readInt())) {
            _arg3 = android.security.keymaster.KeymasterBlob.CREATOR.createFromParcel(data);
          }
          else {
            _arg3 = null;
          }
          int _arg4;
          _arg4 = data.readInt();
          int _result = this.getKeyCharacteristics(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_importKey:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreKeyCharacteristicsCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreKeyCharacteristicsCallback.Stub.asInterface(data.readStrongBinder());
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.security.keymaster.KeymasterArguments _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.security.keymaster.KeymasterArguments.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          int _arg3;
          _arg3 = data.readInt();
          byte[] _arg4;
          _arg4 = data.createByteArray();
          int _arg5;
          _arg5 = data.readInt();
          int _arg6;
          _arg6 = data.readInt();
          int _result = this.importKey(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_exportKey:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreExportKeyCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreExportKeyCallback.Stub.asInterface(data.readStrongBinder());
          java.lang.String _arg1;
          _arg1 = data.readString();
          int _arg2;
          _arg2 = data.readInt();
          android.security.keymaster.KeymasterBlob _arg3;
          if ((0!=data.readInt())) {
            _arg3 = android.security.keymaster.KeymasterBlob.CREATOR.createFromParcel(data);
          }
          else {
            _arg3 = null;
          }
          android.security.keymaster.KeymasterBlob _arg4;
          if ((0!=data.readInt())) {
            _arg4 = android.security.keymaster.KeymasterBlob.CREATOR.createFromParcel(data);
          }
          else {
            _arg4 = null;
          }
          int _arg5;
          _arg5 = data.readInt();
          int _result = this.exportKey(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_begin:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreOperationResultCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreOperationResultCallback.Stub.asInterface(data.readStrongBinder());
          android.os.IBinder _arg1;
          _arg1 = data.readStrongBinder();
          java.lang.String _arg2;
          _arg2 = data.readString();
          int _arg3;
          _arg3 = data.readInt();
          boolean _arg4;
          _arg4 = (0!=data.readInt());
          android.security.keymaster.KeymasterArguments _arg5;
          if ((0!=data.readInt())) {
            _arg5 = android.security.keymaster.KeymasterArguments.CREATOR.createFromParcel(data);
          }
          else {
            _arg5 = null;
          }
          byte[] _arg6;
          _arg6 = data.createByteArray();
          int _arg7;
          _arg7 = data.readInt();
          int _result = this.begin(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg7);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_update:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreOperationResultCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreOperationResultCallback.Stub.asInterface(data.readStrongBinder());
          android.os.IBinder _arg1;
          _arg1 = data.readStrongBinder();
          android.security.keymaster.KeymasterArguments _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.security.keymaster.KeymasterArguments.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          byte[] _arg3;
          _arg3 = data.createByteArray();
          int _result = this.update(_arg0, _arg1, _arg2, _arg3);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_finish:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreOperationResultCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreOperationResultCallback.Stub.asInterface(data.readStrongBinder());
          android.os.IBinder _arg1;
          _arg1 = data.readStrongBinder();
          android.security.keymaster.KeymasterArguments _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.security.keymaster.KeymasterArguments.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          byte[] _arg3;
          _arg3 = data.createByteArray();
          byte[] _arg4;
          _arg4 = data.createByteArray();
          int _result = this.finish(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_abort:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreResponseCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreResponseCallback.Stub.asInterface(data.readStrongBinder());
          android.os.IBinder _arg1;
          _arg1 = data.readStrongBinder();
          int _result = this.abort(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_addAuthToken:
        {
          data.enforceInterface(descriptor);
          byte[] _arg0;
          _arg0 = data.createByteArray();
          int _result = this.addAuthToken(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_onUserAdded:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _arg1;
          _arg1 = data.readInt();
          int _result = this.onUserAdded(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_onUserRemoved:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          int _result = this.onUserRemoved(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_attestKey:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreCertificateChainCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreCertificateChainCallback.Stub.asInterface(data.readStrongBinder());
          java.lang.String _arg1;
          _arg1 = data.readString();
          android.security.keymaster.KeymasterArguments _arg2;
          if ((0!=data.readInt())) {
            _arg2 = android.security.keymaster.KeymasterArguments.CREATOR.createFromParcel(data);
          }
          else {
            _arg2 = null;
          }
          int _result = this.attestKey(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_attestDeviceIds:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreCertificateChainCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreCertificateChainCallback.Stub.asInterface(data.readStrongBinder());
          android.security.keymaster.KeymasterArguments _arg1;
          if ((0!=data.readInt())) {
            _arg1 = android.security.keymaster.KeymasterArguments.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          int _result = this.attestDeviceIds(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_onDeviceOffBody:
        {
          data.enforceInterface(descriptor);
          int _result = this.onDeviceOffBody();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_importWrappedKey:
        {
          data.enforceInterface(descriptor);
          android.security.keystore.IKeystoreKeyCharacteristicsCallback _arg0;
          _arg0 = android.security.keystore.IKeystoreKeyCharacteristicsCallback.Stub.asInterface(data.readStrongBinder());
          java.lang.String _arg1;
          _arg1 = data.readString();
          byte[] _arg2;
          _arg2 = data.createByteArray();
          java.lang.String _arg3;
          _arg3 = data.readString();
          byte[] _arg4;
          _arg4 = data.createByteArray();
          android.security.keymaster.KeymasterArguments _arg5;
          if ((0!=data.readInt())) {
            _arg5 = android.security.keymaster.KeymasterArguments.CREATOR.createFromParcel(data);
          }
          else {
            _arg5 = null;
          }
          long _arg6;
          _arg6 = data.readLong();
          long _arg7;
          _arg7 = data.readLong();
          int _result = this.importWrappedKey(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5, _arg6, _arg7);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_presentConfirmationPrompt:
        {
          data.enforceInterface(descriptor);
          android.os.IBinder _arg0;
          _arg0 = data.readStrongBinder();
          java.lang.String _arg1;
          _arg1 = data.readString();
          byte[] _arg2;
          _arg2 = data.createByteArray();
          java.lang.String _arg3;
          _arg3 = data.readString();
          int _arg4;
          _arg4 = data.readInt();
          int _result = this.presentConfirmationPrompt(_arg0, _arg1, _arg2, _arg3, _arg4);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_cancelConfirmationPrompt:
        {
          data.enforceInterface(descriptor);
          android.os.IBinder _arg0;
          _arg0 = data.readStrongBinder();
          int _result = this.cancelConfirmationPrompt(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_isConfirmationPromptSupported:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isConfirmationPromptSupported();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_onKeyguardVisibilityChanged:
        {
          data.enforceInterface(descriptor);
          boolean _arg0;
          _arg0 = (0!=data.readInt());
          int _arg1;
          _arg1 = data.readInt();
          int _result = this.onKeyguardVisibilityChanged(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_listUidsOfAuthBoundKeys:
        {
          data.enforceInterface(descriptor);
          java.util.List<java.lang.String> _arg0;
          _arg0 = new java.util.ArrayList<java.lang.String>();
          int _result = this.listUidsOfAuthBoundKeys(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          reply.writeStringList(_arg0);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.security.keystore.IKeystoreService
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public int getState(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getState(userId);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public byte[] get(java.lang.String name, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        byte[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_get, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().get(name, uid);
          }
          _reply.readException();
          _result = _reply.createByteArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int insert(java.lang.String name, byte[] item, int uid, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeByteArray(item);
          _data.writeInt(uid);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_insert, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().insert(name, item, uid, flags);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int del(java.lang.String name, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_del, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().del(name, uid);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int exist(java.lang.String name, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_exist, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().exist(name, uid);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String[] list(java.lang.String namePrefix, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(namePrefix);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_list, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().list(namePrefix, uid);
          }
          _reply.readException();
          _result = _reply.createStringArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int reset() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_reset, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().reset();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int onUserPasswordChanged(int userId, java.lang.String newPassword) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeString(newPassword);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onUserPasswordChanged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().onUserPasswordChanged(userId, newPassword);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int lock(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_lock, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().lock(userId);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int unlock(int userId, java.lang.String userPassword) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeString(userPassword);
          boolean _status = mRemote.transact(Stub.TRANSACTION_unlock, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().unlock(userId, userPassword);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int isEmpty(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isEmpty, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isEmpty(userId);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String grant(java.lang.String name, int granteeUid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(granteeUid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_grant, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().grant(name, granteeUid);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int ungrant(java.lang.String name, int granteeUid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(granteeUid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_ungrant, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().ungrant(name, granteeUid);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public long getmtime(java.lang.String name, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(name);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getmtime, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getmtime(name, uid);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int is_hardware_backed(java.lang.String string) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(string);
          boolean _status = mRemote.transact(Stub.TRANSACTION_is_hardware_backed, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().is_hardware_backed(string);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int clear_uid(long uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeLong(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_clear_uid, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().clear_uid(uid);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int addRngEntropy(android.security.keystore.IKeystoreResponseCallback cb, byte[] data, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeByteArray(data);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_addRngEntropy, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().addRngEntropy(cb, data, flags);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int generateKey(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String alias, android.security.keymaster.KeymasterArguments arguments, byte[] entropy, int uid, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeString(alias);
          if ((arguments!=null)) {
            _data.writeInt(1);
            arguments.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeByteArray(entropy);
          _data.writeInt(uid);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_generateKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().generateKey(cb, alias, arguments, entropy, uid, flags);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getKeyCharacteristics(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String alias, android.security.keymaster.KeymasterBlob clientId, android.security.keymaster.KeymasterBlob appData, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeString(alias);
          if ((clientId!=null)) {
            _data.writeInt(1);
            clientId.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((appData!=null)) {
            _data.writeInt(1);
            appData.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getKeyCharacteristics, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getKeyCharacteristics(cb, alias, clientId, appData, uid);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int importKey(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String alias, android.security.keymaster.KeymasterArguments arguments, int format, byte[] keyData, int uid, int flags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeString(alias);
          if ((arguments!=null)) {
            _data.writeInt(1);
            arguments.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(format);
          _data.writeByteArray(keyData);
          _data.writeInt(uid);
          _data.writeInt(flags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_importKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().importKey(cb, alias, arguments, format, keyData, uid, flags);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int exportKey(android.security.keystore.IKeystoreExportKeyCallback cb, java.lang.String alias, int format, android.security.keymaster.KeymasterBlob clientId, android.security.keymaster.KeymasterBlob appData, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeString(alias);
          _data.writeInt(format);
          if ((clientId!=null)) {
            _data.writeInt(1);
            clientId.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((appData!=null)) {
            _data.writeInt(1);
            appData.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_exportKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().exportKey(cb, alias, format, clientId, appData, uid);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int begin(android.security.keystore.IKeystoreOperationResultCallback cb, android.os.IBinder appToken, java.lang.String alias, int purpose, boolean pruneable, android.security.keymaster.KeymasterArguments params, byte[] entropy, int uid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeStrongBinder(appToken);
          _data.writeString(alias);
          _data.writeInt(purpose);
          _data.writeInt(((pruneable)?(1):(0)));
          if ((params!=null)) {
            _data.writeInt(1);
            params.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeByteArray(entropy);
          _data.writeInt(uid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_begin, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().begin(cb, appToken, alias, purpose, pruneable, params, entropy, uid);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int update(android.security.keystore.IKeystoreOperationResultCallback cb, android.os.IBinder token, android.security.keymaster.KeymasterArguments params, byte[] input) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeStrongBinder(token);
          if ((params!=null)) {
            _data.writeInt(1);
            params.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeByteArray(input);
          boolean _status = mRemote.transact(Stub.TRANSACTION_update, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().update(cb, token, params, input);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int finish(android.security.keystore.IKeystoreOperationResultCallback cb, android.os.IBinder token, android.security.keymaster.KeymasterArguments params, byte[] signature, byte[] entropy) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeStrongBinder(token);
          if ((params!=null)) {
            _data.writeInt(1);
            params.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeByteArray(signature);
          _data.writeByteArray(entropy);
          boolean _status = mRemote.transact(Stub.TRANSACTION_finish, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().finish(cb, token, params, signature, entropy);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int abort(android.security.keystore.IKeystoreResponseCallback cb, android.os.IBinder token) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeStrongBinder(token);
          boolean _status = mRemote.transact(Stub.TRANSACTION_abort, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().abort(cb, token);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int addAuthToken(byte[] authToken) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeByteArray(authToken);
          boolean _status = mRemote.transact(Stub.TRANSACTION_addAuthToken, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().addAuthToken(authToken);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int onUserAdded(int userId, int parentId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          _data.writeInt(parentId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onUserAdded, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().onUserAdded(userId, parentId);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int onUserRemoved(int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onUserRemoved, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().onUserRemoved(userId);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int attestKey(android.security.keystore.IKeystoreCertificateChainCallback cb, java.lang.String alias, android.security.keymaster.KeymasterArguments params) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeString(alias);
          if ((params!=null)) {
            _data.writeInt(1);
            params.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_attestKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().attestKey(cb, alias, params);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int attestDeviceIds(android.security.keystore.IKeystoreCertificateChainCallback cb, android.security.keymaster.KeymasterArguments params) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          if ((params!=null)) {
            _data.writeInt(1);
            params.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_attestDeviceIds, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().attestDeviceIds(cb, params);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int onDeviceOffBody() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onDeviceOffBody, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().onDeviceOffBody();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int importWrappedKey(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String wrappedKeyAlias, byte[] wrappedKey, java.lang.String wrappingKeyAlias, byte[] maskingKey, android.security.keymaster.KeymasterArguments arguments, long rootSid, long fingerprintSid) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
          _data.writeString(wrappedKeyAlias);
          _data.writeByteArray(wrappedKey);
          _data.writeString(wrappingKeyAlias);
          _data.writeByteArray(maskingKey);
          if ((arguments!=null)) {
            _data.writeInt(1);
            arguments.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          _data.writeLong(rootSid);
          _data.writeLong(fingerprintSid);
          boolean _status = mRemote.transact(Stub.TRANSACTION_importWrappedKey, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().importWrappedKey(cb, wrappedKeyAlias, wrappedKey, wrappingKeyAlias, maskingKey, arguments, rootSid, fingerprintSid);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int presentConfirmationPrompt(android.os.IBinder listener, java.lang.String promptText, byte[] extraData, java.lang.String locale, int uiOptionsAsFlags) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder(listener);
          _data.writeString(promptText);
          _data.writeByteArray(extraData);
          _data.writeString(locale);
          _data.writeInt(uiOptionsAsFlags);
          boolean _status = mRemote.transact(Stub.TRANSACTION_presentConfirmationPrompt, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().presentConfirmationPrompt(listener, promptText, extraData, locale, uiOptionsAsFlags);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int cancelConfirmationPrompt(android.os.IBinder listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder(listener);
          boolean _status = mRemote.transact(Stub.TRANSACTION_cancelConfirmationPrompt, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().cancelConfirmationPrompt(listener);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isConfirmationPromptSupported() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isConfirmationPromptSupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isConfirmationPromptSupported();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int onKeyguardVisibilityChanged(boolean isShowing, int userId) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(((isShowing)?(1):(0)));
          _data.writeInt(userId);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onKeyguardVisibilityChanged, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().onKeyguardVisibilityChanged(isShowing, userId);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int listUidsOfAuthBoundKeys(java.util.List<java.lang.String> uids) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_listUidsOfAuthBoundKeys, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().listUidsOfAuthBoundKeys(uids);
          }
          _reply.readException();
          _result = _reply.readInt();
          _reply.readStringList(uids);
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static android.security.keystore.IKeystoreService sDefaultImpl;
    }
    static final int TRANSACTION_getState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_get = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_insert = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_del = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_exist = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_list = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_reset = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_onUserPasswordChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_lock = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_unlock = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_isEmpty = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_grant = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_ungrant = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_getmtime = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_is_hardware_backed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_clear_uid = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    static final int TRANSACTION_addRngEntropy = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
    static final int TRANSACTION_generateKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
    static final int TRANSACTION_getKeyCharacteristics = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
    static final int TRANSACTION_importKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
    static final int TRANSACTION_exportKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 20);
    static final int TRANSACTION_begin = (android.os.IBinder.FIRST_CALL_TRANSACTION + 21);
    static final int TRANSACTION_update = (android.os.IBinder.FIRST_CALL_TRANSACTION + 22);
    static final int TRANSACTION_finish = (android.os.IBinder.FIRST_CALL_TRANSACTION + 23);
    static final int TRANSACTION_abort = (android.os.IBinder.FIRST_CALL_TRANSACTION + 24);
    static final int TRANSACTION_addAuthToken = (android.os.IBinder.FIRST_CALL_TRANSACTION + 25);
    static final int TRANSACTION_onUserAdded = (android.os.IBinder.FIRST_CALL_TRANSACTION + 26);
    static final int TRANSACTION_onUserRemoved = (android.os.IBinder.FIRST_CALL_TRANSACTION + 27);
    static final int TRANSACTION_attestKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 28);
    static final int TRANSACTION_attestDeviceIds = (android.os.IBinder.FIRST_CALL_TRANSACTION + 29);
    static final int TRANSACTION_onDeviceOffBody = (android.os.IBinder.FIRST_CALL_TRANSACTION + 30);
    static final int TRANSACTION_importWrappedKey = (android.os.IBinder.FIRST_CALL_TRANSACTION + 31);
    static final int TRANSACTION_presentConfirmationPrompt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 32);
    static final int TRANSACTION_cancelConfirmationPrompt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 33);
    static final int TRANSACTION_isConfirmationPromptSupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 34);
    static final int TRANSACTION_onKeyguardVisibilityChanged = (android.os.IBinder.FIRST_CALL_TRANSACTION + 35);
    static final int TRANSACTION_listUidsOfAuthBoundKeys = (android.os.IBinder.FIRST_CALL_TRANSACTION + 36);
    public static boolean setDefaultImpl(android.security.keystore.IKeystoreService impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.security.keystore.IKeystoreService getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public int getState(int userId) throws android.os.RemoteException;
  public byte[] get(java.lang.String name, int uid) throws android.os.RemoteException;
  public int insert(java.lang.String name, byte[] item, int uid, int flags) throws android.os.RemoteException;
  public int del(java.lang.String name, int uid) throws android.os.RemoteException;
  public int exist(java.lang.String name, int uid) throws android.os.RemoteException;
  public java.lang.String[] list(java.lang.String namePrefix, int uid) throws android.os.RemoteException;
  public int reset() throws android.os.RemoteException;
  public int onUserPasswordChanged(int userId, java.lang.String newPassword) throws android.os.RemoteException;
  public int lock(int userId) throws android.os.RemoteException;
  public int unlock(int userId, java.lang.String userPassword) throws android.os.RemoteException;
  public int isEmpty(int userId) throws android.os.RemoteException;
  public java.lang.String grant(java.lang.String name, int granteeUid) throws android.os.RemoteException;
  public int ungrant(java.lang.String name, int granteeUid) throws android.os.RemoteException;
  public long getmtime(java.lang.String name, int uid) throws android.os.RemoteException;
  public int is_hardware_backed(java.lang.String string) throws android.os.RemoteException;
  public int clear_uid(long uid) throws android.os.RemoteException;
  public int addRngEntropy(android.security.keystore.IKeystoreResponseCallback cb, byte[] data, int flags) throws android.os.RemoteException;
  public int generateKey(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String alias, android.security.keymaster.KeymasterArguments arguments, byte[] entropy, int uid, int flags) throws android.os.RemoteException;
  public int getKeyCharacteristics(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String alias, android.security.keymaster.KeymasterBlob clientId, android.security.keymaster.KeymasterBlob appData, int uid) throws android.os.RemoteException;
  public int importKey(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String alias, android.security.keymaster.KeymasterArguments arguments, int format, byte[] keyData, int uid, int flags) throws android.os.RemoteException;
  public int exportKey(android.security.keystore.IKeystoreExportKeyCallback cb, java.lang.String alias, int format, android.security.keymaster.KeymasterBlob clientId, android.security.keymaster.KeymasterBlob appData, int uid) throws android.os.RemoteException;
  public int begin(android.security.keystore.IKeystoreOperationResultCallback cb, android.os.IBinder appToken, java.lang.String alias, int purpose, boolean pruneable, android.security.keymaster.KeymasterArguments params, byte[] entropy, int uid) throws android.os.RemoteException;
  public int update(android.security.keystore.IKeystoreOperationResultCallback cb, android.os.IBinder token, android.security.keymaster.KeymasterArguments params, byte[] input) throws android.os.RemoteException;
  public int finish(android.security.keystore.IKeystoreOperationResultCallback cb, android.os.IBinder token, android.security.keymaster.KeymasterArguments params, byte[] signature, byte[] entropy) throws android.os.RemoteException;
  public int abort(android.security.keystore.IKeystoreResponseCallback cb, android.os.IBinder token) throws android.os.RemoteException;
  public int addAuthToken(byte[] authToken) throws android.os.RemoteException;
  public int onUserAdded(int userId, int parentId) throws android.os.RemoteException;
  public int onUserRemoved(int userId) throws android.os.RemoteException;
  public int attestKey(android.security.keystore.IKeystoreCertificateChainCallback cb, java.lang.String alias, android.security.keymaster.KeymasterArguments params) throws android.os.RemoteException;
  public int attestDeviceIds(android.security.keystore.IKeystoreCertificateChainCallback cb, android.security.keymaster.KeymasterArguments params) throws android.os.RemoteException;
  public int onDeviceOffBody() throws android.os.RemoteException;
  public int importWrappedKey(android.security.keystore.IKeystoreKeyCharacteristicsCallback cb, java.lang.String wrappedKeyAlias, byte[] wrappedKey, java.lang.String wrappingKeyAlias, byte[] maskingKey, android.security.keymaster.KeymasterArguments arguments, long rootSid, long fingerprintSid) throws android.os.RemoteException;
  public int presentConfirmationPrompt(android.os.IBinder listener, java.lang.String promptText, byte[] extraData, java.lang.String locale, int uiOptionsAsFlags) throws android.os.RemoteException;
  public int cancelConfirmationPrompt(android.os.IBinder listener) throws android.os.RemoteException;
  public boolean isConfirmationPromptSupported() throws android.os.RemoteException;
  public int onKeyguardVisibilityChanged(boolean isShowing, int userId) throws android.os.RemoteException;
  public int listUidsOfAuthBoundKeys(java.util.List<java.lang.String> uids) throws android.os.RemoteException;
}
