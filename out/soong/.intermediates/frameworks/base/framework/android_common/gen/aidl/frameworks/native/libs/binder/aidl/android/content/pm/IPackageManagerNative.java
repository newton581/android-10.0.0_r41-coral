/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.content.pm;
/**
 * Parallel implementation of certain {@link PackageManager} APIs that need to
 * be exposed to native code.
 * <p>These APIs are a parallel definition to the APIs in PackageManager, so,
 * they can technically diverge. However, it's good practice to keep these
 * APIs in sync with each other.
 * <p>Because these APIs are exposed to native code, it's possible they will
 * be exposed to privileged components [such as UID 0]. Care should be taken
 * to avoid exposing potential security holes for methods where permission
 * checks are bypassed based upon UID alone.
 *
 * @hide
 */
public interface IPackageManagerNative extends android.os.IInterface
{
  /** Default implementation for IPackageManagerNative. */
  public static class Default implements android.content.pm.IPackageManagerNative
  {
    /**
         * Returns a set of names for the given UIDs.
         * IMPORTANT: Unlike the Java version of this API, unknown UIDs are
         * not represented by 'null's. Instead, they are represented by empty
         * strings.
         */
    @Override public java.lang.String[] getNamesForUids(int[] uids) throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Returns the name of the installer (a package) which installed the named
         * package. Preloaded packages return the string "preload". Sideloaded packages
         * return an empty string. Unknown or unknowable are returned as empty strings.
         */
    @Override public java.lang.String getInstallerForPackage(java.lang.String packageName) throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Returns the version code of the named package.
         * Unknown or unknowable versions are returned as 0.
         */
    @Override public long getVersionCodeForPackage(java.lang.String packageName) throws android.os.RemoteException
    {
      return 0L;
    }
    /**
         * Return if each app, identified by its package name allows its audio to be recorded.
         * Unknown packages are mapped to false.
         */
    @Override public boolean[] isAudioPlaybackCaptureAllowed(java.lang.String[] packageNames) throws android.os.RemoteException
    {
      return null;
    }
    /**
         * Returns a set of bitflags about package location.
         * LOCATION_SYSTEM: getApplicationInfo(packageName).isSystemApp()
         * LOCATION_VENDOR: getApplicationInfo(packageName).isVendor()
         * LOCATION_PRODUCT: getApplicationInfo(packageName).isProduct()
         */
    @Override public int getLocationFlags(java.lang.String packageName) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Returns the target SDK version for the given package.
         * Unknown packages will cause the call to fail. The caller must check the
         * returned Status before using the result of this function.
         */
    @Override public int getTargetSdkVersionForPackage(java.lang.String packageName) throws android.os.RemoteException
    {
      return 0;
    }
    /**
         * Returns the name of module metadata package, or empty string if device doesn't have such
         * package.
         */
    @Override public java.lang.String getModuleMetadataPackageName() throws android.os.RemoteException
    {
      return null;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.content.pm.IPackageManagerNative
  {
    private static final java.lang.String DESCRIPTOR = "android.content.pm.IPackageManagerNative";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.content.pm.IPackageManagerNative interface,
     * generating a proxy if needed.
     */
    public static android.content.pm.IPackageManagerNative asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.content.pm.IPackageManagerNative))) {
        return ((android.content.pm.IPackageManagerNative)iin);
      }
      return new android.content.pm.IPackageManagerNative.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_getNamesForUids:
        {
          return "getNamesForUids";
        }
        case TRANSACTION_getInstallerForPackage:
        {
          return "getInstallerForPackage";
        }
        case TRANSACTION_getVersionCodeForPackage:
        {
          return "getVersionCodeForPackage";
        }
        case TRANSACTION_isAudioPlaybackCaptureAllowed:
        {
          return "isAudioPlaybackCaptureAllowed";
        }
        case TRANSACTION_getLocationFlags:
        {
          return "getLocationFlags";
        }
        case TRANSACTION_getTargetSdkVersionForPackage:
        {
          return "getTargetSdkVersionForPackage";
        }
        case TRANSACTION_getModuleMetadataPackageName:
        {
          return "getModuleMetadataPackageName";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_getNamesForUids:
        {
          data.enforceInterface(descriptor);
          int[] _arg0;
          _arg0 = data.createIntArray();
          java.lang.String[] _result = this.getNamesForUids(_arg0);
          reply.writeNoException();
          reply.writeStringArray(_result);
          return true;
        }
        case TRANSACTION_getInstallerForPackage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          java.lang.String _result = this.getInstallerForPackage(_arg0);
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_getVersionCodeForPackage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          long _result = this.getVersionCodeForPackage(_arg0);
          reply.writeNoException();
          reply.writeLong(_result);
          return true;
        }
        case TRANSACTION_isAudioPlaybackCaptureAllowed:
        {
          data.enforceInterface(descriptor);
          java.lang.String[] _arg0;
          _arg0 = data.createStringArray();
          boolean[] _result = this.isAudioPlaybackCaptureAllowed(_arg0);
          reply.writeNoException();
          reply.writeBooleanArray(_result);
          return true;
        }
        case TRANSACTION_getLocationFlags:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _result = this.getLocationFlags(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getTargetSdkVersionForPackage:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          int _result = this.getTargetSdkVersionForPackage(_arg0);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getModuleMetadataPackageName:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getModuleMetadataPackageName();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.content.pm.IPackageManagerNative
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
           * Returns a set of names for the given UIDs.
           * IMPORTANT: Unlike the Java version of this API, unknown UIDs are
           * not represented by 'null's. Instead, they are represented by empty
           * strings.
           */
      @Override public java.lang.String[] getNamesForUids(int[] uids) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeIntArray(uids);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getNamesForUids, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getNamesForUids(uids);
          }
          _reply.readException();
          _result = _reply.createStringArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Returns the name of the installer (a package) which installed the named
           * package. Preloaded packages return the string "preload". Sideloaded packages
           * return an empty string. Unknown or unknowable are returned as empty strings.
           */
      @Override public java.lang.String getInstallerForPackage(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getInstallerForPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getInstallerForPackage(packageName);
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Returns the version code of the named package.
           * Unknown or unknowable versions are returned as 0.
           */
      @Override public long getVersionCodeForPackage(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        long _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getVersionCodeForPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getVersionCodeForPackage(packageName);
          }
          _reply.readException();
          _result = _reply.readLong();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Return if each app, identified by its package name allows its audio to be recorded.
           * Unknown packages are mapped to false.
           */
      @Override public boolean[] isAudioPlaybackCaptureAllowed(java.lang.String[] packageNames) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStringArray(packageNames);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isAudioPlaybackCaptureAllowed, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isAudioPlaybackCaptureAllowed(packageNames);
          }
          _reply.readException();
          _result = _reply.createBooleanArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Returns a set of bitflags about package location.
           * LOCATION_SYSTEM: getApplicationInfo(packageName).isSystemApp()
           * LOCATION_VENDOR: getApplicationInfo(packageName).isVendor()
           * LOCATION_PRODUCT: getApplicationInfo(packageName).isProduct()
           */
      @Override public int getLocationFlags(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getLocationFlags, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getLocationFlags(packageName);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Returns the target SDK version for the given package.
           * Unknown packages will cause the call to fail. The caller must check the
           * returned Status before using the result of this function.
           */
      @Override public int getTargetSdkVersionForPackage(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getTargetSdkVersionForPackage, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getTargetSdkVersionForPackage(packageName);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      /**
           * Returns the name of module metadata package, or empty string if device doesn't have such
           * package.
           */
      @Override public java.lang.String getModuleMetadataPackageName() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getModuleMetadataPackageName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getModuleMetadataPackageName();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static android.content.pm.IPackageManagerNative sDefaultImpl;
    }
    static final int TRANSACTION_getNamesForUids = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_getInstallerForPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_getVersionCodeForPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_isAudioPlaybackCaptureAllowed = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_getLocationFlags = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_getTargetSdkVersionForPackage = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_getModuleMetadataPackageName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    public static boolean setDefaultImpl(android.content.pm.IPackageManagerNative impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.content.pm.IPackageManagerNative getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int LOCATION_SYSTEM = 1;
  public static final int LOCATION_VENDOR = 2;
  public static final int LOCATION_PRODUCT = 4;
  /**
       * Returns a set of names for the given UIDs.
       * IMPORTANT: Unlike the Java version of this API, unknown UIDs are
       * not represented by 'null's. Instead, they are represented by empty
       * strings.
       */
  public java.lang.String[] getNamesForUids(int[] uids) throws android.os.RemoteException;
  /**
       * Returns the name of the installer (a package) which installed the named
       * package. Preloaded packages return the string "preload". Sideloaded packages
       * return an empty string. Unknown or unknowable are returned as empty strings.
       */
  public java.lang.String getInstallerForPackage(java.lang.String packageName) throws android.os.RemoteException;
  /**
       * Returns the version code of the named package.
       * Unknown or unknowable versions are returned as 0.
       */
  public long getVersionCodeForPackage(java.lang.String packageName) throws android.os.RemoteException;
  /**
       * Return if each app, identified by its package name allows its audio to be recorded.
       * Unknown packages are mapped to false.
       */
  public boolean[] isAudioPlaybackCaptureAllowed(java.lang.String[] packageNames) throws android.os.RemoteException;
  /**
       * Returns a set of bitflags about package location.
       * LOCATION_SYSTEM: getApplicationInfo(packageName).isSystemApp()
       * LOCATION_VENDOR: getApplicationInfo(packageName).isVendor()
       * LOCATION_PRODUCT: getApplicationInfo(packageName).isProduct()
       */
  public int getLocationFlags(java.lang.String packageName) throws android.os.RemoteException;
  /**
       * Returns the target SDK version for the given package.
       * Unknown packages will cause the call to fail. The caller must check the
       * returned Status before using the result of this function.
       */
  public int getTargetSdkVersionForPackage(java.lang.String packageName) throws android.os.RemoteException;
  /**
       * Returns the name of module metadata package, or empty string if device doesn't have such
       * package.
       */
  public java.lang.String getModuleMetadataPackageName() throws android.os.RemoteException;
}
