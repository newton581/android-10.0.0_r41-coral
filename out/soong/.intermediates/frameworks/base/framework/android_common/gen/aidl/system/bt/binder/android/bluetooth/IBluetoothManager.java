/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.bluetooth;
/**
 * System private API for talking with the Bluetooth service.
 *
 * {@hide}
 */
public interface IBluetoothManager extends android.os.IInterface
{
  /** Default implementation for IBluetoothManager. */
  public static class Default implements android.bluetooth.IBluetoothManager
  {
    @Override public android.bluetooth.IBluetooth registerAdapter(android.bluetooth.IBluetoothManagerCallback callback) throws android.os.RemoteException
    {
      return null;
    }
    @Override public void unregisterAdapter(android.bluetooth.IBluetoothManagerCallback callback) throws android.os.RemoteException
    {
    }
    @Override public void registerStateChangeCallback(android.bluetooth.IBluetoothStateChangeCallback callback) throws android.os.RemoteException
    {
    }
    @Override public void unregisterStateChangeCallback(android.bluetooth.IBluetoothStateChangeCallback callback) throws android.os.RemoteException
    {
    }
    @Override public boolean isEnabled() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean enable(java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean enableNoAutoConnect(java.lang.String packageName) throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean disable(java.lang.String packageName, boolean persist) throws android.os.RemoteException
    {
      return false;
    }
    @Override public int getState() throws android.os.RemoteException
    {
      return 0;
    }
    @Override public android.bluetooth.IBluetoothGatt getBluetoothGatt() throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean bindBluetoothProfileService(int profile, android.bluetooth.IBluetoothProfileServiceConnection proxy) throws android.os.RemoteException
    {
      return false;
    }
    @Override public void unbindBluetoothProfileService(int profile, android.bluetooth.IBluetoothProfileServiceConnection proxy) throws android.os.RemoteException
    {
    }
    @Override public java.lang.String getAddress() throws android.os.RemoteException
    {
      return null;
    }
    @Override public java.lang.String getName() throws android.os.RemoteException
    {
      return null;
    }
    @Override public boolean onFactoryReset() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isBleScanAlwaysAvailable() throws android.os.RemoteException
    {
      return false;
    }
    @Override public int updateBleAppCount(android.os.IBinder b, boolean enable, java.lang.String packageName) throws android.os.RemoteException
    {
      return 0;
    }
    @Override public boolean isBleAppPresent() throws android.os.RemoteException
    {
      return false;
    }
    @Override public boolean isHearingAidProfileSupported() throws android.os.RemoteException
    {
      return false;
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.bluetooth.IBluetoothManager
  {
    private static final java.lang.String DESCRIPTOR = "android.bluetooth.IBluetoothManager";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.bluetooth.IBluetoothManager interface,
     * generating a proxy if needed.
     */
    public static android.bluetooth.IBluetoothManager asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.bluetooth.IBluetoothManager))) {
        return ((android.bluetooth.IBluetoothManager)iin);
      }
      return new android.bluetooth.IBluetoothManager.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    /** @hide */
    public static java.lang.String getDefaultTransactionName(int transactionCode)
    {
      switch (transactionCode)
      {
        case TRANSACTION_registerAdapter:
        {
          return "registerAdapter";
        }
        case TRANSACTION_unregisterAdapter:
        {
          return "unregisterAdapter";
        }
        case TRANSACTION_registerStateChangeCallback:
        {
          return "registerStateChangeCallback";
        }
        case TRANSACTION_unregisterStateChangeCallback:
        {
          return "unregisterStateChangeCallback";
        }
        case TRANSACTION_isEnabled:
        {
          return "isEnabled";
        }
        case TRANSACTION_enable:
        {
          return "enable";
        }
        case TRANSACTION_enableNoAutoConnect:
        {
          return "enableNoAutoConnect";
        }
        case TRANSACTION_disable:
        {
          return "disable";
        }
        case TRANSACTION_getState:
        {
          return "getState";
        }
        case TRANSACTION_getBluetoothGatt:
        {
          return "getBluetoothGatt";
        }
        case TRANSACTION_bindBluetoothProfileService:
        {
          return "bindBluetoothProfileService";
        }
        case TRANSACTION_unbindBluetoothProfileService:
        {
          return "unbindBluetoothProfileService";
        }
        case TRANSACTION_getAddress:
        {
          return "getAddress";
        }
        case TRANSACTION_getName:
        {
          return "getName";
        }
        case TRANSACTION_onFactoryReset:
        {
          return "onFactoryReset";
        }
        case TRANSACTION_isBleScanAlwaysAvailable:
        {
          return "isBleScanAlwaysAvailable";
        }
        case TRANSACTION_updateBleAppCount:
        {
          return "updateBleAppCount";
        }
        case TRANSACTION_isBleAppPresent:
        {
          return "isBleAppPresent";
        }
        case TRANSACTION_isHearingAidProfileSupported:
        {
          return "isHearingAidProfileSupported";
        }
        default:
        {
          return null;
        }
      }
    }
    /** @hide */
    public java.lang.String getTransactionName(int transactionCode)
    {
      return this.getDefaultTransactionName(transactionCode);
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_registerAdapter:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.IBluetoothManagerCallback _arg0;
          _arg0 = android.bluetooth.IBluetoothManagerCallback.Stub.asInterface(data.readStrongBinder());
          android.bluetooth.IBluetooth _result = this.registerAdapter(_arg0);
          reply.writeNoException();
          reply.writeStrongBinder((((_result!=null))?(_result.asBinder()):(null)));
          return true;
        }
        case TRANSACTION_unregisterAdapter:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.IBluetoothManagerCallback _arg0;
          _arg0 = android.bluetooth.IBluetoothManagerCallback.Stub.asInterface(data.readStrongBinder());
          this.unregisterAdapter(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_registerStateChangeCallback:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.IBluetoothStateChangeCallback _arg0;
          _arg0 = android.bluetooth.IBluetoothStateChangeCallback.Stub.asInterface(data.readStrongBinder());
          this.registerStateChangeCallback(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_unregisterStateChangeCallback:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.IBluetoothStateChangeCallback _arg0;
          _arg0 = android.bluetooth.IBluetoothStateChangeCallback.Stub.asInterface(data.readStrongBinder());
          this.unregisterStateChangeCallback(_arg0);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_isEnabled:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isEnabled();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_enable:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.enable(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_enableNoAutoConnect:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _result = this.enableNoAutoConnect(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_disable:
        {
          data.enforceInterface(descriptor);
          java.lang.String _arg0;
          _arg0 = data.readString();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          boolean _result = this.disable(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_getState:
        {
          data.enforceInterface(descriptor);
          int _result = this.getState();
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_getBluetoothGatt:
        {
          data.enforceInterface(descriptor);
          android.bluetooth.IBluetoothGatt _result = this.getBluetoothGatt();
          reply.writeNoException();
          reply.writeStrongBinder((((_result!=null))?(_result.asBinder()):(null)));
          return true;
        }
        case TRANSACTION_bindBluetoothProfileService:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.bluetooth.IBluetoothProfileServiceConnection _arg1;
          _arg1 = android.bluetooth.IBluetoothProfileServiceConnection.Stub.asInterface(data.readStrongBinder());
          boolean _result = this.bindBluetoothProfileService(_arg0, _arg1);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_unbindBluetoothProfileService:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          android.bluetooth.IBluetoothProfileServiceConnection _arg1;
          _arg1 = android.bluetooth.IBluetoothProfileServiceConnection.Stub.asInterface(data.readStrongBinder());
          this.unbindBluetoothProfileService(_arg0, _arg1);
          reply.writeNoException();
          return true;
        }
        case TRANSACTION_getAddress:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getAddress();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_getName:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getName();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_onFactoryReset:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.onFactoryReset();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isBleScanAlwaysAvailable:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isBleScanAlwaysAvailable();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_updateBleAppCount:
        {
          data.enforceInterface(descriptor);
          android.os.IBinder _arg0;
          _arg0 = data.readStrongBinder();
          boolean _arg1;
          _arg1 = (0!=data.readInt());
          java.lang.String _arg2;
          _arg2 = data.readString();
          int _result = this.updateBleAppCount(_arg0, _arg1, _arg2);
          reply.writeNoException();
          reply.writeInt(_result);
          return true;
        }
        case TRANSACTION_isBleAppPresent:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isBleAppPresent();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_isHearingAidProfileSupported:
        {
          data.enforceInterface(descriptor);
          boolean _result = this.isHearingAidProfileSupported();
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.bluetooth.IBluetoothManager
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public android.bluetooth.IBluetooth registerAdapter(android.bluetooth.IBluetoothManagerCallback callback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.bluetooth.IBluetooth _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_registerAdapter, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().registerAdapter(callback);
          }
          _reply.readException();
          _result = android.bluetooth.IBluetooth.Stub.asInterface(_reply.readStrongBinder());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void unregisterAdapter(android.bluetooth.IBluetoothManagerCallback callback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_unregisterAdapter, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unregisterAdapter(callback);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void registerStateChangeCallback(android.bluetooth.IBluetoothStateChangeCallback callback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_registerStateChangeCallback, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().registerStateChangeCallback(callback);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public void unregisterStateChangeCallback(android.bluetooth.IBluetoothStateChangeCallback callback) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_unregisterStateChangeCallback, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unregisterStateChangeCallback(callback);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public boolean isEnabled() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isEnabled, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isEnabled();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean enable(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_enable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().enable(packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean enableNoAutoConnect(java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_enableNoAutoConnect, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().enableNoAutoConnect(packageName);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean disable(java.lang.String packageName, boolean persist) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeString(packageName);
          _data.writeInt(((persist)?(1):(0)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_disable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().disable(packageName, persist);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int getState() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getState, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getState();
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public android.bluetooth.IBluetoothGatt getBluetoothGatt() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.bluetooth.IBluetoothGatt _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getBluetoothGatt, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getBluetoothGatt();
          }
          _reply.readException();
          _result = android.bluetooth.IBluetoothGatt.Stub.asInterface(_reply.readStrongBinder());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean bindBluetoothProfileService(int profile, android.bluetooth.IBluetoothProfileServiceConnection proxy) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(profile);
          _data.writeStrongBinder((((proxy!=null))?(proxy.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_bindBluetoothProfileService, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().bindBluetoothProfileService(profile, proxy);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public void unbindBluetoothProfileService(int profile, android.bluetooth.IBluetoothProfileServiceConnection proxy) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(profile);
          _data.writeStrongBinder((((proxy!=null))?(proxy.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_unbindBluetoothProfileService, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().unbindBluetoothProfileService(profile, proxy);
            return;
          }
          _reply.readException();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
      }
      @Override public java.lang.String getAddress() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getAddress, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getAddress();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public java.lang.String getName() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getName();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean onFactoryReset() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_onFactoryReset, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().onFactoryReset();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isBleScanAlwaysAvailable() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isBleScanAlwaysAvailable, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isBleScanAlwaysAvailable();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public int updateBleAppCount(android.os.IBinder b, boolean enable, java.lang.String packageName) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder(b);
          _data.writeInt(((enable)?(1):(0)));
          _data.writeString(packageName);
          boolean _status = mRemote.transact(Stub.TRANSACTION_updateBleAppCount, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().updateBleAppCount(b, enable, packageName);
          }
          _reply.readException();
          _result = _reply.readInt();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isBleAppPresent() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isBleAppPresent, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isBleAppPresent();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      @Override public boolean isHearingAidProfileSupported() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_isHearingAidProfileSupported, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().isHearingAidProfileSupported();
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      public static android.bluetooth.IBluetoothManager sDefaultImpl;
    }
    static final int TRANSACTION_registerAdapter = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_unregisterAdapter = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_registerStateChangeCallback = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_unregisterStateChangeCallback = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_isEnabled = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_enable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_enableNoAutoConnect = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    static final int TRANSACTION_disable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
    static final int TRANSACTION_getState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
    static final int TRANSACTION_getBluetoothGatt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
    static final int TRANSACTION_bindBluetoothProfileService = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
    static final int TRANSACTION_unbindBluetoothProfileService = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
    static final int TRANSACTION_getAddress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
    static final int TRANSACTION_getName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
    static final int TRANSACTION_onFactoryReset = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
    static final int TRANSACTION_isBleScanAlwaysAvailable = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
    static final int TRANSACTION_updateBleAppCount = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
    static final int TRANSACTION_isBleAppPresent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
    static final int TRANSACTION_isHearingAidProfileSupported = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
    public static boolean setDefaultImpl(android.bluetooth.IBluetoothManager impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.bluetooth.IBluetoothManager getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public android.bluetooth.IBluetooth registerAdapter(android.bluetooth.IBluetoothManagerCallback callback) throws android.os.RemoteException;
  public void unregisterAdapter(android.bluetooth.IBluetoothManagerCallback callback) throws android.os.RemoteException;
  public void registerStateChangeCallback(android.bluetooth.IBluetoothStateChangeCallback callback) throws android.os.RemoteException;
  public void unregisterStateChangeCallback(android.bluetooth.IBluetoothStateChangeCallback callback) throws android.os.RemoteException;
  public boolean isEnabled() throws android.os.RemoteException;
  public boolean enable(java.lang.String packageName) throws android.os.RemoteException;
  public boolean enableNoAutoConnect(java.lang.String packageName) throws android.os.RemoteException;
  public boolean disable(java.lang.String packageName, boolean persist) throws android.os.RemoteException;
  public int getState() throws android.os.RemoteException;
  public android.bluetooth.IBluetoothGatt getBluetoothGatt() throws android.os.RemoteException;
  public boolean bindBluetoothProfileService(int profile, android.bluetooth.IBluetoothProfileServiceConnection proxy) throws android.os.RemoteException;
  public void unbindBluetoothProfileService(int profile, android.bluetooth.IBluetoothProfileServiceConnection proxy) throws android.os.RemoteException;
  public java.lang.String getAddress() throws android.os.RemoteException;
  public java.lang.String getName() throws android.os.RemoteException;
  public boolean onFactoryReset() throws android.os.RemoteException;
  public boolean isBleScanAlwaysAvailable() throws android.os.RemoteException;
  public int updateBleAppCount(android.os.IBinder b, boolean enable, java.lang.String packageName) throws android.os.RemoteException;
  public boolean isBleAppPresent() throws android.os.RemoteException;
  public boolean isHearingAidProfileSupported() throws android.os.RemoteException;
}
