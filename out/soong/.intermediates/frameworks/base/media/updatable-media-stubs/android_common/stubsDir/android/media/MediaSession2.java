/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.media;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * This API is not generally intended for third party application developers.
 * Use the <a href="{@docRoot}jetpack/androidx.html">AndroidX</a>
 * <a href="{@docRoot}reference/androidx/media2/session/package-summary.html">Media2 session
 * Library</a> for consistent behavior across all devices.
 * <p>
 * Allows a media app to expose its transport controls and playback information in a process to
 * other processes including the Android framework and other apps.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MediaSession2 implements java.lang.AutoCloseable {

MediaSession2(android.content.Context context, java.lang.String id, android.app.PendingIntent sessionActivity, java.util.concurrent.Executor callbackExecutor, android.media.MediaSession2.SessionCallback callback, android.os.Bundle tokenExtras) { throw new RuntimeException("Stub!"); }

public void close() { throw new RuntimeException("Stub!"); }

/**
 * Returns the session ID

 * @return This value will never be {@code null}.
 */

public java.lang.String getId() { throw new RuntimeException("Stub!"); }

/**
 * Returns the {@link Session2Token} for creating {@link MediaController2}.

 * @return This value will never be {@code null}.
 */

public android.media.Session2Token getToken() { throw new RuntimeException("Stub!"); }

/**
 * Broadcasts a session command to all the connected controllers
 * <p>
 * @param command the session command
 * This value must never be {@code null}.
 * @param args optional arguments

 * This value may be {@code null}.
 */

public void broadcastSessionCommand(android.media.Session2Command command, android.os.Bundle args) { throw new RuntimeException("Stub!"); }

/**
 * Sends a session command to a specific controller
 * <p>
 * @param controller the controller to get the session command
 * This value must never be {@code null}.
 * @param command the session command
 * This value must never be {@code null}.
 * @param args optional arguments
 * This value may be {@code null}.
 * @return a token which will be sent together in {@link SessionCallback#onCommandResult}
 *     when its result is received.
 
 * This value will never be {@code null}.
 */

public java.lang.Object sendSessionCommand(android.media.MediaSession2.ControllerInfo controller, android.media.Session2Command command, android.os.Bundle args) { throw new RuntimeException("Stub!"); }

/**
 * Cancels the session command previously sent.
 *
 * @param controller the controller to get the session command
 * This value must never be {@code null}.
 * @param token the token which is returned from {@link #sendSessionCommand}.

 * This value must never be {@code null}.
 */

public void cancelSessionCommand(android.media.MediaSession2.ControllerInfo controller, java.lang.Object token) { throw new RuntimeException("Stub!"); }

/**
 * Sets whether the playback is active (i.e. playing something)
 *
 * @param playbackActive {@code true} if the playback active, {@code false} otherwise.
 **/

public void setPlaybackActive(boolean playbackActive) { throw new RuntimeException("Stub!"); }

/**
 * Returns whehther the playback is active (i.e. playing something)
 *
 * @return {@code true} if the playback active, {@code false} otherwise.
 */

public boolean isPlaybackActive() { throw new RuntimeException("Stub!"); }

/**
 * Gets the list of the connected controllers
 *
 * @return list of the connected controllers.

 * This value will never be {@code null}.
 */

public java.util.List<android.media.MediaSession2.ControllerInfo> getConnectedControllers() { throw new RuntimeException("Stub!"); }
/**
 * This API is not generally intended for third party application developers.
 * Use the <a href="{@docRoot}jetpack/androidx.html">AndroidX</a>
 * <a href="{@docRoot}reference/androidx/media2/session/package-summary.html">Media2 session
 * Library</a> for consistent behavior across all devices.
 * <p>
 * Builder for {@link MediaSession2}.
 * <p>
 * Any incoming event from the {@link MediaController2} will be handled on the callback
 * executor. If it's not set, {@link Context#getMainExecutor()} will be used by default.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class Builder {

/**
 * Creates a builder for {@link MediaSession2}.
 *
 * @param context Context
 * This value must never be {@code null}.
 * @throws IllegalArgumentException if context is {@code null}.
 */

public Builder(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Set an intent for launching UI for this Session. This can be used as a
 * quick link to an ongoing media screen. The intent should be for an
 * activity that may be started using {@link Context#startActivity(Intent)}.
 *
 * @param pi The intent to launch to show UI for this session.
 * This value may be {@code null}.
 * @return The Builder to allow chaining
 
 * This value will never be {@code null}.
 */

public android.media.MediaSession2.Builder setSessionActivity(android.app.PendingIntent pi) { throw new RuntimeException("Stub!"); }

/**
 * Set ID of the session. If it's not set, an empty string will be used to create a session.
 * <p>
 * Use this if and only if your app supports multiple playback at the same time and also
 * wants to provide external apps to have finer controls of them.
 *
 * @param id id of the session. Must be unique per package.
 * This value must never be {@code null}.
 * @throws IllegalArgumentException if id is {@code null}.
 * @return The Builder to allow chaining
 */

public android.media.MediaSession2.Builder setId(java.lang.String id) { throw new RuntimeException("Stub!"); }

/**
 * Set callback for the session and its executor.
 *
 * @param executor callback executor
 * This value must never be {@code null}.
 * @param callback session callback.
 * This value must never be {@code null}.
 * @return The Builder to allow chaining
 
 * This value will never be {@code null}.
 */

public android.media.MediaSession2.Builder setSessionCallback(java.util.concurrent.Executor executor, android.media.MediaSession2.SessionCallback callback) { throw new RuntimeException("Stub!"); }

/**
 * Set extras for the session token. If null or not set, {@link Session2Token#getExtras()}
 * will return an empty {@link Bundle}. An {@link IllegalArgumentException} will be thrown
 * if the bundle contains any non-framework Parcelable objects.
 *
 * @param extras This value must never be {@code null}.
 * @return The Builder to allow chaining
 * @see Session2Token#getExtras()
 */

public android.media.MediaSession2.Builder setExtras(android.os.Bundle extras) { throw new RuntimeException("Stub!"); }

/**
 * Build {@link MediaSession2}.
 *
 * @return a new session
 * This value will never be {@code null}.
 * @throws IllegalStateException if the session with the same id is already exists for the
 *      package.
 */

public android.media.MediaSession2 build() { throw new RuntimeException("Stub!"); }
}

/**
 * This API is not generally intended for third party application developers.
 * Use the <a href="{@docRoot}jetpack/androidx.html">AndroidX</a>
 * <a href="{@docRoot}reference/androidx/media2/session/package-summary.html">Media2 session
 * Library</a> for consistent behavior across all devices.
 * <p>
 * Information of a controller.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class ControllerInfo {

ControllerInfo() { throw new RuntimeException("Stub!"); }

/**
 * @return remote user info of the controller.

 * This value will never be {@code null}.
 */

public android.media.session.MediaSessionManager.RemoteUserInfo getRemoteUserInfo() { throw new RuntimeException("Stub!"); }

/**
 * @return package name of the controller.

 * This value will never be {@code null}.
 */

public java.lang.String getPackageName() { throw new RuntimeException("Stub!"); }

/**
 * @return uid of the controller. Can be a negative value if the uid cannot be obtained.
 */

public int getUid() { throw new RuntimeException("Stub!"); }

/**
 * @return connection hints sent from controller.

 * This value will never be {@code null}.
 */

public android.os.Bundle getConnectionHints() { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @param obj This value may be {@code null}.
 */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * {@inheritDoc}

 * @return This value will never be {@code null}.
 */

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

/**
 * This API is not generally intended for third party application developers.
 * Use the <a href="{@docRoot}jetpack/androidx.html">AndroidX</a>
 * <a href="{@docRoot}reference/androidx/media2/session/package-summary.html">Media2 session
 * Library</a> for consistent behavior across all devices.
 * <p>
 * Callback to be called for all incoming commands from {@link MediaController2}s.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class SessionCallback {

public SessionCallback() { throw new RuntimeException("Stub!"); }

/**
 * Called when a controller is created for this session. Return allowed commands for
 * controller. By default it returns {@code null}.
 * <p>
 * You can reject the connection by returning {@code null}. In that case, controller
 * receives {@link MediaController2.ControllerCallback#onDisconnected(MediaController2)}
 * and cannot be used.
 * <p>
 * The controller hasn't connected yet in this method, so calls to the controller
 * (e.g. {@link #sendSessionCommand}) would be ignored. Override {@link #onPostConnect} for
 * the custom initialization for the controller instead.
 *
 * @param session the session for this event
 * This value must never be {@code null}.
 * @param controller controller information.
 * This value must never be {@code null}.
 * @return allowed commands. Can be {@code null} to reject connection.
 */

public android.media.Session2CommandGroup onConnect(android.media.MediaSession2 session, android.media.MediaSession2.ControllerInfo controller) { throw new RuntimeException("Stub!"); }

/**
 * Called immediately after a controller is connected. This is a convenient method to add
 * custom initialization between the session and a controller.
 * <p>
 * Note that calls to the controller (e.g. {@link #sendSessionCommand}) work here but don't
 * work in {@link #onConnect} because the controller hasn't connected yet in
 * {@link #onConnect}.
 *
 * @param session the session for this event
 * This value must never be {@code null}.
 * @param controller controller information.

 * This value must never be {@code null}.
 */

public void onPostConnect(android.media.MediaSession2 session, android.media.MediaSession2.ControllerInfo controller) { throw new RuntimeException("Stub!"); }

/**
 * Called when a controller is disconnected
 *
 * @param session the session for this event
 * This value must never be {@code null}.
 * @param controller controller information

 * This value must never be {@code null}.
 */

public void onDisconnected(android.media.MediaSession2 session, android.media.MediaSession2.ControllerInfo controller) { throw new RuntimeException("Stub!"); }

/**
 * Called when a controller sent a session command.
 *
 * @param session the session for this event
 * This value must never be {@code null}.
 * @param controller controller information
 * This value must never be {@code null}.
 * @param command the session command
 * This value must never be {@code null}.
 * @param args optional arguments
 * This value may be {@code null}.
 * @return the result for the session command. If {@code null}, RESULT_INFO_SKIPPED
 *         will be sent to the session.
 */

public android.media.Session2Command.Result onSessionCommand(android.media.MediaSession2 session, android.media.MediaSession2.ControllerInfo controller, android.media.Session2Command command, android.os.Bundle args) { throw new RuntimeException("Stub!"); }

/**
 * Called when the command sent to the controller is finished.
 *
 * @param session the session for this event
 * This value must never be {@code null}.
 * @param controller controller information
 * This value must never be {@code null}.
 * @param token the token got from {@link MediaSession2#sendSessionCommand}
 * This value must never be {@code null}.
 * @param command the session command
 * This value must never be {@code null}.
 * @param result the result of the session command

 * This value must never be {@code null}.
 */

public void onCommandResult(android.media.MediaSession2 session, android.media.MediaSession2.ControllerInfo controller, java.lang.Object token, android.media.Session2Command command, android.media.Session2Command.Result result) { throw new RuntimeException("Stub!"); }
}

}

