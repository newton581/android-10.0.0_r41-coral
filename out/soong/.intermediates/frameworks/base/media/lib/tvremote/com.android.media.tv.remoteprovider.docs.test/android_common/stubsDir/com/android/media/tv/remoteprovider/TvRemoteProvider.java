/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.media.tv.remoteprovider;

import android.content.Context;
import android.os.IBinder;

/**
 * Base class for emote providers implemented in unbundled service.
 * <p/>
 * This object is not thread safe.  It is only intended to be accessed on the
 * {@link Context#getMainLooper main looper thread} of an application.
 * </p><p>
 * IMPORTANT: This class is effectively a system API for unbundled emote service, and
 * must remain API stable. See README.txt in the root of this package for more information.
 * </p>
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class TvRemoteProvider {

/**
 * Creates a provider for an unbundled emote controller
 * service allowing it to interface with the tv remote controller
 * system service.
 *
 * @param context The application context for the remote provider.
 */

public TvRemoteProvider(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Gets the context of the remote service provider.
 */

public final android.content.Context getContext() { throw new RuntimeException("Stub!"); }

/**
 * Gets the Binder associated with the provider.
 * <p>
 * This is intended to be used for the onBind() method of a service that implements
 * a remote provider service.
 * </p>
 *
 * @return The IBinder instance associated with the provider.
 */

public android.os.IBinder getBinder() { throw new RuntimeException("Stub!"); }

/**
 * Information about the InputBridge connected status.
 *
 * @param token Identifier for the connection. Null, if failed.
 */

public void onInputBridgeConnected(android.os.IBinder token) { throw new RuntimeException("Stub!"); }

/**
 * openRemoteInputBridge : Open an input bridge for a particular device.
 * Clients should pass in a token that can be used to match this request with a token that
 * will be returned by {@link TvRemoteProvider#onInputBridgeConnected(IBinder token)}
 * <p>
 * The token should be used for subsequent calls.
 * </p>
 *
 * @param name        Device name
 * @param token       Identifier for this connection
 * @param width       Width of the device's virtual touchpad
 * @param height      Height of the device's virtual touchpad
 * @param maxPointers Maximum supported pointers
 * @throws RuntimeException
 */

public void openRemoteInputBridge(android.os.IBinder token, java.lang.String name, int width, int height, int maxPointers) throws java.lang.RuntimeException { throw new RuntimeException("Stub!"); }

/**
 * closeInputBridge : Close input bridge for a device
 *
 * @param token identifier for this connection
 * @throws RuntimeException
 */

public void closeInputBridge(android.os.IBinder token) throws java.lang.RuntimeException { throw new RuntimeException("Stub!"); }

/**
 * clearInputBridge : Clear out any existing key or pointer events in queue for this device by
 *                    dropping them on the floor and sending an UP to all keys and pointer
 *                    slots.
 *
 * @param token identifier for this connection
 * @throws RuntimeException
 */

public void clearInputBridge(android.os.IBinder token) throws java.lang.RuntimeException { throw new RuntimeException("Stub!"); }

/**
 * sendTimestamp : Send a timestamp for a set of pointer events
 *
 * @param token     identifier for the device
 * @param timestamp Timestamp to be used in
 *                  {@link android.os.SystemClock#uptimeMillis} time base
 * @throws RuntimeException
 */

public void sendTimestamp(android.os.IBinder token, long timestamp) throws java.lang.RuntimeException { throw new RuntimeException("Stub!"); }

/**
 * sendKeyUp : Send key up event for a device
 *
 * @param token   identifier for this connection
 * @param keyCode Key code to be sent
 * @throws RuntimeException
 */

public void sendKeyUp(android.os.IBinder token, int keyCode) throws java.lang.RuntimeException { throw new RuntimeException("Stub!"); }

/**
 * sendKeyDown : Send key down event for a device
 *
 * @param token   identifier for this connection
 * @param keyCode Key code to be sent
 * @throws RuntimeException
 */

public void sendKeyDown(android.os.IBinder token, int keyCode) throws java.lang.RuntimeException { throw new RuntimeException("Stub!"); }

/**
 * sendPointerUp : Send pointer up event for a device
 *
 * @param token     identifier for the device
 * @param pointerId Pointer id to be used. Value may be from 0
 *                  to {@link MotionEvent#getPointerCount()} -1
 * @throws RuntimeException
 */

public void sendPointerUp(android.os.IBinder token, int pointerId) throws java.lang.RuntimeException { throw new RuntimeException("Stub!"); }

/**
 * sendPointerDown : Send pointer down event for a device
 *
 * @param token     identifier for the device
 * @param pointerId Pointer id to be used. Value may be from 0
 *                  to {@link MotionEvent#getPointerCount()} -1
 * @param x         X co-ordinates in display pixels
 * @param y         Y co-ordinates in display pixels
 * @throws RuntimeException
 */

public void sendPointerDown(android.os.IBinder token, int pointerId, int x, int y) throws java.lang.RuntimeException { throw new RuntimeException("Stub!"); }

/**
 * sendPointerSync : Send pointer sync event for a device
 *
 * @param token identifier for the device
 * @throws RuntimeException
 */

public void sendPointerSync(android.os.IBinder token) throws java.lang.RuntimeException { throw new RuntimeException("Stub!"); }

/**
 * The {@link Intent} that must be declared as handled by the service.
 * The service must also require the {@link android.Manifest.permission#BIND_TV_REMOTE_SERVICE}
 * permission so that other applications cannot abuse it.
 */

public static final java.lang.String SERVICE_INTERFACE = "com.android.media.tv.remoteprovider.TvRemoteProvider";
}

