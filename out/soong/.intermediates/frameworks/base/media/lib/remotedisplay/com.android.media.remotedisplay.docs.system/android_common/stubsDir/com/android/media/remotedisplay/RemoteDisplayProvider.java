/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.media.remotedisplay;

import android.content.Context;
import android.os.IBinder;

/**
 * Base class for remote display providers implemented as unbundled services.
 * <p>
 * To implement your remote display provider service, create a subclass of
 * {@link Service} and override the {@link Service#onBind Service.onBind()} method
 * to return the provider's binder when the {@link #SERVICE_INTERFACE} is requested.
 * </p>
 * <pre>
 *   public class SampleRemoteDisplayProviderService extends Service {
 *       private SampleProvider mProvider;
 *
 *       public IBinder onBind(Intent intent) {
 *           if (intent.getAction().equals(RemoteDisplayProvider.SERVICE_INTERFACE)) {
 *               if (mProvider == null) {
 *                   mProvider = new SampleProvider(this);
 *               }
 *               return mProvider.getBinder();
 *           }
 *           return null;
 *       }
 *
 *       class SampleProvider extends RemoteDisplayProvider {
 *           public SampleProvider() {
 *               super(SampleRemoteDisplayProviderService.this);
 *           }
 *
 *           // --- Implementation goes here ---
 *       }
 *   }
 * </pre>
 * <p>
 * Declare your remote display provider service in your application manifest
 * like this:
 * </p>
 * <pre>
 *   &lt;application>
 *       &lt;uses-library android:name="com.android.media.remotedisplay" />
 *
 *       &lt;service android:name=".SampleRemoteDisplayProviderService"
 *               android:label="@string/sample_remote_display_provider_service"
 *               android:exported="true"
 *               android:permission="android.permission.BIND_REMOTE_DISPLAY">
 *           &lt;intent-filter>
 *               &lt;action android:name="com.android.media.remotedisplay.RemoteDisplayProvider" />
 *           &lt;/intent-filter>
 *       &lt;/service>
 *   &lt;/application>
 * </pre>
 * <p>
 * This object is not thread safe.  It is only intended to be accessed on the
 * {@link Context#getMainLooper main looper thread} of an application.
 * </p><p>
 * IMPORTANT: This class is effectively a public API for unbundled applications, and
 * must remain API stable. See README.txt in the root of this package for more information.
 * </p>
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class RemoteDisplayProvider {

/**
 * Creates a remote display provider.
 *
 * @param context The application context for the remote display provider.
 */

public RemoteDisplayProvider(android.content.Context context) { throw new RuntimeException("Stub!"); }

/**
 * Gets the context of the remote display provider.
 */

public final android.content.Context getContext() { throw new RuntimeException("Stub!"); }

/**
 * Gets the Binder associated with the provider.
 * <p>
 * This is intended to be used for the onBind() method of a service that implements
 * a remote display provider service.
 * </p>
 *
 * @return The IBinder instance associated with the provider.
 */

public android.os.IBinder getBinder() { throw new RuntimeException("Stub!"); }

/**
 * Called when the current discovery mode changes.
 *
 * @param mode The new discovery mode.
 */

public void onDiscoveryModeChanged(int mode) { throw new RuntimeException("Stub!"); }

/**
 * Called when the system would like to connect to a display.
 *
 * @param display The remote display.
 */

public void onConnect(com.android.media.remotedisplay.RemoteDisplay display) { throw new RuntimeException("Stub!"); }

/**
 * Called when the system would like to disconnect from a display.
 *
 * @param display The remote display.
 */

public void onDisconnect(com.android.media.remotedisplay.RemoteDisplay display) { throw new RuntimeException("Stub!"); }

/**
 * Called when the system would like to set the volume of a display.
 *
 * @param display The remote display.
 * @param volume The desired volume.
 */

public void onSetVolume(com.android.media.remotedisplay.RemoteDisplay display, int volume) { throw new RuntimeException("Stub!"); }

/**
 * Called when the system would like to adjust the volume of a display.
 *
 * @param display The remote display.
 * @param delta An increment to add to the current volume, such as +1 or -1.
 */

public void onAdjustVolume(com.android.media.remotedisplay.RemoteDisplay display, int delta) { throw new RuntimeException("Stub!"); }

/**
 * Gets the current discovery mode.
 *
 * @return The current discovery mode.
 */

public int getDiscoveryMode() { throw new RuntimeException("Stub!"); }

/**
 * Gets the current collection of displays.
 *
 * @return The current collection of displays, which must not be modified.
 */

public java.util.Collection<com.android.media.remotedisplay.RemoteDisplay> getDisplays() { throw new RuntimeException("Stub!"); }

/**
 * Adds the specified remote display and notifies the system.
 *
 * @param display The remote display that was added.
 * @throws IllegalStateException if there is already a display with the same id.
 */

public void addDisplay(com.android.media.remotedisplay.RemoteDisplay display) { throw new RuntimeException("Stub!"); }

/**
 * Updates information about the specified remote display and notifies the system.
 *
 * @param display The remote display that was added.
 * @throws IllegalStateException if the display was n
 */

public void updateDisplay(com.android.media.remotedisplay.RemoteDisplay display) { throw new RuntimeException("Stub!"); }

/**
 * Removes the specified remote display and tells the system about it.
 *
 * @param display The remote display that was removed.
 */

public void removeDisplay(com.android.media.remotedisplay.RemoteDisplay display) { throw new RuntimeException("Stub!"); }

/**
 * Finds the remote display with the specified id, returns null if not found.
 *
 * @param id Id of the remote display.
 * @return The display, or null if none.
 */

public com.android.media.remotedisplay.RemoteDisplay findRemoteDisplay(java.lang.String id) { throw new RuntimeException("Stub!"); }

/**
 * Gets a pending intent to launch the remote display settings activity.
 *
 * @return A pending intent to launch the settings activity.
 */

public android.app.PendingIntent getSettingsPendingIntent() { throw new RuntimeException("Stub!"); }

/**
 * Discovery mode: Active discovery.
 * <p>
 * This mode indicates that the user is actively trying to connect to a route
 * and we should perform continuous scans.  This mode may use significantly more
 * power but is intended to be short-lived.
 * </p>
 */

public static final int DISCOVERY_MODE_ACTIVE = 2; // 0x2

/**
 * Discovery mode: Do not perform any discovery.
 */

public static final int DISCOVERY_MODE_NONE = 0; // 0x0

/**
 * Discovery mode: Passive or low-power periodic discovery.
 * <p>
 * This mode indicates that an application is interested in knowing whether there
 * are any remote displays paired or available but doesn't need the latest or
 * most detailed information.  The provider may scan at a lower rate or rely on
 * knowledge of previously paired devices.
 * </p>
 */

public static final int DISCOVERY_MODE_PASSIVE = 1; // 0x1

/**
 * The {@link Intent} that must be declared as handled by the service.
 * Put this in your manifest.
 */

public static final java.lang.String SERVICE_INTERFACE = "com.android.media.remotedisplay.RemoteDisplayProvider";
}

