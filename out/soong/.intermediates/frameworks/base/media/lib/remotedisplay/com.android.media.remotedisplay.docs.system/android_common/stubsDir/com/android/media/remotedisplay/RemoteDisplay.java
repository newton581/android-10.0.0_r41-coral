/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.media.remotedisplay;


/**
 * Represents a remote display that has been discovered.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class RemoteDisplay {

/**
 * Creates a remote display with the specified name and id.
 */

public RemoteDisplay(java.lang.String id, java.lang.String name) { throw new RuntimeException("Stub!"); }

public java.lang.String getId() { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public java.lang.String getDescription() { throw new RuntimeException("Stub!"); }

public void setDescription(java.lang.String description) { throw new RuntimeException("Stub!"); }

public int getStatus() { throw new RuntimeException("Stub!"); }

public void setStatus(int status) { throw new RuntimeException("Stub!"); }

public int getVolume() { throw new RuntimeException("Stub!"); }

public void setVolume(int volume) { throw new RuntimeException("Stub!"); }

public int getVolumeMax() { throw new RuntimeException("Stub!"); }

public void setVolumeMax(int volumeMax) { throw new RuntimeException("Stub!"); }

public int getVolumeHandling() { throw new RuntimeException("Stub!"); }

public void setVolumeHandling(int volumeHandling) { throw new RuntimeException("Stub!"); }

public int getPresentationDisplayId() { throw new RuntimeException("Stub!"); }

public void setPresentationDisplayId(int presentationDisplayId) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Volume handling: Output volume is fixed.
 */

public static final int PLAYBACK_VOLUME_FIXED = 0; // 0x0

/**
 * Volume handling: Output volume can be changed.
 */

public static final int PLAYBACK_VOLUME_VARIABLE = 1; // 0x1

/**
 * Status code: Indicates that the remote display is available for new connections.
 */

public static final int STATUS_AVAILABLE = 2; // 0x2

/**
 * Status code: Indicates that the remote display is connected and is mirroring
 * display contents.
 */

public static final int STATUS_CONNECTED = 4; // 0x4

/**
 * Status code: Indicates that the remote display is current connecting.
 */

public static final int STATUS_CONNECTING = 3; // 0x3

/**
 * Status code: Indicates that the remote display is in use by someone else.
 */

public static final int STATUS_IN_USE = 1; // 0x1

/**
 * Status code: Indicates that the remote display is not available.
 */

public static final int STATUS_NOT_AVAILABLE = 0; // 0x0
}

