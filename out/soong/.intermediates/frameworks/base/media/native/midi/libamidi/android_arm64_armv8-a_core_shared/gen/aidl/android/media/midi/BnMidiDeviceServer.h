#ifndef AIDL_GENERATED_ANDROID_MEDIA_MIDI_BN_MIDI_DEVICE_SERVER_H_
#define AIDL_GENERATED_ANDROID_MEDIA_MIDI_BN_MIDI_DEVICE_SERVER_H_

#include <binder/IInterface.h>
#include <android/media/midi/IMidiDeviceServer.h>

namespace android {

namespace media {

namespace midi {

class BnMidiDeviceServer : public ::android::BnInterface<IMidiDeviceServer> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnMidiDeviceServer

}  // namespace midi

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_MIDI_BN_MIDI_DEVICE_SERVER_H_
