/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package com.google.android.startop.iorap;
/**
* IIOrap is a client interface to the input/output readahead and pin daemon (iorapd).
*
* The aim is to speed-up the cold start-up time of certain use-cases like application startup
* by utilizing trace-based pinning or readahead.
*
* Programatically, the behavior of iorapd should be treated like a black box. There is no
* "correctness", but only performance. By sending the right events at the appropriate time,
* we can squeeze more performance out of the system.
*
* If some events are not appropriately wired up, system performance may (temporarily) degrade.
*
* {@hide} */
public interface IIorap extends android.os.IInterface
{
  /** Default implementation for IIorap. */
  public static class Default implements com.google.android.startop.iorap.IIorap
  {
    /**
        * Set an ITaskListener which will be used to deliver notifications of in-progress/completition
        * for the onXEvent method calls below this.<br /><br />
        *
        * iorapd does all the work asynchronously and may deliver one or more onProgress events after
        * the event begins to be processed. It will always send back one onComplete that is considered
        * terminal.<br /><br />
        *
        * onProgress/onComplete are matched to the original event by the requestId. Once an onComplete
        * occurs for any given requestId, no further callbacks with the same requestId will occur.
        * It is illegal for the caller to reuse the same requestId on different invocations of IIorap.
        * <br /><br />
        *
        * onXEvent(id1) must be well-ordered w.r.t. onXEvent(id2), the assumption is that later
        * calls happen-after earlier calls and that id2 > id1. Decreasing request IDs will
        * immediately get rejected.
        * <br /><br />
        *
        * Sequence diagram of stereotypical successful event delivery and response notification:
        *
        * <pre>
        *
        *           ┌─────────────┐                ┌──────┐
        *           │system_server│                │iorapd│
        *           └──────┬──────┘                └──┬───┘
        *                  Request [01]: onSomeEvent ┌┴┐
        *                  │────────────────────────>│ │
        *                  │                         │ │
        *                  │                         │ │  ╔════════════════════════╗
        *                  │                         │ │  ║start processing event ░║
        *                  │                         │ │  ╚════════════════════════╝
        *                  │                         │ │
        * ╔═══════╤════════╪═════════════════════════╪═╪══════════════════════════════╗
        * ║ LOOP  │  1 or more times                 │ │                              ║
        * ╟───────┘        │                         │ │                              ║
        * ║                │Request [01]: onProgress │ │                              ║
        * ║                │<────────────────────────│ │                              ║
        * ║                │                         │ │                              ║
        * ║                │                         │ │────┐                         ║
        * ║                │                         │ │    │ workload in progress    ║
        * ║                │                         │ │<───┘                         ║
        * ╚════════════════╪═════════════════════════╪═╪══════════════════════════════╝
        *                  │                         └┬┘
        *                  .                          .
        *                  .                          .
        *                  .                          .
        *                  .                          .
        *                  .                          .
        *                  │                         ┌┴┐  ╔═════════════════════════╗
        *                  │                         │ │  ║finish processing event ░║
        *                  │                         │ │  ╚═════════════════════════╝
        *                  │Request [01]: onComplete │ │
        *                  │<────────────────────────│ │
        *           ┌──────┴──────┐                ┌─└┬┘──┐
        *           │system_server│                │iorapd│
        *           └─────────────┘                └──────┘
        *
        * </pre> <!-- system/iorap/docs/binder/IIorap_setTaskListener.plantuml -->
        */
    @Override public void setTaskListener(com.google.android.startop.iorap.ITaskListener listener) throws android.os.RemoteException
    {
    }
    // All callbacks will be done via the ITaskListener.
    // The RequestId passed in is the same RequestId sent back via the ITaskListener.
    // See above for more details.
    // Note: For each ${Type}Event, see the ${Type}Event.java for more documentation
    // in frameworks/base/startop/src/com/google/android/startop/iorap/${Type}Event.java
    // void onActivityHintEvent(in RequestId request, in ActivityHintEvent event);

    @Override public void onAppLaunchEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.AppLaunchEvent event) throws android.os.RemoteException
    {
    }
    @Override public void onPackageEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.PackageEvent event) throws android.os.RemoteException
    {
    }
    @Override public void onAppIntentEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.AppIntentEvent event) throws android.os.RemoteException
    {
    }
    @Override public void onSystemServiceEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.SystemServiceEvent event) throws android.os.RemoteException
    {
    }
    @Override public void onSystemServiceUserEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.SystemServiceUserEvent event) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements com.google.android.startop.iorap.IIorap
  {
    private static final java.lang.String DESCRIPTOR = "com.google.android.startop.iorap.IIorap";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an com.google.android.startop.iorap.IIorap interface,
     * generating a proxy if needed.
     */
    public static com.google.android.startop.iorap.IIorap asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof com.google.android.startop.iorap.IIorap))) {
        return ((com.google.android.startop.iorap.IIorap)iin);
      }
      return new com.google.android.startop.iorap.IIorap.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_setTaskListener:
        {
          data.enforceInterface(descriptor);
          com.google.android.startop.iorap.ITaskListener _arg0;
          _arg0 = com.google.android.startop.iorap.ITaskListener.Stub.asInterface(data.readStrongBinder());
          this.setTaskListener(_arg0);
          return true;
        }
        case TRANSACTION_onAppLaunchEvent:
        {
          data.enforceInterface(descriptor);
          com.google.android.startop.iorap.RequestId _arg0;
          if ((0!=data.readInt())) {
            _arg0 = com.google.android.startop.iorap.RequestId.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          com.google.android.startop.iorap.AppLaunchEvent _arg1;
          if ((0!=data.readInt())) {
            _arg1 = com.google.android.startop.iorap.AppLaunchEvent.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onAppLaunchEvent(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onPackageEvent:
        {
          data.enforceInterface(descriptor);
          com.google.android.startop.iorap.RequestId _arg0;
          if ((0!=data.readInt())) {
            _arg0 = com.google.android.startop.iorap.RequestId.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          com.google.android.startop.iorap.PackageEvent _arg1;
          if ((0!=data.readInt())) {
            _arg1 = com.google.android.startop.iorap.PackageEvent.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onPackageEvent(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onAppIntentEvent:
        {
          data.enforceInterface(descriptor);
          com.google.android.startop.iorap.RequestId _arg0;
          if ((0!=data.readInt())) {
            _arg0 = com.google.android.startop.iorap.RequestId.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          com.google.android.startop.iorap.AppIntentEvent _arg1;
          if ((0!=data.readInt())) {
            _arg1 = com.google.android.startop.iorap.AppIntentEvent.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onAppIntentEvent(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onSystemServiceEvent:
        {
          data.enforceInterface(descriptor);
          com.google.android.startop.iorap.RequestId _arg0;
          if ((0!=data.readInt())) {
            _arg0 = com.google.android.startop.iorap.RequestId.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          com.google.android.startop.iorap.SystemServiceEvent _arg1;
          if ((0!=data.readInt())) {
            _arg1 = com.google.android.startop.iorap.SystemServiceEvent.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onSystemServiceEvent(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onSystemServiceUserEvent:
        {
          data.enforceInterface(descriptor);
          com.google.android.startop.iorap.RequestId _arg0;
          if ((0!=data.readInt())) {
            _arg0 = com.google.android.startop.iorap.RequestId.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          com.google.android.startop.iorap.SystemServiceUserEvent _arg1;
          if ((0!=data.readInt())) {
            _arg1 = com.google.android.startop.iorap.SystemServiceUserEvent.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onSystemServiceUserEvent(_arg0, _arg1);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements com.google.android.startop.iorap.IIorap
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      /**
          * Set an ITaskListener which will be used to deliver notifications of in-progress/completition
          * for the onXEvent method calls below this.<br /><br />
          *
          * iorapd does all the work asynchronously and may deliver one or more onProgress events after
          * the event begins to be processed. It will always send back one onComplete that is considered
          * terminal.<br /><br />
          *
          * onProgress/onComplete are matched to the original event by the requestId. Once an onComplete
          * occurs for any given requestId, no further callbacks with the same requestId will occur.
          * It is illegal for the caller to reuse the same requestId on different invocations of IIorap.
          * <br /><br />
          *
          * onXEvent(id1) must be well-ordered w.r.t. onXEvent(id2), the assumption is that later
          * calls happen-after earlier calls and that id2 > id1. Decreasing request IDs will
          * immediately get rejected.
          * <br /><br />
          *
          * Sequence diagram of stereotypical successful event delivery and response notification:
          *
          * <pre>
          *
          *           ┌─────────────┐                ┌──────┐
          *           │system_server│                │iorapd│
          *           └──────┬──────┘                └──┬───┘
          *                  Request [01]: onSomeEvent ┌┴┐
          *                  │────────────────────────>│ │
          *                  │                         │ │
          *                  │                         │ │  ╔════════════════════════╗
          *                  │                         │ │  ║start processing event ░║
          *                  │                         │ │  ╚════════════════════════╝
          *                  │                         │ │
          * ╔═══════╤════════╪═════════════════════════╪═╪══════════════════════════════╗
          * ║ LOOP  │  1 or more times                 │ │                              ║
          * ╟───────┘        │                         │ │                              ║
          * ║                │Request [01]: onProgress │ │                              ║
          * ║                │<────────────────────────│ │                              ║
          * ║                │                         │ │                              ║
          * ║                │                         │ │────┐                         ║
          * ║                │                         │ │    │ workload in progress    ║
          * ║                │                         │ │<───┘                         ║
          * ╚════════════════╪═════════════════════════╪═╪══════════════════════════════╝
          *                  │                         └┬┘
          *                  .                          .
          *                  .                          .
          *                  .                          .
          *                  .                          .
          *                  .                          .
          *                  │                         ┌┴┐  ╔═════════════════════════╗
          *                  │                         │ │  ║finish processing event ░║
          *                  │                         │ │  ╚═════════════════════════╝
          *                  │Request [01]: onComplete │ │
          *                  │<────────────────────────│ │
          *           ┌──────┴──────┐                ┌─└┬┘──┐
          *           │system_server│                │iorapd│
          *           └─────────────┘                └──────┘
          *
          * </pre> <!-- system/iorap/docs/binder/IIorap_setTaskListener.plantuml -->
          */
      @Override public void setTaskListener(com.google.android.startop.iorap.ITaskListener listener) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeStrongBinder((((listener!=null))?(listener.asBinder()):(null)));
          boolean _status = mRemote.transact(Stub.TRANSACTION_setTaskListener, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().setTaskListener(listener);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      // All callbacks will be done via the ITaskListener.
      // The RequestId passed in is the same RequestId sent back via the ITaskListener.
      // See above for more details.
      // Note: For each ${Type}Event, see the ${Type}Event.java for more documentation
      // in frameworks/base/startop/src/com/google/android/startop/iorap/${Type}Event.java
      // void onActivityHintEvent(in RequestId request, in ActivityHintEvent event);

      @Override public void onAppLaunchEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.AppLaunchEvent event) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((request!=null)) {
            _data.writeInt(1);
            request.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((event!=null)) {
            _data.writeInt(1);
            event.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onAppLaunchEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onAppLaunchEvent(request, event);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onPackageEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.PackageEvent event) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((request!=null)) {
            _data.writeInt(1);
            request.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((event!=null)) {
            _data.writeInt(1);
            event.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onPackageEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onPackageEvent(request, event);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onAppIntentEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.AppIntentEvent event) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((request!=null)) {
            _data.writeInt(1);
            request.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((event!=null)) {
            _data.writeInt(1);
            event.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onAppIntentEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onAppIntentEvent(request, event);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onSystemServiceEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.SystemServiceEvent event) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((request!=null)) {
            _data.writeInt(1);
            request.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((event!=null)) {
            _data.writeInt(1);
            event.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onSystemServiceEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onSystemServiceEvent(request, event);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onSystemServiceUserEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.SystemServiceUserEvent event) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((request!=null)) {
            _data.writeInt(1);
            request.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((event!=null)) {
            _data.writeInt(1);
            event.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onSystemServiceUserEvent, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onSystemServiceUserEvent(request, event);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static com.google.android.startop.iorap.IIorap sDefaultImpl;
    }
    static final int TRANSACTION_setTaskListener = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onAppLaunchEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_onPackageEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_onAppIntentEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_onSystemServiceEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_onSystemServiceUserEvent = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    public static boolean setDefaultImpl(com.google.android.startop.iorap.IIorap impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static com.google.android.startop.iorap.IIorap getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  /**
      * Set an ITaskListener which will be used to deliver notifications of in-progress/completition
      * for the onXEvent method calls below this.<br /><br />
      *
      * iorapd does all the work asynchronously and may deliver one or more onProgress events after
      * the event begins to be processed. It will always send back one onComplete that is considered
      * terminal.<br /><br />
      *
      * onProgress/onComplete are matched to the original event by the requestId. Once an onComplete
      * occurs for any given requestId, no further callbacks with the same requestId will occur.
      * It is illegal for the caller to reuse the same requestId on different invocations of IIorap.
      * <br /><br />
      *
      * onXEvent(id1) must be well-ordered w.r.t. onXEvent(id2), the assumption is that later
      * calls happen-after earlier calls and that id2 > id1. Decreasing request IDs will
      * immediately get rejected.
      * <br /><br />
      *
      * Sequence diagram of stereotypical successful event delivery and response notification:
      *
      * <pre>
      *
      *           ┌─────────────┐                ┌──────┐
      *           │system_server│                │iorapd│
      *           └──────┬──────┘                └──┬───┘
      *                  Request [01]: onSomeEvent ┌┴┐
      *                  │────────────────────────>│ │
      *                  │                         │ │
      *                  │                         │ │  ╔════════════════════════╗
      *                  │                         │ │  ║start processing event ░║
      *                  │                         │ │  ╚════════════════════════╝
      *                  │                         │ │
      * ╔═══════╤════════╪═════════════════════════╪═╪══════════════════════════════╗
      * ║ LOOP  │  1 or more times                 │ │                              ║
      * ╟───────┘        │                         │ │                              ║
      * ║                │Request [01]: onProgress │ │                              ║
      * ║                │<────────────────────────│ │                              ║
      * ║                │                         │ │                              ║
      * ║                │                         │ │────┐                         ║
      * ║                │                         │ │    │ workload in progress    ║
      * ║                │                         │ │<───┘                         ║
      * ╚════════════════╪═════════════════════════╪═╪══════════════════════════════╝
      *                  │                         └┬┘
      *                  .                          .
      *                  .                          .
      *                  .                          .
      *                  .                          .
      *                  .                          .
      *                  │                         ┌┴┐  ╔═════════════════════════╗
      *                  │                         │ │  ║finish processing event ░║
      *                  │                         │ │  ╚═════════════════════════╝
      *                  │Request [01]: onComplete │ │
      *                  │<────────────────────────│ │
      *           ┌──────┴──────┐                ┌─└┬┘──┐
      *           │system_server│                │iorapd│
      *           └─────────────┘                └──────┘
      *
      * </pre> <!-- system/iorap/docs/binder/IIorap_setTaskListener.plantuml -->
      */
  public void setTaskListener(com.google.android.startop.iorap.ITaskListener listener) throws android.os.RemoteException;
  // All callbacks will be done via the ITaskListener.
  // The RequestId passed in is the same RequestId sent back via the ITaskListener.
  // See above for more details.
  // Note: For each ${Type}Event, see the ${Type}Event.java for more documentation
  // in frameworks/base/startop/src/com/google/android/startop/iorap/${Type}Event.java
  // void onActivityHintEvent(in RequestId request, in ActivityHintEvent event);

  public void onAppLaunchEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.AppLaunchEvent event) throws android.os.RemoteException;
  public void onPackageEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.PackageEvent event) throws android.os.RemoteException;
  public void onAppIntentEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.AppIntentEvent event) throws android.os.RemoteException;
  public void onSystemServiceEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.SystemServiceEvent event) throws android.os.RemoteException;
  public void onSystemServiceUserEvent(com.google.android.startop.iorap.RequestId request, com.google.android.startop.iorap.SystemServiceUserEvent event) throws android.os.RemoteException;
}
