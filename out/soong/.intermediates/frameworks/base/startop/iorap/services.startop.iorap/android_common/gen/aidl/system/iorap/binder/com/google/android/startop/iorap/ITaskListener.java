/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package com.google.android.startop.iorap;
/**
* Provide callbacks to the {@code IIorap} client in response to method invocations.<br /><br />
*
* @see com.google.android.startop.iorap.IIorap
*
* {@hide} */
public interface ITaskListener extends android.os.IInterface
{
  /** Default implementation for ITaskListener. */
  public static class Default implements com.google.android.startop.iorap.ITaskListener
  {
    @Override public void onProgress(com.google.android.startop.iorap.RequestId requestId, com.google.android.startop.iorap.TaskResult result) throws android.os.RemoteException
    {
    }
    @Override public void onComplete(com.google.android.startop.iorap.RequestId requestId, com.google.android.startop.iorap.TaskResult result) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements com.google.android.startop.iorap.ITaskListener
  {
    private static final java.lang.String DESCRIPTOR = "com.google.android.startop.iorap.ITaskListener";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an com.google.android.startop.iorap.ITaskListener interface,
     * generating a proxy if needed.
     */
    public static com.google.android.startop.iorap.ITaskListener asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof com.google.android.startop.iorap.ITaskListener))) {
        return ((com.google.android.startop.iorap.ITaskListener)iin);
      }
      return new com.google.android.startop.iorap.ITaskListener.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_onProgress:
        {
          data.enforceInterface(descriptor);
          com.google.android.startop.iorap.RequestId _arg0;
          if ((0!=data.readInt())) {
            _arg0 = com.google.android.startop.iorap.RequestId.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          com.google.android.startop.iorap.TaskResult _arg1;
          if ((0!=data.readInt())) {
            _arg1 = com.google.android.startop.iorap.TaskResult.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onProgress(_arg0, _arg1);
          return true;
        }
        case TRANSACTION_onComplete:
        {
          data.enforceInterface(descriptor);
          com.google.android.startop.iorap.RequestId _arg0;
          if ((0!=data.readInt())) {
            _arg0 = com.google.android.startop.iorap.RequestId.CREATOR.createFromParcel(data);
          }
          else {
            _arg0 = null;
          }
          com.google.android.startop.iorap.TaskResult _arg1;
          if ((0!=data.readInt())) {
            _arg1 = com.google.android.startop.iorap.TaskResult.CREATOR.createFromParcel(data);
          }
          else {
            _arg1 = null;
          }
          this.onComplete(_arg0, _arg1);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements com.google.android.startop.iorap.ITaskListener
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      @Override public void onProgress(com.google.android.startop.iorap.RequestId requestId, com.google.android.startop.iorap.TaskResult result) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((requestId!=null)) {
            _data.writeInt(1);
            requestId.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((result!=null)) {
            _data.writeInt(1);
            result.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onProgress, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onProgress(requestId, result);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      @Override public void onComplete(com.google.android.startop.iorap.RequestId requestId, com.google.android.startop.iorap.TaskResult result) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          if ((requestId!=null)) {
            _data.writeInt(1);
            requestId.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          if ((result!=null)) {
            _data.writeInt(1);
            result.writeToParcel(_data, 0);
          }
          else {
            _data.writeInt(0);
          }
          boolean _status = mRemote.transact(Stub.TRANSACTION_onComplete, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().onComplete(requestId, result);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static com.google.android.startop.iorap.ITaskListener sDefaultImpl;
    }
    static final int TRANSACTION_onProgress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_onComplete = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    public static boolean setDefaultImpl(com.google.android.startop.iorap.ITaskListener impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static com.google.android.startop.iorap.ITaskListener getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public void onProgress(com.google.android.startop.iorap.RequestId requestId, com.google.android.startop.iorap.TaskResult result) throws android.os.RemoteException;
  public void onComplete(com.google.android.startop.iorap.RequestId requestId, com.google.android.startop.iorap.TaskResult result) throws android.os.RemoteException;
}
