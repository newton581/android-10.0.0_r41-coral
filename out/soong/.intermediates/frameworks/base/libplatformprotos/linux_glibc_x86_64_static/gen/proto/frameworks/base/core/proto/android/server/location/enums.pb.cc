// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/server/location/enums.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/server/location/enums.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace server {
namespace location {

namespace {

const ::google::protobuf::EnumDescriptor* GpsSignalQualityEnum_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* GnssNiType_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* GnssUserResponseType_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* GnssNiEncodingType_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* NfwProtocolStack_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* NfwRequestor_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* NfwResponseType_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* SuplMode_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* LppProfile_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* GlonassPosProtocol_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* GpsLock_descriptor_ = NULL;

}  // namespace


void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fserver_2flocation_2fenums_2eproto() {
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fserver_2flocation_2fenums_2eproto();
  const ::google::protobuf::FileDescriptor* file =
    ::google::protobuf::DescriptorPool::generated_pool()->FindFileByName(
      "frameworks/base/core/proto/android/server/location/enums.proto");
  GOOGLE_CHECK(file != NULL);
  GpsSignalQualityEnum_descriptor_ = file->enum_type(0);
  GnssNiType_descriptor_ = file->enum_type(1);
  GnssUserResponseType_descriptor_ = file->enum_type(2);
  GnssNiEncodingType_descriptor_ = file->enum_type(3);
  NfwProtocolStack_descriptor_ = file->enum_type(4);
  NfwRequestor_descriptor_ = file->enum_type(5);
  NfwResponseType_descriptor_ = file->enum_type(6);
  SuplMode_descriptor_ = file->enum_type(7);
  LppProfile_descriptor_ = file->enum_type(8);
  GlonassPosProtocol_descriptor_ = file->enum_type(9);
  GpsLock_descriptor_ = file->enum_type(10);
}

namespace {

GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AssignDescriptors_once_);
inline void protobuf_AssignDescriptorsOnce() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AssignDescriptors_once_,
                 &protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fserver_2flocation_2fenums_2eproto);
}

void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
}

}  // namespace

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fserver_2flocation_2fenums_2eproto() {
}

void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fserver_2flocation_2fenums_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
    "\n>frameworks/base/core/proto/android/ser"
    "ver/location/enums.proto\022\027android.server"
    ".location*y\n\024GpsSignalQualityEnum\022\'\n\032GPS"
    "_SIGNAL_QUALITY_UNKNOWN\020\377\377\377\377\377\377\377\377\377\001\022\033\n\027GP"
    "S_SIGNAL_QUALITY_POOR\020\000\022\033\n\027GPS_SIGNAL_QU"
    "ALITY_GOOD\020\001*O\n\nGnssNiType\022\t\n\005VOICE\020\001\022\r\n"
    "\tUMTS_SUPL\020\002\022\023\n\017UMTS_CTRL_PLANE\020\003\022\022\n\016EME"
    "RGENCY_SUPL\020\004*S\n\024GnssUserResponseType\022\023\n"
    "\017RESPONSE_ACCEPT\020\001\022\021\n\rRESPONSE_DENY\020\002\022\023\n"
    "\017RESPONSE_NORESP\020\003*|\n\022GnssNiEncodingType"
    "\022\014\n\010ENC_NONE\020\000\022\030\n\024ENC_SUPL_GSM_DEFAULT\020\001"
    "\022\021\n\rENC_SUPL_UTF8\020\002\022\021\n\rENC_SUPL_UCS2\020\003\022\030"
    "\n\013ENC_UNKNOWN\020\377\377\377\377\377\377\377\377\377\001*X\n\020NfwProtocolS"
    "tack\022\016\n\nCTRL_PLANE\020\000\022\010\n\004SUPL\020\001\022\007\n\003IMS\020\n\022"
    "\007\n\003SIM\020\013\022\030\n\024OTHER_PROTOCOL_STACK\020d*\235\001\n\014N"
    "fwRequestor\022\013\n\007CARRIER\020\000\022\007\n\003OEM\020\n\022\030\n\024MOD"
    "EM_CHIPSET_VENDOR\020\013\022\027\n\023GNSS_CHIPSET_VEND"
    "OR\020\014\022\030\n\024OTHER_CHIPSET_VENDOR\020\r\022\025\n\021AUTOMO"
    "BILE_CLIENT\020\024\022\023\n\017OTHER_REQUESTOR\020d*b\n\017Nf"
    "wResponseType\022\014\n\010REJECTED\020\000\022!\n\035ACCEPTED_"
    "NO_LOCATION_PROVIDED\020\001\022\036\n\032ACCEPTED_LOCAT"
    "ION_PROVIDED\020\002*\034\n\010SuplMode\022\007\n\003MSB\020\001\022\007\n\003M"
    "SA\020\002*/\n\nLppProfile\022\016\n\nUSER_PLANE\020\001\022\021\n\rCO"
    "NTROL_PLANE\020\002*E\n\022GlonassPosProtocol\022\016\n\nR"
    "RC_CPLANE\020\001\022\017\n\013RRLP_CPLANE\020\002\022\016\n\nLPP_UPLA"
    "NE\020\004*\031\n\007GpsLock\022\006\n\002MO\020\001\022\006\n\002NI\020\002B\034B\030Serve"
    "rLocationProtoEnumsP\001", 1061);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "frameworks/base/core/proto/android/server/location/enums.proto", &protobuf_RegisterTypes);
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fserver_2flocation_2fenums_2eproto);
}

// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fserver_2flocation_2fenums_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fserver_2flocation_2fenums_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fserver_2flocation_2fenums_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fserver_2flocation_2fenums_2eproto_;
const ::google::protobuf::EnumDescriptor* GpsSignalQualityEnum_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return GpsSignalQualityEnum_descriptor_;
}
bool GpsSignalQualityEnum_IsValid(int value) {
  switch(value) {
    case -1:
    case 0:
    case 1:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* GnssNiType_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return GnssNiType_descriptor_;
}
bool GnssNiType_IsValid(int value) {
  switch(value) {
    case 1:
    case 2:
    case 3:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* GnssUserResponseType_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return GnssUserResponseType_descriptor_;
}
bool GnssUserResponseType_IsValid(int value) {
  switch(value) {
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* GnssNiEncodingType_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return GnssNiEncodingType_descriptor_;
}
bool GnssNiEncodingType_IsValid(int value) {
  switch(value) {
    case -1:
    case 0:
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* NfwProtocolStack_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return NfwProtocolStack_descriptor_;
}
bool NfwProtocolStack_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 10:
    case 11:
    case 100:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* NfwRequestor_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return NfwRequestor_descriptor_;
}
bool NfwRequestor_IsValid(int value) {
  switch(value) {
    case 0:
    case 10:
    case 11:
    case 12:
    case 13:
    case 20:
    case 100:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* NfwResponseType_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return NfwResponseType_descriptor_;
}
bool NfwResponseType_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* SuplMode_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return SuplMode_descriptor_;
}
bool SuplMode_IsValid(int value) {
  switch(value) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* LppProfile_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return LppProfile_descriptor_;
}
bool LppProfile_IsValid(int value) {
  switch(value) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* GlonassPosProtocol_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return GlonassPosProtocol_descriptor_;
}
bool GlonassPosProtocol_IsValid(int value) {
  switch(value) {
    case 1:
    case 2:
    case 4:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* GpsLock_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return GpsLock_descriptor_;
}
bool GpsLock_IsValid(int value) {
  switch(value) {
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace location
}  // namespace server
}  // namespace android

// @@protoc_insertion_point(global_scope)
