// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/content/activityinfo.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fcontent_2factivityinfo_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fcontent_2factivityinfo_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace content {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fcontent_2factivityinfo_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fcontent_2factivityinfo_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fcontent_2factivityinfo_2eproto();

class ActivityInfoProto;

enum ActivityInfoProto_ScreenOrientation {
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_UNSET = -2,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_UNSPECIFIED = -1,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_LANDSCAPE = 0,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_PORTRAIT = 1,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_USER = 2,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_BEHIND = 3,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_SENSOR = 4,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_NOSENSOR = 5,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_SENSOR_LANDSCAPE = 6,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_SENSOR_PORTRAIT = 7,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_REVERSE_LANDSCAPE = 8,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_REVERSE_PORTRAIT = 9,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_FULL_SENSOR = 10,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_USER_LANDSCAPE = 11,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_USER_PORTRAIT = 12,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_FULL_USER = 13,
  ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_LOCKED = 14
};
bool ActivityInfoProto_ScreenOrientation_IsValid(int value);
const ActivityInfoProto_ScreenOrientation ActivityInfoProto_ScreenOrientation_ScreenOrientation_MIN = ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_UNSET;
const ActivityInfoProto_ScreenOrientation ActivityInfoProto_ScreenOrientation_ScreenOrientation_MAX = ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_LOCKED;
const int ActivityInfoProto_ScreenOrientation_ScreenOrientation_ARRAYSIZE = ActivityInfoProto_ScreenOrientation_ScreenOrientation_MAX + 1;

const ::google::protobuf::EnumDescriptor* ActivityInfoProto_ScreenOrientation_descriptor();
inline const ::std::string& ActivityInfoProto_ScreenOrientation_Name(ActivityInfoProto_ScreenOrientation value) {
  return ::google::protobuf::internal::NameOfEnum(
    ActivityInfoProto_ScreenOrientation_descriptor(), value);
}
inline bool ActivityInfoProto_ScreenOrientation_Parse(
    const ::std::string& name, ActivityInfoProto_ScreenOrientation* value) {
  return ::google::protobuf::internal::ParseNamedEnum<ActivityInfoProto_ScreenOrientation>(
    ActivityInfoProto_ScreenOrientation_descriptor(), name, value);
}
// ===================================================================

class ActivityInfoProto : public ::google::protobuf::Message {
 public:
  ActivityInfoProto();
  virtual ~ActivityInfoProto();

  ActivityInfoProto(const ActivityInfoProto& from);

  inline ActivityInfoProto& operator=(const ActivityInfoProto& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const ActivityInfoProto& default_instance();

  void Swap(ActivityInfoProto* other);

  // implements Message ----------------------------------------------

  inline ActivityInfoProto* New() const { return New(NULL); }

  ActivityInfoProto* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const ActivityInfoProto& from);
  void MergeFrom(const ActivityInfoProto& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(ActivityInfoProto* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  typedef ActivityInfoProto_ScreenOrientation ScreenOrientation;
  static const ScreenOrientation SCREEN_ORIENTATION_UNSET =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_UNSET;
  static const ScreenOrientation SCREEN_ORIENTATION_UNSPECIFIED =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_UNSPECIFIED;
  static const ScreenOrientation SCREEN_ORIENTATION_LANDSCAPE =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_LANDSCAPE;
  static const ScreenOrientation SCREEN_ORIENTATION_PORTRAIT =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_PORTRAIT;
  static const ScreenOrientation SCREEN_ORIENTATION_USER =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_USER;
  static const ScreenOrientation SCREEN_ORIENTATION_BEHIND =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_BEHIND;
  static const ScreenOrientation SCREEN_ORIENTATION_SENSOR =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_SENSOR;
  static const ScreenOrientation SCREEN_ORIENTATION_NOSENSOR =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_NOSENSOR;
  static const ScreenOrientation SCREEN_ORIENTATION_SENSOR_LANDSCAPE =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_SENSOR_LANDSCAPE;
  static const ScreenOrientation SCREEN_ORIENTATION_SENSOR_PORTRAIT =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_SENSOR_PORTRAIT;
  static const ScreenOrientation SCREEN_ORIENTATION_REVERSE_LANDSCAPE =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
  static const ScreenOrientation SCREEN_ORIENTATION_REVERSE_PORTRAIT =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_REVERSE_PORTRAIT;
  static const ScreenOrientation SCREEN_ORIENTATION_FULL_SENSOR =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_FULL_SENSOR;
  static const ScreenOrientation SCREEN_ORIENTATION_USER_LANDSCAPE =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_USER_LANDSCAPE;
  static const ScreenOrientation SCREEN_ORIENTATION_USER_PORTRAIT =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_USER_PORTRAIT;
  static const ScreenOrientation SCREEN_ORIENTATION_FULL_USER =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_FULL_USER;
  static const ScreenOrientation SCREEN_ORIENTATION_LOCKED =
    ActivityInfoProto_ScreenOrientation_SCREEN_ORIENTATION_LOCKED;
  static inline bool ScreenOrientation_IsValid(int value) {
    return ActivityInfoProto_ScreenOrientation_IsValid(value);
  }
  static const ScreenOrientation ScreenOrientation_MIN =
    ActivityInfoProto_ScreenOrientation_ScreenOrientation_MIN;
  static const ScreenOrientation ScreenOrientation_MAX =
    ActivityInfoProto_ScreenOrientation_ScreenOrientation_MAX;
  static const int ScreenOrientation_ARRAYSIZE =
    ActivityInfoProto_ScreenOrientation_ScreenOrientation_ARRAYSIZE;
  static inline const ::google::protobuf::EnumDescriptor*
  ScreenOrientation_descriptor() {
    return ActivityInfoProto_ScreenOrientation_descriptor();
  }
  static inline const ::std::string& ScreenOrientation_Name(ScreenOrientation value) {
    return ActivityInfoProto_ScreenOrientation_Name(value);
  }
  static inline bool ScreenOrientation_Parse(const ::std::string& name,
      ScreenOrientation* value) {
    return ActivityInfoProto_ScreenOrientation_Parse(name, value);
  }

  // accessors -------------------------------------------------------

  // @@protoc_insertion_point(class_scope:android.content.ActivityInfoProto)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  friend void  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fcontent_2factivityinfo_2eproto();
  friend void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fcontent_2factivityinfo_2eproto();
  friend void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fcontent_2factivityinfo_2eproto();

  void InitAsDefaultInstance();
  static ActivityInfoProto* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// ActivityInfoProto

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace content
}  // namespace android

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::android::content::ActivityInfoProto_ScreenOrientation> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::content::ActivityInfoProto_ScreenOrientation>() {
  return ::android::content::ActivityInfoProto_ScreenOrientation_descriptor();
}

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fcontent_2factivityinfo_2eproto__INCLUDED
