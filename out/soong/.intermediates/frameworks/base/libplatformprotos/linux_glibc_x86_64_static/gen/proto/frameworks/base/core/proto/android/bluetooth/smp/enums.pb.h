// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/bluetooth/smp/enums.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fsmp_2fenums_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fsmp_2fenums_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace bluetooth {
namespace smp {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fsmp_2fenums_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fsmp_2fenums_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fsmp_2fenums_2eproto();


enum CommandEnum {
  CMD_UNKNOWN = 0,
  CMD_PAIRING_REQUEST = 1,
  CMD_PAIRING_RESPONSE = 2,
  CMD_PAIRING_CONFIRM = 3,
  CMD_PAIRING_RANDOM = 4,
  CMD_PAIRING_FAILED = 5,
  CMD_ENCRYPTION_INFON = 6,
  CMD_MASTER_IDENTIFICATION = 7,
  CMD_IDENTITY_INFO = 8,
  CMD_IDENTITY_ADDR_INFO = 9,
  CMD_SIGNING_INFO = 10,
  CMD_SECURITY_REQUEST = 11,
  CMD_PAIRING_PUBLIC_KEY = 12,
  CMD_PAIRING_DHKEY_CHECK = 13,
  CMD_PAIRING_KEYPRESS_INFO = 14
};
bool CommandEnum_IsValid(int value);
const CommandEnum CommandEnum_MIN = CMD_UNKNOWN;
const CommandEnum CommandEnum_MAX = CMD_PAIRING_KEYPRESS_INFO;
const int CommandEnum_ARRAYSIZE = CommandEnum_MAX + 1;

const ::google::protobuf::EnumDescriptor* CommandEnum_descriptor();
inline const ::std::string& CommandEnum_Name(CommandEnum value) {
  return ::google::protobuf::internal::NameOfEnum(
    CommandEnum_descriptor(), value);
}
inline bool CommandEnum_Parse(
    const ::std::string& name, CommandEnum* value) {
  return ::google::protobuf::internal::ParseNamedEnum<CommandEnum>(
    CommandEnum_descriptor(), name, value);
}
enum PairingFailReasonEnum {
  PAIRING_FAIL_REASON_RESERVED = 0,
  PAIRING_FAIL_REASON_PASSKEY_ENTRY = 1,
  PAIRING_FAIL_REASON_OOB = 2,
  PAIRING_FAIL_REASON_AUTH_REQ = 3,
  PAIRING_FAIL_REASON_CONFIRM_VALUE = 4,
  PAIRING_FAIL_REASON_PAIR_NOT_SUPPORT = 5,
  PAIRING_FAIL_REASON_ENC_KEY_SIZE = 6,
  PAIRING_FAIL_REASON_INVALID_CMD = 7,
  PAIRING_FAIL_REASON_UNSPECIFIED = 8,
  PAIRING_FAIL_REASON_REPEATED_ATTEMPTS = 9,
  PAIRING_FAIL_REASON_INVALID_PARAMETERS = 10,
  PAIRING_FAIL_REASON_DHKEY_CHK = 11,
  PAIRING_FAIL_REASON_NUMERIC_COMPARISON = 12,
  PAIRING_FAIL_REASON_CLASSIC_PAIRING_IN_PROGR = 13,
  PAIRING_FAIL_REASON_XTRANS_DERIVE_NOT_ALLOW = 14
};
bool PairingFailReasonEnum_IsValid(int value);
const PairingFailReasonEnum PairingFailReasonEnum_MIN = PAIRING_FAIL_REASON_RESERVED;
const PairingFailReasonEnum PairingFailReasonEnum_MAX = PAIRING_FAIL_REASON_XTRANS_DERIVE_NOT_ALLOW;
const int PairingFailReasonEnum_ARRAYSIZE = PairingFailReasonEnum_MAX + 1;

const ::google::protobuf::EnumDescriptor* PairingFailReasonEnum_descriptor();
inline const ::std::string& PairingFailReasonEnum_Name(PairingFailReasonEnum value) {
  return ::google::protobuf::internal::NameOfEnum(
    PairingFailReasonEnum_descriptor(), value);
}
inline bool PairingFailReasonEnum_Parse(
    const ::std::string& name, PairingFailReasonEnum* value) {
  return ::google::protobuf::internal::ParseNamedEnum<PairingFailReasonEnum>(
    PairingFailReasonEnum_descriptor(), name, value);
}
// ===================================================================


// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace smp
}  // namespace bluetooth
}  // namespace android

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::android::bluetooth::smp::CommandEnum> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::bluetooth::smp::CommandEnum>() {
  return ::android::bluetooth::smp::CommandEnum_descriptor();
}
template <> struct is_proto_enum< ::android::bluetooth::smp::PairingFailReasonEnum> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::bluetooth::smp::PairingFailReasonEnum>() {
  return ::android::bluetooth::smp::PairingFailReasonEnum_descriptor();
}

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fsmp_2fenums_2eproto__INCLUDED
