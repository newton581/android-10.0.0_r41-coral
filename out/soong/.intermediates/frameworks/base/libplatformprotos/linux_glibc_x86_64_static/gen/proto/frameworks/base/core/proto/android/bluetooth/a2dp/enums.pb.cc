// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/bluetooth/a2dp/enums.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/bluetooth/a2dp/enums.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace bluetooth {
namespace a2dp {

namespace {

const ::google::protobuf::EnumDescriptor* PlaybackStateEnum_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* AudioCodingModeEnum_descriptor_ = NULL;

}  // namespace


void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fa2dp_2fenums_2eproto() {
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fa2dp_2fenums_2eproto();
  const ::google::protobuf::FileDescriptor* file =
    ::google::protobuf::DescriptorPool::generated_pool()->FindFileByName(
      "frameworks/base/core/proto/android/bluetooth/a2dp/enums.proto");
  GOOGLE_CHECK(file != NULL);
  PlaybackStateEnum_descriptor_ = file->enum_type(0);
  AudioCodingModeEnum_descriptor_ = file->enum_type(1);
}

namespace {

GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AssignDescriptors_once_);
inline void protobuf_AssignDescriptorsOnce() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AssignDescriptors_once_,
                 &protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fa2dp_2fenums_2eproto);
}

void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
}

}  // namespace

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fa2dp_2fenums_2eproto() {
}

void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fa2dp_2fenums_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
    "\n=frameworks/base/core/proto/android/blu"
    "etooth/a2dp/enums.proto\022\026android.bluetoo"
    "th.a2dp*k\n\021PlaybackStateEnum\022\032\n\026PLAYBACK"
    "_STATE_UNKNOWN\020\000\022\032\n\026PLAYBACK_STATE_PLAYI"
    "NG\020\n\022\036\n\032PLAYBACK_STATE_NOT_PLAYING\020\013*t\n\023"
    "AudioCodingModeEnum\022\035\n\031AUDIO_CODING_MODE"
    "_UNKNOWN\020\000\022\036\n\032AUDIO_CODING_MODE_HARDWARE"
    "\020\001\022\036\n\032AUDIO_CODING_MODE_SOFTWARE\020\002B\033B\027Bl"
    "uetoothA2dpProtoEnumsP\001", 343);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "frameworks/base/core/proto/android/bluetooth/a2dp/enums.proto", &protobuf_RegisterTypes);
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fa2dp_2fenums_2eproto);
}

// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fa2dp_2fenums_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fa2dp_2fenums_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fa2dp_2fenums_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fbluetooth_2fa2dp_2fenums_2eproto_;
const ::google::protobuf::EnumDescriptor* PlaybackStateEnum_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return PlaybackStateEnum_descriptor_;
}
bool PlaybackStateEnum_IsValid(int value) {
  switch(value) {
    case 0:
    case 10:
    case 11:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* AudioCodingModeEnum_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return AudioCodingModeEnum_descriptor_;
}
bool AudioCodingModeEnum_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace a2dp
}  // namespace bluetooth
}  // namespace android

// @@protoc_insertion_point(global_scope)
