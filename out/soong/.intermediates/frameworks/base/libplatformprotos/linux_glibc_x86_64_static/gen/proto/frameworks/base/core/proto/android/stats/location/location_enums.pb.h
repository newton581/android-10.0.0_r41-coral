// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/stats/location/location_enums.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2flocation_2flocation_5fenums_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2flocation_2flocation_5fenums_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace stats {
namespace location {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2flocation_2flocation_5fenums_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2flocation_2flocation_5fenums_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2flocation_2flocation_5fenums_2eproto();


enum LocationManagerServiceApi {
  API_UNKNOWN = 0,
  API_REQUEST_LOCATION_UPDATES = 1,
  API_ADD_GNSS_MEASUREMENTS_LISTENER = 2,
  API_REGISTER_GNSS_STATUS_CALLBACK = 3,
  API_REQUEST_GEOFENCE = 4,
  API_SEND_EXTRA_COMMAND = 5
};
bool LocationManagerServiceApi_IsValid(int value);
const LocationManagerServiceApi LocationManagerServiceApi_MIN = API_UNKNOWN;
const LocationManagerServiceApi LocationManagerServiceApi_MAX = API_SEND_EXTRA_COMMAND;
const int LocationManagerServiceApi_ARRAYSIZE = LocationManagerServiceApi_MAX + 1;

const ::google::protobuf::EnumDescriptor* LocationManagerServiceApi_descriptor();
inline const ::std::string& LocationManagerServiceApi_Name(LocationManagerServiceApi value) {
  return ::google::protobuf::internal::NameOfEnum(
    LocationManagerServiceApi_descriptor(), value);
}
inline bool LocationManagerServiceApi_Parse(
    const ::std::string& name, LocationManagerServiceApi* value) {
  return ::google::protobuf::internal::ParseNamedEnum<LocationManagerServiceApi>(
    LocationManagerServiceApi_descriptor(), name, value);
}
enum UsageState {
  USAGE_STARTED = 0,
  USAGE_ENDED = 1
};
bool UsageState_IsValid(int value);
const UsageState UsageState_MIN = USAGE_STARTED;
const UsageState UsageState_MAX = USAGE_ENDED;
const int UsageState_ARRAYSIZE = UsageState_MAX + 1;

const ::google::protobuf::EnumDescriptor* UsageState_descriptor();
inline const ::std::string& UsageState_Name(UsageState value) {
  return ::google::protobuf::internal::NameOfEnum(
    UsageState_descriptor(), value);
}
inline bool UsageState_Parse(
    const ::std::string& name, UsageState* value) {
  return ::google::protobuf::internal::ParseNamedEnum<UsageState>(
    UsageState_descriptor(), name, value);
}
enum ProviderType {
  PROVIDER_UNKNOWN = 0,
  PROVIDER_NETWORK = 1,
  PROVIDER_GPS = 2,
  PROVIDER_PASSIVE = 3,
  PROVIDER_FUSED = 4
};
bool ProviderType_IsValid(int value);
const ProviderType ProviderType_MIN = PROVIDER_UNKNOWN;
const ProviderType ProviderType_MAX = PROVIDER_FUSED;
const int ProviderType_ARRAYSIZE = ProviderType_MAX + 1;

const ::google::protobuf::EnumDescriptor* ProviderType_descriptor();
inline const ::std::string& ProviderType_Name(ProviderType value) {
  return ::google::protobuf::internal::NameOfEnum(
    ProviderType_descriptor(), value);
}
inline bool ProviderType_Parse(
    const ::std::string& name, ProviderType* value) {
  return ::google::protobuf::internal::ParseNamedEnum<ProviderType>(
    ProviderType_descriptor(), name, value);
}
enum CallbackType {
  CALLBACK_UNKNOWN = 0,
  CALLBACK_NOT_APPLICABLE = 1,
  CALLBACK_LISTENER = 2,
  CALLBACK_PENDING_INTENT = 3
};
bool CallbackType_IsValid(int value);
const CallbackType CallbackType_MIN = CALLBACK_UNKNOWN;
const CallbackType CallbackType_MAX = CALLBACK_PENDING_INTENT;
const int CallbackType_ARRAYSIZE = CallbackType_MAX + 1;

const ::google::protobuf::EnumDescriptor* CallbackType_descriptor();
inline const ::std::string& CallbackType_Name(CallbackType value) {
  return ::google::protobuf::internal::NameOfEnum(
    CallbackType_descriptor(), value);
}
inline bool CallbackType_Parse(
    const ::std::string& name, CallbackType* value) {
  return ::google::protobuf::internal::ParseNamedEnum<CallbackType>(
    CallbackType_descriptor(), name, value);
}
enum LocationRequestQuality {
  QUALITY_UNKNOWN = 0,
  ACCURACY_FINE = 100,
  ACCURACY_BLOCK = 102,
  ACCURACY_CITY = 104,
  POWER_NONE = 200,
  POWER_LOW = 201,
  POWER_HIGH = 203
};
bool LocationRequestQuality_IsValid(int value);
const LocationRequestQuality LocationRequestQuality_MIN = QUALITY_UNKNOWN;
const LocationRequestQuality LocationRequestQuality_MAX = POWER_HIGH;
const int LocationRequestQuality_ARRAYSIZE = LocationRequestQuality_MAX + 1;

const ::google::protobuf::EnumDescriptor* LocationRequestQuality_descriptor();
inline const ::std::string& LocationRequestQuality_Name(LocationRequestQuality value) {
  return ::google::protobuf::internal::NameOfEnum(
    LocationRequestQuality_descriptor(), value);
}
inline bool LocationRequestQuality_Parse(
    const ::std::string& name, LocationRequestQuality* value) {
  return ::google::protobuf::internal::ParseNamedEnum<LocationRequestQuality>(
    LocationRequestQuality_descriptor(), name, value);
}
enum LocationRequestIntervalBucket {
  INTERVAL_UNKNOWN = 0,
  INTERVAL_BETWEEN_0_SEC_AND_1_SEC = 1,
  INTERVAL_BETWEEN_1_SEC_AND_5_SEC = 2,
  INTERVAL_BETWEEN_5_SEC_AND_1_MIN = 3,
  INTERVAL_BETWEEN_1_MIN_AND_10_MIN = 4,
  INTERVAL_BETWEEN_10_MIN_AND_1_HOUR = 5,
  INTERVAL_LARGER_THAN_1_HOUR = 6
};
bool LocationRequestIntervalBucket_IsValid(int value);
const LocationRequestIntervalBucket LocationRequestIntervalBucket_MIN = INTERVAL_UNKNOWN;
const LocationRequestIntervalBucket LocationRequestIntervalBucket_MAX = INTERVAL_LARGER_THAN_1_HOUR;
const int LocationRequestIntervalBucket_ARRAYSIZE = LocationRequestIntervalBucket_MAX + 1;

const ::google::protobuf::EnumDescriptor* LocationRequestIntervalBucket_descriptor();
inline const ::std::string& LocationRequestIntervalBucket_Name(LocationRequestIntervalBucket value) {
  return ::google::protobuf::internal::NameOfEnum(
    LocationRequestIntervalBucket_descriptor(), value);
}
inline bool LocationRequestIntervalBucket_Parse(
    const ::std::string& name, LocationRequestIntervalBucket* value) {
  return ::google::protobuf::internal::ParseNamedEnum<LocationRequestIntervalBucket>(
    LocationRequestIntervalBucket_descriptor(), name, value);
}
enum SmallestDisplacementBucket {
  DISTANCE_UNKNOWN = 0,
  DISTANCE_ZERO = 1,
  DISTANCE_BETWEEN_0_AND_100 = 2,
  DISTANCE_LARGER_THAN_100 = 3
};
bool SmallestDisplacementBucket_IsValid(int value);
const SmallestDisplacementBucket SmallestDisplacementBucket_MIN = DISTANCE_UNKNOWN;
const SmallestDisplacementBucket SmallestDisplacementBucket_MAX = DISTANCE_LARGER_THAN_100;
const int SmallestDisplacementBucket_ARRAYSIZE = SmallestDisplacementBucket_MAX + 1;

const ::google::protobuf::EnumDescriptor* SmallestDisplacementBucket_descriptor();
inline const ::std::string& SmallestDisplacementBucket_Name(SmallestDisplacementBucket value) {
  return ::google::protobuf::internal::NameOfEnum(
    SmallestDisplacementBucket_descriptor(), value);
}
inline bool SmallestDisplacementBucket_Parse(
    const ::std::string& name, SmallestDisplacementBucket* value) {
  return ::google::protobuf::internal::ParseNamedEnum<SmallestDisplacementBucket>(
    SmallestDisplacementBucket_descriptor(), name, value);
}
enum ExpirationBucket {
  EXPIRATION_UNKNOWN = 0,
  EXPIRATION_BETWEEN_0_AND_20_SEC = 1,
  EXPIRATION_BETWEEN_20_SEC_AND_1_MIN = 2,
  EXPIRATION_BETWEEN_1_MIN_AND_10_MIN = 3,
  EXPIRATION_BETWEEN_10_MIN_AND_1_HOUR = 4,
  EXPIRATION_LARGER_THAN_1_HOUR = 5,
  EXPIRATION_NO_EXPIRY = 6
};
bool ExpirationBucket_IsValid(int value);
const ExpirationBucket ExpirationBucket_MIN = EXPIRATION_UNKNOWN;
const ExpirationBucket ExpirationBucket_MAX = EXPIRATION_NO_EXPIRY;
const int ExpirationBucket_ARRAYSIZE = ExpirationBucket_MAX + 1;

const ::google::protobuf::EnumDescriptor* ExpirationBucket_descriptor();
inline const ::std::string& ExpirationBucket_Name(ExpirationBucket value) {
  return ::google::protobuf::internal::NameOfEnum(
    ExpirationBucket_descriptor(), value);
}
inline bool ExpirationBucket_Parse(
    const ::std::string& name, ExpirationBucket* value) {
  return ::google::protobuf::internal::ParseNamedEnum<ExpirationBucket>(
    ExpirationBucket_descriptor(), name, value);
}
enum GeofenceRadiusBucket {
  RADIUS_UNKNOWN = 0,
  RADIUS_BETWEEN_0_AND_100 = 1,
  RADIUS_BETWEEN_100_AND_200 = 2,
  RADIUS_BETWEEN_200_AND_300 = 3,
  RADIUS_BETWEEN_300_AND_1000 = 4,
  RADIUS_BETWEEN_1000_AND_10000 = 5,
  RADIUS_LARGER_THAN_100000 = 6,
  RADIUS_NEGATIVE = 7
};
bool GeofenceRadiusBucket_IsValid(int value);
const GeofenceRadiusBucket GeofenceRadiusBucket_MIN = RADIUS_UNKNOWN;
const GeofenceRadiusBucket GeofenceRadiusBucket_MAX = RADIUS_NEGATIVE;
const int GeofenceRadiusBucket_ARRAYSIZE = GeofenceRadiusBucket_MAX + 1;

const ::google::protobuf::EnumDescriptor* GeofenceRadiusBucket_descriptor();
inline const ::std::string& GeofenceRadiusBucket_Name(GeofenceRadiusBucket value) {
  return ::google::protobuf::internal::NameOfEnum(
    GeofenceRadiusBucket_descriptor(), value);
}
inline bool GeofenceRadiusBucket_Parse(
    const ::std::string& name, GeofenceRadiusBucket* value) {
  return ::google::protobuf::internal::ParseNamedEnum<GeofenceRadiusBucket>(
    GeofenceRadiusBucket_descriptor(), name, value);
}
enum ActivityImportance {
  IMPORTANCE_UNKNOWN = 0,
  IMPORTANCE_TOP = 1,
  IMPORTANCE_FORGROUND_SERVICE = 2,
  IMPORTANCE_BACKGROUND = 3
};
bool ActivityImportance_IsValid(int value);
const ActivityImportance ActivityImportance_MIN = IMPORTANCE_UNKNOWN;
const ActivityImportance ActivityImportance_MAX = IMPORTANCE_BACKGROUND;
const int ActivityImportance_ARRAYSIZE = ActivityImportance_MAX + 1;

const ::google::protobuf::EnumDescriptor* ActivityImportance_descriptor();
inline const ::std::string& ActivityImportance_Name(ActivityImportance value) {
  return ::google::protobuf::internal::NameOfEnum(
    ActivityImportance_descriptor(), value);
}
inline bool ActivityImportance_Parse(
    const ::std::string& name, ActivityImportance* value) {
  return ::google::protobuf::internal::ParseNamedEnum<ActivityImportance>(
    ActivityImportance_descriptor(), name, value);
}
// ===================================================================


// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace location
}  // namespace stats
}  // namespace android

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::android::stats::location::LocationManagerServiceApi> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::stats::location::LocationManagerServiceApi>() {
  return ::android::stats::location::LocationManagerServiceApi_descriptor();
}
template <> struct is_proto_enum< ::android::stats::location::UsageState> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::stats::location::UsageState>() {
  return ::android::stats::location::UsageState_descriptor();
}
template <> struct is_proto_enum< ::android::stats::location::ProviderType> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::stats::location::ProviderType>() {
  return ::android::stats::location::ProviderType_descriptor();
}
template <> struct is_proto_enum< ::android::stats::location::CallbackType> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::stats::location::CallbackType>() {
  return ::android::stats::location::CallbackType_descriptor();
}
template <> struct is_proto_enum< ::android::stats::location::LocationRequestQuality> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::stats::location::LocationRequestQuality>() {
  return ::android::stats::location::LocationRequestQuality_descriptor();
}
template <> struct is_proto_enum< ::android::stats::location::LocationRequestIntervalBucket> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::stats::location::LocationRequestIntervalBucket>() {
  return ::android::stats::location::LocationRequestIntervalBucket_descriptor();
}
template <> struct is_proto_enum< ::android::stats::location::SmallestDisplacementBucket> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::stats::location::SmallestDisplacementBucket>() {
  return ::android::stats::location::SmallestDisplacementBucket_descriptor();
}
template <> struct is_proto_enum< ::android::stats::location::ExpirationBucket> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::stats::location::ExpirationBucket>() {
  return ::android::stats::location::ExpirationBucket_descriptor();
}
template <> struct is_proto_enum< ::android::stats::location::GeofenceRadiusBucket> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::stats::location::GeofenceRadiusBucket>() {
  return ::android::stats::location::GeofenceRadiusBucket_descriptor();
}
template <> struct is_proto_enum< ::android::stats::location::ActivityImportance> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::stats::location::ActivityImportance>() {
  return ::android::stats::location::ActivityImportance_descriptor();
}

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2flocation_2flocation_5fenums_2eproto__INCLUDED
