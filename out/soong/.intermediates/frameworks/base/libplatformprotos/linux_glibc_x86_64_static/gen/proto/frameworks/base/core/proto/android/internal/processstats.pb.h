// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/internal/processstats.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

namespace com {
namespace android {
namespace internal {
namespace app {
namespace procstats {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();

class ProcessStatsProto;

enum ProcessStatsProto_MemoryFactor {
  ProcessStatsProto_MemoryFactor_MEM_FACTOR_NORMAL = 0,
  ProcessStatsProto_MemoryFactor_MEM_FACTOR_MODERATE = 1,
  ProcessStatsProto_MemoryFactor_MEM_FACTOR_LOW = 2,
  ProcessStatsProto_MemoryFactor_MEM_FACTOR_CRITICAL = 3
};
bool ProcessStatsProto_MemoryFactor_IsValid(int value);
const ProcessStatsProto_MemoryFactor ProcessStatsProto_MemoryFactor_MemoryFactor_MIN = ProcessStatsProto_MemoryFactor_MEM_FACTOR_NORMAL;
const ProcessStatsProto_MemoryFactor ProcessStatsProto_MemoryFactor_MemoryFactor_MAX = ProcessStatsProto_MemoryFactor_MEM_FACTOR_CRITICAL;
const int ProcessStatsProto_MemoryFactor_MemoryFactor_ARRAYSIZE = ProcessStatsProto_MemoryFactor_MemoryFactor_MAX + 1;

const ::google::protobuf::EnumDescriptor* ProcessStatsProto_MemoryFactor_descriptor();
inline const ::std::string& ProcessStatsProto_MemoryFactor_Name(ProcessStatsProto_MemoryFactor value) {
  return ::google::protobuf::internal::NameOfEnum(
    ProcessStatsProto_MemoryFactor_descriptor(), value);
}
inline bool ProcessStatsProto_MemoryFactor_Parse(
    const ::std::string& name, ProcessStatsProto_MemoryFactor* value) {
  return ::google::protobuf::internal::ParseNamedEnum<ProcessStatsProto_MemoryFactor>(
    ProcessStatsProto_MemoryFactor_descriptor(), name, value);
}
// ===================================================================

class ProcessStatsProto : public ::google::protobuf::Message {
 public:
  ProcessStatsProto();
  virtual ~ProcessStatsProto();

  ProcessStatsProto(const ProcessStatsProto& from);

  inline ProcessStatsProto& operator=(const ProcessStatsProto& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const ProcessStatsProto& default_instance();

  void Swap(ProcessStatsProto* other);

  // implements Message ----------------------------------------------

  inline ProcessStatsProto* New() const { return New(NULL); }

  ProcessStatsProto* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const ProcessStatsProto& from);
  void MergeFrom(const ProcessStatsProto& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(ProcessStatsProto* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  typedef ProcessStatsProto_MemoryFactor MemoryFactor;
  static const MemoryFactor MEM_FACTOR_NORMAL =
    ProcessStatsProto_MemoryFactor_MEM_FACTOR_NORMAL;
  static const MemoryFactor MEM_FACTOR_MODERATE =
    ProcessStatsProto_MemoryFactor_MEM_FACTOR_MODERATE;
  static const MemoryFactor MEM_FACTOR_LOW =
    ProcessStatsProto_MemoryFactor_MEM_FACTOR_LOW;
  static const MemoryFactor MEM_FACTOR_CRITICAL =
    ProcessStatsProto_MemoryFactor_MEM_FACTOR_CRITICAL;
  static inline bool MemoryFactor_IsValid(int value) {
    return ProcessStatsProto_MemoryFactor_IsValid(value);
  }
  static const MemoryFactor MemoryFactor_MIN =
    ProcessStatsProto_MemoryFactor_MemoryFactor_MIN;
  static const MemoryFactor MemoryFactor_MAX =
    ProcessStatsProto_MemoryFactor_MemoryFactor_MAX;
  static const int MemoryFactor_ARRAYSIZE =
    ProcessStatsProto_MemoryFactor_MemoryFactor_ARRAYSIZE;
  static inline const ::google::protobuf::EnumDescriptor*
  MemoryFactor_descriptor() {
    return ProcessStatsProto_MemoryFactor_descriptor();
  }
  static inline const ::std::string& MemoryFactor_Name(MemoryFactor value) {
    return ProcessStatsProto_MemoryFactor_Name(value);
  }
  static inline bool MemoryFactor_Parse(const ::std::string& name,
      MemoryFactor* value) {
    return ProcessStatsProto_MemoryFactor_Parse(name, value);
  }

  // accessors -------------------------------------------------------

  // @@protoc_insertion_point(class_scope:com.android.internal.app.procstats.ProcessStatsProto)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  friend void  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();
  friend void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();
  friend void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();

  void InitAsDefaultInstance();
  static ProcessStatsProto* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// ProcessStatsProto

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace procstats
}  // namespace app
}  // namespace internal
}  // namespace android
}  // namespace com

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::com::android::internal::app::procstats::ProcessStatsProto_MemoryFactor> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::com::android::internal::app::procstats::ProcessStatsProto_MemoryFactor>() {
  return ::com::android::internal::app::procstats::ProcessStatsProto_MemoryFactor_descriptor();
}

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto__INCLUDED
