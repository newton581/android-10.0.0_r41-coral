// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/telecomm/enums.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/telecomm/enums.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace telecom {

namespace {

const ::google::protobuf::EnumDescriptor* CallStateEnum_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* DisconnectCauseEnum_descriptor_ = NULL;

}  // namespace


void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2ftelecomm_2fenums_2eproto() {
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2ftelecomm_2fenums_2eproto();
  const ::google::protobuf::FileDescriptor* file =
    ::google::protobuf::DescriptorPool::generated_pool()->FindFileByName(
      "frameworks/base/core/proto/android/telecomm/enums.proto");
  GOOGLE_CHECK(file != NULL);
  CallStateEnum_descriptor_ = file->enum_type(0);
  DisconnectCauseEnum_descriptor_ = file->enum_type(1);
}

namespace {

GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AssignDescriptors_once_);
inline void protobuf_AssignDescriptorsOnce() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AssignDescriptors_once_,
                 &protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2ftelecomm_2fenums_2eproto);
}

void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
}

}  // namespace

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2ftelecomm_2fenums_2eproto() {
}

void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2ftelecomm_2fenums_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
    "\n7frameworks/base/core/proto/android/tel"
    "ecomm/enums.proto\022\017android.telecom*\264\001\n\rC"
    "allStateEnum\022\007\n\003NEW\020\000\022\016\n\nCONNECTING\020\001\022\030\n"
    "\024SELECT_PHONE_ACCOUNT\020\002\022\013\n\007DIALING\020\003\022\013\n\007"
    "RINGING\020\004\022\n\n\006ACTIVE\020\005\022\013\n\007ON_HOLD\020\006\022\020\n\014DI"
    "SCONNECTED\020\007\022\013\n\007ABORTED\020\010\022\021\n\rDISCONNECTI"
    "NG\020\t\022\013\n\007PULLING\020\n*\340\001\n\023DisconnectCauseEnu"
    "m\022\013\n\007UNKNOWN\020\000\022\t\n\005ERROR\020\001\022\t\n\005LOCAL\020\002\022\n\n\006"
    "REMOTE\020\003\022\014\n\010CANCELED\020\004\022\n\n\006MISSED\020\005\022\014\n\010RE"
    "JECTED\020\006\022\010\n\004BUSY\020\007\022\016\n\nRESTRICTED\020\010\022\t\n\005OT"
    "HER\020\t\022$\n CONNECTION_MANAGER_NOT_SUPPORTE"
    "D\020\n\022\026\n\022ANSWERED_ELSEWHERE\020\013\022\017\n\013CALL_PULL"
    "ED\020\014B\025B\021TelecomProtoEnumsP\001", 507);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "frameworks/base/core/proto/android/telecomm/enums.proto", &protobuf_RegisterTypes);
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2ftelecomm_2fenums_2eproto);
}

// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2ftelecomm_2fenums_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2ftelecomm_2fenums_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2ftelecomm_2fenums_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2ftelecomm_2fenums_2eproto_;
const ::google::protobuf::EnumDescriptor* CallStateEnum_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return CallStateEnum_descriptor_;
}
bool CallStateEnum_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
      return true;
    default:
      return false;
  }
}

const ::google::protobuf::EnumDescriptor* DisconnectCauseEnum_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return DisconnectCauseEnum_descriptor_;
}
bool DisconnectCauseEnum_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
      return true;
    default:
      return false;
  }
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace telecom
}  // namespace android

// @@protoc_insertion_point(global_scope)
