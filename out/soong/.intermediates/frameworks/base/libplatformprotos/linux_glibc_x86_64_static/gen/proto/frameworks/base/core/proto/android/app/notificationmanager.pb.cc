// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/app/notificationmanager.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/app/notificationmanager.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace app {

namespace {

const ::google::protobuf::Descriptor* PolicyProto_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  PolicyProto_reflection_ = NULL;
const ::google::protobuf::EnumDescriptor* PolicyProto_Category_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* PolicyProto_Sender_descriptor_ = NULL;
const ::google::protobuf::EnumDescriptor* PolicyProto_SuppressedVisualEffect_descriptor_ = NULL;

}  // namespace


void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto() {
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto();
  const ::google::protobuf::FileDescriptor* file =
    ::google::protobuf::DescriptorPool::generated_pool()->FindFileByName(
      "frameworks/base/core/proto/android/app/notificationmanager.proto");
  GOOGLE_CHECK(file != NULL);
  PolicyProto_descriptor_ = file->message_type(0);
  static const int PolicyProto_offsets_[4] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PolicyProto, priority_categories_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PolicyProto, priority_call_sender_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PolicyProto, priority_message_sender_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PolicyProto, suppressed_visual_effects_),
  };
  PolicyProto_reflection_ =
    ::google::protobuf::internal::GeneratedMessageReflection::NewGeneratedMessageReflection(
      PolicyProto_descriptor_,
      PolicyProto::default_instance_,
      PolicyProto_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PolicyProto, _has_bits_[0]),
      -1,
      -1,
      sizeof(PolicyProto),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(PolicyProto, _internal_metadata_),
      -1);
  PolicyProto_Category_descriptor_ = PolicyProto_descriptor_->enum_type(0);
  PolicyProto_Sender_descriptor_ = PolicyProto_descriptor_->enum_type(1);
  PolicyProto_SuppressedVisualEffect_descriptor_ = PolicyProto_descriptor_->enum_type(2);
}

namespace {

GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AssignDescriptors_once_);
inline void protobuf_AssignDescriptorsOnce() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AssignDescriptors_once_,
                 &protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto);
}

void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
      PolicyProto_descriptor_, &PolicyProto::default_instance());
}

}  // namespace

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto() {
  delete PolicyProto::default_instance_;
  delete PolicyProto_reflection_;
}

void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ::android::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
    "\n@frameworks/base/core/proto/android/app"
    "/notificationmanager.proto\022\013android.app\032"
    "0frameworks/base/core/proto/android/priv"
    "acy.proto\"\251\005\n\013PolicyProto\022>\n\023priority_ca"
    "tegories\030\001 \003(\0162!.android.app.PolicyProto"
    ".Category\022=\n\024priority_call_sender\030\002 \001(\0162"
    "\037.android.app.PolicyProto.Sender\022@\n\027prio"
    "rity_message_sender\030\003 \001(\0162\037.android.app."
    "PolicyProto.Sender\022R\n\031suppressed_visual_"
    "effects\030\004 \003(\0162/.android.app.PolicyProto."
    "SuppressedVisualEffect\"\213\001\n\010Category\022\024\n\020C"
    "ATEGORY_UNKNOWN\020\000\022\r\n\tREMINDERS\020\001\022\n\n\006EVEN"
    "TS\020\002\022\014\n\010MESSAGES\020\003\022\t\n\005CALLS\020\004\022\022\n\016REPEAT_"
    "CALLERS\020\005\022\n\n\006ALARMS\020\006\022\t\n\005MEDIA\020\007\022\n\n\006SYST"
    "EM\020\010\",\n\006Sender\022\007\n\003ANY\020\000\022\014\n\010CONTACTS\020\001\022\013\n"
    "\007STARRED\020\002\"\275\001\n\026SuppressedVisualEffect\022\017\n"
    "\013SVE_UNKNOWN\020\000\022\022\n\nSCREEN_OFF\020\001\032\002\010\001\022\021\n\tSC"
    "REEN_ON\020\002\032\002\010\001\022\026\n\022FULL_SCREEN_INTENT\020\003\022\n\n"
    "\006LIGHTS\020\004\022\010\n\004PEEK\020\005\022\016\n\nSTATUS_BAR\020\006\022\t\n\005B"
    "ADGE\020\007\022\013\n\007AMBIENT\020\010\022\025\n\021NOTIFICATION_LIST"
    "\020\t:\t\232\237\325\207\003\003\010\310\001B\002P\001", 817);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "frameworks/base/core/proto/android/app/notificationmanager.proto", &protobuf_RegisterTypes);
  PolicyProto::default_instance_ = new PolicyProto();
  PolicyProto::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto);
}

// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto_;

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

const ::google::protobuf::EnumDescriptor* PolicyProto_Category_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return PolicyProto_Category_descriptor_;
}
bool PolicyProto_Category_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const PolicyProto_Category PolicyProto::CATEGORY_UNKNOWN;
const PolicyProto_Category PolicyProto::REMINDERS;
const PolicyProto_Category PolicyProto::EVENTS;
const PolicyProto_Category PolicyProto::MESSAGES;
const PolicyProto_Category PolicyProto::CALLS;
const PolicyProto_Category PolicyProto::REPEAT_CALLERS;
const PolicyProto_Category PolicyProto::ALARMS;
const PolicyProto_Category PolicyProto::MEDIA;
const PolicyProto_Category PolicyProto::SYSTEM;
const PolicyProto_Category PolicyProto::Category_MIN;
const PolicyProto_Category PolicyProto::Category_MAX;
const int PolicyProto::Category_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
const ::google::protobuf::EnumDescriptor* PolicyProto_Sender_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return PolicyProto_Sender_descriptor_;
}
bool PolicyProto_Sender_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const PolicyProto_Sender PolicyProto::ANY;
const PolicyProto_Sender PolicyProto::CONTACTS;
const PolicyProto_Sender PolicyProto::STARRED;
const PolicyProto_Sender PolicyProto::Sender_MIN;
const PolicyProto_Sender PolicyProto::Sender_MAX;
const int PolicyProto::Sender_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
const ::google::protobuf::EnumDescriptor* PolicyProto_SuppressedVisualEffect_descriptor() {
  protobuf_AssignDescriptorsOnce();
  return PolicyProto_SuppressedVisualEffect_descriptor_;
}
bool PolicyProto_SuppressedVisualEffect_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const PolicyProto_SuppressedVisualEffect PolicyProto::SVE_UNKNOWN;
const PolicyProto_SuppressedVisualEffect PolicyProto::SCREEN_OFF;
const PolicyProto_SuppressedVisualEffect PolicyProto::SCREEN_ON;
const PolicyProto_SuppressedVisualEffect PolicyProto::FULL_SCREEN_INTENT;
const PolicyProto_SuppressedVisualEffect PolicyProto::LIGHTS;
const PolicyProto_SuppressedVisualEffect PolicyProto::PEEK;
const PolicyProto_SuppressedVisualEffect PolicyProto::STATUS_BAR;
const PolicyProto_SuppressedVisualEffect PolicyProto::BADGE;
const PolicyProto_SuppressedVisualEffect PolicyProto::AMBIENT;
const PolicyProto_SuppressedVisualEffect PolicyProto::NOTIFICATION_LIST;
const PolicyProto_SuppressedVisualEffect PolicyProto::SuppressedVisualEffect_MIN;
const PolicyProto_SuppressedVisualEffect PolicyProto::SuppressedVisualEffect_MAX;
const int PolicyProto::SuppressedVisualEffect_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int PolicyProto::kPriorityCategoriesFieldNumber;
const int PolicyProto::kPriorityCallSenderFieldNumber;
const int PolicyProto::kPriorityMessageSenderFieldNumber;
const int PolicyProto::kSuppressedVisualEffectsFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

PolicyProto::PolicyProto()
  : ::google::protobuf::Message(), _internal_metadata_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.app.PolicyProto)
}

void PolicyProto::InitAsDefaultInstance() {
}

PolicyProto::PolicyProto(const PolicyProto& from)
  : ::google::protobuf::Message(),
    _internal_metadata_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.app.PolicyProto)
}

void PolicyProto::SharedCtor() {
  _cached_size_ = 0;
  priority_call_sender_ = 0;
  priority_message_sender_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

PolicyProto::~PolicyProto() {
  // @@protoc_insertion_point(destructor:android.app.PolicyProto)
  SharedDtor();
}

void PolicyProto::SharedDtor() {
  if (this != default_instance_) {
  }
}

void PolicyProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* PolicyProto::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return PolicyProto_descriptor_;
}

const PolicyProto& PolicyProto::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto();
  return *default_instance_; /* NOLINT */
}

PolicyProto* PolicyProto::default_instance_ = NULL;

PolicyProto* PolicyProto::New(::google::protobuf::Arena* arena) const {
  PolicyProto* n = new PolicyProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void PolicyProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.app.PolicyProto)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(PolicyProto, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<PolicyProto*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  ZR_(priority_call_sender_, priority_message_sender_);

#undef ZR_HELPER_
#undef ZR_

  priority_categories_.Clear();
  suppressed_visual_effects_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  if (_internal_metadata_.have_unknown_fields()) {
    mutable_unknown_fields()->Clear();
  }
}

bool PolicyProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  // @@protoc_insertion_point(parse_start:android.app.PolicyProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // repeated .android.app.PolicyProto.Category priority_categories = 1;
      case 1: {
        if (tag == 8) {
         parse_priority_categories:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          if (::android::app::PolicyProto_Category_IsValid(value)) {
            add_priority_categories(static_cast< ::android::app::PolicyProto_Category >(value));
          } else {
            mutable_unknown_fields()->AddVarint(1, value);
          }
        } else if (tag == 10) {
          DO_((::google::protobuf::internal::WireFormat::ReadPackedEnumPreserveUnknowns(
                 input,
                 1,
                 ::android::app::PolicyProto_Category_IsValid,
                 mutable_unknown_fields(),
                 this->mutable_priority_categories())));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(8)) goto parse_priority_categories;
        if (input->ExpectTag(16)) goto parse_priority_call_sender;
        break;
      }

      // optional .android.app.PolicyProto.Sender priority_call_sender = 2;
      case 2: {
        if (tag == 16) {
         parse_priority_call_sender:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          if (::android::app::PolicyProto_Sender_IsValid(value)) {
            set_priority_call_sender(static_cast< ::android::app::PolicyProto_Sender >(value));
          } else {
            mutable_unknown_fields()->AddVarint(2, value);
          }
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(24)) goto parse_priority_message_sender;
        break;
      }

      // optional .android.app.PolicyProto.Sender priority_message_sender = 3;
      case 3: {
        if (tag == 24) {
         parse_priority_message_sender:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          if (::android::app::PolicyProto_Sender_IsValid(value)) {
            set_priority_message_sender(static_cast< ::android::app::PolicyProto_Sender >(value));
          } else {
            mutable_unknown_fields()->AddVarint(3, value);
          }
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(32)) goto parse_suppressed_visual_effects;
        break;
      }

      // repeated .android.app.PolicyProto.SuppressedVisualEffect suppressed_visual_effects = 4;
      case 4: {
        if (tag == 32) {
         parse_suppressed_visual_effects:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          if (::android::app::PolicyProto_SuppressedVisualEffect_IsValid(value)) {
            add_suppressed_visual_effects(static_cast< ::android::app::PolicyProto_SuppressedVisualEffect >(value));
          } else {
            mutable_unknown_fields()->AddVarint(4, value);
          }
        } else if (tag == 34) {
          DO_((::google::protobuf::internal::WireFormat::ReadPackedEnumPreserveUnknowns(
                 input,
                 4,
                 ::android::app::PolicyProto_SuppressedVisualEffect_IsValid,
                 mutable_unknown_fields(),
                 this->mutable_suppressed_visual_effects())));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(32)) goto parse_suppressed_visual_effects;
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.app.PolicyProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.app.PolicyProto)
  return false;
#undef DO_
}

void PolicyProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.app.PolicyProto)
  // repeated .android.app.PolicyProto.Category priority_categories = 1;
  for (int i = 0; i < this->priority_categories_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      1, this->priority_categories(i), output);
  }

  // optional .android.app.PolicyProto.Sender priority_call_sender = 2;
  if (has_priority_call_sender()) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      2, this->priority_call_sender(), output);
  }

  // optional .android.app.PolicyProto.Sender priority_message_sender = 3;
  if (has_priority_message_sender()) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      3, this->priority_message_sender(), output);
  }

  // repeated .android.app.PolicyProto.SuppressedVisualEffect suppressed_visual_effects = 4;
  for (int i = 0; i < this->suppressed_visual_effects_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      4, this->suppressed_visual_effects(i), output);
  }

  if (_internal_metadata_.have_unknown_fields()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
  // @@protoc_insertion_point(serialize_end:android.app.PolicyProto)
}

::google::protobuf::uint8* PolicyProto::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // @@protoc_insertion_point(serialize_to_array_start:android.app.PolicyProto)
  // repeated .android.app.PolicyProto.Category priority_categories = 1;
  for (int i = 0; i < this->priority_categories_size(); i++) {
    target = ::google::protobuf::internal::WireFormatLite::WriteEnumToArray(
      1, this->priority_categories(i), target);
  }

  // optional .android.app.PolicyProto.Sender priority_call_sender = 2;
  if (has_priority_call_sender()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteEnumToArray(
      2, this->priority_call_sender(), target);
  }

  // optional .android.app.PolicyProto.Sender priority_message_sender = 3;
  if (has_priority_message_sender()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteEnumToArray(
      3, this->priority_message_sender(), target);
  }

  // repeated .android.app.PolicyProto.SuppressedVisualEffect suppressed_visual_effects = 4;
  for (int i = 0; i < this->suppressed_visual_effects_size(); i++) {
    target = ::google::protobuf::internal::WireFormatLite::WriteEnumToArray(
      4, this->suppressed_visual_effects(i), target);
  }

  if (_internal_metadata_.have_unknown_fields()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  // @@protoc_insertion_point(serialize_to_array_end:android.app.PolicyProto)
  return target;
}

int PolicyProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.app.PolicyProto)
  int total_size = 0;

  if (_has_bits_[1 / 32] & 6u) {
    // optional .android.app.PolicyProto.Sender priority_call_sender = 2;
    if (has_priority_call_sender()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::EnumSize(this->priority_call_sender());
    }

    // optional .android.app.PolicyProto.Sender priority_message_sender = 3;
    if (has_priority_message_sender()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::EnumSize(this->priority_message_sender());
    }

  }
  // repeated .android.app.PolicyProto.Category priority_categories = 1;
  {
    int data_size = 0;
    for (int i = 0; i < this->priority_categories_size(); i++) {
      data_size += ::google::protobuf::internal::WireFormatLite::EnumSize(
        this->priority_categories(i));
    }
    total_size += 1 * this->priority_categories_size() + data_size;
  }

  // repeated .android.app.PolicyProto.SuppressedVisualEffect suppressed_visual_effects = 4;
  {
    int data_size = 0;
    for (int i = 0; i < this->suppressed_visual_effects_size(); i++) {
      data_size += ::google::protobuf::internal::WireFormatLite::EnumSize(
        this->suppressed_visual_effects(i));
    }
    total_size += 1 * this->suppressed_visual_effects_size() + data_size;
  }

  if (_internal_metadata_.have_unknown_fields()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void PolicyProto::MergeFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:android.app.PolicyProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  const PolicyProto* source = 
      ::google::protobuf::internal::DynamicCastToGenerated<const PolicyProto>(
          &from);
  if (source == NULL) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:android.app.PolicyProto)
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:android.app.PolicyProto)
    MergeFrom(*source);
  }
}

void PolicyProto::MergeFrom(const PolicyProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.app.PolicyProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  priority_categories_.MergeFrom(from.priority_categories_);
  suppressed_visual_effects_.MergeFrom(from.suppressed_visual_effects_);
  if (from._has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    if (from.has_priority_call_sender()) {
      set_priority_call_sender(from.priority_call_sender());
    }
    if (from.has_priority_message_sender()) {
      set_priority_message_sender(from.priority_message_sender());
    }
  }
  if (from._internal_metadata_.have_unknown_fields()) {
    mutable_unknown_fields()->MergeFrom(from.unknown_fields());
  }
}

void PolicyProto::CopyFrom(const ::google::protobuf::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:android.app.PolicyProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void PolicyProto::CopyFrom(const PolicyProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.app.PolicyProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool PolicyProto::IsInitialized() const {

  return true;
}

void PolicyProto::Swap(PolicyProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void PolicyProto::InternalSwap(PolicyProto* other) {
  priority_categories_.UnsafeArenaSwap(&other->priority_categories_);
  std::swap(priority_call_sender_, other->priority_call_sender_);
  std::swap(priority_message_sender_, other->priority_message_sender_);
  suppressed_visual_effects_.UnsafeArenaSwap(&other->suppressed_visual_effects_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _internal_metadata_.Swap(&other->_internal_metadata_);
  std::swap(_cached_size_, other->_cached_size_);
}

::google::protobuf::Metadata PolicyProto::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = PolicyProto_descriptor_;
  metadata.reflection = PolicyProto_reflection_;
  return metadata;
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// PolicyProto

// repeated .android.app.PolicyProto.Category priority_categories = 1;
int PolicyProto::priority_categories_size() const {
  return priority_categories_.size();
}
void PolicyProto::clear_priority_categories() {
  priority_categories_.Clear();
}
 ::android::app::PolicyProto_Category PolicyProto::priority_categories(int index) const {
  // @@protoc_insertion_point(field_get:android.app.PolicyProto.priority_categories)
  return static_cast< ::android::app::PolicyProto_Category >(priority_categories_.Get(index));
}
 void PolicyProto::set_priority_categories(int index, ::android::app::PolicyProto_Category value) {
  assert(::android::app::PolicyProto_Category_IsValid(value));
  priority_categories_.Set(index, value);
  // @@protoc_insertion_point(field_set:android.app.PolicyProto.priority_categories)
}
 void PolicyProto::add_priority_categories(::android::app::PolicyProto_Category value) {
  assert(::android::app::PolicyProto_Category_IsValid(value));
  priority_categories_.Add(value);
  // @@protoc_insertion_point(field_add:android.app.PolicyProto.priority_categories)
}
 const ::google::protobuf::RepeatedField<int>&
PolicyProto::priority_categories() const {
  // @@protoc_insertion_point(field_list:android.app.PolicyProto.priority_categories)
  return priority_categories_;
}
 ::google::protobuf::RepeatedField<int>*
PolicyProto::mutable_priority_categories() {
  // @@protoc_insertion_point(field_mutable_list:android.app.PolicyProto.priority_categories)
  return &priority_categories_;
}

// optional .android.app.PolicyProto.Sender priority_call_sender = 2;
bool PolicyProto::has_priority_call_sender() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void PolicyProto::set_has_priority_call_sender() {
  _has_bits_[0] |= 0x00000002u;
}
void PolicyProto::clear_has_priority_call_sender() {
  _has_bits_[0] &= ~0x00000002u;
}
void PolicyProto::clear_priority_call_sender() {
  priority_call_sender_ = 0;
  clear_has_priority_call_sender();
}
 ::android::app::PolicyProto_Sender PolicyProto::priority_call_sender() const {
  // @@protoc_insertion_point(field_get:android.app.PolicyProto.priority_call_sender)
  return static_cast< ::android::app::PolicyProto_Sender >(priority_call_sender_);
}
 void PolicyProto::set_priority_call_sender(::android::app::PolicyProto_Sender value) {
  assert(::android::app::PolicyProto_Sender_IsValid(value));
  set_has_priority_call_sender();
  priority_call_sender_ = value;
  // @@protoc_insertion_point(field_set:android.app.PolicyProto.priority_call_sender)
}

// optional .android.app.PolicyProto.Sender priority_message_sender = 3;
bool PolicyProto::has_priority_message_sender() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
void PolicyProto::set_has_priority_message_sender() {
  _has_bits_[0] |= 0x00000004u;
}
void PolicyProto::clear_has_priority_message_sender() {
  _has_bits_[0] &= ~0x00000004u;
}
void PolicyProto::clear_priority_message_sender() {
  priority_message_sender_ = 0;
  clear_has_priority_message_sender();
}
 ::android::app::PolicyProto_Sender PolicyProto::priority_message_sender() const {
  // @@protoc_insertion_point(field_get:android.app.PolicyProto.priority_message_sender)
  return static_cast< ::android::app::PolicyProto_Sender >(priority_message_sender_);
}
 void PolicyProto::set_priority_message_sender(::android::app::PolicyProto_Sender value) {
  assert(::android::app::PolicyProto_Sender_IsValid(value));
  set_has_priority_message_sender();
  priority_message_sender_ = value;
  // @@protoc_insertion_point(field_set:android.app.PolicyProto.priority_message_sender)
}

// repeated .android.app.PolicyProto.SuppressedVisualEffect suppressed_visual_effects = 4;
int PolicyProto::suppressed_visual_effects_size() const {
  return suppressed_visual_effects_.size();
}
void PolicyProto::clear_suppressed_visual_effects() {
  suppressed_visual_effects_.Clear();
}
 ::android::app::PolicyProto_SuppressedVisualEffect PolicyProto::suppressed_visual_effects(int index) const {
  // @@protoc_insertion_point(field_get:android.app.PolicyProto.suppressed_visual_effects)
  return static_cast< ::android::app::PolicyProto_SuppressedVisualEffect >(suppressed_visual_effects_.Get(index));
}
 void PolicyProto::set_suppressed_visual_effects(int index, ::android::app::PolicyProto_SuppressedVisualEffect value) {
  assert(::android::app::PolicyProto_SuppressedVisualEffect_IsValid(value));
  suppressed_visual_effects_.Set(index, value);
  // @@protoc_insertion_point(field_set:android.app.PolicyProto.suppressed_visual_effects)
}
 void PolicyProto::add_suppressed_visual_effects(::android::app::PolicyProto_SuppressedVisualEffect value) {
  assert(::android::app::PolicyProto_SuppressedVisualEffect_IsValid(value));
  suppressed_visual_effects_.Add(value);
  // @@protoc_insertion_point(field_add:android.app.PolicyProto.suppressed_visual_effects)
}
 const ::google::protobuf::RepeatedField<int>&
PolicyProto::suppressed_visual_effects() const {
  // @@protoc_insertion_point(field_list:android.app.PolicyProto.suppressed_visual_effects)
  return suppressed_visual_effects_;
}
 ::google::protobuf::RepeatedField<int>*
PolicyProto::mutable_suppressed_visual_effects() {
  // @@protoc_insertion_point(field_mutable_list:android.app.PolicyProto.suppressed_visual_effects)
  return &suppressed_visual_effects_;
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace app
}  // namespace android

// @@protoc_insertion_point(global_scope)
