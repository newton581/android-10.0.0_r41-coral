// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/service/procstats_enum.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fservice_2fprocstats_5fenum_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fservice_2fprocstats_5fenum_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace service {
namespace procstats {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fservice_2fprocstats_5fenum_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fservice_2fprocstats_5fenum_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fservice_2fprocstats_5fenum_2eproto();


enum ScreenState {
  SCREEN_STATE_UNKNOWN = 0,
  SCREEN_STATE_OFF = 1,
  SCREEN_STATE_ON = 2
};
bool ScreenState_IsValid(int value);
const ScreenState ScreenState_MIN = SCREEN_STATE_UNKNOWN;
const ScreenState ScreenState_MAX = SCREEN_STATE_ON;
const int ScreenState_ARRAYSIZE = ScreenState_MAX + 1;

const ::google::protobuf::EnumDescriptor* ScreenState_descriptor();
inline const ::std::string& ScreenState_Name(ScreenState value) {
  return ::google::protobuf::internal::NameOfEnum(
    ScreenState_descriptor(), value);
}
inline bool ScreenState_Parse(
    const ::std::string& name, ScreenState* value) {
  return ::google::protobuf::internal::ParseNamedEnum<ScreenState>(
    ScreenState_descriptor(), name, value);
}
enum MemoryState {
  MEMORY_STATE_UNKNOWN = 0,
  MEMORY_STATE_NORMAL = 1,
  MEMORY_STATE_MODERATE = 2,
  MEMORY_STATE_LOW = 3,
  MEMORY_STATE_CRITICAL = 4
};
bool MemoryState_IsValid(int value);
const MemoryState MemoryState_MIN = MEMORY_STATE_UNKNOWN;
const MemoryState MemoryState_MAX = MEMORY_STATE_CRITICAL;
const int MemoryState_ARRAYSIZE = MemoryState_MAX + 1;

const ::google::protobuf::EnumDescriptor* MemoryState_descriptor();
inline const ::std::string& MemoryState_Name(MemoryState value) {
  return ::google::protobuf::internal::NameOfEnum(
    MemoryState_descriptor(), value);
}
inline bool MemoryState_Parse(
    const ::std::string& name, MemoryState* value) {
  return ::google::protobuf::internal::ParseNamedEnum<MemoryState>(
    MemoryState_descriptor(), name, value);
}
enum ProcessState {
  PROCESS_STATE_UNKNOWN = 0,
  PROCESS_STATE_PERSISTENT = 1,
  PROCESS_STATE_TOP = 2,
  PROCESS_STATE_IMPORTANT_FOREGROUND = 3,
  PROCESS_STATE_IMPORTANT_BACKGROUND = 4,
  PROCESS_STATE_BACKUP = 5,
  PROCESS_STATE_SERVICE = 6,
  PROCESS_STATE_SERVICE_RESTARTING = 7,
  PROCESS_STATE_RECEIVER = 8,
  PROCESS_STATE_HEAVY_WEIGHT = 9,
  PROCESS_STATE_HOME = 10,
  PROCESS_STATE_LAST_ACTIVITY = 11,
  PROCESS_STATE_CACHED_ACTIVITY = 12,
  PROCESS_STATE_CACHED_ACTIVITY_CLIENT = 13,
  PROCESS_STATE_CACHED_EMPTY = 14
};
bool ProcessState_IsValid(int value);
const ProcessState ProcessState_MIN = PROCESS_STATE_UNKNOWN;
const ProcessState ProcessState_MAX = PROCESS_STATE_CACHED_EMPTY;
const int ProcessState_ARRAYSIZE = ProcessState_MAX + 1;

const ::google::protobuf::EnumDescriptor* ProcessState_descriptor();
inline const ::std::string& ProcessState_Name(ProcessState value) {
  return ::google::protobuf::internal::NameOfEnum(
    ProcessState_descriptor(), value);
}
inline bool ProcessState_Parse(
    const ::std::string& name, ProcessState* value) {
  return ::google::protobuf::internal::ParseNamedEnum<ProcessState>(
    ProcessState_descriptor(), name, value);
}
enum ServiceOperationState {
  SERVICE_OPERATION_STATE_UNKNOWN = 0,
  SERVICE_OPERATION_STATE_RUNNING = 1,
  SERVICE_OPERATION_STATE_STARTED = 2,
  SERVICE_OPERATION_STATE_FOREGROUND = 3,
  SERVICE_OPERATION_STATE_BOUND = 4,
  SERVICE_OPERATION_STATE_EXECUTING = 5
};
bool ServiceOperationState_IsValid(int value);
const ServiceOperationState ServiceOperationState_MIN = SERVICE_OPERATION_STATE_UNKNOWN;
const ServiceOperationState ServiceOperationState_MAX = SERVICE_OPERATION_STATE_EXECUTING;
const int ServiceOperationState_ARRAYSIZE = ServiceOperationState_MAX + 1;

const ::google::protobuf::EnumDescriptor* ServiceOperationState_descriptor();
inline const ::std::string& ServiceOperationState_Name(ServiceOperationState value) {
  return ::google::protobuf::internal::NameOfEnum(
    ServiceOperationState_descriptor(), value);
}
inline bool ServiceOperationState_Parse(
    const ::std::string& name, ServiceOperationState* value) {
  return ::google::protobuf::internal::ParseNamedEnum<ServiceOperationState>(
    ServiceOperationState_descriptor(), name, value);
}
// ===================================================================


// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace procstats
}  // namespace service
}  // namespace android

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::android::service::procstats::ScreenState> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::service::procstats::ScreenState>() {
  return ::android::service::procstats::ScreenState_descriptor();
}
template <> struct is_proto_enum< ::android::service::procstats::MemoryState> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::service::procstats::MemoryState>() {
  return ::android::service::procstats::MemoryState_descriptor();
}
template <> struct is_proto_enum< ::android::service::procstats::ProcessState> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::service::procstats::ProcessState>() {
  return ::android::service::procstats::ProcessState_descriptor();
}
template <> struct is_proto_enum< ::android::service::procstats::ServiceOperationState> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::android::service::procstats::ServiceOperationState>() {
  return ::android::service::procstats::ServiceOperationState_descriptor();
}

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fservice_2fprocstats_5fenum_2eproto__INCLUDED
