// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/os/cpufreq.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/os/cpufreq.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace os {

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto() {
  delete CpuFreqProto::default_instance_;
  delete CpuFreqProto_Stats::default_instance_;
  delete CpuFreqProto_Stats_TimeInState::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ::android::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
  CpuFreqProto::default_instance_ = new CpuFreqProto();
  CpuFreqProto_Stats::default_instance_ = new CpuFreqProto_Stats();
  CpuFreqProto_Stats_TimeInState::default_instance_ = new CpuFreqProto_Stats_TimeInState();
  CpuFreqProto::default_instance_->InitAsDefaultInstance();
  CpuFreqProto_Stats::default_instance_->InitAsDefaultInstance();
  CpuFreqProto_Stats_TimeInState::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto_once_);
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto_once_,
                 &protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForCpuFreqProto(
    CpuFreqProto* ptr) {
  return ptr->mutable_unknown_fields();
}

static ::std::string* MutableUnknownFieldsForCpuFreqProto_Stats(
    CpuFreqProto_Stats* ptr) {
  return ptr->mutable_unknown_fields();
}

static ::std::string* MutableUnknownFieldsForCpuFreqProto_Stats_TimeInState(
    CpuFreqProto_Stats_TimeInState* ptr) {
  return ptr->mutable_unknown_fields();
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int CpuFreqProto_Stats_TimeInState::kStateKhzFieldNumber;
const int CpuFreqProto_Stats_TimeInState::kTimeJiffyFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

CpuFreqProto_Stats_TimeInState::CpuFreqProto_Stats_TimeInState()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.os.CpuFreqProto.Stats.TimeInState)
}

void CpuFreqProto_Stats_TimeInState::InitAsDefaultInstance() {
}

CpuFreqProto_Stats_TimeInState::CpuFreqProto_Stats_TimeInState(const CpuFreqProto_Stats_TimeInState& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.os.CpuFreqProto.Stats.TimeInState)
}

void CpuFreqProto_Stats_TimeInState::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  state_khz_ = 0;
  time_jiffy_ = GOOGLE_LONGLONG(0);
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

CpuFreqProto_Stats_TimeInState::~CpuFreqProto_Stats_TimeInState() {
  // @@protoc_insertion_point(destructor:android.os.CpuFreqProto.Stats.TimeInState)
  SharedDtor();
}

void CpuFreqProto_Stats_TimeInState::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void CpuFreqProto_Stats_TimeInState::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const CpuFreqProto_Stats_TimeInState& CpuFreqProto_Stats_TimeInState::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

CpuFreqProto_Stats_TimeInState* CpuFreqProto_Stats_TimeInState::default_instance_ = NULL;

CpuFreqProto_Stats_TimeInState* CpuFreqProto_Stats_TimeInState::New(::google::protobuf::Arena* arena) const {
  CpuFreqProto_Stats_TimeInState* n = new CpuFreqProto_Stats_TimeInState;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void CpuFreqProto_Stats_TimeInState::Clear() {
// @@protoc_insertion_point(message_clear_start:android.os.CpuFreqProto.Stats.TimeInState)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(CpuFreqProto_Stats_TimeInState, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<CpuFreqProto_Stats_TimeInState*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  ZR_(time_jiffy_, state_khz_);

#undef ZR_HELPER_
#undef ZR_

  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool CpuFreqProto_Stats_TimeInState::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForCpuFreqProto_Stats_TimeInState, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.os.CpuFreqProto.Stats.TimeInState)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional int32 state_khz = 1;
      case 1: {
        if (tag == 8) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &state_khz_)));
          set_has_state_khz();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(16)) goto parse_time_jiffy;
        break;
      }

      // optional int64 time_jiffy = 2;
      case 2: {
        if (tag == 16) {
         parse_time_jiffy:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int64, ::google::protobuf::internal::WireFormatLite::TYPE_INT64>(
                 input, &time_jiffy_)));
          set_has_time_jiffy();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.os.CpuFreqProto.Stats.TimeInState)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.os.CpuFreqProto.Stats.TimeInState)
  return false;
#undef DO_
}

void CpuFreqProto_Stats_TimeInState::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.os.CpuFreqProto.Stats.TimeInState)
  // optional int32 state_khz = 1;
  if (has_state_khz()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(1, this->state_khz(), output);
  }

  // optional int64 time_jiffy = 2;
  if (has_time_jiffy()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt64(2, this->time_jiffy(), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.os.CpuFreqProto.Stats.TimeInState)
}

int CpuFreqProto_Stats_TimeInState::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.os.CpuFreqProto.Stats.TimeInState)
  int total_size = 0;

  if (_has_bits_[0 / 32] & 3u) {
    // optional int32 state_khz = 1;
    if (has_state_khz()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->state_khz());
    }

    // optional int64 time_jiffy = 2;
    if (has_time_jiffy()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int64Size(
          this->time_jiffy());
    }

  }
  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void CpuFreqProto_Stats_TimeInState::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const CpuFreqProto_Stats_TimeInState*>(&from));
}

void CpuFreqProto_Stats_TimeInState::MergeFrom(const CpuFreqProto_Stats_TimeInState& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.os.CpuFreqProto.Stats.TimeInState)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_state_khz()) {
      set_state_khz(from.state_khz());
    }
    if (from.has_time_jiffy()) {
      set_time_jiffy(from.time_jiffy());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void CpuFreqProto_Stats_TimeInState::CopyFrom(const CpuFreqProto_Stats_TimeInState& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.os.CpuFreqProto.Stats.TimeInState)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool CpuFreqProto_Stats_TimeInState::IsInitialized() const {

  return true;
}

void CpuFreqProto_Stats_TimeInState::Swap(CpuFreqProto_Stats_TimeInState* other) {
  if (other == this) return;
  InternalSwap(other);
}
void CpuFreqProto_Stats_TimeInState::InternalSwap(CpuFreqProto_Stats_TimeInState* other) {
  std::swap(state_khz_, other->state_khz_);
  std::swap(time_jiffy_, other->time_jiffy_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string CpuFreqProto_Stats_TimeInState::GetTypeName() const {
  return "android.os.CpuFreqProto.Stats.TimeInState";
}


// -------------------------------------------------------------------

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int CpuFreqProto_Stats::kCpuNameFieldNumber;
const int CpuFreqProto_Stats::kTimesFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

CpuFreqProto_Stats::CpuFreqProto_Stats()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.os.CpuFreqProto.Stats)
}

void CpuFreqProto_Stats::InitAsDefaultInstance() {
}

CpuFreqProto_Stats::CpuFreqProto_Stats(const CpuFreqProto_Stats& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.os.CpuFreqProto.Stats)
}

void CpuFreqProto_Stats::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  cpu_name_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

CpuFreqProto_Stats::~CpuFreqProto_Stats() {
  // @@protoc_insertion_point(destructor:android.os.CpuFreqProto.Stats)
  SharedDtor();
}

void CpuFreqProto_Stats::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  cpu_name_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void CpuFreqProto_Stats::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const CpuFreqProto_Stats& CpuFreqProto_Stats::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

CpuFreqProto_Stats* CpuFreqProto_Stats::default_instance_ = NULL;

CpuFreqProto_Stats* CpuFreqProto_Stats::New(::google::protobuf::Arena* arena) const {
  CpuFreqProto_Stats* n = new CpuFreqProto_Stats;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void CpuFreqProto_Stats::Clear() {
// @@protoc_insertion_point(message_clear_start:android.os.CpuFreqProto.Stats)
  if (has_cpu_name()) {
    cpu_name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }
  times_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool CpuFreqProto_Stats::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForCpuFreqProto_Stats, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.os.CpuFreqProto.Stats)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional string cpu_name = 1;
      case 1: {
        if (tag == 10) {
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_cpu_name()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(18)) goto parse_times;
        break;
      }

      // repeated .android.os.CpuFreqProto.Stats.TimeInState times = 2;
      case 2: {
        if (tag == 18) {
         parse_times:
          DO_(input->IncrementRecursionDepth());
         parse_loop_times:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtualNoRecursionDepth(
                input, add_times()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(18)) goto parse_loop_times;
        input->UnsafeDecrementRecursionDepth();
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.os.CpuFreqProto.Stats)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.os.CpuFreqProto.Stats)
  return false;
#undef DO_
}

void CpuFreqProto_Stats::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.os.CpuFreqProto.Stats)
  // optional string cpu_name = 1;
  if (has_cpu_name()) {
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      1, this->cpu_name(), output);
  }

  // repeated .android.os.CpuFreqProto.Stats.TimeInState times = 2;
  for (unsigned int i = 0, n = this->times_size(); i < n; i++) {
    ::google::protobuf::internal::WireFormatLite::WriteMessage(
      2, this->times(i), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.os.CpuFreqProto.Stats)
}

int CpuFreqProto_Stats::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.os.CpuFreqProto.Stats)
  int total_size = 0;

  // optional string cpu_name = 1;
  if (has_cpu_name()) {
    total_size += 1 +
      ::google::protobuf::internal::WireFormatLite::StringSize(
        this->cpu_name());
  }

  // repeated .android.os.CpuFreqProto.Stats.TimeInState times = 2;
  total_size += 1 * this->times_size();
  for (int i = 0; i < this->times_size(); i++) {
    total_size +=
      ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
        this->times(i));
  }

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void CpuFreqProto_Stats::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const CpuFreqProto_Stats*>(&from));
}

void CpuFreqProto_Stats::MergeFrom(const CpuFreqProto_Stats& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.os.CpuFreqProto.Stats)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  times_.MergeFrom(from.times_);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_cpu_name()) {
      set_has_cpu_name();
      cpu_name_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.cpu_name_);
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void CpuFreqProto_Stats::CopyFrom(const CpuFreqProto_Stats& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.os.CpuFreqProto.Stats)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool CpuFreqProto_Stats::IsInitialized() const {

  return true;
}

void CpuFreqProto_Stats::Swap(CpuFreqProto_Stats* other) {
  if (other == this) return;
  InternalSwap(other);
}
void CpuFreqProto_Stats::InternalSwap(CpuFreqProto_Stats* other) {
  cpu_name_.Swap(&other->cpu_name_);
  times_.UnsafeArenaSwap(&other->times_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string CpuFreqProto_Stats::GetTypeName() const {
  return "android.os.CpuFreqProto.Stats";
}


// -------------------------------------------------------------------

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int CpuFreqProto::kJiffyHzFieldNumber;
const int CpuFreqProto::kCpuFreqsFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

CpuFreqProto::CpuFreqProto()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.os.CpuFreqProto)
}

void CpuFreqProto::InitAsDefaultInstance() {
}

CpuFreqProto::CpuFreqProto(const CpuFreqProto& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.os.CpuFreqProto)
}

void CpuFreqProto::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  jiffy_hz_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

CpuFreqProto::~CpuFreqProto() {
  // @@protoc_insertion_point(destructor:android.os.CpuFreqProto)
  SharedDtor();
}

void CpuFreqProto::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void CpuFreqProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const CpuFreqProto& CpuFreqProto::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fcpufreq_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

CpuFreqProto* CpuFreqProto::default_instance_ = NULL;

CpuFreqProto* CpuFreqProto::New(::google::protobuf::Arena* arena) const {
  CpuFreqProto* n = new CpuFreqProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void CpuFreqProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.os.CpuFreqProto)
  jiffy_hz_ = 0;
  cpu_freqs_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool CpuFreqProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForCpuFreqProto, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.os.CpuFreqProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional int32 jiffy_hz = 1;
      case 1: {
        if (tag == 8) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &jiffy_hz_)));
          set_has_jiffy_hz();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(18)) goto parse_cpu_freqs;
        break;
      }

      // repeated .android.os.CpuFreqProto.Stats cpu_freqs = 2;
      case 2: {
        if (tag == 18) {
         parse_cpu_freqs:
          DO_(input->IncrementRecursionDepth());
         parse_loop_cpu_freqs:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtualNoRecursionDepth(
                input, add_cpu_freqs()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(18)) goto parse_loop_cpu_freqs;
        input->UnsafeDecrementRecursionDepth();
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.os.CpuFreqProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.os.CpuFreqProto)
  return false;
#undef DO_
}

void CpuFreqProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.os.CpuFreqProto)
  // optional int32 jiffy_hz = 1;
  if (has_jiffy_hz()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(1, this->jiffy_hz(), output);
  }

  // repeated .android.os.CpuFreqProto.Stats cpu_freqs = 2;
  for (unsigned int i = 0, n = this->cpu_freqs_size(); i < n; i++) {
    ::google::protobuf::internal::WireFormatLite::WriteMessage(
      2, this->cpu_freqs(i), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.os.CpuFreqProto)
}

int CpuFreqProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.os.CpuFreqProto)
  int total_size = 0;

  // optional int32 jiffy_hz = 1;
  if (has_jiffy_hz()) {
    total_size += 1 +
      ::google::protobuf::internal::WireFormatLite::Int32Size(
        this->jiffy_hz());
  }

  // repeated .android.os.CpuFreqProto.Stats cpu_freqs = 2;
  total_size += 1 * this->cpu_freqs_size();
  for (int i = 0; i < this->cpu_freqs_size(); i++) {
    total_size +=
      ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
        this->cpu_freqs(i));
  }

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void CpuFreqProto::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const CpuFreqProto*>(&from));
}

void CpuFreqProto::MergeFrom(const CpuFreqProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.os.CpuFreqProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  cpu_freqs_.MergeFrom(from.cpu_freqs_);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_jiffy_hz()) {
      set_jiffy_hz(from.jiffy_hz());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void CpuFreqProto::CopyFrom(const CpuFreqProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.os.CpuFreqProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool CpuFreqProto::IsInitialized() const {

  return true;
}

void CpuFreqProto::Swap(CpuFreqProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void CpuFreqProto::InternalSwap(CpuFreqProto* other) {
  std::swap(jiffy_hz_, other->jiffy_hz_);
  cpu_freqs_.UnsafeArenaSwap(&other->cpu_freqs_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string CpuFreqProto::GetTypeName() const {
  return "android.os.CpuFreqProto";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// CpuFreqProto_Stats_TimeInState

// optional int32 state_khz = 1;
bool CpuFreqProto_Stats_TimeInState::has_state_khz() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
void CpuFreqProto_Stats_TimeInState::set_has_state_khz() {
  _has_bits_[0] |= 0x00000001u;
}
void CpuFreqProto_Stats_TimeInState::clear_has_state_khz() {
  _has_bits_[0] &= ~0x00000001u;
}
void CpuFreqProto_Stats_TimeInState::clear_state_khz() {
  state_khz_ = 0;
  clear_has_state_khz();
}
 ::google::protobuf::int32 CpuFreqProto_Stats_TimeInState::state_khz() const {
  // @@protoc_insertion_point(field_get:android.os.CpuFreqProto.Stats.TimeInState.state_khz)
  return state_khz_;
}
 void CpuFreqProto_Stats_TimeInState::set_state_khz(::google::protobuf::int32 value) {
  set_has_state_khz();
  state_khz_ = value;
  // @@protoc_insertion_point(field_set:android.os.CpuFreqProto.Stats.TimeInState.state_khz)
}

// optional int64 time_jiffy = 2;
bool CpuFreqProto_Stats_TimeInState::has_time_jiffy() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void CpuFreqProto_Stats_TimeInState::set_has_time_jiffy() {
  _has_bits_[0] |= 0x00000002u;
}
void CpuFreqProto_Stats_TimeInState::clear_has_time_jiffy() {
  _has_bits_[0] &= ~0x00000002u;
}
void CpuFreqProto_Stats_TimeInState::clear_time_jiffy() {
  time_jiffy_ = GOOGLE_LONGLONG(0);
  clear_has_time_jiffy();
}
 ::google::protobuf::int64 CpuFreqProto_Stats_TimeInState::time_jiffy() const {
  // @@protoc_insertion_point(field_get:android.os.CpuFreqProto.Stats.TimeInState.time_jiffy)
  return time_jiffy_;
}
 void CpuFreqProto_Stats_TimeInState::set_time_jiffy(::google::protobuf::int64 value) {
  set_has_time_jiffy();
  time_jiffy_ = value;
  // @@protoc_insertion_point(field_set:android.os.CpuFreqProto.Stats.TimeInState.time_jiffy)
}

// -------------------------------------------------------------------

// CpuFreqProto_Stats

// optional string cpu_name = 1;
bool CpuFreqProto_Stats::has_cpu_name() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
void CpuFreqProto_Stats::set_has_cpu_name() {
  _has_bits_[0] |= 0x00000001u;
}
void CpuFreqProto_Stats::clear_has_cpu_name() {
  _has_bits_[0] &= ~0x00000001u;
}
void CpuFreqProto_Stats::clear_cpu_name() {
  cpu_name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_cpu_name();
}
 const ::std::string& CpuFreqProto_Stats::cpu_name() const {
  // @@protoc_insertion_point(field_get:android.os.CpuFreqProto.Stats.cpu_name)
  return cpu_name_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void CpuFreqProto_Stats::set_cpu_name(const ::std::string& value) {
  set_has_cpu_name();
  cpu_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:android.os.CpuFreqProto.Stats.cpu_name)
}
 void CpuFreqProto_Stats::set_cpu_name(const char* value) {
  set_has_cpu_name();
  cpu_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:android.os.CpuFreqProto.Stats.cpu_name)
}
 void CpuFreqProto_Stats::set_cpu_name(const char* value, size_t size) {
  set_has_cpu_name();
  cpu_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:android.os.CpuFreqProto.Stats.cpu_name)
}
 ::std::string* CpuFreqProto_Stats::mutable_cpu_name() {
  set_has_cpu_name();
  // @@protoc_insertion_point(field_mutable:android.os.CpuFreqProto.Stats.cpu_name)
  return cpu_name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 ::std::string* CpuFreqProto_Stats::release_cpu_name() {
  // @@protoc_insertion_point(field_release:android.os.CpuFreqProto.Stats.cpu_name)
  clear_has_cpu_name();
  return cpu_name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void CpuFreqProto_Stats::set_allocated_cpu_name(::std::string* cpu_name) {
  if (cpu_name != NULL) {
    set_has_cpu_name();
  } else {
    clear_has_cpu_name();
  }
  cpu_name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), cpu_name);
  // @@protoc_insertion_point(field_set_allocated:android.os.CpuFreqProto.Stats.cpu_name)
}

// repeated .android.os.CpuFreqProto.Stats.TimeInState times = 2;
int CpuFreqProto_Stats::times_size() const {
  return times_.size();
}
void CpuFreqProto_Stats::clear_times() {
  times_.Clear();
}
const ::android::os::CpuFreqProto_Stats_TimeInState& CpuFreqProto_Stats::times(int index) const {
  // @@protoc_insertion_point(field_get:android.os.CpuFreqProto.Stats.times)
  return times_.Get(index);
}
::android::os::CpuFreqProto_Stats_TimeInState* CpuFreqProto_Stats::mutable_times(int index) {
  // @@protoc_insertion_point(field_mutable:android.os.CpuFreqProto.Stats.times)
  return times_.Mutable(index);
}
::android::os::CpuFreqProto_Stats_TimeInState* CpuFreqProto_Stats::add_times() {
  // @@protoc_insertion_point(field_add:android.os.CpuFreqProto.Stats.times)
  return times_.Add();
}
::google::protobuf::RepeatedPtrField< ::android::os::CpuFreqProto_Stats_TimeInState >*
CpuFreqProto_Stats::mutable_times() {
  // @@protoc_insertion_point(field_mutable_list:android.os.CpuFreqProto.Stats.times)
  return &times_;
}
const ::google::protobuf::RepeatedPtrField< ::android::os::CpuFreqProto_Stats_TimeInState >&
CpuFreqProto_Stats::times() const {
  // @@protoc_insertion_point(field_list:android.os.CpuFreqProto.Stats.times)
  return times_;
}

// -------------------------------------------------------------------

// CpuFreqProto

// optional int32 jiffy_hz = 1;
bool CpuFreqProto::has_jiffy_hz() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
void CpuFreqProto::set_has_jiffy_hz() {
  _has_bits_[0] |= 0x00000001u;
}
void CpuFreqProto::clear_has_jiffy_hz() {
  _has_bits_[0] &= ~0x00000001u;
}
void CpuFreqProto::clear_jiffy_hz() {
  jiffy_hz_ = 0;
  clear_has_jiffy_hz();
}
 ::google::protobuf::int32 CpuFreqProto::jiffy_hz() const {
  // @@protoc_insertion_point(field_get:android.os.CpuFreqProto.jiffy_hz)
  return jiffy_hz_;
}
 void CpuFreqProto::set_jiffy_hz(::google::protobuf::int32 value) {
  set_has_jiffy_hz();
  jiffy_hz_ = value;
  // @@protoc_insertion_point(field_set:android.os.CpuFreqProto.jiffy_hz)
}

// repeated .android.os.CpuFreqProto.Stats cpu_freqs = 2;
int CpuFreqProto::cpu_freqs_size() const {
  return cpu_freqs_.size();
}
void CpuFreqProto::clear_cpu_freqs() {
  cpu_freqs_.Clear();
}
const ::android::os::CpuFreqProto_Stats& CpuFreqProto::cpu_freqs(int index) const {
  // @@protoc_insertion_point(field_get:android.os.CpuFreqProto.cpu_freqs)
  return cpu_freqs_.Get(index);
}
::android::os::CpuFreqProto_Stats* CpuFreqProto::mutable_cpu_freqs(int index) {
  // @@protoc_insertion_point(field_mutable:android.os.CpuFreqProto.cpu_freqs)
  return cpu_freqs_.Mutable(index);
}
::android::os::CpuFreqProto_Stats* CpuFreqProto::add_cpu_freqs() {
  // @@protoc_insertion_point(field_add:android.os.CpuFreqProto.cpu_freqs)
  return cpu_freqs_.Add();
}
::google::protobuf::RepeatedPtrField< ::android::os::CpuFreqProto_Stats >*
CpuFreqProto::mutable_cpu_freqs() {
  // @@protoc_insertion_point(field_mutable_list:android.os.CpuFreqProto.cpu_freqs)
  return &cpu_freqs_;
}
const ::google::protobuf::RepeatedPtrField< ::android::os::CpuFreqProto_Stats >&
CpuFreqProto::cpu_freqs() const {
  // @@protoc_insertion_point(field_list:android.os.CpuFreqProto.cpu_freqs)
  return cpu_freqs_;
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace os
}  // namespace android

// @@protoc_insertion_point(global_scope)
