// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/providers/settings.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/providers/settings.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace providers {
namespace settings {

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto() {
  delete SettingsServiceDumpProto::default_instance_;
  delete UserSettingsProto::default_instance_;
  delete SettingsProto::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ::android::providers::settings::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2fglobal_2eproto();
  ::android::providers::settings::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2fsecure_2eproto();
  ::android::providers::settings::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2fsystem_2eproto();
  ::android::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
  SettingsServiceDumpProto::default_instance_ = new SettingsServiceDumpProto();
  UserSettingsProto::default_instance_ = new UserSettingsProto();
  SettingsProto::default_instance_ = new SettingsProto();
  SettingsServiceDumpProto::default_instance_->InitAsDefaultInstance();
  UserSettingsProto::default_instance_->InitAsDefaultInstance();
  SettingsProto::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto_once_);
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto_once_,
                 &protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForSettingsServiceDumpProto(
    SettingsServiceDumpProto* ptr) {
  return ptr->mutable_unknown_fields();
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int SettingsServiceDumpProto::kUserSettingsFieldNumber;
const int SettingsServiceDumpProto::kGlobalSettingsFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

SettingsServiceDumpProto::SettingsServiceDumpProto()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.providers.settings.SettingsServiceDumpProto)
}

void SettingsServiceDumpProto::InitAsDefaultInstance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  global_settings_ = const_cast< ::android::providers::settings::GlobalSettingsProto*>(
      ::android::providers::settings::GlobalSettingsProto::internal_default_instance());
#else
  global_settings_ = const_cast< ::android::providers::settings::GlobalSettingsProto*>(&::android::providers::settings::GlobalSettingsProto::default_instance());
#endif
}

SettingsServiceDumpProto::SettingsServiceDumpProto(const SettingsServiceDumpProto& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.providers.settings.SettingsServiceDumpProto)
}

void SettingsServiceDumpProto::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  global_settings_ = NULL;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

SettingsServiceDumpProto::~SettingsServiceDumpProto() {
  // @@protoc_insertion_point(destructor:android.providers.settings.SettingsServiceDumpProto)
  SharedDtor();
}

void SettingsServiceDumpProto::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
    delete global_settings_;
  }
}

void SettingsServiceDumpProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const SettingsServiceDumpProto& SettingsServiceDumpProto::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

SettingsServiceDumpProto* SettingsServiceDumpProto::default_instance_ = NULL;

SettingsServiceDumpProto* SettingsServiceDumpProto::New(::google::protobuf::Arena* arena) const {
  SettingsServiceDumpProto* n = new SettingsServiceDumpProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void SettingsServiceDumpProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.providers.settings.SettingsServiceDumpProto)
  if (has_global_settings()) {
    if (global_settings_ != NULL) global_settings_->::android::providers::settings::GlobalSettingsProto::Clear();
  }
  user_settings_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool SettingsServiceDumpProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForSettingsServiceDumpProto, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.providers.settings.SettingsServiceDumpProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // repeated .android.providers.settings.UserSettingsProto user_settings = 1;
      case 1: {
        if (tag == 10) {
          DO_(input->IncrementRecursionDepth());
         parse_loop_user_settings:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtualNoRecursionDepth(
                input, add_user_settings()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(10)) goto parse_loop_user_settings;
        input->UnsafeDecrementRecursionDepth();
        if (input->ExpectTag(18)) goto parse_global_settings;
        break;
      }

      // optional .android.providers.settings.GlobalSettingsProto global_settings = 2;
      case 2: {
        if (tag == 18) {
         parse_global_settings:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtual(
               input, mutable_global_settings()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.providers.settings.SettingsServiceDumpProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.providers.settings.SettingsServiceDumpProto)
  return false;
#undef DO_
}

void SettingsServiceDumpProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.providers.settings.SettingsServiceDumpProto)
  // repeated .android.providers.settings.UserSettingsProto user_settings = 1;
  for (unsigned int i = 0, n = this->user_settings_size(); i < n; i++) {
    ::google::protobuf::internal::WireFormatLite::WriteMessage(
      1, this->user_settings(i), output);
  }

  // optional .android.providers.settings.GlobalSettingsProto global_settings = 2;
  if (has_global_settings()) {
    ::google::protobuf::internal::WireFormatLite::WriteMessage(
      2, *this->global_settings_, output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.providers.settings.SettingsServiceDumpProto)
}

int SettingsServiceDumpProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.providers.settings.SettingsServiceDumpProto)
  int total_size = 0;

  // optional .android.providers.settings.GlobalSettingsProto global_settings = 2;
  if (has_global_settings()) {
    total_size += 1 +
      ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
        *this->global_settings_);
  }

  // repeated .android.providers.settings.UserSettingsProto user_settings = 1;
  total_size += 1 * this->user_settings_size();
  for (int i = 0; i < this->user_settings_size(); i++) {
    total_size +=
      ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
        this->user_settings(i));
  }

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void SettingsServiceDumpProto::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const SettingsServiceDumpProto*>(&from));
}

void SettingsServiceDumpProto::MergeFrom(const SettingsServiceDumpProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.providers.settings.SettingsServiceDumpProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  user_settings_.MergeFrom(from.user_settings_);
  if (from._has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    if (from.has_global_settings()) {
      mutable_global_settings()->::android::providers::settings::GlobalSettingsProto::MergeFrom(from.global_settings());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void SettingsServiceDumpProto::CopyFrom(const SettingsServiceDumpProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.providers.settings.SettingsServiceDumpProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool SettingsServiceDumpProto::IsInitialized() const {

  return true;
}

void SettingsServiceDumpProto::Swap(SettingsServiceDumpProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void SettingsServiceDumpProto::InternalSwap(SettingsServiceDumpProto* other) {
  user_settings_.UnsafeArenaSwap(&other->user_settings_);
  std::swap(global_settings_, other->global_settings_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string SettingsServiceDumpProto::GetTypeName() const {
  return "android.providers.settings.SettingsServiceDumpProto";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// SettingsServiceDumpProto

// repeated .android.providers.settings.UserSettingsProto user_settings = 1;
int SettingsServiceDumpProto::user_settings_size() const {
  return user_settings_.size();
}
void SettingsServiceDumpProto::clear_user_settings() {
  user_settings_.Clear();
}
const ::android::providers::settings::UserSettingsProto& SettingsServiceDumpProto::user_settings(int index) const {
  // @@protoc_insertion_point(field_get:android.providers.settings.SettingsServiceDumpProto.user_settings)
  return user_settings_.Get(index);
}
::android::providers::settings::UserSettingsProto* SettingsServiceDumpProto::mutable_user_settings(int index) {
  // @@protoc_insertion_point(field_mutable:android.providers.settings.SettingsServiceDumpProto.user_settings)
  return user_settings_.Mutable(index);
}
::android::providers::settings::UserSettingsProto* SettingsServiceDumpProto::add_user_settings() {
  // @@protoc_insertion_point(field_add:android.providers.settings.SettingsServiceDumpProto.user_settings)
  return user_settings_.Add();
}
::google::protobuf::RepeatedPtrField< ::android::providers::settings::UserSettingsProto >*
SettingsServiceDumpProto::mutable_user_settings() {
  // @@protoc_insertion_point(field_mutable_list:android.providers.settings.SettingsServiceDumpProto.user_settings)
  return &user_settings_;
}
const ::google::protobuf::RepeatedPtrField< ::android::providers::settings::UserSettingsProto >&
SettingsServiceDumpProto::user_settings() const {
  // @@protoc_insertion_point(field_list:android.providers.settings.SettingsServiceDumpProto.user_settings)
  return user_settings_;
}

// optional .android.providers.settings.GlobalSettingsProto global_settings = 2;
bool SettingsServiceDumpProto::has_global_settings() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void SettingsServiceDumpProto::set_has_global_settings() {
  _has_bits_[0] |= 0x00000002u;
}
void SettingsServiceDumpProto::clear_has_global_settings() {
  _has_bits_[0] &= ~0x00000002u;
}
void SettingsServiceDumpProto::clear_global_settings() {
  if (global_settings_ != NULL) global_settings_->::android::providers::settings::GlobalSettingsProto::Clear();
  clear_has_global_settings();
}
const ::android::providers::settings::GlobalSettingsProto& SettingsServiceDumpProto::global_settings() const {
  // @@protoc_insertion_point(field_get:android.providers.settings.SettingsServiceDumpProto.global_settings)
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  return global_settings_ != NULL ? *global_settings_ : *default_instance().global_settings_;
#else
  return global_settings_ != NULL ? *global_settings_ : *default_instance_->global_settings_;
#endif
}
::android::providers::settings::GlobalSettingsProto* SettingsServiceDumpProto::mutable_global_settings() {
  set_has_global_settings();
  if (global_settings_ == NULL) {
    global_settings_ = new ::android::providers::settings::GlobalSettingsProto;
  }
  // @@protoc_insertion_point(field_mutable:android.providers.settings.SettingsServiceDumpProto.global_settings)
  return global_settings_;
}
::android::providers::settings::GlobalSettingsProto* SettingsServiceDumpProto::release_global_settings() {
  // @@protoc_insertion_point(field_release:android.providers.settings.SettingsServiceDumpProto.global_settings)
  clear_has_global_settings();
  ::android::providers::settings::GlobalSettingsProto* temp = global_settings_;
  global_settings_ = NULL;
  return temp;
}
void SettingsServiceDumpProto::set_allocated_global_settings(::android::providers::settings::GlobalSettingsProto* global_settings) {
  delete global_settings_;
  global_settings_ = global_settings;
  if (global_settings) {
    set_has_global_settings();
  } else {
    clear_has_global_settings();
  }
  // @@protoc_insertion_point(field_set_allocated:android.providers.settings.SettingsServiceDumpProto.global_settings)
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// ===================================================================

static ::std::string* MutableUnknownFieldsForUserSettingsProto(
    UserSettingsProto* ptr) {
  return ptr->mutable_unknown_fields();
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int UserSettingsProto::kUserIdFieldNumber;
const int UserSettingsProto::kSecureSettingsFieldNumber;
const int UserSettingsProto::kSystemSettingsFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

UserSettingsProto::UserSettingsProto()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.providers.settings.UserSettingsProto)
}

void UserSettingsProto::InitAsDefaultInstance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  secure_settings_ = const_cast< ::android::providers::settings::SecureSettingsProto*>(
      ::android::providers::settings::SecureSettingsProto::internal_default_instance());
#else
  secure_settings_ = const_cast< ::android::providers::settings::SecureSettingsProto*>(&::android::providers::settings::SecureSettingsProto::default_instance());
#endif
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  system_settings_ = const_cast< ::android::providers::settings::SystemSettingsProto*>(
      ::android::providers::settings::SystemSettingsProto::internal_default_instance());
#else
  system_settings_ = const_cast< ::android::providers::settings::SystemSettingsProto*>(&::android::providers::settings::SystemSettingsProto::default_instance());
#endif
}

UserSettingsProto::UserSettingsProto(const UserSettingsProto& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.providers.settings.UserSettingsProto)
}

void UserSettingsProto::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  user_id_ = 0;
  secure_settings_ = NULL;
  system_settings_ = NULL;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

UserSettingsProto::~UserSettingsProto() {
  // @@protoc_insertion_point(destructor:android.providers.settings.UserSettingsProto)
  SharedDtor();
}

void UserSettingsProto::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
    delete secure_settings_;
    delete system_settings_;
  }
}

void UserSettingsProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const UserSettingsProto& UserSettingsProto::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

UserSettingsProto* UserSettingsProto::default_instance_ = NULL;

UserSettingsProto* UserSettingsProto::New(::google::protobuf::Arena* arena) const {
  UserSettingsProto* n = new UserSettingsProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void UserSettingsProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.providers.settings.UserSettingsProto)
  if (_has_bits_[0 / 32] & 7u) {
    user_id_ = 0;
    if (has_secure_settings()) {
      if (secure_settings_ != NULL) secure_settings_->::android::providers::settings::SecureSettingsProto::Clear();
    }
    if (has_system_settings()) {
      if (system_settings_ != NULL) system_settings_->::android::providers::settings::SystemSettingsProto::Clear();
    }
  }
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool UserSettingsProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForUserSettingsProto, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.providers.settings.UserSettingsProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional int32 user_id = 1;
      case 1: {
        if (tag == 8) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &user_id_)));
          set_has_user_id();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(18)) goto parse_secure_settings;
        break;
      }

      // optional .android.providers.settings.SecureSettingsProto secure_settings = 2;
      case 2: {
        if (tag == 18) {
         parse_secure_settings:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtual(
               input, mutable_secure_settings()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(26)) goto parse_system_settings;
        break;
      }

      // optional .android.providers.settings.SystemSettingsProto system_settings = 3;
      case 3: {
        if (tag == 26) {
         parse_system_settings:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtual(
               input, mutable_system_settings()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.providers.settings.UserSettingsProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.providers.settings.UserSettingsProto)
  return false;
#undef DO_
}

void UserSettingsProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.providers.settings.UserSettingsProto)
  // optional int32 user_id = 1;
  if (has_user_id()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(1, this->user_id(), output);
  }

  // optional .android.providers.settings.SecureSettingsProto secure_settings = 2;
  if (has_secure_settings()) {
    ::google::protobuf::internal::WireFormatLite::WriteMessage(
      2, *this->secure_settings_, output);
  }

  // optional .android.providers.settings.SystemSettingsProto system_settings = 3;
  if (has_system_settings()) {
    ::google::protobuf::internal::WireFormatLite::WriteMessage(
      3, *this->system_settings_, output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.providers.settings.UserSettingsProto)
}

int UserSettingsProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.providers.settings.UserSettingsProto)
  int total_size = 0;

  if (_has_bits_[0 / 32] & 7u) {
    // optional int32 user_id = 1;
    if (has_user_id()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->user_id());
    }

    // optional .android.providers.settings.SecureSettingsProto secure_settings = 2;
    if (has_secure_settings()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
          *this->secure_settings_);
    }

    // optional .android.providers.settings.SystemSettingsProto system_settings = 3;
    if (has_system_settings()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
          *this->system_settings_);
    }

  }
  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void UserSettingsProto::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const UserSettingsProto*>(&from));
}

void UserSettingsProto::MergeFrom(const UserSettingsProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.providers.settings.UserSettingsProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_user_id()) {
      set_user_id(from.user_id());
    }
    if (from.has_secure_settings()) {
      mutable_secure_settings()->::android::providers::settings::SecureSettingsProto::MergeFrom(from.secure_settings());
    }
    if (from.has_system_settings()) {
      mutable_system_settings()->::android::providers::settings::SystemSettingsProto::MergeFrom(from.system_settings());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void UserSettingsProto::CopyFrom(const UserSettingsProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.providers.settings.UserSettingsProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool UserSettingsProto::IsInitialized() const {

  return true;
}

void UserSettingsProto::Swap(UserSettingsProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void UserSettingsProto::InternalSwap(UserSettingsProto* other) {
  std::swap(user_id_, other->user_id_);
  std::swap(secure_settings_, other->secure_settings_);
  std::swap(system_settings_, other->system_settings_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string UserSettingsProto::GetTypeName() const {
  return "android.providers.settings.UserSettingsProto";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// UserSettingsProto

// optional int32 user_id = 1;
bool UserSettingsProto::has_user_id() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
void UserSettingsProto::set_has_user_id() {
  _has_bits_[0] |= 0x00000001u;
}
void UserSettingsProto::clear_has_user_id() {
  _has_bits_[0] &= ~0x00000001u;
}
void UserSettingsProto::clear_user_id() {
  user_id_ = 0;
  clear_has_user_id();
}
 ::google::protobuf::int32 UserSettingsProto::user_id() const {
  // @@protoc_insertion_point(field_get:android.providers.settings.UserSettingsProto.user_id)
  return user_id_;
}
 void UserSettingsProto::set_user_id(::google::protobuf::int32 value) {
  set_has_user_id();
  user_id_ = value;
  // @@protoc_insertion_point(field_set:android.providers.settings.UserSettingsProto.user_id)
}

// optional .android.providers.settings.SecureSettingsProto secure_settings = 2;
bool UserSettingsProto::has_secure_settings() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void UserSettingsProto::set_has_secure_settings() {
  _has_bits_[0] |= 0x00000002u;
}
void UserSettingsProto::clear_has_secure_settings() {
  _has_bits_[0] &= ~0x00000002u;
}
void UserSettingsProto::clear_secure_settings() {
  if (secure_settings_ != NULL) secure_settings_->::android::providers::settings::SecureSettingsProto::Clear();
  clear_has_secure_settings();
}
const ::android::providers::settings::SecureSettingsProto& UserSettingsProto::secure_settings() const {
  // @@protoc_insertion_point(field_get:android.providers.settings.UserSettingsProto.secure_settings)
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  return secure_settings_ != NULL ? *secure_settings_ : *default_instance().secure_settings_;
#else
  return secure_settings_ != NULL ? *secure_settings_ : *default_instance_->secure_settings_;
#endif
}
::android::providers::settings::SecureSettingsProto* UserSettingsProto::mutable_secure_settings() {
  set_has_secure_settings();
  if (secure_settings_ == NULL) {
    secure_settings_ = new ::android::providers::settings::SecureSettingsProto;
  }
  // @@protoc_insertion_point(field_mutable:android.providers.settings.UserSettingsProto.secure_settings)
  return secure_settings_;
}
::android::providers::settings::SecureSettingsProto* UserSettingsProto::release_secure_settings() {
  // @@protoc_insertion_point(field_release:android.providers.settings.UserSettingsProto.secure_settings)
  clear_has_secure_settings();
  ::android::providers::settings::SecureSettingsProto* temp = secure_settings_;
  secure_settings_ = NULL;
  return temp;
}
void UserSettingsProto::set_allocated_secure_settings(::android::providers::settings::SecureSettingsProto* secure_settings) {
  delete secure_settings_;
  secure_settings_ = secure_settings;
  if (secure_settings) {
    set_has_secure_settings();
  } else {
    clear_has_secure_settings();
  }
  // @@protoc_insertion_point(field_set_allocated:android.providers.settings.UserSettingsProto.secure_settings)
}

// optional .android.providers.settings.SystemSettingsProto system_settings = 3;
bool UserSettingsProto::has_system_settings() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
void UserSettingsProto::set_has_system_settings() {
  _has_bits_[0] |= 0x00000004u;
}
void UserSettingsProto::clear_has_system_settings() {
  _has_bits_[0] &= ~0x00000004u;
}
void UserSettingsProto::clear_system_settings() {
  if (system_settings_ != NULL) system_settings_->::android::providers::settings::SystemSettingsProto::Clear();
  clear_has_system_settings();
}
const ::android::providers::settings::SystemSettingsProto& UserSettingsProto::system_settings() const {
  // @@protoc_insertion_point(field_get:android.providers.settings.UserSettingsProto.system_settings)
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  return system_settings_ != NULL ? *system_settings_ : *default_instance().system_settings_;
#else
  return system_settings_ != NULL ? *system_settings_ : *default_instance_->system_settings_;
#endif
}
::android::providers::settings::SystemSettingsProto* UserSettingsProto::mutable_system_settings() {
  set_has_system_settings();
  if (system_settings_ == NULL) {
    system_settings_ = new ::android::providers::settings::SystemSettingsProto;
  }
  // @@protoc_insertion_point(field_mutable:android.providers.settings.UserSettingsProto.system_settings)
  return system_settings_;
}
::android::providers::settings::SystemSettingsProto* UserSettingsProto::release_system_settings() {
  // @@protoc_insertion_point(field_release:android.providers.settings.UserSettingsProto.system_settings)
  clear_has_system_settings();
  ::android::providers::settings::SystemSettingsProto* temp = system_settings_;
  system_settings_ = NULL;
  return temp;
}
void UserSettingsProto::set_allocated_system_settings(::android::providers::settings::SystemSettingsProto* system_settings) {
  delete system_settings_;
  system_settings_ = system_settings;
  if (system_settings) {
    set_has_system_settings();
  } else {
    clear_has_system_settings();
  }
  // @@protoc_insertion_point(field_set_allocated:android.providers.settings.UserSettingsProto.system_settings)
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// ===================================================================

static ::std::string* MutableUnknownFieldsForSettingsProto(
    SettingsProto* ptr) {
  return ptr->mutable_unknown_fields();
}

bool SettingsProto_ScreenBrightnessMode_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const SettingsProto_ScreenBrightnessMode SettingsProto::SCREEN_BRIGHTNESS_MODE_MANUAL;
const SettingsProto_ScreenBrightnessMode SettingsProto::SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
const SettingsProto_ScreenBrightnessMode SettingsProto::ScreenBrightnessMode_MIN;
const SettingsProto_ScreenBrightnessMode SettingsProto::ScreenBrightnessMode_MAX;
const int SettingsProto::ScreenBrightnessMode_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
#if !defined(_MSC_VER) || _MSC_VER >= 1900
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

SettingsProto::SettingsProto()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.providers.settings.SettingsProto)
}

void SettingsProto::InitAsDefaultInstance() {
}

SettingsProto::SettingsProto(const SettingsProto& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.providers.settings.SettingsProto)
}

void SettingsProto::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

SettingsProto::~SettingsProto() {
  // @@protoc_insertion_point(destructor:android.providers.settings.SettingsProto)
  SharedDtor();
}

void SettingsProto::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void SettingsProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const SettingsProto& SettingsProto::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fproviders_2fsettings_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

SettingsProto* SettingsProto::default_instance_ = NULL;

SettingsProto* SettingsProto::New(::google::protobuf::Arena* arena) const {
  SettingsProto* n = new SettingsProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void SettingsProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.providers.settings.SettingsProto)
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool SettingsProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForSettingsProto, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.providers.settings.SettingsProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
  handle_unusual:
    if (tag == 0 ||
        ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
        ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
      goto success;
    }
    DO_(::google::protobuf::internal::WireFormatLite::SkipField(
        input, tag, &unknown_fields_stream));
  }
success:
  // @@protoc_insertion_point(parse_success:android.providers.settings.SettingsProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.providers.settings.SettingsProto)
  return false;
#undef DO_
}

void SettingsProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.providers.settings.SettingsProto)
  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.providers.settings.SettingsProto)
}

int SettingsProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.providers.settings.SettingsProto)
  int total_size = 0;

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void SettingsProto::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const SettingsProto*>(&from));
}

void SettingsProto::MergeFrom(const SettingsProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.providers.settings.SettingsProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void SettingsProto::CopyFrom(const SettingsProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.providers.settings.SettingsProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool SettingsProto::IsInitialized() const {

  return true;
}

void SettingsProto::Swap(SettingsProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void SettingsProto::InternalSwap(SettingsProto* other) {
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string SettingsProto::GetTypeName() const {
  return "android.providers.settings.SettingsProto";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// SettingsProto

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace settings
}  // namespace providers
}  // namespace android

// @@protoc_insertion_point(global_scope)
