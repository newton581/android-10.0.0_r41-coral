// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/graphics/rect.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include "frameworks/base/core/proto/android/privacy.pb.h"
// @@protoc_insertion_point(includes)

namespace android {
namespace graphics {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto();

class RectProto;

// ===================================================================

class RectProto : public ::google::protobuf::MessageLite {
 public:
  RectProto();
  virtual ~RectProto();

  RectProto(const RectProto& from);

  inline RectProto& operator=(const RectProto& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const RectProto& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const RectProto* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(RectProto* other);

  // implements Message ----------------------------------------------

  inline RectProto* New() const { return New(NULL); }

  RectProto* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const RectProto& from);
  void MergeFrom(const RectProto& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(RectProto* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional int32 left = 1;
  bool has_left() const;
  void clear_left();
  static const int kLeftFieldNumber = 1;
  ::google::protobuf::int32 left() const;
  void set_left(::google::protobuf::int32 value);

  // optional int32 top = 2;
  bool has_top() const;
  void clear_top();
  static const int kTopFieldNumber = 2;
  ::google::protobuf::int32 top() const;
  void set_top(::google::protobuf::int32 value);

  // optional int32 right = 3;
  bool has_right() const;
  void clear_right();
  static const int kRightFieldNumber = 3;
  ::google::protobuf::int32 right() const;
  void set_right(::google::protobuf::int32 value);

  // optional int32 bottom = 4;
  bool has_bottom() const;
  void clear_bottom();
  static const int kBottomFieldNumber = 4;
  ::google::protobuf::int32 bottom() const;
  void set_bottom(::google::protobuf::int32 value);

  // @@protoc_insertion_point(class_scope:android.graphics.RectProto)
 private:
  inline void set_has_left();
  inline void clear_has_left();
  inline void set_has_top();
  inline void clear_has_top();
  inline void set_has_right();
  inline void clear_has_right();
  inline void set_has_bottom();
  inline void clear_has_bottom();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::int32 left_;
  ::google::protobuf::int32 top_;
  ::google::protobuf::int32 right_;
  ::google::protobuf::int32 bottom_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto();
  #endif
  friend void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto();
  friend void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto();

  void InitAsDefaultInstance();
  static RectProto* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// RectProto

// optional int32 left = 1;
inline bool RectProto::has_left() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void RectProto::set_has_left() {
  _has_bits_[0] |= 0x00000001u;
}
inline void RectProto::clear_has_left() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void RectProto::clear_left() {
  left_ = 0;
  clear_has_left();
}
inline ::google::protobuf::int32 RectProto::left() const {
  // @@protoc_insertion_point(field_get:android.graphics.RectProto.left)
  return left_;
}
inline void RectProto::set_left(::google::protobuf::int32 value) {
  set_has_left();
  left_ = value;
  // @@protoc_insertion_point(field_set:android.graphics.RectProto.left)
}

// optional int32 top = 2;
inline bool RectProto::has_top() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void RectProto::set_has_top() {
  _has_bits_[0] |= 0x00000002u;
}
inline void RectProto::clear_has_top() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void RectProto::clear_top() {
  top_ = 0;
  clear_has_top();
}
inline ::google::protobuf::int32 RectProto::top() const {
  // @@protoc_insertion_point(field_get:android.graphics.RectProto.top)
  return top_;
}
inline void RectProto::set_top(::google::protobuf::int32 value) {
  set_has_top();
  top_ = value;
  // @@protoc_insertion_point(field_set:android.graphics.RectProto.top)
}

// optional int32 right = 3;
inline bool RectProto::has_right() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void RectProto::set_has_right() {
  _has_bits_[0] |= 0x00000004u;
}
inline void RectProto::clear_has_right() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void RectProto::clear_right() {
  right_ = 0;
  clear_has_right();
}
inline ::google::protobuf::int32 RectProto::right() const {
  // @@protoc_insertion_point(field_get:android.graphics.RectProto.right)
  return right_;
}
inline void RectProto::set_right(::google::protobuf::int32 value) {
  set_has_right();
  right_ = value;
  // @@protoc_insertion_point(field_set:android.graphics.RectProto.right)
}

// optional int32 bottom = 4;
inline bool RectProto::has_bottom() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void RectProto::set_has_bottom() {
  _has_bits_[0] |= 0x00000008u;
}
inline void RectProto::clear_has_bottom() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void RectProto::clear_bottom() {
  bottom_ = 0;
  clear_has_bottom();
}
inline ::google::protobuf::int32 RectProto::bottom() const {
  // @@protoc_insertion_point(field_get:android.graphics.RectProto.bottom)
  return bottom_;
}
inline void RectProto::set_bottom(::google::protobuf::int32 value) {
  set_has_bottom();
  bottom_ = value;
  // @@protoc_insertion_point(field_set:android.graphics.RectProto.bottom)
}

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace graphics
}  // namespace android

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto__INCLUDED
