// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/stats/connectivity/network_stack.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/stats/connectivity/network_stack.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace stats {
namespace connectivity {

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto() {
  delete NetworkStackEventData::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  NetworkStackEventData::default_instance_ = new NetworkStackEventData();
  NetworkStackEventData::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto_once_);
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto_once_,
                 &protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForNetworkStackEventData(
    NetworkStackEventData* ptr) {
  return ptr->mutable_unknown_fields();
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

NetworkStackEventData::NetworkStackEventData()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.stats.connectivity.NetworkStackEventData)
}

void NetworkStackEventData::InitAsDefaultInstance() {
}

NetworkStackEventData::NetworkStackEventData(const NetworkStackEventData& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.stats.connectivity.NetworkStackEventData)
}

void NetworkStackEventData::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

NetworkStackEventData::~NetworkStackEventData() {
  // @@protoc_insertion_point(destructor:android.stats.connectivity.NetworkStackEventData)
  SharedDtor();
}

void NetworkStackEventData::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void NetworkStackEventData::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const NetworkStackEventData& NetworkStackEventData::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fconnectivity_2fnetwork_5fstack_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

NetworkStackEventData* NetworkStackEventData::default_instance_ = NULL;

NetworkStackEventData* NetworkStackEventData::New(::google::protobuf::Arena* arena) const {
  NetworkStackEventData* n = new NetworkStackEventData;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void NetworkStackEventData::Clear() {
// @@protoc_insertion_point(message_clear_start:android.stats.connectivity.NetworkStackEventData)
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool NetworkStackEventData::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForNetworkStackEventData, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.stats.connectivity.NetworkStackEventData)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
  handle_unusual:
    if (tag == 0 ||
        ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
        ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
      goto success;
    }
    DO_(::google::protobuf::internal::WireFormatLite::SkipField(
        input, tag, &unknown_fields_stream));
  }
success:
  // @@protoc_insertion_point(parse_success:android.stats.connectivity.NetworkStackEventData)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.stats.connectivity.NetworkStackEventData)
  return false;
#undef DO_
}

void NetworkStackEventData::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.stats.connectivity.NetworkStackEventData)
  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.stats.connectivity.NetworkStackEventData)
}

int NetworkStackEventData::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.stats.connectivity.NetworkStackEventData)
  int total_size = 0;

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void NetworkStackEventData::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const NetworkStackEventData*>(&from));
}

void NetworkStackEventData::MergeFrom(const NetworkStackEventData& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.stats.connectivity.NetworkStackEventData)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void NetworkStackEventData::CopyFrom(const NetworkStackEventData& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.stats.connectivity.NetworkStackEventData)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool NetworkStackEventData::IsInitialized() const {

  return true;
}

void NetworkStackEventData::Swap(NetworkStackEventData* other) {
  if (other == this) return;
  InternalSwap(other);
}
void NetworkStackEventData::InternalSwap(NetworkStackEventData* other) {
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string NetworkStackEventData::GetTypeName() const {
  return "android.stats.connectivity.NetworkStackEventData";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// NetworkStackEventData

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace connectivity
}  // namespace stats
}  // namespace android

// @@protoc_insertion_point(global_scope)
