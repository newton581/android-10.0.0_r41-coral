// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/os/powermanager.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/os/powermanager.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace os {

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto() {
  delete PowerManagerProto::default_instance_;
  delete PowerManagerProto_WakeLock::default_instance_;
  delete PowerManagerInternalProto::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ::android::os::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fworksource_2eproto();
  ::android::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
  PowerManagerProto::default_instance_ = new PowerManagerProto();
  PowerManagerProto_WakeLock::default_instance_ = new PowerManagerProto_WakeLock();
  PowerManagerInternalProto::default_instance_ = new PowerManagerInternalProto();
  PowerManagerProto::default_instance_->InitAsDefaultInstance();
  PowerManagerProto_WakeLock::default_instance_->InitAsDefaultInstance();
  PowerManagerInternalProto::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto_once_);
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto_once_,
                 &protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForPowerManagerProto(
    PowerManagerProto* ptr) {
  return ptr->mutable_unknown_fields();
}

bool PowerManagerProto_UserActivityEvent_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const PowerManagerProto_UserActivityEvent PowerManagerProto::USER_ACTIVITY_EVENT_OTHER;
const PowerManagerProto_UserActivityEvent PowerManagerProto::USER_ACTIVITY_EVENT_BUTTON;
const PowerManagerProto_UserActivityEvent PowerManagerProto::USER_ACTIVITY_EVENT_TOUCH;
const PowerManagerProto_UserActivityEvent PowerManagerProto::USER_ACTIVITY_EVENT_ACCESSIBILITY;
const PowerManagerProto_UserActivityEvent PowerManagerProto::UserActivityEvent_MIN;
const PowerManagerProto_UserActivityEvent PowerManagerProto::UserActivityEvent_MAX;
const int PowerManagerProto::UserActivityEvent_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
static ::std::string* MutableUnknownFieldsForPowerManagerProto_WakeLock(
    PowerManagerProto_WakeLock* ptr) {
  return ptr->mutable_unknown_fields();
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int PowerManagerProto_WakeLock::kTagFieldNumber;
const int PowerManagerProto_WakeLock::kPackageNameFieldNumber;
const int PowerManagerProto_WakeLock::kHeldFieldNumber;
const int PowerManagerProto_WakeLock::kInternalCountFieldNumber;
const int PowerManagerProto_WakeLock::kWorkSourceFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

PowerManagerProto_WakeLock::PowerManagerProto_WakeLock()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.os.PowerManagerProto.WakeLock)
}

void PowerManagerProto_WakeLock::InitAsDefaultInstance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  work_source_ = const_cast< ::android::os::WorkSourceProto*>(
      ::android::os::WorkSourceProto::internal_default_instance());
#else
  work_source_ = const_cast< ::android::os::WorkSourceProto*>(&::android::os::WorkSourceProto::default_instance());
#endif
}

PowerManagerProto_WakeLock::PowerManagerProto_WakeLock(const PowerManagerProto_WakeLock& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.os.PowerManagerProto.WakeLock)
}

void PowerManagerProto_WakeLock::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  tag_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  package_name_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  held_ = false;
  internal_count_ = 0;
  work_source_ = NULL;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

PowerManagerProto_WakeLock::~PowerManagerProto_WakeLock() {
  // @@protoc_insertion_point(destructor:android.os.PowerManagerProto.WakeLock)
  SharedDtor();
}

void PowerManagerProto_WakeLock::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  tag_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  package_name_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
    delete work_source_;
  }
}

void PowerManagerProto_WakeLock::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const PowerManagerProto_WakeLock& PowerManagerProto_WakeLock::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

PowerManagerProto_WakeLock* PowerManagerProto_WakeLock::default_instance_ = NULL;

PowerManagerProto_WakeLock* PowerManagerProto_WakeLock::New(::google::protobuf::Arena* arena) const {
  PowerManagerProto_WakeLock* n = new PowerManagerProto_WakeLock;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void PowerManagerProto_WakeLock::Clear() {
// @@protoc_insertion_point(message_clear_start:android.os.PowerManagerProto.WakeLock)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(PowerManagerProto_WakeLock, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<PowerManagerProto_WakeLock*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  if (_has_bits_[0 / 32] & 31u) {
    ZR_(held_, internal_count_);
    if (has_tag()) {
      tag_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    }
    if (has_package_name()) {
      package_name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    }
    if (has_work_source()) {
      if (work_source_ != NULL) work_source_->::android::os::WorkSourceProto::Clear();
    }
  }

#undef ZR_HELPER_
#undef ZR_

  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool PowerManagerProto_WakeLock::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForPowerManagerProto_WakeLock, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.os.PowerManagerProto.WakeLock)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional string tag = 1;
      case 1: {
        if (tag == 10) {
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_tag()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(18)) goto parse_package_name;
        break;
      }

      // optional string package_name = 2;
      case 2: {
        if (tag == 18) {
         parse_package_name:
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_package_name()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(24)) goto parse_held;
        break;
      }

      // optional bool held = 3;
      case 3: {
        if (tag == 24) {
         parse_held:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   bool, ::google::protobuf::internal::WireFormatLite::TYPE_BOOL>(
                 input, &held_)));
          set_has_held();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(32)) goto parse_internal_count;
        break;
      }

      // optional int32 internal_count = 4;
      case 4: {
        if (tag == 32) {
         parse_internal_count:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &internal_count_)));
          set_has_internal_count();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(42)) goto parse_work_source;
        break;
      }

      // optional .android.os.WorkSourceProto work_source = 5;
      case 5: {
        if (tag == 42) {
         parse_work_source:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtual(
               input, mutable_work_source()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.os.PowerManagerProto.WakeLock)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.os.PowerManagerProto.WakeLock)
  return false;
#undef DO_
}

void PowerManagerProto_WakeLock::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.os.PowerManagerProto.WakeLock)
  // optional string tag = 1;
  if (has_tag()) {
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      1, this->tag(), output);
  }

  // optional string package_name = 2;
  if (has_package_name()) {
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      2, this->package_name(), output);
  }

  // optional bool held = 3;
  if (has_held()) {
    ::google::protobuf::internal::WireFormatLite::WriteBool(3, this->held(), output);
  }

  // optional int32 internal_count = 4;
  if (has_internal_count()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(4, this->internal_count(), output);
  }

  // optional .android.os.WorkSourceProto work_source = 5;
  if (has_work_source()) {
    ::google::protobuf::internal::WireFormatLite::WriteMessage(
      5, *this->work_source_, output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.os.PowerManagerProto.WakeLock)
}

int PowerManagerProto_WakeLock::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.os.PowerManagerProto.WakeLock)
  int total_size = 0;

  if (_has_bits_[0 / 32] & 31u) {
    // optional string tag = 1;
    if (has_tag()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::StringSize(
          this->tag());
    }

    // optional string package_name = 2;
    if (has_package_name()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::StringSize(
          this->package_name());
    }

    // optional bool held = 3;
    if (has_held()) {
      total_size += 1 + 1;
    }

    // optional int32 internal_count = 4;
    if (has_internal_count()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->internal_count());
    }

    // optional .android.os.WorkSourceProto work_source = 5;
    if (has_work_source()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
          *this->work_source_);
    }

  }
  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void PowerManagerProto_WakeLock::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const PowerManagerProto_WakeLock*>(&from));
}

void PowerManagerProto_WakeLock::MergeFrom(const PowerManagerProto_WakeLock& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.os.PowerManagerProto.WakeLock)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_tag()) {
      set_has_tag();
      tag_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.tag_);
    }
    if (from.has_package_name()) {
      set_has_package_name();
      package_name_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.package_name_);
    }
    if (from.has_held()) {
      set_held(from.held());
    }
    if (from.has_internal_count()) {
      set_internal_count(from.internal_count());
    }
    if (from.has_work_source()) {
      mutable_work_source()->::android::os::WorkSourceProto::MergeFrom(from.work_source());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void PowerManagerProto_WakeLock::CopyFrom(const PowerManagerProto_WakeLock& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.os.PowerManagerProto.WakeLock)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool PowerManagerProto_WakeLock::IsInitialized() const {

  return true;
}

void PowerManagerProto_WakeLock::Swap(PowerManagerProto_WakeLock* other) {
  if (other == this) return;
  InternalSwap(other);
}
void PowerManagerProto_WakeLock::InternalSwap(PowerManagerProto_WakeLock* other) {
  tag_.Swap(&other->tag_);
  package_name_.Swap(&other->package_name_);
  std::swap(held_, other->held_);
  std::swap(internal_count_, other->internal_count_);
  std::swap(work_source_, other->work_source_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string PowerManagerProto_WakeLock::GetTypeName() const {
  return "android.os.PowerManagerProto.WakeLock";
}


// -------------------------------------------------------------------

#if !defined(_MSC_VER) || _MSC_VER >= 1900
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

PowerManagerProto::PowerManagerProto()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.os.PowerManagerProto)
}

void PowerManagerProto::InitAsDefaultInstance() {
}

PowerManagerProto::PowerManagerProto(const PowerManagerProto& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.os.PowerManagerProto)
}

void PowerManagerProto::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

PowerManagerProto::~PowerManagerProto() {
  // @@protoc_insertion_point(destructor:android.os.PowerManagerProto)
  SharedDtor();
}

void PowerManagerProto::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void PowerManagerProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const PowerManagerProto& PowerManagerProto::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

PowerManagerProto* PowerManagerProto::default_instance_ = NULL;

PowerManagerProto* PowerManagerProto::New(::google::protobuf::Arena* arena) const {
  PowerManagerProto* n = new PowerManagerProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void PowerManagerProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.os.PowerManagerProto)
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool PowerManagerProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForPowerManagerProto, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.os.PowerManagerProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
  handle_unusual:
    if (tag == 0 ||
        ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
        ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
      goto success;
    }
    DO_(::google::protobuf::internal::WireFormatLite::SkipField(
        input, tag, &unknown_fields_stream));
  }
success:
  // @@protoc_insertion_point(parse_success:android.os.PowerManagerProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.os.PowerManagerProto)
  return false;
#undef DO_
}

void PowerManagerProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.os.PowerManagerProto)
  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.os.PowerManagerProto)
}

int PowerManagerProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.os.PowerManagerProto)
  int total_size = 0;

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void PowerManagerProto::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const PowerManagerProto*>(&from));
}

void PowerManagerProto::MergeFrom(const PowerManagerProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.os.PowerManagerProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void PowerManagerProto::CopyFrom(const PowerManagerProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.os.PowerManagerProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool PowerManagerProto::IsInitialized() const {

  return true;
}

void PowerManagerProto::Swap(PowerManagerProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void PowerManagerProto::InternalSwap(PowerManagerProto* other) {
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string PowerManagerProto::GetTypeName() const {
  return "android.os.PowerManagerProto";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// PowerManagerProto_WakeLock

// optional string tag = 1;
bool PowerManagerProto_WakeLock::has_tag() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
void PowerManagerProto_WakeLock::set_has_tag() {
  _has_bits_[0] |= 0x00000001u;
}
void PowerManagerProto_WakeLock::clear_has_tag() {
  _has_bits_[0] &= ~0x00000001u;
}
void PowerManagerProto_WakeLock::clear_tag() {
  tag_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_tag();
}
 const ::std::string& PowerManagerProto_WakeLock::tag() const {
  // @@protoc_insertion_point(field_get:android.os.PowerManagerProto.WakeLock.tag)
  return tag_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void PowerManagerProto_WakeLock::set_tag(const ::std::string& value) {
  set_has_tag();
  tag_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:android.os.PowerManagerProto.WakeLock.tag)
}
 void PowerManagerProto_WakeLock::set_tag(const char* value) {
  set_has_tag();
  tag_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:android.os.PowerManagerProto.WakeLock.tag)
}
 void PowerManagerProto_WakeLock::set_tag(const char* value, size_t size) {
  set_has_tag();
  tag_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:android.os.PowerManagerProto.WakeLock.tag)
}
 ::std::string* PowerManagerProto_WakeLock::mutable_tag() {
  set_has_tag();
  // @@protoc_insertion_point(field_mutable:android.os.PowerManagerProto.WakeLock.tag)
  return tag_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 ::std::string* PowerManagerProto_WakeLock::release_tag() {
  // @@protoc_insertion_point(field_release:android.os.PowerManagerProto.WakeLock.tag)
  clear_has_tag();
  return tag_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void PowerManagerProto_WakeLock::set_allocated_tag(::std::string* tag) {
  if (tag != NULL) {
    set_has_tag();
  } else {
    clear_has_tag();
  }
  tag_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), tag);
  // @@protoc_insertion_point(field_set_allocated:android.os.PowerManagerProto.WakeLock.tag)
}

// optional string package_name = 2;
bool PowerManagerProto_WakeLock::has_package_name() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void PowerManagerProto_WakeLock::set_has_package_name() {
  _has_bits_[0] |= 0x00000002u;
}
void PowerManagerProto_WakeLock::clear_has_package_name() {
  _has_bits_[0] &= ~0x00000002u;
}
void PowerManagerProto_WakeLock::clear_package_name() {
  package_name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_package_name();
}
 const ::std::string& PowerManagerProto_WakeLock::package_name() const {
  // @@protoc_insertion_point(field_get:android.os.PowerManagerProto.WakeLock.package_name)
  return package_name_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void PowerManagerProto_WakeLock::set_package_name(const ::std::string& value) {
  set_has_package_name();
  package_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:android.os.PowerManagerProto.WakeLock.package_name)
}
 void PowerManagerProto_WakeLock::set_package_name(const char* value) {
  set_has_package_name();
  package_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:android.os.PowerManagerProto.WakeLock.package_name)
}
 void PowerManagerProto_WakeLock::set_package_name(const char* value, size_t size) {
  set_has_package_name();
  package_name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:android.os.PowerManagerProto.WakeLock.package_name)
}
 ::std::string* PowerManagerProto_WakeLock::mutable_package_name() {
  set_has_package_name();
  // @@protoc_insertion_point(field_mutable:android.os.PowerManagerProto.WakeLock.package_name)
  return package_name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 ::std::string* PowerManagerProto_WakeLock::release_package_name() {
  // @@protoc_insertion_point(field_release:android.os.PowerManagerProto.WakeLock.package_name)
  clear_has_package_name();
  return package_name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void PowerManagerProto_WakeLock::set_allocated_package_name(::std::string* package_name) {
  if (package_name != NULL) {
    set_has_package_name();
  } else {
    clear_has_package_name();
  }
  package_name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), package_name);
  // @@protoc_insertion_point(field_set_allocated:android.os.PowerManagerProto.WakeLock.package_name)
}

// optional bool held = 3;
bool PowerManagerProto_WakeLock::has_held() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
void PowerManagerProto_WakeLock::set_has_held() {
  _has_bits_[0] |= 0x00000004u;
}
void PowerManagerProto_WakeLock::clear_has_held() {
  _has_bits_[0] &= ~0x00000004u;
}
void PowerManagerProto_WakeLock::clear_held() {
  held_ = false;
  clear_has_held();
}
 bool PowerManagerProto_WakeLock::held() const {
  // @@protoc_insertion_point(field_get:android.os.PowerManagerProto.WakeLock.held)
  return held_;
}
 void PowerManagerProto_WakeLock::set_held(bool value) {
  set_has_held();
  held_ = value;
  // @@protoc_insertion_point(field_set:android.os.PowerManagerProto.WakeLock.held)
}

// optional int32 internal_count = 4;
bool PowerManagerProto_WakeLock::has_internal_count() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
void PowerManagerProto_WakeLock::set_has_internal_count() {
  _has_bits_[0] |= 0x00000008u;
}
void PowerManagerProto_WakeLock::clear_has_internal_count() {
  _has_bits_[0] &= ~0x00000008u;
}
void PowerManagerProto_WakeLock::clear_internal_count() {
  internal_count_ = 0;
  clear_has_internal_count();
}
 ::google::protobuf::int32 PowerManagerProto_WakeLock::internal_count() const {
  // @@protoc_insertion_point(field_get:android.os.PowerManagerProto.WakeLock.internal_count)
  return internal_count_;
}
 void PowerManagerProto_WakeLock::set_internal_count(::google::protobuf::int32 value) {
  set_has_internal_count();
  internal_count_ = value;
  // @@protoc_insertion_point(field_set:android.os.PowerManagerProto.WakeLock.internal_count)
}

// optional .android.os.WorkSourceProto work_source = 5;
bool PowerManagerProto_WakeLock::has_work_source() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
void PowerManagerProto_WakeLock::set_has_work_source() {
  _has_bits_[0] |= 0x00000010u;
}
void PowerManagerProto_WakeLock::clear_has_work_source() {
  _has_bits_[0] &= ~0x00000010u;
}
void PowerManagerProto_WakeLock::clear_work_source() {
  if (work_source_ != NULL) work_source_->::android::os::WorkSourceProto::Clear();
  clear_has_work_source();
}
const ::android::os::WorkSourceProto& PowerManagerProto_WakeLock::work_source() const {
  // @@protoc_insertion_point(field_get:android.os.PowerManagerProto.WakeLock.work_source)
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  return work_source_ != NULL ? *work_source_ : *default_instance().work_source_;
#else
  return work_source_ != NULL ? *work_source_ : *default_instance_->work_source_;
#endif
}
::android::os::WorkSourceProto* PowerManagerProto_WakeLock::mutable_work_source() {
  set_has_work_source();
  if (work_source_ == NULL) {
    work_source_ = new ::android::os::WorkSourceProto;
  }
  // @@protoc_insertion_point(field_mutable:android.os.PowerManagerProto.WakeLock.work_source)
  return work_source_;
}
::android::os::WorkSourceProto* PowerManagerProto_WakeLock::release_work_source() {
  // @@protoc_insertion_point(field_release:android.os.PowerManagerProto.WakeLock.work_source)
  clear_has_work_source();
  ::android::os::WorkSourceProto* temp = work_source_;
  work_source_ = NULL;
  return temp;
}
void PowerManagerProto_WakeLock::set_allocated_work_source(::android::os::WorkSourceProto* work_source) {
  delete work_source_;
  work_source_ = work_source;
  if (work_source) {
    set_has_work_source();
  } else {
    clear_has_work_source();
  }
  // @@protoc_insertion_point(field_set_allocated:android.os.PowerManagerProto.WakeLock.work_source)
}

// -------------------------------------------------------------------

// PowerManagerProto

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// ===================================================================

static ::std::string* MutableUnknownFieldsForPowerManagerInternalProto(
    PowerManagerInternalProto* ptr) {
  return ptr->mutable_unknown_fields();
}

bool PowerManagerInternalProto_Wakefulness_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const PowerManagerInternalProto_Wakefulness PowerManagerInternalProto::WAKEFULNESS_ASLEEP;
const PowerManagerInternalProto_Wakefulness PowerManagerInternalProto::WAKEFULNESS_AWAKE;
const PowerManagerInternalProto_Wakefulness PowerManagerInternalProto::WAKEFULNESS_DREAMING;
const PowerManagerInternalProto_Wakefulness PowerManagerInternalProto::WAKEFULNESS_DOZING;
const PowerManagerInternalProto_Wakefulness PowerManagerInternalProto::Wakefulness_MIN;
const PowerManagerInternalProto_Wakefulness PowerManagerInternalProto::Wakefulness_MAX;
const int PowerManagerInternalProto::Wakefulness_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
#if !defined(_MSC_VER) || _MSC_VER >= 1900
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

PowerManagerInternalProto::PowerManagerInternalProto()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.os.PowerManagerInternalProto)
}

void PowerManagerInternalProto::InitAsDefaultInstance() {
}

PowerManagerInternalProto::PowerManagerInternalProto(const PowerManagerInternalProto& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.os.PowerManagerInternalProto)
}

void PowerManagerInternalProto::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

PowerManagerInternalProto::~PowerManagerInternalProto() {
  // @@protoc_insertion_point(destructor:android.os.PowerManagerInternalProto)
  SharedDtor();
}

void PowerManagerInternalProto::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void PowerManagerInternalProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const PowerManagerInternalProto& PowerManagerInternalProto::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fpowermanager_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

PowerManagerInternalProto* PowerManagerInternalProto::default_instance_ = NULL;

PowerManagerInternalProto* PowerManagerInternalProto::New(::google::protobuf::Arena* arena) const {
  PowerManagerInternalProto* n = new PowerManagerInternalProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void PowerManagerInternalProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.os.PowerManagerInternalProto)
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool PowerManagerInternalProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForPowerManagerInternalProto, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.os.PowerManagerInternalProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
  handle_unusual:
    if (tag == 0 ||
        ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
        ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
      goto success;
    }
    DO_(::google::protobuf::internal::WireFormatLite::SkipField(
        input, tag, &unknown_fields_stream));
  }
success:
  // @@protoc_insertion_point(parse_success:android.os.PowerManagerInternalProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.os.PowerManagerInternalProto)
  return false;
#undef DO_
}

void PowerManagerInternalProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.os.PowerManagerInternalProto)
  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.os.PowerManagerInternalProto)
}

int PowerManagerInternalProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.os.PowerManagerInternalProto)
  int total_size = 0;

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void PowerManagerInternalProto::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const PowerManagerInternalProto*>(&from));
}

void PowerManagerInternalProto::MergeFrom(const PowerManagerInternalProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.os.PowerManagerInternalProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void PowerManagerInternalProto::CopyFrom(const PowerManagerInternalProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.os.PowerManagerInternalProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool PowerManagerInternalProto::IsInitialized() const {

  return true;
}

void PowerManagerInternalProto::Swap(PowerManagerInternalProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void PowerManagerInternalProto::InternalSwap(PowerManagerInternalProto* other) {
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string PowerManagerInternalProto::GetTypeName() const {
  return "android.os.PowerManagerInternalProto";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// PowerManagerInternalProto

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace os
}  // namespace android

// @@protoc_insertion_point(global_scope)
