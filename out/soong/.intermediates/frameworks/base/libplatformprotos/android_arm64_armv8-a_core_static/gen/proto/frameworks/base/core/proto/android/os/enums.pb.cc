// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/os/enums.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/os/enums.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace os {

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto() {
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto_once_);
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto_once_,
                 &protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fenums_2eproto_;
#endif
bool BatteryHealthEnum_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
      return true;
    default:
      return false;
  }
}

bool BatteryPluggedStateEnum_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 4:
      return true;
    default:
      return false;
  }
}

bool BatteryStatusEnum_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      return true;
    default:
      return false;
  }
}

bool TemperatureTypeEnum_IsValid(int value) {
  switch(value) {
    case -1:
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      return true;
    default:
      return false;
  }
}

bool ThrottlingSeverityEnum_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      return true;
    default:
      return false;
  }
}

bool CoolingTypeEnum_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      return true;
    default:
      return false;
  }
}

bool WakeLockLevelEnum_IsValid(int value) {
  switch(value) {
    case 1:
    case 6:
    case 10:
    case 26:
    case 32:
    case 64:
    case 128:
      return true;
    default:
      return false;
  }
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace os
}  // namespace android

// @@protoc_insertion_point(global_scope)
