// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/graphics/rect.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/graphics/rect.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace graphics {

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto() {
  delete RectProto::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ::android::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
  RectProto::default_instance_ = new RectProto();
  RectProto::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto_once_);
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto_once_,
                 &protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForRectProto(
    RectProto* ptr) {
  return ptr->mutable_unknown_fields();
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int RectProto::kLeftFieldNumber;
const int RectProto::kTopFieldNumber;
const int RectProto::kRightFieldNumber;
const int RectProto::kBottomFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

RectProto::RectProto()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.graphics.RectProto)
}

void RectProto::InitAsDefaultInstance() {
}

RectProto::RectProto(const RectProto& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.graphics.RectProto)
}

void RectProto::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  left_ = 0;
  top_ = 0;
  right_ = 0;
  bottom_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

RectProto::~RectProto() {
  // @@protoc_insertion_point(destructor:android.graphics.RectProto)
  SharedDtor();
}

void RectProto::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void RectProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const RectProto& RectProto::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fgraphics_2frect_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

RectProto* RectProto::default_instance_ = NULL;

RectProto* RectProto::New(::google::protobuf::Arena* arena) const {
  RectProto* n = new RectProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void RectProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.graphics.RectProto)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(RectProto, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<RectProto*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  ZR_(left_, bottom_);

#undef ZR_HELPER_
#undef ZR_

  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool RectProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForRectProto, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.graphics.RectProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional int32 left = 1;
      case 1: {
        if (tag == 8) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &left_)));
          set_has_left();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(16)) goto parse_top;
        break;
      }

      // optional int32 top = 2;
      case 2: {
        if (tag == 16) {
         parse_top:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &top_)));
          set_has_top();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(24)) goto parse_right;
        break;
      }

      // optional int32 right = 3;
      case 3: {
        if (tag == 24) {
         parse_right:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &right_)));
          set_has_right();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(32)) goto parse_bottom;
        break;
      }

      // optional int32 bottom = 4;
      case 4: {
        if (tag == 32) {
         parse_bottom:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &bottom_)));
          set_has_bottom();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.graphics.RectProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.graphics.RectProto)
  return false;
#undef DO_
}

void RectProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.graphics.RectProto)
  // optional int32 left = 1;
  if (has_left()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(1, this->left(), output);
  }

  // optional int32 top = 2;
  if (has_top()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(2, this->top(), output);
  }

  // optional int32 right = 3;
  if (has_right()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(3, this->right(), output);
  }

  // optional int32 bottom = 4;
  if (has_bottom()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(4, this->bottom(), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.graphics.RectProto)
}

int RectProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.graphics.RectProto)
  int total_size = 0;

  if (_has_bits_[0 / 32] & 15u) {
    // optional int32 left = 1;
    if (has_left()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->left());
    }

    // optional int32 top = 2;
    if (has_top()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->top());
    }

    // optional int32 right = 3;
    if (has_right()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->right());
    }

    // optional int32 bottom = 4;
    if (has_bottom()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->bottom());
    }

  }
  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void RectProto::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const RectProto*>(&from));
}

void RectProto::MergeFrom(const RectProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.graphics.RectProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_left()) {
      set_left(from.left());
    }
    if (from.has_top()) {
      set_top(from.top());
    }
    if (from.has_right()) {
      set_right(from.right());
    }
    if (from.has_bottom()) {
      set_bottom(from.bottom());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void RectProto::CopyFrom(const RectProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.graphics.RectProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool RectProto::IsInitialized() const {

  return true;
}

void RectProto::Swap(RectProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void RectProto::InternalSwap(RectProto* other) {
  std::swap(left_, other->left_);
  std::swap(top_, other->top_);
  std::swap(right_, other->right_);
  std::swap(bottom_, other->bottom_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string RectProto::GetTypeName() const {
  return "android.graphics.RectProto";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// RectProto

// optional int32 left = 1;
bool RectProto::has_left() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
void RectProto::set_has_left() {
  _has_bits_[0] |= 0x00000001u;
}
void RectProto::clear_has_left() {
  _has_bits_[0] &= ~0x00000001u;
}
void RectProto::clear_left() {
  left_ = 0;
  clear_has_left();
}
 ::google::protobuf::int32 RectProto::left() const {
  // @@protoc_insertion_point(field_get:android.graphics.RectProto.left)
  return left_;
}
 void RectProto::set_left(::google::protobuf::int32 value) {
  set_has_left();
  left_ = value;
  // @@protoc_insertion_point(field_set:android.graphics.RectProto.left)
}

// optional int32 top = 2;
bool RectProto::has_top() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void RectProto::set_has_top() {
  _has_bits_[0] |= 0x00000002u;
}
void RectProto::clear_has_top() {
  _has_bits_[0] &= ~0x00000002u;
}
void RectProto::clear_top() {
  top_ = 0;
  clear_has_top();
}
 ::google::protobuf::int32 RectProto::top() const {
  // @@protoc_insertion_point(field_get:android.graphics.RectProto.top)
  return top_;
}
 void RectProto::set_top(::google::protobuf::int32 value) {
  set_has_top();
  top_ = value;
  // @@protoc_insertion_point(field_set:android.graphics.RectProto.top)
}

// optional int32 right = 3;
bool RectProto::has_right() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
void RectProto::set_has_right() {
  _has_bits_[0] |= 0x00000004u;
}
void RectProto::clear_has_right() {
  _has_bits_[0] &= ~0x00000004u;
}
void RectProto::clear_right() {
  right_ = 0;
  clear_has_right();
}
 ::google::protobuf::int32 RectProto::right() const {
  // @@protoc_insertion_point(field_get:android.graphics.RectProto.right)
  return right_;
}
 void RectProto::set_right(::google::protobuf::int32 value) {
  set_has_right();
  right_ = value;
  // @@protoc_insertion_point(field_set:android.graphics.RectProto.right)
}

// optional int32 bottom = 4;
bool RectProto::has_bottom() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
void RectProto::set_has_bottom() {
  _has_bits_[0] |= 0x00000008u;
}
void RectProto::clear_has_bottom() {
  _has_bits_[0] &= ~0x00000008u;
}
void RectProto::clear_bottom() {
  bottom_ = 0;
  clear_has_bottom();
}
 ::google::protobuf::int32 RectProto::bottom() const {
  // @@protoc_insertion_point(field_get:android.graphics.RectProto.bottom)
  return bottom_;
}
 void RectProto::set_bottom(::google::protobuf::int32 value) {
  set_has_bottom();
  bottom_ = value;
  // @@protoc_insertion_point(field_set:android.graphics.RectProto.bottom)
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace graphics
}  // namespace android

// @@protoc_insertion_point(global_scope)
