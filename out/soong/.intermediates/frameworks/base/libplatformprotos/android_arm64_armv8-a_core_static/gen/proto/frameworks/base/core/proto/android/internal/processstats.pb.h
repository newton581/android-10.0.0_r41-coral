// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/internal/processstats.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_util.h>
// @@protoc_insertion_point(includes)

namespace com {
namespace android {
namespace internal {
namespace app {
namespace procstats {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();

class ProcessStatsProto;

enum ProcessStatsProto_MemoryFactor {
  ProcessStatsProto_MemoryFactor_MEM_FACTOR_NORMAL = 0,
  ProcessStatsProto_MemoryFactor_MEM_FACTOR_MODERATE = 1,
  ProcessStatsProto_MemoryFactor_MEM_FACTOR_LOW = 2,
  ProcessStatsProto_MemoryFactor_MEM_FACTOR_CRITICAL = 3
};
bool ProcessStatsProto_MemoryFactor_IsValid(int value);
const ProcessStatsProto_MemoryFactor ProcessStatsProto_MemoryFactor_MemoryFactor_MIN = ProcessStatsProto_MemoryFactor_MEM_FACTOR_NORMAL;
const ProcessStatsProto_MemoryFactor ProcessStatsProto_MemoryFactor_MemoryFactor_MAX = ProcessStatsProto_MemoryFactor_MEM_FACTOR_CRITICAL;
const int ProcessStatsProto_MemoryFactor_MemoryFactor_ARRAYSIZE = ProcessStatsProto_MemoryFactor_MemoryFactor_MAX + 1;

// ===================================================================

class ProcessStatsProto : public ::google::protobuf::MessageLite {
 public:
  ProcessStatsProto();
  virtual ~ProcessStatsProto();

  ProcessStatsProto(const ProcessStatsProto& from);

  inline ProcessStatsProto& operator=(const ProcessStatsProto& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const ProcessStatsProto& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const ProcessStatsProto* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(ProcessStatsProto* other);

  // implements Message ----------------------------------------------

  inline ProcessStatsProto* New() const { return New(NULL); }

  ProcessStatsProto* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const ProcessStatsProto& from);
  void MergeFrom(const ProcessStatsProto& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(ProcessStatsProto* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  typedef ProcessStatsProto_MemoryFactor MemoryFactor;
  static const MemoryFactor MEM_FACTOR_NORMAL =
    ProcessStatsProto_MemoryFactor_MEM_FACTOR_NORMAL;
  static const MemoryFactor MEM_FACTOR_MODERATE =
    ProcessStatsProto_MemoryFactor_MEM_FACTOR_MODERATE;
  static const MemoryFactor MEM_FACTOR_LOW =
    ProcessStatsProto_MemoryFactor_MEM_FACTOR_LOW;
  static const MemoryFactor MEM_FACTOR_CRITICAL =
    ProcessStatsProto_MemoryFactor_MEM_FACTOR_CRITICAL;
  static inline bool MemoryFactor_IsValid(int value) {
    return ProcessStatsProto_MemoryFactor_IsValid(value);
  }
  static const MemoryFactor MemoryFactor_MIN =
    ProcessStatsProto_MemoryFactor_MemoryFactor_MIN;
  static const MemoryFactor MemoryFactor_MAX =
    ProcessStatsProto_MemoryFactor_MemoryFactor_MAX;
  static const int MemoryFactor_ARRAYSIZE =
    ProcessStatsProto_MemoryFactor_MemoryFactor_ARRAYSIZE;

  // accessors -------------------------------------------------------

  // @@protoc_insertion_point(class_scope:com.android.internal.app.procstats.ProcessStatsProto)
 private:

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();
  #endif
  friend void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();
  friend void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto();

  void InitAsDefaultInstance();
  static ProcessStatsProto* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// ProcessStatsProto

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace procstats
}  // namespace app
}  // namespace internal
}  // namespace android
}  // namespace com

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::com::android::internal::app::procstats::ProcessStatsProto_MemoryFactor> : ::google::protobuf::internal::true_type {};

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2finternal_2fprocessstats_2eproto__INCLUDED
