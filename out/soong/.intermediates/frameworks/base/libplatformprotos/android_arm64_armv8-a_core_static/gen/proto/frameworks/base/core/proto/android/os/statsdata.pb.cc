// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/os/statsdata.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/os/statsdata.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace os {

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto() {
  delete StatsDataDumpProto::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ::android::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
  StatsDataDumpProto::default_instance_ = new StatsDataDumpProto();
  StatsDataDumpProto::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto_once_);
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto_once_,
                 &protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForStatsDataDumpProto(
    StatsDataDumpProto* ptr) {
  return ptr->mutable_unknown_fields();
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int StatsDataDumpProto::kConfigMetricsReportListFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

StatsDataDumpProto::StatsDataDumpProto()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.os.StatsDataDumpProto)
}

void StatsDataDumpProto::InitAsDefaultInstance() {
}

StatsDataDumpProto::StatsDataDumpProto(const StatsDataDumpProto& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.os.StatsDataDumpProto)
}

void StatsDataDumpProto::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

StatsDataDumpProto::~StatsDataDumpProto() {
  // @@protoc_insertion_point(destructor:android.os.StatsDataDumpProto)
  SharedDtor();
}

void StatsDataDumpProto::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void StatsDataDumpProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const StatsDataDumpProto& StatsDataDumpProto::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fstatsdata_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

StatsDataDumpProto* StatsDataDumpProto::default_instance_ = NULL;

StatsDataDumpProto* StatsDataDumpProto::New(::google::protobuf::Arena* arena) const {
  StatsDataDumpProto* n = new StatsDataDumpProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void StatsDataDumpProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.os.StatsDataDumpProto)
  config_metrics_report_list_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool StatsDataDumpProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForStatsDataDumpProto, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.os.StatsDataDumpProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // repeated bytes config_metrics_report_list = 1;
      case 1: {
        if (tag == 10) {
         parse_config_metrics_report_list:
          DO_(::google::protobuf::internal::WireFormatLite::ReadBytes(
                input, this->add_config_metrics_report_list()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(10)) goto parse_config_metrics_report_list;
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.os.StatsDataDumpProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.os.StatsDataDumpProto)
  return false;
#undef DO_
}

void StatsDataDumpProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.os.StatsDataDumpProto)
  // repeated bytes config_metrics_report_list = 1;
  for (int i = 0; i < this->config_metrics_report_list_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteBytes(
      1, this->config_metrics_report_list(i), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.os.StatsDataDumpProto)
}

int StatsDataDumpProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.os.StatsDataDumpProto)
  int total_size = 0;

  // repeated bytes config_metrics_report_list = 1;
  total_size += 1 * this->config_metrics_report_list_size();
  for (int i = 0; i < this->config_metrics_report_list_size(); i++) {
    total_size += ::google::protobuf::internal::WireFormatLite::BytesSize(
      this->config_metrics_report_list(i));
  }

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void StatsDataDumpProto::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const StatsDataDumpProto*>(&from));
}

void StatsDataDumpProto::MergeFrom(const StatsDataDumpProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.os.StatsDataDumpProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  config_metrics_report_list_.MergeFrom(from.config_metrics_report_list_);
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void StatsDataDumpProto::CopyFrom(const StatsDataDumpProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.os.StatsDataDumpProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool StatsDataDumpProto::IsInitialized() const {

  return true;
}

void StatsDataDumpProto::Swap(StatsDataDumpProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void StatsDataDumpProto::InternalSwap(StatsDataDumpProto* other) {
  config_metrics_report_list_.UnsafeArenaSwap(&other->config_metrics_report_list_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string StatsDataDumpProto::GetTypeName() const {
  return "android.os.StatsDataDumpProto";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// StatsDataDumpProto

// repeated bytes config_metrics_report_list = 1;
int StatsDataDumpProto::config_metrics_report_list_size() const {
  return config_metrics_report_list_.size();
}
void StatsDataDumpProto::clear_config_metrics_report_list() {
  config_metrics_report_list_.Clear();
}
 const ::std::string& StatsDataDumpProto::config_metrics_report_list(int index) const {
  // @@protoc_insertion_point(field_get:android.os.StatsDataDumpProto.config_metrics_report_list)
  return config_metrics_report_list_.Get(index);
}
 ::std::string* StatsDataDumpProto::mutable_config_metrics_report_list(int index) {
  // @@protoc_insertion_point(field_mutable:android.os.StatsDataDumpProto.config_metrics_report_list)
  return config_metrics_report_list_.Mutable(index);
}
 void StatsDataDumpProto::set_config_metrics_report_list(int index, const ::std::string& value) {
  // @@protoc_insertion_point(field_set:android.os.StatsDataDumpProto.config_metrics_report_list)
  config_metrics_report_list_.Mutable(index)->assign(value);
}
 void StatsDataDumpProto::set_config_metrics_report_list(int index, const char* value) {
  config_metrics_report_list_.Mutable(index)->assign(value);
  // @@protoc_insertion_point(field_set_char:android.os.StatsDataDumpProto.config_metrics_report_list)
}
 void StatsDataDumpProto::set_config_metrics_report_list(int index, const void* value, size_t size) {
  config_metrics_report_list_.Mutable(index)->assign(
    reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:android.os.StatsDataDumpProto.config_metrics_report_list)
}
 ::std::string* StatsDataDumpProto::add_config_metrics_report_list() {
  // @@protoc_insertion_point(field_add_mutable:android.os.StatsDataDumpProto.config_metrics_report_list)
  return config_metrics_report_list_.Add();
}
 void StatsDataDumpProto::add_config_metrics_report_list(const ::std::string& value) {
  config_metrics_report_list_.Add()->assign(value);
  // @@protoc_insertion_point(field_add:android.os.StatsDataDumpProto.config_metrics_report_list)
}
 void StatsDataDumpProto::add_config_metrics_report_list(const char* value) {
  config_metrics_report_list_.Add()->assign(value);
  // @@protoc_insertion_point(field_add_char:android.os.StatsDataDumpProto.config_metrics_report_list)
}
 void StatsDataDumpProto::add_config_metrics_report_list(const void* value, size_t size) {
  config_metrics_report_list_.Add()->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_add_pointer:android.os.StatsDataDumpProto.config_metrics_report_list)
}
 const ::google::protobuf::RepeatedPtrField< ::std::string>&
StatsDataDumpProto::config_metrics_report_list() const {
  // @@protoc_insertion_point(field_list:android.os.StatsDataDumpProto.config_metrics_report_list)
  return config_metrics_report_list_;
}
 ::google::protobuf::RepeatedPtrField< ::std::string>*
StatsDataDumpProto::mutable_config_metrics_report_list() {
  // @@protoc_insertion_point(field_mutable_list:android.os.StatsDataDumpProto.config_metrics_report_list)
  return &config_metrics_report_list_;
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace os
}  // namespace android

// @@protoc_insertion_point(global_scope)
