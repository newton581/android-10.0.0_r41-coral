// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/app/settings_enums.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/app/settings_enums.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace app {
namespace settings {

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto() {
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto_once_);
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto_once_,
                 &protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fsettings_5fenums_2eproto_;
#endif
bool Action_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 135:
    case 137:
    case 138:
    case 139:
    case 161:
    case 162:
    case 167:
    case 168:
    case 169:
    case 170:
    case 171:
    case 175:
    case 177:
    case 178:
    case 203:
    case 226:
    case 253:
    case 254:
    case 294:
    case 295:
    case 373:
    case 375:
    case 376:
    case 394:
    case 395:
    case 396:
    case 489:
    case 495:
    case 764:
    case 765:
    case 766:
    case 767:
    case 768:
    case 769:
    case 770:
    case 771:
    case 772:
    case 773:
    case 774:
    case 775:
    case 776:
    case 777:
    case 778:
    case 779:
    case 780:
    case 781:
    case 782:
    case 783:
    case 784:
    case 807:
    case 813:
    case 814:
    case 816:
    case 829:
    case 834:
    case 847:
    case 851:
    case 852:
    case 853:
    case 866:
    case 867:
    case 868:
    case 869:
    case 870:
    case 872:
    case 873:
    case 874:
    case 875:
    case 876:
    case 877:
    case 923:
    case 991:
    case 992:
    case 993:
    case 1000:
    case 1008:
    case 1016:
    case 1017:
    case 1019:
    case 1033:
    case 1096:
    case 1210:
    case 1226:
    case 1227:
    case 1249:
    case 1267:
    case 1268:
    case 1332:
    case 1333:
    case 1334:
    case 1335:
    case 1336:
    case 1337:
    case 1338:
    case 1340:
    case 1347:
    case 1348:
    case 1349:
    case 1350:
    case 1351:
    case 1352:
    case 1353:
    case 1354:
    case 1361:
    case 1362:
    case 1363:
    case 1364:
    case 1365:
    case 1367:
    case 1371:
    case 1372:
    case 1378:
    case 1379:
    case 1387:
    case 1388:
    case 1396:
    case 1397:
    case 1398:
    case 1399:
    case 1406:
    case 1407:
    case 1408:
    case 1409:
    case 1410:
    case 1411:
    case 1412:
    case 1413:
    case 1415:
    case 1416:
    case 1598:
    case 1622:
    case 1623:
    case 1643:
    case 1645:
    case 1646:
    case 1658:
    case 1662:
    case 1663:
    case 1664:
    case 1665:
    case 1666:
    case 1674:
    case 1675:
    case 1676:
    case 1677:
    case 1678:
    case 1679:
    case 1680:
    case 1681:
    case 1684:
    case 1685:
    case 1686:
    case 1703:
    case 1710:
    case 1711:
    case 1712:
    case 1725:
    case 1726:
    case 1727:
      return true;
    default:
      return false;
  }
}

bool PageId_IsValid(int value) {
  switch(value) {
    case 0:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 12:
    case 13:
    case 17:
    case 19:
    case 20:
    case 21:
    case 23:
    case 25:
    case 27:
    case 28:
    case 29:
    case 30:
    case 31:
    case 32:
    case 33:
    case 35:
    case 37:
    case 38:
    case 39:
    case 40:
    case 42:
    case 46:
    case 47:
    case 48:
    case 49:
    case 51:
    case 52:
    case 53:
    case 56:
    case 58:
    case 59:
    case 60:
    case 61:
    case 62:
    case 63:
    case 65:
    case 66:
    case 67:
    case 69:
    case 70:
    case 72:
    case 74:
    case 75:
    case 76:
    case 78:
    case 79:
    case 80:
    case 81:
    case 82:
    case 83:
    case 84:
    case 85:
    case 86:
    case 87:
    case 88:
    case 89:
    case 90:
    case 91:
    case 92:
    case 93:
    case 94:
    case 95:
    case 96:
    case 97:
    case 98:
    case 100:
    case 101:
    case 102:
    case 103:
    case 105:
    case 106:
    case 109:
    case 130:
    case 131:
    case 133:
    case 141:
    case 142:
    case 143:
    case 144:
    case 146:
    case 179:
    case 180:
    case 182:
    case 183:
    case 184:
    case 201:
    case 202:
    case 221:
    case 225:
    case 238:
    case 240:
    case 241:
    case 242:
    case 243:
    case 245:
    case 246:
    case 247:
    case 248:
    case 249:
    case 258:
    case 265:
    case 285:
    case 334:
    case 335:
    case 336:
    case 337:
    case 338:
    case 339:
    case 340:
    case 341:
    case 342:
    case 343:
    case 344:
    case 345:
    case 346:
    case 347:
    case 348:
    case 349:
    case 351:
    case 367:
    case 368:
    case 369:
    case 370:
    case 371:
    case 377:
    case 378:
    case 379:
    case 380:
    case 381:
    case 382:
    case 383:
    case 388:
    case 401:
    case 402:
    case 403:
    case 404:
    case 405:
    case 458:
    case 459:
    case 488:
    case 492:
    case 514:
    case 515:
    case 516:
    case 528:
    case 529:
    case 530:
    case 531:
    case 532:
    case 533:
    case 534:
    case 535:
    case 536:
    case 538:
    case 540:
    case 541:
    case 543:
    case 545:
    case 546:
    case 547:
    case 548:
    case 549:
    case 550:
    case 551:
    case 552:
    case 553:
    case 554:
    case 555:
    case 556:
    case 557:
    case 558:
    case 559:
    case 561:
    case 562:
    case 563:
    case 564:
    case 565:
    case 566:
    case 567:
    case 568:
    case 569:
    case 570:
    case 571:
    case 573:
    case 574:
    case 575:
    case 576:
    case 577:
    case 578:
    case 579:
    case 581:
    case 583:
    case 584:
    case 585:
    case 586:
    case 587:
    case 588:
    case 589:
    case 590:
    case 591:
    case 592:
    case 593:
    case 594:
    case 595:
    case 596:
    case 597:
    case 598:
    case 599:
    case 600:
    case 601:
    case 602:
    case 603:
    case 606:
    case 607:
    case 608:
    case 609:
    case 613:
    case 628:
    case 744:
    case 745:
    case 746:
    case 747:
    case 748:
    case 750:
    case 751:
    case 752:
    case 753:
    case 754:
    case 755:
    case 785:
    case 786:
    case 787:
    case 788:
    case 789:
    case 790:
    case 791:
    case 792:
    case 808:
    case 812:
    case 817:
    case 818:
    case 838:
    case 839:
    case 840:
    case 841:
    case 843:
    case 844:
    case 845:
    case 846:
    case 849:
    case 861:
    case 862:
    case 882:
    case 921:
    case 922:
    case 924:
    case 934:
    case 935:
    case 938:
    case 939:
    case 940:
    case 990:
    case 996:
    case 1009:
    case 1010:
    case 1014:
    case 1015:
    case 1018:
    case 1031:
    case 1092:
    case 1143:
    case 1217:
    case 1218:
    case 1219:
    case 1220:
    case 1221:
    case 1222:
    case 1223:
    case 1224:
    case 1225:
    case 1230:
    case 1238:
    case 1240:
    case 1246:
    case 1247:
    case 1263:
    case 1264:
    case 1265:
    case 1266:
    case 1269:
    case 1270:
    case 1281:
    case 1285:
    case 1286:
    case 1291:
    case 1292:
    case 1293:
    case 1294:
    case 1312:
    case 1323:
    case 1339:
    case 1341:
    case 1355:
    case 1356:
    case 1357:
    case 1360:
    case 1368:
    case 1369:
    case 1370:
    case 1373:
    case 1374:
    case 1375:
    case 1380:
    case 1381:
    case 1390:
    case 1400:
    case 1401:
    case 1441:
    case 1502:
    case 1503:
    case 1506:
    case 1507:
    case 1508:
    case 1509:
    case 1510:
    case 1511:
    case 1512:
    case 1554:
    case 1556:
    case 1557:
    case 1570:
    case 1571:
    case 1581:
    case 1582:
    case 1583:
    case 1584:
    case 1585:
    case 1586:
    case 1587:
    case 1588:
    case 1589:
    case 1591:
    case 1595:
    case 1596:
    case 1597:
    case 1604:
    case 1605:
    case 1606:
    case 1608:
    case 1609:
    case 1610:
    case 1611:
    case 1612:
    case 1613:
    case 1620:
    case 1624:
    case 1625:
    case 1626:
    case 1627:
    case 1628:
    case 1632:
    case 1633:
    case 1642:
    case 1644:
    case 1654:
    case 1655:
    case 1656:
    case 1657:
    case 1667:
    case 1668:
    case 1669:
    case 1670:
    case 1671:
    case 1672:
    case 1673:
    case 1682:
    case 1683:
    case 1687:
    case 1692:
    case 1693:
    case 1698:
    case 1699:
    case 1700:
    case 1701:
    case 1702:
    case 1707:
    case 1708:
    case 1709:
    case 1713:
    case 1714:
    case 1715:
    case 1728:
    case 1740:
    case 1746:
    case 1747:
    case 1748:
    case 1750:
    case 1751:
      return true;
    default:
      return false;
  }
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace settings
}  // namespace app
}  // namespace android

// @@protoc_insertion_point(global_scope)
