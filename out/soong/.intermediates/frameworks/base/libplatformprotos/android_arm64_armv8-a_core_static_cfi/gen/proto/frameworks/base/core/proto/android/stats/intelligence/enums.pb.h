// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/stats/intelligence/enums.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fintelligence_2fenums_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fintelligence_2fenums_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_util.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace stats {
namespace intelligence {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fintelligence_2fenums_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fintelligence_2fenums_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fintelligence_2fenums_2eproto();


enum Status {
  STATUS_UNKNOWN = 0,
  STATUS_SUCCEEDED = 1,
  STATUS_FAILED = 2
};
bool Status_IsValid(int value);
const Status Status_MIN = STATUS_UNKNOWN;
const Status Status_MAX = STATUS_FAILED;
const int Status_ARRAYSIZE = Status_MAX + 1;

enum EventType {
  EVENT_UNKNOWN = 0,
  EVENT_CONTENT_SUGGESTIONS_CLASSIFY_CONTENT_CALL = 1,
  EVENT_CONTENT_SUGGESTIONS_SUGGEST_CONTENT_CALL = 2
};
bool EventType_IsValid(int value);
const EventType EventType_MIN = EVENT_UNKNOWN;
const EventType EventType_MAX = EVENT_CONTENT_SUGGESTIONS_SUGGEST_CONTENT_CALL;
const int EventType_ARRAYSIZE = EventType_MAX + 1;

// ===================================================================


// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace intelligence
}  // namespace stats
}  // namespace android

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::android::stats::intelligence::Status> : ::google::protobuf::internal::true_type {};
template <> struct is_proto_enum< ::android::stats::intelligence::EventType> : ::google::protobuf::internal::true_type {};

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fintelligence_2fenums_2eproto__INCLUDED
