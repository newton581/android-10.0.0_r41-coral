// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/os/message.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fmessage_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fmessage_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include "frameworks/base/core/proto/android/privacy.pb.h"
// @@protoc_insertion_point(includes)

namespace android {
namespace os {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fmessage_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fmessage_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fmessage_2eproto();

class MessageProto;

// ===================================================================

class MessageProto : public ::google::protobuf::MessageLite {
 public:
  MessageProto();
  virtual ~MessageProto();

  MessageProto(const MessageProto& from);

  inline MessageProto& operator=(const MessageProto& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const MessageProto& default_instance();

  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  // Returns the internal default instance pointer. This function can
  // return NULL thus should not be used by the user. This is intended
  // for Protobuf internal code. Please use default_instance() declared
  // above instead.
  static inline const MessageProto* internal_default_instance() {
    return default_instance_;
  }
  #endif

  void Swap(MessageProto* other);

  // implements Message ----------------------------------------------

  inline MessageProto* New() const { return New(NULL); }

  MessageProto* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const MessageProto& from);
  void MergeFrom(const MessageProto& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(MessageProto* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional int64 when = 1;
  bool has_when() const;
  void clear_when();
  static const int kWhenFieldNumber = 1;
  ::google::protobuf::int64 when() const;
  void set_when(::google::protobuf::int64 value);

  // optional string callback = 2;
  bool has_callback() const;
  void clear_callback();
  static const int kCallbackFieldNumber = 2;
  const ::std::string& callback() const;
  void set_callback(const ::std::string& value);
  void set_callback(const char* value);
  void set_callback(const char* value, size_t size);
  ::std::string* mutable_callback();
  ::std::string* release_callback();
  void set_allocated_callback(::std::string* callback);

  // optional int32 what = 3;
  bool has_what() const;
  void clear_what();
  static const int kWhatFieldNumber = 3;
  ::google::protobuf::int32 what() const;
  void set_what(::google::protobuf::int32 value);

  // optional int32 arg1 = 4;
  bool has_arg1() const;
  void clear_arg1();
  static const int kArg1FieldNumber = 4;
  ::google::protobuf::int32 arg1() const;
  void set_arg1(::google::protobuf::int32 value);

  // optional int32 arg2 = 5;
  bool has_arg2() const;
  void clear_arg2();
  static const int kArg2FieldNumber = 5;
  ::google::protobuf::int32 arg2() const;
  void set_arg2(::google::protobuf::int32 value);

  // optional string obj = 6;
  bool has_obj() const;
  void clear_obj();
  static const int kObjFieldNumber = 6;
  const ::std::string& obj() const;
  void set_obj(const ::std::string& value);
  void set_obj(const char* value);
  void set_obj(const char* value, size_t size);
  ::std::string* mutable_obj();
  ::std::string* release_obj();
  void set_allocated_obj(::std::string* obj);

  // optional string target = 7;
  bool has_target() const;
  void clear_target();
  static const int kTargetFieldNumber = 7;
  const ::std::string& target() const;
  void set_target(const ::std::string& value);
  void set_target(const char* value);
  void set_target(const char* value, size_t size);
  ::std::string* mutable_target();
  ::std::string* release_target();
  void set_allocated_target(::std::string* target);

  // optional int32 barrier = 8;
  bool has_barrier() const;
  void clear_barrier();
  static const int kBarrierFieldNumber = 8;
  ::google::protobuf::int32 barrier() const;
  void set_barrier(::google::protobuf::int32 value);

  // @@protoc_insertion_point(class_scope:android.os.MessageProto)
 private:
  inline void set_has_when();
  inline void clear_has_when();
  inline void set_has_callback();
  inline void clear_has_callback();
  inline void set_has_what();
  inline void clear_has_what();
  inline void set_has_arg1();
  inline void clear_has_arg1();
  inline void set_has_arg2();
  inline void clear_has_arg2();
  inline void set_has_obj();
  inline void clear_has_obj();
  inline void set_has_target();
  inline void clear_has_target();
  inline void set_has_barrier();
  inline void clear_has_barrier();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::int64 when_;
  ::google::protobuf::internal::ArenaStringPtr callback_;
  ::google::protobuf::int32 what_;
  ::google::protobuf::int32 arg1_;
  ::google::protobuf::internal::ArenaStringPtr obj_;
  ::google::protobuf::int32 arg2_;
  ::google::protobuf::int32 barrier_;
  ::google::protobuf::internal::ArenaStringPtr target_;
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  friend void  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fmessage_2eproto_impl();
  #else
  friend void  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fmessage_2eproto();
  #endif
  friend void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fmessage_2eproto();
  friend void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fmessage_2eproto();

  void InitAsDefaultInstance();
  static MessageProto* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// MessageProto

// optional int64 when = 1;
inline bool MessageProto::has_when() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void MessageProto::set_has_when() {
  _has_bits_[0] |= 0x00000001u;
}
inline void MessageProto::clear_has_when() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void MessageProto::clear_when() {
  when_ = GOOGLE_LONGLONG(0);
  clear_has_when();
}
inline ::google::protobuf::int64 MessageProto::when() const {
  // @@protoc_insertion_point(field_get:android.os.MessageProto.when)
  return when_;
}
inline void MessageProto::set_when(::google::protobuf::int64 value) {
  set_has_when();
  when_ = value;
  // @@protoc_insertion_point(field_set:android.os.MessageProto.when)
}

// optional string callback = 2;
inline bool MessageProto::has_callback() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void MessageProto::set_has_callback() {
  _has_bits_[0] |= 0x00000002u;
}
inline void MessageProto::clear_has_callback() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void MessageProto::clear_callback() {
  callback_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_callback();
}
inline const ::std::string& MessageProto::callback() const {
  // @@protoc_insertion_point(field_get:android.os.MessageProto.callback)
  return callback_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void MessageProto::set_callback(const ::std::string& value) {
  set_has_callback();
  callback_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:android.os.MessageProto.callback)
}
inline void MessageProto::set_callback(const char* value) {
  set_has_callback();
  callback_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:android.os.MessageProto.callback)
}
inline void MessageProto::set_callback(const char* value, size_t size) {
  set_has_callback();
  callback_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:android.os.MessageProto.callback)
}
inline ::std::string* MessageProto::mutable_callback() {
  set_has_callback();
  // @@protoc_insertion_point(field_mutable:android.os.MessageProto.callback)
  return callback_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* MessageProto::release_callback() {
  // @@protoc_insertion_point(field_release:android.os.MessageProto.callback)
  clear_has_callback();
  return callback_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void MessageProto::set_allocated_callback(::std::string* callback) {
  if (callback != NULL) {
    set_has_callback();
  } else {
    clear_has_callback();
  }
  callback_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), callback);
  // @@protoc_insertion_point(field_set_allocated:android.os.MessageProto.callback)
}

// optional int32 what = 3;
inline bool MessageProto::has_what() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void MessageProto::set_has_what() {
  _has_bits_[0] |= 0x00000004u;
}
inline void MessageProto::clear_has_what() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void MessageProto::clear_what() {
  what_ = 0;
  clear_has_what();
}
inline ::google::protobuf::int32 MessageProto::what() const {
  // @@protoc_insertion_point(field_get:android.os.MessageProto.what)
  return what_;
}
inline void MessageProto::set_what(::google::protobuf::int32 value) {
  set_has_what();
  what_ = value;
  // @@protoc_insertion_point(field_set:android.os.MessageProto.what)
}

// optional int32 arg1 = 4;
inline bool MessageProto::has_arg1() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void MessageProto::set_has_arg1() {
  _has_bits_[0] |= 0x00000008u;
}
inline void MessageProto::clear_has_arg1() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void MessageProto::clear_arg1() {
  arg1_ = 0;
  clear_has_arg1();
}
inline ::google::protobuf::int32 MessageProto::arg1() const {
  // @@protoc_insertion_point(field_get:android.os.MessageProto.arg1)
  return arg1_;
}
inline void MessageProto::set_arg1(::google::protobuf::int32 value) {
  set_has_arg1();
  arg1_ = value;
  // @@protoc_insertion_point(field_set:android.os.MessageProto.arg1)
}

// optional int32 arg2 = 5;
inline bool MessageProto::has_arg2() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
inline void MessageProto::set_has_arg2() {
  _has_bits_[0] |= 0x00000010u;
}
inline void MessageProto::clear_has_arg2() {
  _has_bits_[0] &= ~0x00000010u;
}
inline void MessageProto::clear_arg2() {
  arg2_ = 0;
  clear_has_arg2();
}
inline ::google::protobuf::int32 MessageProto::arg2() const {
  // @@protoc_insertion_point(field_get:android.os.MessageProto.arg2)
  return arg2_;
}
inline void MessageProto::set_arg2(::google::protobuf::int32 value) {
  set_has_arg2();
  arg2_ = value;
  // @@protoc_insertion_point(field_set:android.os.MessageProto.arg2)
}

// optional string obj = 6;
inline bool MessageProto::has_obj() const {
  return (_has_bits_[0] & 0x00000020u) != 0;
}
inline void MessageProto::set_has_obj() {
  _has_bits_[0] |= 0x00000020u;
}
inline void MessageProto::clear_has_obj() {
  _has_bits_[0] &= ~0x00000020u;
}
inline void MessageProto::clear_obj() {
  obj_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_obj();
}
inline const ::std::string& MessageProto::obj() const {
  // @@protoc_insertion_point(field_get:android.os.MessageProto.obj)
  return obj_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void MessageProto::set_obj(const ::std::string& value) {
  set_has_obj();
  obj_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:android.os.MessageProto.obj)
}
inline void MessageProto::set_obj(const char* value) {
  set_has_obj();
  obj_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:android.os.MessageProto.obj)
}
inline void MessageProto::set_obj(const char* value, size_t size) {
  set_has_obj();
  obj_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:android.os.MessageProto.obj)
}
inline ::std::string* MessageProto::mutable_obj() {
  set_has_obj();
  // @@protoc_insertion_point(field_mutable:android.os.MessageProto.obj)
  return obj_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* MessageProto::release_obj() {
  // @@protoc_insertion_point(field_release:android.os.MessageProto.obj)
  clear_has_obj();
  return obj_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void MessageProto::set_allocated_obj(::std::string* obj) {
  if (obj != NULL) {
    set_has_obj();
  } else {
    clear_has_obj();
  }
  obj_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), obj);
  // @@protoc_insertion_point(field_set_allocated:android.os.MessageProto.obj)
}

// optional string target = 7;
inline bool MessageProto::has_target() const {
  return (_has_bits_[0] & 0x00000040u) != 0;
}
inline void MessageProto::set_has_target() {
  _has_bits_[0] |= 0x00000040u;
}
inline void MessageProto::clear_has_target() {
  _has_bits_[0] &= ~0x00000040u;
}
inline void MessageProto::clear_target() {
  target_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_target();
}
inline const ::std::string& MessageProto::target() const {
  // @@protoc_insertion_point(field_get:android.os.MessageProto.target)
  return target_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void MessageProto::set_target(const ::std::string& value) {
  set_has_target();
  target_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:android.os.MessageProto.target)
}
inline void MessageProto::set_target(const char* value) {
  set_has_target();
  target_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:android.os.MessageProto.target)
}
inline void MessageProto::set_target(const char* value, size_t size) {
  set_has_target();
  target_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:android.os.MessageProto.target)
}
inline ::std::string* MessageProto::mutable_target() {
  set_has_target();
  // @@protoc_insertion_point(field_mutable:android.os.MessageProto.target)
  return target_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* MessageProto::release_target() {
  // @@protoc_insertion_point(field_release:android.os.MessageProto.target)
  clear_has_target();
  return target_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void MessageProto::set_allocated_target(::std::string* target) {
  if (target != NULL) {
    set_has_target();
  } else {
    clear_has_target();
  }
  target_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), target);
  // @@protoc_insertion_point(field_set_allocated:android.os.MessageProto.target)
}

// optional int32 barrier = 8;
inline bool MessageProto::has_barrier() const {
  return (_has_bits_[0] & 0x00000080u) != 0;
}
inline void MessageProto::set_has_barrier() {
  _has_bits_[0] |= 0x00000080u;
}
inline void MessageProto::clear_has_barrier() {
  _has_bits_[0] &= ~0x00000080u;
}
inline void MessageProto::clear_barrier() {
  barrier_ = 0;
  clear_has_barrier();
}
inline ::google::protobuf::int32 MessageProto::barrier() const {
  // @@protoc_insertion_point(field_get:android.os.MessageProto.barrier)
  return barrier_;
}
inline void MessageProto::set_barrier(::google::protobuf::int32 value) {
  set_has_barrier();
  barrier_ = value;
  // @@protoc_insertion_point(field_set:android.os.MessageProto.barrier)
}

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace os
}  // namespace android

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fos_2fmessage_2eproto__INCLUDED
