// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/app/notification_channel_group.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/app/notification_channel_group.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace app {

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto() {
  delete NotificationChannelGroupProto::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ::android::app::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_2eproto();
  ::android::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
  NotificationChannelGroupProto::default_instance_ = new NotificationChannelGroupProto();
  NotificationChannelGroupProto::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto_once_);
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto_once_,
                 &protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForNotificationChannelGroupProto(
    NotificationChannelGroupProto* ptr) {
  return ptr->mutable_unknown_fields();
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int NotificationChannelGroupProto::kIdFieldNumber;
const int NotificationChannelGroupProto::kNameFieldNumber;
const int NotificationChannelGroupProto::kDescriptionFieldNumber;
const int NotificationChannelGroupProto::kIsBlockedFieldNumber;
const int NotificationChannelGroupProto::kChannelsFieldNumber;
const int NotificationChannelGroupProto::kAllowAppOverlayFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

NotificationChannelGroupProto::NotificationChannelGroupProto()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.app.NotificationChannelGroupProto)
}

void NotificationChannelGroupProto::InitAsDefaultInstance() {
}

NotificationChannelGroupProto::NotificationChannelGroupProto(const NotificationChannelGroupProto& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.app.NotificationChannelGroupProto)
}

void NotificationChannelGroupProto::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  id_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  name_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  description_.UnsafeSetDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  is_blocked_ = false;
  allow_app_overlay_ = false;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

NotificationChannelGroupProto::~NotificationChannelGroupProto() {
  // @@protoc_insertion_point(destructor:android.app.NotificationChannelGroupProto)
  SharedDtor();
}

void NotificationChannelGroupProto::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  id_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  name_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  description_.DestroyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void NotificationChannelGroupProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const NotificationChannelGroupProto& NotificationChannelGroupProto::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotification_5fchannel_5fgroup_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

NotificationChannelGroupProto* NotificationChannelGroupProto::default_instance_ = NULL;

NotificationChannelGroupProto* NotificationChannelGroupProto::New(::google::protobuf::Arena* arena) const {
  NotificationChannelGroupProto* n = new NotificationChannelGroupProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void NotificationChannelGroupProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.app.NotificationChannelGroupProto)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(NotificationChannelGroupProto, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<NotificationChannelGroupProto*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  if (_has_bits_[0 / 32] & 47u) {
    ZR_(is_blocked_, allow_app_overlay_);
    if (has_id()) {
      id_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    }
    if (has_name()) {
      name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    }
    if (has_description()) {
      description_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
    }
  }

#undef ZR_HELPER_
#undef ZR_

  channels_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool NotificationChannelGroupProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForNotificationChannelGroupProto, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.app.NotificationChannelGroupProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional string id = 1;
      case 1: {
        if (tag == 10) {
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_id()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(18)) goto parse_name;
        break;
      }

      // optional string name = 2;
      case 2: {
        if (tag == 18) {
         parse_name:
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_name()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(26)) goto parse_description;
        break;
      }

      // optional string description = 3;
      case 3: {
        if (tag == 26) {
         parse_description:
          DO_(::google::protobuf::internal::WireFormatLite::ReadString(
                input, this->mutable_description()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(32)) goto parse_is_blocked;
        break;
      }

      // optional bool is_blocked = 4;
      case 4: {
        if (tag == 32) {
         parse_is_blocked:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   bool, ::google::protobuf::internal::WireFormatLite::TYPE_BOOL>(
                 input, &is_blocked_)));
          set_has_is_blocked();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(42)) goto parse_channels;
        break;
      }

      // repeated .android.app.NotificationChannelProto channels = 5;
      case 5: {
        if (tag == 42) {
         parse_channels:
          DO_(input->IncrementRecursionDepth());
         parse_loop_channels:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtualNoRecursionDepth(
                input, add_channels()));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(42)) goto parse_loop_channels;
        input->UnsafeDecrementRecursionDepth();
        if (input->ExpectTag(48)) goto parse_allow_app_overlay;
        break;
      }

      // optional bool allow_app_overlay = 6;
      case 6: {
        if (tag == 48) {
         parse_allow_app_overlay:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   bool, ::google::protobuf::internal::WireFormatLite::TYPE_BOOL>(
                 input, &allow_app_overlay_)));
          set_has_allow_app_overlay();
        } else {
          goto handle_unusual;
        }
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.app.NotificationChannelGroupProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.app.NotificationChannelGroupProto)
  return false;
#undef DO_
}

void NotificationChannelGroupProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.app.NotificationChannelGroupProto)
  // optional string id = 1;
  if (has_id()) {
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      1, this->id(), output);
  }

  // optional string name = 2;
  if (has_name()) {
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      2, this->name(), output);
  }

  // optional string description = 3;
  if (has_description()) {
    ::google::protobuf::internal::WireFormatLite::WriteStringMaybeAliased(
      3, this->description(), output);
  }

  // optional bool is_blocked = 4;
  if (has_is_blocked()) {
    ::google::protobuf::internal::WireFormatLite::WriteBool(4, this->is_blocked(), output);
  }

  // repeated .android.app.NotificationChannelProto channels = 5;
  for (unsigned int i = 0, n = this->channels_size(); i < n; i++) {
    ::google::protobuf::internal::WireFormatLite::WriteMessage(
      5, this->channels(i), output);
  }

  // optional bool allow_app_overlay = 6;
  if (has_allow_app_overlay()) {
    ::google::protobuf::internal::WireFormatLite::WriteBool(6, this->allow_app_overlay(), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.app.NotificationChannelGroupProto)
}

int NotificationChannelGroupProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.app.NotificationChannelGroupProto)
  int total_size = 0;

  if (_has_bits_[0 / 32] & 47u) {
    // optional string id = 1;
    if (has_id()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::StringSize(
          this->id());
    }

    // optional string name = 2;
    if (has_name()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::StringSize(
          this->name());
    }

    // optional string description = 3;
    if (has_description()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::StringSize(
          this->description());
    }

    // optional bool is_blocked = 4;
    if (has_is_blocked()) {
      total_size += 1 + 1;
    }

    // optional bool allow_app_overlay = 6;
    if (has_allow_app_overlay()) {
      total_size += 1 + 1;
    }

  }
  // repeated .android.app.NotificationChannelProto channels = 5;
  total_size += 1 * this->channels_size();
  for (int i = 0; i < this->channels_size(); i++) {
    total_size +=
      ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
        this->channels(i));
  }

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void NotificationChannelGroupProto::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const NotificationChannelGroupProto*>(&from));
}

void NotificationChannelGroupProto::MergeFrom(const NotificationChannelGroupProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.app.NotificationChannelGroupProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  channels_.MergeFrom(from.channels_);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_id()) {
      set_has_id();
      id_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.id_);
    }
    if (from.has_name()) {
      set_has_name();
      name_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.name_);
    }
    if (from.has_description()) {
      set_has_description();
      description_.AssignWithDefault(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), from.description_);
    }
    if (from.has_is_blocked()) {
      set_is_blocked(from.is_blocked());
    }
    if (from.has_allow_app_overlay()) {
      set_allow_app_overlay(from.allow_app_overlay());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void NotificationChannelGroupProto::CopyFrom(const NotificationChannelGroupProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.app.NotificationChannelGroupProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool NotificationChannelGroupProto::IsInitialized() const {

  return true;
}

void NotificationChannelGroupProto::Swap(NotificationChannelGroupProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void NotificationChannelGroupProto::InternalSwap(NotificationChannelGroupProto* other) {
  id_.Swap(&other->id_);
  name_.Swap(&other->name_);
  description_.Swap(&other->description_);
  std::swap(is_blocked_, other->is_blocked_);
  channels_.UnsafeArenaSwap(&other->channels_);
  std::swap(allow_app_overlay_, other->allow_app_overlay_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string NotificationChannelGroupProto::GetTypeName() const {
  return "android.app.NotificationChannelGroupProto";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// NotificationChannelGroupProto

// optional string id = 1;
bool NotificationChannelGroupProto::has_id() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
void NotificationChannelGroupProto::set_has_id() {
  _has_bits_[0] |= 0x00000001u;
}
void NotificationChannelGroupProto::clear_has_id() {
  _has_bits_[0] &= ~0x00000001u;
}
void NotificationChannelGroupProto::clear_id() {
  id_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_id();
}
 const ::std::string& NotificationChannelGroupProto::id() const {
  // @@protoc_insertion_point(field_get:android.app.NotificationChannelGroupProto.id)
  return id_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void NotificationChannelGroupProto::set_id(const ::std::string& value) {
  set_has_id();
  id_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:android.app.NotificationChannelGroupProto.id)
}
 void NotificationChannelGroupProto::set_id(const char* value) {
  set_has_id();
  id_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:android.app.NotificationChannelGroupProto.id)
}
 void NotificationChannelGroupProto::set_id(const char* value, size_t size) {
  set_has_id();
  id_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:android.app.NotificationChannelGroupProto.id)
}
 ::std::string* NotificationChannelGroupProto::mutable_id() {
  set_has_id();
  // @@protoc_insertion_point(field_mutable:android.app.NotificationChannelGroupProto.id)
  return id_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 ::std::string* NotificationChannelGroupProto::release_id() {
  // @@protoc_insertion_point(field_release:android.app.NotificationChannelGroupProto.id)
  clear_has_id();
  return id_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void NotificationChannelGroupProto::set_allocated_id(::std::string* id) {
  if (id != NULL) {
    set_has_id();
  } else {
    clear_has_id();
  }
  id_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), id);
  // @@protoc_insertion_point(field_set_allocated:android.app.NotificationChannelGroupProto.id)
}

// optional string name = 2;
bool NotificationChannelGroupProto::has_name() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void NotificationChannelGroupProto::set_has_name() {
  _has_bits_[0] |= 0x00000002u;
}
void NotificationChannelGroupProto::clear_has_name() {
  _has_bits_[0] &= ~0x00000002u;
}
void NotificationChannelGroupProto::clear_name() {
  name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_name();
}
 const ::std::string& NotificationChannelGroupProto::name() const {
  // @@protoc_insertion_point(field_get:android.app.NotificationChannelGroupProto.name)
  return name_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void NotificationChannelGroupProto::set_name(const ::std::string& value) {
  set_has_name();
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:android.app.NotificationChannelGroupProto.name)
}
 void NotificationChannelGroupProto::set_name(const char* value) {
  set_has_name();
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:android.app.NotificationChannelGroupProto.name)
}
 void NotificationChannelGroupProto::set_name(const char* value, size_t size) {
  set_has_name();
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:android.app.NotificationChannelGroupProto.name)
}
 ::std::string* NotificationChannelGroupProto::mutable_name() {
  set_has_name();
  // @@protoc_insertion_point(field_mutable:android.app.NotificationChannelGroupProto.name)
  return name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 ::std::string* NotificationChannelGroupProto::release_name() {
  // @@protoc_insertion_point(field_release:android.app.NotificationChannelGroupProto.name)
  clear_has_name();
  return name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void NotificationChannelGroupProto::set_allocated_name(::std::string* name) {
  if (name != NULL) {
    set_has_name();
  } else {
    clear_has_name();
  }
  name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), name);
  // @@protoc_insertion_point(field_set_allocated:android.app.NotificationChannelGroupProto.name)
}

// optional string description = 3;
bool NotificationChannelGroupProto::has_description() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
void NotificationChannelGroupProto::set_has_description() {
  _has_bits_[0] |= 0x00000004u;
}
void NotificationChannelGroupProto::clear_has_description() {
  _has_bits_[0] &= ~0x00000004u;
}
void NotificationChannelGroupProto::clear_description() {
  description_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_description();
}
 const ::std::string& NotificationChannelGroupProto::description() const {
  // @@protoc_insertion_point(field_get:android.app.NotificationChannelGroupProto.description)
  return description_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void NotificationChannelGroupProto::set_description(const ::std::string& value) {
  set_has_description();
  description_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:android.app.NotificationChannelGroupProto.description)
}
 void NotificationChannelGroupProto::set_description(const char* value) {
  set_has_description();
  description_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:android.app.NotificationChannelGroupProto.description)
}
 void NotificationChannelGroupProto::set_description(const char* value, size_t size) {
  set_has_description();
  description_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:android.app.NotificationChannelGroupProto.description)
}
 ::std::string* NotificationChannelGroupProto::mutable_description() {
  set_has_description();
  // @@protoc_insertion_point(field_mutable:android.app.NotificationChannelGroupProto.description)
  return description_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 ::std::string* NotificationChannelGroupProto::release_description() {
  // @@protoc_insertion_point(field_release:android.app.NotificationChannelGroupProto.description)
  clear_has_description();
  return description_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
 void NotificationChannelGroupProto::set_allocated_description(::std::string* description) {
  if (description != NULL) {
    set_has_description();
  } else {
    clear_has_description();
  }
  description_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), description);
  // @@protoc_insertion_point(field_set_allocated:android.app.NotificationChannelGroupProto.description)
}

// optional bool is_blocked = 4;
bool NotificationChannelGroupProto::has_is_blocked() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
void NotificationChannelGroupProto::set_has_is_blocked() {
  _has_bits_[0] |= 0x00000008u;
}
void NotificationChannelGroupProto::clear_has_is_blocked() {
  _has_bits_[0] &= ~0x00000008u;
}
void NotificationChannelGroupProto::clear_is_blocked() {
  is_blocked_ = false;
  clear_has_is_blocked();
}
 bool NotificationChannelGroupProto::is_blocked() const {
  // @@protoc_insertion_point(field_get:android.app.NotificationChannelGroupProto.is_blocked)
  return is_blocked_;
}
 void NotificationChannelGroupProto::set_is_blocked(bool value) {
  set_has_is_blocked();
  is_blocked_ = value;
  // @@protoc_insertion_point(field_set:android.app.NotificationChannelGroupProto.is_blocked)
}

// repeated .android.app.NotificationChannelProto channels = 5;
int NotificationChannelGroupProto::channels_size() const {
  return channels_.size();
}
void NotificationChannelGroupProto::clear_channels() {
  channels_.Clear();
}
const ::android::app::NotificationChannelProto& NotificationChannelGroupProto::channels(int index) const {
  // @@protoc_insertion_point(field_get:android.app.NotificationChannelGroupProto.channels)
  return channels_.Get(index);
}
::android::app::NotificationChannelProto* NotificationChannelGroupProto::mutable_channels(int index) {
  // @@protoc_insertion_point(field_mutable:android.app.NotificationChannelGroupProto.channels)
  return channels_.Mutable(index);
}
::android::app::NotificationChannelProto* NotificationChannelGroupProto::add_channels() {
  // @@protoc_insertion_point(field_add:android.app.NotificationChannelGroupProto.channels)
  return channels_.Add();
}
::google::protobuf::RepeatedPtrField< ::android::app::NotificationChannelProto >*
NotificationChannelGroupProto::mutable_channels() {
  // @@protoc_insertion_point(field_mutable_list:android.app.NotificationChannelGroupProto.channels)
  return &channels_;
}
const ::google::protobuf::RepeatedPtrField< ::android::app::NotificationChannelProto >&
NotificationChannelGroupProto::channels() const {
  // @@protoc_insertion_point(field_list:android.app.NotificationChannelGroupProto.channels)
  return channels_;
}

// optional bool allow_app_overlay = 6;
bool NotificationChannelGroupProto::has_allow_app_overlay() const {
  return (_has_bits_[0] & 0x00000020u) != 0;
}
void NotificationChannelGroupProto::set_has_allow_app_overlay() {
  _has_bits_[0] |= 0x00000020u;
}
void NotificationChannelGroupProto::clear_has_allow_app_overlay() {
  _has_bits_[0] &= ~0x00000020u;
}
void NotificationChannelGroupProto::clear_allow_app_overlay() {
  allow_app_overlay_ = false;
  clear_has_allow_app_overlay();
}
 bool NotificationChannelGroupProto::allow_app_overlay() const {
  // @@protoc_insertion_point(field_get:android.app.NotificationChannelGroupProto.allow_app_overlay)
  return allow_app_overlay_;
}
 void NotificationChannelGroupProto::set_allow_app_overlay(bool value) {
  set_has_allow_app_overlay();
  allow_app_overlay_ = value;
  // @@protoc_insertion_point(field_set:android.app.NotificationChannelGroupProto.allow_app_overlay)
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace app
}  // namespace android

// @@protoc_insertion_point(global_scope)
