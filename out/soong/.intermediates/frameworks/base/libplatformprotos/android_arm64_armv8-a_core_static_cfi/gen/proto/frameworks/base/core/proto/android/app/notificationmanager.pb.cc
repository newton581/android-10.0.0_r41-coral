// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/app/notificationmanager.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "frameworks/base/core/proto/android/app/notificationmanager.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/port.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/io/zero_copy_stream_impl_lite.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace app {

void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto() {
  delete PolicyProto::default_instance_;
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto_impl() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#else
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

#endif
  ::android::protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
  PolicyProto::default_instance_ = new PolicyProto();
  PolicyProto::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto);
}

#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto_once_);
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto_once_,
                 &protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto_impl);
}
#else
// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto {
  StaticDescriptorInitializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto() {
    protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto();
  }
} static_descriptor_initializer_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto_;
#endif

namespace {

static void MergeFromFail(int line) GOOGLE_ATTRIBUTE_COLD;
static void MergeFromFail(int line) {
  GOOGLE_CHECK(false) << __FILE__ << ":" << line;
}

}  // namespace


// ===================================================================

static ::std::string* MutableUnknownFieldsForPolicyProto(
    PolicyProto* ptr) {
  return ptr->mutable_unknown_fields();
}

bool PolicyProto_Category_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const PolicyProto_Category PolicyProto::CATEGORY_UNKNOWN;
const PolicyProto_Category PolicyProto::REMINDERS;
const PolicyProto_Category PolicyProto::EVENTS;
const PolicyProto_Category PolicyProto::MESSAGES;
const PolicyProto_Category PolicyProto::CALLS;
const PolicyProto_Category PolicyProto::REPEAT_CALLERS;
const PolicyProto_Category PolicyProto::ALARMS;
const PolicyProto_Category PolicyProto::MEDIA;
const PolicyProto_Category PolicyProto::SYSTEM;
const PolicyProto_Category PolicyProto::Category_MIN;
const PolicyProto_Category PolicyProto::Category_MAX;
const int PolicyProto::Category_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
bool PolicyProto_Sender_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const PolicyProto_Sender PolicyProto::ANY;
const PolicyProto_Sender PolicyProto::CONTACTS;
const PolicyProto_Sender PolicyProto::STARRED;
const PolicyProto_Sender PolicyProto::Sender_MIN;
const PolicyProto_Sender PolicyProto::Sender_MAX;
const int PolicyProto::Sender_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
bool PolicyProto_SuppressedVisualEffect_IsValid(int value) {
  switch(value) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      return true;
    default:
      return false;
  }
}

#if !defined(_MSC_VER) || _MSC_VER >= 1900
const PolicyProto_SuppressedVisualEffect PolicyProto::SVE_UNKNOWN;
const PolicyProto_SuppressedVisualEffect PolicyProto::SCREEN_OFF;
const PolicyProto_SuppressedVisualEffect PolicyProto::SCREEN_ON;
const PolicyProto_SuppressedVisualEffect PolicyProto::FULL_SCREEN_INTENT;
const PolicyProto_SuppressedVisualEffect PolicyProto::LIGHTS;
const PolicyProto_SuppressedVisualEffect PolicyProto::PEEK;
const PolicyProto_SuppressedVisualEffect PolicyProto::STATUS_BAR;
const PolicyProto_SuppressedVisualEffect PolicyProto::BADGE;
const PolicyProto_SuppressedVisualEffect PolicyProto::AMBIENT;
const PolicyProto_SuppressedVisualEffect PolicyProto::NOTIFICATION_LIST;
const PolicyProto_SuppressedVisualEffect PolicyProto::SuppressedVisualEffect_MIN;
const PolicyProto_SuppressedVisualEffect PolicyProto::SuppressedVisualEffect_MAX;
const int PolicyProto::SuppressedVisualEffect_ARRAYSIZE;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900
#if !defined(_MSC_VER) || _MSC_VER >= 1900
const int PolicyProto::kPriorityCategoriesFieldNumber;
const int PolicyProto::kPriorityCallSenderFieldNumber;
const int PolicyProto::kPriorityMessageSenderFieldNumber;
const int PolicyProto::kSuppressedVisualEffectsFieldNumber;
#endif  // !defined(_MSC_VER) || _MSC_VER >= 1900

PolicyProto::PolicyProto()
  : ::google::protobuf::MessageLite(), _arena_ptr_(NULL) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:android.app.PolicyProto)
}

void PolicyProto::InitAsDefaultInstance() {
}

PolicyProto::PolicyProto(const PolicyProto& from)
  : ::google::protobuf::MessageLite(),
    _arena_ptr_(NULL) {
  SharedCtor();
  MergeFrom(from);
  // @@protoc_insertion_point(copy_constructor:android.app.PolicyProto)
}

void PolicyProto::SharedCtor() {
  ::google::protobuf::internal::GetEmptyString();
  _cached_size_ = 0;
  _unknown_fields_.UnsafeSetDefault(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  priority_call_sender_ = 0;
  priority_message_sender_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

PolicyProto::~PolicyProto() {
  // @@protoc_insertion_point(destructor:android.app.PolicyProto)
  SharedDtor();
}

void PolicyProto::SharedDtor() {
  _unknown_fields_.DestroyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  #ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  if (this != &default_instance()) {
  #else
  if (this != default_instance_) {
  #endif
  }
}

void PolicyProto::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const PolicyProto& PolicyProto::default_instance() {
#ifdef GOOGLE_PROTOBUF_NO_STATIC_INITIALIZER
  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto();
#else
  if (default_instance_ == NULL) protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fapp_2fnotificationmanager_2eproto();
#endif
  return *default_instance_; /* NOLINT */
}

PolicyProto* PolicyProto::default_instance_ = NULL;

PolicyProto* PolicyProto::New(::google::protobuf::Arena* arena) const {
  PolicyProto* n = new PolicyProto;
  if (arena != NULL) {
    arena->Own(n);
  }
  return n;
}

void PolicyProto::Clear() {
// @@protoc_insertion_point(message_clear_start:android.app.PolicyProto)
#if defined(__clang__)
#define ZR_HELPER_(f) \
  _Pragma("clang diagnostic push") \
  _Pragma("clang diagnostic ignored \"-Winvalid-offsetof\"") \
  __builtin_offsetof(PolicyProto, f) \
  _Pragma("clang diagnostic pop")
#else
#define ZR_HELPER_(f) reinterpret_cast<char*>(\
  &reinterpret_cast<PolicyProto*>(16)->f)
#endif

#define ZR_(first, last) do {\
  ::memset(&(first), 0,\
           ZR_HELPER_(last) - ZR_HELPER_(first) + sizeof(last));\
} while (0)

  ZR_(priority_call_sender_, priority_message_sender_);

#undef ZR_HELPER_
#undef ZR_

  priority_categories_.Clear();
  suppressed_visual_effects_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  _unknown_fields_.ClearToEmptyNoArena(
      &::google::protobuf::internal::GetEmptyStringAlreadyInited());
}

bool PolicyProto::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!GOOGLE_PREDICT_TRUE(EXPRESSION)) goto failure
  ::google::protobuf::uint32 tag;
  ::google::protobuf::io::LazyStringOutputStream unknown_fields_string(
      ::google::protobuf::internal::NewPermanentCallback(
          &MutableUnknownFieldsForPolicyProto, this));
  ::google::protobuf::io::CodedOutputStream unknown_fields_stream(
      &unknown_fields_string, false);
  // @@protoc_insertion_point(parse_start:android.app.PolicyProto)
  for (;;) {
    ::std::pair< ::google::protobuf::uint32, bool> p = input->ReadTagWithCutoff(127);
    tag = p.first;
    if (!p.second) goto handle_unusual;
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // repeated .android.app.PolicyProto.Category priority_categories = 1;
      case 1: {
        if (tag == 8) {
         parse_priority_categories:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          if (::android::app::PolicyProto_Category_IsValid(value)) {
            add_priority_categories(static_cast< ::android::app::PolicyProto_Category >(value));
          } else {
            unknown_fields_stream.WriteVarint32(tag);
            unknown_fields_stream.WriteVarint32(value);
          }
        } else if (tag == 10) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedEnumPreserveUnknowns(
                 input,
                 1,
                 ::android::app::PolicyProto_Category_IsValid,
                 &unknown_fields_stream,
                 this->mutable_priority_categories())));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(8)) goto parse_priority_categories;
        if (input->ExpectTag(16)) goto parse_priority_call_sender;
        break;
      }

      // optional .android.app.PolicyProto.Sender priority_call_sender = 2;
      case 2: {
        if (tag == 16) {
         parse_priority_call_sender:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          if (::android::app::PolicyProto_Sender_IsValid(value)) {
            set_priority_call_sender(static_cast< ::android::app::PolicyProto_Sender >(value));
          } else {
            unknown_fields_stream.WriteVarint32(16);
            unknown_fields_stream.WriteVarint32(value);
          }
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(24)) goto parse_priority_message_sender;
        break;
      }

      // optional .android.app.PolicyProto.Sender priority_message_sender = 3;
      case 3: {
        if (tag == 24) {
         parse_priority_message_sender:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          if (::android::app::PolicyProto_Sender_IsValid(value)) {
            set_priority_message_sender(static_cast< ::android::app::PolicyProto_Sender >(value));
          } else {
            unknown_fields_stream.WriteVarint32(24);
            unknown_fields_stream.WriteVarint32(value);
          }
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(32)) goto parse_suppressed_visual_effects;
        break;
      }

      // repeated .android.app.PolicyProto.SuppressedVisualEffect suppressed_visual_effects = 4;
      case 4: {
        if (tag == 32) {
         parse_suppressed_visual_effects:
          int value;
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   int, ::google::protobuf::internal::WireFormatLite::TYPE_ENUM>(
                 input, &value)));
          if (::android::app::PolicyProto_SuppressedVisualEffect_IsValid(value)) {
            add_suppressed_visual_effects(static_cast< ::android::app::PolicyProto_SuppressedVisualEffect >(value));
          } else {
            unknown_fields_stream.WriteVarint32(tag);
            unknown_fields_stream.WriteVarint32(value);
          }
        } else if (tag == 34) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPackedEnumPreserveUnknowns(
                 input,
                 4,
                 ::android::app::PolicyProto_SuppressedVisualEffect_IsValid,
                 &unknown_fields_stream,
                 this->mutable_suppressed_visual_effects())));
        } else {
          goto handle_unusual;
        }
        if (input->ExpectTag(32)) goto parse_suppressed_visual_effects;
        if (input->ExpectAtEnd()) goto success;
        break;
      }

      default: {
      handle_unusual:
        if (tag == 0 ||
            ::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          goto success;
        }
        DO_(::google::protobuf::internal::WireFormatLite::SkipField(
            input, tag, &unknown_fields_stream));
        break;
      }
    }
  }
success:
  // @@protoc_insertion_point(parse_success:android.app.PolicyProto)
  return true;
failure:
  // @@protoc_insertion_point(parse_failure:android.app.PolicyProto)
  return false;
#undef DO_
}

void PolicyProto::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // @@protoc_insertion_point(serialize_start:android.app.PolicyProto)
  // repeated .android.app.PolicyProto.Category priority_categories = 1;
  for (int i = 0; i < this->priority_categories_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      1, this->priority_categories(i), output);
  }

  // optional .android.app.PolicyProto.Sender priority_call_sender = 2;
  if (has_priority_call_sender()) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      2, this->priority_call_sender(), output);
  }

  // optional .android.app.PolicyProto.Sender priority_message_sender = 3;
  if (has_priority_message_sender()) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      3, this->priority_message_sender(), output);
  }

  // repeated .android.app.PolicyProto.SuppressedVisualEffect suppressed_visual_effects = 4;
  for (int i = 0; i < this->suppressed_visual_effects_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteEnum(
      4, this->suppressed_visual_effects(i), output);
  }

  output->WriteRaw(unknown_fields().data(),
                   static_cast<int>(unknown_fields().size()));
  // @@protoc_insertion_point(serialize_end:android.app.PolicyProto)
}

int PolicyProto::ByteSize() const {
// @@protoc_insertion_point(message_byte_size_start:android.app.PolicyProto)
  int total_size = 0;

  if (_has_bits_[1 / 32] & 6u) {
    // optional .android.app.PolicyProto.Sender priority_call_sender = 2;
    if (has_priority_call_sender()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::EnumSize(this->priority_call_sender());
    }

    // optional .android.app.PolicyProto.Sender priority_message_sender = 3;
    if (has_priority_message_sender()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::EnumSize(this->priority_message_sender());
    }

  }
  // repeated .android.app.PolicyProto.Category priority_categories = 1;
  {
    int data_size = 0;
    for (int i = 0; i < this->priority_categories_size(); i++) {
      data_size += ::google::protobuf::internal::WireFormatLite::EnumSize(
        this->priority_categories(i));
    }
    total_size += 1 * this->priority_categories_size() + data_size;
  }

  // repeated .android.app.PolicyProto.SuppressedVisualEffect suppressed_visual_effects = 4;
  {
    int data_size = 0;
    for (int i = 0; i < this->suppressed_visual_effects_size(); i++) {
      data_size += ::google::protobuf::internal::WireFormatLite::EnumSize(
        this->suppressed_visual_effects(i));
    }
    total_size += 1 * this->suppressed_visual_effects_size() + data_size;
  }

  total_size += unknown_fields().size();

  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void PolicyProto::CheckTypeAndMergeFrom(
    const ::google::protobuf::MessageLite& from) {
  MergeFrom(*::google::protobuf::down_cast<const PolicyProto*>(&from));
}

void PolicyProto::MergeFrom(const PolicyProto& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:android.app.PolicyProto)
  if (GOOGLE_PREDICT_FALSE(&from == this)) MergeFromFail(__LINE__);
  priority_categories_.MergeFrom(from.priority_categories_);
  suppressed_visual_effects_.MergeFrom(from.suppressed_visual_effects_);
  if (from._has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    if (from.has_priority_call_sender()) {
      set_priority_call_sender(from.priority_call_sender());
    }
    if (from.has_priority_message_sender()) {
      set_priority_message_sender(from.priority_message_sender());
    }
  }
  if (!from.unknown_fields().empty()) {
    mutable_unknown_fields()->append(from.unknown_fields());
  }
}

void PolicyProto::CopyFrom(const PolicyProto& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:android.app.PolicyProto)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool PolicyProto::IsInitialized() const {

  return true;
}

void PolicyProto::Swap(PolicyProto* other) {
  if (other == this) return;
  InternalSwap(other);
}
void PolicyProto::InternalSwap(PolicyProto* other) {
  priority_categories_.UnsafeArenaSwap(&other->priority_categories_);
  std::swap(priority_call_sender_, other->priority_call_sender_);
  std::swap(priority_message_sender_, other->priority_message_sender_);
  suppressed_visual_effects_.UnsafeArenaSwap(&other->suppressed_visual_effects_);
  std::swap(_has_bits_[0], other->_has_bits_[0]);
  _unknown_fields_.Swap(&other->_unknown_fields_);
  std::swap(_cached_size_, other->_cached_size_);
}

::std::string PolicyProto::GetTypeName() const {
  return "android.app.PolicyProto";
}

#if PROTOBUF_INLINE_NOT_IN_HEADERS
// PolicyProto

// repeated .android.app.PolicyProto.Category priority_categories = 1;
int PolicyProto::priority_categories_size() const {
  return priority_categories_.size();
}
void PolicyProto::clear_priority_categories() {
  priority_categories_.Clear();
}
 ::android::app::PolicyProto_Category PolicyProto::priority_categories(int index) const {
  // @@protoc_insertion_point(field_get:android.app.PolicyProto.priority_categories)
  return static_cast< ::android::app::PolicyProto_Category >(priority_categories_.Get(index));
}
 void PolicyProto::set_priority_categories(int index, ::android::app::PolicyProto_Category value) {
  assert(::android::app::PolicyProto_Category_IsValid(value));
  priority_categories_.Set(index, value);
  // @@protoc_insertion_point(field_set:android.app.PolicyProto.priority_categories)
}
 void PolicyProto::add_priority_categories(::android::app::PolicyProto_Category value) {
  assert(::android::app::PolicyProto_Category_IsValid(value));
  priority_categories_.Add(value);
  // @@protoc_insertion_point(field_add:android.app.PolicyProto.priority_categories)
}
 const ::google::protobuf::RepeatedField<int>&
PolicyProto::priority_categories() const {
  // @@protoc_insertion_point(field_list:android.app.PolicyProto.priority_categories)
  return priority_categories_;
}
 ::google::protobuf::RepeatedField<int>*
PolicyProto::mutable_priority_categories() {
  // @@protoc_insertion_point(field_mutable_list:android.app.PolicyProto.priority_categories)
  return &priority_categories_;
}

// optional .android.app.PolicyProto.Sender priority_call_sender = 2;
bool PolicyProto::has_priority_call_sender() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
void PolicyProto::set_has_priority_call_sender() {
  _has_bits_[0] |= 0x00000002u;
}
void PolicyProto::clear_has_priority_call_sender() {
  _has_bits_[0] &= ~0x00000002u;
}
void PolicyProto::clear_priority_call_sender() {
  priority_call_sender_ = 0;
  clear_has_priority_call_sender();
}
 ::android::app::PolicyProto_Sender PolicyProto::priority_call_sender() const {
  // @@protoc_insertion_point(field_get:android.app.PolicyProto.priority_call_sender)
  return static_cast< ::android::app::PolicyProto_Sender >(priority_call_sender_);
}
 void PolicyProto::set_priority_call_sender(::android::app::PolicyProto_Sender value) {
  assert(::android::app::PolicyProto_Sender_IsValid(value));
  set_has_priority_call_sender();
  priority_call_sender_ = value;
  // @@protoc_insertion_point(field_set:android.app.PolicyProto.priority_call_sender)
}

// optional .android.app.PolicyProto.Sender priority_message_sender = 3;
bool PolicyProto::has_priority_message_sender() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
void PolicyProto::set_has_priority_message_sender() {
  _has_bits_[0] |= 0x00000004u;
}
void PolicyProto::clear_has_priority_message_sender() {
  _has_bits_[0] &= ~0x00000004u;
}
void PolicyProto::clear_priority_message_sender() {
  priority_message_sender_ = 0;
  clear_has_priority_message_sender();
}
 ::android::app::PolicyProto_Sender PolicyProto::priority_message_sender() const {
  // @@protoc_insertion_point(field_get:android.app.PolicyProto.priority_message_sender)
  return static_cast< ::android::app::PolicyProto_Sender >(priority_message_sender_);
}
 void PolicyProto::set_priority_message_sender(::android::app::PolicyProto_Sender value) {
  assert(::android::app::PolicyProto_Sender_IsValid(value));
  set_has_priority_message_sender();
  priority_message_sender_ = value;
  // @@protoc_insertion_point(field_set:android.app.PolicyProto.priority_message_sender)
}

// repeated .android.app.PolicyProto.SuppressedVisualEffect suppressed_visual_effects = 4;
int PolicyProto::suppressed_visual_effects_size() const {
  return suppressed_visual_effects_.size();
}
void PolicyProto::clear_suppressed_visual_effects() {
  suppressed_visual_effects_.Clear();
}
 ::android::app::PolicyProto_SuppressedVisualEffect PolicyProto::suppressed_visual_effects(int index) const {
  // @@protoc_insertion_point(field_get:android.app.PolicyProto.suppressed_visual_effects)
  return static_cast< ::android::app::PolicyProto_SuppressedVisualEffect >(suppressed_visual_effects_.Get(index));
}
 void PolicyProto::set_suppressed_visual_effects(int index, ::android::app::PolicyProto_SuppressedVisualEffect value) {
  assert(::android::app::PolicyProto_SuppressedVisualEffect_IsValid(value));
  suppressed_visual_effects_.Set(index, value);
  // @@protoc_insertion_point(field_set:android.app.PolicyProto.suppressed_visual_effects)
}
 void PolicyProto::add_suppressed_visual_effects(::android::app::PolicyProto_SuppressedVisualEffect value) {
  assert(::android::app::PolicyProto_SuppressedVisualEffect_IsValid(value));
  suppressed_visual_effects_.Add(value);
  // @@protoc_insertion_point(field_add:android.app.PolicyProto.suppressed_visual_effects)
}
 const ::google::protobuf::RepeatedField<int>&
PolicyProto::suppressed_visual_effects() const {
  // @@protoc_insertion_point(field_list:android.app.PolicyProto.suppressed_visual_effects)
  return suppressed_visual_effects_;
}
 ::google::protobuf::RepeatedField<int>*
PolicyProto::mutable_suppressed_visual_effects() {
  // @@protoc_insertion_point(field_mutable_list:android.app.PolicyProto.suppressed_visual_effects)
  return &suppressed_visual_effects_;
}

#endif  // PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace app
}  // namespace android

// @@protoc_insertion_point(global_scope)
