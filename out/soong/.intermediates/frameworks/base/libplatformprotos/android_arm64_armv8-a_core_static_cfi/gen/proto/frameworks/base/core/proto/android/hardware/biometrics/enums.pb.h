// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/hardware/biometrics/enums.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fhardware_2fbiometrics_2fenums_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fhardware_2fbiometrics_2fenums_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_util.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace hardware {
namespace biometrics {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fhardware_2fbiometrics_2fenums_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fhardware_2fbiometrics_2fenums_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fhardware_2fbiometrics_2fenums_2eproto();


enum ModalityEnum {
  MODALITY_UNKNOWN = 0,
  MODALITY_FINGERPRINT = 1,
  MODALITY_IRIS = 2,
  MODALITY_FACE = 4
};
bool ModalityEnum_IsValid(int value);
const ModalityEnum ModalityEnum_MIN = MODALITY_UNKNOWN;
const ModalityEnum ModalityEnum_MAX = MODALITY_FACE;
const int ModalityEnum_ARRAYSIZE = ModalityEnum_MAX + 1;

enum ClientEnum {
  CLIENT_UNKNOWN = 0,
  CLIENT_KEYGUARD = 1,
  CLIENT_BIOMETRIC_PROMPT = 2,
  CLIENT_FINGERPRINT_MANAGER = 3
};
bool ClientEnum_IsValid(int value);
const ClientEnum ClientEnum_MIN = CLIENT_UNKNOWN;
const ClientEnum ClientEnum_MAX = CLIENT_FINGERPRINT_MANAGER;
const int ClientEnum_ARRAYSIZE = ClientEnum_MAX + 1;

enum ActionEnum {
  ACTION_UNKNOWN = 0,
  ACTION_ENROLL = 1,
  ACTION_AUTHENTICATE = 2,
  ACTION_ENUMERATE = 3,
  ACTION_REMOVE = 4
};
bool ActionEnum_IsValid(int value);
const ActionEnum ActionEnum_MIN = ACTION_UNKNOWN;
const ActionEnum ActionEnum_MAX = ACTION_REMOVE;
const int ActionEnum_ARRAYSIZE = ActionEnum_MAX + 1;

enum IssueEnum {
  ISSUE_UNKNOWN = 0,
  ISSUE_HAL_DEATH = 1,
  ISSUE_UNKNOWN_TEMPLATE_ENROLLED_FRAMEWORK = 2,
  ISSUE_UNKNOWN_TEMPLATE_ENROLLED_HAL = 3,
  ISSUE_CANCEL_TIMED_OUT = 4
};
bool IssueEnum_IsValid(int value);
const IssueEnum IssueEnum_MIN = ISSUE_UNKNOWN;
const IssueEnum IssueEnum_MAX = ISSUE_CANCEL_TIMED_OUT;
const int IssueEnum_ARRAYSIZE = IssueEnum_MAX + 1;

// ===================================================================


// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace biometrics
}  // namespace hardware
}  // namespace android

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::android::hardware::biometrics::ModalityEnum> : ::google::protobuf::internal::true_type {};
template <> struct is_proto_enum< ::android::hardware::biometrics::ClientEnum> : ::google::protobuf::internal::true_type {};
template <> struct is_proto_enum< ::android::hardware::biometrics::ActionEnum> : ::google::protobuf::internal::true_type {};
template <> struct is_proto_enum< ::android::hardware::biometrics::IssueEnum> : ::google::protobuf::internal::true_type {};

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fhardware_2fbiometrics_2fenums_2eproto__INCLUDED
