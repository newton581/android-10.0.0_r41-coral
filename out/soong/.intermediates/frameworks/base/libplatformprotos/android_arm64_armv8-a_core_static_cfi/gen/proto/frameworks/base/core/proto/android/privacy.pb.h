// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/privacy.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message_lite.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_util.h>
#include <google/protobuf/descriptor.pb.h>
// @@protoc_insertion_point(includes)

namespace android {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();

class PrivacyFlags;

enum Destination {
  DEST_LOCAL = 0,
  DEST_EXPLICIT = 100,
  DEST_AUTOMATIC = 200,
  DEST_UNSET = 255
};
bool Destination_IsValid(int value);
const Destination Destination_MIN = DEST_LOCAL;
const Destination Destination_MAX = DEST_UNSET;
const int Destination_ARRAYSIZE = Destination_MAX + 1;

// ===================================================================

class PrivacyFlags : public ::google::protobuf::MessageLite {
 public:
  PrivacyFlags();
  virtual ~PrivacyFlags();

  PrivacyFlags(const PrivacyFlags& from);

  inline PrivacyFlags& operator=(const PrivacyFlags& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::std::string& unknown_fields() const {
    return _unknown_fields_.GetNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  inline ::std::string* mutable_unknown_fields() {
    return _unknown_fields_.MutableNoArena(
        &::google::protobuf::internal::GetEmptyStringAlreadyInited());
  }

  static const PrivacyFlags& default_instance();

  void Swap(PrivacyFlags* other);

  // implements Message ----------------------------------------------

  inline PrivacyFlags* New() const { return New(NULL); }

  PrivacyFlags* New(::google::protobuf::Arena* arena) const;
  void CheckTypeAndMergeFrom(const ::google::protobuf::MessageLite& from);
  void CopyFrom(const PrivacyFlags& from);
  void MergeFrom(const PrivacyFlags& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  void DiscardUnknownFields();
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(PrivacyFlags* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _arena_ptr_;
  }
  inline ::google::protobuf::Arena* MaybeArenaPtr() const {
    return _arena_ptr_;
  }
  public:

  ::std::string GetTypeName() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional .android.Destination dest = 1 [default = DEST_UNSET];
  bool has_dest() const;
  void clear_dest();
  static const int kDestFieldNumber = 1;
  ::android::Destination dest() const;
  void set_dest(::android::Destination value);

  // repeated string patterns = 2;
  int patterns_size() const;
  void clear_patterns();
  static const int kPatternsFieldNumber = 2;
  const ::std::string& patterns(int index) const;
  ::std::string* mutable_patterns(int index);
  void set_patterns(int index, const ::std::string& value);
  void set_patterns(int index, const char* value);
  void set_patterns(int index, const char* value, size_t size);
  ::std::string* add_patterns();
  void add_patterns(const ::std::string& value);
  void add_patterns(const char* value);
  void add_patterns(const char* value, size_t size);
  const ::google::protobuf::RepeatedPtrField< ::std::string>& patterns() const;
  ::google::protobuf::RepeatedPtrField< ::std::string>* mutable_patterns();

  // @@protoc_insertion_point(class_scope:android.PrivacyFlags)
 private:
  inline void set_has_dest();
  inline void clear_has_dest();

  ::google::protobuf::internal::ArenaStringPtr _unknown_fields_;
  ::google::protobuf::Arena* _arena_ptr_;

  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::RepeatedPtrField< ::std::string> patterns_;
  int dest_;
  friend void  protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
  friend void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();
  friend void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto();

  void InitAsDefaultInstance();
  static PrivacyFlags* default_instance_;
};
// ===================================================================

static const int kPrivacyFieldNumber = 102672883;
extern ::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::FieldOptions,
    ::google::protobuf::internal::MessageTypeTraits< ::android::PrivacyFlags >, 11, false >
  privacy;
static const int kMsgPrivacyFieldNumber = 102672883;
extern ::google::protobuf::internal::ExtensionIdentifier< ::google::protobuf::MessageOptions,
    ::google::protobuf::internal::MessageTypeTraits< ::android::PrivacyFlags >, 11, false >
  msg_privacy;

// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// PrivacyFlags

// optional .android.Destination dest = 1 [default = DEST_UNSET];
inline bool PrivacyFlags::has_dest() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void PrivacyFlags::set_has_dest() {
  _has_bits_[0] |= 0x00000001u;
}
inline void PrivacyFlags::clear_has_dest() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void PrivacyFlags::clear_dest() {
  dest_ = 255;
  clear_has_dest();
}
inline ::android::Destination PrivacyFlags::dest() const {
  // @@protoc_insertion_point(field_get:android.PrivacyFlags.dest)
  return static_cast< ::android::Destination >(dest_);
}
inline void PrivacyFlags::set_dest(::android::Destination value) {
  assert(::android::Destination_IsValid(value));
  set_has_dest();
  dest_ = value;
  // @@protoc_insertion_point(field_set:android.PrivacyFlags.dest)
}

// repeated string patterns = 2;
inline int PrivacyFlags::patterns_size() const {
  return patterns_.size();
}
inline void PrivacyFlags::clear_patterns() {
  patterns_.Clear();
}
inline const ::std::string& PrivacyFlags::patterns(int index) const {
  // @@protoc_insertion_point(field_get:android.PrivacyFlags.patterns)
  return patterns_.Get(index);
}
inline ::std::string* PrivacyFlags::mutable_patterns(int index) {
  // @@protoc_insertion_point(field_mutable:android.PrivacyFlags.patterns)
  return patterns_.Mutable(index);
}
inline void PrivacyFlags::set_patterns(int index, const ::std::string& value) {
  // @@protoc_insertion_point(field_set:android.PrivacyFlags.patterns)
  patterns_.Mutable(index)->assign(value);
}
inline void PrivacyFlags::set_patterns(int index, const char* value) {
  patterns_.Mutable(index)->assign(value);
  // @@protoc_insertion_point(field_set_char:android.PrivacyFlags.patterns)
}
inline void PrivacyFlags::set_patterns(int index, const char* value, size_t size) {
  patterns_.Mutable(index)->assign(
    reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:android.PrivacyFlags.patterns)
}
inline ::std::string* PrivacyFlags::add_patterns() {
  // @@protoc_insertion_point(field_add_mutable:android.PrivacyFlags.patterns)
  return patterns_.Add();
}
inline void PrivacyFlags::add_patterns(const ::std::string& value) {
  patterns_.Add()->assign(value);
  // @@protoc_insertion_point(field_add:android.PrivacyFlags.patterns)
}
inline void PrivacyFlags::add_patterns(const char* value) {
  patterns_.Add()->assign(value);
  // @@protoc_insertion_point(field_add_char:android.PrivacyFlags.patterns)
}
inline void PrivacyFlags::add_patterns(const char* value, size_t size) {
  patterns_.Add()->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_add_pointer:android.PrivacyFlags.patterns)
}
inline const ::google::protobuf::RepeatedPtrField< ::std::string>&
PrivacyFlags::patterns() const {
  // @@protoc_insertion_point(field_list:android.PrivacyFlags.patterns)
  return patterns_;
}
inline ::google::protobuf::RepeatedPtrField< ::std::string>*
PrivacyFlags::mutable_patterns() {
  // @@protoc_insertion_point(field_mutable_list:android.PrivacyFlags.patterns)
  return &patterns_;
}

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace android

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::android::Destination> : ::google::protobuf::internal::true_type {};

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fprivacy_2eproto__INCLUDED
