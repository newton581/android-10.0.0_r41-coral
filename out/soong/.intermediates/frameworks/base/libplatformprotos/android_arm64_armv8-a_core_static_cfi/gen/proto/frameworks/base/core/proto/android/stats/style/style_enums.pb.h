// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: frameworks/base/core/proto/android/stats/style/style_enums.proto

#ifndef PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fstyle_2fstyle_5fenums_2eproto__INCLUDED
#define PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fstyle_2fstyle_5fenums_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_util.h>
// @@protoc_insertion_point(includes)

namespace android {
namespace stats {
namespace style {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fstyle_2fstyle_5fenums_2eproto();
void protobuf_AssignDesc_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fstyle_2fstyle_5fenums_2eproto();
void protobuf_ShutdownFile_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fstyle_2fstyle_5fenums_2eproto();


enum Action {
  DEFAULT_ACTION = 0,
  ONRESUME = 1,
  ONSTOP = 2,
  PICKER_SELECT = 3,
  PICKER_APPLIED = 4,
  WALLPAPER_OPEN_CATEGORY = 5,
  WALLPAPER_SELECT = 6,
  WALLPAPER_APPLIED = 7,
  WALLPAPER_EXPLORE = 8,
  WALLPAPER_DOWNLOAD = 9,
  WALLPAPER_REMOVE = 10,
  LIVE_WALLPAPER_DOWNLOAD_SUCCESS = 11,
  LIVE_WALLPAPER_DOWNLOAD_FAILED = 12,
  LIVE_WALLPAPER_DOWNLOAD_CANCELLED = 13,
  LIVE_WALLPAPER_DELETE_SUCCESS = 14,
  LIVE_WALLPAPER_DELETE_FAILED = 15,
  LIVE_WALLPAPER_APPLIED = 16
};
bool Action_IsValid(int value);
const Action Action_MIN = DEFAULT_ACTION;
const Action Action_MAX = LIVE_WALLPAPER_APPLIED;
const int Action_ARRAYSIZE = Action_MAX + 1;

enum LocationPreference {
  LOCATION_PREFERENCE_UNSPECIFIED = 0,
  LOCATION_UNAVAILABLE = 1,
  LOCATION_CURRENT = 2,
  LOCATION_MANUAL = 3
};
bool LocationPreference_IsValid(int value);
const LocationPreference LocationPreference_MIN = LOCATION_PREFERENCE_UNSPECIFIED;
const LocationPreference LocationPreference_MAX = LOCATION_MANUAL;
const int LocationPreference_ARRAYSIZE = LocationPreference_MAX + 1;

// ===================================================================


// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS

// @@protoc_insertion_point(namespace_scope)

}  // namespace style
}  // namespace stats
}  // namespace android

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::android::stats::style::Action> : ::google::protobuf::internal::true_type {};
template <> struct is_proto_enum< ::android::stats::style::LocationPreference> : ::google::protobuf::internal::true_type {};

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_frameworks_2fbase_2fcore_2fproto_2fandroid_2fstats_2fstyle_2fstyle_5fenums_2eproto__INCLUDED
