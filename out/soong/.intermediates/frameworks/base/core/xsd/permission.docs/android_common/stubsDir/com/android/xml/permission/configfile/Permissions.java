
package com.android.xml.permission.configfile;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Permissions {

public Permissions() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.Group> getGroup_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.Permission> getPermission_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.AssignPermission> getAssignPermission_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.SplitPermission> getSplitPermission_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.Library> getLibrary_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.Feature> getFeature_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.UnavailableFeature> getUnavailableFeature_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.AllowInPowerSaveExceptIdle> getAllowInPowerSaveExceptIdle_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.AllowInPowerSave> getAllowInPowerSave_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.AllowInDataUsageSave> getAllowInDataUsageSave_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.AllowUnthrottledLocation> getAllowUnthrottledLocation_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.AllowIgnoreLocationSettings> getAllowIgnoreLocationSettings_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.AllowImplicitBroadcast> getAllowImplicitBroadcast_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.AppLink> getAppLink_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.SystemUserWhitelistedApp> getSystemUserWhitelistedApp_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.SystemUserBlacklistedApp> getSystemUserBlacklistedApp_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.DefaultEnabledVrApp> getDefaultEnabledVrApp_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.BackupTransportWhitelistedService> getBackupTransportWhitelistedService_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.DisabledUntilUsedPreinstalledCarrierAssociatedApp> getDisabledUntilUsedPreinstalledCarrierAssociatedApp_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.DisabledUntilUsedPreinstalledCarrierApp> getDisabledUntilUsedPreinstalledCarrierApp_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.PrivappPermissions> getPrivappPermissions_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.OemPermissions> getOemPermissions_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.HiddenApiWhitelistedApp> getHiddenApiWhitelistedApp_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.AllowAssociation> getAllowAssociation_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.BugreportWhitelisted> getBugreportWhitelisted_optional() { throw new RuntimeException("Stub!"); }
}

