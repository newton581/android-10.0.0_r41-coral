
package com.android.xml.permission.configfile;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class OemPermissions {

public OemPermissions() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.OemPermissions.Permission> getPermission() { throw new RuntimeException("Stub!"); }

public java.util.List<com.android.xml.permission.configfile.OemPermissions.DenyPermission> getDenyPermission() { throw new RuntimeException("Stub!"); }

public java.lang.String get_package() { throw new RuntimeException("Stub!"); }

public void set_package(java.lang.String _package) { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class DenyPermission {

public DenyPermission() { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }
}

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class Permission {

public Permission() { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }
}

}

