
package media.profiles;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AudioEncoderCap {

public AudioEncoderCap() { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public boolean getEnabled() { throw new RuntimeException("Stub!"); }

public void setEnabled(boolean enabled) { throw new RuntimeException("Stub!"); }

public int getMinBitRate() { throw new RuntimeException("Stub!"); }

public void setMinBitRate(int minBitRate) { throw new RuntimeException("Stub!"); }

public int getMaxBitRate() { throw new RuntimeException("Stub!"); }

public void setMaxBitRate(int maxBitRate) { throw new RuntimeException("Stub!"); }

public int getMinSampleRate() { throw new RuntimeException("Stub!"); }

public void setMinSampleRate(int minSampleRate) { throw new RuntimeException("Stub!"); }

public int getMaxSampleRate() { throw new RuntimeException("Stub!"); }

public void setMaxSampleRate(int maxSampleRate) { throw new RuntimeException("Stub!"); }

public int getMinChannels() { throw new RuntimeException("Stub!"); }

public void setMinChannels(int minChannels) { throw new RuntimeException("Stub!"); }

public int getMaxChannels() { throw new RuntimeException("Stub!"); }

public void setMaxChannels(int maxChannels) { throw new RuntimeException("Stub!"); }
}

