
package media.profiles;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class EncoderProfile {

public EncoderProfile() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.Video> getVideo() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.Audio> getAudio() { throw new RuntimeException("Stub!"); }

public java.lang.String getQuality() { throw new RuntimeException("Stub!"); }

public void setQuality(java.lang.String quality) { throw new RuntimeException("Stub!"); }

public java.lang.String getFileFormat() { throw new RuntimeException("Stub!"); }

public void setFileFormat(java.lang.String fileFormat) { throw new RuntimeException("Stub!"); }

public int getDuration() { throw new RuntimeException("Stub!"); }

public void setDuration(int duration) { throw new RuntimeException("Stub!"); }
}

