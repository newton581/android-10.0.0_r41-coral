
package media.profiles;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class CamcorderProfiles {

public CamcorderProfiles() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.EncoderProfile> getEncoderProfile_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.CamcorderProfiles.ImageEncodingOptional> getImageEncoding_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.CamcorderProfiles.ImageDecodingOptional> getImageDecoding_optional() { throw new RuntimeException("Stub!"); }

public int getCameraId() { throw new RuntimeException("Stub!"); }

public void setCameraId(int cameraId) { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ImageDecodingOptional {

public ImageDecodingOptional() { throw new RuntimeException("Stub!"); }

public int getMemCap() { throw new RuntimeException("Stub!"); }

public void setMemCap(int memCap) { throw new RuntimeException("Stub!"); }
}

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ImageEncodingOptional {

public ImageEncodingOptional() { throw new RuntimeException("Stub!"); }

public int getQuality() { throw new RuntimeException("Stub!"); }

public void setQuality(int quality) { throw new RuntimeException("Stub!"); }
}

}

