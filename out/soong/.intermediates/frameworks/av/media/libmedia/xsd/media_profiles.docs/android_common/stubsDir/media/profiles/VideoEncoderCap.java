
package media.profiles;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class VideoEncoderCap {

public VideoEncoderCap() { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public boolean getEnabled() { throw new RuntimeException("Stub!"); }

public void setEnabled(boolean enabled) { throw new RuntimeException("Stub!"); }

public int getMinBitRate() { throw new RuntimeException("Stub!"); }

public void setMinBitRate(int minBitRate) { throw new RuntimeException("Stub!"); }

public int getMaxBitRate() { throw new RuntimeException("Stub!"); }

public void setMaxBitRate(int maxBitRate) { throw new RuntimeException("Stub!"); }

public int getMinFrameWidth() { throw new RuntimeException("Stub!"); }

public void setMinFrameWidth(int minFrameWidth) { throw new RuntimeException("Stub!"); }

public int getMaxFrameWidth() { throw new RuntimeException("Stub!"); }

public void setMaxFrameWidth(int maxFrameWidth) { throw new RuntimeException("Stub!"); }

public int getMinFrameHeight() { throw new RuntimeException("Stub!"); }

public void setMinFrameHeight(int minFrameHeight) { throw new RuntimeException("Stub!"); }

public int getMaxFrameHeight() { throw new RuntimeException("Stub!"); }

public void setMaxFrameHeight(int maxFrameHeight) { throw new RuntimeException("Stub!"); }

public int getMinFrameRate() { throw new RuntimeException("Stub!"); }

public void setMinFrameRate(int minFrameRate) { throw new RuntimeException("Stub!"); }

public int getMaxFrameRate() { throw new RuntimeException("Stub!"); }

public void setMaxFrameRate(int maxFrameRate) { throw new RuntimeException("Stub!"); }
}

