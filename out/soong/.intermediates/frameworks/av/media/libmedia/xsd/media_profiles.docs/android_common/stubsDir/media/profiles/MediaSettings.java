
package media.profiles;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MediaSettings {

public MediaSettings() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.CamcorderProfiles> getCamcorderProfiles() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.MediaSettings.EncoderOutputFileFormat> getEncoderOutputFileFormat() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.VideoEncoderCap> getVideoEncoderCap() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.AudioEncoderCap> getAudioEncoderCap() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.VideoDecoderCap> getVideoDecoderCap() { throw new RuntimeException("Stub!"); }

public java.util.List<media.profiles.AudioDecoderCap> getAudioDecoderCap() { throw new RuntimeException("Stub!"); }
@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class EncoderOutputFileFormat {

public EncoderOutputFileFormat() { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }
}

}

