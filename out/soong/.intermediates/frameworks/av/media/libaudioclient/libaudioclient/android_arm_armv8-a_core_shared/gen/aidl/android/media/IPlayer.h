#ifndef AIDL_GENERATED_ANDROID_MEDIA_I_PLAYER_H_
#define AIDL_GENERATED_ANDROID_MEDIA_I_PLAYER_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <cstdint>
#include <media/VolumeShaper.h>
#include <utils/StrongPointer.h>

namespace android {

namespace media {

class IPlayer : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(Player)
  virtual ::android::binder::Status start() = 0;
  virtual ::android::binder::Status pause() = 0;
  virtual ::android::binder::Status stop() = 0;
  virtual ::android::binder::Status setVolume(float vol) = 0;
  virtual ::android::binder::Status setPan(float pan) = 0;
  virtual ::android::binder::Status setStartDelayMs(int32_t delayMs) = 0;
  virtual ::android::binder::Status applyVolumeShaper(const ::android::media::VolumeShaper::Configuration& configuration, const ::android::media::VolumeShaper::Operation& operation) = 0;
};  // class IPlayer

class IPlayerDefault : public IPlayer {
public:
  ::android::IBinder* onAsBinder() override;
  ::android::binder::Status start() override;
  ::android::binder::Status pause() override;
  ::android::binder::Status stop() override;
  ::android::binder::Status setVolume(float vol) override;
  ::android::binder::Status setPan(float pan) override;
  ::android::binder::Status setStartDelayMs(int32_t delayMs) override;
  ::android::binder::Status applyVolumeShaper(const ::android::media::VolumeShaper::Configuration& configuration, const ::android::media::VolumeShaper::Operation& operation) override;

};

}  // namespace media

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_MEDIA_I_PLAYER_H_
