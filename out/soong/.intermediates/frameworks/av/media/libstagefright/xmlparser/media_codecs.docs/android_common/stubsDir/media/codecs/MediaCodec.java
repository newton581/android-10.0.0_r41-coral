
package media.codecs;


@SuppressWarnings({"unchecked", "deprecation", "all"})
public class MediaCodec {

public MediaCodec() { throw new RuntimeException("Stub!"); }

public java.util.List<media.codecs.Quirk> getQuirk_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<media.codecs.Quirk> getAttribute_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<media.codecs.Type> getType_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<media.codecs.Alias> getAlias_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<media.codecs.Limit> getLimit_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<media.codecs.Feature> getFeature_optional() { throw new RuntimeException("Stub!"); }

public java.util.List<media.codecs.Variant> getVariant_optional() { throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

public void setName(java.lang.String name) { throw new RuntimeException("Stub!"); }

public java.lang.String getType() { throw new RuntimeException("Stub!"); }

public void setType(java.lang.String type) { throw new RuntimeException("Stub!"); }

public java.lang.String getUpdate() { throw new RuntimeException("Stub!"); }

public void setUpdate(java.lang.String update) { throw new RuntimeException("Stub!"); }

public java.lang.String getRank() { throw new RuntimeException("Stub!"); }

public void setRank(java.lang.String rank) { throw new RuntimeException("Stub!"); }

public java.lang.String getDomain() { throw new RuntimeException("Stub!"); }

public void setDomain(java.lang.String domain) { throw new RuntimeException("Stub!"); }

public java.lang.String getVariant() { throw new RuntimeException("Stub!"); }

public void setVariant(java.lang.String variant) { throw new RuntimeException("Stub!"); }

public java.lang.String getEnabled() { throw new RuntimeException("Stub!"); }

public void setEnabled(java.lang.String enabled) { throw new RuntimeException("Stub!"); }
}

