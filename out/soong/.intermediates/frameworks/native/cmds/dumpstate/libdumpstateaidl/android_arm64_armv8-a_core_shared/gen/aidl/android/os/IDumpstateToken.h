#ifndef AIDL_GENERATED_ANDROID_OS_I_DUMPSTATE_TOKEN_H_
#define AIDL_GENERATED_ANDROID_OS_I_DUMPSTATE_TOKEN_H_

#include <binder/IBinder.h>
#include <binder/IInterface.h>
#include <binder/Status.h>
#include <utils/StrongPointer.h>

namespace android {

namespace os {

class IDumpstateToken : public ::android::IInterface {
public:
  DECLARE_META_INTERFACE(DumpstateToken)
};  // class IDumpstateToken

class IDumpstateTokenDefault : public IDumpstateToken {
public:
  ::android::IBinder* onAsBinder() override;
  
};

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_I_DUMPSTATE_TOKEN_H_
