#ifndef AIDL_GENERATED_ANDROID_OS_BN_INSTALLD_H_
#define AIDL_GENERATED_ANDROID_OS_BN_INSTALLD_H_

#include <binder/IInterface.h>
#include <android/os/IInstalld.h>

namespace android {

namespace os {

class BnInstalld : public ::android::BnInterface<IInstalld> {
public:
  ::android::status_t onTransact(uint32_t _aidl_code, const ::android::Parcel& _aidl_data, ::android::Parcel* _aidl_reply, uint32_t _aidl_flags) override;
};  // class BnInstalld

}  // namespace os

}  // namespace android

#endif  // AIDL_GENERATED_ANDROID_OS_BN_INSTALLD_H_
