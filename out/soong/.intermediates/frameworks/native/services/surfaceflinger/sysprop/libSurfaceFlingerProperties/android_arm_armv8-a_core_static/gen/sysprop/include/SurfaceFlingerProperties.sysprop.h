// Generated by the sysprop generator. DO NOT EDIT!

#pragma once

#include <cstdint>
#include <optional>
#include <string>
#include <vector>

namespace android::sysprop::SurfaceFlingerProperties {

std::optional<std::int64_t> vsync_event_phase_offset_ns();

std::optional<std::int64_t> vsync_sf_event_phase_offset_ns();

std::optional<bool> use_context_priority();

std::optional<std::int64_t> max_frame_buffer_acquired_buffers();

std::optional<bool> has_wide_color_display();

std::optional<bool> running_without_sync_framework();

std::optional<bool> has_HDR_display();

std::optional<std::int64_t> present_time_offset_from_vsync_ns();

std::optional<bool> force_hwc_copy_for_virtual_displays();

std::optional<std::int64_t> max_virtual_display_dimension();

std::optional<bool> use_vr_flinger();

std::optional<bool> start_graphics_allocator_service();

enum class primary_display_orientation_values {
    ORIENTATION_0,
    ORIENTATION_90,
    ORIENTATION_180,
    ORIENTATION_270,
};

std::optional<primary_display_orientation_values> primary_display_orientation();

std::optional<bool> use_color_management();

std::optional<std::int64_t> default_composition_dataspace();

std::optional<std::int32_t> default_composition_pixel_format();

std::optional<std::int64_t> wcg_composition_dataspace();

std::optional<std::int32_t> wcg_composition_pixel_format();

std::optional<std::int64_t> color_space_agnostic_dataspace();

std::vector<std::optional<double>> display_primary_red();

std::vector<std::optional<double>> display_primary_green();

std::vector<std::optional<double>> display_primary_blue();

std::vector<std::optional<double>> display_primary_white();

std::optional<bool> refresh_rate_switching();

std::optional<std::int32_t> set_idle_timer_ms();

std::optional<std::int32_t> set_touch_timer_ms();

std::optional<std::int32_t> set_display_power_timer_ms();

std::optional<bool> use_smart_90_for_video();

std::optional<bool> enable_protected_contents();

std::optional<bool> support_kernel_idle_timer();

}  // namespace android::sysprop::SurfaceFlingerProperties
