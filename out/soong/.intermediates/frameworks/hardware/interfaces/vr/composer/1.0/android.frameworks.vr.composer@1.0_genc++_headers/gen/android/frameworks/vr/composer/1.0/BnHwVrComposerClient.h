#ifndef HIDL_GENERATED_ANDROID_FRAMEWORKS_VR_COMPOSER_V1_0_BNHWVRCOMPOSERCLIENT_H
#define HIDL_GENERATED_ANDROID_FRAMEWORKS_VR_COMPOSER_V1_0_BNHWVRCOMPOSERCLIENT_H

#include <android/frameworks/vr/composer/1.0/IHwVrComposerClient.h>

namespace android {
namespace frameworks {
namespace vr {
namespace composer {
namespace V1_0 {

struct BnHwVrComposerClient : public ::android::hidl::base::V1_0::BnHwBase {
    explicit BnHwVrComposerClient(const ::android::sp<IVrComposerClient> &_hidl_impl);
    explicit BnHwVrComposerClient(const ::android::sp<IVrComposerClient> &_hidl_impl, const std::string& HidlInstrumentor_package, const std::string& HidlInstrumentor_interface);

    virtual ~BnHwVrComposerClient();

    ::android::status_t onTransact(
            uint32_t _hidl_code,
            const ::android::hardware::Parcel &_hidl_data,
            ::android::hardware::Parcel *_hidl_reply,
            uint32_t _hidl_flags = 0,
            TransactCallback _hidl_cb = nullptr) override;


    /**
     * The pure class is what this class wraps.
     */
    typedef IVrComposerClient Pure;

    /**
     * Type tag for use in template logic that indicates this is a 'native' class.
     */
    typedef android::hardware::details::bnhw_tag _hidl_tag;

    ::android::sp<IVrComposerClient> getImpl() { return _hidl_mImpl; }

private:
    // Methods from ::android::hardware::graphics::composer::V2_1::IComposerClient follow.

    // Methods from ::android::hidl::base::V1_0::IBase follow.
    ::android::hardware::Return<void> ping();
    using getDebugInfo_cb = ::android::hidl::base::V1_0::IBase::getDebugInfo_cb;
    ::android::hardware::Return<void> getDebugInfo(getDebugInfo_cb _hidl_cb);

    ::android::sp<IVrComposerClient> _hidl_mImpl;
};

}  // namespace V1_0
}  // namespace composer
}  // namespace vr
}  // namespace frameworks
}  // namespace android

#endif  // HIDL_GENERATED_ANDROID_FRAMEWORKS_VR_COMPOSER_V1_0_BNHWVRCOMPOSERCLIENT_H
