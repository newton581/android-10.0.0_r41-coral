#ifndef HIDL_GENERATED_ANDROID_FRAMEWORKS_VR_COMPOSER_V1_0_IVRCOMPOSERCLIENT_H
#define HIDL_GENERATED_ANDROID_FRAMEWORKS_VR_COMPOSER_V1_0_IVRCOMPOSERCLIENT_H

#include <android/hardware/graphics/common/1.0/types.h>
#include <android/hardware/graphics/composer/2.1/IComposerClient.h>

#include <android/hidl/manager/1.0/IServiceNotification.h>

#include <hidl/HidlSupport.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <utils/NativeHandle.h>
#include <utils/misc.h>

namespace android {
namespace frameworks {
namespace vr {
namespace composer {
namespace V1_0 {

struct IVrComposerClient : public ::android::hardware::graphics::composer::V2_1::IComposerClient {
    /**
     * Type tag for use in template logic that indicates this is a 'pure' class.
     */
    typedef android::hardware::details::i_tag _hidl_tag;

    /**
     * Fully qualified interface name: "android.frameworks.vr.composer@1.0::IVrComposerClient"
     */
    static const char* descriptor;

    // Forward declaration for forward reference support:
    struct BufferMetadata;
    enum class VrCommand : int32_t;

    struct BufferMetadata final {
        uint32_t width __attribute__ ((aligned(4)));
        uint32_t height __attribute__ ((aligned(4)));
        uint32_t stride __attribute__ ((aligned(4)));
        uint32_t layerCount __attribute__ ((aligned(4)));
        ::android::hardware::graphics::common::V1_0::PixelFormat format __attribute__ ((aligned(4)));
        ::android::hardware::hidl_bitfield<::android::hardware::graphics::common::V1_0::BufferUsage> usage __attribute__ ((aligned(8)));
    };

    static_assert(offsetof(::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata, width) == 0, "wrong offset");
    static_assert(offsetof(::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata, height) == 4, "wrong offset");
    static_assert(offsetof(::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata, stride) == 8, "wrong offset");
    static_assert(offsetof(::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata, layerCount) == 12, "wrong offset");
    static_assert(offsetof(::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata, format) == 16, "wrong offset");
    static_assert(offsetof(::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata, usage) == 24, "wrong offset");
    static_assert(sizeof(::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata) == 32, "wrong size");
    static_assert(__alignof(::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata) == 8, "wrong alignment");

    enum class VrCommand : int32_t {
        LENGTH_MASK = 65535 /* 0xffff */,
        OPCODE_SHIFT = 16,
        OPCODE_MASK = -65536 /* (0xffff << OPCODE_SHIFT) */,
        /**
         * special commands
         */
        SELECT_DISPLAY = 0 /* (0x000 << OPCODE_SHIFT) */,
        SELECT_LAYER = 65536 /* (0x001 << OPCODE_SHIFT) */,
        /**
         * value commands (for return values)
         */
        SET_ERROR = 16777216 /* (0x100 << OPCODE_SHIFT) */,
        SET_CHANGED_COMPOSITION_TYPES = 16842752 /* (0x101 << OPCODE_SHIFT) */,
        SET_DISPLAY_REQUESTS = 16908288 /* (0x102 << OPCODE_SHIFT) */,
        SET_PRESENT_FENCE = 16973824 /* (0x103 << OPCODE_SHIFT) */,
        SET_RELEASE_FENCES = 17039360 /* (0x104 << OPCODE_SHIFT) */,
        /**
         * display commands
         */
        SET_COLOR_TRANSFORM = 33554432 /* (0x200 << OPCODE_SHIFT) */,
        SET_CLIENT_TARGET = 33619968 /* (0x201 << OPCODE_SHIFT) */,
        SET_OUTPUT_BUFFER = 33685504 /* (0x202 << OPCODE_SHIFT) */,
        VALIDATE_DISPLAY = 33751040 /* (0x203 << OPCODE_SHIFT) */,
        ACCEPT_DISPLAY_CHANGES = 33816576 /* (0x204 << OPCODE_SHIFT) */,
        PRESENT_DISPLAY = 33882112 /* (0x205 << OPCODE_SHIFT) */,
        PRESENT_OR_VALIDATE_DISPLAY = 33947648 /* (0x206 << OPCODE_SHIFT) */,
        /**
         * layer commands (VALIDATE_DISPLAY not required)
         */
        SET_LAYER_CURSOR_POSITION = 50331648 /* (0x300 << OPCODE_SHIFT) */,
        SET_LAYER_BUFFER = 50397184 /* (0x301 << OPCODE_SHIFT) */,
        SET_LAYER_SURFACE_DAMAGE = 50462720 /* (0x302 << OPCODE_SHIFT) */,
        /**
         * layer state commands (VALIDATE_DISPLAY required)
         */
        SET_LAYER_BLEND_MODE = 67108864 /* (0x400 << OPCODE_SHIFT) */,
        SET_LAYER_COLOR = 67174400 /* (0x401 << OPCODE_SHIFT) */,
        SET_LAYER_COMPOSITION_TYPE = 67239936 /* (0x402 << OPCODE_SHIFT) */,
        SET_LAYER_DATASPACE = 67305472 /* (0x403 << OPCODE_SHIFT) */,
        SET_LAYER_DISPLAY_FRAME = 67371008 /* (0x404 << OPCODE_SHIFT) */,
        SET_LAYER_PLANE_ALPHA = 67436544 /* (0x405 << OPCODE_SHIFT) */,
        SET_LAYER_SIDEBAND_STREAM = 67502080 /* (0x406 << OPCODE_SHIFT) */,
        SET_LAYER_SOURCE_CROP = 67567616 /* (0x407 << OPCODE_SHIFT) */,
        SET_LAYER_TRANSFORM = 67633152 /* (0x408 << OPCODE_SHIFT) */,
        SET_LAYER_VISIBLE_REGION = 67698688 /* (0x409 << OPCODE_SHIFT) */,
        SET_LAYER_Z_ORDER = 67764224 /* (0x40a << OPCODE_SHIFT) */,
        SET_PRESENT_OR_VALIDATE_DISPLAY_RESULT = 67829760 /* (0x40b << OPCODE_SHIFT) */,
        SET_LAYER_INFO = 134217728 /* (0x800 << OPCODE_SHIFT) */,
        SET_CLIENT_TARGET_METADATA = 134283264 /* (0x801 << OPCODE_SHIFT) */,
        SET_LAYER_BUFFER_METADATA = 134348800 /* (0x802 << OPCODE_SHIFT) */,
    };

    /**
     * Returns whether this object's implementation is outside of the current process.
     */
    virtual bool isRemote() const override { return false; }

    // @entry @callflow(next="*")
    /**
     * Provides a IComposerCallback object for the device to call.
     * 
     * This function must be called only once.
     * 
     * @param callback is the IComposerCallback object.
     */
    virtual ::android::hardware::Return<void> registerCallback(const ::android::sp<::android::hardware::graphics::composer::V2_1::IComposerCallback>& callback) = 0;

    // @callflow(next="*")
    /**
     * Returns the maximum number of virtual displays supported by this device
     * (which may be 0). The client must not attempt to create more than this
     * many virtual displays on this device. This number must not change for
     * the lifetime of the device.
     * 
     * @return count is the maximum number of virtual displays supported.
     */
    virtual ::android::hardware::Return<uint32_t> getMaxVirtualDisplayCount() = 0;

    /**
     * Return callback for createVirtualDisplay
     */
    using createVirtualDisplay_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, uint64_t display, ::android::hardware::graphics::common::V1_0::PixelFormat format)>;
    // @callflow(next="*")
    /**
     * Creates a new virtual display with the given width and height. The
     * format passed into this function is the default format requested by the
     * consumer of the virtual display output buffers.
     * 
     * The display must be assumed to be on from the time the first frame is
     * presented until the display is destroyed.
     * 
     * @param width is the width in pixels.
     * @param height is the height in pixels.
     * @param formatHint is the default output buffer format selected by
     *        the consumer.
     * @param outputBufferSlotCount is the number of output buffer slots to be
     *        reserved.
     * @return error is NONE upon success. Otherwise,
     *         UNSUPPORTED when the width or height is too large for the
     *                     device to be able to create a virtual display.
     *         NO_RESOURCES when the device is unable to create a new virtual
     *                      display at this time.
     * @return display is the newly-created virtual display.
     * @return format is the format of the buffer the device will produce.
     */
    virtual ::android::hardware::Return<void> createVirtualDisplay(uint32_t width, uint32_t height, ::android::hardware::graphics::common::V1_0::PixelFormat formatHint, uint32_t outputBufferSlotCount, createVirtualDisplay_cb _hidl_cb) = 0;

    // @callflow(next="*")
    /**
     * Destroys a virtual display. After this call all resources consumed by
     * this display may be freed by the device and any operations performed on
     * this display must fail.
     * 
     * @param display is the virtual display to destroy.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         BAD_PARAMETER when the display handle which was passed in does
     *                       not refer to a virtual display.
     */
    virtual ::android::hardware::Return<::android::hardware::graphics::composer::V2_1::Error> destroyVirtualDisplay(uint64_t display) = 0;

    /**
     * Return callback for createLayer
     */
    using createLayer_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, uint64_t layer)>;
    // @callflow(next="*")
    /**
     * Creates a new layer on the given display.
     * 
     * @param display is the display on which to create the layer.
     * @param bufferSlotCount is the number of buffer slot to be reserved.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         NO_RESOURCES when the device was unable to create a layer this
     *                      time.
     * @return layer is the handle of the new layer.
     */
    virtual ::android::hardware::Return<void> createLayer(uint64_t display, uint32_t bufferSlotCount, createLayer_cb _hidl_cb) = 0;

    // @callflow(next="*")
    /**
     * Destroys the given layer.
     * 
     * @param display is the display on which the layer was created.
     * @param layer is the layer to destroy.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         BAD_LAYER when an invalid layer handle was passed in.
     */
    virtual ::android::hardware::Return<::android::hardware::graphics::composer::V2_1::Error> destroyLayer(uint64_t display, uint64_t layer) = 0;

    /**
     * Return callback for getActiveConfig
     */
    using getActiveConfig_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, uint32_t config)>;
    // @callflow(next="*")
    /**
     * Retrieves which display configuration is currently active.
     * 
     * If no display configuration is currently active, this function must
     * return BAD_CONFIG. It is the responsibility of the client to call
     * setActiveConfig with a valid configuration before attempting to present
     * anything on the display.
     * 
     * @param display is the display to which the active config is queried.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         BAD_CONFIG when no configuration is currently active.
     * @return config is the currently active display configuration.
     */
    virtual ::android::hardware::Return<void> getActiveConfig(uint64_t display, getActiveConfig_cb _hidl_cb) = 0;

    // @callflow(next="*")
    /**
     * Returns whether a client target with the given properties can be
     * handled by the device.
     * 
     * This function must return true for a client target with width and
     * height equal to the active display configuration dimensions,
     * PixelFormat::RGBA_8888, and Dataspace::UNKNOWN. It is not required to
     * return true for any other configuration.
     * 
     * @param display is the display to query.
     * @param width is the client target width in pixels.
     * @param height is the client target height in pixels.
     * @param format is the client target format.
     * @param dataspace is the client target dataspace, as described in
     *        setLayerDataspace.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         UNSUPPORTED when the given configuration is not supported.
     */
    virtual ::android::hardware::Return<::android::hardware::graphics::composer::V2_1::Error> getClientTargetSupport(uint64_t display, uint32_t width, uint32_t height, ::android::hardware::graphics::common::V1_0::PixelFormat format, ::android::hardware::graphics::common::V1_0::Dataspace dataspace) = 0;

    /**
     * Return callback for getColorModes
     */
    using getColorModes_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, const ::android::hardware::hidl_vec<::android::hardware::graphics::common::V1_0::ColorMode>& modes)>;
    // @callflow(next="*")
    /**
     * Returns the color modes supported on this display.
     * 
     * All devices must support at least ColorMode::NATIVE.
     * 
     * @param display is the display to query.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     * @return modes is an array of color modes.
     */
    virtual ::android::hardware::Return<void> getColorModes(uint64_t display, getColorModes_cb _hidl_cb) = 0;

    /**
     * Return callback for getDisplayAttribute
     */
    using getDisplayAttribute_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, int32_t value)>;
    // @callflow(next="*")
    /**
     * Returns a display attribute value for a particular display
     * configuration.
     * 
     * @param display is the display to query.
     * @param config is the display configuration for which to return
     *        attribute values.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         BAD_CONFIG when config does not name a valid configuration for
     *                    this display.
     *         BAD_PARAMETER when attribute is unrecognized.
     *         UNSUPPORTED when attribute cannot be queried for the config.
     * @return value is the value of the attribute.
     */
    virtual ::android::hardware::Return<void> getDisplayAttribute(uint64_t display, uint32_t config, ::android::hardware::graphics::composer::V2_1::IComposerClient::Attribute attribute, getDisplayAttribute_cb _hidl_cb) = 0;

    /**
     * Return callback for getDisplayConfigs
     */
    using getDisplayConfigs_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, const ::android::hardware::hidl_vec<uint32_t>& configs)>;
    // @callflow(next="*")
    /**
     * Returns handles for all of the valid display configurations on this
     * display.
     * 
     * @param display is the display to query.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     * @return configs is an array of configuration handles.
     */
    virtual ::android::hardware::Return<void> getDisplayConfigs(uint64_t display, getDisplayConfigs_cb _hidl_cb) = 0;

    /**
     * Return callback for getDisplayName
     */
    using getDisplayName_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, const ::android::hardware::hidl_string& name)>;
    // @callflow(next="*")
    /**
     * Returns a human-readable version of the display's name.
     * 
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     * @return name is the name of the display.
     */
    virtual ::android::hardware::Return<void> getDisplayName(uint64_t display, getDisplayName_cb _hidl_cb) = 0;

    /**
     * Return callback for getDisplayType
     */
    using getDisplayType_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, ::android::hardware::graphics::composer::V2_1::IComposerClient::DisplayType type)>;
    // @callflow(next="*")
    /**
     * Returns whether the given display is a physical or virtual display.
     * 
     * @param display is the display to query.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     * @return type is the type of the display.
     */
    virtual ::android::hardware::Return<void> getDisplayType(uint64_t display, getDisplayType_cb _hidl_cb) = 0;

    /**
     * Return callback for getDozeSupport
     */
    using getDozeSupport_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, bool support)>;
    // @callflow(next="*")
    /**
     * Returns whether the given display supports PowerMode::DOZE and
     * PowerMode::DOZE_SUSPEND. DOZE_SUSPEND may not provide any benefit over
     * DOZE (see the definition of PowerMode for more information), but if
     * both DOZE and DOZE_SUSPEND are no different from PowerMode::ON, the
     * device must not claim support.
     * 
     * @param display is the display to query.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     * @return support is true only when the display supports doze modes.
     */
    virtual ::android::hardware::Return<void> getDozeSupport(uint64_t display, getDozeSupport_cb _hidl_cb) = 0;

    /**
     * Return callback for getHdrCapabilities
     */
    using getHdrCapabilities_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, const ::android::hardware::hidl_vec<::android::hardware::graphics::common::V1_0::Hdr>& types, float maxLuminance, float maxAverageLuminance, float minLuminance)>;
    // @callflow(next="*")
    /**
     * Returns the high dynamic range (HDR) capabilities of the given display,
     * which are invariant with regard to the active configuration.
     * 
     * Displays which are not HDR-capable must return no types.
     * 
     * @param display is the display to query.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     * @return types is an array of HDR types, may have 0 elements if the
     *         display is not HDR-capable.
     * @return maxLuminance is the desired content maximum luminance for this
     *         display in cd/m^2.
     * @return maxAverageLuminance - the desired content maximum frame-average
     *         luminance for this display in cd/m^2.
     * @return minLuminance is the desired content minimum luminance for this
     *         display in cd/m^2.
     */
    virtual ::android::hardware::Return<void> getHdrCapabilities(uint64_t display, getHdrCapabilities_cb _hidl_cb) = 0;

    // @callflow(next="*")
    /**
     * Set the number of client target slots to be reserved.
     * 
     * @param display is the display to which the slots are reserved.
     * @param clientTargetSlotCount is the slot count for client targets.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         NO_RESOURCES when unable to reserve the slots.
     */
    virtual ::android::hardware::Return<::android::hardware::graphics::composer::V2_1::Error> setClientTargetSlotCount(uint64_t display, uint32_t clientTargetSlotCount) = 0;

    // @callflow(next="*")
    /**
     * Sets the active configuration for this display. Upon returning, the
     * given display configuration must be active and remain so until either
     * this function is called again or the display is disconnected.
     * 
     * @param display is the display to which the active config is set.
     * @param config is the new display configuration.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         BAD_CONFIG when the configuration handle passed in is not valid
     *                    for this display.
     */
    virtual ::android::hardware::Return<::android::hardware::graphics::composer::V2_1::Error> setActiveConfig(uint64_t display, uint32_t config) = 0;

    // @callflow(next="*")
    /**
     * Sets the color mode of the given display.
     * 
     * Upon returning from this function, the color mode change must have
     * fully taken effect.
     * 
     * All devices must support at least ColorMode::NATIVE, and displays are
     * assumed to be in this mode upon hotplug.
     * 
     * @param display is the display to which the color mode is set.
     * @param mode is the mode to set to.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         BAD_PARAMETER when mode is not a valid color mode.
     *         UNSUPPORTED when mode is not supported on this display.
     */
    virtual ::android::hardware::Return<::android::hardware::graphics::composer::V2_1::Error> setColorMode(uint64_t display, ::android::hardware::graphics::common::V1_0::ColorMode mode) = 0;

    // @callflow(next="*")
    /**
     * Sets the power mode of the given display. The transition must be
     * complete when this function returns. It is valid to call this function
     * multiple times with the same power mode.
     * 
     * All displays must support PowerMode::ON and PowerMode::OFF.  Whether a
     * display supports PowerMode::DOZE or PowerMode::DOZE_SUSPEND may be
     * queried using getDozeSupport.
     * 
     * @param display is the display to which the power mode is set.
     * @param mode is the new power mode.
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         BAD_PARAMETER when mode was not a valid power mode.
     *         UNSUPPORTED when mode is not supported on this display.
     */
    virtual ::android::hardware::Return<::android::hardware::graphics::composer::V2_1::Error> setPowerMode(uint64_t display, ::android::hardware::graphics::composer::V2_1::IComposerClient::PowerMode mode) = 0;

    // @callflow(next="*")
    /**
     * Enables or disables the vsync signal for the given display. Virtual
     * displays never generate vsync callbacks, and any attempt to enable
     * vsync for a virtual display though this function must succeed and have
     * no other effect.
     * 
     * @param display is the display to which the vsync mode is set.
     * @param enabled indicates whether to enable or disable vsync
     * @return error is NONE upon success. Otherwise,
     *         BAD_DISPLAY when an invalid display handle was passed in.
     *         BAD_PARAMETER when enabled was an invalid value.
     */
    virtual ::android::hardware::Return<::android::hardware::graphics::composer::V2_1::Error> setVsyncEnabled(uint64_t display, ::android::hardware::graphics::composer::V2_1::IComposerClient::Vsync enabled) = 0;

    // @callflow(next="*")
    /**
     * Sets the input command message queue.
     * 
     * @param descriptor is the descriptor of the input command message queue.
     * @return error is NONE upon success. Otherwise,
     *         NO_RESOURCES when failed to set the queue temporarily.
     */
    virtual ::android::hardware::Return<::android::hardware::graphics::composer::V2_1::Error> setInputCommandQueue(const ::android::hardware::MQDescriptorSync<uint32_t>& descriptor) = 0;

    /**
     * Return callback for getOutputCommandQueue
     */
    using getOutputCommandQueue_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, const ::android::hardware::MQDescriptorSync<uint32_t>& descriptor)>;
    // @callflow(next="*")
    /**
     * Gets the output command message queue.
     * 
     * This function must only be called inside executeCommands closure.
     * 
     * @return error is NONE upon success. Otherwise,
     *         NO_RESOURCES when failed to get the queue temporarily.
     * @return descriptor is the descriptor of the output command queue.
     */
    virtual ::android::hardware::Return<void> getOutputCommandQueue(getOutputCommandQueue_cb _hidl_cb) = 0;

    /**
     * Return callback for executeCommands
     */
    using executeCommands_cb = std::function<void(::android::hardware::graphics::composer::V2_1::Error error, bool outQueueChanged, uint32_t outLength, const ::android::hardware::hidl_vec<::android::hardware::hidl_handle>& outHandles)>;
    // @callflow(next="*")
    /**
     * Executes commands from the input command message queue. Return values
     * generated by the input commands are written to the output command
     * message queue in the form of value commands.
     * 
     * @param inLength is the length of input commands.
     * @param inHandles is an array of handles referenced by the input
     *        commands.
     * @return error is NONE upon success. Otherwise,
     *         BAD_PARAMETER when inLength is not equal to the length of
     *                       commands in the input command message queue.
     *         NO_RESOURCES when the output command message queue was not
     *                      properly drained.
     * @param outQueueChanged indicates whether the output command message
     *        queue has changed.
     * @param outLength is the length of output commands.
     * @param outHandles is an array of handles referenced by the output
     *        commands.
     */
    virtual ::android::hardware::Return<void> executeCommands(uint32_t inLength, const ::android::hardware::hidl_vec<::android::hardware::hidl_handle>& inHandles, executeCommands_cb _hidl_cb) = 0;

    /**
     * Return callback for interfaceChain
     */
    using interfaceChain_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& descriptors)>;
    virtual ::android::hardware::Return<void> interfaceChain(interfaceChain_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options) override;

    /**
     * Return callback for interfaceDescriptor
     */
    using interfaceDescriptor_cb = std::function<void(const ::android::hardware::hidl_string& descriptor)>;
    virtual ::android::hardware::Return<void> interfaceDescriptor(interfaceDescriptor_cb _hidl_cb) override;

    /**
     * Return callback for getHashChain
     */
    using getHashChain_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::hidl_array<uint8_t, 32>>& hashchain)>;
    virtual ::android::hardware::Return<void> getHashChain(getHashChain_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> setHALInstrumentation() override;

    virtual ::android::hardware::Return<bool> linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie) override;

    virtual ::android::hardware::Return<void> ping() override;

    /**
     * Return callback for getDebugInfo
     */
    using getDebugInfo_cb = std::function<void(const ::android::hidl::base::V1_0::DebugInfo& info)>;
    virtual ::android::hardware::Return<void> getDebugInfo(getDebugInfo_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> notifySyspropsChanged() override;

    virtual ::android::hardware::Return<bool> unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient) override;

    // cast static functions
    /**
     * This performs a checked cast based on what the underlying implementation actually is.
     */
    static ::android::hardware::Return<::android::sp<::android::frameworks::vr::composer::V1_0::IVrComposerClient>> castFrom(const ::android::sp<::android::frameworks::vr::composer::V1_0::IVrComposerClient>& parent, bool emitError = false);
    /**
     * This performs a checked cast based on what the underlying implementation actually is.
     */
    static ::android::hardware::Return<::android::sp<::android::frameworks::vr::composer::V1_0::IVrComposerClient>> castFrom(const ::android::sp<::android::hardware::graphics::composer::V2_1::IComposerClient>& parent, bool emitError = false);
    /**
     * This performs a checked cast based on what the underlying implementation actually is.
     */
    static ::android::hardware::Return<::android::sp<::android::frameworks::vr::composer::V1_0::IVrComposerClient>> castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError = false);

    // helper methods for interactions with the hwservicemanager
    /**
     * This gets the service of this type with the specified instance name. If the
     * service is currently not available or not in the VINTF manifest on a Trebilized
     * device, this will return nullptr. This is useful when you don't want to block
     * during device boot. If getStub is true, this will try to return an unwrapped
     * passthrough implementation in the same process. This is useful when getting an
     * implementation from the same partition/compilation group.
     * 
     * In general, prefer getService(std::string,bool)
     */
    static ::android::sp<IVrComposerClient> tryGetService(const std::string &serviceName="default", bool getStub=false);
    /**
     * Deprecated. See tryGetService(std::string, bool)
     */
    static ::android::sp<IVrComposerClient> tryGetService(const char serviceName[], bool getStub=false)  { std::string str(serviceName ? serviceName : "");      return tryGetService(str, getStub); }
    /**
     * Deprecated. See tryGetService(std::string, bool)
     */
    static ::android::sp<IVrComposerClient> tryGetService(const ::android::hardware::hidl_string& serviceName, bool getStub=false)  { std::string str(serviceName.c_str());      return tryGetService(str, getStub); }
    /**
     * Calls tryGetService("default", bool). This is the recommended instance name for singleton services.
     */
    static ::android::sp<IVrComposerClient> tryGetService(bool getStub) { return tryGetService("default", getStub); }
    /**
     * This gets the service of this type with the specified instance name. If the
     * service is not in the VINTF manifest on a Trebilized device, this will return
     * nullptr. If the service is not available, this will wait for the service to
     * become available. If the service is a lazy service, this will start the service
     * and return when it becomes available. If getStub is true, this will try to
     * return an unwrapped passthrough implementation in the same process. This is
     * useful when getting an implementation from the same partition/compilation group.
     */
    static ::android::sp<IVrComposerClient> getService(const std::string &serviceName="default", bool getStub=false);
    /**
     * Deprecated. See getService(std::string, bool)
     */
    static ::android::sp<IVrComposerClient> getService(const char serviceName[], bool getStub=false)  { std::string str(serviceName ? serviceName : "");      return getService(str, getStub); }
    /**
     * Deprecated. See getService(std::string, bool)
     */
    static ::android::sp<IVrComposerClient> getService(const ::android::hardware::hidl_string& serviceName, bool getStub=false)  { std::string str(serviceName.c_str());      return getService(str, getStub); }
    /**
     * Calls getService("default", bool). This is the recommended instance name for singleton services.
     */
    static ::android::sp<IVrComposerClient> getService(bool getStub) { return getService("default", getStub); }
    /**
     * Registers a service with the service manager. For Trebilized devices, the service
     * must also be in the VINTF manifest.
     */
    __attribute__ ((warn_unused_result))::android::status_t registerAsService(const std::string &serviceName="default");
    /**
     * Registers for notifications for when a service is registered.
     */
    static bool registerForNotifications(
            const std::string &serviceName,
            const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification);
};

//
// type declarations for package
//

static inline std::string toString(const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata& o);
static inline bool operator==(const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata& lhs, const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata& rhs);
static inline bool operator!=(const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata& lhs, const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata& rhs);

template<typename>
static inline std::string toString(int32_t o);
static inline std::string toString(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand o);

constexpr int32_t operator|(const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand lhs, const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) | static_cast<int32_t>(rhs));
}
constexpr int32_t operator|(const int32_t lhs, const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand rhs) {
    return static_cast<int32_t>(lhs | static_cast<int32_t>(rhs));
}
constexpr int32_t operator|(const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand lhs, const int32_t rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) | rhs);
}
constexpr int32_t operator&(const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand lhs, const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) & static_cast<int32_t>(rhs));
}
constexpr int32_t operator&(const int32_t lhs, const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand rhs) {
    return static_cast<int32_t>(lhs & static_cast<int32_t>(rhs));
}
constexpr int32_t operator&(const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand lhs, const int32_t rhs) {
    return static_cast<int32_t>(static_cast<int32_t>(lhs) & rhs);
}
constexpr int32_t &operator|=(int32_t& v, const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand e) {
    v |= static_cast<int32_t>(e);
    return v;
}
constexpr int32_t &operator&=(int32_t& v, const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand e) {
    v &= static_cast<int32_t>(e);
    return v;
}

static inline std::string toString(const ::android::sp<::android::frameworks::vr::composer::V1_0::IVrComposerClient>& o);

//
// type header definitions for package
//

static inline std::string toString(const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata& o) {
    using ::android::hardware::toString;
    std::string os;
    os += "{";
    os += ".width = ";
    os += ::android::hardware::toString(o.width);
    os += ", .height = ";
    os += ::android::hardware::toString(o.height);
    os += ", .stride = ";
    os += ::android::hardware::toString(o.stride);
    os += ", .layerCount = ";
    os += ::android::hardware::toString(o.layerCount);
    os += ", .format = ";
    os += ::android::hardware::graphics::common::V1_0::toString(o.format);
    os += ", .usage = ";
    os += ::android::hardware::graphics::common::V1_0::toString<::android::hardware::graphics::common::V1_0::BufferUsage>(o.usage);
    os += "}"; return os;
}

static inline bool operator==(const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata& lhs, const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata& rhs) {
    if (lhs.width != rhs.width) {
        return false;
    }
    if (lhs.height != rhs.height) {
        return false;
    }
    if (lhs.stride != rhs.stride) {
        return false;
    }
    if (lhs.layerCount != rhs.layerCount) {
        return false;
    }
    if (lhs.format != rhs.format) {
        return false;
    }
    if (lhs.usage != rhs.usage) {
        return false;
    }
    return true;
}

static inline bool operator!=(const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata& lhs, const ::android::frameworks::vr::composer::V1_0::IVrComposerClient::BufferMetadata& rhs){
    return !(lhs == rhs);
}

template<>
inline std::string toString<::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand>(int32_t o) {
    using ::android::hardware::details::toHexString;
    std::string os;
    ::android::hardware::hidl_bitfield<::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand> flipped = 0;
    bool first = true;
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::LENGTH_MASK) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::LENGTH_MASK)) {
        os += (first ? "" : " | ");
        os += "LENGTH_MASK";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::LENGTH_MASK;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::OPCODE_SHIFT) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::OPCODE_SHIFT)) {
        os += (first ? "" : " | ");
        os += "OPCODE_SHIFT";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::OPCODE_SHIFT;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::OPCODE_MASK) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::OPCODE_MASK)) {
        os += (first ? "" : " | ");
        os += "OPCODE_MASK";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::OPCODE_MASK;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SELECT_DISPLAY) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SELECT_DISPLAY)) {
        os += (first ? "" : " | ");
        os += "SELECT_DISPLAY";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SELECT_DISPLAY;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SELECT_LAYER) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SELECT_LAYER)) {
        os += (first ? "" : " | ");
        os += "SELECT_LAYER";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SELECT_LAYER;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_ERROR) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_ERROR)) {
        os += (first ? "" : " | ");
        os += "SET_ERROR";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_ERROR;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CHANGED_COMPOSITION_TYPES) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CHANGED_COMPOSITION_TYPES)) {
        os += (first ? "" : " | ");
        os += "SET_CHANGED_COMPOSITION_TYPES";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CHANGED_COMPOSITION_TYPES;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_DISPLAY_REQUESTS) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_DISPLAY_REQUESTS)) {
        os += (first ? "" : " | ");
        os += "SET_DISPLAY_REQUESTS";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_DISPLAY_REQUESTS;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_PRESENT_FENCE) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_PRESENT_FENCE)) {
        os += (first ? "" : " | ");
        os += "SET_PRESENT_FENCE";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_PRESENT_FENCE;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_RELEASE_FENCES) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_RELEASE_FENCES)) {
        os += (first ? "" : " | ");
        os += "SET_RELEASE_FENCES";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_RELEASE_FENCES;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_COLOR_TRANSFORM) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_COLOR_TRANSFORM)) {
        os += (first ? "" : " | ");
        os += "SET_COLOR_TRANSFORM";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_COLOR_TRANSFORM;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CLIENT_TARGET) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CLIENT_TARGET)) {
        os += (first ? "" : " | ");
        os += "SET_CLIENT_TARGET";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CLIENT_TARGET;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_OUTPUT_BUFFER) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_OUTPUT_BUFFER)) {
        os += (first ? "" : " | ");
        os += "SET_OUTPUT_BUFFER";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_OUTPUT_BUFFER;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::VALIDATE_DISPLAY) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::VALIDATE_DISPLAY)) {
        os += (first ? "" : " | ");
        os += "VALIDATE_DISPLAY";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::VALIDATE_DISPLAY;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::ACCEPT_DISPLAY_CHANGES) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::ACCEPT_DISPLAY_CHANGES)) {
        os += (first ? "" : " | ");
        os += "ACCEPT_DISPLAY_CHANGES";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::ACCEPT_DISPLAY_CHANGES;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::PRESENT_DISPLAY) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::PRESENT_DISPLAY)) {
        os += (first ? "" : " | ");
        os += "PRESENT_DISPLAY";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::PRESENT_DISPLAY;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::PRESENT_OR_VALIDATE_DISPLAY) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::PRESENT_OR_VALIDATE_DISPLAY)) {
        os += (first ? "" : " | ");
        os += "PRESENT_OR_VALIDATE_DISPLAY";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::PRESENT_OR_VALIDATE_DISPLAY;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_CURSOR_POSITION) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_CURSOR_POSITION)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_CURSOR_POSITION";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_CURSOR_POSITION;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BUFFER) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BUFFER)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_BUFFER";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BUFFER;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SURFACE_DAMAGE) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SURFACE_DAMAGE)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_SURFACE_DAMAGE";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SURFACE_DAMAGE;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BLEND_MODE) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BLEND_MODE)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_BLEND_MODE";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BLEND_MODE;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_COLOR) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_COLOR)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_COLOR";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_COLOR;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_COMPOSITION_TYPE) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_COMPOSITION_TYPE)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_COMPOSITION_TYPE";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_COMPOSITION_TYPE;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_DATASPACE) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_DATASPACE)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_DATASPACE";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_DATASPACE;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_DISPLAY_FRAME) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_DISPLAY_FRAME)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_DISPLAY_FRAME";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_DISPLAY_FRAME;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_PLANE_ALPHA) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_PLANE_ALPHA)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_PLANE_ALPHA";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_PLANE_ALPHA;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SIDEBAND_STREAM) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SIDEBAND_STREAM)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_SIDEBAND_STREAM";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SIDEBAND_STREAM;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SOURCE_CROP) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SOURCE_CROP)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_SOURCE_CROP";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SOURCE_CROP;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_TRANSFORM) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_TRANSFORM)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_TRANSFORM";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_TRANSFORM;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_VISIBLE_REGION) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_VISIBLE_REGION)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_VISIBLE_REGION";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_VISIBLE_REGION;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_Z_ORDER) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_Z_ORDER)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_Z_ORDER";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_Z_ORDER;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_PRESENT_OR_VALIDATE_DISPLAY_RESULT) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_PRESENT_OR_VALIDATE_DISPLAY_RESULT)) {
        os += (first ? "" : " | ");
        os += "SET_PRESENT_OR_VALIDATE_DISPLAY_RESULT";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_PRESENT_OR_VALIDATE_DISPLAY_RESULT;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_INFO) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_INFO)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_INFO";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_INFO;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CLIENT_TARGET_METADATA) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CLIENT_TARGET_METADATA)) {
        os += (first ? "" : " | ");
        os += "SET_CLIENT_TARGET_METADATA";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CLIENT_TARGET_METADATA;
    }
    if ((o & ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BUFFER_METADATA) == static_cast<int32_t>(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BUFFER_METADATA)) {
        os += (first ? "" : " | ");
        os += "SET_LAYER_BUFFER_METADATA";
        first = false;
        flipped |= ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BUFFER_METADATA;
    }
    if (o != flipped) {
        os += (first ? "" : " | ");
        os += toHexString(o & (~flipped));
    }os += " (";
    os += toHexString(o);
    os += ")";
    return os;
}

static inline std::string toString(::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand o) {
    using ::android::hardware::details::toHexString;
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::LENGTH_MASK) {
        return "LENGTH_MASK";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::OPCODE_SHIFT) {
        return "OPCODE_SHIFT";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::OPCODE_MASK) {
        return "OPCODE_MASK";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SELECT_DISPLAY) {
        return "SELECT_DISPLAY";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SELECT_LAYER) {
        return "SELECT_LAYER";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_ERROR) {
        return "SET_ERROR";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CHANGED_COMPOSITION_TYPES) {
        return "SET_CHANGED_COMPOSITION_TYPES";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_DISPLAY_REQUESTS) {
        return "SET_DISPLAY_REQUESTS";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_PRESENT_FENCE) {
        return "SET_PRESENT_FENCE";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_RELEASE_FENCES) {
        return "SET_RELEASE_FENCES";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_COLOR_TRANSFORM) {
        return "SET_COLOR_TRANSFORM";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CLIENT_TARGET) {
        return "SET_CLIENT_TARGET";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_OUTPUT_BUFFER) {
        return "SET_OUTPUT_BUFFER";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::VALIDATE_DISPLAY) {
        return "VALIDATE_DISPLAY";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::ACCEPT_DISPLAY_CHANGES) {
        return "ACCEPT_DISPLAY_CHANGES";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::PRESENT_DISPLAY) {
        return "PRESENT_DISPLAY";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::PRESENT_OR_VALIDATE_DISPLAY) {
        return "PRESENT_OR_VALIDATE_DISPLAY";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_CURSOR_POSITION) {
        return "SET_LAYER_CURSOR_POSITION";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BUFFER) {
        return "SET_LAYER_BUFFER";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SURFACE_DAMAGE) {
        return "SET_LAYER_SURFACE_DAMAGE";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BLEND_MODE) {
        return "SET_LAYER_BLEND_MODE";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_COLOR) {
        return "SET_LAYER_COLOR";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_COMPOSITION_TYPE) {
        return "SET_LAYER_COMPOSITION_TYPE";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_DATASPACE) {
        return "SET_LAYER_DATASPACE";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_DISPLAY_FRAME) {
        return "SET_LAYER_DISPLAY_FRAME";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_PLANE_ALPHA) {
        return "SET_LAYER_PLANE_ALPHA";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SIDEBAND_STREAM) {
        return "SET_LAYER_SIDEBAND_STREAM";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SOURCE_CROP) {
        return "SET_LAYER_SOURCE_CROP";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_TRANSFORM) {
        return "SET_LAYER_TRANSFORM";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_VISIBLE_REGION) {
        return "SET_LAYER_VISIBLE_REGION";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_Z_ORDER) {
        return "SET_LAYER_Z_ORDER";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_PRESENT_OR_VALIDATE_DISPLAY_RESULT) {
        return "SET_PRESENT_OR_VALIDATE_DISPLAY_RESULT";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_INFO) {
        return "SET_LAYER_INFO";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CLIENT_TARGET_METADATA) {
        return "SET_CLIENT_TARGET_METADATA";
    }
    if (o == ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BUFFER_METADATA) {
        return "SET_LAYER_BUFFER_METADATA";
    }
    std::string os;
    os += toHexString(static_cast<int32_t>(o));
    return os;
}

static inline std::string toString(const ::android::sp<::android::frameworks::vr::composer::V1_0::IVrComposerClient>& o) {
    std::string os = "[class or subclass of ";
    os += ::android::frameworks::vr::composer::V1_0::IVrComposerClient::descriptor;
    os += "]";
    os += o->isRemote() ? "@remote" : "@local";
    return os;
}


}  // namespace V1_0
}  // namespace composer
}  // namespace vr
}  // namespace frameworks
}  // namespace android

//
// global type declarations for package
//

namespace android {
namespace hardware {
namespace details {
template<> constexpr std::array<::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand, 35> hidl_enum_values<::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand> = {
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::LENGTH_MASK,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::OPCODE_SHIFT,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::OPCODE_MASK,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SELECT_DISPLAY,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SELECT_LAYER,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_ERROR,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CHANGED_COMPOSITION_TYPES,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_DISPLAY_REQUESTS,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_PRESENT_FENCE,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_RELEASE_FENCES,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_COLOR_TRANSFORM,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CLIENT_TARGET,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_OUTPUT_BUFFER,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::VALIDATE_DISPLAY,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::ACCEPT_DISPLAY_CHANGES,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::PRESENT_DISPLAY,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::PRESENT_OR_VALIDATE_DISPLAY,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_CURSOR_POSITION,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BUFFER,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SURFACE_DAMAGE,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BLEND_MODE,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_COLOR,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_COMPOSITION_TYPE,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_DATASPACE,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_DISPLAY_FRAME,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_PLANE_ALPHA,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SIDEBAND_STREAM,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_SOURCE_CROP,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_TRANSFORM,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_VISIBLE_REGION,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_Z_ORDER,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_PRESENT_OR_VALIDATE_DISPLAY_RESULT,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_INFO,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_CLIENT_TARGET_METADATA,
    ::android::frameworks::vr::composer::V1_0::IVrComposerClient::VrCommand::SET_LAYER_BUFFER_METADATA,
};
}  // namespace details
}  // namespace hardware
}  // namespace android


#endif  // HIDL_GENERATED_ANDROID_FRAMEWORKS_VR_COMPOSER_V1_0_IVRCOMPOSERCLIENT_H
