#ifndef HIDL_GENERATED_ANDROID_FRAMEWORKS_VR_COMPOSER_V1_0_IHWVRCOMPOSERCLIENT_H
#define HIDL_GENERATED_ANDROID_FRAMEWORKS_VR_COMPOSER_V1_0_IHWVRCOMPOSERCLIENT_H

#include <android/frameworks/vr/composer/1.0/IVrComposerClient.h>

#include <android/hardware/graphics/common/1.0/hwtypes.h>
#include <android/hardware/graphics/composer/2.1/BnHwComposerClient.h>
#include <android/hardware/graphics/composer/2.1/BpHwComposerClient.h>

#include <hidl/Status.h>
#include <hwbinder/IBinder.h>
#include <hwbinder/Parcel.h>

namespace android {
namespace frameworks {
namespace vr {
namespace composer {
namespace V1_0 {
}  // namespace V1_0
}  // namespace composer
}  // namespace vr
}  // namespace frameworks
}  // namespace android

#endif  // HIDL_GENERATED_ANDROID_FRAMEWORKS_VR_COMPOSER_V1_0_IHWVRCOMPOSERCLIENT_H
