#ifndef HIDL_GENERATED_ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_ISENSORMANAGER_H
#define HIDL_GENERATED_ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_ISENSORMANAGER_H

#include <android/frameworks/sensorservice/1.0/IDirectReportChannel.h>
#include <android/frameworks/sensorservice/1.0/IEventQueue.h>
#include <android/frameworks/sensorservice/1.0/IEventQueueCallback.h>
#include <android/frameworks/sensorservice/1.0/types.h>
#include <android/hardware/sensors/1.0/types.h>
#include <android/hidl/base/1.0/IBase.h>

#include <android/hidl/manager/1.0/IServiceNotification.h>

#include <hidl/HidlSupport.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <utils/NativeHandle.h>
#include <utils/misc.h>

namespace android {
namespace frameworks {
namespace sensorservice {
namespace V1_0 {

/**
 * ISensorManager is an interface to manage sensors
 * 
 * This file provides a set of functions that uses
 * ISensorManager to access and list hardware sensors.
 */
struct ISensorManager : public ::android::hidl::base::V1_0::IBase {
    /**
     * Type tag for use in template logic that indicates this is a 'pure' class.
     */
    typedef android::hardware::details::i_tag _hidl_tag;

    /**
     * Fully qualified interface name: "android.frameworks.sensorservice@1.0::ISensorManager"
     */
    static const char* descriptor;

    /**
     * Returns whether this object's implementation is outside of the current process.
     */
    virtual bool isRemote() const override { return false; }

    /**
     * Return callback for getSensorList
     */
    using getSensorList_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::sensors::V1_0::SensorInfo>& list, ::android::frameworks::sensorservice::V1_0::Result result)>;
    /**
     * Get the list of available sensors.
     * 
     * @return list   the list of available sensors, or empty on failure
     * @return result OK on success or UNKNOWN_ERROR on failure
     */
    virtual ::android::hardware::Return<void> getSensorList(getSensorList_cb _hidl_cb) = 0;

    /**
     * Return callback for getDefaultSensor
     */
    using getDefaultSensor_cb = std::function<void(const ::android::hardware::sensors::V1_0::SensorInfo& sensor, ::android::frameworks::sensorservice::V1_0::Result result)>;
    /**
     * Get the default sensor of the specified type.
     * 
     * @return sensor the default sensor for the given type, or undetermined
     *                value on failure.
     * @return result OK on success or
     * NOT_EXIST if no sensor of that type exists.
     */
    virtual ::android::hardware::Return<void> getDefaultSensor(::android::hardware::sensors::V1_0::SensorType type, getDefaultSensor_cb _hidl_cb) = 0;

    /**
     * Return callback for createAshmemDirectChannel
     */
    using createAshmemDirectChannel_cb = std::function<void(const ::android::sp<::android::frameworks::sensorservice::V1_0::IDirectReportChannel>& chan, ::android::frameworks::sensorservice::V1_0::Result result)>;
    /**
     * Create direct channel based on shared memory
     * 
     * Create a direct channel of DIRECT_CHANNEL_ASHMEM type to be used
     * for configuring sensor direct report.
     * 
     * The memory layout looks as follows. These offsets can be found in
     * android.hardware.sensors@1.0::SensorsEventFormatOffset.
     *   offset   type        name
     *  -----------------------------------
     *   0x0000  int32_t     size (SensorsEventFormatOffset::TOTAL_LENGTH)
     *   0x0004  int32_t     sensor report token
     *   0x0008  int32_t     type (see android.hardware.sensors@1.0::SensorType)
     *   0x000C  uint32_t    atomic counter
     *   0x0010  int64_t     timestamp (see android.hardware.sensors@1.0::Event)
     *   0x0018  float[16]/  data
     *           int64_t[8]
     *   0x0058  int32_t[4]  reserved (set to zero)
     * 
     * @param mem     the shared memory to use, must be ashmem.
     * @param size    the intended size to be used. The following must be true:
     *                SensorsEventFormatOffset::TOTAL_LENGTH <= size <= mem.size
     * 
     * @return chan   The created channel, or NULL if failure.
     * @return result OK if successful;
     *                BAD_VALUE if size > mem.size();
     *                BAD_VALUE if size < TOTAL_LENGTH;
     *                NO_MEMORY, NO_INIT, BAD_VALUE for underlying errors;
     *                UNKNOWN_ERROR if the underlying error is not recognized;
     *                UNKNOWN_ERROR if the underlying call returns channelId = 0
     */
    virtual ::android::hardware::Return<void> createAshmemDirectChannel(const ::android::hardware::hidl_memory& mem, uint64_t size, createAshmemDirectChannel_cb _hidl_cb) = 0;

    /**
     * Return callback for createGrallocDirectChannel
     */
    using createGrallocDirectChannel_cb = std::function<void(const ::android::sp<::android::frameworks::sensorservice::V1_0::IDirectReportChannel>& chan, ::android::frameworks::sensorservice::V1_0::Result result)>;
    /**
     * Create direct channel based on hardware buffer
     * 
     * Create a direct channel of DIRECT_CHANNEL_GRALLOC type to be used
     * for configuring sensor direct report.
     * 
     * @param buffer  file descriptor describing the gralloc buffer.
     * @param size    the intended size to be used, must be less than or equal
     *                to the size of the buffer.
     * 
     * @return chan   The created channel, or NULL if failure.
     * @return result OK if successful;
     *                NO_MEMORY, NO_INIT, BAD_VALUE for underlying errors;
     *                UNKNOWN_ERROR if the underlying error is not recognized;
     *                UNKNOWN_ERROR if the underlying call returns channelId = 0
     */
    virtual ::android::hardware::Return<void> createGrallocDirectChannel(const ::android::hardware::hidl_handle& buffer, uint64_t size, createGrallocDirectChannel_cb _hidl_cb) = 0;

    /**
     * Return callback for createEventQueue
     */
    using createEventQueue_cb = std::function<void(const ::android::sp<::android::frameworks::sensorservice::V1_0::IEventQueue>& queue, ::android::frameworks::sensorservice::V1_0::Result result)>;
    /**
     * Create a sensor event queue.
     * 
     * Create a sensor event queue with an IEventQueueCallback object.
     * Subsequently, one can enable sensors on the event queue so that sensor
     * events are passed via the specified callback.
     * 
     * @param  callback the callback to call on events. Must not be null.
     * @return queue    the event queue created. null on failure.
     * @return result   OK if successful, BAD_VALUE if callback is null,
     *                  or other Result values for any underlying errors.
     */
    virtual ::android::hardware::Return<void> createEventQueue(const ::android::sp<::android::frameworks::sensorservice::V1_0::IEventQueueCallback>& callback, createEventQueue_cb _hidl_cb) = 0;

    /**
     * Return callback for interfaceChain
     */
    using interfaceChain_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& descriptors)>;
    virtual ::android::hardware::Return<void> interfaceChain(interfaceChain_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> debug(const ::android::hardware::hidl_handle& fd, const ::android::hardware::hidl_vec<::android::hardware::hidl_string>& options) override;

    /**
     * Return callback for interfaceDescriptor
     */
    using interfaceDescriptor_cb = std::function<void(const ::android::hardware::hidl_string& descriptor)>;
    virtual ::android::hardware::Return<void> interfaceDescriptor(interfaceDescriptor_cb _hidl_cb) override;

    /**
     * Return callback for getHashChain
     */
    using getHashChain_cb = std::function<void(const ::android::hardware::hidl_vec<::android::hardware::hidl_array<uint8_t, 32>>& hashchain)>;
    virtual ::android::hardware::Return<void> getHashChain(getHashChain_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> setHALInstrumentation() override;

    virtual ::android::hardware::Return<bool> linkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient, uint64_t cookie) override;

    virtual ::android::hardware::Return<void> ping() override;

    /**
     * Return callback for getDebugInfo
     */
    using getDebugInfo_cb = std::function<void(const ::android::hidl::base::V1_0::DebugInfo& info)>;
    virtual ::android::hardware::Return<void> getDebugInfo(getDebugInfo_cb _hidl_cb) override;

    virtual ::android::hardware::Return<void> notifySyspropsChanged() override;

    virtual ::android::hardware::Return<bool> unlinkToDeath(const ::android::sp<::android::hardware::hidl_death_recipient>& recipient) override;

    // cast static functions
    /**
     * This performs a checked cast based on what the underlying implementation actually is.
     */
    static ::android::hardware::Return<::android::sp<::android::frameworks::sensorservice::V1_0::ISensorManager>> castFrom(const ::android::sp<::android::frameworks::sensorservice::V1_0::ISensorManager>& parent, bool emitError = false);
    /**
     * This performs a checked cast based on what the underlying implementation actually is.
     */
    static ::android::hardware::Return<::android::sp<::android::frameworks::sensorservice::V1_0::ISensorManager>> castFrom(const ::android::sp<::android::hidl::base::V1_0::IBase>& parent, bool emitError = false);

    // helper methods for interactions with the hwservicemanager
    /**
     * This gets the service of this type with the specified instance name. If the
     * service is currently not available or not in the VINTF manifest on a Trebilized
     * device, this will return nullptr. This is useful when you don't want to block
     * during device boot. If getStub is true, this will try to return an unwrapped
     * passthrough implementation in the same process. This is useful when getting an
     * implementation from the same partition/compilation group.
     * 
     * In general, prefer getService(std::string,bool)
     */
    static ::android::sp<ISensorManager> tryGetService(const std::string &serviceName="default", bool getStub=false);
    /**
     * Deprecated. See tryGetService(std::string, bool)
     */
    static ::android::sp<ISensorManager> tryGetService(const char serviceName[], bool getStub=false)  { std::string str(serviceName ? serviceName : "");      return tryGetService(str, getStub); }
    /**
     * Deprecated. See tryGetService(std::string, bool)
     */
    static ::android::sp<ISensorManager> tryGetService(const ::android::hardware::hidl_string& serviceName, bool getStub=false)  { std::string str(serviceName.c_str());      return tryGetService(str, getStub); }
    /**
     * Calls tryGetService("default", bool). This is the recommended instance name for singleton services.
     */
    static ::android::sp<ISensorManager> tryGetService(bool getStub) { return tryGetService("default", getStub); }
    /**
     * This gets the service of this type with the specified instance name. If the
     * service is not in the VINTF manifest on a Trebilized device, this will return
     * nullptr. If the service is not available, this will wait for the service to
     * become available. If the service is a lazy service, this will start the service
     * and return when it becomes available. If getStub is true, this will try to
     * return an unwrapped passthrough implementation in the same process. This is
     * useful when getting an implementation from the same partition/compilation group.
     */
    static ::android::sp<ISensorManager> getService(const std::string &serviceName="default", bool getStub=false);
    /**
     * Deprecated. See getService(std::string, bool)
     */
    static ::android::sp<ISensorManager> getService(const char serviceName[], bool getStub=false)  { std::string str(serviceName ? serviceName : "");      return getService(str, getStub); }
    /**
     * Deprecated. See getService(std::string, bool)
     */
    static ::android::sp<ISensorManager> getService(const ::android::hardware::hidl_string& serviceName, bool getStub=false)  { std::string str(serviceName.c_str());      return getService(str, getStub); }
    /**
     * Calls getService("default", bool). This is the recommended instance name for singleton services.
     */
    static ::android::sp<ISensorManager> getService(bool getStub) { return getService("default", getStub); }
    /**
     * Registers a service with the service manager. For Trebilized devices, the service
     * must also be in the VINTF manifest.
     */
    __attribute__ ((warn_unused_result))::android::status_t registerAsService(const std::string &serviceName="default");
    /**
     * Registers for notifications for when a service is registered.
     */
    static bool registerForNotifications(
            const std::string &serviceName,
            const ::android::sp<::android::hidl::manager::V1_0::IServiceNotification> &notification);
};

//
// type declarations for package
//

static inline std::string toString(const ::android::sp<::android::frameworks::sensorservice::V1_0::ISensorManager>& o);

//
// type header definitions for package
//

static inline std::string toString(const ::android::sp<::android::frameworks::sensorservice::V1_0::ISensorManager>& o) {
    std::string os = "[class or subclass of ";
    os += ::android::frameworks::sensorservice::V1_0::ISensorManager::descriptor;
    os += "]";
    os += o->isRemote() ? "@remote" : "@local";
    return os;
}


}  // namespace V1_0
}  // namespace sensorservice
}  // namespace frameworks
}  // namespace android

//
// global type declarations for package
//


#endif  // HIDL_GENERATED_ANDROID_FRAMEWORKS_SENSORSERVICE_V1_0_ISENSORMANAGER_H
