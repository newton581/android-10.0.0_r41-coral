#ifndef HIDL_GENERATED_ANDROID_FRAMEWORKS_BUFFERHUB_V1_0_IHWBUFFERHUB_H
#define HIDL_GENERATED_ANDROID_FRAMEWORKS_BUFFERHUB_V1_0_IHWBUFFERHUB_H

#include <android/frameworks/bufferhub/1.0/IBufferHub.h>

#include <android/frameworks/bufferhub/1.0/BnHwBufferClient.h>
#include <android/frameworks/bufferhub/1.0/BpHwBufferClient.h>
#include <android/frameworks/bufferhub/1.0/hwtypes.h>
#include <android/hardware/graphics/common/1.2/hwtypes.h>
#include <android/hidl/base/1.0/BnHwBase.h>
#include <android/hidl/base/1.0/BpHwBase.h>

#include <hidl/Status.h>
#include <hwbinder/IBinder.h>
#include <hwbinder/Parcel.h>

namespace android {
namespace frameworks {
namespace bufferhub {
namespace V1_0 {
}  // namespace V1_0
}  // namespace bufferhub
}  // namespace frameworks
}  // namespace android

#endif  // HIDL_GENERATED_ANDROID_FRAMEWORKS_BUFFERHUB_V1_0_IHWBUFFERHUB_H
