/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.x509;


/**
 * class to produce an X.509 Version 1 certificate.
 * @deprecated use org.bouncycastle.cert.X509v1CertificateBuilder.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class X509V1CertificateGenerator {

@Deprecated
public X509V1CertificateGenerator() { throw new RuntimeException("Stub!"); }

/**
 * set the serial number for the certificate.
 */

@Deprecated
public void setSerialNumber(java.math.BigInteger serialNumber) { throw new RuntimeException("Stub!"); }

/**
 * Set the issuer distinguished name - the issuer is the entity whose private key is used to sign the
 * certificate.
 */

@Deprecated
public void setIssuerDN(javax.security.auth.x500.X500Principal issuer) { throw new RuntimeException("Stub!"); }

@Deprecated
public void setNotBefore(java.util.Date date) { throw new RuntimeException("Stub!"); }

@Deprecated
public void setNotAfter(java.util.Date date) { throw new RuntimeException("Stub!"); }

/**
 * Set the subject distinguished name. The subject describes the entity associated with the public key.
 */

@Deprecated
public void setSubjectDN(javax.security.auth.x500.X500Principal subject) { throw new RuntimeException("Stub!"); }

@Deprecated
public void setPublicKey(java.security.PublicKey key) { throw new RuntimeException("Stub!"); }

/**
 * Set the signature algorithm. This can be either a name or an OID, names
 * are treated as case insensitive.
 *
 * @param signatureAlgorithm string representation of the algorithm name.
 */

@Deprecated
public void setSignatureAlgorithm(java.lang.String signatureAlgorithm) { throw new RuntimeException("Stub!"); }

/**
 * generate an X509 certificate, based on the current issuer and subject,
 * using the passed in provider for the signing, and the passed in source
 * of randomness (if required).
 */

@Deprecated
public java.security.cert.X509Certificate generate(java.security.PrivateKey key, java.lang.String provider) throws java.security.cert.CertificateEncodingException, java.lang.IllegalStateException, java.security.InvalidKeyException, java.security.NoSuchAlgorithmException, java.security.NoSuchProviderException, java.security.SignatureException { throw new RuntimeException("Stub!"); }
}

