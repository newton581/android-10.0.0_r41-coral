/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.x509;

import java.util.Locale;

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Time extends com.android.org.bouncycastle.asn1.ASN1Object {

/**
 * Creates a time object from a given date - if the date is between 1950
 * and 2049 a UTCTime object is generated, otherwise a GeneralizedTime
 * is used.
 *
 * @param time a date object representing the time of interest.
 */

public Time(java.util.Date time) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

