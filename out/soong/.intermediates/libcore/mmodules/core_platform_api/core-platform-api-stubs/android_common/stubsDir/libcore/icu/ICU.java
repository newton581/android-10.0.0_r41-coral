/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.icu;

import java.util.Locale;

/**
 * Makes ICU data accessible to Java.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ICU {

ICU() { throw new RuntimeException("Stub!"); }

public static java.lang.String getBestDateTimePattern(java.lang.String skeleton, java.util.Locale locale) { throw new RuntimeException("Stub!"); }

public static char[] getDateFormatOrder(java.lang.String pattern) { throw new RuntimeException("Stub!"); }

public static java.util.Locale addLikelySubtags(java.util.Locale locale) { throw new RuntimeException("Stub!"); }

/** Returns the TZData version as reported by ICU4C. */

public static native java.lang.String getTZDataVersion();
}

