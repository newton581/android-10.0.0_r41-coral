/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * An ASN.1 DER NULL object.
 * <p>
 * Preferably use the constant:  DERNull.INSTANCE.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DERNull extends com.android.org.bouncycastle.asn1.ASN1Null {

/**
 * @deprecated use DERNull.INSTANCE
 */

@Deprecated
DERNull() { throw new RuntimeException("Stub!"); }

public static final com.android.org.bouncycastle.asn1.DERNull INSTANCE;
static { INSTANCE = null; }
}

