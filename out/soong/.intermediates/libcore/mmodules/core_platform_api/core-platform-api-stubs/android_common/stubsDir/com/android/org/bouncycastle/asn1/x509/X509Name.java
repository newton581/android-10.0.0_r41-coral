/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.x509;

import com.android.org.bouncycastle.asn1.ASN1Sequence;
import com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier;

/**
 * <pre>
 *     RDNSequence ::= SEQUENCE OF RelativeDistinguishedName
 *
 *     RelativeDistinguishedName ::= SET SIZE (1..MAX) OF AttributeTypeAndValue
 *
 *     AttributeTypeAndValue ::= SEQUENCE {
 *                                   type  OBJECT IDENTIFIER,
 *                                   value ANY }
 * </pre>
 * @deprecated use org.bouncycastle.asn1.x500.X500Name.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class X509Name extends com.android.org.bouncycastle.asn1.ASN1Object {

/**
 * Takes an X509 dir name as a string of the format "C=AU, ST=Victoria", or
 * some such, converting it into an ordered set of name attributes.
 * @deprecated use X500Name, X500NameBuilder
 */

@Deprecated
public X509Name(java.lang.String dirName) { throw new RuntimeException("Stub!"); }

@Deprecated
public static com.android.org.bouncycastle.asn1.x509.X509Name getInstance(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * return a vector of the oids in the name, in the order they were found.
 */

@Deprecated
public java.util.Vector getOIDs() { throw new RuntimeException("Stub!"); }

/**
 * return a vector of the values found in the name, in the order they
 * were found.
 */

@Deprecated
public java.util.Vector getValues() { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * test for equality - note: case is ignored.
 */

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * convert the structure to a string - if reverse is true the
 * oids and values are listed out starting with the last element
 * in the sequence (ala RFC 2253), otherwise the string will begin
 * with the first element of the structure. If no string definition
 * for the oid is found in oidSymbols the string value of the oid is
 * added. Two standard symbol tables are provided DefaultSymbols, and
 * RFC2253Symbols as part of this class.
 *
 * @param reverse if true start at the end of the sequence and work back.
 * @param oidSymbols look up table strings for oids.
 */

@Deprecated
public java.lang.String toString(boolean reverse, java.util.Hashtable oidSymbols) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * common name - StringType(SIZE(1..64))
 * @deprecated use a X500NameStyle
 */

@Deprecated public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier CN;
static { CN = null; }

/**
 * default look up table translating OID values into their common symbols following
 * the convention in RFC 2253 with a few extras
 */

@Deprecated public static final java.util.Hashtable DefaultSymbols;
static { DefaultSymbols = null; }

/**
 * organization - StringType(SIZE(1..64))
 * @deprecated use a X500NameStyle
 */

@Deprecated public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier O;
static { O = null; }

/**
 * organizational unit name - StringType(SIZE(1..64))
 * @deprecated use a X500NameStyle
 */

@Deprecated public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier OU;
static { OU = null; }
}

