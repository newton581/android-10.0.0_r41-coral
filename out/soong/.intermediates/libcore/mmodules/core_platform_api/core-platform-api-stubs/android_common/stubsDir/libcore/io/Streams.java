/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.io;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.EOFException;

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class Streams {

Streams() { throw new RuntimeException("Stub!"); }

/**
 * Implements InputStream.read(int) in terms of InputStream.read(byte[], int, int).
 * InputStream assumes that you implement InputStream.read(int) and provides default
 * implementations of the others, but often the opposite is more efficient.
 */

public static int readSingleByte(java.io.InputStream in) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Implements OutputStream.write(int) in terms of OutputStream.write(byte[], int, int).
 * OutputStream assumes that you implement OutputStream.write(int) and provides default
 * implementations of the others, but often the opposite is more efficient.
 */

public static void writeSingleByte(java.io.OutputStream out, int b) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Fills 'dst' with bytes from 'in', throwing EOFException if insufficient bytes are available.
 */

public static void readFully(java.io.InputStream in, byte[] dst) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Returns a byte[] containing the remainder of 'in', closing it when done.
 */

public static byte[] readFully(java.io.InputStream in) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Returns a byte[] containing the remainder of 'in'.
 */

public static byte[] readFullyNoClose(java.io.InputStream in) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Returns the remainder of 'reader' as a string, closing it when done.
 */

public static java.lang.String readFully(java.io.Reader reader) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Skip <b>at most</b> {@code byteCount} bytes from {@code in} by calling read
 * repeatedly until either the stream is exhausted or we read fewer bytes than
 * we ask for.
 *
 * <p>This method reuses the skip buffer but is careful to never use it at
 * the same time that another stream is using it. Otherwise streams that use
 * the caller's buffer for consistency checks like CRC could be clobbered by
 * other threads. A thread-local buffer is also insufficient because some
 * streams may call other streams in their skip() method, also clobbering the
 * buffer.
 */

public static long skipByReading(java.io.InputStream in, long byteCount) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Copies all of the bytes from {@code in} to {@code out}. Neither stream is closed.
 * Returns the total number of bytes transferred.
 */

public static int copy(java.io.InputStream in, java.io.OutputStream out) throws java.io.IOException { throw new RuntimeException("Stub!"); }
}

