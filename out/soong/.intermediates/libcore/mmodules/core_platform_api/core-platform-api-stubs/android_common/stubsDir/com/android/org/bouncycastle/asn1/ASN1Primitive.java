/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;

import java.io.IOException;

/**
 * Base class for ASN.1 primitive objects. These are the actual objects used to generate byte encodings.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ASN1Primitive extends com.android.org.bouncycastle.asn1.ASN1Object {

ASN1Primitive() { throw new RuntimeException("Stub!"); }

/**
 * Create a base ASN.1 object from a byte stream.
 *
 * @param data the byte stream to parse.
 * @return the base ASN.1 object represented by the byte stream.
 * @exception IOException if there is a problem parsing the data, or parsing the stream did not exhaust the available data.
 */

public static com.android.org.bouncycastle.asn1.ASN1Primitive fromByteArray(byte[] data) throws java.io.IOException { throw new RuntimeException("Stub!"); }

public final boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

public com.android.org.bouncycastle.asn1.ASN1Primitive toASN1Primitive() { throw new RuntimeException("Stub!"); }

public abstract int hashCode();
}

