/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.util;


/**
 * Hexadecimal encoding where each byte is represented by two hexadecimal digits.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class HexEncoding {

/** Hidden constructor to prevent instantiation. */

HexEncoding() { throw new RuntimeException("Stub!"); }

/**
 * Encodes the provided data as a sequence of hexadecimal characters.
 */

public static char[] encode(byte[] data) { throw new RuntimeException("Stub!"); }

/**
 * Encodes the provided data as a sequence of hexadecimal characters.
 */

public static char[] encode(byte[] data, int offset, int len) { throw new RuntimeException("Stub!"); }

/**
 * Encodes the provided data as a sequence of hexadecimal characters.
 */

public static java.lang.String encodeToString(byte[] data) { throw new RuntimeException("Stub!"); }

/**
 * Decodes the provided hexadecimal string into a byte array.  Odd-length inputs
 * are not allowed.
 *
 * Throws an {@code IllegalArgumentException} if the input is malformed.
 */

public static byte[] decode(java.lang.String encoded) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Decodes the provided hexadecimal string into a byte array.  Odd-length inputs
 * are not allowed.
 *
 * Throws an {@code IllegalArgumentException} if the input is malformed.
 */

public static byte[] decode(char[] encoded) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }

/**
 * Decodes the provided hexadecimal string into a byte array. If {@code allowSingleChar}
 * is {@code true} odd-length inputs are allowed and the first character is interpreted
 * as the lower bits of the first result byte.
 *
 * Throws an {@code IllegalArgumentException} if the input is malformed.
 */

public static byte[] decode(char[] encoded, boolean allowSingleChar) throws java.lang.IllegalArgumentException { throw new RuntimeException("Stub!"); }
}

