/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.pkcs;


/**
 * pkcs-1 OBJECT IDENTIFIER ::=<p>
 *   { iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1) 1 }
 * @hide This class is not part of the Android public SDK API
 *
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface PKCSObjectIdentifiers {

/**  1.2.840.113549.3.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier RC2_CBC = null;

/** PKCS#12: 1.2.840.113549.1.12.10.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier bagtypes = null;

/** PKCS#9: 1.2.840.113549.1.9.15.2 -- smime capability  */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier canNotDecryptAny = null;

/** PKCS#12: 1.2.840.113549.1.12.10.1.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier certBag = null;

/** PKCS#9: 1.2.840.113549.1.9.22 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier certTypes = null;

/** PKCS#12: 1.2.840.113549.1.12.10.1.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier crlBag = null;

/** PKCS#9: 1.2.840.113549.1.9.23 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier crlTypes = null;

/** PKCS#7: 1.2.840.113549.1.7.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier data = null;

/**  1.2.840.113549.3.7 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier des_EDE3_CBC = null;

/** PKCS#3: 1.2.840.113549.1.3.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier dhKeyAgreement = null;

/**  1.2.840.113549.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier digestAlgorithm = null;

/** PKCS#7: 1.2.840.113549.1.7.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier digestedData = null;

/** PKCS#7: 1.2.840.113549.1.7.76 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier encryptedData = null;

/**  1.2.840.113549.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier encryptionAlgorithm = null;

/** PKCS#7: 1.2.840.113549.1.7.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier envelopedData = null;

/** PKCS#5: 1.2.840.113549.1.5.13 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_PBES2 = null;

/** PKCS#5: 1.2.840.113549.1.5.12 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_PBKDF2 = null;

/** PKCS#1: 1.2.840.113549.1.1.7 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_RSAES_OAEP = null;

/** PKCS#1: 1.2.840.113549.1.1.10 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_RSASSA_PSS = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2 - smime attributes */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.54 <a href="https://tools.ietf.org/html/rfc7030">RFC7030</a>*/

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_asymmDecryptKeyID = null;

/** RFC 6211 -  id-aa-cmsAlgorithmProtect OBJECT IDENTIFIER ::= {
 iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1)
 pkcs9(9) 52 }  */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_cmsAlgorithmProtect = null;

/** @deprecated use id_aa_ets_commitmentType instead */

@Deprecated public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_commitmentType = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.40   <a href="https://tools.ietf.org/html/rfc7030">RFC7030</a>*/

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_communityIdentifiers = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.4 - See <a href="http://tools.ietf.org/html/rfc2634">RFC 2634</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_contentHint = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.7 - See <a href="http://tools.ietf.org/html/rfc2634">RFC 2634</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_contentIdentifier = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.10 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_contentReference = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.37 - <a href="https://tools.ietf.org/html/rfc4108#section-2.2.5">RFC 4108</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_decryptKeyID = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.11 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_encrypKeyPref = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.27 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_archiveTimestamp = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.26 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_certCRLTimestamp = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.23 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_certValues = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.21 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_certificateRefs = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.16 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_commitmentType = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.20 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_contentTimestamp = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.25 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_escTimeStamp = null;

/** PKCS#9: 1.2.840.113549.1.9.16.6.2.19 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_otherSigCert = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.22 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_revocationRefs = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.24 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_revocationValues = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.15 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_sigPolicyId = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.18 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_signerAttr = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.17 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_ets_signerLocation = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.43   <a href="https://tools.ietf.org/html/rfc7030">RFC7030</a>*/

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_implCompressAlgs = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.38 - <a href="https://tools.ietf.org/html/rfc4108#section-2.2.6">RFC 4108</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_implCryptoAlgs = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_msgSigDigest = null;

/** @deprecated use id_aa_ets_otherSigCert instead */

@Deprecated public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_otherSigCert = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.1 -- smime attribute receiptRequest */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_receiptRequest = null;

/** @deprecated use id_aa_ets_sigPolicyId instead */

@Deprecated public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_sigPolicyId = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.14 - <a href="http://tools.ietf.org/html/rfc3126">RFC 3126</a> */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_signatureTimeStampToken = null;

/** @deprecated use id_aa_ets_signerLocation instead */

@Deprecated public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_signerLocation = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.12 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_signingCertificate = null;

/** PKCS#9: 1.2.840.113549.1.9.16.2.47 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_aa_signingCertificateV2 = null;

/** S/MIME: Algorithm Identifiers ; 1.2.840.113549.1.9.16.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_alg = null;

/** PKCS#9: 1.2.840.113549.1.9.16.3.6 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_alg_CMS3DESwrap = null;

/** PKCS#9: 1.2.840.113549.1.9.16.3.7 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_alg_CMSRC2wrap = null;

/** PKCS#9: 1.2.840.113549.1.9.16.3.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_alg_ESDH = null;

/** PKCS#9: 1.2.840.113549.1.9.16.3.9 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_alg_PWRI_KEK = null;

/** PKCS#9: 1.2.840.113549.1.9.16.3.10 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_alg_SSDH = null;

/** PKCS#9: 1.2.840.113549.1.9.16.1 -- smime ct */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_ct = null;

/** PKCS#9: 1.2.840.113549.1.9.16.1.4 -- smime ct TSTInfo*/

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_ct_TSTInfo = null;

/** PKCS#9: 1.2.840.113549.1.9.16.1.2 -- smime ct authData */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_ct_authData = null;

/** PKCS#9: 1.2.840.113549.1.9.16.1.23 -- smime ct authEnvelopedData */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_ct_authEnvelopedData = null;

/** PKCS#9: 1.2.840.113549.1.9.16.1.9 -- smime ct compressedData */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_ct_compressedData = null;

/** PKCS#9: 1.2.840.113549.1.9.16.1.31 -- smime ct timestampedData*/

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_ct_timestampedData = null;

/** PKCS#9: 1.2.840.113549.1.9.16.6 -- smime cti */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_cti = null;

/** PKCS#9: 1.2.840.113549.1.9.16.6.5 -- smime cti proofOfApproval */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_cti_ets_proofOfApproval = null;

/** PKCS#9: 1.2.840.113549.1.9.16.6.6 -- smime cti proofOfCreation */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_cti_ets_proofOfCreation = null;

/** PKCS#9: 1.2.840.113549.1.9.16.6.3 -- smime cti proofOfDelivery */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_cti_ets_proofOfDelivery = null;

/** PKCS#9: 1.2.840.113549.1.9.16.6.1 -- smime cti proofOfOrigin */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_cti_ets_proofOfOrigin = null;

/** PKCS#9: 1.2.840.113549.1.9.16.6.2 -- smime cti proofOfReceipt*/

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_cti_ets_proofOfReceipt = null;

/** PKCS#9: 1.2.840.113549.1.9.16.6.4 -- smime cti proofOfSender */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_cti_ets_proofOfSender = null;

/**  1.2.840.113549.2.7 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_hmacWithSHA1 = null;

/**  1.2.840.113549.2.8 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_hmacWithSHA224 = null;

/**  1.2.840.113549.2.9 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_hmacWithSHA256 = null;

/**  1.2.840.113549.2.10 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_hmacWithSHA384 = null;

/**  1.2.840.113549.2.11 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_hmacWithSHA512 = null;

/** PKCS#1: 1.2.840.113549.1.1.8 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_mgf1 = null;

/** PKCS#1: 1.2.840.113549.1.1.9 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_pSpecified = null;

/**
 * <pre>
 * -- RSA-KEM Key Transport Algorithm  RFC 5990
 *
 * id-rsa-kem OID ::= {
 *      iso(1) member-body(2) us(840) rsadsi(113549) pkcs(1)
 *      pkcs-9(9) smime(16) alg(3) 14
 *   }
 * </pre>
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_rsa_KEM = null;

/** PKCS#9: 1.2.840.113549.1.9.16 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_smime = null;

/**
 * id-spq OBJECT IDENTIFIER ::= {iso(1) member-body(2) usa(840)
 * rsadsi(113549) pkcs(1) pkcs-9(9) smime(16) id-spq(5)}; <p>
 * 1.2.840.113549.1.9.16.5
 */

public static final java.lang.String id_spq = "1.2.840.113549.1.9.16.5";

/** SMIME SPQ UNOTICE: 1.2.840.113549.1.9.16.5.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_spq_ets_unotice = null;

/** SMIME SPQ URI:     1.2.840.113549.1.9.16.5.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_spq_ets_uri = null;

/** PKCS#12: 1.2.840.113549.1.12.10.1.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier keyBag = null;

/**  1.2.840.113549.2.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier md5 = null;

/** PKCS#1: 1.2.840.113549.1.1.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier md5WithRSAEncryption = null;

/** PKCS#5: 1.2.840.113549.1.5.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithMD2AndDES_CBC = null;

/** PKCS#5: 1.2.840.113549.1.5.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithMD2AndRC2_CBC = null;

/** PKCS#5: 1.2.840.113549.1.5.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithMD5AndDES_CBC = null;

/** PKCS#5: 1.2.840.113549.1.5.6 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithMD5AndRC2_CBC = null;

/** PKCS#5: 1.2.840.113549.1.5.10 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithSHA1AndDES_CBC = null;

/** PKCS#5: 1.2.840.113549.1.5.11 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithSHA1AndRC2_CBC = null;

/** PKCS#12: 1.2.840.113549.1.12.1.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithSHAAnd128BitRC2_CBC = null;

/** PKCS#12: 1.2.840.113549.1.12.1.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithSHAAnd128BitRC4 = null;

/** PKCS#12: 1.2.840.113549.1.12.1.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithSHAAnd2_KeyTripleDES_CBC = null;

/** PKCS#12: 1.2.840.113549.1.12.1.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithSHAAnd3_KeyTripleDES_CBC = null;

/** PKCS#12: 1.2.840.113549.1.12.1.6 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithSHAAnd40BitRC2_CBC = null;

/** PKCS#12: 1.2.840.113549.1.12.1.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbeWithSHAAnd40BitRC4 = null;

/**
 * PKCS#12: 1.2.840.113549.1.12.1.6
 * @deprecated use pbeWithSHAAnd40BitRC2_CBC
 */

@Deprecated public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pbewithSHAAnd40BitRC2_CBC = null;

/** PKCS#12: 1.2.840.113549.1.12.10.1.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs8ShroudedKeyBag = null;

/** PKCS#1: 1.2.840.113549.1.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_1 = null;

/** PKCS#12: 1.2.840.113549.1.12 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_12 = null;

/** PKCS#12: 1.2.840.113549.1.12.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_12PbeIds = null;

/** PKCS#3: 1.2.840.113549.1.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_3 = null;

/** PKCS#5: 1.2.840.113549.1.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_5 = null;

/** pkcs#7: 1.2.840.113549.1.7 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_7 = null;

/** PKCS#9: 1.2.840.113549.1.9 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9 = null;

/** PKCS#9: 1.2.840.113549.1.9.7 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_challengePassword = null;

/** PKCS#9: 1.2.840.113549.1.9.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_contentType = null;

/** PKCS#9: 1.2.840.113549.1.9.6 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_counterSignature = null;

/** PKCS#9: 1.2.840.113549.1.9.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_emailAddress = null;

/** PKCS#9: 1.2.840.113549.1.9.9 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_extendedCertificateAttributes = null;

/** PKCS#9: 1.2.840.113549.1.9.14 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_extensionRequest = null;

/** PKCS#9: 1.2.840.113549.1.9.20 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_friendlyName = null;

/** PKCS#9: 1.2.840.113549.1.9.21 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_localKeyId = null;

/** PKCS#9: 1.2.840.113549.1.9.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_messageDigest = null;

/** PKCS#9: 1.2.840.113549.1.9.13 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_signingDescription = null;

/** PKCS#9: 1.2.840.113549.1.9.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_signingTime = null;

/** PKCS#9: 1.2.840.113549.1.9.15 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_smimeCapabilities = null;

/** PKCS#9: 1.2.840.113549.1.9.8 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_unstructuredAddress = null;

/** PKCS#9: 1.2.840.113549.1.9.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier pkcs_9_at_unstructuredName = null;

/** PKCS#9: 1.2.840.113549.1.9.15.1 -- smime capability */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier preferSignedData = null;

/**  1.2.840.113549.3.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier rc4 = null;

/** PKCS#1: 1.2.840.113549.1.1.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier rsaEncryption = null;

/** PKCS#9: 1.2.840.113549.1.9.15.3 -- smime capability  */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier sMIMECapabilitiesVersions = null;

/** PKCS#12: 1.2.840.113549.1.12.10.1.6 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier safeContentsBag = null;

/** PKCS#9: 1.2.840.113549.1.9.22.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier sdsiCertificate = null;

/** PKCS#12: 1.2.840.113549.1.12.10.1.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier secretBag = null;

/** PKCS#1: 1.2.840.113549.1.1.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier sha1WithRSAEncryption = null;

/** PKCS#1: 1.2.840.113549.1.1.14 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier sha224WithRSAEncryption = null;

/** PKCS#1: 1.2.840.113549.1.1.11 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier sha256WithRSAEncryption = null;

/** PKCS#1: 1.2.840.113549.1.1.12 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier sha384WithRSAEncryption = null;

/** PKCS#1: 1.2.840.113549.1.1.13 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier sha512WithRSAEncryption = null;

/** PKCS#1: 1.2.840.113549.1.1.15 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier sha512_224WithRSAEncryption = null;

/** PKCS#1: 1.2.840.113549.1.1.16 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier sha512_256WithRSAEncryption = null;

/** PKCS#7: 1.2.840.113549.1.7.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier signedAndEnvelopedData = null;

/** PKCS#7: 1.2.840.113549.1.7.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier signedData = null;

/** PKCS#1: 1.2.840.113549.1.1.6 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier srsaOAEPEncryptionSET = null;

/** PKCS#9: 1.2.840.113549.1.9.22.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier x509Certificate = null;

/** PKCS#9: 1.2.840.113549.1.9.23.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier x509Crl = null;

/** PKCS#9: 1.2.840.113549.1.9.22.1
 * @deprecated use x509Certificate instead */

@Deprecated public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier x509certType = null;
}

