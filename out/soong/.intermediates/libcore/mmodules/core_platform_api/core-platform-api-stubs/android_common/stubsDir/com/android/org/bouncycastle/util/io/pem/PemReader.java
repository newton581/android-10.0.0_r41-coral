/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.util.io.pem;

import java.io.IOException;

/**
 * A generic PEM reader, based on the format outlined in RFC 1421
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class PemReader extends java.io.BufferedReader {

public PemReader(java.io.Reader reader) { super((java.io.Reader)null); throw new RuntimeException("Stub!"); }

/**
 * Read the next PEM object as a blob of raw data with header information.
 *
 * @return the next object in the stream, null if no objects left.
 * @throws IOException in case of a parse error.
 */

public com.android.org.bouncycastle.util.io.pem.PemObject readPemObject() throws java.io.IOException { throw new RuntimeException("Stub!"); }
}

