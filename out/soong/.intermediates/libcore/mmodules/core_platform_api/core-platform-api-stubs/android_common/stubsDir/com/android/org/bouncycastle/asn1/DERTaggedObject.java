/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * DER TaggedObject - in ASN.1 notation this is any object preceded by
 * a [n] where n is some number - these are assumed to follow the construction
 * rules (as with sequences).
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DERTaggedObject extends com.android.org.bouncycastle.asn1.ASN1TaggedObject {

public DERTaggedObject(int tagNo, com.android.org.bouncycastle.asn1.ASN1Encodable encodable) { super(false, 0, null); throw new RuntimeException("Stub!"); }
}

