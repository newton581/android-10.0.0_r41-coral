/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * A BIT STRING with DER encoding - the first byte contains the count of padding bits included in the byte array's last byte.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DERBitString extends com.android.org.bouncycastle.asn1.ASN1BitString {

public DERBitString(byte[] data) { super(null, 0); throw new RuntimeException("Stub!"); }
}

