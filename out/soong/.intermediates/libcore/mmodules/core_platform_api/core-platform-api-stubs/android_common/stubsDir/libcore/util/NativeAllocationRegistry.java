/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */


package libcore.util;


/**
 * A NativeAllocationRegistry is used to associate native allocations with
 * Java objects and register them with the runtime.
 * There are two primary benefits of registering native allocations associated
 * with Java objects:
 * <ol>
 *  <li>The runtime will account for the native allocations when scheduling
 *  garbage collection to run.</li>
 *  <li>The runtime will arrange for the native allocation to be automatically
 *  freed by a user-supplied function when the associated Java object becomes
 *  unreachable.</li>
 * </ol>
 * A separate NativeAllocationRegistry should be instantiated for each kind
 * of native allocation, where the kind of a native allocation consists of the
 * native function used to free the allocation and the estimated size of the
 * allocation. Once a NativeAllocationRegistry is instantiated, it can be
 * used to register any number of native allocations of that kind.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class NativeAllocationRegistry {

/**
 * Constructs a NativeAllocationRegistry for a particular kind of native
 * allocation.
 * <p>
 * New code should use the preceding factory methods rather than calling this
 * constructor directly.
 * <p>
 * The <code>size</code> should be an estimate of the total number of
 * native bytes this kind of native allocation takes up excluding bytes allocated
 * with system malloc. Different
 * NativeAllocationRegistrys must be used to register native allocations
 * with different estimated sizes, even if they use the same
 * <code>freeFunction</code>. This is used to help inform the garbage
 * collector about the possible need for collection. Memory allocated with
 * native malloc is implicitly included, and ideally should not be included in this
 * argument.
 * <p>
 * @param classLoader  ClassLoader that was used to load the native
 *                     library freeFunction belongs to.
 * @param freeFunction address of a native function used to free this
 *                     kind of native allocation
 * @param size         estimated size in bytes of this kind of native
 *                     allocation, excluding memory allocated with system malloc.
 *                     A value of 0 indicates that the memory was allocated mainly
 *                     with malloc.
 *
 * @param mallocAllocation the native object is primarily allocated via malloc.
 */

public NativeAllocationRegistry(java.lang.ClassLoader classLoader, long freeFunction, long size) { throw new RuntimeException("Stub!"); }

/**
 * Return a NativeAllocationRegistry for native memory that is mostly
 * allocated by means other than the system memory allocator. For example,
 * the memory may be allocated directly with mmap.
 * @param classLoader  ClassLoader that was used to load the native
 *                     library defining freeFunction.
 *                     This ensures that the the native library isn't unloaded
 *                     before freeFunction is called.
 * @param freeFunction address of a native function of type
 *                     <code>void f(void* nativePtr)</code> used to free this
 *                     kind of native allocation
 * @param size         estimated size in bytes of the part of the described
 *                     native memory that is not allocated with system malloc.
 *                     Approximate values are acceptable.
 * @throws IllegalArgumentException If <code>size</code> is negative
 */

public static libcore.util.NativeAllocationRegistry createNonmalloced(java.lang.ClassLoader classLoader, long freeFunction, long size) { throw new RuntimeException("Stub!"); }

/**
 * Return a NativeAllocationRegistry for native memory that is mostly
 * allocated by the system memory allocator.
 * For example, the memory may be allocated directly with new or malloc.
 * <p>
 * The native function should have the type:
 * <pre>
 *    void f(void* nativePtr);
 * </pre>
 * <p>
 * @param classLoader  ClassLoader that was used to load the native
 *                     library freeFunction belongs to.
 * @param freeFunction address of a native function of type
 *                     <code>void f(void* nativePtr)</code> used to free this
 *                     kind of native allocation
 * @param size         estimated size in bytes of the part of the described
 *                     native memory allocated with system malloc.
 *                     Approximate values are acceptable. For sizes less than
 *                     a few hundered KB, use the simplified overload below.
 * @throws IllegalArgumentException If <code>size</code> is negative
 */

public static libcore.util.NativeAllocationRegistry createMalloced(java.lang.ClassLoader classLoader, long freeFunction, long size) { throw new RuntimeException("Stub!"); }

/**
 * Return a NativeAllocationRegistry for native memory that is mostly
 * allocated by the system memory allocator. This version is preferred
 * for smaller objects (typically less than a few hundred KB).
 * @param classLoader  ClassLoader that was used to load the native
 *                     library freeFunction belongs to.
 * @param freeFunction address of a native function of type
 *                     <code>void f(void* nativePtr)</code> used to free this
 *                     kind of native allocation
 */

public static libcore.util.NativeAllocationRegistry createMalloced(java.lang.ClassLoader classLoader, long freeFunction) { throw new RuntimeException("Stub!"); }

/**
 * Registers a new native allocation and associated Java object with the
 * runtime.
 * This NativeAllocationRegistry's <code>freeFunction</code> will
 * automatically be called with <code>nativePtr</code> as its sole
 * argument when <code>referent</code> becomes unreachable. If you
 * maintain copies of <code>nativePtr</code> outside
 * <code>referent</code>, you must not access these after
 * <code>referent</code> becomes unreachable, because they may be dangling
 * pointers.
 * <p>
 * The returned Runnable can be used to free the native allocation before
 * <code>referent</code> becomes unreachable. The runnable will have no
 * effect if the native allocation has already been freed by the runtime
 * or by using the runnable.
 * <p>
 * WARNING: This unconditionally takes ownership, i.e. deallocation
 * responsibility of nativePtr. nativePtr will be DEALLOCATED IMMEDIATELY
 * if the registration attempt throws an exception (other than one reporting
 * a programming error).
 *
 * @param referent      Non-null java object to associate the native allocation with
 * @param nativePtr     Non-zero address of the native allocation
 * @return runnable to explicitly free native allocation
 * @throws IllegalArgumentException if either referent or nativePtr is null.
 * @throws OutOfMemoryError  if there is not enough space on the Java heap
 *                           in which to register the allocation. In this
 *                           case, <code>freeFunction</code> will be
 *                           called with <code>nativePtr</code> as its
 *                           argument before the OutOfMemoryError is
 *                           thrown.
 */

public java.lang.Runnable registerNativeAllocation(java.lang.Object referent, long nativePtr) { throw new RuntimeException("Stub!"); }

/**
 * Calls <code>freeFunction</code>(<code>nativePtr</code>).
 * Provided as a convenience in the case where you wish to manually free a
 * native allocation using a <code>freeFunction</code> without using a
 * NativeAllocationRegistry.
 */

public static native void applyFreeFunction(long freeFunction, long nativePtr);
}

