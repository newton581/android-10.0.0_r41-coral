/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package android.system;


/**
 * A UNIX-domain (AF_UNIX / AF_LOCAL) socket address.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class UnixSocketAddress extends java.net.SocketAddress {

/** This constructor is also used from JNI. */

UnixSocketAddress(byte[] sun_path) { throw new RuntimeException("Stub!"); }

/**
 * Creates a named, filesystem AF_UNIX socket address.
 */

public static android.system.UnixSocketAddress createFileSystem(java.lang.String pathName) { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

