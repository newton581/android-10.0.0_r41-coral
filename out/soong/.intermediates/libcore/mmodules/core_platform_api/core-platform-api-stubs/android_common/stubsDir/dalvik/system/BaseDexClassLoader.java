/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package dalvik.system;

import java.io.File;

/**
 * Base class for common functionality between various dex-based
 * {@link ClassLoader} implementations.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class BaseDexClassLoader extends java.lang.ClassLoader {

/**
 * Constructs an instance.
 * Note that all the *.jar and *.apk files from {@code dexPath} might be
 * first extracted in-memory before the code is loaded. This can be avoided
 * by passing raw dex files (*.dex) in the {@code dexPath}.
 *
 * @param dexPath the list of jar/apk files containing classes and
 * resources, delimited by {@code File.pathSeparator}, which
 * defaults to {@code ":"} on Android.
 * @param optimizedDirectory this parameter is deprecated and has no effect since API level 26.
 * @param librarySearchPath the list of directories containing native
 * libraries, delimited by {@code File.pathSeparator}; may be
 * {@code null}
 * @param parent the parent class loader
 */

public BaseDexClassLoader(java.lang.String dexPath, java.io.File optimizedDirectory, java.lang.String librarySearchPath, java.lang.ClassLoader parent) { throw new RuntimeException("Stub!"); }

protected java.lang.Class<?> findClass(java.lang.String name) throws java.lang.ClassNotFoundException { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public void addDexPath(java.lang.String dexPath) { throw new RuntimeException("Stub!"); }

/**
 * Adds additional native paths for consideration in subsequent calls to
 * {@link #findLibrary(String)}
 * @hide
 */

public void addNativePath(java.util.Collection<java.lang.String> libPaths) { throw new RuntimeException("Stub!"); }

protected java.net.URL findResource(java.lang.String name) { throw new RuntimeException("Stub!"); }

protected java.util.Enumeration<java.net.URL> findResources(java.lang.String name) { throw new RuntimeException("Stub!"); }

public java.lang.String findLibrary(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * Returns package information for the given package.
 * Unfortunately, instances of this class don't really have this
 * information, and as a non-secure {@code ClassLoader}, it isn't
 * even required to, according to the spec. Yet, we want to
 * provide it, in order to make all those hopeful callers of
 * {@code myClass.getPackage().getName()} happy. Thus we construct
 * a {@code Package} object the first time it is being requested
 * and fill most of the fields with dummy values. The {@code
 * Package} object is then put into the {@code ClassLoader}'s
 * package cache, so we see the same one next time. We don't
 * create {@code Package} objects for {@code null} arguments or
 * for the default package.
 *
 * <p>There is a limited chance that we end up with multiple
 * {@code Package} objects representing the same package: It can
 * happen when when a package is scattered across different JAR
 * files which were loaded by different {@code ClassLoader}
 * instances. This is rather unlikely, and given that this whole
 * thing is more or less a workaround, probably not worth the
 * effort to address.
 *
 * @param name the name of the class
 * @return the package information for the class, or {@code null}
 * if there is no package information available for it
 */

protected synchronized java.lang.Package getPackage(java.lang.String name) { throw new RuntimeException("Stub!"); }

/**
 * @hide
 */

public java.lang.String getLdLibraryPath() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * Sets the reporter for dex load notifications.
 * Once set, all new instances of BaseDexClassLoader will report upon
 * constructions the loaded dex files.
 *
 * @param newReporter the new Reporter. Setting null will cancel reporting.
 * @hide
 */

public static void setReporter(dalvik.system.BaseDexClassLoader.Reporter newReporter) { throw new RuntimeException("Stub!"); }
/**
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface Reporter {

/**
 * Reports the construction of a BaseDexClassLoader and provides information about the
 * class loader chain.
 *
 * @param classLoadersChain the chain of class loaders used during the construction of the
 *     class loader. The first element is the BaseDexClassLoader being constructed,
 *     the second element is its parent, and so on.
 * @param classPaths the class paths of the class loaders present in
 *     {@param classLoadersChain}. The first element corresponds to the first class
 *     loader and so on. A classpath is represented as a list of dex files separated by
 *     {@code File.pathSeparator}. If the class loader is not a BaseDexClassLoader the
 *     classpath will be null.
 */

public void report(java.util.List<java.lang.ClassLoader> classLoadersChain, java.util.List<java.lang.String> classPaths);
}

}

