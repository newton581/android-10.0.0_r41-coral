/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.io;

import java.io.FileDescriptor;
import java.io.File;

/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class IoUtils {

IoUtils() { throw new RuntimeException("Stub!"); }

/**
 * Acquires ownership of an integer file descriptor from a FileDescriptor.
 *
 * This method invalidates the FileDescriptor passed in.
 *
 * The important part of this function is that you are taking ownership of a resource that you
 * must either clean up yourself, or hand off to some other object that does that for you.
 *
 * See bionic/include/android/fdsan.h for more details.
 *
 * @param fd FileDescriptor to take ownership from, must be non-null.
 * @throws NullPointerException if fd is null
 */

public static int acquireRawFd(java.io.FileDescriptor fd) { throw new RuntimeException("Stub!"); }

/**
 * Assigns ownership of an unowned FileDescriptor.
 *
 * Associates the supplied FileDescriptor and the underlying Unix file descriptor with an owner
 * ID derived from the supplied {@code owner} object. If the FileDescriptor already has an
 * associated owner an {@link IllegalStateException} will be thrown. If the underlying Unix
 * file descriptor already has an associated owner, the process will abort.
 *
 * See bionic/include/android/fdsan.h for more details.
 *
 * @param fd FileDescriptor to take ownership from, must be non-null.
 * @throws NullPointerException if fd or owner are null
 * @throws IllegalStateException if fd is already owned
 */

public static void setFdOwner(java.io.FileDescriptor fd, java.lang.Object owner) { throw new RuntimeException("Stub!"); }

/**
 * Calls close(2) on 'fd'. Also resets the internal int to -1. Does nothing if 'fd' is null
 * or invalid.
 */

public static void close(java.io.FileDescriptor fd) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Closes 'closeable', ignoring any checked exceptions. Does nothing if 'closeable' is null.
 */

public static void closeQuietly(java.lang.AutoCloseable closeable) { throw new RuntimeException("Stub!"); }

/**
 * Closes 'fd', ignoring any exceptions. Does nothing if 'fd' is null or invalid.
 */

public static void closeQuietly(java.io.FileDescriptor fd) { throw new RuntimeException("Stub!"); }

/**
 * Closes 'socket', ignoring any exceptions. Does nothing if 'socket' is null.
 */

public static void closeQuietly(java.net.Socket socket) { throw new RuntimeException("Stub!"); }

/**
 * Sets 'fd' to be blocking or non-blocking, according to the state of 'blocking'.
 */

public static void setBlocking(java.io.FileDescriptor fd, boolean blocking) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Returns the contents of 'path' as a byte array.
 */

public static byte[] readFileAsByteArray(java.lang.String absolutePath) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Returns the contents of 'path' as a string. The contents are assumed to be UTF-8.
 */

public static java.lang.String readFileAsString(java.lang.String absolutePath) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Do not use. Use createTemporaryDirectory instead.
 *
 * Used by frameworks/base unit tests to clean up a temporary directory.
 * Deliberately ignores errors, on the assumption that test cleanup is only
 * supposed to be best-effort.
 *
 * @deprecated Use {@link TestIoUtils#createTemporaryDirectory} instead.
 */

@Deprecated
public static void deleteContents(java.io.File dir) throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Creates a unique new temporary directory under "java.io.tmpdir".
 *
 * @deprecated Use {@link TestIoUtils#createTemporaryDirectory} instead.
 */

@Deprecated
public static java.io.File createTemporaryDirectory(java.lang.String prefix) { throw new RuntimeException("Stub!"); }
}

