/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.util.io.pem;


/**
 * A generic PEM object - type, header properties, and byte content.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class PemObject implements com.android.org.bouncycastle.util.io.pem.PemObjectGenerator {

/**
 * Generic constructor for object without headers.
 *
 * @param type pem object type.
 * @param content the binary content of the object.
 */

public PemObject(java.lang.String type, byte[] content) { throw new RuntimeException("Stub!"); }

/**
 * Generic constructor for object with headers.
 *
 * @param type pem object type.
 * @param headers a list of PemHeader objects.
 * @param content the binary content of the object.
 */

public PemObject(java.lang.String type, java.util.List headers, byte[] content) { throw new RuntimeException("Stub!"); }

public java.lang.String getType() { throw new RuntimeException("Stub!"); }

public byte[] getContent() { throw new RuntimeException("Stub!"); }
}

