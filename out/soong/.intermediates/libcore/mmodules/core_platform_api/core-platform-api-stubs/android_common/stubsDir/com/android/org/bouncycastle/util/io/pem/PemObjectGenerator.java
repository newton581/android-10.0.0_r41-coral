/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.util.io.pem;


/**
 * Base interface for generators of PEM objects.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface PemObjectGenerator {
}

