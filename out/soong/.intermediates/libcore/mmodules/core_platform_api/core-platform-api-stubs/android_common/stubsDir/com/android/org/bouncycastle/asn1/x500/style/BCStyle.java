/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.x500.style;

import java.util.Hashtable;
import com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier;

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class BCStyle {

BCStyle() { throw new RuntimeException("Stub!"); }

/**
 * Tool function to shallow copy a Hashtable.
 *
 * @param paramsMap table to copy
 * @return the copy of the table
 */

public static java.util.Hashtable copyHashTable(java.util.Hashtable paramsMap) { throw new RuntimeException("Stub!"); }

/**
 * For all string values starting with '#' is assumed, that these are
 * already valid ASN.1 objects encoded in hex.
 * <p>
 * All other string values are send to
 * {@link AbstractX500NameStyle#encodeStringValue(ASN1ObjectIdentifier, String)}.
 * </p>
 * Subclasses should overwrite
 * {@link AbstractX500NameStyle#encodeStringValue(ASN1ObjectIdentifier, String)}
 * to change the encoding of specific types.
 *
 * @param oid the DN name of the value.
 * @param value the String representation of the value.
 */

public com.android.org.bouncycastle.asn1.ASN1Encodable stringToValue(com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier oid, java.lang.String value) { throw new RuntimeException("Stub!"); }

/**
 * country code - StringType(SIZE(2))
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier C;
static { C = null; }

/**
 * common name - StringType(SIZE(1..64))
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier CN;
static { CN = null; }

/**
 * email address in Verisign certificates
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier E;
static { E = null; }

/**
 * locality name - StringType(SIZE(1..64))
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier L;
static { L = null; }

/**
 * organization - StringType(SIZE(1..64))
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier O;
static { O = null; }

/**
 * organizational unit name - StringType(SIZE(1..64))
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier OU;
static { OU = null; }

/**
 * state, or province name - StringType(SIZE(1..64))
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ST;
static { ST = null; }
}

