/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * Base class for BIT STRING objects
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ASN1BitString extends com.android.org.bouncycastle.asn1.ASN1Primitive {

/**
 * Base constructor.
 *
 * @param data the octets making up the bit string.
 * @param padBits the number of extra bits at the end of the string.
 */

ASN1BitString(byte[] data, int padBits) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }
}

