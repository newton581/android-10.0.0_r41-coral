/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.pkcs;

import com.android.org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import java.io.IOException;

/**
 * RFC 5958
 *
 * <pre>
 *  [IMPLICIT TAGS]
 *
 *  OneAsymmetricKey ::= SEQUENCE {
 *      version                   Version,
 *      privateKeyAlgorithm       PrivateKeyAlgorithmIdentifier,
 *      privateKey                PrivateKey,
 *      attributes            [0] Attributes OPTIONAL,
 *      ...,
 *      [[2: publicKey        [1] PublicKey OPTIONAL ]],
 *      ...
 *  }
 *
 *  PrivateKeyInfo ::= OneAsymmetricKey
 *
 *  Version ::= INTEGER { v1(0), v2(1) } (v1, ..., v2)
 *
 *  PrivateKeyAlgorithmIdentifier ::= AlgorithmIdentifier
 *                                     { PUBLIC-KEY,
 *                                       { PrivateKeyAlgorithms } }
 *
 *  PrivateKey ::= OCTET STRING
 *                     -- Content varies based on type of key.  The
 *                     -- algorithm identifier dictates the format of
 *                     -- the key.
 *
 *  PublicKey ::= BIT STRING
 *                     -- Content varies based on type of key.  The
 *                     -- algorithm identifier dictates the format of
 *                     -- the key.
 *
 *  Attributes ::= SET OF Attribute { { OneAsymmetricKeyAttributes } }
 *  </pre>
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class PrivateKeyInfo extends com.android.org.bouncycastle.asn1.ASN1Object {

PrivateKeyInfo(com.android.org.bouncycastle.asn1.ASN1Sequence seq) { throw new RuntimeException("Stub!"); }

public static com.android.org.bouncycastle.asn1.pkcs.PrivateKeyInfo getInstance(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

public com.android.org.bouncycastle.asn1.x509.AlgorithmIdentifier getPrivateKeyAlgorithm() { throw new RuntimeException("Stub!"); }

public com.android.org.bouncycastle.asn1.ASN1Encodable parsePrivateKey() throws java.io.IOException { throw new RuntimeException("Stub!"); }
}

