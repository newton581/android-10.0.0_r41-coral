/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.io;


/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface Os {

/**
 * Atomically sets the system's default {@link Os} implementation to be
 * {@code update} if the current value {@code == expect}.
 *
 * @param expect the expected value.
 * @param update the new value to set; must not be null.
 * @return whether the update was successful.
 */

public static boolean compareAndSetDefault(libcore.io.Os expect, libcore.io.Os update) { throw new RuntimeException("Stub!"); }

/**
 * @return the system's default {@link Os} implementation currently in use.
 */

public static libcore.io.Os getDefault() { throw new RuntimeException("Stub!"); }
}

