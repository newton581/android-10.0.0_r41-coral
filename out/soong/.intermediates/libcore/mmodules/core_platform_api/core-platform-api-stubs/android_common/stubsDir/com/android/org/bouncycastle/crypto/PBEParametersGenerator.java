/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.crypto;


/**
 * super class for all Password Based Encryption (PBE) parameter generator classes.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class PBEParametersGenerator {

/**
 * base constructor.
 */

protected PBEParametersGenerator() { throw new RuntimeException("Stub!"); }

/**
 * converts a password to a byte array according to the scheme in
 * PKCS5 (ascii, no padding)
 *
 * @param password a character array representing the password.
 * @return a byte array representing the password.
 */

public static byte[] PKCS5PasswordToBytes(char[] password) { throw new RuntimeException("Stub!"); }
}

