/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.crypto.generators;

import com.android.org.bouncycastle.crypto.params.KeyParameter;

/**
 * Generator for PBE derived keys and ivs as usd by OpenSSL.
 * <p>
 * The scheme is a simple extension of PKCS 5 V2.0 Scheme 1 using MD5 with an
 * iteration count of 1.
 * <p>
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class OpenSSLPBEParametersGenerator extends com.android.org.bouncycastle.crypto.PBEParametersGenerator {

/**
 * Construct a OpenSSL Parameters generator.
 */

public OpenSSLPBEParametersGenerator() { throw new RuntimeException("Stub!"); }

/**
 * Initialise - note the iteration count for this algorithm is fixed at 1.
 *
 * @param password password to use.
 * @param salt salt to use.
 */

public void init(byte[] password, byte[] salt) { throw new RuntimeException("Stub!"); }

/**
 * Generate a key parameter derived from the password, salt, and iteration
 * count we are currently initialised with.
 *
 * @param keySize the size of the key we want (in bits)
 * @return a KeyParameter object.
 * @exception IllegalArgumentException if the key length larger than the base hash size.
 */

public com.android.org.bouncycastle.crypto.CipherParameters generateDerivedParameters(int keySize) { throw new RuntimeException("Stub!"); }
}

