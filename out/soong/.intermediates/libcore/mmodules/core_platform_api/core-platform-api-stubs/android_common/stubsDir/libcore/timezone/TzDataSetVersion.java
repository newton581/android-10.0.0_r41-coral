/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.timezone;

import java.io.File;

/**
 * Constants and logic associated with the time zone data version file.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class TzDataSetVersion {

public TzDataSetVersion(int formatMajorVersion, int formatMinorVersion, java.lang.String rulesVersion, int revision) throws libcore.timezone.TzDataSetVersion.TzDataSetException { throw new RuntimeException("Stub!"); }

/**
 * Returns the major tz data format version supported by this device.
 */

public static int currentFormatMajorVersion() { throw new RuntimeException("Stub!"); }

/**
 * Returns the minor tz data format version supported by this device.
 */

public static int currentFormatMinorVersion() { throw new RuntimeException("Stub!"); }

public static libcore.timezone.TzDataSetVersion readFromFile(java.io.File file) throws java.io.IOException, libcore.timezone.TzDataSetVersion.TzDataSetException { throw new RuntimeException("Stub!"); }

public byte[] toBytes() { throw new RuntimeException("Stub!"); }

public static boolean isCompatibleWithThisDevice(libcore.timezone.TzDataSetVersion tzDataVersion) { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/**
 * The name typically given to the {@link TzDataSetVersion} file. See
 * {@link TzDataSetVersion#readFromFile(File)}.
 */

public static final java.lang.String DEFAULT_FILE_NAME = "tz_version";

public final java.lang.String rulesVersion;
{ rulesVersion = null; }
/**
 * A checked exception used in connection with time zone data sets.
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class TzDataSetException extends java.lang.Exception {

public TzDataSetException(java.lang.String message) { throw new RuntimeException("Stub!"); }

public TzDataSetException(java.lang.String message, java.lang.Throwable cause) { throw new RuntimeException("Stub!"); }
}

}

