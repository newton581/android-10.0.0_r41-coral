/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.jcajce.util;


/**
 * {@link JcaJceHelper} that obtains all algorithms using the default JCA/JCE mechanism (i.e.
 * without specifying a provider).
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DefaultJcaJceHelper {

DefaultJcaJceHelper() { throw new RuntimeException("Stub!"); }

public javax.crypto.Cipher createCipher(java.lang.String algorithm) throws java.security.NoSuchAlgorithmException, javax.crypto.NoSuchPaddingException { throw new RuntimeException("Stub!"); }
}

