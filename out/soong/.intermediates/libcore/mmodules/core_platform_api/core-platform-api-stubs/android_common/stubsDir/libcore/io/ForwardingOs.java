/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.io;


/**
 * Subclass this if you want to override some {@link Os} methods but otherwise delegate.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ForwardingOs implements libcore.io.Os {

protected ForwardingOs(libcore.io.Os os) { throw new RuntimeException("Stub!"); }

public boolean access(java.lang.String path, int mode) throws android.system.ErrnoException { throw new RuntimeException("Stub!"); }

public java.io.FileDescriptor open(java.lang.String path, int flags, int mode) throws android.system.ErrnoException { throw new RuntimeException("Stub!"); }

public void remove(java.lang.String path) throws android.system.ErrnoException { throw new RuntimeException("Stub!"); }

public void rename(java.lang.String oldPath, java.lang.String newPath) throws android.system.ErrnoException { throw new RuntimeException("Stub!"); }

public android.system.StructStat stat(java.lang.String path) throws android.system.ErrnoException { throw new RuntimeException("Stub!"); }

public void unlink(java.lang.String pathname) throws android.system.ErrnoException { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

