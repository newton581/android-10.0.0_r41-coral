/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.util.io.pem;


/**
 * Class representing a PEM header (name, value) pair.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class PemHeader {

/**
 * Base constructor.
 *
 * @param name name of the header property.
 * @param value value of the header property.
 */

public PemHeader(java.lang.String name, java.lang.String value) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }
}

