/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.jce;

import com.android.org.bouncycastle.asn1.x509.X509Name;

/**
 * a general extension of X509Name with a couple of extra methods and
 * constructors.
 * <p>
 * Objects of this type can be created from certificates and CRLs using the
 * PrincipalUtil class.
 * </p>
 * @see com.android.org.bouncycastle.jce.PrincipalUtil
 * @deprecated use the X500Name class.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class X509Principal extends com.android.org.bouncycastle.asn1.x509.X509Name implements java.security.Principal {

/**
 * Constructor from an encoded byte array.
 */

@Deprecated
public X509Principal(byte[] bytes) throws java.io.IOException { super(null); throw new RuntimeException("Stub!"); }

/**
 * constructor from a table of attributes and a vector giving the
 * specific ordering required for encoding or conversion to a string.
 * <p>
 * it's is assumed the table contains OID/String pairs.
 */

@Deprecated
public X509Principal(java.util.Vector ordering, java.util.Hashtable attributes) { super(null); throw new RuntimeException("Stub!"); }

public java.lang.String getName() { throw new RuntimeException("Stub!"); }

/**
 * return a DER encoded byte array representing this object
 */

public byte[] getEncoded() { throw new RuntimeException("Stub!"); }
}

