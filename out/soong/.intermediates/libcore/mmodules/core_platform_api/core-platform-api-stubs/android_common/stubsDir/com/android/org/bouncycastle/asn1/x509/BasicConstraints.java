/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.x509;


/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class BasicConstraints extends com.android.org.bouncycastle.asn1.ASN1Object {

BasicConstraints(com.android.org.bouncycastle.asn1.ASN1Sequence seq) { throw new RuntimeException("Stub!"); }

public static com.android.org.bouncycastle.asn1.x509.BasicConstraints getInstance(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

public boolean isCA() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

