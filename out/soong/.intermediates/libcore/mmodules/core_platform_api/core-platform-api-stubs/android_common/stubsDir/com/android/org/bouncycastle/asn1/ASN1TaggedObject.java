/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * ASN.1 TaggedObject - in ASN.1 notation this is any object preceded by
 * a [n] where n is some number - these are assumed to follow the construction
 * rules (as with sequences).
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ASN1TaggedObject extends com.android.org.bouncycastle.asn1.ASN1Primitive implements com.android.org.bouncycastle.asn1.ASN1Encodable {

/**
 * Create a tagged object with the style given by the value of explicit.
 * <p>
 * If the object implements ASN1Choice the tag style will always be changed
 * to explicit in accordance with the ASN.1 encoding rules.
 * </p>
 * @param explicit true if the object is explicitly tagged.
 * @param tagNo the tag number for this object.
 * @param obj the tagged object.
 */

ASN1TaggedObject(boolean explicit, int tagNo, com.android.org.bouncycastle.asn1.ASN1Encodable obj) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * Return whatever was following the tag.
 * <p>
 * Note: tagged objects are generally context dependent if you're
 * trying to extract a tagged object you should be going via the
 * appropriate getInstance method.
 */

public com.android.org.bouncycastle.asn1.ASN1Primitive getObject() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

