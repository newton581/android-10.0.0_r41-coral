/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;

import java.io.IOException;

/**
 * A general purpose ASN.1 decoder - note: this class differs from the
 * others in that it returns null after it has read the last object in
 * the stream. If an ASN.1 NULL is encountered a DER/BER Null object is
 * returned.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ASN1InputStream extends java.io.FilterInputStream {

public ASN1InputStream(java.io.InputStream is) { super(null); throw new RuntimeException("Stub!"); }

/**
 * Create an ASN1InputStream based on the input byte array. The length of DER objects in
 * the stream is automatically limited to the length of the input array.
 *
 * @param input array containing ASN.1 encoded data.
 */

public ASN1InputStream(byte[] input) { super(null); throw new RuntimeException("Stub!"); }

public com.android.org.bouncycastle.asn1.ASN1Primitive readObject() throws java.io.IOException { throw new RuntimeException("Stub!"); }

public static final int APPLICATION = 64; // 0x40

public static final int BIT_STRING = 3; // 0x3

public static final int BMP_STRING = 30; // 0x1e

public static final int BOOLEAN = 1; // 0x1

public static final int CONSTRUCTED = 32; // 0x20

public static final int ENUMERATED = 10; // 0xa

public static final int EXTERNAL = 8; // 0x8

public static final int GENERALIZED_TIME = 24; // 0x18

public static final int GENERAL_STRING = 27; // 0x1b

public static final int GRAPHIC_STRING = 25; // 0x19

public static final int IA5_STRING = 22; // 0x16

public static final int INTEGER = 2; // 0x2

public static final int NULL = 5; // 0x5

public static final int NUMERIC_STRING = 18; // 0x12

public static final int OBJECT_IDENTIFIER = 6; // 0x6

public static final int OCTET_STRING = 4; // 0x4

public static final int PRINTABLE_STRING = 19; // 0x13

public static final int SEQUENCE = 16; // 0x10

public static final int SEQUENCE_OF = 16; // 0x10

public static final int SET = 17; // 0x11

public static final int SET_OF = 17; // 0x11

public static final int T61_STRING = 20; // 0x14

public static final int TAGGED = 128; // 0x80

public static final int UNIVERSAL_STRING = 28; // 0x1c

public static final int UTC_TIME = 23; // 0x17

public static final int UTF8_STRING = 12; // 0xc

public static final int VIDEOTEX_STRING = 21; // 0x15

public static final int VISIBLE_STRING = 26; // 0x1a
}

