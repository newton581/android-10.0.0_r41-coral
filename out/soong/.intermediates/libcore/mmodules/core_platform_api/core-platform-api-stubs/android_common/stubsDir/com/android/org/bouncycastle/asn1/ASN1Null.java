/* GENERATED SOURCE. DO NOT MODIFY. */
/***************************************************************/
/******    DO NOT EDIT THIS CLASS bc-java SOURCE FILE     ******/
/***************************************************************/

package com.android.org.bouncycastle.asn1;


/**
 * A NULL object - use DERNull.INSTANCE for populating structures.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ASN1Null extends com.android.org.bouncycastle.asn1.ASN1Primitive {

ASN1Null() { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

