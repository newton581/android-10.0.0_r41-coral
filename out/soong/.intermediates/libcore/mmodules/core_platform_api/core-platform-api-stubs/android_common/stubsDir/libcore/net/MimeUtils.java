/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.net;


/**
 * Utilities for dealing with MIME types.
 * Used to implement java.net.URLConnection and android.webkit.MimeTypeMap.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class MimeUtils {

MimeUtils() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if the given case insensitive MIME type has an entry in the map.
 * @param mimeType A MIME type (i.e. text/plain)
 * @return True if a extension has been registered for
 * the given case insensitive MIME type.
 */

public static boolean hasMimeType(java.lang.String mimeType) { throw new RuntimeException("Stub!"); }

/**
 * Returns the MIME type for the given case insensitive file extension.
 * @param extension A file extension without the leading '.'
 * @return The MIME type has been registered for
 * the given case insensitive file extension or null if there is none.
 */

public static java.lang.String guessMimeTypeFromExtension(java.lang.String extension) { throw new RuntimeException("Stub!"); }

/**
 * Returns true if the given case insensitive extension has a registered MIME type.
 * @param extension A file extension without the leading '.'
 * @return True if a MIME type has been registered for
 * the given case insensitive file extension.
 */

public static boolean hasExtension(java.lang.String extension) { throw new RuntimeException("Stub!"); }

/**
 * Returns the registered extension for the given case insensitive MIME type. Note that some
 * MIME types map to multiple extensions. This call will return the most
 * common extension for the given MIME type.
 * @param mimeType A MIME type (i.e. text/plain)
 * @return The extension has been registered for
 * the given case insensitive MIME type or null if there is none.
 */

public static java.lang.String guessExtensionFromMimeType(java.lang.String mimeType) { throw new RuntimeException("Stub!"); }
}

