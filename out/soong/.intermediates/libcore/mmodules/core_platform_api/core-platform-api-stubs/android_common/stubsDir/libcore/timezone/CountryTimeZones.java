/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.timezone;

import java.util.List;
import android.icu.util.TimeZone;

/**
 * Information about a country's time zones.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class CountryTimeZones {

CountryTimeZones(java.lang.String countryIso, java.lang.String defaultTimeZoneId, boolean everUsesUtc, java.util.List<libcore.timezone.CountryTimeZones.TimeZoneMapping> timeZoneMappings) { throw new RuntimeException("Stub!"); }

/**
 * Returns the ISO code for the country.
 */

public java.lang.String getCountryIso() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if the ISO code for the country is a match for the one specified.
 */

public boolean isForCountryCode(java.lang.String countryIso) { throw new RuntimeException("Stub!"); }

/**
 * Returns the default time zone ID for the country. Can return null in cases when no data is
 * available or the time zone ID provided to
 * {@link #createValidated(String, String, boolean, List, String)} was not recognized.
 */

public java.lang.String getDefaultTimeZoneId() { throw new RuntimeException("Stub!"); }

/**
 * Returns an immutable, ordered list of time zone mappings for the country in an undefined but
 * "priority" order. The list can be empty if there were no zones configured or the configured
 * zone IDs were not recognized.
 */

public java.util.List<libcore.timezone.CountryTimeZones.TimeZoneMapping> getTimeZoneMappings() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

/**
 * Returns true if the country has at least one zone that is the same as UTC at the given time.
 */

public boolean hasUtcZone(long whenMillis) { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if the default time zone for the country is either the only zone used or
 * if it has the same offsets as all other zones used by the country <em>at the specified time
 * </em> making the default equivalent to all other zones used by the country <em>at that time
 * </em>.
 */

public boolean isDefaultOkForCountryTimeZoneDetection(long whenMillis) { throw new RuntimeException("Stub!"); }

/**
 * Returns a time zone for the country, if there is one, that has the desired properties. If
 * there are multiple matches and the {@code bias} is one of them then it is returned, otherwise
 * an arbitrary match is returned based on the {@link #getTimeZoneMappings()} ordering.
 *
 * @param offsetMillis the offset from UTC at {@code whenMillis}
 * @param isDst whether the zone is in DST
 * @param whenMillis the UTC time to match against
 * @param bias the time zone to prefer, can be null
 * @deprecated Use {@link #lookupByOffsetWithBias(int, Integer, long, TimeZone)} instead
 */

@Deprecated
public libcore.timezone.CountryTimeZones.OffsetResult lookupByOffsetWithBias(int offsetMillis, boolean isDst, long whenMillis, android.icu.util.TimeZone bias) { throw new RuntimeException("Stub!"); }
/**
 * The result of lookup up a time zone using offset information (and possibly more).
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class OffsetResult {

OffsetResult(android.icu.util.TimeZone timeZone, boolean oneMatch) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

/** True if there is one match for the supplied criteria */

public final boolean mOneMatch;
{ mOneMatch = false; }

/** A zone that matches the supplied criteria. See also {@link #mOneMatch}. */

public final android.icu.util.TimeZone mTimeZone;
{ mTimeZone = null; }
}

/**
 * A mapping to a time zone ID with some associated metadata.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class TimeZoneMapping {

TimeZoneMapping(java.lang.String timeZoneId, boolean showInPicker, java.lang.Long notUsedAfter) { throw new RuntimeException("Stub!"); }

public static libcore.timezone.CountryTimeZones.TimeZoneMapping createForTests(java.lang.String timeZoneId, boolean showInPicker, java.lang.Long notUsedAfter) { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public final java.lang.Long notUsedAfter;
{ notUsedAfter = null; }

public final boolean showInPicker;
{ showInPicker = false; }

public final java.lang.String timeZoneId;
{ timeZoneId = null; }
}

}

