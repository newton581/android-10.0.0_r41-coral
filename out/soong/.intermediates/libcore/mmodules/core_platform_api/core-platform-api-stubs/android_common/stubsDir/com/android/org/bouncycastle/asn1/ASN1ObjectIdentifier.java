/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * Class representing the ASN.1 OBJECT IDENTIFIER type.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ASN1ObjectIdentifier extends com.android.org.bouncycastle.asn1.ASN1Primitive {

/**
 * Create an OID based on the passed in String.
 *
 * @param identifier a string representation of an OID.
 */

public ASN1ObjectIdentifier(java.lang.String identifier) { throw new RuntimeException("Stub!"); }

/**
 * Return an OID from the passed in object
 *
 * @param obj an ASN1ObjectIdentifier or an object that can be converted into one.
 * @return an ASN1ObjectIdentifier instance, or null.
 * @throws IllegalArgumentException if the object cannot be converted.
 */

public static com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier getInstance(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

/**
 * Return the OID as a string.
 *
 * @return the string representation of the OID carried by this object.
 */

public java.lang.String getId() { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

