/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * Mutable class for building ASN.1 constructed objects such as SETs or SEQUENCEs.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ASN1EncodableVector {

/**
 * Base constructor.
 */

public ASN1EncodableVector() { throw new RuntimeException("Stub!"); }

/**
 * Add an encodable to the vector.
 *
 * @param obj the encodable to add.
 */

public void add(com.android.org.bouncycastle.asn1.ASN1Encodable obj) { throw new RuntimeException("Stub!"); }
}

