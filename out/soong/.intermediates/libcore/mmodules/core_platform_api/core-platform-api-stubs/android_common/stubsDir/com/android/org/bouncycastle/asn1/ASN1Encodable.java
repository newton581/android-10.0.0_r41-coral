/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * Basic interface to produce serialisers for ASN.1 encodings.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface ASN1Encodable {
}

