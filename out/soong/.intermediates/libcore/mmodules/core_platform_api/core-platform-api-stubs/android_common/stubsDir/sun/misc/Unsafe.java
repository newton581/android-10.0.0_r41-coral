/*
 * Copyright (c) 2000, 2013, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */


package sun.misc;


/**
 * A collection of methods for performing low-level, unsafe operations.
 * Although the class and all methods are public, use of this class is
 * limited because only trusted code can obtain instances of it.
 *
 * @author John R. Rose
 * @see #getUnsafe
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class Unsafe {

/**
 * This class is only privately instantiable.
 */

Unsafe() { throw new RuntimeException("Stub!"); }

/**
 * Gets the unique instance of this class. This is only allowed in
 * very limited situations.
 */

public static sun.misc.Unsafe getUnsafe() { throw new RuntimeException("Stub!"); }

/**
 * Gets the raw byte offset from the start of an object's memory to
 * the memory used to store the indicated instance field.
 *
 * @param field non-null; the field in question, which must be an
 * instance field
 * @return the offset to the field
 */

public long objectFieldOffset(java.lang.reflect.Field field) { throw new RuntimeException("Stub!"); }

/**
 * Gets the offset from the start of an array object's memory to
 * the memory used to store its initial (zeroeth) element.
 *
 * @param clazz non-null; class in question; must be an array class
 * @return the offset to the initial element
 */

public int arrayBaseOffset(java.lang.Class clazz) { throw new RuntimeException("Stub!"); }

/**
 * Gets a <code>long</code> field from the given object.
 *
 * @param obj non-null; object containing the field
 * @param offset offset to the field within <code>obj</code>
 * @return the retrieved value
 */

public native long getLong(java.lang.Object obj, long offset);

public native byte getByte(java.lang.Object obj, long offset);

public native void putByte(java.lang.Object obj, long offset, byte newValue);

public native byte getByte(long address);

public native void putByte(long address, byte x);

public native long getLong(long address);
}

