/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.x509;


/**
 * Generator for Version 3 TBSCertificateStructures.
 * <pre>
 * TBSCertificate ::= SEQUENCE {
 *      version          [ 0 ]  Version DEFAULT v1(0),
 *      serialNumber            CertificateSerialNumber,
 *      signature               AlgorithmIdentifier,
 *      issuer                  Name,
 *      validity                Validity,
 *      subject                 Name,
 *      subjectPublicKeyInfo    SubjectPublicKeyInfo,
 *      issuerUniqueID    [ 1 ] IMPLICIT UniqueIdentifier OPTIONAL,
 *      subjectUniqueID   [ 2 ] IMPLICIT UniqueIdentifier OPTIONAL,
 *      extensions        [ 3 ] Extensions OPTIONAL
 *      }
 * </pre>
 * @hide This class is not part of the Android public SDK API
 *
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class V3TBSCertificateGenerator {

public V3TBSCertificateGenerator() { throw new RuntimeException("Stub!"); }

public void setSerialNumber(com.android.org.bouncycastle.asn1.ASN1Integer serialNumber) { throw new RuntimeException("Stub!"); }

public void setSignature(com.android.org.bouncycastle.asn1.x509.AlgorithmIdentifier signature) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated use X500Name method
 */

@Deprecated
public void setIssuer(com.android.org.bouncycastle.asn1.x509.X509Name issuer) { throw new RuntimeException("Stub!"); }

public void setStartDate(com.android.org.bouncycastle.asn1.x509.Time startDate) { throw new RuntimeException("Stub!"); }

public void setEndDate(com.android.org.bouncycastle.asn1.x509.Time endDate) { throw new RuntimeException("Stub!"); }

/**
 * @deprecated use X500Name method
 */

@Deprecated
public void setSubject(com.android.org.bouncycastle.asn1.x509.X509Name subject) { throw new RuntimeException("Stub!"); }

public void setSubjectPublicKeyInfo(com.android.org.bouncycastle.asn1.x509.SubjectPublicKeyInfo pubKeyInfo) { throw new RuntimeException("Stub!"); }

public com.android.org.bouncycastle.asn1.x509.TBSCertificate generateTBSCertificate() { throw new RuntimeException("Stub!"); }
}

