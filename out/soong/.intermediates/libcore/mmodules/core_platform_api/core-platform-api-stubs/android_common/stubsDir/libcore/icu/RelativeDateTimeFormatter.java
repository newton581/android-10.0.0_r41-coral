/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.icu;

import java.util.Locale;

/**
 * Exposes icu4j's RelativeDateTimeFormatter.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class RelativeDateTimeFormatter {

RelativeDateTimeFormatter() { throw new RuntimeException("Stub!"); }

/**
 * This is the internal API that implements the functionality of
 * DateUtils.getRelativeTimeSpanString(long, long, long, int), which is to
 * return a string describing 'time' as a time relative to 'now' such as
 * '5 minutes ago', or 'In 2 days'. More examples can be found in DateUtils'
 * doc.
 *
 * In the implementation below, it selects the appropriate time unit based on
 * the elapsed time between time' and 'now', e.g. minutes, days and etc.
 * Callers may also specify the desired minimum resolution to show in the
 * result. For example, '45 minutes ago' will become '0 hours ago' when
 * minResolution is HOUR_IN_MILLIS. Once getting the quantity and unit to
 * display, it calls icu4j's RelativeDateTimeFormatter to format the actual
 * string according to the given locale.
 *
 * Note that when minResolution is set to DAY_IN_MILLIS, it returns the
 * result depending on the actual date difference. For example, it will
 * return 'Yesterday' even if 'time' was less than 24 hours ago but falling
 * onto a different calendar day.
 *
 * It takes two additional parameters of Locale and TimeZone than the
 * DateUtils' API. Caller must specify the locale and timezone.
 * FORMAT_ABBREV_RELATIVE or FORMAT_ABBREV_ALL can be set in 'flags' to get
 * the abbreviated forms when available. When 'time' equals to 'now', it
 * always // returns a string like '0 seconds/minutes/... ago' according to
 * minResolution.
 */

public static java.lang.String getRelativeTimeSpanString(java.util.Locale locale, java.util.TimeZone tz, long time, long now, long minResolution, int flags) { throw new RuntimeException("Stub!"); }

/**
 * This is the internal API that implements
 * DateUtils.getRelativeDateTimeString(long, long, long, long, int), which is
 * to return a string describing 'time' as a time relative to 'now', formatted
 * like '[relative time/date], [time]'. More examples can be found in
 * DateUtils' doc.
 *
 * The function is similar to getRelativeTimeSpanString, but it always
 * appends the absolute time to the relative time string to return
 * '[relative time/date clause], [absolute time clause]'. It also takes an
 * extra parameter transitionResolution to determine the format of the date
 * clause. When the elapsed time is less than the transition resolution, it
 * displays the relative time string. Otherwise, it gives the absolute
 * numeric date string as the date clause. With the date and time clauses, it
 * relies on icu4j's RelativeDateTimeFormatter::combineDateAndTime() to
 * concatenate the two.
 *
 * It takes two additional parameters of Locale and TimeZone than the
 * DateUtils' API. Caller must specify the locale and timezone.
 * FORMAT_ABBREV_RELATIVE or FORMAT_ABBREV_ALL can be set in 'flags' to get
 * the abbreviated forms when they are available.
 *
 * Bug 5252772: Since the absolute time will always be part of the result,
 * minResolution will be set to at least DAY_IN_MILLIS to correctly indicate
 * the date difference. For example, when it's 1:30 AM, it will return
 * 'Yesterday, 11:30 PM' for getRelativeDateTimeString(null, null,
 * now - 2 hours, now, HOUR_IN_MILLIS, DAY_IN_MILLIS, 0), instead of '2
 * hours ago, 11:30 PM' even with minResolution being HOUR_IN_MILLIS.
 */

public static java.lang.String getRelativeDateTimeString(java.util.Locale locale, java.util.TimeZone tz, long time, long now, long minResolution, long transitionResolution, int flags) { throw new RuntimeException("Stub!"); }
}

