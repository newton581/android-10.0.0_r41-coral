/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;

import java.math.BigInteger;

/**
 * Class representing the ASN.1 INTEGER type.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class ASN1Integer extends com.android.org.bouncycastle.asn1.ASN1Primitive {

/**
 * Construct an INTEGER from the passed in BigInteger value.
 *
 * @param value the BigInteger representing the value desired.
 */

public ASN1Integer(java.math.BigInteger value) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }
}

