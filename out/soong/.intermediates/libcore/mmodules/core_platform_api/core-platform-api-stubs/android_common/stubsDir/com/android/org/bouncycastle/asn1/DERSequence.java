/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * Definite length SEQUENCE, encoding tells explicit number of bytes
 * that the content of this sequence occupies.
 * <p>
 * For X.690 syntax rules, see {@link ASN1Sequence}.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DERSequence extends com.android.org.bouncycastle.asn1.ASN1Sequence {

/**
 * Create an empty sequence
 */

public DERSequence() { throw new RuntimeException("Stub!"); }

/**
 * Create a sequence containing a vector of objects.
 * @param v the vector of objects to make up the sequence.
 */

public DERSequence(com.android.org.bouncycastle.asn1.ASN1EncodableVector v) { throw new RuntimeException("Stub!"); }
}

