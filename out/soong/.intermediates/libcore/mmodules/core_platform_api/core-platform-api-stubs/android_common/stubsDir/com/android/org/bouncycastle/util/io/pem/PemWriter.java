/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.util.io.pem;


/**
 * A generic PEM writer, based on RFC 1421
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class PemWriter extends java.io.BufferedWriter {

/**
 * Base constructor.
 *
 * @param out output stream to use.
 */

public PemWriter(java.io.Writer out) { super((java.io.Writer)null); throw new RuntimeException("Stub!"); }

public void writeObject(com.android.org.bouncycastle.util.io.pem.PemObjectGenerator objGen) throws java.io.IOException { throw new RuntimeException("Stub!"); }
}

