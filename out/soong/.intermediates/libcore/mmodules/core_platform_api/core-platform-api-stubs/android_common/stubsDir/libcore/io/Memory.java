/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


package libcore.io;

import java.nio.ByteBuffer;

/**
 * Unsafe access to memory.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class Memory {

Memory() { throw new RuntimeException("Stub!"); }

public static int peekInt(byte[] src, int offset, java.nio.ByteOrder order) { throw new RuntimeException("Stub!"); }

public static short peekShort(byte[] src, int offset, java.nio.ByteOrder order) { throw new RuntimeException("Stub!"); }

public static void pokeInt(byte[] dst, int offset, int value, java.nio.ByteOrder order) { throw new RuntimeException("Stub!"); }

public static void pokeLong(byte[] dst, int offset, long value, java.nio.ByteOrder order) { throw new RuntimeException("Stub!"); }

public static void pokeShort(byte[] dst, int offset, short value, java.nio.ByteOrder order) { throw new RuntimeException("Stub!"); }

/**
 * Copies 'byteCount' bytes from the source to the destination. The objects are either
 * instances of DirectByteBuffer or byte[]. The offsets in the byte[] case must include
 * the Buffer.arrayOffset if the array came from a Buffer.array call. We could make this
 * private and provide the four type-safe variants, but then ByteBuffer.put(ByteBuffer)
 * would need to work out which to call based on whether the source and destination buffers
 * are direct or not.
 *
 * @hide make type-safe before making public?
 */

public static native void memmove(java.lang.Object dstObject, int dstOffset, java.lang.Object srcObject, int srcOffset, long byteCount);
}

