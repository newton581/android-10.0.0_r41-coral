/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.icu;


/**
 * Passes locale-specific from ICU native code to Java.
 * <p>
 * Note that you share these; you must not alter any of the fields, nor their array elements
 * in the case of arrays. If you ever expose any of these things to user code, you must give
 * them a clone rather than the original.
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class LocaleData {

LocaleData() { throw new RuntimeException("Stub!"); }

/**
 * Returns a shared LocaleData for the given locale.
 */

public static libcore.icu.LocaleData get(java.util.Locale locale) { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public java.lang.String getDateFormat(int style) { throw new RuntimeException("Stub!"); }

public java.lang.String[] amPm;

public java.lang.Integer firstDayOfWeek;

public java.lang.String[] longMonthNames;

public java.lang.String[] longStandAloneMonthNames;

public java.lang.String[] longStandAloneWeekdayNames;

public java.lang.String[] longWeekdayNames;

public java.lang.String narrowAm;

public java.lang.String narrowPm;

public java.lang.String[] shortMonthNames;

public java.lang.String[] shortStandAloneMonthNames;

public java.lang.String[] shortStandAloneWeekdayNames;

public java.lang.String[] shortWeekdayNames;

public java.lang.String timeFormat_Hm;

public java.lang.String timeFormat_Hms;

public java.lang.String timeFormat_hm;

public java.lang.String timeFormat_hms;

public java.lang.String[] tinyMonthNames;

public java.lang.String[] tinyStandAloneMonthNames;

public java.lang.String[] tinyStandAloneWeekdayNames;

public java.lang.String[] tinyWeekdayNames;

public java.lang.String today;

public java.lang.String yesterday;

public char zeroDigit;
}

