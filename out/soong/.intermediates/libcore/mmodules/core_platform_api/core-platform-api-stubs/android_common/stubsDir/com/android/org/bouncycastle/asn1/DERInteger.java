/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * @deprecated  Use ASN1Integer instead of this,
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class DERInteger extends com.android.org.bouncycastle.asn1.ASN1Integer {

@Deprecated
public DERInteger(long value) { super(null); throw new RuntimeException("Stub!"); }
}

