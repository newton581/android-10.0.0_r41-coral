/*
 * Copyright (C) 2014 The Android Open Source Project
 * Copyright (c) 2003, 2011, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */


package sun.security.jca;

import java.util.Set;
import java.security.NoSuchAlgorithmException;

/**
 * Collection of methods to get and set provider list. Also includes
 * special code for the provider list during JAR verification.
 *
 * @author  Andreas Sterbenz
 * @since   1.5
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Providers {

Providers() { throw new RuntimeException("Stub!"); }

/**
 * Start JAR verification. This sets a special provider list for
 * the current thread. You MUST save the return value from this
 * method and you MUST call stopJarVerification() with that object
 * once you are done.
 */

public static java.lang.Object startJarVerification() { throw new RuntimeException("Stub!"); }

/**
 * Stop JAR verification. Call once you have completed JAR verification.
 */

public static void stopJarVerification(java.lang.Object obj) { throw new RuntimeException("Stub!"); }
}

