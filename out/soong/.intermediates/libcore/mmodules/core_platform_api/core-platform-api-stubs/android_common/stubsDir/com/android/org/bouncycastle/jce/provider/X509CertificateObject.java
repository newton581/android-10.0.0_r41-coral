/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.jce.provider;


/**
 * @deprecated Do not use this class directly - either use org.bouncycastle.cert (bcpkix) or CertificateFactory.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
@Deprecated
public class X509CertificateObject extends java.security.cert.X509Certificate {

@Deprecated
public X509CertificateObject(com.android.org.bouncycastle.asn1.x509.Certificate c) throws java.security.cert.CertificateParsingException { throw new RuntimeException("Stub!"); }

public void checkValidity() throws java.security.cert.CertificateExpiredException, java.security.cert.CertificateNotYetValidException { throw new RuntimeException("Stub!"); }

public void checkValidity(java.util.Date date) throws java.security.cert.CertificateExpiredException, java.security.cert.CertificateNotYetValidException { throw new RuntimeException("Stub!"); }

public int getVersion() { throw new RuntimeException("Stub!"); }

public java.math.BigInteger getSerialNumber() { throw new RuntimeException("Stub!"); }

public java.security.Principal getIssuerDN() { throw new RuntimeException("Stub!"); }

public javax.security.auth.x500.X500Principal getIssuerX500Principal() { throw new RuntimeException("Stub!"); }

public java.security.Principal getSubjectDN() { throw new RuntimeException("Stub!"); }

public javax.security.auth.x500.X500Principal getSubjectX500Principal() { throw new RuntimeException("Stub!"); }

public java.util.Date getNotBefore() { throw new RuntimeException("Stub!"); }

public java.util.Date getNotAfter() { throw new RuntimeException("Stub!"); }

public byte[] getTBSCertificate() throws java.security.cert.CertificateEncodingException { throw new RuntimeException("Stub!"); }

public byte[] getSignature() { throw new RuntimeException("Stub!"); }

/**
 * return a more "meaningful" representation for the signature algorithm used in
 * the certficate.
 */

public java.lang.String getSigAlgName() { throw new RuntimeException("Stub!"); }

/**
 * return the object identifier for the signature.
 */

public java.lang.String getSigAlgOID() { throw new RuntimeException("Stub!"); }

/**
 * return the signature parameters, or null if there aren't any.
 */

public byte[] getSigAlgParams() { throw new RuntimeException("Stub!"); }

public boolean[] getIssuerUniqueID() { throw new RuntimeException("Stub!"); }

public boolean[] getSubjectUniqueID() { throw new RuntimeException("Stub!"); }

public boolean[] getKeyUsage() { throw new RuntimeException("Stub!"); }

public java.util.List getExtendedKeyUsage() throws java.security.cert.CertificateParsingException { throw new RuntimeException("Stub!"); }

public int getBasicConstraints() { throw new RuntimeException("Stub!"); }

public java.util.Collection getSubjectAlternativeNames() throws java.security.cert.CertificateParsingException { throw new RuntimeException("Stub!"); }

public java.util.Collection getIssuerAlternativeNames() throws java.security.cert.CertificateParsingException { throw new RuntimeException("Stub!"); }

public java.util.Set getCriticalExtensionOIDs() { throw new RuntimeException("Stub!"); }

public byte[] getExtensionValue(java.lang.String oid) { throw new RuntimeException("Stub!"); }

public java.util.Set getNonCriticalExtensionOIDs() { throw new RuntimeException("Stub!"); }

public boolean hasUnsupportedCriticalExtension() { throw new RuntimeException("Stub!"); }

public java.security.PublicKey getPublicKey() { throw new RuntimeException("Stub!"); }

public byte[] getEncoded() throws java.security.cert.CertificateEncodingException { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }

public synchronized int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public final void verify(java.security.PublicKey key) throws java.security.cert.CertificateException, java.security.InvalidKeyException, java.security.NoSuchAlgorithmException, java.security.NoSuchProviderException, java.security.SignatureException { throw new RuntimeException("Stub!"); }

public final void verify(java.security.PublicKey key, java.lang.String sigProvider) throws java.security.cert.CertificateException, java.security.InvalidKeyException, java.security.NoSuchAlgorithmException, java.security.NoSuchProviderException, java.security.SignatureException { throw new RuntimeException("Stub!"); }

public final void verify(java.security.PublicKey key, java.security.Provider sigProvider) throws java.security.cert.CertificateException, java.security.InvalidKeyException, java.security.NoSuchAlgorithmException, java.security.SignatureException { throw new RuntimeException("Stub!"); }
}

