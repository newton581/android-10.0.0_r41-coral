/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;

import java.io.IOException;

/**
 * Base class for defining an ASN.1 object.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class ASN1Object implements com.android.org.bouncycastle.asn1.ASN1Encodable {

public ASN1Object() { throw new RuntimeException("Stub!"); }

/**
 * Return the default BER or DER encoding for this object.
 *
 * @return BER/DER byte encoded object.
 * @throws java.io.IOException on encoding error.
 */

public byte[] getEncoded() throws java.io.IOException { throw new RuntimeException("Stub!"); }

/**
 * Return either the default for "BER" or a DER encoding if "DER" is specified.
 *
 * @param encoding name of encoding to use.
 * @return byte encoded object.
 * @throws IOException on encoding error.
 */

public byte[] getEncoded(java.lang.String encoding) throws java.io.IOException { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object o) { throw new RuntimeException("Stub!"); }
}

