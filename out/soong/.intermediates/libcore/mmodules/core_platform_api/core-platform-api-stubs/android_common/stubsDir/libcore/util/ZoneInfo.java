/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Elements of the WallTime class are a port of Bionic's localtime.c to Java. That code had the
 * following header:
 *
 * This file is in the public domain, so clarified as of
 * 1996-06-05 by Arthur David Olson.
 */

package libcore.util;

import java.util.TimeZone;
import java.util.GregorianCalendar;
import libcore.timezone.ZoneInfoDB;
import java.io.ObjectInputStream;

/**
 * Our concrete TimeZone implementation, backed by zoneinfo data.
 *
 * <p>This reads time zone information from a binary file stored on the platform. The binary file
 * is essentially a single file containing compacted versions of all the tzfiles produced by the
 * zone info compiler (zic) tool (see {@code man 5 tzfile} for details of the format and
 * {@code man 8 zic}) and an index by long name, e.g. Europe/London.
 *
 * <p>The compacted form is created by {@code external/icu/tools/ZoneCompactor.java} and is used
 * by both this and Bionic. {@link ZoneInfoDB} is responsible for mapping the binary file, and
 * reading the index and creating a {@link BufferIterator} that provides access to an entry for a
 * specific file. This class is responsible for reading the data from that {@link BufferIterator}
 * and storing it a representation to support the {@link TimeZone} and {@link GregorianCalendar}
 * implementations. See {@link ZoneInfo#readTimeZone(String, BufferIterator, long)}.
 *
 * <p>The main difference between {@code tzfile} and the compacted form is that the
 * {@code struct ttinfo} only uses a single byte for {@code tt_isdst} and {@code tt_abbrind}.
 *
 * <p>This class does not use all the information from the {@code tzfile}; it uses:
 * {@code tzh_timecnt} and the associated transition times and type information. For each type
 * (described by {@code struct ttinfo}) it uses {@code tt_gmtoff} and {@code tt_isdst}. Note, that
 * the definition of {@code struct ttinfo} uses {@code long}, and {@code int} but they do not have
 * the same meaning as Java. The prose following the definition makes it clear that the {@code long}
 * is 4 bytes and the {@code int} fields are 1 byte.
 *
 * <p>As the data uses 32 bits to store the time in seconds the time range is limited to roughly
 * 69 years either side of the epoch (1st Jan 1970 00:00:00) that means that it cannot handle any
 * dates before 1900 and after 2038. There is an extended version of the table that uses 64 bits
 * to store the data but that information is not used by this.
 *
 * <p>This class should be in libcore.timezone but this class is Serializable so cannot
 * be moved there without breaking apps that have (for some reason) serialized TimeZone objects.
 *
 * @hide - used to implement TimeZone
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ZoneInfo extends java.util.TimeZone {

ZoneInfo(java.lang.String name, long[] transitions, byte[] types, int[] gmtOffsets, byte[] isDsts, long currentTimeMillis) { throw new RuntimeException("Stub!"); }

public int getOffset(int era, int year, int month, int day, int dayOfWeek, int millis) { throw new RuntimeException("Stub!"); }

public int getOffset(long when) { throw new RuntimeException("Stub!"); }

public boolean inDaylightTime(java.util.Date time) { throw new RuntimeException("Stub!"); }

public int getRawOffset() { throw new RuntimeException("Stub!"); }

public void setRawOffset(int off) { throw new RuntimeException("Stub!"); }

public int getDSTSavings() { throw new RuntimeException("Stub!"); }

public boolean useDaylightTime() { throw new RuntimeException("Stub!"); }

public boolean hasSameRules(java.util.TimeZone timeZone) { throw new RuntimeException("Stub!"); }

public boolean equals(java.lang.Object obj) { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public java.lang.Object clone() { throw new RuntimeException("Stub!"); }
/**
 * A class that represents a "wall time". This class is modeled on the C tm struct and
 * is used to support android.text.format.Time behavior. Unlike the tm struct the year is
 * represented as the full year, not the years since 1900.
 *
 * <p>This class contains a rewrite of various native functions that android.text.format.Time
 * once relied on such as mktime_tz and localtime_tz. This replacement does not support leap
 * seconds but does try to preserve behavior around ambiguous date/times found in the BSD
 * version of mktime that was previously used.
 *
 * <p>The original native code used a 32-bit value for time_t on 32-bit Android, which
 * was the only variant of Android available at the time. To preserve old behavior this code
 * deliberately uses {@code int} rather than {@code long} for most things and performs
 * calculations in seconds. This creates deliberate truncation issues for date / times before
 * 1901 and after 2038. This is intentional but might be fixed in future if all the knock-ons
 * can be resolved: Application code may have come to rely on the range so previously values
 * like zero for year could indicate an invalid date but if we move to long the year zero would
 * be valid.
 *
 * <p>All offsets are considered to be safe for addition / subtraction / multiplication without
 * worrying about overflow. All absolute time arithmetic is checked for overflow / underflow.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class WallTime {

public WallTime() { throw new RuntimeException("Stub!"); }

/**
 * Sets the wall time to a point in time using the time zone information provided. This
 * is a replacement for the old native localtime_tz() function.
 *
 * <p>When going from an instant to a wall time it is always unambiguous because there
 * is only one offset rule acting at any given instant. We do not consider leap seconds.
 */

public void localtime(int timeSeconds, libcore.util.ZoneInfo zoneInfo) { throw new RuntimeException("Stub!"); }

/**
 * Returns the time in seconds since beginning of the Unix epoch for the wall time using the
 * time zone information provided. This is a replacement for an old native mktime_tz() C
 * function.
 *
 * <p>When going from a wall time to an instant the answer can be ambiguous. A wall
 * time can map to zero, one or two instants given sane date/time transitions. Sane
 * in this case means that transitions occur less frequently than the offset
 * differences between them (which could cause all sorts of craziness like the
 * skipping out of transitions).
 *
 * <p>For example, this is not fully supported:
 * <ul>
 *     <li>t1 { time = 1, offset = 0 }
 *     <li>t2 { time = 2, offset = -1 }
 *     <li>t3 { time = 3, offset = -2 }
 * </ul>
 * A wall time in this case might map to t1, t2 or t3.
 *
 * <p>We do not handle leap seconds.
 * <p>We assume that no timezone offset transition has an absolute offset > 24 hours.
 * <p>We do not assume that adjacent transitions modify the DST state; adjustments can
 * occur for other reasons such as when a zone changes its raw offset.
 */

public int mktime(libcore.util.ZoneInfo zoneInfo) { throw new RuntimeException("Stub!"); }

public void setYear(int year) { throw new RuntimeException("Stub!"); }

public void setMonth(int month) { throw new RuntimeException("Stub!"); }

public void setMonthDay(int monthDay) { throw new RuntimeException("Stub!"); }

public void setHour(int hour) { throw new RuntimeException("Stub!"); }

public void setMinute(int minute) { throw new RuntimeException("Stub!"); }

public void setSecond(int second) { throw new RuntimeException("Stub!"); }

public void setWeekDay(int weekDay) { throw new RuntimeException("Stub!"); }

public void setYearDay(int yearDay) { throw new RuntimeException("Stub!"); }

public void setIsDst(int isDst) { throw new RuntimeException("Stub!"); }

public void setGmtOffset(int gmtoff) { throw new RuntimeException("Stub!"); }

public int getYear() { throw new RuntimeException("Stub!"); }

public int getMonth() { throw new RuntimeException("Stub!"); }

public int getMonthDay() { throw new RuntimeException("Stub!"); }

public int getHour() { throw new RuntimeException("Stub!"); }

public int getMinute() { throw new RuntimeException("Stub!"); }

public int getSecond() { throw new RuntimeException("Stub!"); }

public int getWeekDay() { throw new RuntimeException("Stub!"); }

public int getYearDay() { throw new RuntimeException("Stub!"); }

public int getGmtOffset() { throw new RuntimeException("Stub!"); }

public int getIsDst() { throw new RuntimeException("Stub!"); }
}

}

