/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * DER UTF8String object.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DERUTF8String extends com.android.org.bouncycastle.asn1.ASN1Primitive {

/**
 * Basic constructor
 *
 * @param string the string to be carried in the UTF8String object,
 */

public DERUTF8String(java.lang.String string) { throw new RuntimeException("Stub!"); }

public java.lang.String getString() { throw new RuntimeException("Stub!"); }

public java.lang.String toString() { throw new RuntimeException("Stub!"); }

public int hashCode() { throw new RuntimeException("Stub!"); }
}

