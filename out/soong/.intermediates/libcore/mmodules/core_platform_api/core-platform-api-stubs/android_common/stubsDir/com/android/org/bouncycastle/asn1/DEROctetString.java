/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1;


/**
 * Carrier class for a DER encoding OCTET STRING
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DEROctetString extends com.android.org.bouncycastle.asn1.ASN1OctetString {

/**
 * Base constructor.
 *
 * @param string the octets making up the octet string.
 */

public DEROctetString(byte[] string) { super(null); throw new RuntimeException("Stub!"); }
}

