/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.jce.provider;


/**
 * To add the provider at runtime use:
 * <pre>
 * import java.security.Security;
 * import org.bouncycastle.jce.provider.BouncyCastleProvider;
 *
 * Security.addProvider(new BouncyCastleProvider());
 * </pre>
 * The provider can also be configured as part of your environment via
 * static registration by adding an entry to the java.security properties
 * file (found in $JAVA_HOME/jre/lib/security/java.security, where
 * $JAVA_HOME is the location of your JDK/JRE distribution). You'll find
 * detailed instructions in the file but basically it comes down to adding
 * a line:
 * <pre>
 * <code>
 *    security.provider.&lt;n&gt;=org.bouncycastle.jce.provider.BouncyCastleProvider
 * </code>
 * </pre>
 * Where &lt;n&gt; is the preference you want the provider at (1 being the
 * most preferred).
 * <p>Note: JCE algorithm names should be upper-case only so the case insensitive
 * test for getInstance works.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class BouncyCastleProvider extends java.security.Provider {

/**
 * Construct a new provider.  This should only be required when
 * using runtime registration of the provider using the
 * <code>Security.addProvider()</code> mechanism.
 */

public BouncyCastleProvider() { super(null, (double)0, null); throw new RuntimeException("Stub!"); }

/**
 * A set of OBJECT IDENTIFIERs representing acceptable named curves for imported keys.
 */

public static final java.lang.String ACCEPTABLE_EC_CURVES = "acceptableEcCurves";

/**
 * A set of OBJECT IDENTIFIERs to EC Curves providing local curve name mapping.
 */

public static final java.lang.String ADDITIONAL_EC_PARAMETERS = "additionalEcParameters";

/**
 * Diffie-Hellman Default Parameters - VM wide version
 */

public static final java.lang.String DH_DEFAULT_PARAMS = "DhDefaultParams";

/**
 * Elliptic Curve CA parameters - VM wide version
 */

public static final java.lang.String EC_IMPLICITLY_CA = "ecImplicitlyCa";

/**
 * Diffie-Hellman Default Parameters - thread local version
 */

public static final java.lang.String THREAD_LOCAL_DH_DEFAULT_PARAMS = "threadLocalDhDefaultParams";

/**
 * Elliptic Curve CA parameters - thread local version
 */

public static final java.lang.String THREAD_LOCAL_EC_IMPLICITLY_CA = "threadLocalEcImplicitlyCa";
}

