/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.x9;


/**
 *
 * Object identifiers for the various X9 standards.
 * <pre>
 * ansi-X9-62 OBJECT IDENTIFIER ::= { iso(1) member-body(2)
 *                                    us(840) ansi-x962(10045) }
 * </pre>
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public interface X9ObjectIdentifiers {

/**
 * X9.42
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ansi_X9_42 = null;

/** Base OID: 1.2.840.10045 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ansi_X9_62 = null;

/** Two Curve c2onb191v4, OID: 1.2.840.10045.3.0.8 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2onb191v4 = null;

/** Two Curve c2onb191v5, OID: 1.2.840.10045.3.0.9 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2onb191v5 = null;

/** Two Curve c2onb239v4, OID: 1.2.840.10045.3.0.14 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2onb239v4 = null;

/** Two Curve c2onb239v5, OID: 1.2.840.10045.3.0.15 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2onb239v5 = null;

/** Two Curve c2pnb163v1, OID: 1.2.840.10045.3.0.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2pnb163v1 = null;

/** Two Curve c2pnb163v2, OID: 1.2.840.10045.3.0.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2pnb163v2 = null;

/** Two Curve c2pnb163v3, OID: 1.2.840.10045.3.0.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2pnb163v3 = null;

/** Two Curve c2pnb176w1, OID: 1.2.840.10045.3.0.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2pnb176w1 = null;

/** Two Curve c2pnb208w1, OID: 1.2.840.10045.3.0.10 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2pnb208w1 = null;

/** Two Curve c2pnb272w1, OID: 1.2.840.10045.3.0.16 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2pnb272w1 = null;

/** Two Curve c2pnb304w1, OID: 1.2.840.10045.3.0.17 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2pnb304w1 = null;

/** Two Curve c2pnb368w1, OID: 1.2.840.10045.3.0.19 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2pnb368w1 = null;

/** Two Curve c2tnb191v1, OID: 1.2.840.10045.3.0.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2tnb191v1 = null;

/** Two Curve c2tnb191v2, OID: 1.2.840.10045.3.0.6 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2tnb191v2 = null;

/** Two Curve c2tnb191v3, OID: 1.2.840.10045.3.0.7 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2tnb191v3 = null;

/** Two Curve c2tnb239v1, OID: 1.2.840.10045.3.0.11 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2tnb239v1 = null;

/** Two Curve c2tnb239v2, OID: 1.2.840.10045.3.0.12 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2tnb239v2 = null;

/** Two Curve c2tnb239v3, OID: 1.2.840.10045.3.0.13 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2tnb239v3 = null;

/** Two Curve c2tnb359v1, OID: 1.2.840.10045.3.0.18 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2tnb359v1 = null;

/** Two Curve c2tnb431r1, OID: 1.2.840.10045.3.0.20 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier c2tnb431r1 = null;

/**
 * Two Curves
 * <p>
 * OID: 1.2.840.10045.3.0
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier cTwoCurve = null;

/** OID: 1.2.840.10045.1.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier characteristic_two_field = null;

/** X9.42 dhEphem OID: 1.2.840.10046.3.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier dhEphem = null;

/** X9.42 dhHybrid1 OID: 1.2.840.10046.3.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier dhHybrid1 = null;

/** X9.42 dhHybrid2 OID: 1.2.840.10046.3.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier dhHybrid2 = null;

/** X9.42 dhHybridOneFlow OID: 1.2.840.10046.3.6 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier dhHybridOneFlow = null;

/** X9.42 dhOneFlow OID: 1.2.840.10046.3.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier dhOneFlow = null;

/** OID: 1.3.133.16.840.63.0.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier dhSinglePass_cofactorDH_sha1kdf_scheme = null;

/** OID: 1.3.133.16.840.63.0.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier dhSinglePass_stdDH_sha1kdf_scheme = null;

/** X9.42 dhStatic OID: 1.2.840.10046.3.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier dhStatic = null;

/**
 * Diffie-Hellman
 * <pre>
 * dhpublicnumber OBJECT IDENTIFIER ::= {
 *    iso(1) member-body(2)  us(840) ansi-x942(10046) number-type(2) 1
 * }
 * </pre>
 * OID: 1.2.840.10046.2.1
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier dhpublicnumber = null;

/** OID: 1.2.840.10045.4.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ecdsa_with_SHA1 = null;

/** OID: 1.2.840.10045.4.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ecdsa_with_SHA2 = null;

/** OID: 1.2.840.10045.4.3.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ecdsa_with_SHA224 = null;

/** OID: 1.2.840.10045.4.3.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ecdsa_with_SHA256 = null;

/** OID: 1.2.840.10045.4.3.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ecdsa_with_SHA384 = null;

/** OID: 1.2.840.10045.4.3.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ecdsa_with_SHA512 = null;

/**
 * Named curves base
 * <p>
 * OID: 1.2.840.10045.3
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ellipticCurve = null;

/** OID: 1.2.840.10045.1.2.3.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier gnBasis = null;

/**
 * DSA
 * <pre>
 * dsapublicnumber OBJECT IDENTIFIER ::= { iso(1) member-body(2)
 *                                         us(840) ansi-x957(10040) number-type(4) 1 }
 * </pre>
 * Base OID: 1.2.840.10040.4.1
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_dsa = null;

/**
 * <pre>
 * id-dsa-with-sha1 OBJECT IDENTIFIER ::= {
 *     iso(1) member-body(2) us(840) x9-57(10040) x9cm(4) 3 }
 * </pre>
 * OID: 1.2.840.10040.4.3
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_dsa_with_sha1 = null;

/** OID: 1.2.840.10045.2.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_ecPublicKey = null;

/** OID: 1.2.840.10045.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_ecSigType = null;

/** OID: 1.2.840.10045.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_fieldType = null;

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_kdf_kdf2 = null;

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_kdf_kdf3 = null;

/** OID: 1.2.840.10045.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier id_publicKeyType = null;

/** X9.42 MQV1 OID: 1.2.840.10046.3.8 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier mqv1 = null;

/** X9.42 MQV2 OID: 1.2.840.10046.3.7 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier mqv2 = null;

/** OID: 1.3.133.16.840.63.0.16 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier mqvSinglePass_sha1kdf_scheme = null;

/** OID: 1.2.840.10045.1.2.3.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier ppBasis = null;

/** Prime Curve prime192v1, OID: 1.2.840.10045.3.1.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier prime192v1 = null;

/** Prime Curve prime192v2, OID: 1.2.840.10045.3.1.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier prime192v2 = null;

/** Prime Curve prime192v3, OID: 1.2.840.10045.3.1.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier prime192v3 = null;

/** Prime Curve prime239v1, OID: 1.2.840.10045.3.1.4 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier prime239v1 = null;

/** Prime Curve prime239v2, OID: 1.2.840.10045.3.1.5 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier prime239v2 = null;

/** Prime Curve prime239v3, OID: 1.2.840.10045.3.1.6 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier prime239v3 = null;

/** Prime Curve prime256v1, OID: 1.2.840.10045.3.1.7 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier prime256v1 = null;

/**
 * Prime Curves
 * <p>
 * OID: 1.2.840.10045.3.1
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier primeCurve = null;

/** OID: 1.2.840.10045.1.1 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier prime_field = null;

/** OID: 1.2.840.10045.1.2.3.2 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier tpBasis = null;

/** X9.42 schemas base OID: 1.2.840.10046.3 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier x9_42_schemes = null;

/**
 * X9.44
 * <pre>
 *    x9-44 OID ::= {
 *      iso(1) identified-organization(3) tc68(133) country(16) x9(840)
 *      x9Standards(9) x9-44(44)
 *   }
 * </pre>
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier x9_44 = null;

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier x9_44_components = null;

/**
 * X9.63 - Signature Specification
 * <p>
 * Base OID: 1.3.133.16.840.63.0
 */

public static final com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier x9_63_scheme = null;
}

