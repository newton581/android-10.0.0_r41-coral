/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.timezone;

import libcore.util.ZoneInfo;

/**
 * A class used to initialize the time zone database. This implementation uses the
 * Olson tzdata as the source of time zone information. However, to conserve
 * disk space (inodes) and reduce I/O, all the data is concatenated into a single file,
 * with an index to indicate the starting position of each time zone record.
 *
 * @hide - used to implement TimeZone
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class ZoneInfoDB {

ZoneInfoDB() { throw new RuntimeException("Stub!"); }

public static libcore.timezone.ZoneInfoDB.TzData getInstance() { throw new RuntimeException("Stub!"); }
/** @hide */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class TzData implements java.lang.AutoCloseable {

TzData() { throw new RuntimeException("Stub!"); }

/**
 * Loads the data at the specified path and returns the {@link TzData} object if it is valid,
 * otherwise {@code null}.
 */

public static libcore.timezone.ZoneInfoDB.TzData loadTzData(java.lang.String path) { throw new RuntimeException("Stub!"); }

public void validate() throws java.io.IOException { throw new RuntimeException("Stub!"); }

public java.lang.String getVersion() { throw new RuntimeException("Stub!"); }

public libcore.util.ZoneInfo makeTimeZone(java.lang.String id) throws java.io.IOException { throw new RuntimeException("Stub!"); }

public boolean hasTimeZone(java.lang.String id) throws java.io.IOException { throw new RuntimeException("Stub!"); }

public void close() { throw new RuntimeException("Stub!"); }

protected void finalize() throws java.lang.Throwable { throw new RuntimeException("Stub!"); }
}

}

