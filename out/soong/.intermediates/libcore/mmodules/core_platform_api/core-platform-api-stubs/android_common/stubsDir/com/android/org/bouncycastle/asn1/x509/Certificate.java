/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.x509;


/**
 * an X509Certificate structure.
 * <pre>
 *  Certificate ::= SEQUENCE {
 *      tbsCertificate          TBSCertificate,
 *      signatureAlgorithm      AlgorithmIdentifier,
 *      signature               BIT STRING
 *  }
 * </pre>
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class Certificate extends com.android.org.bouncycastle.asn1.ASN1Object {

Certificate(com.android.org.bouncycastle.asn1.ASN1Sequence seq) { throw new RuntimeException("Stub!"); }

public static com.android.org.bouncycastle.asn1.x509.Certificate getInstance(java.lang.Object obj) { throw new RuntimeException("Stub!"); }
}

