/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.x509;

import java.io.IOException;

/**
 * The object that contains the public key stored in a certificate.
 * <p>
 * The getEncoded() method in the public keys in the JCE produces a DER
 * encoded one of these.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class SubjectPublicKeyInfo extends com.android.org.bouncycastle.asn1.ASN1Object {

/**
 @deprecated use SubjectPublicKeyInfo.getInstance()
 */

@Deprecated
SubjectPublicKeyInfo(com.android.org.bouncycastle.asn1.ASN1Sequence seq) { throw new RuntimeException("Stub!"); }

public static com.android.org.bouncycastle.asn1.x509.SubjectPublicKeyInfo getInstance(java.lang.Object obj) { throw new RuntimeException("Stub!"); }
}

