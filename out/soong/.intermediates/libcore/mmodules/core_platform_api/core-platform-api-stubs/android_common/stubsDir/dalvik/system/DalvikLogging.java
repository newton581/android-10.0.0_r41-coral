/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package dalvik.system;


/**
 * Utility methods for logging to {@code DalvikHandlers}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class DalvikLogging {

DalvikLogging() { throw new RuntimeException("Stub!"); }

/**
 * Returns the short logger tag (up to 23 chars) for the given logger name.
 * Traditionally loggers are named by fully-qualified Java classes; this
 * method attempts to return a concise identifying part of such names.
 */

public static java.lang.String loggerNameToTag(java.lang.String loggerName) { throw new RuntimeException("Stub!"); }
}

