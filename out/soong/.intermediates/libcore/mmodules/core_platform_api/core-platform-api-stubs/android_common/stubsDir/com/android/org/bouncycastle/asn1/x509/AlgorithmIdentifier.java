/* GENERATED SOURCE. DO NOT MODIFY. */

package com.android.org.bouncycastle.asn1.x509;


/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class AlgorithmIdentifier extends com.android.org.bouncycastle.asn1.ASN1Object {

public AlgorithmIdentifier(com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier algorithm) { throw new RuntimeException("Stub!"); }

public AlgorithmIdentifier(com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier algorithm, com.android.org.bouncycastle.asn1.ASN1Encodable parameters) { throw new RuntimeException("Stub!"); }

public com.android.org.bouncycastle.asn1.ASN1ObjectIdentifier getAlgorithm() { throw new RuntimeException("Stub!"); }
}

