/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.org.conscrypt;


/**
 * Implements the subset of the JDK Signature interface needed for
 * signature verification using OpenSSL.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class OpenSSLSignature extends java.security.SignatureSpi {

OpenSSLSignature() { throw new RuntimeException("Stub!"); }

protected void engineUpdate(byte input) { throw new RuntimeException("Stub!"); }

protected void engineUpdate(byte[] input, int offset, int len) { throw new RuntimeException("Stub!"); }

protected void engineUpdate(java.nio.ByteBuffer input) { throw new RuntimeException("Stub!"); }

@Deprecated
protected java.lang.Object engineGetParameter(java.lang.String param) throws java.security.InvalidParameterException { throw new RuntimeException("Stub!"); }

protected void engineInitSign(java.security.PrivateKey privateKey) throws java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

protected void engineInitVerify(java.security.PublicKey publicKey) throws java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

@Deprecated
protected void engineSetParameter(java.lang.String param, java.lang.Object value) throws java.security.InvalidParameterException { throw new RuntimeException("Stub!"); }

protected byte[] engineSign() throws java.security.SignatureException { throw new RuntimeException("Stub!"); }

protected boolean engineVerify(byte[] sigBytes) throws java.security.SignatureException { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class MD5RSA extends com.android.org.conscrypt.OpenSSLSignature {

public MD5RSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA1ECDSA extends com.android.org.conscrypt.OpenSSLSignature {

public SHA1ECDSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA1RSA extends com.android.org.conscrypt.OpenSSLSignature {

public SHA1RSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA1RSAPSS extends com.android.org.conscrypt.OpenSSLSignature {

public SHA1RSAPSS() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA224ECDSA extends com.android.org.conscrypt.OpenSSLSignature {

public SHA224ECDSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA224RSA extends com.android.org.conscrypt.OpenSSLSignature {

public SHA224RSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA224RSAPSS extends com.android.org.conscrypt.OpenSSLSignature {

public SHA224RSAPSS() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA256ECDSA extends com.android.org.conscrypt.OpenSSLSignature {

public SHA256ECDSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA256RSA extends com.android.org.conscrypt.OpenSSLSignature {

public SHA256RSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA256RSAPSS extends com.android.org.conscrypt.OpenSSLSignature {

public SHA256RSAPSS() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA384ECDSA extends com.android.org.conscrypt.OpenSSLSignature {

public SHA384ECDSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA384RSA extends com.android.org.conscrypt.OpenSSLSignature {

public SHA384RSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA384RSAPSS extends com.android.org.conscrypt.OpenSSLSignature {

public SHA384RSAPSS() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA512ECDSA extends com.android.org.conscrypt.OpenSSLSignature {

public SHA512ECDSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA512RSA extends com.android.org.conscrypt.OpenSSLSignature {

public SHA512RSA() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class SHA512RSAPSS extends com.android.org.conscrypt.OpenSSLSignature {

public SHA512RSAPSS() { throw new RuntimeException("Stub!"); }
}

}

