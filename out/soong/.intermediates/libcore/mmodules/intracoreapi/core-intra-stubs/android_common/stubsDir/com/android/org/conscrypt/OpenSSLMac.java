/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.org.conscrypt;


/**
 * An implementation of {@link javax.crypto.Mac} which uses BoringSSL to perform all the operations.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class OpenSSLMac extends javax.crypto.MacSpi {

OpenSSLMac(long evp_md, int size) { throw new RuntimeException("Stub!"); }

protected int engineGetMacLength() { throw new RuntimeException("Stub!"); }

protected void engineInit(java.security.Key key, java.security.spec.AlgorithmParameterSpec params) throws java.security.InvalidAlgorithmParameterException, java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

protected void engineUpdate(byte input) { throw new RuntimeException("Stub!"); }

protected void engineUpdate(byte[] input, int offset, int len) { throw new RuntimeException("Stub!"); }

protected void engineUpdate(java.nio.ByteBuffer input) { throw new RuntimeException("Stub!"); }

protected byte[] engineDoFinal() { throw new RuntimeException("Stub!"); }

protected void engineReset() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacMD5 extends com.android.org.conscrypt.OpenSSLMac {

public HmacMD5() { super(0, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacSHA1 extends com.android.org.conscrypt.OpenSSLMac {

public HmacSHA1() { super(0, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacSHA224 extends com.android.org.conscrypt.OpenSSLMac {

public HmacSHA224() throws java.security.NoSuchAlgorithmException { super(0, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacSHA256 extends com.android.org.conscrypt.OpenSSLMac {

public HmacSHA256() throws java.security.NoSuchAlgorithmException { super(0, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacSHA384 extends com.android.org.conscrypt.OpenSSLMac {

public HmacSHA384() throws java.security.NoSuchAlgorithmException { super(0, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacSHA512 extends com.android.org.conscrypt.OpenSSLMac {

public HmacSHA512() { super(0, 0); throw new RuntimeException("Stub!"); }
}

}

