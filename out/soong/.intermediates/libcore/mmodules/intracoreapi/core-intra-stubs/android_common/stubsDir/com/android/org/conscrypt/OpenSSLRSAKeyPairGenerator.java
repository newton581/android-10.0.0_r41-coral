/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.org.conscrypt;


/**
 * An implementation of {@link java.security.KeyPairGenerator} which uses BoringSSL to perform all
 * the operations.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class OpenSSLRSAKeyPairGenerator extends java.security.KeyPairGeneratorSpi {

public OpenSSLRSAKeyPairGenerator() { throw new RuntimeException("Stub!"); }

public java.security.KeyPair generateKeyPair() { throw new RuntimeException("Stub!"); }

public void initialize(int keysize, java.security.SecureRandom random) { throw new RuntimeException("Stub!"); }

public void initialize(java.security.spec.AlgorithmParameterSpec params, java.security.SecureRandom random) throws java.security.InvalidAlgorithmParameterException { throw new RuntimeException("Stub!"); }
}

