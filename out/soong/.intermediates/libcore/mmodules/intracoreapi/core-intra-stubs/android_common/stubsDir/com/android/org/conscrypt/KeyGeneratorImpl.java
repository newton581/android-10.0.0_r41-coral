/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.org.conscrypt;


/**
 * An implementation of {@link javax.crypto.KeyGenerator} suitable for use with other Conscrypt
 * algorithms.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class KeyGeneratorImpl extends javax.crypto.KeyGeneratorSpi {

KeyGeneratorImpl(java.lang.String algorithm, int defaultKeySizeBits) { throw new RuntimeException("Stub!"); }

protected void engineInit(java.security.SecureRandom secureRandom) { throw new RuntimeException("Stub!"); }

protected void engineInit(java.security.spec.AlgorithmParameterSpec params, java.security.SecureRandom secureRandom) throws java.security.InvalidAlgorithmParameterException { throw new RuntimeException("Stub!"); }

protected void engineInit(int keySize, java.security.SecureRandom secureRandom) { throw new RuntimeException("Stub!"); }

protected javax.crypto.SecretKey engineGenerateKey() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class AES extends com.android.org.conscrypt.KeyGeneratorImpl {

public AES() { super(null, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class ARC4 extends com.android.org.conscrypt.KeyGeneratorImpl {

public ARC4() { super(null, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class ChaCha20 extends com.android.org.conscrypt.KeyGeneratorImpl {

public ChaCha20() { super(null, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class DESEDE extends com.android.org.conscrypt.KeyGeneratorImpl {

public DESEDE() { super(null, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacMD5 extends com.android.org.conscrypt.KeyGeneratorImpl {

public HmacMD5() { super(null, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacSHA1 extends com.android.org.conscrypt.KeyGeneratorImpl {

public HmacSHA1() { super(null, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacSHA224 extends com.android.org.conscrypt.KeyGeneratorImpl {

public HmacSHA224() { super(null, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacSHA256 extends com.android.org.conscrypt.KeyGeneratorImpl {

public HmacSHA256() { super(null, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacSHA384 extends com.android.org.conscrypt.KeyGeneratorImpl {

public HmacSHA384() { super(null, 0); throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class HmacSHA512 extends com.android.org.conscrypt.KeyGeneratorImpl {

public HmacSHA512() { super(null, 0); throw new RuntimeException("Stub!"); }
}

}

