/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.org.conscrypt;


/**
 * An implementation of {@link java.security.AlgorithmParameters} that contains only an IV.  The
 * supported encoding formats are ASN.1 (primary) and RAW.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class IvParameters extends java.security.AlgorithmParametersSpi {

public IvParameters() { throw new RuntimeException("Stub!"); }

protected void engineInit(java.security.spec.AlgorithmParameterSpec algorithmParameterSpec) throws java.security.spec.InvalidParameterSpecException { throw new RuntimeException("Stub!"); }

protected void engineInit(byte[] bytes) throws java.io.IOException { throw new RuntimeException("Stub!"); }

protected void engineInit(byte[] bytes, java.lang.String format) throws java.io.IOException { throw new RuntimeException("Stub!"); }

protected <T extends java.security.spec.AlgorithmParameterSpec> T engineGetParameterSpec(java.lang.Class<T> aClass) throws java.security.spec.InvalidParameterSpecException { throw new RuntimeException("Stub!"); }

protected byte[] engineGetEncoded() throws java.io.IOException { throw new RuntimeException("Stub!"); }

protected byte[] engineGetEncoded(java.lang.String format) throws java.io.IOException { throw new RuntimeException("Stub!"); }

protected java.lang.String engineToString() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class AES extends com.android.org.conscrypt.IvParameters {

public AES() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ChaCha20 extends com.android.org.conscrypt.IvParameters {

public ChaCha20() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class DESEDE extends com.android.org.conscrypt.IvParameters {

public DESEDE() { throw new RuntimeException("Stub!"); }
}

}

