/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.org.conscrypt;

import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.DESedeKeySpec;

/**
 * An implementation of {@link javax.crypto.SecretKeyFactory} for use with DESEDE keys.  This
 * class supports {@link SecretKeySpec} and {@link DESedeKeySpec} for key specs.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class DESEDESecretKeyFactory extends javax.crypto.SecretKeyFactorySpi {

public DESEDESecretKeyFactory() { throw new RuntimeException("Stub!"); }

protected javax.crypto.SecretKey engineGenerateSecret(java.security.spec.KeySpec keySpec) throws java.security.spec.InvalidKeySpecException { throw new RuntimeException("Stub!"); }

protected java.security.spec.KeySpec engineGetKeySpec(javax.crypto.SecretKey secretKey, java.lang.Class aClass) throws java.security.spec.InvalidKeySpecException { throw new RuntimeException("Stub!"); }

protected javax.crypto.SecretKey engineTranslateKey(javax.crypto.SecretKey secretKey) throws java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }
}

