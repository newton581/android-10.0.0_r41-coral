/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.org.conscrypt;


/**
 * Implements the JDK Signature interface needed for RAW ECDSA signature
 * generation and verification using BoringSSL.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class OpenSSLSignatureRawECDSA extends java.security.SignatureSpi {

public OpenSSLSignatureRawECDSA() { throw new RuntimeException("Stub!"); }

protected void engineUpdate(byte input) { throw new RuntimeException("Stub!"); }

protected void engineUpdate(byte[] input, int offset, int len) { throw new RuntimeException("Stub!"); }

protected java.lang.Object engineGetParameter(java.lang.String param) throws java.security.InvalidParameterException { throw new RuntimeException("Stub!"); }

protected void engineInitSign(java.security.PrivateKey privateKey) throws java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

protected void engineInitVerify(java.security.PublicKey publicKey) throws java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

protected void engineSetParameter(java.lang.String param, java.lang.Object value) throws java.security.InvalidParameterException { throw new RuntimeException("Stub!"); }

protected byte[] engineSign() throws java.security.SignatureException { throw new RuntimeException("Stub!"); }

protected boolean engineVerify(byte[] sigBytes) throws java.security.SignatureException { throw new RuntimeException("Stub!"); }
}

