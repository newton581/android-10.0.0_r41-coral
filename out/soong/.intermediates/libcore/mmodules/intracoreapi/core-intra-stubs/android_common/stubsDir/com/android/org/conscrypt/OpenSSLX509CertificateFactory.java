/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.org.conscrypt;

import java.security.cert.CRL;

/**
 * An implementation of {@link java.security.cert.CertificateFactory} based on BoringSSL.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public class OpenSSLX509CertificateFactory extends java.security.cert.CertificateFactorySpi {

public OpenSSLX509CertificateFactory() { throw new RuntimeException("Stub!"); }

public java.security.cert.Certificate engineGenerateCertificate(java.io.InputStream inStream) throws java.security.cert.CertificateException { throw new RuntimeException("Stub!"); }

public java.util.Collection<? extends java.security.cert.Certificate> engineGenerateCertificates(java.io.InputStream inStream) throws java.security.cert.CertificateException { throw new RuntimeException("Stub!"); }

public java.security.cert.CRL engineGenerateCRL(java.io.InputStream inStream) throws java.security.cert.CRLException { throw new RuntimeException("Stub!"); }

public java.util.Collection<? extends java.security.cert.CRL> engineGenerateCRLs(java.io.InputStream inStream) throws java.security.cert.CRLException { throw new RuntimeException("Stub!"); }

public java.util.Iterator<java.lang.String> engineGetCertPathEncodings() { throw new RuntimeException("Stub!"); }

public java.security.cert.CertPath engineGenerateCertPath(java.io.InputStream inStream) throws java.security.cert.CertificateException { throw new RuntimeException("Stub!"); }

public java.security.cert.CertPath engineGenerateCertPath(java.io.InputStream inStream, java.lang.String encoding) throws java.security.cert.CertificateException { throw new RuntimeException("Stub!"); }

public java.security.cert.CertPath engineGenerateCertPath(java.util.List<? extends java.security.cert.Certificate> certificates) throws java.security.cert.CertificateException { throw new RuntimeException("Stub!"); }
}

