/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package libcore.net;


/**
 * Network security policy for this process/application.
 *
 * <p>Network stacks/components are expected to honor this policy. Components which can use the
 * Android framework API should be accessing this policy via the framework's
 * {@code android.security.NetworkSecurityPolicy} instead of via this class.
 *
 * <p>The policy currently consists of a single flag: whether cleartext network traffic is
 * permitted. See {@link #isCleartextTrafficPermitted()}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class NetworkSecurityPolicy {

public NetworkSecurityPolicy() { throw new RuntimeException("Stub!"); }

public static libcore.net.NetworkSecurityPolicy getInstance() { throw new RuntimeException("Stub!"); }

/**
 * Returns {@code true} if Certificate Transparency information is required to be presented by
 * the server and verified by the client in TLS connections to {@code hostname}.
 *
 * <p>See RFC6962 section 3.3 for more details.
 */

public abstract boolean isCertificateTransparencyVerificationRequired(java.lang.String hostname);
}

