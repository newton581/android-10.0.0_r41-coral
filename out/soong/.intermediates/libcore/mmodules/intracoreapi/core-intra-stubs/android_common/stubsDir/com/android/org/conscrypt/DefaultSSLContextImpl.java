/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


package com.android.org.conscrypt;


/**
 * Support class for this package.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class DefaultSSLContextImpl extends com.android.org.conscrypt.OpenSSLContextImpl {

/**
 * DefaultSSLContextImpl delegates the work to the super class since there
 * is no way to put a synchronized around both the call to super and the
 * rest of this constructor to guarantee that we don't have races in
 * creating the state shared between all default SSLContexts.
 */

public DefaultSSLContextImpl() throws java.security.GeneralSecurityException, java.io.IOException { super(null); throw new RuntimeException("Stub!"); }

public void engineInit(javax.net.ssl.KeyManager[] kms, javax.net.ssl.TrustManager[] tms, java.security.SecureRandom sr) throws java.security.KeyManagementException { throw new RuntimeException("Stub!"); }
}

