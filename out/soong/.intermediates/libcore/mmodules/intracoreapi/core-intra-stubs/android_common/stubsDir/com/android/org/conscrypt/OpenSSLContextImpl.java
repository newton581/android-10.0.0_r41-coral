/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.org.conscrypt;

import java.security.KeyManagementException;

/**
 * OpenSSL-backed SSLContext service provider interface.
 *
 * <p>Public to allow contruction via the provider framework.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class OpenSSLContextImpl extends javax.net.ssl.SSLContextSpi {

OpenSSLContextImpl(java.lang.String[] algorithms) { throw new RuntimeException("Stub!"); }

/**
 * Initializes this {@code SSLContext} instance. All of the arguments are
 * optional, and the security providers will be searched for the required
 * implementations of the needed algorithms.
 *
 * @param kms the key sources or {@code null}
 * @param tms the trust decision sources or {@code null}
 * @param sr the randomness source or {@code null}
 * @throws KeyManagementException if initializing this instance fails
 */

public void engineInit(javax.net.ssl.KeyManager[] kms, javax.net.ssl.TrustManager[] tms, java.security.SecureRandom sr) throws java.security.KeyManagementException { throw new RuntimeException("Stub!"); }

public javax.net.ssl.SSLSocketFactory engineGetSocketFactory() { throw new RuntimeException("Stub!"); }

public javax.net.ssl.SSLServerSocketFactory engineGetServerSocketFactory() { throw new RuntimeException("Stub!"); }

public javax.net.ssl.SSLEngine engineCreateSSLEngine(java.lang.String host, int port) { throw new RuntimeException("Stub!"); }

public javax.net.ssl.SSLEngine engineCreateSSLEngine() { throw new RuntimeException("Stub!"); }

public javax.net.ssl.SSLSessionContext engineGetServerSessionContext() { throw new RuntimeException("Stub!"); }

public javax.net.ssl.SSLSessionContext engineGetClientSessionContext() { throw new RuntimeException("Stub!"); }
/**
 * Public to allow construction via the provider framework.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class TLSv1 extends com.android.org.conscrypt.OpenSSLContextImpl {

public TLSv1() { super(null); throw new RuntimeException("Stub!"); }
}

/**
 * Public to allow construction via the provider framework.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class TLSv11 extends com.android.org.conscrypt.OpenSSLContextImpl {

public TLSv11() { super(null); throw new RuntimeException("Stub!"); }
}

/**
 * Public to allow construction via the provider framework.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class TLSv12 extends com.android.org.conscrypt.OpenSSLContextImpl {

public TLSv12() { super(null); throw new RuntimeException("Stub!"); }
}

/**
 * Public to allow construction via the provider framework.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static final class TLSv13 extends com.android.org.conscrypt.OpenSSLContextImpl {

public TLSv13() { super(null); throw new RuntimeException("Stub!"); }
}

}

