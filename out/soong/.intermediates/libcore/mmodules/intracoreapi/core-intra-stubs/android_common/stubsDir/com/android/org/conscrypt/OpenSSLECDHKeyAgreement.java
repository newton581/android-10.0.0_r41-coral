/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.org.conscrypt;

import java.security.Key;

/**
 * Elliptic Curve Diffie-Hellman key agreement backed by the OpenSSL engine.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class OpenSSLECDHKeyAgreement extends javax.crypto.KeyAgreementSpi {

public OpenSSLECDHKeyAgreement() { throw new RuntimeException("Stub!"); }

public java.security.Key engineDoPhase(java.security.Key key, boolean lastPhase) throws java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

protected int engineGenerateSecret(byte[] sharedSecret, int offset) throws javax.crypto.ShortBufferException { throw new RuntimeException("Stub!"); }

protected byte[] engineGenerateSecret() { throw new RuntimeException("Stub!"); }

protected javax.crypto.SecretKey engineGenerateSecret(java.lang.String algorithm) { throw new RuntimeException("Stub!"); }

protected void engineInit(java.security.Key key, java.security.SecureRandom random) throws java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

protected void engineInit(java.security.Key key, java.security.spec.AlgorithmParameterSpec params, java.security.SecureRandom random) throws java.security.InvalidAlgorithmParameterException, java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }
}

