/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.org.conscrypt;


/**
 * GCM parameters used during an ciphering operation with {@link OpenSSLCipher}.
 * This class is used internally for backward compatibility with Android versions
 * that did not have the {@code GCMParameterSpec} class, in addition to being the
 * implementation of the GCM AlgorithmParameters implementation.
 * <p>
 * The only supported encoding format is ASN.1, as specified in RFC 5084 section 3.2.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class GCMParameters extends java.security.AlgorithmParametersSpi {

public GCMParameters() { throw new RuntimeException("Stub!"); }

protected void engineInit(java.security.spec.AlgorithmParameterSpec algorithmParameterSpec) throws java.security.spec.InvalidParameterSpecException { throw new RuntimeException("Stub!"); }

protected void engineInit(byte[] bytes) throws java.io.IOException { throw new RuntimeException("Stub!"); }

protected void engineInit(byte[] bytes, java.lang.String format) throws java.io.IOException { throw new RuntimeException("Stub!"); }

protected <T extends java.security.spec.AlgorithmParameterSpec> T engineGetParameterSpec(java.lang.Class<T> aClass) throws java.security.spec.InvalidParameterSpecException { throw new RuntimeException("Stub!"); }

protected byte[] engineGetEncoded() throws java.io.IOException { throw new RuntimeException("Stub!"); }

protected byte[] engineGetEncoded(java.lang.String format) throws java.io.IOException { throw new RuntimeException("Stub!"); }

protected java.lang.String engineToString() { throw new RuntimeException("Stub!"); }
}

