/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package dalvik.system;


/**
 * Interface that enables {@code StrictMode} to install callbacks to implement
 * its policy detection and penalty behavior in {@code libcore} code.
 * <p>
 * The framework separately defines {@code StrictMode.ThreadPolicy} and
 * {@code StrictMode.VmPolicy}, so we mirror that separation here; the former is
 * designed for per-thread policies, and the latter for process-wide policies.
 * <p>
 * Note that this is all best-effort to catch most accidental mistakes and isn't
 * intended to be a perfect mechanism, nor provide any sort of security.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public final class BlockGuard {

BlockGuard() { throw new RuntimeException("Stub!"); }

/**
 * Get the per-thread policy for the current thread.
 *
 * @return the current thread's policy. Will return the {@link #LAX_POLICY}
 *         instance if nothing else is set.
 */

public static dalvik.system.BlockGuard.Policy getThreadPolicy() { throw new RuntimeException("Stub!"); }
/**
 * Per-thread interface used to implement {@code StrictMode.ThreadPolicy}.
 *
 * @hide
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static interface Policy {

/**
 * Called on network operations.
 */

public void onNetwork();
}

}

