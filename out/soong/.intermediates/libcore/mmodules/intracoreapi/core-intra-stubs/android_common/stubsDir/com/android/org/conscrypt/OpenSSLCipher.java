/* GENERATED SOURCE. DO NOT MODIFY. */
/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.org.conscrypt;

import javax.crypto.Cipher;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.NoSuchPaddingException;

/**
 * An implementation of {@link Cipher} using BoringSSL as the backing library.
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract class OpenSSLCipher extends javax.crypto.CipherSpi {

OpenSSLCipher() { throw new RuntimeException("Stub!"); }

protected void engineSetMode(java.lang.String modeStr) throws java.security.NoSuchAlgorithmException { throw new RuntimeException("Stub!"); }

protected void engineSetPadding(java.lang.String paddingStr) throws javax.crypto.NoSuchPaddingException { throw new RuntimeException("Stub!"); }

protected int engineGetBlockSize() { throw new RuntimeException("Stub!"); }

protected int engineGetOutputSize(int inputLen) { throw new RuntimeException("Stub!"); }

protected byte[] engineGetIV() { throw new RuntimeException("Stub!"); }

protected java.security.AlgorithmParameters engineGetParameters() { throw new RuntimeException("Stub!"); }

protected void engineInit(int opmode, java.security.Key key, java.security.SecureRandom random) throws java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

protected void engineInit(int opmode, java.security.Key key, java.security.spec.AlgorithmParameterSpec params, java.security.SecureRandom random) throws java.security.InvalidAlgorithmParameterException, java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

protected void engineInit(int opmode, java.security.Key key, java.security.AlgorithmParameters params, java.security.SecureRandom random) throws java.security.InvalidAlgorithmParameterException, java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

protected byte[] engineUpdate(byte[] input, int inputOffset, int inputLen) { throw new RuntimeException("Stub!"); }

protected int engineUpdate(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset) throws javax.crypto.ShortBufferException { throw new RuntimeException("Stub!"); }

protected byte[] engineDoFinal(byte[] input, int inputOffset, int inputLen) throws javax.crypto.BadPaddingException, javax.crypto.IllegalBlockSizeException { throw new RuntimeException("Stub!"); }

protected int engineDoFinal(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset) throws javax.crypto.BadPaddingException, javax.crypto.IllegalBlockSizeException, javax.crypto.ShortBufferException { throw new RuntimeException("Stub!"); }

protected byte[] engineWrap(java.security.Key key) throws javax.crypto.IllegalBlockSizeException, java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }

protected java.security.Key engineUnwrap(byte[] wrappedKey, java.lang.String wrappedKeyAlgorithm, int wrappedKeyType) throws java.security.InvalidKeyException, java.security.NoSuchAlgorithmException { throw new RuntimeException("Stub!"); }

protected int engineGetKeySize(java.security.Key key) throws java.security.InvalidKeyException { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class EVP_AEAD extends com.android.org.conscrypt.OpenSSLCipher {

EVP_AEAD() { throw new RuntimeException("Stub!"); }

protected int engineDoFinal(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset) throws javax.crypto.BadPaddingException, javax.crypto.IllegalBlockSizeException, javax.crypto.ShortBufferException { throw new RuntimeException("Stub!"); }

protected void engineUpdateAAD(byte[] input, int inputOffset, int inputLen) { throw new RuntimeException("Stub!"); }

protected void engineUpdateAAD(java.nio.ByteBuffer buf) { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class AES extends com.android.org.conscrypt.OpenSSLCipher.EVP_AEAD {

AES() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class GCM extends com.android.org.conscrypt.OpenSSLCipher.EVP_AEAD.AES {

public GCM() { throw new RuntimeException("Stub!"); }

protected java.security.AlgorithmParameters engineGetParameters() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class AES_128 extends com.android.org.conscrypt.OpenSSLCipher.EVP_AEAD.AES.GCM {

public AES_128() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class AES_256 extends com.android.org.conscrypt.OpenSSLCipher.EVP_AEAD.AES.GCM {

public AES_256() { throw new RuntimeException("Stub!"); }
}

}

}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ChaCha20 extends com.android.org.conscrypt.OpenSSLCipher.EVP_AEAD {

public ChaCha20() { throw new RuntimeException("Stub!"); }
}

}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public abstract static class EVP_CIPHER extends com.android.org.conscrypt.OpenSSLCipher {

EVP_CIPHER() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class AES extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER {

AES() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class CBC extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES {

CBC() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class NoPadding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES.CBC {

public NoPadding() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class PKCS5Padding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES.CBC {

public PKCS5Padding() { throw new RuntimeException("Stub!"); }
}

}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class CTR extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES {

public CTR() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ECB extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES {

ECB() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class NoPadding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES.ECB {

public NoPadding() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class PKCS5Padding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES.ECB {

public PKCS5Padding() { throw new RuntimeException("Stub!"); }
}

}

}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class AES_128 extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER {

AES_128() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class CBC extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_128 {

CBC() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class NoPadding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_128.CBC {

public NoPadding() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class PKCS5Padding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_128.CBC {

public PKCS5Padding() { throw new RuntimeException("Stub!"); }
}

}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ECB extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_128 {

ECB() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class NoPadding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_128.ECB {

public NoPadding() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class PKCS5Padding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_128.ECB {

public PKCS5Padding() { throw new RuntimeException("Stub!"); }
}

}

}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class AES_256 extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER {

AES_256() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class CBC extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_256 {

CBC() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class NoPadding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_256.CBC {

public NoPadding() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class PKCS5Padding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_256.CBC {

public PKCS5Padding() { throw new RuntimeException("Stub!"); }
}

}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ECB extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_256 {

ECB() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class NoPadding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_256.ECB {

public NoPadding() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class PKCS5Padding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.AES_256.ECB {

public PKCS5Padding() { throw new RuntimeException("Stub!"); }
}

}

}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class ARC4 extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER {

public ARC4() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class DESEDE extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER {

DESEDE() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class CBC extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.DESEDE {

CBC() { throw new RuntimeException("Stub!"); }
/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class NoPadding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.DESEDE.CBC {

public NoPadding() { throw new RuntimeException("Stub!"); }
}

/**
 * @hide This class is not part of the Android public SDK API
 */

@SuppressWarnings({"unchecked", "deprecation", "all"})
public static class PKCS5Padding extends com.android.org.conscrypt.OpenSSLCipher.EVP_CIPHER.DESEDE.CBC {

public PKCS5Padding() { throw new RuntimeException("Stub!"); }
}

}

}

}

}

