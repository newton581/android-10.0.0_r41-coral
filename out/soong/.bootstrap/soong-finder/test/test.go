
package main

import (
	"io"

	"os"

	"regexp"
	"testing"

	pkg "android/soong/finder"
)

var t = []testing.InternalTest{

	{"TestAddPruneFile", pkg.TestAddPruneFile},

	{"TestCacheEntryPathUnexpectedError", pkg.TestCacheEntryPathUnexpectedError},

	{"TestCanUseCache", pkg.TestCanUseCache},

	{"TestChangeOfDevice", pkg.TestChangeOfDevice},

	{"TestChangeOfUserOrHost", pkg.TestChangeOfUserOrHost},

	{"TestChangingParamsOfSecondFind", pkg.TestChangingParamsOfSecondFind},

	{"TestConcurrentFindDifferentDirectories", pkg.TestConcurrentFindDifferentDirectories},

	{"TestConcurrentFindSameDirectory", pkg.TestConcurrentFindSameDirectory},

	{"TestConsistentCacheOrdering", pkg.TestConsistentCacheOrdering},

	{"TestCorruptedCacheBody", pkg.TestCorruptedCacheBody},

	{"TestCorruptedCacheHeader", pkg.TestCorruptedCacheHeader},

	{"TestDirectoriesAdded", pkg.TestDirectoriesAdded},

	{"TestDirectoriesDeleted", pkg.TestDirectoriesDeleted},

	{"TestDirectoriesMoved", pkg.TestDirectoriesMoved},

	{"TestDirectoriesSwapped", pkg.TestDirectoriesSwapped},

	{"TestDirectoryAndSubdirectoryBothUpdated", pkg.TestDirectoryAndSubdirectoryBothUpdated},

	{"TestDirectoryNotPermitted", pkg.TestDirectoryNotPermitted},

	{"TestEmptyDirectory", pkg.TestEmptyDirectory},

	{"TestEmptyPath", pkg.TestEmptyPath},

	{"TestExcludeDirs", pkg.TestExcludeDirs},

	{"TestFileAdded", pkg.TestFileAdded},

	{"TestFileDeleted", pkg.TestFileDeleted},

	{"TestFileNotPermitted", pkg.TestFileNotPermitted},

	{"TestFilesystemRoot", pkg.TestFilesystemRoot},

	{"TestFindFirst", pkg.TestFindFirst},

	{"TestIncludeFiles", pkg.TestIncludeFiles},

	{"TestNestedDirectories", pkg.TestNestedDirectories},

	{"TestNonexistentDir", pkg.TestNonexistentDir},

	{"TestNumSyscallsOfSecondFind", pkg.TestNumSyscallsOfSecondFind},

	{"TestPruneFiles", pkg.TestPruneFiles},

	{"TestRelativeFilePaths", pkg.TestRelativeFilePaths},

	{"TestRootDir", pkg.TestRootDir},

	{"TestRootDirsContainedInOtherRootDirs", pkg.TestRootDirsContainedInOtherRootDirs},

	{"TestSearchingForFilesExcludedFromCache", pkg.TestSearchingForFilesExcludedFromCache},

	{"TestSingleFile", pkg.TestSingleFile},

	{"TestStatCalls", pkg.TestStatCalls},

	{"TestStrangelyFormattedPaths", pkg.TestStrangelyFormattedPaths},

	{"TestSymlinkPointingToDirectory", pkg.TestSymlinkPointingToDirectory},

	{"TestSymlinkPointingToFile", pkg.TestSymlinkPointingToFile},

	{"TestUncachedDir", pkg.TestUncachedDir},

	{"TestUpdatingDbIffChanged", pkg.TestUpdatingDbIffChanged},

}

var e = []testing.InternalExample{

}

var matchPat string
var matchRe *regexp.Regexp

type matchString struct{}

func MatchString(pat, str string) (result bool, err error) {
	if matchRe == nil || matchPat != pat {
		matchPat = pat
		matchRe, err = regexp.Compile(matchPat)
		if err != nil {
			return
		}
	}
	return matchRe.MatchString(str), nil
}

func (matchString) MatchString(pat, str string) (bool, error) {
	return MatchString(pat, str)
}

func (matchString) StartCPUProfile(w io.Writer) error {
	panic("shouldn't get here")
}

func (matchString) StopCPUProfile() {
}

func (matchString) WriteHeapProfile(w io.Writer) error {
    panic("shouldn't get here")
}

func (matchString) WriteProfileTo(string, io.Writer, int) error {
    panic("shouldn't get here")
}

func (matchString) ImportPath() string {
	return "android/soong/finder"
}

func (matchString) StartTestLog(io.Writer) {
	panic("shouldn't get here")
}

func (matchString) StopTestLog() error {
	panic("shouldn't get here")
}

func main() {

	m := testing.MainStart(matchString{}, t, nil, e)


	os.Exit(m.Run())

}
