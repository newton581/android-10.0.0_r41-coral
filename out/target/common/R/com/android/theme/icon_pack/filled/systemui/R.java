/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.android.theme.icon_pack.filled.systemui;

public final class R {
  public static final class drawable {
    public static final int ic_alarm=0x7f010000;
    public static final int ic_alarm_dim=0x7f010001;
    public static final int ic_arrow_back=0x7f010002;
    public static final int ic_bluetooth_connected=0x7f010003;
    public static final int ic_brightness_thumb=0x7f010004;
    public static final int ic_camera=0x7f010005;
    public static final int ic_cast=0x7f010006;
    public static final int ic_cast_connected=0x7f010007;
    public static final int ic_cast_connected_fill=0x7f010008;
    public static final int ic_close_white=0x7f010009;
    public static final int ic_data_saver=0x7f01000a;
    public static final int ic_data_saver_off=0x7f01000b;
    public static final int ic_drag_handle=0x7f01000c;
    public static final int ic_headset=0x7f01000d;
    public static final int ic_headset_mic=0x7f01000e;
    public static final int ic_hotspot=0x7f01000f;
    public static final int ic_info=0x7f010010;
    public static final int ic_info_outline=0x7f010011;
    public static final int ic_invert_colors=0x7f010012;
    public static final int ic_location=0x7f010013;
    public static final int ic_lockscreen_ime=0x7f010014;
    public static final int ic_notifications_alert=0x7f010015;
    public static final int ic_notifications_silence=0x7f010016;
    public static final int ic_power_low=0x7f010017;
    public static final int ic_power_saver=0x7f010018;
    public static final int ic_qs_bluetooth_connecting=0x7f010019;
    public static final int ic_qs_bluetooth_on=0x7f01001a;
    public static final int ic_qs_cancel=0x7f01001b;
    public static final int ic_qs_no_sim=0x7f01001c;
    public static final int ic_qs_wifi_0=0x7f01001d;
    public static final int ic_qs_wifi_1=0x7f01001e;
    public static final int ic_qs_wifi_2=0x7f01001f;
    public static final int ic_qs_wifi_3=0x7f010020;
    public static final int ic_qs_wifi_4=0x7f010021;
    public static final int ic_qs_wifi_disconnected=0x7f010022;
    public static final int ic_screenshot_delete=0x7f010023;
    public static final int ic_settings=0x7f010024;
    public static final int ic_settings_16dp=0x7f010025;
    public static final int ic_swap_vert=0x7f010026;
    public static final int ic_tune_black_16dp=0x7f010027;
    public static final int ic_volume_alarm=0x7f010028;
    public static final int ic_volume_alarm_mute=0x7f010029;
    public static final int ic_volume_bt_sco=0x7f01002a;
    public static final int ic_volume_media=0x7f01002b;
    public static final int ic_volume_media_mute=0x7f01002c;
    public static final int ic_volume_odi_captions=0x7f01002d;
    public static final int ic_volume_odi_captions_disabled=0x7f01002e;
    public static final int ic_volume_ringer=0x7f01002f;
    public static final int ic_volume_ringer_mute=0x7f010030;
    public static final int ic_volume_ringer_vibrate=0x7f010031;
    public static final int ic_volume_voice=0x7f010032;
    public static final int stat_sys_camera=0x7f010033;
    public static final int stat_sys_managed_profile_status=0x7f010034;
    public static final int stat_sys_mic_none=0x7f010035;
    public static final int stat_sys_vpn_ic=0x7f010036;
  }
}