/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.android.systemui.auto_generated_rro_vendor__;

public final class R {
  public static final class array {
    /**
     * Doze: Table that translates sensor values from the doze_brightness_sensor_type sensor
     * to brightness values; -1 means keeping the current brightness.
     */
    public static final int config_doze_brightness_sensor_to_brightness=0x7f010000;
    /**
     * Doze: Table that translates sensor values from the doze_brightness_sensor_type sensor
     * to an opacity value for a black scrim that is overlayed in AOD1.
     * Valid range is from 0 (transparent) to 255 (opaque).
     * -1 means keeping the current opacity.
     */
    public static final int config_doze_brightness_sensor_to_scrim_opacity=0x7f010001;
  }
  public static final class bool {
    /**
     * Control whether status bar should distinguish HSPA data icon from UMTS
     * data icon on devices
     */
    public static final int config_hspa_data_distinguishable=0x7f020000;
    /**
     * Doze: does this device support STATE_DOZE?
     */
    public static final int doze_display_state_supported=0x7f020001;
    /**
     * Doze: whether the double tap sensor reports 2D touch coordinates
     */
    public static final int doze_double_tap_reports_touch_coordinates=0x7f020002;
    /**
     * Doze: can we assume the pickup sensor includes a proximity check?
     */
    public static final int doze_pickup_performs_proximity_check=0x7f020003;
    /**
     * Doze: does this device support STATE_DOZE_SUSPEND?
     */
    public static final int doze_suspend_display_state_supported=0x7f020004;
  }
  public static final class dimen {
    public static final int keyguard_carrier_text_margin=0x7f030000;
    /**
     * Multi user switch has some intrinsic padding to it
     */
    public static final int multi_user_switch_keyguard_margin=0x7f030001;
    /**
     * for 20dp of padding at 3.5px/dp at default density
     */
    public static final int rounded_corner_content_padding=0x7f030002;
    /**
     * the padding on the top of the statusbar (usually 0)
     */
    public static final int status_bar_padding_top=0x7f030003;
    /**
     * Padding for the system icons on the keyguard (when no multi user switch is showing).
     * The icons always have a 4dp padding in the container so we only need 56 extra px of padding
     * for the corners
     */
    public static final int system_icons_super_container_avatarless_margin_end=0x7f030004;
  }
  public static final class drawable {
    public static final int rounded=0x7f040000;
  }
  public static final class integer {
    /**
     * Preferred refresh rate at keyguard, if supported by the display
     */
    public static final int config_keyguardRefreshRate=0x7f050000;
    /**
     * For how long the lock screen can be on before the display turns off.
     */
    public static final int config_lockScreenDisplayTimeout=0x7f050001;
  }
  public static final class string {
    public static final int config_rounded_mask=0x7f060000;
    /**
     * Type of a sensor that provides a low-power estimate of the desired display
     * brightness, suitable to listen to while the device is asleep (e.g. during
     * always-on display)
     */
    public static final int doze_brightness_sensor_type=0x7f060001;
  }
}