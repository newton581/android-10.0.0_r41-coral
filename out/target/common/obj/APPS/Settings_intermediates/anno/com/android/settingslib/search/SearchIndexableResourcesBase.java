package com.android.settingslib.search;

import java.lang.Class;
import java.lang.Override;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SearchIndexableResourcesBase implements SearchIndexableResources {
  private final Set<Class> mProviders = new HashSet();

  public SearchIndexableResourcesBase() {
    addIndex(com.android.settings.DateTimeSettings.class);
    addIndex(com.android.settings.LegalSettings.class);
    addIndex(com.android.settings.TetherSettings.class);
    addIndex(com.android.settings.accessibility.AccessibilityControlTimeoutPreferenceFragment.class);
    addIndex(com.android.settings.accessibility.AccessibilitySettings.class);
    addIndex(com.android.settings.accessibility.AccessibilityShortcutPreferenceFragment.class);
    addIndex(com.android.settings.accessibility.MagnificationPreferenceFragment.class);
    addIndex(com.android.settings.accessibility.ToggleAutoclickPreferenceFragment.class);
    addIndex(com.android.settings.accessibility.ToggleDaltonizerPreferenceFragment.class);
    addIndex(com.android.settings.accessibility.VibrationSettings.class);
    addIndex(com.android.settings.accounts.AccountDashboardFragment.class);
    addIndex(com.android.settings.accounts.ChooseAccountFragment.class);
    addIndex(com.android.settings.accounts.ManagedProfileSettings.class);
    addIndex(com.android.settings.applications.AppAndNotificationDashboardFragment.class);
    addIndex(com.android.settings.applications.assist.ManageAssist.class);
    addIndex(com.android.settings.applications.specialaccess.SpecialAccessSettings.class);
    addIndex(com.android.settings.applications.specialaccess.deviceadmin.DeviceAdminSettings.class);
    addIndex(com.android.settings.applications.specialaccess.pictureinpicture.PictureInPictureSettings.class);
    addIndex(com.android.settings.applications.specialaccess.premiumsms.PremiumSmsAccess.class);
    addIndex(com.android.settings.applications.specialaccess.vrlistener.VrListenerSettings.class);
    addIndex(com.android.settings.backup.BackupSettingsFragment.class);
    addIndex(com.android.settings.backup.PrivacySettings.class);
    addIndex(com.android.settings.backup.UserBackupSettingsActivity.class);
    addIndex(com.android.settings.biometrics.face.FaceSettings.class);
    addIndex(com.android.settings.connecteddevice.BluetoothDashboardFragment.class);
    addIndex(com.android.settings.datausage.BillingCycleSettings.class);
    addIndex(com.android.settings.datausage.DataSaverSummary.class);
    addIndex(com.android.settings.datausage.UnrestrictedDataAccess.class);
    addIndex(com.android.settings.deletionhelper.AutomaticStorageManagerSettings.class);
    addIndex(com.android.settings.development.featureflags.FeatureFlagsDashboard.class);
    addIndex(com.android.settings.development.gamedriver.GameDriverDashboard.class);
    addIndex(com.android.settings.development.qstile.DevelopmentTileConfigFragment.class);
    addIndex(com.android.settings.deviceinfo.StorageDashboardFragment.class);
    addIndex(com.android.settings.deviceinfo.StorageSettings.class);
    addIndex(com.android.settings.deviceinfo.aboutphone.MyDeviceInfoFragment.class);
    addIndex(com.android.settings.deviceinfo.firmwareversion.FirmwareVersionSettings.class);
    addIndex(com.android.settings.deviceinfo.hardwareinfo.HardwareInfoFragment.class);
    addIndex(com.android.settings.display.ColorModePreferenceFragment.class);
    addIndex(com.android.settings.display.ToggleFontSizePreferenceFragment.class);
    addIndex(com.android.settings.dream.DreamSettings.class);
    addIndex(com.android.settings.enterprise.EnterprisePrivacySettings.class);
    addIndex(com.android.settings.gestures.AssistGestureSettings.class);
    addIndex(com.android.settings.gestures.DoubleTapPowerSettings.class);
    addIndex(com.android.settings.gestures.DoubleTapScreenSettings.class);
    addIndex(com.android.settings.gestures.DoubleTwistGestureSettings.class);
    addIndex(com.android.settings.gestures.GestureSettings.class);
    addIndex(com.android.settings.gestures.GlobalActionsPanelSettings.class);
    addIndex(com.android.settings.gestures.PickupGestureSettings.class);
    addIndex(com.android.settings.gestures.PreventRingingGestureSettings.class);
    addIndex(com.android.settings.gestures.SwipeToNotificationSettings.class);
    addIndex(com.android.settings.gestures.SystemNavigationGestureSettings.class);
    addIndex(com.android.settings.gestures.TapScreenGestureSettings.class);
    addIndex(com.android.settings.inputmethod.AvailableVirtualKeyboardFragment.class);
    addIndex(com.android.settings.inputmethod.PhysicalKeyboardFragment.class);
    addIndex(com.android.settings.inputmethod.UserDictionaryList.class);
    addIndex(com.android.settings.inputmethod.VirtualKeyboardFragment.class);
    addIndex(com.android.settings.language.LanguageAndInputSettings.class);
    addIndex(com.android.settings.location.LocationSettings.class);
    addIndex(com.android.settings.location.RecentLocationRequestSeeAllFragment.class);
    addIndex(com.android.settings.network.NetworkDashboardFragment.class);
    addIndex(com.android.settings.nfc.PaymentSettings.class);
    addIndex(com.android.settings.notification.AppBubbleNotificationSettings.class);
    addIndex(com.android.settings.notification.ConfigureNotificationSettings.class);
    addIndex(com.android.settings.notification.NotificationAccessSettings.class);
    addIndex(com.android.settings.notification.SoundSettings.class);
    addIndex(com.android.settings.notification.ZenAccessSettings.class);
    addIndex(com.android.settings.notification.ZenModeAutomationSettings.class);
    addIndex(com.android.settings.notification.ZenModeBlockedEffectsSettings.class);
    addIndex(com.android.settings.notification.ZenModeBypassingAppsSettings.class);
    addIndex(com.android.settings.notification.ZenModeCallsSettings.class);
    addIndex(com.android.settings.notification.ZenModeMessagesSettings.class);
    addIndex(com.android.settings.notification.ZenModeSettings.class);
    addIndex(com.android.settings.notification.ZenModeSoundVibrationSettings.class);
    addIndex(com.android.settings.print.PrintSettingsFragment.class);
    addIndex(com.android.settings.privacy.PrivacyDashboardFragment.class);
    addIndex(com.android.settings.security.EncryptionAndCredential.class);
    addIndex(com.android.settings.security.LockscreenDashboardFragment.class);
    addIndex(com.android.settings.security.ScreenPinningSettings.class);
    addIndex(com.android.settings.security.SecuritySettings.class);
    addIndex(com.android.settings.security.screenlock.ScreenLockSettings.class);
    addIndex(com.android.settings.security.trustagent.TrustAgentSettings.class);
    addIndex(com.android.settings.sim.SimSettings.class);
    addIndex(com.android.settings.support.SupportDashboardActivity.class);
    addIndex(com.android.settings.system.ResetDashboardFragment.class);
    addIndex(com.android.settings.system.SystemDashboardFragment.class);
    addIndex(com.android.settings.tts.TextToSpeechSettings.class);
    addIndex(com.android.settings.tts.TtsEnginePreferenceFragment.class);
    addIndex(com.android.settings.users.UserSettings.class);
    addIndex(com.android.settings.wallpaper.WallpaperSuggestionActivity.class);
    addIndex(com.android.settings.wifi.ConfigureWifiSettings.class);
    addIndex(com.android.settings.wifi.WifiSettings.class);
    addIndex(com.android.settings.wifi.tether.WifiTetherSettings.class);
  }

  public void addIndex(Class indexClass) {
    mProviders.add(indexClass);
  }

  @Override
  public Collection<Class> getProviderValues() {
    return mProviders;
  }
}
