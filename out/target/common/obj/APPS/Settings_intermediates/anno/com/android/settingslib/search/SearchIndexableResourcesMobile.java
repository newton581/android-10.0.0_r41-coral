package com.android.settingslib.search;

public class SearchIndexableResourcesMobile extends SearchIndexableResourcesBase {
  public SearchIndexableResourcesMobile() {
    addIndex(com.android.settings.DisplaySettings.class);
    addIndex(com.android.settings.applications.defaultapps.AutofillPicker.class);
    addIndex(com.android.settings.applications.managedomainurls.ManageDomainUrls.class);
    addIndex(com.android.settings.connecteddevice.AdvancedConnectedDeviceDashboardFragment.class);
    addIndex(com.android.settings.connecteddevice.ConnectedDeviceDashboardFragment.class);
    addIndex(com.android.settings.connecteddevice.PreviouslyConnectedDeviceDashboardFragment.class);
    addIndex(com.android.settings.connecteddevice.usb.UsbDetailsFragment.class);
    addIndex(com.android.settings.development.DevelopmentSettingsDashboardFragment.class);
    addIndex(com.android.settings.display.AdaptiveSleepSettings.class);
    addIndex(com.android.settings.display.AutoBrightnessSettings.class);
    addIndex(com.android.settings.display.NightDisplaySettings.class);
    addIndex(com.android.settings.display.ScreenZoomSettings.class);
    addIndex(com.android.settings.display.darkmode.DarkModeSettingsFragment.class);
    addIndex(com.android.settings.flashlight.FlashlightHandleActivity.class);
    addIndex(com.android.settings.fuelgauge.PowerUsageAdvanced.class);
    addIndex(com.android.settings.fuelgauge.PowerUsageSummary.class);
    addIndex(com.android.settings.fuelgauge.SmartBatterySettings.class);
    addIndex(com.android.settings.fuelgauge.batterysaver.BatterySaverSettings.class);
    addIndex(com.android.settings.homepage.TopLevelSettings.class);
    addIndex(com.android.settings.location.ScanningSettings.class);
    addIndex(com.android.settings.network.MobileNetworkListFragment.class);
    addIndex(com.android.settings.network.telephony.MobileNetworkSettings.class);
    addIndex(com.android.settings.notification.ZenModeRestrictNotificationsSettings.class);
    addIndex(com.android.settings.wfd.WifiDisplaySettings.class);
  }
}
