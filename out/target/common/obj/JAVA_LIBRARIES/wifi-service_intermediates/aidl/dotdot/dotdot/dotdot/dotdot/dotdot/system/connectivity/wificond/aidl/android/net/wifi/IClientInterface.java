/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.net.wifi;
// IClientInterface represents a network interface that can be used to connect
// to access points and obtain internet connectivity.

public interface IClientInterface extends android.os.IInterface
{
  /** Default implementation for IClientInterface. */
  public static class Default implements android.net.wifi.IClientInterface
  {
    // Get packet counters for this interface.
    // First element in array is the number of successfully transmitted packets.
    // Second element in array is the number of tramsmission failure.
    // This call is valid only when interface is associated with an AP, otherwise
    // it returns an empty array.

    @Override public int[] getPacketCounters() throws android.os.RemoteException
    {
      return null;
    }
    // Do signal poll for this interface.
    // First element in array is the RSSI value in dBM.
    // Second element in array is the transmission bit rate in Mbps.
    // Third element in array is the association frequency in MHz.
    // Fourth element in array is the last received packet bit rate in Mbps.
    // This call is valid only when interface is associated with an AP, otherwise
    // it returns an empty array.

    @Override public int[] signalPoll() throws android.os.RemoteException
    {
      return null;
    }
    // Get the MAC address of this interface.

    @Override public byte[] getMacAddress() throws android.os.RemoteException
    {
      return null;
    }
    // Retrieve the name of the network interface corresponding to this
    // IClientInterface instance (e.g. "wlan0")

    @Override public java.lang.String getInterfaceName() throws android.os.RemoteException
    {
      return null;
    }
    // Get a WifiScanner interface associated with this interface.
    // Returns null when the underlying interface object is destroyed.

    @Override public android.net.wifi.IWifiScannerImpl getWifiScannerImpl() throws android.os.RemoteException
    {
      return null;
    }
    // Set the MAC address of this interface
    // Returns true if the set was successful

    @Override public boolean setMacAddress(byte[] mac) throws android.os.RemoteException
    {
      return false;
    }
    // Sends an arbitrary 802.11 management frame on the current channel.
    // @param frame Bytes of the 802.11 management frame to be sent, including the
    //     header, but not including the frame check sequence (FCS).
    // @param Callback triggered when the transmitted frame is ACKed or the
    //     transmission fails.
    // @param mcs MCS rate which the management frame will be sent at. If mcs < 0,
    //     the driver will select the rate automatically. If the device does not
    //     support sending the frame at a specified MCS rate, the transmission
    //     will be aborted and ISendMgmtFrameEvent.OnFailure() will be called with
    //     reason ISendMgmtFrameEvent.SEND_MGMT_FRAME_ERROR_MCS_UNSUPPORTED.

    @Override public void SendMgmtFrame(byte[] frame, android.net.wifi.ISendMgmtFrameEvent callback, int mcs) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.net.wifi.IClientInterface
  {
    private static final java.lang.String DESCRIPTOR = "android.net.wifi.IClientInterface";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.net.wifi.IClientInterface interface,
     * generating a proxy if needed.
     */
    public static android.net.wifi.IClientInterface asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.net.wifi.IClientInterface))) {
        return ((android.net.wifi.IClientInterface)iin);
      }
      return new android.net.wifi.IClientInterface.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_getPacketCounters:
        {
          data.enforceInterface(descriptor);
          int[] _result = this.getPacketCounters();
          reply.writeNoException();
          reply.writeIntArray(_result);
          return true;
        }
        case TRANSACTION_signalPoll:
        {
          data.enforceInterface(descriptor);
          int[] _result = this.signalPoll();
          reply.writeNoException();
          reply.writeIntArray(_result);
          return true;
        }
        case TRANSACTION_getMacAddress:
        {
          data.enforceInterface(descriptor);
          byte[] _result = this.getMacAddress();
          reply.writeNoException();
          reply.writeByteArray(_result);
          return true;
        }
        case TRANSACTION_getInterfaceName:
        {
          data.enforceInterface(descriptor);
          java.lang.String _result = this.getInterfaceName();
          reply.writeNoException();
          reply.writeString(_result);
          return true;
        }
        case TRANSACTION_getWifiScannerImpl:
        {
          data.enforceInterface(descriptor);
          android.net.wifi.IWifiScannerImpl _result = this.getWifiScannerImpl();
          reply.writeNoException();
          reply.writeStrongBinder((((_result!=null))?(_result.asBinder()):(null)));
          return true;
        }
        case TRANSACTION_setMacAddress:
        {
          data.enforceInterface(descriptor);
          byte[] _arg0;
          _arg0 = data.createByteArray();
          boolean _result = this.setMacAddress(_arg0);
          reply.writeNoException();
          reply.writeInt(((_result)?(1):(0)));
          return true;
        }
        case TRANSACTION_SendMgmtFrame:
        {
          data.enforceInterface(descriptor);
          byte[] _arg0;
          _arg0 = data.createByteArray();
          android.net.wifi.ISendMgmtFrameEvent _arg1;
          _arg1 = android.net.wifi.ISendMgmtFrameEvent.Stub.asInterface(data.readStrongBinder());
          int _arg2;
          _arg2 = data.readInt();
          this.SendMgmtFrame(_arg0, _arg1, _arg2);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.net.wifi.IClientInterface
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      // Get packet counters for this interface.
      // First element in array is the number of successfully transmitted packets.
      // Second element in array is the number of tramsmission failure.
      // This call is valid only when interface is associated with an AP, otherwise
      // it returns an empty array.

      @Override public int[] getPacketCounters() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getPacketCounters, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getPacketCounters();
          }
          _reply.readException();
          _result = _reply.createIntArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // Do signal poll for this interface.
      // First element in array is the RSSI value in dBM.
      // Second element in array is the transmission bit rate in Mbps.
      // Third element in array is the association frequency in MHz.
      // Fourth element in array is the last received packet bit rate in Mbps.
      // This call is valid only when interface is associated with an AP, otherwise
      // it returns an empty array.

      @Override public int[] signalPoll() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        int[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_signalPoll, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().signalPoll();
          }
          _reply.readException();
          _result = _reply.createIntArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // Get the MAC address of this interface.

      @Override public byte[] getMacAddress() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        byte[] _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getMacAddress, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getMacAddress();
          }
          _reply.readException();
          _result = _reply.createByteArray();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // Retrieve the name of the network interface corresponding to this
      // IClientInterface instance (e.g. "wlan0")

      @Override public java.lang.String getInterfaceName() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        java.lang.String _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getInterfaceName, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getInterfaceName();
          }
          _reply.readException();
          _result = _reply.readString();
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // Get a WifiScanner interface associated with this interface.
      // Returns null when the underlying interface object is destroyed.

      @Override public android.net.wifi.IWifiScannerImpl getWifiScannerImpl() throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        android.net.wifi.IWifiScannerImpl _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          boolean _status = mRemote.transact(Stub.TRANSACTION_getWifiScannerImpl, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().getWifiScannerImpl();
          }
          _reply.readException();
          _result = android.net.wifi.IWifiScannerImpl.Stub.asInterface(_reply.readStrongBinder());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // Set the MAC address of this interface
      // Returns true if the set was successful

      @Override public boolean setMacAddress(byte[] mac) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        android.os.Parcel _reply = android.os.Parcel.obtain();
        boolean _result;
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeByteArray(mac);
          boolean _status = mRemote.transact(Stub.TRANSACTION_setMacAddress, _data, _reply, 0);
          if (!_status && getDefaultImpl() != null) {
            return getDefaultImpl().setMacAddress(mac);
          }
          _reply.readException();
          _result = (0!=_reply.readInt());
        }
        finally {
          _reply.recycle();
          _data.recycle();
        }
        return _result;
      }
      // Sends an arbitrary 802.11 management frame on the current channel.
      // @param frame Bytes of the 802.11 management frame to be sent, including the
      //     header, but not including the frame check sequence (FCS).
      // @param Callback triggered when the transmitted frame is ACKed or the
      //     transmission fails.
      // @param mcs MCS rate which the management frame will be sent at. If mcs < 0,
      //     the driver will select the rate automatically. If the device does not
      //     support sending the frame at a specified MCS rate, the transmission
      //     will be aborted and ISendMgmtFrameEvent.OnFailure() will be called with
      //     reason ISendMgmtFrameEvent.SEND_MGMT_FRAME_ERROR_MCS_UNSUPPORTED.

      @Override public void SendMgmtFrame(byte[] frame, android.net.wifi.ISendMgmtFrameEvent callback, int mcs) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeByteArray(frame);
          _data.writeStrongBinder((((callback!=null))?(callback.asBinder()):(null)));
          _data.writeInt(mcs);
          boolean _status = mRemote.transact(Stub.TRANSACTION_SendMgmtFrame, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().SendMgmtFrame(frame, callback, mcs);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.net.wifi.IClientInterface sDefaultImpl;
    }
    static final int TRANSACTION_getPacketCounters = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_signalPoll = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    static final int TRANSACTION_getMacAddress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
    static final int TRANSACTION_getInterfaceName = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
    static final int TRANSACTION_getWifiScannerImpl = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
    static final int TRANSACTION_setMacAddress = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
    static final int TRANSACTION_SendMgmtFrame = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
    public static boolean setDefaultImpl(android.net.wifi.IClientInterface impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.net.wifi.IClientInterface getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  // Get packet counters for this interface.
  // First element in array is the number of successfully transmitted packets.
  // Second element in array is the number of tramsmission failure.
  // This call is valid only when interface is associated with an AP, otherwise
  // it returns an empty array.

  public int[] getPacketCounters() throws android.os.RemoteException;
  // Do signal poll for this interface.
  // First element in array is the RSSI value in dBM.
  // Second element in array is the transmission bit rate in Mbps.
  // Third element in array is the association frequency in MHz.
  // Fourth element in array is the last received packet bit rate in Mbps.
  // This call is valid only when interface is associated with an AP, otherwise
  // it returns an empty array.

  public int[] signalPoll() throws android.os.RemoteException;
  // Get the MAC address of this interface.

  public byte[] getMacAddress() throws android.os.RemoteException;
  // Retrieve the name of the network interface corresponding to this
  // IClientInterface instance (e.g. "wlan0")

  public java.lang.String getInterfaceName() throws android.os.RemoteException;
  // Get a WifiScanner interface associated with this interface.
  // Returns null when the underlying interface object is destroyed.

  public android.net.wifi.IWifiScannerImpl getWifiScannerImpl() throws android.os.RemoteException;
  // Set the MAC address of this interface
  // Returns true if the set was successful

  public boolean setMacAddress(byte[] mac) throws android.os.RemoteException;
  // Sends an arbitrary 802.11 management frame on the current channel.
  // @param frame Bytes of the 802.11 management frame to be sent, including the
  //     header, but not including the frame check sequence (FCS).
  // @param Callback triggered when the transmitted frame is ACKed or the
  //     transmission fails.
  // @param mcs MCS rate which the management frame will be sent at. If mcs < 0,
  //     the driver will select the rate automatically. If the device does not
  //     support sending the frame at a specified MCS rate, the transmission
  //     will be aborted and ISendMgmtFrameEvent.OnFailure() will be called with
  //     reason ISendMgmtFrameEvent.SEND_MGMT_FRAME_ERROR_MCS_UNSUPPORTED.

  public void SendMgmtFrame(byte[] frame, android.net.wifi.ISendMgmtFrameEvent callback, int mcs) throws android.os.RemoteException;
}
